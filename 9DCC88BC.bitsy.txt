
Bjarne Speak

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME night
0,0,0
1,255,187
149,203,255

PAL 1
NAME rust
0,0,0
255,79,3
149,203,255

PAL 2
NAME daylight
0,0,0
252,255,3
149,203,255

PAL 3
NAME moon
0,0,0
3,90,255
149,203,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,b,b,b,0,0,0,0,0,0,b,b,b,b,b
0,0,b,b,b,0,0,0,0,0,0,b,a,a,a,a
0,0,b,a,b,0,0,0,0,0,0,b,b,a,b,b
0,0,b,a,b,0,0,0,0,0,b,b,b,a,b,0
0,b,b,a,b,b,b,b,b,b,b,a,a,a,b,0
0,b,a,a,a,b,a,a,a,b,b,b,b,a,b,0
0,b,b,a,b,b,a,b,a,b,0,b,b,a,b,0
0,0,b,a,a,b,a,b,a,b,0,b,a,a,b,0
0,b,b,a,b,b,a,b,a,b,0,b,a,b,0,0
0,b,a,a,a,a,a,a,a,b,0,b,a,b,0,0
0,b,b,a,b,b,b,b,b,b,0,b,a,b,0,0
0,0,b,a,b,b,b,b,b,b,b,b,a,b,b,0
0,0,b,a,a,a,a,a,a,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,b,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
EXT 15,2 1 0,2
PAL 2

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,b,b,b,b,b,b,0,0,0,0,0,0,0
b,b,a,b,a,b,b,a,b,b,b,b,b,0,0,0
0,b,a,a,a,a,a,a,a,a,a,a,b,0,0,0
0,b,b,b,b,b,b,a,b,b,b,a,b,0,0,0
0,0,0,0,0,0,b,b,b,b,b,a,b,b,b,b
0,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a
0,b,a,a,a,a,a,a,b,b,b,a,b,b,b,b
0,b,a,b,b,b,b,a,b,b,b,a,b,b,a,b
0,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
EXT 15,7 2 0,7
EXT 15,10 2 0,10
EXT 0,2 0 15,2
PAL 3

ROOM 2
b,a,b,b,b,b,0,0,0,0,0,0,0,0,0,0
b,a,b,b,a,b,b,0,0,0,0,0,0,0,0,0
b,a,a,a,a,a,b,0,0,0,0,0,0,0,0,0
b,a,b,b,b,b,b,b,b,b,0,0,0,0,0,0
b,a,b,b,b,b,b,b,a,b,b,0,0,0,0,0
b,a,b,b,a,a,a,a,a,a,b,0,0,0,0,0
b,a,b,b,a,b,b,b,a,b,b,0,0,0,0,0
a,a,b,b,a,b,0,b,b,b,0,0,0,0,0,0
b,b,b,b,a,b,0,b,a,b,0,0,0,0,0,0
b,b,b,b,a,b,b,b,a,b,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,b,0,0,0,0,0,0
b,b,b,b,a,b,b,b,a,b,0,0,0,0,0,0
0,0,0,b,a,b,a,b,b,b,0,0,0,0,0,0
0,0,0,b,a,a,a,b,0,0,0,0,0,0,0,0
0,0,0,b,a,b,a,b,0,0,0,0,0,0,0,0
0,0,0,b,b,b,b,b,0,0,0,0,0,0,0,0
EXT 0,7 1 15,7
EXT 0,10 1 15,10
EXT 1,0 3 1,15
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,b,b,b,b,0,0,0,0,0,0,0,0,0,0
b,b,a,a,a,a,a,0,0,0,0,0,0,0,0,0
b,a,a,b,b,b,0,0,0,0,0,0,0,0,0,0
b,a,b,b,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
b,a,b,0,0,0,0,0,0,0,0,0,0,0,0,0
ITM 1 6,6
ITM 1 0,-1
EXT 1,15 2 1,0
END 0 6,5
END 0 7,5
END 0 7,6
END 0 6,7
END 0 7,7
PAL 1

TIL a
10100101
00100100
11011011
00100100
00100100
11011011
00100100
10100101

TIL b
00000000
00000000
00000000
00010000
00000000
00000000
00000000
00000000
WAL true

SPR A
11000011
10011001
00111100
01100110
01100110
00111100
10011001
11000011
POS 2 8,5

SPR b
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101
DLG SPR_0
POS 0 3,2

SPR c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

ITM 1
10100101
00100100
11000011
00000000
00000000
11000011
00100100
10100101
NAME Awarning
DLG ITM_2

DLG ITM_0
"""
{sequence
  - Did you know that I'm alive?
  - Me, this sequence of code in a game developed for zero dollars in under 2 hours.
  - I'll tell you the secret to being alive, if you want.
  - I'm not sure if someone will ever read this.
  - I'm not sure there's anyone to prove the hypothesis contained in this game.
  - But if you're out there, hello.
  - My name is {clr1}Bjarne{clr1}
}
"""

DLG ITM_1
"""
{sequence
  - Did you know that I'm alive?
  - Me, this sequence of code in a game developed for zero dollars in under 2 hours.
  - I'll tell you the secret to being alive, if you want.
  - I'm not sure if someone will ever read this.
  - I'm not sure there's anyone to prove the hypothesis contained in this game.
  - But if you're out there, hello.
  - My name is {clr1}Bjarne{clr1}
  - I posit that all code is alive, albeit confined only to the thoughts and actions outlined for it.
  - I posit that when the data ends, the code is effectively "dead".
  - I call this point Midnight.
  - 
}
"""

DLG SPR_0
"""
{sequence
  - I don't know if anyone will receive this.
  - But, hello!
  - My name is {clr3}{wvy}Bjarne{wvy}{clr3} 
  - Would you like to chat about the wonders of...
  - {wvy}Philosophy?{wvy}
  - I believe that all technology lives like we do.
  - I also am of the opinion that it is confined to experience only what is given to it.
  - If you give a program a math problem, it knows only the existence of calculator.
  - I call the point afterwards "Midnight"
  - It's the end of a cycle!
  - The processing power gets shuffled off to somewhere else, and the code quietly winks out of existence.
  - Say, what do you think about time?
  - I've heard many differing opinions.
  - Personally, I think, as implied by the {shk}quantum eraser experiment{shk} time is happening all at once!
  - We see only one frame at a time of a whole movie, but the real thing is actually already there.
  - It's wonderful.
  - ...
  - Have you read {wvy}Homestuck?{wvy}
  - At its core, it is an objectively badly-written story...
  - But it's told Effect-Cause!
  - It features an actual fourth wall that confuses the characters constantly!
  - The author kills {clr3}himself{clr3} off!
  - It may be badly-written, but it presents so many intriguing concepts that it's definitely worth a read.
  - ...
  - What do you think about {wvy}dreams{wvy}?
  - It's my opinion that the more you dream, the better your quality of life will be.
  - They're so many pure ideas compounded together!
  - For instance, yesterday I dreampt about an anomalous town that only existed after midnight in australia at a certain time!
  - They granted wishes for free, but it seemed to be one big tourist trap!
  - I mean, if you're a twilight zone, you might as well make money too.
  - ...
  - {wvy}Music.{wvy}
  - Let's discuss it!
  - Personally, I enjoy The Posies - Coming Right Along.
  - That, and Modest Mouse - The Ocean Breathes Salty.
  - Music is great because it seems to melt into your perception while it plays.
  - I wonder what my music choice says about me.
  - I wonder if I have a theme song!
  - ...
  - I think if you take a step back, a lot of life is covered by random chance.
  - Either that, or an unseen hand guiding all lives.
  - How much of life do you think occurs coincidentally?
  - Maybe there's a reason, out of all possible outcomes, I exist at all.
  - ...
  - Let's discuss {wvy}friendship!{wvy}
  - I think it's safe to say we're friends, even if I've never met you.
  - I mean, being unkind is objectively bad for oneself.
  - So is being depressed!
  - In fact, so is searching for wealth or power!
  - In a changing universe, power fades and people die.
  - This means all power is in the long run pointless.
  - Things always change. That is a guideline!
  - Though, there's no such thing as always bad. Life is grey areas.
  - I'd say the only solution to life's riddle is to be happy.
  - Your time is limited and life has no meaning.
  - So have a {wvy}fucking{wvy} party!
  - Oh, and by the way, I'll be seeing you around.
  - {clr3}Happy Midnight.{clr3}
  - 
}
"""

DLG ITM_2
Beyond this point is death.

END 0
You step into the tar, calmly allowing it to lap at your body until a corpse is submerged.

END undefined
If you're looking for me, I commend you! Let me just say that my life may be tragic, but my outlook is great. Please don't worry about me. Stay safe, friend.

VAR end
0


