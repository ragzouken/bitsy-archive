Hex World

PAL 0
46,49,73
158,83,27
238,184,70

SET 0
0cefbcefbcefbcef
efbcefbcefbcefbc
bcefbcefbcefbcef
efb000bcefbcefbc
bc000000b00fbcef
ef000000000cefbc
bc000000000fbcef
ef000000000cefbc
bce000000000bcef
efbc00000000efbc
bcef0000000fbcef
efbc0000000cefbc
bcef00e0000fbcef
efbcefbce00cefbc
bcefbcefbcefbcef
efbcefbcefbcefbc
WAL b,a,c,e,f

TIL a
01000011
11000111
11001110
11011100
10111000
11110110
11111110
11000010

TIL b
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL c
11000000
11100000
01110000
00111000
00011100
00001110
00000111
00000011

TIL d
10000000
10100001
10110011
10111110
10011100
11101110
11000111
11100011

TIL e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111

TIL f
00000011
00000111
00001110
00011100
00111000
01110000
11100000
11000000

SPR A
01110111
01000100
01000100
01110111
01000100
01000100
01000100
01000100
POS 0 7,8

SPR a
01000100
10101010
10101010
10101010
10101010
10101010
10101010
01000100
POS 0 4,3

SPR b
00011001
00011001
00000001
10111101
01011011
00011001
00100101
00100101
POS 0 3,7

SPR c
00010000
00111000
00111000
00111000
00111000
00111000
01111100
00000000
POS 0 10,4

SPR d
00010000
00111000
01111100
01111100
01111100
00010000
00111000
00000000
POS 0 5,11

SPR e
11001100
01001000
10110100
11111100
00000000
01111100
01000011
00111100
POS 0 9,12

DLG a
The mission of Hex Headquarters is to promote creative thought about one of our most basic paradigms: the way we represent numbers. Hexadecimal is a system that has applications in many technical fields and should be a part of basic education in the Information Age. Furthermore, to simplify current usage in the technical arena, Hex Headquarters propose intuitive names and pronunciations for the hex digits. Lastly, Hex Headquarters hopes to open a dialogue on future uses of hexadecimal.

DLG b
HEX-E-POO-HEX-ON-YOU!

DLG c
LELO HEXâ„¢ delivers strength, thinness and sensation through its revolutionary hexagonal structure. It's the first major innovation in years, and the media all over the world is calling it one of the most important advances in condom technology for decades

DLG d
IT WAS ALL A DREAM From HEX Shards of Fate founder Cory Jones, who wanted to innovate classic trading card games and bring them into the 21st century with the depth that only a digital game can provide. Transforming cards, create-a-card gem socketing powers, equipment for your champion that buffed cards all were brought to life by a modern user interface and visual design. In early 2013, HEX Shards of Fate was announced on Kickstarter with a video for the ages, the man showing you exactly what he was willing to do in order to see his dream fulfilled and the game you knew was possible brought to life. Now that HEX is well into its beta, weâ€™ve posted the original Kickstarter video so everyone can remember when HEX was just a manâ€™s vision for the future of trading card games.

DLG e
Honeybeeâ€™s culture is defined by teamwork and dedication to our customers.  Our mission is to build electromechanical and robotic systems that help our customers succeed. We aim to become a seamless extension of our customersâ€™ teams, whether we contribute components or the entire system. To deliver high-performing systems on time and on budget, we get hands-on with projects, diving in with breadboard development and rapid prototyping when other companies would be pushing paper.

"