
                         2099

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME 1
151,148,156
245,249,252
15,15,15

PAL 1
31,102,96
238,255,214
0,0,0

PAL 2
NAME 2
76,71,77
158,122,83
0,0,0

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,a,0,0,0,a,0,0,0,0,a,0,0,0,a,0
0,a,0,0,0,g,0,0,0,0,g,0,0,0,a,0
0,a,0,0,0,g,0,0,0,0,g,0,0,0,a,0
0,a,0,0,0,g,b,0,0,b,g,0,0,0,a,0
0,a,d,d,d,g,0,0,0,0,g,d,d,d,a,0
0,a,d,0,d,g,0,0,0,0,g,d,a,d,a,0
0,a,d,0,d,g,0,e,f,0,g,d,0,d,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,g,b,0,0,b,g,0,0,0,a,0
0,a,0,0,0,g,0,0,0,0,g,0,0,0,a,0
0,a,0,0,0,g,0,0,0,0,g,0,0,0,a,0
0,a,0,0,0,a,0,0,0,0,a,0,0,0,a,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Main entrance
EXT 7,13 1 0,0
PAL 0

ROOM 1
0,0,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,a,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,a,0,0,d,d,0,0,a
a,0,0,0,0,0,0,0,a,0,0,d,d,0,0,a
a,0,0,d,h,d,0,0,a,0,0,0,0,0,0,a
a,0,0,d,h,d,0,0,a,a,a,a,a,a,0,a
a,0,0,d,h,d,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,a,a,a,a,a,a,a,0,a,a,a,a,a,0
a,0,0,0,0,0,0,0,a,0,0,0,0,0,0,0
a,0,0,0,0,a,a,a,a,0,a,a,a,a,0,a
a,0,0,a,a,0,0,b,b,0,a,0,0,0,0,a
a,0,0,a,0,0,0,0,0,0,a,0,d,0,0,a
a,0,0,0,0,0,0,0,0,0,a,0,0,0,0,a
a,a,a,a,a,a,a,i,i,a,a,a,a,a,a,a
NAME Lobby 
ITM 0 7,10
ITM 2 12,14
EXT 7,15 2 7,0
PAL 1

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,d,c,c,0,0,0,0,0,0,0,0,c,c,d,a
a,d,0,0,0,0,0,0,0,0,0,0,0,0,d,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,d,c,c,0,0,0,0,0,0,0,0,c,c,d,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,d,c,c,0,0,0,0,0,0,0,0,c,c,d,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,d,c,c,0,0,0,0,0,0,0,0,c,c,d,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,d,c,c,0,0,0,0,0,0,0,0,c,c,d,a
a,a,a,a,a,b,0,0,0,0,b,a,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,i,i,i,i,i,i,0,0,0,0,a
NAME Offices 
ITM 3 4,12
ITM 6 11,12
EXT 7,15 3 7,0
PAL 2

ROOM 3
0,0,0,0,0,b,0,0,0,0,b,0,0,0,0,0
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
e,j,j,j,f,0,0,0,0,0,0,e,j,j,j,f
0,k,k,k,0,0,0,0,0,0,0,0,k,k,k,0
0,h,h,h,0,0,0,0,0,0,0,0,h,h,h,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,b,0,0,0,0,b,0,a,a,a,a
0,0,0,a,0,0,0,0,0,0,0,0,a,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,i,i,i,i,0,0,0,0,0,0
NAME Production line
ITM 4 14,13
ITM 1 1,13
EXT 7,15 4 7,0
PAL 0

ROOM 4
d,d,d,d,d,b,0,0,0,0,b,d,d,d,d,d
d,d,d,d,d,b,0,0,0,0,b,d,d,d,d,d
d,d,d,d,d,b,0,0,0,0,b,d,d,d,d,d
d,d,d,d,d,b,0,0,0,0,b,d,d,d,d,d
d,d,d,d,d,b,0,0,0,0,b,d,d,d,d,d
d,d,d,d,d,a,0,0,0,0,a,d,d,d,d,d
d,d,d,d,d,a,0,0,0,0,a,d,d,d,d,d
d,d,d,d,d,a,b,0,0,b,a,d,d,d,d,d
d,d,d,d,d,a,b,0,0,b,a,d,d,d,d,d
d,d,d,d,d,a,b,0,0,b,a,d,d,d,d,d
d,d,d,d,d,a,0,0,0,0,a,d,d,d,d,d
d,d,d,d,d,a,c,0,0,c,a,d,d,d,d,d
d,d,d,d,d,a,0,0,0,0,a,d,d,d,d,d
d,d,d,d,d,a,c,0,0,c,a,d,d,d,d,d
d,d,d,d,d,a,b,0,0,b,a,d,d,d,d,d
d,d,d,d,d,a,0,i,i,0,a,d,d,d,d,d
NAME Server room 
ITM 7 7,12
END 0 7,15
PAL 1

TIL a
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
WAL true

TIL b
00011000
00011000
00011000
10011001
01011010
00111100
00011000
00000000

TIL c
00000000
00000000
01111100
01000100
01000100
01000100
01000100
00000000

TIL d
00000000
11010111
00000000
11111111
00000000
11101111
00000000
00000000
>
00000000
11111111
00000000
11111111
00000000
11111111
00000000
00000000
WAL true

TIL e
00000000
00000000
00000000
01111111
01000000
01000000
01000000
00000000

TIL f
00000000
00000000
00000000
11111110
00000010
00000010
00000010
00000000

TIL g
11000000
01100000
00110000
00011000
00001000
00000110
00000011
00000001
WAL true

TIL h
01100110
01100110
01111110
01111110
01111110
01111110
00111100
00000000
>
01100110
01000010
01100110
01100110
01100110
01011010
00111100
00000000
WAL true

TIL i
01000010
01000010
01100110
01100110
01100110
01000010
01000010
01000010

TIL j
00100100
00100100
00100100
00100100
00100100
00100100
00100100
00100100
>
00111100
00110100
00110100
00101100
00101100
00111100
00111100
00111100

TIL k
00000000
00000000
00000000
01000010
01000010
01000010
01000010
01111110

SPR A
01000010
00111100
00100100
00100100
01011010
00011000
00100100
00100100
>
00000000
01111110
00100100
00100100
01011010
00011000
00100100
00100100
POS 0 7,2

SPR a
00000000
00000000
00011000
00011000
00111100
00011000
00011000
00011000
DLG SPR_0
POS 2 7,3

SPR b
00000000
00000000
00011000
00011000
00111100
01011010
00011000
00011000
>
00000000
00000000
00011000
00011000
00111100
00011000
00011000
00011000
DLG SPR_3
POS 0 12,7

SPR c
00100100
00111100
00011000
00111100
01011010
00011000
00011000
00011000
DLG SPR_2
POS 0 8,9

SPR d
00100100
00111100
00011000
00111100
01011010
00011000
00011000
00011000
DLG SPR_1
POS 0 7,9

SPR e
00111100
00100100
00111100
01111110
11111111
10111101
00100100
00100100
>
00111100
00111100
00111100
01111110
11111111
10111101
00100100
00100100
DLG SPR_4
POS 1 3,2

SPR f
00000000
00111100
00100100
00111100
00100100
11111111
01011010
01011010
>
00000000
00111100
00111100
00111100
00111100
11111111
01000010
01000010
DLG SPR_5
POS 1 10,3

SPR g
00000000
00111100
00100100
00111100
00100100
11111111
01011010
01011010
>
00000000
00111100
00111100
00111100
00111100
11111111
01000010
01000010
POS 1 13,3

SPR h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR i
00000000
00000000
00011000
00011000
00111100
00011000
00011000
00011000
DLG SPR_6
POS 0 3,7

SPR j
00000000
01001000
01001000
01111000
00110000
01111000
10000100
10000100
DLG SPR_7
POS 3 8,3

ITM 0
00000000
00111100
00111100
00111100
00111100
00111100
00111100
00000000
NAME E-book 
DLG ITM_0

ITM 1
00000000
00000000
01111100
01111100
01111100
00000000
00000000
00000000
NAME E-article 2
DLG ITM_4

ITM 2
00000000
00000000
11111111
11111111
11111111
11111111
11111111
00000000
NAME E-article  
DLG ITM_1

ITM 3
00000000
00111100
00111100
00111100
00111100
00111100
00111100
00000000
NAME E-book 2 
DLG ITM_2

ITM 4
00000000
00111100
00111100
01111110
01111110
00111100
00111100
00000000
NAME Advert 
DLG ITM_3

ITM 5
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

ITM 6
01111110
01000010
01000010
01011010
01011010
01000010
01000010
01111110
NAME poster 
DLG ITM_5

ITM 7
00000000
00000000
01111100
01111100
01111100
00000000
00000000
00000000
NAME E-letter
DLG ITM_6

DLG SPR_0
Welcome to the offices.   

DLG SPR_1
"""
 Scanning.... Scanning...
Automatic clearance activated please proceed Talus-8.  
"""

DLG SPR_2
"""
 Scanning.... Scanning...
Automatic clearance activated please proceed Talus-8
"""

DLG SPR_3
Hello my fellow robot, welcome back to, Robo systems. 

DLG SPR_4
Hello you are currently in the lobby. The date is the 31st December 2099. Our human compatriots are drinking and celebrating tonight. Robo systems is currently closed for business. But will reopen on 2nd January 2100. My scanner has picked up that you are a Talus 8 model. You are guaranteed all access clearance, feel free to look around the building. 

DLG ITM_0
"""
Whats happening to the world? 
Read an experts view. 
Reports are reading that 83% of the Amazon rain forest has been destroyed. 2 years ago we witnessed Florida and Venice completely disappear, due to the sea levels rising an incredible 7 meters. Fracking and mining still manages to go on causing a mass extinction of flora. The question is how long can the Earth survive? 
"""

DLG ITM_1
"""
Today's headline; Weyland Yuntai poses the question, is there alien life?
Following their emergence as a business this year Weyland Yuntai will be planning several space missions in the coming years. Lets hope they are a success. 
"""

DLG SPR_5
Cleaning in progress please stand clear.

DLG ITM_2
Robots and other machines have cleared the way of humans working full time. The average human now only works 20 hours per week due to most work processes being electronic. 

DLG SPR_6
Welcome to Robo systems.

DLG ITM_3
Brand new model coming soon! A new robot programmed in 6 million forms of communication is due to go on sale next year. Order yours today for a special gold plated edition.  

DLG ITM_4
We look back over the last 20 years. The polar ice caps melted which wiped out the polar bear population. Tensions rose between cities and the native tribes people as their land was destroyed. 90% of the population now live in mega cities such as LA and Beijing. 

DLG SPR_7
Welcome to the production line. Work has temporaily stopped. 

DLG ITM_5
Wipeout 2100 anti grav racing league to begin on 31st January 2100 

DLG ITM_6
Talus 8. The world is dying, population ever increasing, animals becoming extinct, industries shutting down, humans living in a constant cycle of anger, but they are too late. They have caused these problems. There is hope, a settlement has been setup on mars in hope of providing a new start for the humans. I cannot say who I am, but if you carry on through the door, all will be revealed and we will change the world. 

END 0
Happy New Year in 5.. 4.. 3.. 2.. 1...

VAR a
42


