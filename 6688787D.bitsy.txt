After bypassing the arcane wards, the mighty stone doors bow to your will. The room is spacious and refined, but the softly glowing gems in the ceiling fill the room with an uncanny air. A row of bookshelves filled with scholarly works of dubious provenance stretches along one wall, while an altar surrounded by four pillars dominates the center of the room.

PAL 0
64,0,0
192,192,192
192,192,192

SET 0
0000000000000000
aaaaaaaaaaaaaaa0
00000000000000a0
00000000000000a0
aab00000000000a0
0ab000c000c000a0
0ab00000000000a0
0ab00000d0000ea0
0ab00000d00000a0
0ab00000000000a0
0ab000c000c000a0
0ab00000000000a0
0ab00000000000a0
0ab0000000000fa0
0aaaaaaaaaaaaaa0
0000000000000000
WAL a,b,d

TIL a
11111111
10001001
10001001
10011001
10011001
10010001
10010001
11111111

TIL b
01110000
01111000
01110000
01111000
01110000
01111000
01110000
01111000

TIL c
00000000
00011000
00011000
00011000
00011000
00011000
00111100
00111100

TIL d
00000000
00100100
00100100
00011000
00011000
00100100
00100100
00000000

TIL e
00000111
00000001
10000001
10000001
10000001
10000001
00000001
00000111

TIL f
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000

SPR A
00100100
10011100
10011000
10111101
01111111
00111101
00100100
01100110
POS 0 1,2

SPR a
00000000
00011000
00011000
00011000
00011000
00011000
00111100
01111110
POS 0 6,10

SPR b
00000000
00011000
00011000
00011000
00011000
00011000
00111100
01111110
POS 0 6,5

SPR c
00000000
00011000
00011000
00011000
00011000
00011000
00111100
01111110
POS 0 10,5

SPR e
00000000
00011000
00011000
00011000
00011000
00011000
00111100
01111110
POS 0 10,10

SPR f
01110000
01110000
01110000
01110000
01111000
01110000
01110000
01110000
POS 0 2,9

SPR g
00000000
00100100
00100100
00011000
00011000
00100100
00100100
00000000
POS 0 8,8

SPR h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 0 12,7

SPR i
00010010
00100100
01001000
00100100
00010010
00100100
01001000
00100100
POS 0 13,13

DLG a
The pillar is carved with roaring flames lurching from fissures in the base and rising to an twisting inferno at the crown. It warms softly, but relentlessly under your touch. You think it may not be wise to leave your hand there too long...

DLG b
Clouds wrap across the pillar, starting sunlit and golden on top and then growing darker as they descend the polished stone. Jagged lightning rips through the middle of the column, piercing a misty base. The clouds seem to swirl whenever you aren't looking at them.

DLG c
A raging sea filled with dark, swimming shapes roils along the base of the pillar. Great torrent of rain fall from some unspecified source at the top. Whenever you aren't looking directly at them, the shapes seem to glide majestically beneath the waves. The stone is cool and worn, as if eroded into its current form.

DLG e
This pillar of rough hewn stone is without embellishment. It almost looks unfinished, as if someone hacked briefly at a natural stalagmite.

DLG f
The shelves here are without dust and almost entirely empty. A single book on the third shelf lies flat, as if overlooked in the haste of emptying the shelves. The title reads "Elementary" in gold leaf. The text is dense with arcane symbols in tongue best forgotten.

DLG g
The altar hunches before you in tangle of bone and wood. It seems grown more than carved, with sharp outgrowths jutting off from the central lattice. A chill radiates from it, burning your lungs as you breathe. You see a few last dark stains fade into the greedy tendrils of wood and bone.

DLG h
The plush canopy bed before you is draped in heavy velvet covers and down feather pillows. A foot locker also serves as an end table as well. Bottles of fine wine stand in one section, along with fine silk pajamas. The bed is unmade.

DLG i
The air itself shimmers, jagged and torn. A strange pressure greets you as you try to come closer, like a cork in a bottle. You try to catch a glimpse of anything through the gap but your eyes cannot make sense of what they see.

