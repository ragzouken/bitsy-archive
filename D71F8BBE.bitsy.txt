"Princess" - by LadyCookie

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
NAME red
217,180,176
122,29,67
207,71,82

PAL 1
NAME gray
181,181,181
54,54,54
74,148,141

PAL 2
NAME purple
209,172,255
109,70,148
142,103,255

PAL 3
NAME pink
254,176,254
158,41,129
184,90,207

PAL 4
NAME green
185,255,194
50,150,80
166,173,12

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,0,i,0,0,h,0,0,0,0,a,a
a,a,0,0,0,0,i,0,0,h,0,0,0,0,a,a
a,a,0,0,0,o,f,e,e,g,o,0,0,0,a,a
a,a,0,0,0,b,0,c,d,0,b,0,0,0,a,a
a,a,0,0,0,0,0,c,d,0,0,0,0,0,a,a
a,a,0,0,0,o,0,c,d,0,o,0,0,0,a,a
a,a,0,0,0,b,0,c,d,0,b,0,0,0,a,a
a,a,0,0,0,0,0,c,d,0,0,0,0,0,a,a
a,a,0,0,0,o,0,c,d,0,o,0,0,0,a,a
a,a,0,0,0,b,0,c,d,0,b,0,0,0,a,a
a,a,0,0,0,0,0,c,d,0,0,0,0,0,a,a
a,a,0,0,0,0,0,c,d,0,0,0,0,0,a,a
a,a,a,a,a,a,a,c,d,a,a,a,a,a,a,a
a,a,a,a,a,a,a,c,d,a,a,a,a,a,a,a
NAME throne
EXT 7,15 1 3,1
EXT 8,15 1 4,1
PAL 3

ROOM 1
a,a,a,0,0,a,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,a,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,a,a,a,l,m,a,a,l,m,a,a
a,a,a,0,0,a,a,a,k,n,a,a,k,n,a,a
a,a,a,0,0,a,o,a,a,a,o,a,a,a,o,a
a,a,a,0,0,0,b,0,0,0,b,0,0,0,b,0
a,a,a,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,0,0,0,o,0,0,0,o,0,0,0,o,0
a,a,a,0,0,0,b,0,0,0,b,0,0,0,b,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
EXT 3,0 0 7,14
EXT 4,0 0 8,14
EXT 15,6 2 1,7
EXT 15,7 2 1,8
PAL 2

ROOM 2
a,a,a,a,a,a,o,0,0,o,a,a,a,a,a,a
a,a,a,a,a,a,b,0,0,b,a,a,a,a,a,a
a,l,m,a,a,a,0,0,0,0,a,a,a,l,m,a
a,k,n,a,a,a,o,0,0,o,a,a,a,k,n,a
a,a,a,a,a,a,b,0,0,b,a,a,a,a,a,a
o,a,a,a,o,a,0,0,0,0,a,o,a,a,o,a
b,0,0,0,b,0,0,0,0,0,0,b,0,0,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
o,0,0,0,o,0,0,0,0,0,0,o,0,0,o,0
b,0,0,0,b,0,0,0,0,0,0,b,0,0,b,0
a,a,a,a,a,a,o,0,0,o,a,a,a,a,a,a
j,j,j,j,j,a,b,p,p,b,a,j,j,j,j,j
j,j,j,j,j,a,0,p,p,0,a,j,j,j,j,j
j,j,j,j,j,a,o,p,p,o,a,j,j,j,j,j
j,j,j,j,j,a,b,p,p,b,a,j,j,j,j,j
NAME corridor
EXT 0,7 1 14,6
EXT 0,8 1 14,7
EXT 7,0 3 7,14
EXT 8,0 3 8,14
EXT 15,7 4 1,7
EXT 15,8 4 1,7
EXT 15,9 4 1,7
PAL 1

ROOM 3
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,r,a,0,0,0,0,a,r,a,j,j,j
j,j,j,a,q,a,0,0,0,0,a,q,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,a,o,0,0,0,0,o,a,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,r,a,0,0,0,0,a,r,a,j,j,j
j,j,j,a,q,a,0,0,0,0,a,q,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,a,o,0,0,0,0,o,a,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
j,j,j,a,r,a,0,0,0,0,a,r,a,j,j,j
j,j,j,a,q,a,0,0,0,0,a,q,a,j,j,j
j,j,j,a,a,a,0,0,0,0,a,a,a,j,j,j
EXT 6,15 2 7,1
EXT 7,15 2 7,1
EXT 8,15 2 8,1
EXT 9,15 2 8,1
END 0 6,0
END 0 7,0
END 0 8,0
END 0 9,0
PAL 4

ROOM 4
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,o,a,a,a,a,a,a,a,a,a,a,a,a,o,a
a,a,a,u,v,a,a,u,v,a,a,u,v,a,a,a
a,a,a,t,w,a,a,t,w,a,a,t,w,a,a,a
a,a,a,s,x,a,a,s,x,a,a,s,x,a,a,a
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,o,a,a,a,o,a,a,a,a,o,a,a,a,o,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
EXT 0,6 2 14,7
EXT 0,7 2 14,8
EXT 0,8 2 14,9
END 1 15,6
END 1 15,7
END 1 15,8
PAL 0

TIL a
11011111
01100011
11010100
10100011
01010001
10101010
00110101
10111011
WAL true

TIL b
11111111
10000001
01111110
01000010
01000010
10100101
10011001
11100111
WAL true

TIL c
01100000
01010000
01100000
01010000
01100000
01010000
01100000
01010000

TIL d
00000110
00001010
00000110
00001010
00000110
00001010
00000110
00001010

TIL e
00000000
00000000
00000000
00000000
00000000
11111111
00000000
11111111

TIL f
10110000
01001000
00100100
00010010
00001001
00000101
00000011
00000001

TIL g
00001101
00010010
00100100
01001000
10010000
10100000
11000000
10000000

TIL h
00000011
00000011
00000011
00000011
00000011
00000011
00000011
00000011

TIL i
11000000
11000000
11000000
11000000
11000000
11000000
11000000
11000000
WAL false

TIL j
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL k
10000100
10000100
11111111
10000100
10000100
10000100
10000100
11111111
WAL true

TIL l
00011111
00100100
01000100
10000100
10000100
11111111
10000100
10000100
WAL true

TIL m
11111000
00100100
00100010
00100001
00100001
11111111
00100001
00100001
WAL true

TIL n
00100001
00100001
11111111
00100001
00100001
00100001
00100001
11111111
WAL true

TIL o
00100000
00010010
00101000
10100100
00100100
00010100
00111100
00011000
>
00001000
00000000
10010000
00101000
01000100
01000100
00111100
00011000
WAL false

TIL p
00000000
11111111
00000000
11111111
00000000
11111111
00000000
11111111

TIL q
10100101
10011001
10000001
11000011
10100101
01011010
00100100
00011000

TIL r
11111111
10000001
01111110
10000001
10000001
10000001
10011001
10100101

TIL s
01001111
01001111
01001111
01001111
01001111
01101111
10011111
11111111

TIL t
01001111
01001111
01001111
01001111
01001111
01001111
01001111
01001111

TIL u
00011111
00100000
01000000
01000111
01001111
01001111
01001111
01001111

TIL v
11111000
00000100
00000010
11100010
11110010
11110010
11110010
11110010

TIL w
11110010
11110010
11110010
11110010
11110010
11110010
11110010
11110010

TIL x
11110010
11110010
11110010
11110010
11110010
11110110
11111001
11111111

SPR A
00101010
00111110
01110010
01100010
01011100
00100100
01000100
11111110
>
00000000
00101010
00111110
01110010
01100010
01011100
00100100
11111110
POS 0 8,3

SPR b
00111000
01110100
01100100
11000100
11111000
10111000
01111100
11111100
>
00111000
01011100
01001100
01000110
00111110
00111010
01111100
01111110
NAME maid_1
DLG SPR_1
POS 0 12,7

SPR c
00111000
01111100
01110100
11100100
11111000
11101000
01000100
11111100
>
00111000
01111100
01011100
01001110
00111110
00101110
01000100
01111110
NAME maid2
DLG SPR_2
POS 0 3,10

SPR d
00111001
01000101
01010101
01010101
11111111
11000101
01111101
01101101
NAME Guard1
DLG SPR_0
POS 1 8,5

SPR e
00111001
01000101
01111101
01000101
11111111
11000101
01111101
01101101
NAME Guard2
DLG SPR_3
POS 1 12,5

SPR f
11111001
01000101
01000101
01000101
11111111
11000101
01111101
01101101
NAME Guard3
DLG SPR_4
POS 2 7,11

SPR g
00111111
01111101
01111101
01111101
01111111
01111101
01111101
01101101
NAME Guard4
DLG SPR_5
POS 2 8,11

SPR h
00111000
01111100
01110100
11100100
11111000
11111000
01111100
11111100
>
00111000
01111100
01011100
01001110
00111110
00111110
01111100
01111110
NAME Maid3
DLG SPR_6
POS 2 6,6

SPR i
00010000
00101100
00100010
00010100
00100010
00100100
11011000
00000000
>
00010000
00101100
00100010
00010100
00100010
10100100
01011000
00000000
NAME mouse1
DLG SPR_7
POS 3 6,8

SPR j
00010000
00101100
00100010
00010100
00100010
00100100
11011000
00000000
>
00010000
00101100
00100010
00010100
00100010
10100100
01011000
00000000
NAME Mouse2
DLG SPR_8
POS 4 7,5

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG ITM_0
You found a nice warm cup of tea

DLG SPR_1
"""
{clr3}Maid{clr3}: Your highness... You should not be wasting your time on me... I am but a humble servant of the castle.
But I thank you for spending some time with me. It's an honor to serve you.
"""

DLG SPR_2
{clr3}Maid{clr3}: Milady, please don't step here, for I am cleaning the floor. You may accidentally slip and hurt yourself!

DLG SPR_0
{clr3}Guard{clr3}: Good Evening, your highness. We will make sure that you are perfectly safe in your castle.

DLG SPR_3
"""
{clr3}Guard{clr3}: The plebians may be afraid of the bandits in the nearby woods, but you have nothing to fear, Princess.
Many tried to bypass our walls, but nobody ever succeeded.
"""

DLG SPR_4
{clr3}Guard{clr3}: I'm sorry your highness, but the King will not allow your entrance in the dungeons. It's too dangerous for a princess such as yourself.

DLG SPR_5
{clr3}Black Guard{clr3}: ...You shall not pass. I'm sorry your highness, but it's for your own good.

DLG SPR_6
"""
{clr3}Maid{clr3}: ...My name? Oh... I'm new here... I'm lucky the Queen allowed me to stay and work here. When I saw you, princess, at the festivals... I always dreamed of working on the castle.
You're an inspiration for me...
"""

DLG SPR_7
"""
{clr3}Mouse{clr3}:Towards the north lies the throne room. Are you brave enough to defy your father? Will you fight for your freedom, even if it means sacrificing your responsibilities as a princess, and your duty with your people?
It this the path you wish?
"""

DLG SPR_8
"""
{clr3}Mouse{clr3}: Towards the east lies the castle balcony. Are you ready to talk to your mother? Will you uphold to your duty as a princess, even if it means sacrificing your own freedom? Will you dedicate your life for the sake of your people and your kingdom, at the cost of your own happiness?
Is this the path you wish?
"""

END 0
"The Princess fought her father and became free. She fled the castle, taking with her the hope for the kingdom. But she was finally happy, for she decided to follow her heart, and seek her dreams."

END 1
"And the Princess decided to sacrifice her happiness for the sake of her people. She crushed her dreams, to allow the dreams of her people."

VAR a
42

