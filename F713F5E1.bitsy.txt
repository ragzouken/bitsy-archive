
Lesson

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
NAME main
85,98,204
128,159,255
255,255,255

PAL 1
NAME secret
214,142,255
255,255,255
255,255,255

ROOM 0
c,0,c,b,0,0,b,0,c,0,b,b,b,b,c,0
0,0,0,0,c,c,0,0,0,0,b,b,b,0,c,c
b,0,b,b,b,b,0,c,0,c,b,0,0,b,0,c
b,b,b,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,a,d,d,d,d,d,d,d,d,d,d,d,e
0,b,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,b,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,b,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,0,c,a,d,d,d,d,d,d,d,d,d,d,d,e
b,0,c,a,d,d,d,d,d,d,d,d,d,d,d,f
c,0,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,b,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,0,b,a,d,d,d,d,d,d,d,d,d,d,d,e
0,0,b,a,d,d,d,d,d,d,d,d,d,d,d,e
b,c,b,a,a,a,a,a,a,a,a,a,a,a,a,a
b,0,0,0,b,b,b,b,c,c,c,b,b,b,b,b
NAME start room
EXT 15,9 1 0,9
PAL 0

ROOM 1
c,0,c,0,0,b,0,0,c,0,0,b,0,c,0,b
0,b,b,0,b,b,b,b,b,b,b,0,0,0,b,0
0,b,0,c,0,0,0,b,0,b,0,0,b,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,f
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
g,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
h,d,d,d,i,d,d,i,d,d,i,d,d,i,d,e
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
h,d,d,d,i,d,d,i,d,d,i,d,d,i,d,e
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,e
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
c,b,c,b,b,b,b,0,0,c,c,b,0,c,b,b
NAME room 1
EXT 15,6 2 0,6
PAL 0

ROOM 2
b,0,c,c,b,0,a,d,d,d,d,i,d,i,d,i
b,b,0,0,c,b,a,d,d,d,d,d,d,d,d,d
0,c,b,0,b,b,a,d,d,d,d,i,d,i,d,i
a,a,a,a,a,a,a,d,d,d,d,d,d,d,d,d
h,d,d,d,d,d,d,d,d,d,d,i,d,i,d,i
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
g,d,d,d,d,d,d,d,d,d,d,i,d,i,d,i
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
h,d,d,d,d,d,d,d,d,d,d,i,d,i,d,i
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
h,d,d,d,d,d,d,d,d,d,d,i,d,i,d,i
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
h,d,d,d,d,d,d,d,d,d,d,i,d,i,d,i
h,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
c,b,b,b,0,c,0,c,c,b,b,0,b,b,0,c
NAME class west
EXT 0,6 1 15,6
EXT 15,1 3 0,1
EXT 15,3 3 0,3
EXT 15,5 3 0,5
EXT 15,7 3 0,7
EXT 15,9 3 0,9
EXT 15,11 3 0,11
EXT 15,13 3 0,13
EXT 7,0 4 7,15
EXT 8,0 4 8,15
EXT 9,0 4 9,15
EXT 10,0 4 10,15
EXT 12,0 4 12,15
EXT 14,0 4 14,15
PAL 0

ROOM 3
d,i,d,i,d,i,d,d,d,d,a,b,b,b,0,b
d,d,d,d,d,d,d,d,d,d,a,0,c,c,c,0
d,i,d,i,d,i,d,d,d,d,a,c,b,b,b,c
d,d,d,d,d,d,d,d,d,d,a,c,b,c,b,c
d,i,d,i,d,i,d,d,d,d,a,c,b,b,b,c
d,d,d,d,d,d,d,d,d,d,a,0,c,c,c,0
d,i,d,i,d,i,d,d,d,d,a,0,0,0,b,b
d,d,d,d,d,d,d,d,d,d,a,0,b,0,b,0
d,i,d,i,d,i,d,d,d,d,a,b,0,c,0,0
d,d,d,d,d,d,d,d,d,d,a,b,0,c,0,c
d,i,d,i,d,i,d,d,d,d,a,0,b,0,0,0
d,d,d,d,d,d,d,d,d,d,a,c,b,b,0,c
d,i,d,i,d,i,d,d,d,d,a,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,a,0,c,0,0,c
a,a,a,a,a,a,a,a,a,a,a,0,0,c,0,b
b,b,c,b,c,c,0,b,b,b,b,c,0,b,0,0
NAME class south
EXT 0,1 2 15,1
EXT 0,3 2 15,3
EXT 0,5 2 15,5
EXT 0,9 2 15,9
EXT 0,7 2 15,7
EXT 0,11 2 15,11
EXT 0,13 2 15,13
EXT 0,0 5 0,15
EXT 2,0 5 2,15
EXT 4,0 5 4,15
EXT 6,0 5 6,15
EXT 7,0 5 7,15
EXT 8,0 5 8,15
EXT 9,0 5 9,15
PAL 0

ROOM 4
c,0,0,b,b,0,a,j,j,j,j,k,j,j,j,j
0,b,b,0,0,0,a,d,d,d,d,d,d,d,d,d
b,0,c,c,0,c,a,d,d,d,d,d,d,d,d,d
0,0,0,0,b,0,a,d,d,d,d,d,d,d,d,d
0,b,0,0,b,b,a,d,d,d,d,d,d,d,d,d
0,b,b,0,0,b,a,d,d,d,d,d,d,d,d,d
c,0,b,0,c,0,a,d,d,d,d,d,d,d,d,d
0,0,0,c,c,0,a,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,a,d,d,d,d,d,d,d,d,d
0,c,0,0,b,0,a,d,d,d,d,d,d,d,d,d
b,0,0,0,b,0,a,d,d,d,d,d,d,d,d,d
0,b,0,0,0,0,a,d,d,d,d,i,d,i,d,i
0,0,0,0,c,b,a,d,d,d,d,d,d,d,d,d
c,b,b,b,0,b,a,d,d,d,d,i,d,i,d,i
0,b,0,c,0,0,a,d,d,d,d,d,d,d,d,d
b,b,b,0,0,b,a,d,d,d,d,i,d,i,d,i
NAME class north
EXT 7,15 2 7,0
EXT 8,15 2 8,0
EXT 9,15 2 9,0
EXT 10,15 2 10,0
EXT 12,15 2 12,0
EXT 14,15 2 14,0
EXT 15,14 5 0,14
EXT 15,12 5 0,12
EXT 15,10 5 0,10
EXT 15,9 5 0,9
EXT 15,8 5 0,8
EXT 15,7 5 0,7
EXT 15,6 5 0,6
EXT 15,5 5 0,5
EXT 15,4 5 0,4
EXT 15,3 5 0,3
EXT 15,2 5 0,2
EXT 15,1 5 0,1
EXT 11,0 6 11,15
PAL 0

ROOM 5
j,j,j,j,j,j,j,j,j,j,a,c,c,0,b,b
d,d,d,d,d,d,d,d,d,d,a,c,0,b,0,0
d,d,d,d,d,d,d,d,d,d,a,0,0,b,0,b
d,d,d,d,d,d,d,d,d,d,a,0,0,0,c,c
d,d,d,d,d,d,d,d,d,d,a,b,c,0,0,0
d,d,d,d,d,d,d,d,d,d,a,b,0,b,0,b
d,d,d,d,d,d,d,d,d,d,a,0,b,0,0,0
d,d,d,d,d,d,d,d,d,d,a,0,0,c,c,b
d,d,d,d,d,d,d,d,d,d,a,0,0,0,0,b
d,d,d,d,d,d,d,d,d,d,a,0,b,0,0,b
d,d,d,d,d,d,d,d,d,d,a,0,0,b,b,0
d,i,d,i,d,i,d,d,d,d,a,0,c,0,b,c
d,d,d,d,d,d,d,d,d,d,a,c,0,0,0,b
d,i,d,i,d,i,d,d,d,d,a,c,0,0,b,b
d,d,d,d,d,d,d,d,d,d,a,b,b,0,0,0
d,i,d,i,d,i,d,d,d,d,a,0,b,b,0,c
NAME class east
EXT 0,15 3 0,0
EXT 2,15 3 2,0
EXT 4,15 3 4,0
EXT 6,15 3 6,0
EXT 7,15 3 7,0
EXT 8,15 3 8,0
EXT 9,15 3 9,0
EXT 0,14 4 15,14
EXT 0,12 4 15,12
EXT 0,10 4 15,10
EXT 0,9 4 15,9
EXT 0,8 4 15,8
EXT 0,7 4 15,7
EXT 0,6 4 15,6
EXT 0,5 4 15,5
EXT 0,4 4 15,4
EXT 0,3 4 15,3
EXT 0,2 4 15,2
EXT 0,1 4 15,1
EXT 9,0 9 0,0
PAL 0

ROOM 6
c,0,b,0,0,0,0,0,0,0,c,0,0,0,0,0
0,0,0,0,c,0,0,b,0,0,0,0,b,b,0,0
b,0,0,0,0,b,b,0,0,c,0,0,0,0,b,0
0,b,c,0,0,0,0,0,c,0,0,b,0,b,0,0
0,0,0,0,0,0,0,0,c,0,0,b,b,0,0,0
0,0,0,0,0,b,0,0,0,0,0,0,0,0,0,c
c,0,b,0,0,0,a,a,a,a,a,a,a,a,a,a
0,0,b,b,0,0,a,d,d,d,d,d,d,d,d,d
0,0,b,b,0,0,a,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,a,d,d,d,d,d,d,d,d,d
0,b,0,0,0,0,a,d,d,d,d,d,d,d,d,d
0,0,0,b,0,b,a,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,a,d,d,d,d,d,d,d,d,d
0,c,0,b,0,0,a,d,d,d,d,d,d,d,d,d
0,c,0,b,0,0,a,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,a,m,m,m,m,l,m,m,m,m
EXT 11,15 4 11,0
EXT 15,12 7 0,12
EXT 15,11 7 0,11
EXT 15,10 7 0,10
EXT 15,9 7 0,9
EXT 15,8 7 0,8
PAL 0

ROOM 7
0,b,c,c,b,0,0,0,0,c,0,0,0,b,0,0
0,0,0,0,0,b,c,0,0,0,0,c,0,b,b,c
b,c,c,b,0,0,0,c,c,c,0,0,c,0,c,c
c,0,c,c,0,c,0,c,0,c,c,b,0,c,c,0
0,0,0,0,b,0,0,b,0,c,0,c,0,b,0,0
c,c,c,0,0,c,0,b,b,0,c,0,c,b,b,c
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
NAME hallway
EXT 0,12 6 15,12
EXT 0,11 6 15,11
EXT 0,10 6 15,10
EXT 0,9 6 15,9
EXT 0,8 6 15,8
EXT 15,8 8 0,8
EXT 15,9 8 0,9
EXT 15,10 8 0,10
EXT 15,11 8 0,11
EXT 15,12 8 0,12
EXT 15,13 8 0,13
EXT 15,14 8 0,14
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,c,0,0,0,c
b,0,0,0,c,0,0,0,0,b,0,c,0,0,b,0
b,0,0,0,0,c,0,0,0,b,0,0,0,0,0,0
0,b,0,b,b,0,0,c,0,0,0,0,0,b,0,0
0,0,0,c,0,0,0,0,c,0,0,c,0,b,b,c
0,0,c,0,0,0,0,0,0,0,0,0,0,0,b,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,a,c,0
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,c
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,c
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,b
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,a,0,b
m,m,m,m,m,m,m,m,m,m,m,m,m,a,0,0
NAME teacher's room
END 0 12,7
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,b,b,b,0,0,0,0,0,0,0,0,c
b,b,0,0,0,0,0,0,0,c,c,c,c,0,0,0
0,b,b,c,0,b,b,0,c,0,0,b,b,b,b,0
0,0,0,c,0,0,b,0,0,0,0,0,0,0,0,0
0,0,0,c,0,0,0,0,0,c,c,0,0,b,b,b
c,0,0,0,0,c,0,0,0,0,0,0,0,b,0,0
0,0,0,0,c,0,0,0,0,0,0,c,0,0,0,0
0,c,0,b,0,0,b,b,b,0,0,c,0,b,0,0
0,0,0,b,0,b,b,0,0,0,0,c,b,0,c,0
0,0,0,0,b,0,0,0,0,b,0,0,0,0,0,0
0,0,c,0,b,0,c,c,0,b,0,c,0,0,b,0
0,0,0,0,0,b,b,b,b,0,0,0,0,b,b,0
0,b,0,c,0,0,0,0,0,0,c,0,0,b,c,0
0,b,0,c,c,0,0,0,0,b,b,b,0,0,0,0
0,b,b,b,0,0,c,c,0,0,0,b,b,b,0,0
NAME secret room
ITM 0 8,6
ITM 0 14,7
ITM 0 0,11
ITM 0 15,15
EXT 0,5 5 9,4
PAL 1

TIL a
11111111
00100010
11111111
01001000
11111111
00100100
11111111
01010010
NAME brick
WAL true

TIL b
00000000
00000000
01101100
00010010
01001010
00010000
01011010
00010000
>
00000000
00000000
00101000
01011100
00010000
01001010
00010000
00011000
NAME grass

TIL c
00111100
01100110
01000010
01100110
00111100
00001000
00011000
00001000
>
00011110
00110011
00100001
00110011
00011110
00001000
00011000
00001000
NAME flower

TIL d
10000000
10000000
10000000
10000000
10000000
10000000
10000000
11111111
NAME floor
WAL false

TIL e
10000011
10000011
10000011
10000011
10000011
10000011
10000011
11111111
NAME wall right
WAL true

TIL f
10000011
10000111
10000111
10001111
10000111
10000111
10000011
11111111
NAME door right

TIL g
11000000
11100000
11100000
11110000
11100000
11100000
11000000
11111111
NAME door left

TIL h
11000000
11000000
11000000
11000000
11000000
11000000
11000000
11111111
NAME wall left

TIL i
11111111
11111111
11111111
11111111
11111111
10100101
10100101
10000100
NAME desk
WAL true

TIL j
11111111
11111111
10000000
10000000
10000000
10000000
10000000
11111111
NAME wall top

TIL k
11111111
11111111
10111110
10001000
10000000
10000000
10000000
11111111
NAME door top

TIL l
10000000
10000000
10000000
10000000
10001000
10111110
11111111
11111111
NAME door bottom

TIL m
10000000
10000000
10000000
10000000
10000000
10000000
11111111
11111111
NAME wall bottom

SPR A
00111100
00111100
00011000
00111100
01111110
10111101
00100100
00000100
>
00111100
00111100
00011000
00111100
01111110
10111101
00100100
00100000
POS 0 5,5

SPR a
00111100
00111100
00011000
00111100
01011010
10111101
01111110
00011000
NAME girl
DLG SPR_0
POS 0 11,4

SPR b
00111100
00111100
00011000
00111100
01111110
10111101
00100100
00100100
NAME boy
DLG SPR_1
POS 0 4,11

SPR c
00000000
00111100
00111100
00011000
00111100
01111110
10111101
00100100
>
00111100
00111100
00011000
00111100
01111110
10111101
00111100
00100100
NAME anxious person
DLG SPR_2
POS 1 8,5

SPR d
00111100
00110010
01110010
01111100
10010000
00011100
00011000
00011000
NAME pretty girl
DLG SPR_3
POS 2 4,11

SPR e
00111100
01100110
01100110
10011001
10111101
01011010
10111101
00001000
>
00111100
01100110
01100110
10011001
10111101
01011010
10111101
00010000
NAME girl 2
DLG SPR_4
POS 6 9,11

SPR f
00000000
00000000
00000000
00000111
00111101
00111101
00110111
11110100
NAME boy down
DLG SPR_5
POS 6 14,14

SPR g
00111100
00111100
00011000
00111100
01011010
10111101
01111110
00011000
NAME girl 3
DLG SPR_6
POS 7 4,9

SPR h
00000000
01010000
01110001
11110001
11111110
00111100
00111100
00100100
NAME dog
DLG SPR_7
POS 7 10,13

SPR i
00000000
10100001
11100001
11100001
11100010
00111100
00111100
00100100
NAME cat
DLG SPR_8
POS 8 11,8

SPR j
00111100
00111100
00011000
00111100
01011010
10011001
00011000
00011000
NAME child
DLG SPR_9
POS 3 8,8

SPR k
00000000
00010000
00011100
00010000
00111000
01111100
01111100
00111000
NAME apple
DLG SPR_a
POS 5 7,1

SPR l
00011100
00011100
00001000
00011100
00101010
01011101
00111110
00001000
NAME secret girl
DLG SPR_b
POS 9 8,1

SPR m
00011100
00011100
00001000
00011100
00111110
01011101
00011100
00010100
NAME secret boy
DLG SPR_c
POS 9 1,5

SPR n
00111100
00111100
00111100
00011000
00011000
00111100
01011010
00011000
>
00111100
00111100
00111100
01011000
00111000
00011100
00011010
00011000
NAME waving child
DLG SPR_d
POS 2 11,7

SPR o
00111100
00111100
00011000
00111100
01111110
10111101
00111100
00100100
NAME human
DLG SPR_e
POS 8 10,12

SPR p
00011100
00011100
00001000
00011100
00101010
01011101
00111110
00001000
NAME bitsy girl
DLG SPR_f
POS 3 9,2

SPR q
00011000
00100100
00100100
00011000
00100100
01100110
10111101
00100100
NAME sad child
DLG SPR_g
POS 6 12,7

SPR r
00111000
00111000
00010000
00111000
01010100
10111010
00101000
00101000
NAME hungry child
DLG SPR_h
POS 5 5,6

SPR s
00111000
00111000
00010000
00111000
01010100
00111000
00111000
00010000
NAME little girl
DLG SPR_i
POS 4 8,5

SPR t
00111000
00111000
00010000
00111000
01010100
10010010
00101000
01000100
NAME stick child
DLG SPR_j
POS 1 4,11

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG SPR_0
Hi there! You must be the new person! The Teacher likes to meet all new people. You should find the Teacher and learn what to do. The room with the cat is the room with the Teacher.

DLG ITM_0
You found a nice warm cup of tea

DLG SPR_1
Hi, my name is Dave. I like the color purple. What is your favorite color?

DLG SPR_2
Oh, gosh! A new day. New people. New names. How will I remember names? Will anyone remember me? Oh, gosh, my heart is racing!

DLG SPR_3
I tied my hair back today. What do you think? Pretty, huh?

DLG SPR_4
"""
I feel like I need to keep moving...
but I don't know where or why.
"""

DLG SPR_5
"""
I fell down....But I don't want to get back up.
Is that wrong?
"""

DLG SPR_6
"""
La la la la la...
Oh, hi!
I love to sing. Do you?
"""

DLG SPR_7
"""
When you see the Teacher, tell them "What team? Wildcats!"
Ha ha, I never get tired of seeing their reaction!
"""

DLG SPR_9
"""
Hi there. Are you new? Let's chat any time you want!
{shuffle
  - My name is Larry.
  - My favorite color is green. Doesn't matter what kind, emerald, leaf, grassy...probably not puce, though.
  - I have a cousin named Dave. He goes here, too. Have you met him yet?
  - Have you studied marine biology? It seems fishy to me.
  - How do you like creative writing? I think it's all write.
  - Do you like math? You can count me out.
  - There is a secret room. The entrance is near an apple.
}
"""

DLG SPR_a
I'm an apple!

DLG SPR_b
Collect all the tea!

DLG SPR_c
The way back to school is over here.

DLG SPR_d
I know class hasn't started yet, but it doesn't hurt to come early. Besides, I like learning! So many things to discover!

DLG SPR_8
"""
Hello, I am the Teacher.

You were probably expecting a human to be the Teacher, right? Well, humans aren't the only ones you can learn from.
Anyway, now that you are here, I have some questions for you.
What is Dave's favorite color? Which room had a circle of flowers outside? What color is the secret room? Did you talk to anyone? How did you help them?

In life, you will meet many people and encounter new places. While it is important to finish the game or objective, it's also important to learn to slow down. You don't have to speed run every game you come across. And you don't have to rush through life. Make sure to explore every corner of the environment you are in next time. Who knows? You might find something new. And isn't that always nice?

...What's that? You have something to tell me? "What team? Wild..." Oh, the dog told you to say that to me. Haha, like I've never heard that before.
"""

DLG SPR_e
Welcome. Talk to the cat, please. Then, stand in the corner.

DLG SPR_f
How do you like Bitsy games? This is my first one. The idea bounced around in my head for a month before I made it.

DLG SPR_g
I feel invisible. Does anyone even care if I'm here? ...Oh, hello. I've never seen you before.

DLG SPR_h
"""
{
  - {item "tea"} >= 1 ?
    Oh, thank you! This will tide me over until lunch!
  - {item "tea"} == 0 ?
    I'm so hungry. Do you have any tea? Or even...oooh, look, an apple!
}
"""

DLG SPR_i
"""
I love magenta. Do you know where I can find magenta in the school?
What is your favorite color? Favorite class?
"""

DLG SPR_j
You can't go back in the other room. Whatever you didn't do in there, it's too late. You have to keep going.

END 0
What did you learn today?

VAR a
42


