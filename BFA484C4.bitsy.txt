
losirams letter

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
0,82,204
128,159,255
255,255,255

ROOM 0
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,a,a,a,a,a,a,a,a,a,a,a,a,a,a,e
e,a,0,0,0,0,0,0,0,0,0,0,0,0,a,e
e,a,0,0,0,0,d,0,0,0,d,0,0,0,a,e
e,a,0,0,c,d,0,0,0,0,0,d,0,0,a,e
e,a,0,0,0,0,0,0,0,0,0,0,0,0,a,e
e,a,0,0,0,0,c,0,0,0,0,0,0,0,0,0
e,a,d,0,0,0,0,0,0,0,0,0,c,0,0,0
e,a,0,0,0,0,0,0,0,0,0,0,0,0,0,0
e,a,0,0,c,0,0,0,0,0,0,0,0,0,a,e
e,a,0,0,0,0,0,0,0,0,0,0,0,0,a,e
e,a,0,0,0,0,0,0,d,0,c,0,0,0,a,e
e,a,0,c,0,0,0,0,0,0,0,0,0,0,a,e
e,a,0,0,0,d,0,0,0,0,0,0,0,0,a,e
e,a,a,a,a,a,a,a,a,a,a,a,a,a,a,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
NAME beginning
ITM 0 8,4
EXT 15,6 1 0,6
EXT 15,7 1 0,7
EXT 15,8 1 0,8
PAL 0

ROOM 1
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,a,a,a,a,a,a,a,a,a,a,e,e,e
e,e,a,0,0,c,0,0,0,0,0,0,0,a,e,e
e,a,0,0,d,d,d,d,d,c,d,d,0,0,a,e
e,a,0,d,d,d,d,d,d,d,d,c,0,0,0,a
e,a,0,d,d,d,c,c,d,c,d,d,d,0,0,a
0,0,0,d,d,d,d,d,d,d,d,d,c,d,0,a
0,0,0,0,d,d,d,d,d,d,d,c,c,0,d,0
0,0,0,0,d,d,d,d,d,d,c,d,d,d,d,0
e,a,0,d,d,c,c,d,d,d,d,d,0,d,0,a
e,a,0,d,d,c,c,d,d,d,d,d,d,d,0,a
e,a,0,d,c,c,c,d,d,d,d,d,d,0,0,a
e,e,a,0,0,c,0,d,d,d,d,d,0,0,a,e
e,e,e,a,0,0,0,0,0,0,0,0,0,a,e,e
e,e,e,e,a,a,a,a,a,a,a,a,a,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
NAME pg 2
ITM 1 11,7
EXT 15,7 2 0,6
EXT 15,8 2 0,8
PAL 0

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,e,e,e,e,e,e,e,e,e,e,e,e,e,e,a
a,e,e,e,e,e,a,e,a,e,e,e,e,e,e,a
a,e,e,e,e,e,a,a,a,e,e,e,e,e,e,a
a,e,e,e,e,e,e,a,e,e,e,e,e,e,e,a
a,e,e,e,e,e,e,e,e,e,e,e,e,e,e,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,e,e,e,e,e,e,e,e,e,e,e,e,e,e,a
a,e,e,e,e,e,e,e,e,e,e,e,e,e,e,a
a,e,e,e,e,e,a,e,a,e,e,e,e,e,e,a
a,e,e,e,e,e,a,a,a,e,e,e,e,e,e,a
a,e,e,e,e,e,e,a,e,e,e,e,e,e,e,a
a,e,e,e,e,e,e,e,e,e,e,e,e,e,e,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME pg3
ITM 2 10,7
EXT 15,6 3 0,7
EXT 15,7 3 0,8
EXT 15,8 3 0,9
PAL 0

ROOM 3
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,a,a,a,a,a,e,e,a,a,a,a,a,e,e
e,a,a,0,0,0,a,a,a,a,0,0,0,a,a,e
e,a,0,0,0,0,0,a,a,0,0,0,0,0,a,e
a,0,0,d,d,0,0,a,0,0,0,d,d,d,a,a
a,0,0,d,d,d,0,i,0,0,d,d,d,d,0,a
0,0,0,d,d,d,d,j,d,d,d,d,d,d,0,a
0,0,0,0,0,0,0,0,0,d,d,d,d,d,0,a
0,0,0,0,0,0,0,0,0,d,d,d,d,0,0,a
a,a,0,0,0,d,d,d,d,d,d,d,0,0,a,a
e,a,a,0,0,d,d,d,d,d,0,0,0,0,a,e
e,e,a,a,0,0,0,d,d,0,0,0,0,a,e,e
e,e,e,e,a,a,0,0,0,0,0,a,a,e,e,e
e,e,e,e,e,a,a,0,0,a,a,a,e,e,e,e
e,e,e,e,e,e,e,a,a,a,e,e,e,e,e,e
NAME pg4
EXT 7,6 4 7,13
PAL 0

ROOM 4
f,f,f,f,f,f,j,j,j,f,f,f,f,h,f,h
f,f,f,f,f,f,0,0,0,f,g,f,f,f,f,h
h,f,f,f,h,0,g,0,f,f,f,f,f,0,f,f
0,f,f,g,f,f,f,f,f,f,f,h,0,f,f,0
h,f,h,0,f,f,f,0,0,f,f,f,f,f,0,0
0,f,f,f,0,h,f,0,f,0,0,0,0,h,f,f
0,0,0,f,0,0,0,0,0,g,f,0,0,0,0,0
f,0,g,0,h,0,0,f,0,0,0,h,0,0,0,0
0,0,0,f,0,0,0,0,0,0,0,g,0,f,g,f
0,0,h,0,0,0,0,0,h,f,0,h,0,0,0,0
0,0,0,0,0,f,g,0,0,g,0,0,f,0,0,0
0,f,g,0,f,0,0,f,0,f,0,0,0,0,0,0
0,0,0,0,0,0,0,f,0,f,f,0,0,0,h,0
0,0,0,g,0,0,0,0,0,0,0,0,f,0,0,0
0,f,0,0,0,f,0,0,0,0,g,0,f,f,h,0
0,0,0,h,0,0,0,h,0,0,0,0,0,0,g,0
NAME letter
EXT 6,0 5 0,7
EXT 7,0 5 0,8
EXT 8,0 5 0,9
PAL 0

ROOM 5
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,g,g,g,f,f,f,f,f,f,f
f,f,f,f,g,g,0,0,0,g,f,f,f,f,f,g
f,f,g,g,0,0,0,0,0,0,g,g,0,f,g,g
g,g,g,0,0,0,0,0,0,0,0,g,g,g,g,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,g,0,0,0,0,0,0,g
g,0,0,0,0,g,g,g,g,g,0,0,0,g,g,f
g,g,0,0,g,g,f,f,f,g,g,g,g,g,f,f
f,g,g,g,g,f,f,f,f,f,f,f,f,f,f,f
f,f,g,g,f,f,f,f,f,f,f,f,f,f,f,0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME ending
END 0 14,7
END 0 15,8
END 0 15,6
PAL 0

TIL a
01111110
11111111
10011001
10000001
10000001
11000011
11100111
01111110
NAME tiles

TIL c
00000000
00000000
01010000
00100000
00100000
00100101
00000010
00000010
NAME sprout

TIL d
00000000
00000000
00100000
01010000
00100000
00000100
00001010
00000100
NAME flowers

TIL e
00000000
00000000
01010101
10101010
00000000
01010101
10101010
00000000
>
00000000
00000000
10101010
01010101
00000000
10101010
01010101
00000000

TIL f
00000010
00000000
01000100
00000000
00000000
00100000
00000100
00000000
>
00000100
00000000
00100010
00000000
00000000
00000100
00100000
00000000

TIL g
00000000
00000000
00010000
00111000
00010000
00000010
00000101
00000010
>
00000000
00010000
00010000
01101100
00010000
00010000
00000000
00000000

TIL h
00000000
00010000
00010000
11111110
00111000
00101000
01000100
00000000
>
00000000
00000000
00010000
01111100
00111000
00101000
00000000
00000000

TIL i
00000000
00000000
00111100
01100110
01111110
01111110
01111110
01100110

TIL j
00000000
00000000
00010000
00111000
01111100
00010000
00010000
00010000
>
00000000
00010000
00111000
01111100
00010000
00010000
00010000
00010000

SPR A
00000000
00011000
00111100
01101010
01110110
01111110
01010101
00000000
>
00000000
00011000
00111100
01010110
01101110
01111110
10101010
00000000
POS 0 7,5

SPR a
00000000
00011000
00111100
01011010
11100111
11100111
00011000
00011000
>
00000000
00011000
00111100
01011010
11111111
11100111
00011000
00011000
NAME mushyroom
DLG SPR_0
POS 0 10,9

SPR b
00000000
00000000
00100100
01011010
00011000
00100100
01000010
00000000
>
00000000
01000010
00100100
00011000
00011000
00100100
00011000
00000000
NAME lil bitty
DLG SPR_1
POS 0 5,11

SPR d
00000000
00011000
00111100
01011010
11100111
11100111
00011000
00011000
>
00000000
00011000
00111100
01011010
11111111
11100111
00011000
00011000
DLG SPR_3
POS 1 7,5

SPR e
00000000
00000000
00100100
01011010
00011000
00100100
01000010
00000000
>
00000000
01000010
00100100
00011000
00011000
00100100
00011000
00000000
DLG SPR_2
POS 1 7,9

SPR f
00000000
00011000
00111100
01011010
11100111
11100111
00011000
00011000
>
00000000
00011000
00111100
01011010
11111111
11100111
00011000
00011000
DLG SPR_5
POS 2 12,6

SPR g
00000000
00000000
00100100
01011010
00011000
00100100
01000010
00000000
>
00000000
01000010
00100100
00011000
00011000
00100100
00011000
00000000
DLG SPR_4
POS 2 12,8

SPR h
00000000
00011000
00111100
01011010
11100111
11100111
00011000
00011000
>
00000000
00011000
00111100
01011010
11111111
11100111
00011000
00011000
DLG SPR_6
POS 3 6,7

SPR i
00000000
00000000
00100100
01011010
00011000
00100100
01000010
00000000
>
00000000
01000010
00100100
00011000
00011000
00100100
00011000
00000000
DLG SPR_7
POS 3 8,7

SPR j
00000000
01111000
01111100
01010110
01101010
01111110
01010110
01101010
DLG SPR_8
POS 4 7,3

SPR k
00000000
00011100
00111110
00110100
00111110
00111110
00101010
00000000
>
00000000
00011100
00111110
00110100
00111110
00111110
01010100
00000000
POS 5 15,7

ITM 0
00000000
01111000
01111100
01010110
01101010
01111110
01010110
01101010
NAME letter
DLG ITM_0

ITM 1
00000000
00000000
01011101
01101011
01110111
01111111
01111111
00000000
NAME envelope
DLG ITM_1

ITM 2
00000000
00101010
01111100
00110110
01101100
00111110
01010100
00000000
NAME stamp
DLG ITM_2

DLG SPR_0
"""
mushy: hi losiram are u gonna mail your letter

{clr2}losiram: yes but i cant find my stamp or envelope!{clr2}

mushy: oh no! that letter is important we will look for it
"""

DLG ITM_0
a love letter you wrote for your dearest

DLG SPR_1
"""
lil bitty: did you finish your love letter?

{clr2}losiram: yes, but there are no stamps or envelopes!{clr2}

lil bitty: oh crap! we gotta find some!
"""

DLG ITM_1
an envelope!

DLG SPR_2
lil bitty: oh! an envelope! it smells like vanilla a little

DLG SPR_3
mushy: oh! look! remember to leave a kiss mark on it!

DLG ITM_2
yes! a stamp!

DLG SPR_4
lil bitty: we found it! now we just need to get to the mailbox!

DLG SPR_5
mushy: that was easy! your letter should be good to send now!

DLG SPR_6
mushy: its time losiram

DLG SPR_7
lil bitty: are you ready?

DLG SPR_8
"""
dearest,
i know you will never read this. i've come to accept the fact my words can't reach you from this place. i'm thinking of ways to tell you how much you really mean to  me. there's so much i have to apologize for. everything i should of said or done. you've shaped who i am today, and you'll always be apart of me. thank you for the briefest moments of joy, in this life that is always much too short. i've never loved anyone the way i love you. i miss you everyday, and wherever you are in the world i hope you are always seeking your happiness.
love, losiram
"""

END 0
the end

END undefined
{clr2}dearest,

END undefinee


VAR a
42


