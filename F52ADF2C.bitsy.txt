
Reach The End

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME palette 2
38,38,38
255,255,255
255,255,255

PAL 1
255,110,3
20,20,20
0,0,0

PAL 2
NAME palette 3
38,38,38
0,0,0
255,255,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 1
EXT 15,6 1 0,6
EXT 15,7 1 0,7
EXT 15,8 1 0,8
EXT 15,9 1 0,9
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 2
EXT 15,6 2 0,6
EXT 15,7 2 0,7
EXT 15,8 2 0,8
EXT 15,9 2 0,9
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 3
EXT 15,6 3 0,6
EXT 15,7 3 0,7
EXT 15,8 3 0,8
EXT 15,9 3 0,9
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 4
EXT 15,6 4 0,6
EXT 15,7 4 0,7
EXT 15,8 4 0,8
EXT 15,9 4 0,9
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 5
EXT 15,6 5 0,6
EXT 15,7 5 0,7
EXT 15,8 5 0,8
EXT 15,9 5 0,9
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 6
EXT 15,6 6 0,6
EXT 15,7 6 0,7
EXT 15,8 6 0,8
EXT 15,9 6 0,9
PAL 0

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 7
EXT 15,6 7 0,6
EXT 15,7 7 0,7
EXT 15,8 7 0,8
EXT 15,9 7 0,9
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 8
EXT 15,6 8 0,6
EXT 15,7 8 0,7
EXT 15,8 8 0,8
EXT 15,9 8 0,9
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 9
ITM 0 9,8
EXT 15,6 9 0,6
EXT 15,7 9 0,7
EXT 15,8 9 0,8
EXT 15,9 9 0,9
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 10
EXT 15,7 b 0,7
EXT 15,8 b 0,8
PAL 0

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
0,0,0,p,0,0,0,0,q,r,0,0,0,0,h,d
0,0,q,r,0,0,0,0,0,0,14,0,0,e,d,d
0,0,0,0,0,0,0,0,0,0,0,0,e,d,d,d
0,0,0,0,0,0,q,r,0,0,0,h,d,d,d,d
0,0,q,r,0,0,0,0,p,0,f,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,g,d,d,d,d,d
0,0,0,14,0,0,0,0,0,e,d,d,d,d,d,d
0,0,0,0,0,0,0,0,i,d,d,d,d,d,d,d
0,0,0,0,0,0,0,0,j,d,d,d,d,d,d,d
0,0,12,13,0,0,0,0,0,k,d,d,d,d,d,d
0,11,15,d,y,0,0,0,0,0,l,d,d,d,d,d
0,10,w,x,z,0,0,0,0,0,0,m,n,d,d,d
o,o,v,u,o,o,o,o,o,o,o,o,o,o,o,o
0,0,s,t,0,0,0,0,0,0,0,0,0,0,0,0
19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19
NAME Prologue
EXT 15,13 0 0,7
EXT 15,14 0 0,8
PAL 1

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 11
EXT 15,7 c 0,7
EXT 15,8 c 0,8
PAL 0

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 12
EXT 15,7 d 0,7
EXT 15,8 d 0,8
PAL 0

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 13
EXT 15,7 e 0,7
EXT 15,8 e 0,8
PAL 0

ROOM e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,b,0,b,0,b,0,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 14
EXT 15,7 f 0,7
EXT 15,8 f 0,8
PAL 0

ROOM f
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,16,o,o,o,o,o,o
0,b,0,b,0,b,0,b,0,18,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,18,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,18,0,0,0,0,0,0
0,b,0,b,0,b,0,b,0,18,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,17,o,o,o,o,o,o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 15 (fake end)
EXT 15,6 g 0,6
EXT 15,7 g 0,7
EXT 15,8 g 0,8
EXT 15,9 g 0,9
PAL 0

ROOM g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,19,19,0,0,0,0,0,0,0
0,0,0,0,0,0,0,19,19,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room 16 (true end)
END undefineh 6,7
END undefineh 6,8
END undefineh 7,9
END undefineh 8,9
END undefineh 9,8
END undefineh 9,7
END undefineh 8,6
END undefineh 7,6
PAL 2

TIL 10
00011111
00011111
00001111
00000111
00000001
00000000
00000000
00000000
>
00111111
00111111
00011111
00001111
00000011
00000000
00000000
00000000

TIL 11
00000000
00000001
00000011
00000111
00001111
00011111
00111111
00111111
>
00000000
00000000
00000001
00000011
00000111
00001111
00011111
00011111

TIL 12
00000000
00000000
00000000
00000000
00000000
00001111
00111111
01111111
>
00000000
00000000
00000000
00000000
00000000
00000111
00011111
00111111

TIL 13
00000000
00000000
00000000
00000000
00000000
11110000
11111000
11111100
>
00000000
00000000
00000000
00000000
00000000
11111000
11111100
11111110

TIL 14
00000000
00000000
11000011
00100100
00011000
00000000
00000000
00000000
>
00000000
00000000
00000000
01100110
10011001
00000000
00000000
00000000

TIL 15
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
>
01111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL 16
00000000
11111110
11111110
11111110
11111110
11111110
11111110
01111110
WAL true

TIL 17
01111110
11111110
11111110
11111110
11111110
11111110
11111110
00000000
WAL true

TIL 18
01111110
01111110
01111110
01111110
01111110
01111110
01111110
01111110
WAL false

TIL 19
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL a
00000000
11111111
11111111
11111111
11111111
11111111
11111111
00000000
WAL true

TIL b
00000000
01111110
01111110
00111100
00011000
00011000
00011000
00111100
WAL true

TIL c
00000000
00000000
00000000
00000000
01000000
10000000
01001001
10010010

TIL d
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL false

TIL e
00000001
00000111
00001111
00011111
00111111
00111111
01111111
11111111

TIL f
00000001
00000011
00000011
00000111
00000111
00000111
00001111
00001111

TIL g
00001111
00001111
00011111
00011111
00111111
00111111
01111111
11111111

TIL h
00000001
00000011
00000011
00000011
00000111
00111111
11111111
11111111

TIL i
00000001
00000011
00000011
00000011
00000111
00000111
00000111
00000111

TIL j
00000111
00000111
00000111
00000111
00000011
00000001
00000001
00000001

TIL k
11111111
01111111
00111111
00001111
00000111
00000111
00000011
00000001

TIL l
11111111
11111111
01111111
00011111
00000111
00000011
00000001
00000001

TIL m
11111111
01111111
00011111
00000111
00000001
00000000
00000000
00000000

TIL n
11111111
11111111
11111111
11111111
11111111
00111111
00000111
00000001

TIL o
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL p
00000000
00000000
00000000
01100110
10011001
00000000
00000000
00000000
>
00000000
00000000
11000011
00100100
00011000
00000000
00000000
00000000

TIL q
00000000
00000011
00001110
00110000
01000000
01000000
00110000
00001111
>
00000000
00000111
00011100
01100000
10000000
10000000
01100000
00011111

TIL r
00000000
11110000
00011100
00000111
00000001
00000001
00000111
11111100
>
00000000
11100000
00111000
00001110
00000010
00000010
00001110
11111000

TIL s
00000111
00000111
00000111
00000111
00001111
00011111
00111111
01111111

TIL t
11100000
11100000
11100000
11110000
11110000
11111000
11111100
11111110

TIL u
11100000
11100000
11100000
11100000
11100000
11100000
11100000
11100000
WAL true

TIL v
00000111
00000111
00000111
00000111
00000111
00000111
00000111
00000111
WAL true

TIL w
11111111
11111111
11111111
11111111
11111111
01111111
00111111
00000111
>
11111111
11111111
11111111
11111111
11111111
00111111
00011111
00000111

TIL x
11111111
11111111
11111111
11111111
11111111
11111111
11111110
11100000
>
11111111
11111111
11111111
11111111
11111111
11111110
11111100
11100000

TIL y
00000000
11000000
11100000
11110000
11110000
11111000
11111000
11111000
>
00000000
10000000
11000000
11100000
11100000
11110000
11110000
11110000

TIL z
11110000
11100000
11100000
11000000
10000000
00000000
00000000
00000000
>
11111000
11110000
11110000
11100000
11000000
00000000
00000000
00000000

SPR A
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
POS a 0,14

SPR a
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_0
POS 0 8,8

SPR b
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_1
POS 1 6,9

SPR c
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_2
POS 2 4,6

SPR d
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_3
POS 2 9,8

SPR e
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_4
POS 3 5,7

SPR f
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_5
POS 3 13,8

SPR g
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_6
POS 4 2,9

SPR h
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_7
POS 4 8,6

SPR i
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_8
POS 4 11,8

SPR j
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_9
POS 5 7,7

SPR k
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_a
POS 6 4,7

SPR l
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_b
POS 6 11,8

SPR m
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_c
POS 7 6,8

SPR n
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_d
POS 7 12,6

SPR o
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_e
POS 8 5,8

SPR p
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_f
POS 9 5,7

SPR q
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_g
POS 9 9,8

SPR r
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_h
POS 9 12,9

SPR s
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_i
POS b 8,6

SPR t
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_j
POS c 3,8

SPR u
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_k
POS c 11,7

SPR v
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_l
POS d 4,6

SPR w
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_m
POS d 11,8

SPR x
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_n
POS e 10,8

SPR y
00000000
00111100
01111110
01011010
01111110
00100100
00011000
00000000
>
00000000
00111100
01111110
01011010
01111110
00111100
00011000
00000000
DLG SPR_o
POS f 4,8

ITM 0
00000000
00000000
00000111
11111101
10100111
00000000
00000000
00000000
NAME key
DLG ITM_0

DLG SPR_0
"""
Can you reach the end? 

Go on. Try.

You can do it.
"""

DLG ITM_0
"""
You found a Key.

It can be used to unlock things.

...But what 'things?'
"""

DLG SPR_1
"""
This isn't the end.

Keep on going.
"""

DLG SPR_2
"""
Oh hey.

Are you searching for the end?

Have fun.
"""

DLG SPR_3
"""
Hpmh. Trying to find the end?

...Good luck.
"""

DLG SPR_4
Heh! At this rate, you'll never find the end!

DLG SPR_5
"""
Don't listen to him. He never found the end.
Don't listen to him.

I'm sure you'll find it.

.....
"""

DLG SPR_6
"""
I tried looking for the end like you.
I gave up long ago.
"""

DLG SPR_7
"""
Still going are you?

Do you think it's worth it?

What are you trying to achieve by doing this?
"""

DLG SPR_8
"""
When you find the end...

...how will you feel?

Elated? Regretful?

...Anything?
"""

DLG SPR_9
"""
Have you ever considered that there might be no end?
If there isn't one, is your life worth living?
"""

DLG SPR_a
"""
...How long have you been here?

....More to the point, how long will you be here?
.....Will you stay until you find the end?
"""

DLG SPR_b
"""
How far are you willing to go?

Will you give in before you find the end?
"""

DLG SPR_c
"""
If you find the end, who will care?
Your friends?

.....

Do you think they feel the same way about you...
...as you do about them?
"""

DLG SPR_d
"""
Do you have any hobbies or interests?
Do they make you a better person?
.....

Do you want to be a better person?
"""

DLG SPR_e
"""
You see that key behind me?

It could help you on your journey.

It could help.

.....
"""

DLG SPR_f
"""
If you could see your future...

...would you have bother coming here?
Knowing how it would turn out?
"""

DLG SPR_g
"""
I'm scared to find the end.

What if it's horrible.
"""

DLG SPR_h
"""
Do you wish you could turn back?

Would something have been different if you did?
"""

DLG SPR_i
"""
Finding the end...

it's...lonely...

...wouldn't you agree?

.....
"""

DLG SPR_j
"""
I wonder...

Can you ever find the end?

Or is it that the end must instead find you?
"""

DLG SPR_k
"""
Should you try and force an outcome?
Is it the right thing to do?

For everyone included...

...including you?
"""

DLG SPR_l
"""
How much further?

.....

.....

...Do you know?
"""

DLG SPR_m
"""
The end!

I can feel it!

...But where?
"""

DLG SPR_n
"""
You...

.....

...Who are you?

Are you real?

...Am I real?

.....

........Is any of this real?
"""

DLG SPR_o
"""
Ah.

It's you.

So...

you've been searching for the end?
.....

.......

Was it worth it?

.....

Don't look at me like that.

You brought yourself here.

.....{
  - {item "key"} == 0 ?
    How do you feel?
    
    Is there anything you regret?
    
    Was there some way this could have turned out differently?
    .....
    
    Well, for whatever it's worth, you seemed to have reached the end.
    
    ...But I wonder...
    
    Is there nowhere else to go?
    
    Is this truly the end?
    
    .....
    
    I'll leave that for you to decide.
  - {item "key"} == 1 ?
    Hmm. I see you picked up that key.
    ...So what?
    
    Now that you're here. you think that changes anything?
    Only you are responsible for your actions.
    Well, for whatever it's worth, you seemed to have reached the end.
    
    ...But I wonder...
    
    Is there nowhere else to go?
    
    Is this truly the end?
    
    .....
    
    I'll leave that for you to decide.
}
"""

END 0


END undefined
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefinee
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefinef
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefineg
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefineh
You see a hole in the ground. It's deep. You can't see the bottom. Somehow, you fall in. You don't know how; all you know is that this is the end.

END undefinei
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefinej
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

END undefinek
You see a hole in the ground. It's deep. You can't see the bottom. You begin wondering if it even has one. As you contemplate this, you feel yourself falling. Somehow you fell in. You don't know how.

VAR a
42


