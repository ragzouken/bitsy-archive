lunar cultivation

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
54,92,60
191,255,132
255,253,157
0,0,0

PAL 1
35,61,38
180,219,110
230,214,122
0,0,0

PAL 2
24,41,31
133,186,104
207,202,136
0,0,0

PAL 3
11,20,17
104,171,100
178,181,142
0,0,0

PAL 4
0,0,0
59,92,78
156,163,156
0,0,0

ROOM 0
n,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
p,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
n,1s,1s,1s,1s,1s,b,b,b,b,1m,b,b,b,1s,1s
p,1s,1s,1s,1s,1s,b,1s,1s,1s,1s,1s,1s,b,1s,1s
n,1s,1s,1s,1s,1s,b,s,s,s,s,1s,1s,b,1s,1s
p,1s,1s,1s,1s,1s,b,r,r,r,r,1s,1s,b,1s,1s
n,1s,1s,1s,1s,1s,b,1k,y,y,1l,1s,1s,b,1s,1s
o,p,1s,1s,1s,1s,1s,1k,1u,1r,1l,1s,1s,b,1s,1s
m,n,1s,1s,1s,1s,1s,0,0,1j,0,b,b,b,1s,1s
p,o,p,1s,1s,1s,1s,1s,1s,0,0,1s,1s,1s,1s,1s
n,m,n,1s,b,c,g,h,b,0,0,1s,1s,1s,1s,1s
o,p,1s,1s,b,l,k,j,b,0,0,0,0,0,0,0
m,n,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
b,b,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
1s,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
NAME outside
ITM 1 12,6
ITM 2 7,3
EXT 9,7 1 9,12
EXT 15,11 2 1,11
PAL 0

ROOM 1
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,v,x,x,x,x,w,x,x,x,x,x,x,x,v,u
u,w,y,y,y,y,w,y,y,y,y,y,11,11,w,u
u,w,y,y,y,1b,1d,1c,y,y,y,y,y,y,w,u
u,w,1f,1g,z,19,z,1a,z,z,z,z,11,11,w,u
u,w,1h,1e,z,z,z,z,z,z,z,z,z,z,w,u
u,w,z,z,z,z,z,z,z,z,z,z,z,z,w,u
u,w,z,z,z,z,z,z,z,z,z,z,z,z,w,u
u,w,z,z,z,17,16,z,z,z,z,z,z,z,w,u
u,w,z,z,13,14,15,13,z,z,z,z,11,11,w,u
u,w,z,z,z,z,z,z,z,z,z,z,11,11,w,u
u,w,z,1i,z,z,z,z,z,18,z,z,z,z,w,u
u,v,x,x,x,x,x,x,x,w,x,x,x,x,v,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
NAME inside
EXT 9,12 0 9,8
END 0 3,6
END 0 3,5
PAL 0

ROOM 2
o,p,b,1k,1u,y,1u,y,1u,1l,b,f,d,i,b,1s
m,n,1s,1k,y,y,y,y,y,1l,b,f,d,i,b,b
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,b,f,d,i,b,1s
1s,1q,1q,1q,1q,1q,1q,1q,1s,1s,b,f,d,i,b,1s
1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,b,f,d,i,b,b
1q,1s,1s,1s,1q,1t,1t,1t,1q,1s,b,f,d,i,b,1s
1q,1s,1s,1s,1q,1t,1t,1t,1q,1s,b,f,d,i,b,1s
1q,1s,1s,1s,1q,1t,1t,1t,1q,1s,b,f,d,i,b,1s
1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,b,f,d,i,b,1s
1s,1q,1q,1q,1q,1q,1q,1q,1s,0,13,13,13,13,13,0
1s,1s,1s,1s,1s,1s,1s,1s,0,0,13,13,13,13,13,0
0,0,0,1s,1s,0,0,0,0,1s,b,f,d,i,b,1s
1s,1s,0,0,0,0,1s,1s,1s,1s,b,f,d,i,b,b
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,b,f,d,i,b,1s
b,b,b,b,b,b,b,b,b,b,b,f,d,i,b,1s
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,b,f,d,i,b,1s
ITM 0 2,7
ITM 0 1,7
ITM 0 1,6
ITM 0 2,6
ITM 0 2,5
ITM 0 1,5
ITM 0 3,6
ITM 3 6,7
ITM 3 5,7
ITM 3 5,6
ITM 3 6,6
ITM 3 6,5
ITM 3 5,5
ITM 3 7,5
ITM 3 7,6
ITM 3 7,7
ITM 0 3,7
EXT 0,11 0 14,11
EXT 15,10 3 1,10
EXT 15,9 3 1,9
PAL 0

ROOM 3
1s,1s,1s,1s,1s,1s,1s,1s,1q,1q,1s,1s,1s,1q,1q,1q
b,b,b,b,b,b,1s,1s,1q,1q,1s,1s,1s,1q,1q,1q
o,p,1s,1s,1s,b,1s,1s,1q,1q,1q,1s,1s,1s,1q,1q
m,n,1s,1s,1s,b,1s,1s,1q,1s,1q,1q,1q,1s,1s,1q
b,b,b,1m,b,b,1s,1s,1q,1q,1q,1q,1q,1q,1s,1s
1s,1s,1s,0,1s,1s,1s,1s,1s,1s,1s,1q,1q,1q,1s,1s
1s,1s,1s,0,1s,1s,1s,1s,1s,1s,1s,1s,0,s,s,s
1s,1s,1s,0,1s,1s,1s,1s,1s,1s,1s,1s,s,q,q,q
1s,1s,1s,0,1s,1s,1s,1s,1s,1s,1s,1s,q,r,r,r
0,0,0,0,0,0,0,0,0,1s,1s,1s,r,y,1u,y
0,0,0,0,0,0,0,0,0,0,1s,1s,1k,y,y,y
1s,1s,1s,1s,1s,1s,1s,0,0,0,1s,1s,1k,1r,1u,y
b,b,b,1s,1s,1s,1s,1s,0,0,0,0,0,1j,0,b
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1s,1q,1q,b
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1s,1q,1q,b
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1s,1q,b,b
ITM 4 2,2
EXT 0,10 2 14,10
EXT 0,9 2 14,9
EXT 13,11 5 5,11
EXT 8,15 4 8,1
EXT 9,15 4 9,1
PAL 0

ROOM 4
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1s,1q,b,1s
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1s,1q,b,1q
1s,1s,1s,b,1s,1s,1s,1s,0,0,1s,1s,1q,1q,b,1q
b,b,b,1q,1q,1q,1s,1s,0,0,1s,1s,1q,1q,1q,b
1s,1q,1q,1q,1q,1q,1s,1s,0,1s,1s,1s,1q,1q,1q,1s
1s,1q,1q,1s,1s,1s,1s,1s,0,1s,1s,1s,1s,1q,1q,1s
1s,1s,1s,1s,0,0,0,0,0,0,0,1s,1s,1s,1s,1s
1s,1s,1s,b,b,1s,13,1s,1s,1s,0,0,0,1s,1s,1s
b,b,b,c,e,e,e,e,h,1s,1s,1s,0,0,1s,1s
g,h,c,d,d,d,d,d,d,h,1s,1s,1s,0,0,0
l,d,d,d,d,d,d,d,d,i,b,1s,1s,1s,1s,1s
1s,l,d,d,d,d,d,d,d,i,b,b,1s,1s,1s,1s
1s,1s,f,d,d,d,d,d,d,d,k,d,b,b,b,1s
p,1s,l,d,d,d,d,d,d,j,1s,l,k,k,d,b
n,1s,1s,l,k,k,j,f,i,o,p,1s,1s,1s,l,k
o,p,1s,1s,1s,1s,1s,f,i,m,n,1s,1q,1s,1s,1s
NAME lake
EXT 8,0 3 8,14
EXT 9,0 3 9,14
EXT 15,9 7 1,9
PAL 0

ROOM 5
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,v,x,x,x,x,x,x,v,u
u,v,x,x,x,x,x,v,11,y,y,y,y,11,w,u
u,w,y,y,y,y,y,1l,y,y,y,y,y,y,w,u
u,w,y,y,y,y,y,1l,11,z,z,z,z,11,w,u
u,w,s,s,s,z,z,z,11,z,z,z,z,z,w,u
u,w,r,r,r,z,z,z,11,11,11,11,1m,11,w,u
u,w,b,b,b,z,z,z,z,z,z,z,z,z,w,u
u,w,z,z,z,z,z,z,z,z,z,o,p,z,w,u
u,w,o,p,z,z,z,z,z,z,z,14,15,16,w,u
u,w,11,11,z,z,z,z,z,z,z,z,11,11,w,u
u,w,11,11,z,18,z,z,12,13,12,13,12,12,w,u
u,v,x,x,x,w,x,x,x,x,x,x,x,x,v,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
NAME store
EXT 5,12 3 13,12
EXT 12,7 6 12,7
EXT 12,6 6 12,6
PAL 0

ROOM 6
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,v,x,x,x,x,x,x,v,u
u,v,x,x,x,x,x,v,11,y,y,y,y,11,w,u
u,w,y,y,y,y,y,1l,y,y,y,y,y,y,w,u
u,w,y,y,y,y,y,1l,11,z,z,z,z,11,w,u
u,w,s,s,s,z,z,z,11,z,z,z,z,z,w,u
u,w,r,r,r,z,z,z,11,11,11,11,1m,11,w,u
u,w,b,b,b,z,z,z,z,z,z,z,z,z,w,u
u,w,z,z,z,z,z,z,z,z,z,o,p,z,w,u
u,w,o,p,z,z,z,z,z,z,z,14,15,16,w,u
u,w,11,11,z,z,z,z,z,z,z,z,11,11,w,u
u,w,11,11,z,18,z,z,12,13,12,13,12,12,w,u
u,v,x,x,x,w,x,x,x,x,x,x,x,x,v,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
NAME store_counter
EXT 5,12 3 13,12
EXT 12,7 5 12,7
EXT 12,8 5 12,8
PAL 0

ROOM 7
1s,1s,1s,o,p,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s
1q,1q,1q,m,n,1s,b,b,b,b,b,b,b,b,b,1s
1q,1q,1q,1s,1s,b,1q,1q,1q,1q,1q,1s,1q,1q,1q,b
b,b,b,b,b,b,1s,1q,1q,1q,1q,1q,1s,1q,1q,1q
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q,1q
1s,1s,1s,1s,1q,1q,1q,1q,1q,1s,1s,1s,1s,1q,1q,1q
1s,1s,1q,1q,1q,1q,1q,1q,1q,1s,0,0,0,0,1q,1s
1s,1s,1s,1s,1s,1s,1s,1s,1s,0,0,1q,1q,0,1s,1s
1s,1s,1s,1s,0,0,0,0,0,0,1s,1s,1q,0,1s,1s
0,0,0,0,0,1s,1s,1s,1s,1s,1s,1s,1s,0,0,1s
1s,1s,1s,1s,1s,1s,1q,1q,1q,1s,1s,1s,1s,1s,0,0
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1s,1s,1s
1s,1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1s,1s
b,b,b,b,1s,1q,1q,1q,1q,1s,1q,1q,1s,1q,1s,1s
k,g,h,b,1s,1s,o,p,1s,1q,1q,1q,1q,1q,1q,1q
1s,f,i,b,b,b,m,n,1s,1q,1q,1s,1q,1q,1q,1q
NAME field 1
EXT 15,10 8 1,10
EXT 0,9 4 14,9
PAL 0

ROOM 8
1s,1s,1s,b,1s,1s,1s,1s,1s,o,p,1s,1s,1s,1s,1s
1s,1s,b,1s,1s,1s,1s,1q,1q,m,n,1q,1s,1q,1s,1s
b,b,1s,1s,1s,1s,1q,1q,1q,1s,1s,1q,1s,1q,1q,1q
1q,1s,1q,1q,1q,1q,1q,1q,1s,1q,1s,1q,1s,1q,1s,1q
1q,1q,1p,1q,1q,1s,1p,1p,1p,1p,1p,1p,1q,1q,1s,1q
1q,1q,1s,1q,1p,1p,1p,1q,1p,1p,1s,1q,1q,1s,1q,1q
1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,1s
1s,1s,1q,1q,0,0,0,1q,1q,1q,o,p,1q,1q,1q,1s
1s,1s,1s,0,0,1q,0,0,1q,1q,m,n,1q,1s,1s,1s
1s,1s,0,0,1q,1q,1q,0,1q,1q,1q,1q,1s,1s,1s,1s
0,0,0,1s,1s,1s,1s,0,0,1q,1q,1s,0,0,0,0
1s,1s,1s,1s,1s,1q,1q,1q,0,0,0,0,0,1s,1s,1s
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,0,0,1s,1s,1s,1s
1s,1s,1q,1s,1q,1q,1q,1p,1p,1q,1q,1q,1s,1s,1s,1s
1q,1q,1p,1q,1q,1p,1p,1p,1s,1s,1s,1q,1q,1s,1s,1q
1q,1p,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q
NAME field 2
EXT 15,10 9 1,10
EXT 0,10 7 14,10
PAL 0

ROOM 9
1s,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
1s,1s,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q,1q,1q
1q,1q,1q,1q,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q
1s,1s,1q,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1s
1s,1s,1s,1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1s,1q
1s,1s,1s,1s,1q,1s,1s,1s,1s,1q,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1q,1q
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1s,1q,1q,1q,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
NAME field 3
EXT 15,10 a 1,10
EXT 0,10 8 14,10
PAL 0

ROOM a
1o,1o,1o,1o,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n
1p,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1p,1o,1n,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1q,1p,1p,1p,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1q,1q,1q,1q,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1q,1q,1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1o,1o,1o,1o
1q,1q,1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n
1p,1p,1p,1o,1p,1p,1o,1o,1o,1n,1o,1n,1n,1n,1n,1n
1o,1o,1o,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
NAME field 4
ITM 6 4,10
EXT 15,10 b 1,10
EXT 0,10 9 14,10
PAL 0

ROOM b
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,q,q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,b,1k,y
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,1p,1k,1u
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1k,y
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
0,0,0,0,0,0,0,0,13,13,13,13,0,0,0,0
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1q,1q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,s,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,r,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,1k,y,y
NAME town entrance
ITM 5 13,10
EXT 15,10 c 1,10
EXT 0,10 a 14,10
PAL 0

ROOM c
1o,1o,1o,1k,y,y,1l,0,0,1s,1q,1p,b,1o,1o,1o
1o,1o,1o,1k,y,y,1l,0,0,1s,1q,1p,b,1o,1o,1o
1o,1o,1o,b,1p,1q,1s,0,0,1s,1q,1p,b,1o,1o,1o
s,s,s,b,1p,1q,1s,0,0,1s,o,1h,p,1o,1o,1o
q,q,q,1o,b,1p,1q,1s,0,1s,14,1m,15,1o,1o,1o
r,r,r,1o,s,s,s,1s,0,1s,1k,y,1l,1o,1o,1o
y,y,1l,b,r,r,r,1s,0,1s,1k,1u,1l,b,b,b
1u,y,1l,1p,1k,1u,1l,1s,0,1s,1k,y,1l,1p,1p,1p
y,y,1l,1q,1k,y,1l,1s,0,1s,1k,1r,1l,1q,o,p
1s,1j,1s,1s,1s,1s,1s,1s,0,1s,1s,1j,1s,1s,m,n
0,0,0,0,0,0,0,0,0,0,0,0,1s,1s,1p,o
1s,1s,1s,1s,1s,1s,s,s,s,s,s,s,1s,1s,1p,m
1q,1q,1q,1q,1q,1q,q,q,q,q,q,q,1q,1q,1p,1q
s,s,s,s,1p,1p,r,r,r,r,r,r,1p,1p,1p,1p
r,r,r,r,1o,1o,1k,y,y,y,y,1l,1o,1o,1o,1o
y,y,y,1l,b,b,1k,1u,y,y,1u,1l,b,b,b,b
NAME town
EXT 7,0 d 7,14
EXT 8,0 d 8,14
EXT 0,10 e 14,10
EXT 11,8 k 8,10
PAL 1

ROOM d
1o,1o,1o,1o,1p,1q,1s,0,0,1s,1q,1p,1o,1o,1o,1o
1o,1o,1o,1o,1p,1q,1s,13,13,1s,1q,1p,1o,1o,1o,1o
e,e,e,e,e,e,e,13,13,e,e,e,e,e,e,e
k,k,k,k,k,k,k,k,k,k,k,k,k,k,s,s
1p,1q,1q,1s,1s,1s,1s,0,0,1s,1s,1s,1s,1q,r,r
1p,1q,1s,0,0,0,0,0,0,0,0,0,0,1s,1k,y
s,s,1s,0,0,1s,1s,1s,1s,1s,1s,0,0,1s,s,s
r,r,1s,0,0,1s,1s,1s,1s,1s,1s,0,0,1s,r,r
y,1l,1s,0,0,1s,1s,1s,1s,1s,1s,0,0,1s,1k,y
1u,1l,1s,0,0,1s,1s,1s,1s,1s,1s,0,0,1s,1k,1u
y,1l,1s,0,0,0,0,0,0,0,0,0,0,1s,1k,y
1p,1q,1s,s,s,s,s,0,0,1s,1s,1s,1s,1q,1q,1p
1p,1q,1q,q,q,q,q,0,0,1s,1q,1q,1q,1q,1p,b
b,1p,1p,r,r,r,r,0,0,1s,1q,1p,b,b,b,1o
1o,b,b,1k,y,1u,1l,0,0,1s,1q,1p,b,1o,1o,1o
1o,1o,1o,1k,y,y,1l,0,0,1s,1q,1p,b,1o,1o,1o
NAME town center
EXT 7,15 c 7,1
EXT 8,15 c 8,1
PAL 1

ROOM e
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,q,q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,b,1k,y
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,1p,1k,1u
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1k,y
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
0,0,0,0,0,0,0,0,13,13,13,13,0,0,0,0
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1q,1q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,s,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,r,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,1k,y,y
NAME town entrance2
ITM 6 12,10
EXT 15,10 c 1,10
EXT 6,10 f 6,10
PAL 1

ROOM f
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,1o,1o
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,q,q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,1o,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,b,1k,y
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,1p,1k,1u
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1k,y
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
0,0,0,0,0,0,0,0,13,13,13,13,0,0,0,0
1o,1o,1o,1o,1o,1o,1o,1o,b,f,i,b,1s,1s,1s,1s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1q,1q,1q,1q
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1p,s,s,s
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,1o,r,r,r
1n,1n,1n,1n,1n,1n,1n,1n,b,f,i,b,b,1k,y,y
NAME night1
EXT 0,10 g 14,10
PAL 2

ROOM g
1o,1o,1o,1o,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n
1p,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1p,1o,1n,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1q,1p,1p,1p,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1q,1q,1q,1q,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1q,1q,1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1o,1o,1o,1o
1q,1q,1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n
1p,1p,1p,1o,1p,1p,1o,1o,1o,1n,1o,1n,1n,1n,1n,1n
1o,1o,1o,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
NAME night2
EXT 9,10 h 9,10
PAL 2

ROOM h
1o,1o,1o,1o,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n
1p,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1p,1o,1n,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
1q,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n,1n,1n,1n,1n
1q,1q,1p,1o,1p,1o,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n
1q,1q,1q,1p,1p,1p,1p,1o,1o,1n,1o,1n,1n,1n,1n,1n
1q,1q,1q,1q,1q,1q,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1q,1q,1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1o,1o,1o,1o
1q,1q,1q,1q,1p,1o,1p,1o,1p,1o,1o,1n,1n,1n,1n,1n
1q,1q,1p,1p,1p,1p,1p,1p,1o,1o,1n,1n,1o,1n,1n,1n
1p,1p,1p,1o,1p,1p,1o,1o,1o,1n,1o,1n,1n,1n,1n,1n
1o,1o,1o,1p,1p,1o,1o,1n,1n,1n,1n,1n,1n,1n,1n,1n
NAME night3
ITM 6 5,10
EXT 0,10 i 14,10
PAL 3

ROOM i
1s,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
1s,1s,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q,1q,1q
1q,1q,1q,1q,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q
1s,1s,1q,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1s
1s,1s,1s,1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1s,1q
1s,1s,1s,1s,1q,1s,1s,1s,1s,1q,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1q,1q
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1s,1q,1q,1q,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
NAME night4
EXT 7,10 j 7,10
PAL 3

ROOM j
1s,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
1s,1s,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q,1q,1q
1q,1q,1q,1q,1s,1q,1q,1q,1q,1q,1q,1q,1q,1s,1q,1q
1s,1s,1q,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1s
1s,1s,1s,1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1s,1q
1s,1s,1s,1s,1q,1s,1s,1s,1s,1q,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1s,1q,1q,1q
1s,1s,1s,1s,1s,1q,1s,1s,1s,1s,1s,1q,1q,1q,1q,1q
1s,1s,1s,1s,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q,1q
1q,1s,1q,1q,1q,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p,1p
1q,1q,1q,1p,1p,1p,1o,1o,1o,1o,1o,1o,1o,1o,1o,1o
NAME night5
ITM 7 4,10
END 1 3,10
PAL 4

ROOM k
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,v,x,x,x,x,x,x,x,x,v,u,u,u
u,u,u,w,y,y,y,y,0,12,y,y,w,u,u,u
u,u,u,w,y,y,y,0,12,1l,y,y,w,u,u,u
u,u,u,w,z,z,z,12,y,1l,z,z,w,u,u,u
u,u,u,w,z,z,z,z,z,z,z,z,w,u,u,u
u,u,u,w,11,z,z,z,z,z,z,11,w,u,u,u
u,u,u,w,19,z,z,z,18,z,z,1a,w,u,u,u
u,u,u,v,x,x,x,x,w,x,x,x,v,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
NAME tower1
EXT 8,10 c 11,9
EXT 9,5 l 10,7
PAL 1

ROOM l
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,v,x,x,x,x,x,x,v,u,u,u,u
u,u,u,u,w,11,y,1u,y,y,y,w,u,u,u,u
u,u,u,u,w,y,y,y,y,y,y,w,u,u,u,u
u,u,u,u,w,16,z,z,z,13,z,w,u,u,u,u
u,u,u,u,w,15,12,z,z,z,z,w,u,u,u,u
u,u,u,u,w,11,z,z,z,z,11,w,u,u,u,u
u,u,u,u,w,19,z,z,z,z,1a,w,u,u,u,u
u,u,u,u,v,x,x,x,x,x,x,v,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
u,u,u,u,u,u,u,u,u,u,u,u,u,u,u,u
NAME tower2
EXT 9,7 k 8,6
PAL 1

TIL 10
10101010
01000000
00100000
00010000
01010101
00001000
00000100
00000010

TIL 11
11111111
10000001
10000001
10000001
11111111
10101001
10101001
11111111
WAL true

TIL 12
00000000
00000000
00000000
01111110
10000001
10000001
10111101
11000011

TIL 13
00000000
01111110
10100101
10100101
10100101
10100101
11111111
10000001
NAME seat
WAL false

TIL 14
10111111
10111111
10111100
10011111
10000000
01111111
00111100
00100100
NAME table
WAL true

TIL 15
10001101
11111101
00111101
11111001
00000001
11111110
00111100
00100100
NAME table
WAL true

TIL 16
00000000
00000000
01110000
11011000
10001100
10001010
10001101
10001001
NAME table
WAL true

TIL 17
00001000
00000100
00001100
00010010
01110011
10010010
10010011
10110011
>
00000100
00001000
00001100
00010010
01110011
10010010
10010011
10110011
NAME table
WAL true

TIL 18
00000000
00000000
00000000
01000100
00000000
10101010
00000000
11111111
NAME door
WAL false

TIL 19
10100101
10100101
11111111
10010011
10010011
11111111
10100101
11111111
NAME fireplace
WAL true

TIL b
01100000
10010110
10111001
11101101
11101111
11111111
01101111
00000110
NAME fence
WAL true

TIL c
00000000
01010101
00111111
01111111
00111011
01110101
00111111
01111111
>
00000000
01010101
00111111
01111111
00111101
01111010
00111111
01111111
NAME water
WAL true

TIL d
11111111
11011111
10101111
11111111
11111011
11110101
11111111
11111111
>
11111111
11101111
11010111
11111111
11111101
11111010
11111111
11111111
NAME water
WAL true

TIL e
00000000
01010101
11111111
11111111
11111011
11110101
11111111
11111111
>
00000000
01010101
11111111
11111111
11111101
11111010
11111111
11111111
NAME water
WAL true

TIL f
00111111
01111111
00111111
01111111
00111011
01110101
00111111
01111111
>
00111111
01111111
00111111
01111111
00111101
01111010
00111111
01111111
NAME water
WAL true

TIL g
00000000
01010101
11111111
11111111
11111011
11110101
11111111
11111111
>
00000000
01010101
11111111
11111111
11111101
11111010
11111111
11111111
NAME water
WAL true

TIL h
00000000
01010101
11111110
11111111
11111110
11111111
11111110
11111111
>
00000000
01010101
11111110
11111111
11111110
11111111
11111110
11111111
NAME water
WAL true

TIL i
11111110
11011111
10101110
11111111
11111110
11111111
11111110
11111111
>
11111110
11101111
11010110
11111111
11111110
11111111
11111110
11111111
NAME water
WAL true

TIL j
11111110
11011111
10101110
11111111
11111110
11111111
11111110
01010101
>
11111110
11101111
11010110
11111111
11111110
11111111
11111110
01010101
NAME water
WAL true

TIL k
11111111
11011111
10101111
11111111
11111011
11110101
11111111
01010101
>
11111111
11101111
11010111
11111111
11111101
11111010
11111111
01010101
NAME water
WAL true

TIL l
00111111
01111111
00111111
01111111
00111011
01110101
00111111
01010101
>
00111111
01111111
00111111
01111111
00111101
01111010
00111111
01010101
NAME water
WAL true

TIL m
11011111
11111111
01101111
00111111
00000110
00001011
01010111
11111111
NAME tree
WAL true

TIL n
11111011
11111111
11110110
11111100
10100000
11110000
10111010
11011111
NAME tree
WAL true

TIL o
00111111
01011111
10111111
11111111
11111111
11111111
11111111
11111111
NAME tree
WAL true

TIL p
11111100
11111010
11111101
11111111
11111111
11111111
11111111
11111111
NAME tree
WAL true

TIL q
10100001
10100101
10100001
10000101
10100101
10000101
10100001
10000101
NAME roof
WAL true

TIL r
10100001
10100101
10100101
10000101
11111101
10000011
11000001
01111110
NAME roof
WAL true

TIL s
01111110
10000101
10100001
10100101
10100001
10000101
10100101
10000101
NAME roof
WAL true

TIL u
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME black
WAL true
COL 3

TIL v
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
NAME black
WAL true
COL 3

TIL w
01111110
01111110
01111110
01111110
01111110
01111110
01111110
01111110
NAME black
WAL true
COL 3

TIL x
00000000
11111111
11111111
11111111
11111111
11111111
11111111
00000000
NAME black
WAL true
COL 3

TIL y
11110111
11111111
11110111
01001010
11111101
11111111
11111101
01001010
NAME wall
WAL true

TIL z
11111111
10000000
01000000
01000000
11111111
00010000
00001000
00001000
NAME floor
COL 3

TIL 1a
10100101
10100101
11111111
11001001
11001001
11111111
10100101
11111111
NAME fireplace
WAL true

TIL 1b
11110100
11000001
11011111
01010011
00010011
11111111
10100101
11111111
NAME fireplace
WAL true

TIL 1c
00110101
10000011
11111001
11001010
11001000
11111111
10100101
11111111
NAME fireplace
WAL true

TIL 1d
00000000
10111101
11111111
11001001
11001001
11111111
10100101
11111111
NAME fireplace
WAL true

TIL 1e
11001111
11111111
11111101
11111110
11111111
11111111
11111111
11111110
NAME bed

TIL 1f
00011111
10110000
11101111
11011111
11011111
00001111
11110000
11111111

TIL 1g
11111000
00001101
11110111
11111011
11111011
11110000
00001111
11111111

TIL 1h
11111111
11111111
11111001
11100111
11111111
11111111
11111111
01111111
NAME bed

TIL 1i
00000000
00000000
00000000
00000000
00111000
01000100
01111100
00111000
NAME dogfood

TIL 1j
11111111
00000000
10101010
00000000
01000100
00000000
00000000
00000000
NAME door
WAL false

TIL 1k
10110111
10111111
10110111
11001010
10111101
10111111
10111101
11001010
NAME wall
WAL true

TIL 1l
11110101
11111101
11110101
01001011
11111101
11111101
11111101
01001011
NAME wall
WAL true

TIL 1m
00000000
11100000
10010111
11101001
10010111
11101001
00000111
00000000
NAME gate
WAL false

TIL 1n
11110101
10101011
11011111
11111010
01011101
10111111
11110101
11111011
NAME field

TIL 1o
01010101
10101010
01011101
10101010
01010101
10111110
01010101
10101010
NAME field

TIL 1p
01000100
00101010
01000101
10101010
01010000
00001010
01010101
10001000
NAME field

TIL 1q
01000000
00001010
01000001
00001010
01010000
00000000
00010100
00001000
NAME field

TIL 1r
00000000
00111100
01111110
01111110
01111110
01111110
01111110
01111110
NAME door
WAL false
COL 3

TIL 1s
00000000
00001010
00000000
00001010
01000000
00000000
00010000
00000000
NAME field

TIL 1t
00000000
00000000
00000000
00000000
00000000
00100000
00011000
00111100
NAME turnip_plant
COL 2

TIL 1u
11110111
10000001
10010101
00110100
10000001
10110101
10000001
01001010
WAL true

SPR 10
00000000
01111111
01100011
01111111
01000101
01111111
00001000
00001000
POS e 12,9

SPR 11
00000000
01111111
01100011
01111111
01000101
01111111
00001000
00001000
POS f 12,9

SPR 12
00111100
01111110
11011011
01011010
01111110
00100100
10011001
01100110
NAME fish liker
DLG SPR_f
POS l 7,9

SPR A
00111100
01111110
01011010
01011010
01111110
10100101
10111101
01100110
POS 1 5,7

SPR a
00000000
00000000
00000000
10000000
10011100
10111010
01110101
11101111
>
00000000
00000000
00000000
01000000
10011100
10111010
01110101
11101111
NAME dog
DLG SPR_0
POS 1 2,12

SPR b
10101111
10111111
10111111
10111111
10011111
10000000
01111111
00100000
NAME tv
DLG SPR_tv
POS 1 10,5

SPR c
11111101
11111101
11110101
11100101
11111001
00000001
11111110
00000100
NAME tv
DLG SPR_tv
POS 1 11,5

SPR d
00100000
01011110
10000001
00000001
11111111
00000001
11111001
11111101
NAME tv
DLG SPR_tv
POS 1 11,4

SPR e
00000100
01111010
10000001
10000000
11111111
10000000
10011111
10100111
NAME tv
DLG SPR_tv
POS 1 10,4

SPR f
00100000
00010000
00010001
00111010
01111110
01010110
10000001
11111111
>
00000000
00010000
00100100
01110010
01011110
01101011
10000001
11111111
NAME fire
DLG SPR_1
POS 1 6,5

SPR g
00000000
01001111
10011111
10111111
10110000
10111111
10111000
10111111
NAME bed
DLG SPR_bed
POS 1 2,7

SPR h
00000000
11110010
11111001
11111101
00001101
11111101
00011101
11111101
NAME bed
DLG SPR_bed
POS 1 3,7

SPR i
00000000
00000000
00000000
00000000
01100000
00110100
00011000
00111100
NAME turnip
DLG SPR_2
POS 2 3,5

SPR j
00000000
00011101
10101110
01101111
00011110
00101110
00110011
01111110
>
00000000
00011101
00101110
00101111
01011110
10101110
01110011
01111110
DLG SPR_3
POS 3 2,3

SPR k
00111100
11111111
01011010
01011010
01111110
10000001
10111101
01111110
NAME store
POS 5 10,6

SPR l
00111100
11111111
01011010
01011010
01111110
10000001
10111101
01111110
NAME counter
DLG SPR_4
POS 6 10,6

SPR m
00000000
01010101
11111111
11111111
11111011
11110101
11111111
11111111
>
00000000
01010101
11111111
11111111
11111101
11111010
11111111
11111111
NAME fishing
DLG SPR_6
POS 4 6,8
COL 1

SPR n
11111111
10000001
10000001
10000001
11111111
10101001
10101001
11111111
NAME fishing counter
DLG SPR_5
POS 5 10,7
COL 1

SPR o
00000100
00001110
00010111
00110111
00111111
00011001
00000000
00000000
NAME cow
DLG SPR_cow
POS 7 5,5

SPR p
00000000
00000000
11111100
11100110
00111110
00011101
11111000
10001000
NAME cow
DLG SPR_cow
POS 7 6,5

SPR q
00000100
00001110
00010111
00110110
00111111
00011001
00000000
00000000
NAME cow
DLG SPR_cow
POS 7 9,13

SPR r
00000000
00000000
11111100
00111110
01100110
11000101
11111000
10001000
NAME cow
DLG SPR_cow
POS 7 10,13

SPR s
00000000
01111111
01100011
01111111
01000101
01111111
00001000
00001000
DLG SPR_7
POS b 12,9

SPR t
00000000
00111100
01111110
01111110
01000010
01111110
01000010
01111110
DLG SPR_8
POS c 1,8

SPR u
00000000
01111111
01000101
01111111
01000001
01111111
00001000
00001000
DLG SPR_9
POS d 7,4

SPR v
01111110
01111110
11111111
01011010
01111110
10011001
10111101
01100110
DLG SPR_a
POS d 8,5

SPR w
00000000
00111100
01011010
01011010
01111110
10000001
01100110
01111110
NAME the best character
DLG SPR_b
POS d 12,11

SPR x
01111110
11011011
11011011
01111110
01111110
10100101
10100101
01111110
DLG SPR_c
POS d 2,7

SPR y
00000000
10111101
01011010
01011010
01111110
00100100
10011001
01111110
DLG SPR_d
POS c 6,9

SPR z
00000000
00111100
01011010
01011010
01111110
01000010
10011001
01111110
DLG SPR_e
POS c 13,11

ITM 0
00000000
00000000
01100000
10010000
10010110
01001010
00111100
01111110
NAME turnip
DLG ITM_0

ITM 1
00000000
00001110
00111110
00011110
00011100
00100100
11000000
01000000
NAME shovel
DLG ITM_2

ITM 2
00000000
00000000
00000000
01000110
10111101
01111100
00111100
00111100
NAME watering can
DLG ITM_1

ITM 3
00000000
00001010
00000000
00001010
01000000
00000000
00010000
00000000
NAME turnip_plant
DLG ITM_3
COL 1

ITM 4
00000000
00000000
00010100
00101010
01111111
01010101
01101011
00111110
NAME apples
DLG ITM_4

ITM 5
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME tired
DLG ITM_5

ITM 6
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME tired more
DLG ITM_6

ITM 7
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME real tired
DLG ITM_7

DLG ITM_0
{wvy}*you got {rbw}a turnip{rbw}*{wvy}

DLG SPR_tv
"""
{shuffle
  - it's the weather channel:
    {wvy}"cloudy with a chance of rain"{wvy}
  - it's that fishing show:
    {wvy}"remember to use bait!"{wvy}
  - it's the cooking channel:
    {wvy}"ooh getta load of that sizzle!"{wvy}
}
"""

DLG SPR_1
*{shk}crackle{shk}*

DLG SPR_bed
"""
it's my bed
i'm not tired yet though
"""

DLG ITM_1
{wvy}*you got a {rbw}watering can{rbw}*{wvy}

DLG ITM_2
{wvy}*you got a {rbw}shovel{rbw}*{wvy}

DLG SPR_0
i'll let them sleep for now

DLG SPR_2
"""
{
  - {item "watering can"} == 1 ?
    this one's not ready
    {wvy}*you give it some {rbw}water{rbw}*{wvy}
  - else ?
    this one looks like it could use some water...
}
"""

DLG ITM_3
{wvy}*you plant a {rbw}turnip seed{rbw}*{wvy}

DLG ITM_4
{wvy}*you got a {rbw}basket of apples{rbw}*{wvy}

DLG SPR_3
"""
{
  - {item "apples"} == 1 ?
    "{shk}hey!{shk}"
    "those are my apples!"
  - else ?
    {wvy}"hello!"{wvy}
    "nice day out, isn't it?"
}
"""

DLG SPR_4
"""
{shk}"hey!"{shk}
"the counter is employees only!"
"""

DLG SPR_6
"""
{
  - fished == 0 ?
    {
      - bait > 0 ?
        you put some bait on your hook and cast the line
        ...{br}
        you didn't catch anything{bait = bait-1}
      - else ?
        you put some bait on your hook and cast the line
        ...{br}
        you didn't catch anything{fished = 1}
    }
  - fished == 1 ?
    you're out of bait!
  - fished == 2 ?
    you put some bait on your hook and cast the line
    ...{br}
    {wvy}*you caught a {rbw}rainbow salmon{rbw}*{wvy}{fished = 3}{br}
    (of course it only took one more bait...)
  - else ?
    that's enough fishing for today
}
"""

DLG SPR_5
"""
{
  - fished == 0 ?
    "hello! if you want to buy anything, just let me know"
  - fished == 1 ?
    {fished = 2}"here to buy some bait? that'll be $12!"
    {wvy}*you got {rbw}6 fish bait{rbw}*
  - else ?
    {wvy}"thanks for your business!"{wvy}
}
"""

DLG SPR_cow
{wvy}"mooooo"{wvy}

DLG ITM_5
"""
{wvy}*yawn*{wvy}

you're getting kind of tired...
"""

DLG SPR_7
the welcoming sign has the town name and population count

DLG SPR_8
"""
there's a note on the door:
{wvy}"grand opening next week!"{wvy}
"""

DLG SPR_9
"""
there's a sign here. it reads:

"{wvy}sorry!{wvy} the bridge past the town square is out"
"""

DLG SPR_a
"""
{sequence
  - {wvy}"hello citizen!"{wvy}
    "how can the mayor help you?"
  - "the bridge?"
    "i'm afraid it's out of order!"
  - "it'll cost the town at least $2000 to fix!"
  - {wvy}"if only a kind soul donated to our bridge restoration fund..."{wvy}
}
"""

DLG SPR_b
"""
{shuffle
  - "you need a {rbw}watering can{rbw} to water plants!"
  - "if you want to milk a cow, you'll need a {rbw}bucket{rbw} first!"
  - "if you're tired, you should get some sleep!"
  - "i like to watch tv. {wvy}you can learn a lot from television!{wvy}"
  - "if you give people gifts often, maybe they'll like you more!"
  - "you'll need {rbw}energy drink{rbw}s to stay up late!"
  - "they say the town is blessed by pixies. {wvy}what a load of hooey!{wvy}"
  - "dogs are loyal, but they have feelings too!"
  - "you need at least one cattle before you can breed more!"
  - "{rbw}scarecrow{rbw}s can keep unwanted birds away from crops!"
  - "cooking mistakes aren't tasty, but they're {wvy}technically{wvy} edible!"
  - "cheap fishing lines break more often than more expensive ones!"
  - "planning your day may be tough, but helps you get more done!"
  - "the catalog is handy, but {wvy}mail only comes once a week{wvy}!"
  - "tree stumps are tough to remove!"
  - "some plants {wvy}can't{wvy} grow in fall, and some {wvy}only{wvy} grow in fall!"
  - "chickens need to be fed every day, or they might get sick!"
  - "the town hospital takes care of those in need, {wvy}but for a price!{wvy}"
  - "if someone likes you enough, they might agree to {rbw}marriage{rbw}!"
  - "i'm excited for the festival at the end of the year!"
  - "{clr3}golden{clr3} things usually cost more than {clr2}average{clr2} things!"
  - "running is fast, but riding a horse is faster!"
  - "the general store sells all sorts of things!"
  - "i thought about buying more land, but it's {wvy}expensive!{wvy}"
  - "no one's seen a {rbw}dragon{rbw} around these parts in years!"
  - "{rbw}wheat{rbw}, {rbw}cheese{rbw}, and {rbw}anchovies{rbw} can make a good {rbw}pizza{rbw}!"
  - "raising kids is {wvy}tough{wvy}, but {wvy}rewarding!{wvy} or so i've heard..."
  - "i heard {rbw}tulip{rbw}s grow on the other side of the river!"
  - "{rbw}moonbloom{rbw}s only bloom during a {rbw}full moon{rbw}!"
  - "time is limited, and it's always running out!"
  - "if i had a lot of money, i'd keep it at the {rbw}bank{rbw}!"
  - "most people won't go out in the rain, but some like it!"
  - "you can change your hairstyle and clothes at the barbershop!"
  - "i eat big breakfasts every morning to prepare for the day!"
  - "some folks have lots to say if you talk to them repeatedly!"
  - "cooking is fun and easy, so long as you know the {rbw}recipe{rbw}s!"
  - "the lake out west is a great fishing spot!"
  - "people who like you might give you presents on your {rbw}birthday{rbw}!"
  - "i wonder why {rbw}happy{rbw} cows give the best milk? must be biology!"
  - "i entered the cooking contest last year, but came in {wvy}third!{wvy}"
  - "that new restaurant sounds like a good place for a date!"
  - "{rbw}storm{rbw}s can come out of the blue, and are {wvy}very dangerous!{wvy}"
  - "{rbw}blizzard{rbw}s only happen in winter"!
  - "{rbw}turnip{rbw}s are cheap and easy to grow!"
}
"""

DLG SPR_c
"hello! you must be the farmer from just out of town"

DLG SPR_d
"""
{wvy}"hiya!"{wvy}
"one day i'm gonna have a farm!"
"""

DLG SPR_e
"""
{wvy}"good afternoon!"{wvy}
"are you done work for the day?"
"""

DLG ITM_6
{wvy}*yawn*{wvy}

DLG ITM_7
{wvy}*yaaawwwn*{wvy}

DLG SPR_f
"""
{
  - fished == 3 ?
    {sequence
    - "oh wow, that's a nice fish! did you catch that yourself?"
    - "f-{wvy}for me?{wvy} are you sure?"
    - "thank you very much!"
    }
  - else ?
    "i don't really have time to chat right now, {wvy}sorry!{wvy}"
}
"""

END 0
i suppose a nap wouldn't hurt...

END 1
{wvy}you pass out from exhaustion...{wvy}

VAR fished
0

VAR bait
2

