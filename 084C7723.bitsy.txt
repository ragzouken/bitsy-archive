
Roguelike Simulator

# BITSY VERSION 4.4

! ROOM_FORMAT 1

PAL 0
NAME nethack
27,19,32
164,160,160
255,255,255

PAL 1
NAME treasure
76,60,8
253,236,52
255,255,255

PAL 2
NAME hell
130,38,9
236,105,55
248,226,124

PAL 3
NAME end
87,73,95
194,168,210
255,243,112

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,h,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,d,d,d,d,b,d,d,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,i,i,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 1
ITM 0 5,6
EXT 5,11 2 5,11
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,h,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,d,d,d,d,b,d,d,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,i,i,i,i,i,i,0,0,0
0,0,0,0,i,b,b,b,b,b,b,b,i,0,0,0
0,0,0,0,i,i,i,i,i,i,i,i,i,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 1a
EXT 8,11 3 8,11
EXT 5,10 0 5,10
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,h,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,d,d,d,d,b,d,d,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,i,i,i,i,i,i,0,0,0
0,0,0,0,i,b,b,b,b,b,b,b,i,0,0,0
0,0,0,0,i,i,i,i,i,b,i,i,i,0,0,0
0,0,0,0,0,0,0,0,0,b,0,0,0,0,0,0
0,0,0,0,0,0,0,0,c,c,c,0,0,0,0,0
0,0,0,0,0,0,0,0,d,d,d,0,0,0,0,0
NAME floor 1b
EXT 9,13 4 9,13
EXT 11,11 7 11,11
EXT 7,11 2 7,11
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,h,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,d,d,d,d,b,d,d,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,0,0,0,0,0,0,0,0,0
0,0,0,0,i,b,i,i,i,i,i,i,i,0,0,0
0,0,0,0,i,b,b,b,b,b,b,b,i,0,0,0
0,0,0,0,i,i,i,i,i,b,i,i,i,0,0,0
0,0,0,0,0,d,d,d,d,b,d,d,d,0,0,0
0,0,0,0,0,e,c,f,c,c,c,g,e,0,0,0
0,0,0,0,0,d,d,d,d,d,d,d,d,0,0,0
NAME floor 1b-a
ITM 2 6,14
ITM 7 7,14
EXT 9,12 3 9,12
EXT 11,14 c 12,4
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,0,0,0,0,0,0
0,e,c,c,c,c,c,e,0,0,d,d,d,d,d,0
0,e,c,h,c,c,c,e,0,0,e,c,c,c,e,0
0,e,c,c,c,c,c,e,0,0,e,g,c,c,e,0
0,e,c,c,c,c,c,e,0,0,e,c,c,c,e,0
0,d,d,d,d,b,d,d,0,0,e,c,c,c,e,0
0,0,0,0,i,b,i,0,0,0,d,b,d,d,d,0
0,0,0,0,i,b,i,0,0,0,i,b,i,0,0,0
0,0,0,0,i,b,i,i,i,i,i,b,i,0,0,0
0,0,0,0,i,b,b,b,b,b,b,b,i,0,0,0
0,0,0,0,i,i,i,i,i,i,i,i,i,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 1c
ITM 0 13,7
EXT 10,11 3 10,11
EXT 11,5 8 7,12
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,i,i,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,d,d,b,d,d,d,0,0,0,0,0,0,0,0,0
d,d,c,c,c,c,d,d,d,d,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,h,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,i,i,i,i,i,0
e,c,c,c,c,c,c,c,c,b,b,b,b,b,i,0
d,d,d,d,d,d,d,d,d,d,i,i,i,i,i,0
NAME floor 2
ITM 8 2,13
ITM 0 5,11
EXT 3,5 9 3,5
EXT 13,14 a 13,14
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
i,i,i,i,i,0,0,0,0,0,0,0,0,0,0,0
i,b,b,b,i,d,d,d,d,0,0,0,0,0,0,0
i,i,i,b,i,e,c,g,e,0,0,0,0,0,0,0
0,0,i,b,b,b,c,c,e,0,0,0,0,0,0,0
0,0,i,b,i,e,c,c,e,0,0,0,0,0,0,0
0,0,i,b,i,e,c,c,e,0,0,0,0,0,0,0
0,0,i,b,i,d,d,d,d,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,d,d,b,d,d,d,0,0,0,0,0,0,0,0,0
d,d,c,c,c,c,d,d,d,d,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,h,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,i,0,0,0,0,0
e,c,c,c,c,c,c,c,c,b,i,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,i,0,0,0,0,0
NAME floor 2a
ITM 5 6,6
EXT 3,6 8 3,6
EXT 3,2 e 3,2
EXT 7,3 d 13,5
PAL 0

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,d,d,d,d,d,d,d,d,0
0,0,0,0,0,0,0,e,c,c,c,c,c,c,e,0
0,0,0,0,0,0,0,e,c,c,c,g,c,c,e,0
0,0,0,0,0,0,0,e,c,c,c,c,c,c,e,0
0,0,0,0,0,0,0,d,d,b,d,c,c,c,e,0
0,0,0,0,0,0,0,0,i,b,e,c,c,c,e,0
0,0,0,0,0,0,0,0,i,i,e,c,c,c,e,0
0,0,0,i,0,0,0,0,0,0,d,d,d,b,d,0
0,d,d,b,d,d,d,0,0,0,0,0,i,b,i,0
d,d,c,c,c,c,d,d,d,d,0,0,i,b,i,0
e,c,c,c,c,c,c,c,c,e,0,0,i,b,i,0
e,c,c,c,c,c,c,h,c,e,0,0,i,b,i,0
e,c,c,c,c,c,c,c,c,e,i,i,i,b,i,0
e,c,c,c,c,c,c,c,c,b,b,b,b,b,i,0
d,d,d,d,d,d,d,d,d,d,i,i,i,i,i,0
NAME floor 2b
EXT 12,14 8 12,14
EXT 9,6 b 9,6
EXT 11,3 f 3,12
PAL 0

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,d,d,d,d,d,d,d,d,0
0,0,0,0,0,0,0,e,c,c,c,c,c,c,e,0
0,0,0,0,0,0,0,e,c,c,c,g,c,c,e,0
0,0,0,0,d,d,d,e,c,c,c,c,c,c,e,0
0,0,0,0,e,c,e,d,d,b,d,c,c,c,e,0
0,0,0,0,e,c,b,b,b,b,e,c,c,c,e,0
0,0,0,0,e,c,e,i,i,i,e,c,c,c,e,0
0,0,0,i,d,d,d,0,0,0,d,d,d,b,d,0
0,d,d,b,d,d,d,0,0,0,0,0,i,b,i,0
d,d,c,c,c,c,d,d,d,d,0,0,i,b,i,0
e,c,c,c,c,c,c,c,c,e,0,0,i,b,i,0
e,c,c,c,c,c,c,h,c,e,0,0,i,b,i,0
e,c,c,c,c,c,c,c,c,e,i,i,i,b,i,0
e,c,c,c,c,c,c,c,c,b,b,b,b,b,i,0
d,d,d,d,d,d,d,d,d,d,i,i,i,i,i,0
NAME floor 2c
ITM 3 5,5
EXT 9,5 a 9,5
PAL 0

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,f,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,h,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,g,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,e,c,c,c,c,c,c,c,c,c,c,c,c,e,0
0,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 3
ITM 0 12,11
ITM 0 4,5
ITM 0 10,12
EXT 3,9 g 11,11
END 0 2,3
PAL 0

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,d,d,d,d,d,d,0
0,0,0,0,0,0,0,0,0,e,c,c,c,c,e,0
0,0,0,0,0,0,0,0,0,e,c,c,c,g,e,0
0,0,0,0,0,0,0,0,0,e,c,c,c,c,e,0
0,0,0,0,0,0,0,0,0,e,c,c,c,c,e,0
0,0,0,0,0,0,0,0,0,d,d,d,d,d,d,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 4
END 1 13,5
PAL 0

ROOM e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
i,i,i,i,i,0,0,0,0,0,0,0,0,0,0,0
i,b,b,b,i,0,0,0,0,0,0,0,0,0,0,0
i,i,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,0,i,b,i,0,0,0,0,0,0,0,0,0,0,0
0,d,d,b,d,d,d,0,0,0,0,0,0,0,0,0
d,d,c,c,c,c,d,d,d,d,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,h,c,e,0,0,0,0,0,0
e,c,c,c,c,c,c,c,c,e,i,0,0,0,0,0
e,c,c,c,c,c,c,c,c,b,i,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,i,0,0,0,0,0
NAME floor 2d
ITM 0 1,2
EXT 3,3 9 3,3
PAL 0

ROOM f
d,d,d,d,d,d,d,0,0,d,d,d,d,d,d,d
e,c,c,c,c,c,e,0,0,e,c,c,c,c,c,e
e,c,c,c,c,c,e,0,0,e,c,c,c,c,c,e
e,c,c,c,c,c,e,0,0,e,c,c,g,c,c,e
e,c,c,c,c,c,d,d,d,d,c,c,c,c,c,e
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
d,d,d,d,c,c,c,c,c,c,c,c,d,d,d,0
0,0,0,e,c,c,c,c,c,c,c,c,e,0,0,0
0,0,0,e,c,c,c,c,c,c,c,c,e,0,0,0
d,d,d,d,c,c,c,c,c,c,c,c,d,d,d,0
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
e,c,c,c,c,c,d,d,d,d,c,c,c,c,c,e
e,c,c,h,c,c,e,0,0,e,c,c,c,c,c,e
e,c,c,c,c,c,e,0,0,e,c,c,c,c,c,e
e,c,c,c,c,c,e,0,0,e,c,c,c,c,c,e
d,d,d,d,d,d,d,0,0,d,d,d,d,d,d,0
NAME floor 5
ITM 0 2,5
EXT 12,3 h 4,5
PAL 0

ROOM g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,i,i,i,i,d,d,d,d,d,d,0,0,0
0,0,0,i,b,b,b,b,c,c,c,c,e,0,0,0
0,0,0,i,i,i,i,e,c,c,c,c,e,0,0,0
0,0,0,0,0,0,0,e,c,c,c,h,e,0,0,0
0,0,0,0,0,0,0,d,d,d,d,d,d,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 6
EXT 4,9 i 4,9
PAL 0

ROOM h
r,r,s,t,r,r,r,r,r,r,r,r,r,r,r,r
u,t,q,q,w,r,r,r,r,r,r,r,r,r,r,r
r,v,q,x,r,r,r,r,r,s,t,r,r,r,r,r
r,r,r,r,r,r,r,r,u,q,q,t,r,r,r,r
r,r,r,s,q,q,t,r,r,r,v,q,w,r,s,t
r,r,s,q,h,q,q,t,s,t,r,r,r,s,g,x
r,r,v,q,q,q,q,q,q,q,t,r,u,q,x,r
r,r,r,v,q,q,q,q,x,v,x,r,r,y,r,r
r,r,u,x,q,q,q,x,r,r,r,r,s,q,r,r
r,r,r,r,v,q,x,r,r,r,u,q,q,x,r,r
r,r,r,r,r,s,q,t,r,r,r,r,u,q,w,r
r,r,r,u,q,q,q,x,u,q,t,s,q,q,t,r
r,r,u,w,r,v,x,r,r,v,q,q,q,q,x,r
r,r,r,r,u,t,r,r,r,r,v,q,q,x,r,r
r,r,r,r,r,v,w,r,r,r,r,v,x,r,r,r
r,r,r,r,r,r,r,r,r,r,r,r,r,r,r,r
NAME floor 7
ITM 9 13,7
EXT 14,5 k 7,14
END 5 2,4
END 5 3,3
END 5 4,3
END 5 5,3
END 5 6,3
END 5 7,4
END 5 1,5
END 5 1,6
END 5 2,7
END 5 1,8
END 5 2,9
END 5 3,9
END 5 4,10
END 5 3,10
END 5 2,11
END 5 1,12
END 5 2,13
END 5 3,13
END 5 4,12
END 5 4,14
END 5 5,15
END 5 6,15
END 5 7,14
END 5 6,13
END 5 7,12
END 5 8,12
END 5 8,4
END 5 9,4
END 5 10,5
END 5 11,6
END 5 11,7
END 5 9,8
END 5 7,9
END 5 8,8
END 5 8,10
END 5 10,8
END 5 9,10
END 5 10,10
END 5 11,10
END 5 9,13
END 5 10,14
END 5 11,15
END 5 12,15
END 5 13,14
END 5 14,13
END 5 15,12
END 5 15,11
END 5 15,10
END 5 14,9
END 5 14,8
END 5 14,7
END 5 15,6
END 5 15,3
END 5 14,3
END 5 13,4
END 5 12,5
END 5 9,9
END 5 11,8
END 5 12,7
PAL 2

ROOM i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,i,i,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,i,d,d,d,d,d,d,0,0,0
0,0,0,i,b,b,b,b,c,c,c,c,e,0,0,0
0,0,0,i,i,i,i,e,c,c,c,c,e,0,0,0
0,0,0,0,0,0,0,e,c,c,c,h,e,0,0,0
0,0,0,0,0,0,0,d,d,d,d,d,d,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 6a
EXT 5,9 g 5,9
EXT 4,3 j 4,3
PAL 0

ROOM j
0,0,0,0,0,d,d,d,d,d,d,d,d,d,d,0
0,0,0,0,0,e,p,j,l,o,o,o,j,m,e,0
0,0,0,i,i,e,o,o,o,o,j,l,m,p,e,0
0,0,0,i,b,b,0,o,o,o,o,o,o,o,e,0
0,0,0,i,b,e,o,o,j,l,m,o,j,m,e,0
0,0,0,i,b,e,k,o,o,o,o,o,o,n,e,0
0,0,0,i,b,e,o,o,p,o,k,j,m,o,e,0
0,0,0,i,b,d,d,d,d,d,d,d,d,d,d,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,b,i,0,0,0,0,0,0,0,0,0,0
0,0,0,i,i,i,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME floor 6b
EXT 4,4 i 4,4
END 2 6,2
END 2 7,3
END 2 6,4
PAL 1

ROOM k
0,0,0,d,d,d,d,d,d,d,d,d,d,0,0,0
0,0,0,e,i,c,c,c,c,c,c,i,e,0,0,0
d,d,d,d,c,c,c,c,c,c,c,c,d,d,d,d
e,i,c,c,c,c,c,c,c,c,c,c,c,c,i,e
e,c,c,c,c,c,c,0,0,c,c,c,c,c,c,e
e,c,c,c,c,c,c,z,10,c,c,c,c,c,c,e
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
e,c,c,c,c,c,c,c,c,c,c,c,c,c,c,e
e,i,c,c,c,c,c,c,c,c,c,c,c,c,i,e
d,d,d,c,c,c,c,c,c,c,c,c,c,d,d,d
0,0,e,i,c,c,c,c,c,c,c,c,i,e,0,0
0,0,d,d,d,c,c,c,c,c,c,d,d,d,0,0
0,0,0,0,e,i,c,h,c,c,i,e,0,0,0,0
0,0,0,0,d,d,d,d,d,d,d,d,0,0,0,0
NAME bottom
ITM a 4,1
ITM a 11,1
ITM a 14,3
ITM a 14,10
ITM a 12,12
ITM a 10,14
ITM a 5,14
ITM a 3,12
ITM a 1,10
ITM a 1,3
END 3 7,4
END 4 8,4
PAL 3

TIL 10
11111100
11111010
00000100
10101000
10101000
10101000
00001000
11111100
WAL true

TIL a
01000000
10011100
10100010
10101010
10101010
10001010
01110010
00000100
>
00111110
01000001
01011100
01000010
00111010
10000010
01111100
00000000
WAL false

TIL b
00000000
00100100
01111110
00100100
00100100
01111110
00100100
00000000
WAL false

TIL c
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000

TIL d
00000000
00000000
00000000
01111110
01111110
00000000
00000000
00000000
WAL true

TIL e
00000000
00011000
00011000
00011000
00011000
00011000
00011000
00000000
WAL true

TIL f
11111111
10000001
10011001
10100101
10000001
10000001
10000001
11111111

TIL g
01111110
10100101
10111101
10100101
10111101
10100101
11000011
01111110

TIL h
00000000
00100100
00111100
00100100
00111100
00100100
00111100
00100100

TIL i
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL j
00000000
00000000
01000111
00011111
00111111
00111111
01000111
00010000

TIL k
00011000
00111100
10111101
11011011
11111111
10111101
11100111
01111110

TIL l
10000000
00111000
11111110
11111111
11111111
11111111
11111111
00111110

TIL m
00000000
10000000
00111001
11111100
11111100
11110000
10000010
00001000

TIL n
01000000
00000001
00011100
01111110
11111111
11111111
01111110
10000000

TIL o
00000000
00010010
01000000
00001000
00100010
00000100
00010000
01000010

TIL p
00010000
00011100
00000111
01111000
11101110
11010111
11101111
01111110

TIL q
00000000
00000100
01000000
00000000
00000000
00000010
00100000
00000000

TIL r
11111111
11111111
10011111
11111111
11111111
11111111
11111001
11111111
>
11111111
11111111
11001111
11111111
11111111
11111111
11110011
11111111

TIL s
11100110
10000000
00010000
00000000
00000000
10000010
10100000
00000000

TIL t
01100111
00000001
00010000
00000000
00000001
00000001
00100000
00000000

TIL u
11100110
10000000
00010000
00000000
10000000
00000010
10000000
11001100

TIL v
00000000
10000100
10000000
00000000
00100000
10000010
10000000
11001100

TIL w
00110011
00000001
01000000
00000100
00000000
00000000
00100001
00000111

TIL x
00000000
00000100
01000001
00000001
00000000
00001000
00000001
01100111

TIL y
10000001
11111111
10000001
11111111
10000001
11111111
10000001
11111111

TIL z
01111111
10111111
01000000
00101010
00101010
00101010
00100000
01111111
WAL true

SPR A
01111110
11111111
11011011
11011011
11111111
11000011
11100111
01111110
POS 0 3,4

SPR a
00000000
01000010
01100110
01111110
01011010
01011010
00111100
00000000
DLG SPR_0
POS 0 4,2

SPR b
01000010
10000001
10111101
11011011
11011011
01111110
01111110
00100100
DLG SPR_1
POS a 13,5

SPR c
01111110
11011011
11011011
11111111
11100111
11000011
11111111
10101010
DLG SPR_2
POS c 5,13

SPR d
01000010
11000011
10111101
11111111
10111101
11011011
11011011
01111110
DLG SPR_3

SPR e
00010000
00011000
00100100
00101100
00011000
00111100
00011000
00001000
>
00000100
00011100
00100100
00110100
00011000
00111100
00011000
00001000

SPR f
11111111
10010101
11111111
10100011
11111111
10001001
11111111
01000010
DLG SPR_4
POS f 12,13

ITM 0
00000000
00111100
01100110
01011010
01011010
01100110
00111100
00000000
NAME coin
DLG ITM_0

ITM 1
00000000
01111110
10100101
11111011
01110010
00110100
00011000
00000000
NAME gem
DLG ITM_1

ITM 2
00000111
00001001
00010101
00101010
11010100
01101000
01110000
10010000
NAME sword
DLG ITM_3

ITM 3
00111100
01111110
01111110
10011001
10000001
11000011
11100111
11100111
NAME armor
DLG ITM_5

ITM 4
00000000
00011000
00111100
00011000
00100100
00100100
00011000
00000000
NAME ring

ITM 5
00000000
00000110
00000110
00001000
00010000
00100000
01000000
00000000
NAME wand
DLG ITM_7

ITM 7
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000
NAME curse
DLG ITM_2

ITM 8
01111110
10111101
11011011
11011011
11111111
10000001
11011011
01111110
NAME dust
DLG ITM_4

ITM 9
01000010
11000011
10111101
11111111
10111101
11011011
11011011
01111110
NAME amulet
DLG ITM_6

ITM a
00000100
00011000
00100100
00101100
00011000
00111100
00011000
00001000
>
00001000
00011000
00100100
00101100
00011000
00111100
00011000
00001000
NAME torch

DLG ITM_0
"""
{shuffle
  - You found a {rbw}bitcoin{rbw}!
    Now you just need a computer!
  - You found a {rbw}nickel{rbw}!
    It's made of nickel!
  - You found a {rbw}plain metal disk{rbw}!
    Oh.
  - You found a {rbw}Hawaii quarter{rbw}!
    Your collection is complete!
  - You found a {rbw}washer{rbw}!
    That's okay!
  - You found a {rbw}tiny Frisbee{rbw}!
    {wvy}Far out!{wvy}
  - You found a {rbw}coin{rbw}!
    How expected!
  - You found a {rbw}gold coin{rbw}!
    That's no small fiat!
  - You found a {rbw}spare part{rbw}!
    You wonder what it goes to!
  - You found a {rbw}Christmas cookie{rbw}!
    You save it for later!
  - You found a {rbw}fidget spinner{rbw}!
    It's quite trendy!
  - You found a {rbw}removable tattoo{rbw}!
    Has it already been removed?
  - You found a {rbw}funny-shaped rock{rbw}!
    Well, that's fine!
  - You found a {rbw}Lifesaver candy{rbw}!
    It's covered in dust!  Yuck!
  - You found a {rbw}sprocket{rbw}!
    You don't know what that is!
  - You found a {rbw}pizza place token{rbw}!
    Finally, gambling for children!
}
"""

DLG ITM_1
"""
{shuffle
  - You found a worthless piece
    of glass.
  - You found a diamond!
}
"""

DLG ITM_2
"""
A trap activates beneath you!

{shuffle
  - You are cursed to never again
    have exact change!
  - You lose 1000 Life Points and
    cannot Special Summon this turn!
  - Your entire body, aside from
    your head, suddenly vanishes!
  - You fall down 7 dungeon levels,
    into an exactly identical room!
  - You may not assemble contraptions
    for three turns!
  - A pile of kittens drops from the
    ceiling!  Oh no!  Too cute!
  - You polymorph into yourself,
    but with an extra nipple!
  - A dart shoots out of the wall!
    You nimbly dodge!
  - A large boulder starts rolling
    towards you!  Oh, it's just foam.
  - You teleport to a random spot,
    which happens to be here!
  - Jets of flame shoot at you!
    You are lightly toasted!
}
"""

DLG ITM_3
"""
{shuffle
  - You found a {rbw}sword{rbw}!
    How plain!
  - You found a {rbw}katana{rbw}!
    It's time to ganbatte!
  - You found a {rbw}rapier{rbw}!
    You can fence it later!
  - You found an {rbw}epee{rbw}!
    Ah!  That crossword answer!
  - You found a {rbw}narrowsword{rbw}!
    It's so skinny!
  - You found {rbw}Tessaiga{rbw}!
    It's a sword that cuts!
  - You found a {rbw}sabre{rbw}!
    Looking sharp!
}
"""

DLG SPR_0
"""
{shuffle
  - To unlock your cat familiar,
    please purchase a DLC pass!
  - Your cat will follow you at
    loyalty level: {clr2}73{clr2}
    Current loyalty level: {clr2}-2{clr2}

  - {shk}AOOOWRRR!!!!{shk}

  - The cat is busily grooming its
    own butthole.  Yuck!
  - You beckon the cat.
    The cat extremely doesn't care.
  - You pet the cat.
    The cat grants you 1 purr.
  - The cat bats at the tassles on
    your cloak.
  - The cat yawns.
    It's really cute.
  - {
      - wisdom == 1 ?
        The cat whispers ancient feline
        secrets to you.
        You are enlightened.

    }
}
"""

DLG ITM_4
"""
{shuffle
  - The vampire hisses!
    You hiss back!  She runs away!
  - Your sunny disposition turns the
    vampire to dust!
  - You scream!  Your garlic breath
    turns the vampire to dust!
  - You exploit the vampire's sole
    weakness: sharp things!
  - You and the vampire become good
    friends!
    She has to go for now, but you
    promise to write each other!
  - This isn't a real vampire!
    It's a cardboard prop!
}
"""

DLG ITM_5
"""
You found the {rbw}Magneto helm{rbw}!
You develop magnet brain powers!
"""

DLG SPR_1
"""
{shuffle
  - You strike the lich!
    0 damage!
    The lich touches you.
    0 damage.
  - You deal a piercing blow!
    73 damage!
    The lich casts Healaga!
    74 health restored!
  - You swing at the lich and miss.
    
    The lich swings at you and
    misses.
}
"""

DLG SPR_2
"""
{shuffle
  - {wvy}oOOoOOOooOOooOoOoOOOooOooOOooOoo{wvy}
  - {wvy}i was once like you{wvy}
    
    {wvy}now i'm dead and sad{wvy}
  - {wvy}please don't take my money{wvy}
  - {wvy}i don't suppose you have
    a phoenix down on you{wvy}
  - {wvy}beware the dragon's deadly
    fire breath{wvy}
    {wvy}find the legendary ice rod
    if you can{wvy}
  - {wvy}how did i die?
    um wow that's kind of private{wvy}
  - {wvy}i was killed by exodia, so,
    i can't feel too bad i guess{wvy}
  - {wvy}i'm the ghost of christmas
    future{wvy}
    {wvy}you will die before christmas{wvy}
    
    {wvy}i mean...
    some christmas, eventually{wvy}
}
"""

DLG SPR_3
"""
{
  - {item "coin"} * {item "armor"} > 0 ?
    i see you have all the stuff
  - else ?
    aha!!  you lack a thing
}
"""

DLG ITM_6
"""
This is it!  No turning back!
You attack the {clr2}Demon Lord{clr2}!!
{
  - {item "coin"} * {item "wand"} >= 5 ?
    You try to bribe the {clr2}Demon Lord{clr2}!

    The {clr2}Demon Lord{clr2} demands at least
    4000 {rbw}gold coins{rbw}!
    You turn around for a moment and
    use your {rbw}magic wand{rbw} to turn the
    garbage you've collected into
    4000 {rbw}gold coins{rbw}!
    The {clr2}Demon Lord{clr2} accepts and goes
    on vacation, leaving her {rbw}amulet{rbw}!
  - wisdom > 0 ?
    You challenge the {clr2}Demon Lord{clr2}
    to a battle of wits!
    She accepts!  The battlefield is
    Legends of the Hidden Temple!
    The Demon Lord attempts to
    assemble the {rbw}silver monkey{rbw}!
    She runs out of time!

    With {rbw}ancient cat wisdom{rbw}, the
    {rbw}silver monkey{rbw} is a breeze!
    The {clr2}Demon Lord{clr2} begrudgingly
    gives you her {rbw}amulet{rbw}!
  - else ?
    {
      - {item "coin"} >= 1 ?
        You try to bribe the {clr2}Demon Lord{clr2}!

        The {clr2}Demon Lord{clr2} demands at least
        4000 {rbw}gold coins{rbw}!
        You meekly offer the worthless
        junk you've found!
        The {clr2}Demon Lord{clr2} smacks it out of
        your hand!
        How embarrassing!
        {shk}2 pride damage!!{shk}
      - else ?
        {shuffle
          - You attack with pocket sand!

            The {clr2}Demon Lord{clr2}'s eyes are made
            of pure darkness!  No effect!
          - You do a cool pose!
            ATK increases by 6!
          - You summon your faithful cat!
            Your cat runs away!  Ugh!
          - You eat a banana!
            Potassium is very important!
        }
    }
    {shuffle
      - The {clr2}Demon Lord{clr2} casts a curse on
        you!  You stub your toe!  Ouch!
      - The {clr2}Demon Lord{clr2} makes you do her
        taxes!  Augh!  It's awful!
      - The {clr2}Demon Lord{clr2} challenges you
        to a round of Overwatch!
        You run Linux and don't even
        know what that is!
      - The {clr2}Demon Lord{clr2} opens her maw
        and releases a horde of flies!
        Gross!  You offer her a breath
        mint!
    }
    {
      - {item "wand"} > 0 ?
        You wave your {rbw}magic wand{rbw} at the
        {clr2}Demon Lord{clr2}!
        {shuffle
          - But it's plastic!  No effect!

          - But it has no charges left!
            That sucks!
          - Flowers sprout from the tip!
            The {clr2}Demon Lord{clr2} applauds!
          - The {clr2}Demon Lord{clr2} grows to twice
            her original size!  Oh no!
          - It's a {rbw}wand of death{rbw}!  Score!
            The {clr2}Demon Lord{clr2} collapses!
            Suddenly, the {clr2}Demon Lord{clr2}'s
            twin sister appears!  Oh no!
          - Somehow you miss!  You're at
            point blank range!  Come on!
          - The {clr2}Demon Lord{clr2} turns invisible!
            Luckily, only for a moment!
          - The {clr2}Demon Lord{clr2} polymorphs into
            a Greater {clr2}Demon Lord{clr2}!  Augh!
        }
      - else ?
        {shuffle
          - You kick the {clr2}Demon Lord{clr2}'s shins!

            She yelps and hops around a bit!
            {shk}2 damage!!{shk}
          - You do finger guns at the {clr2}Demon
            Lord{clr2}!
            The {clr2}Demon Lord{clr2} is immune to
            finger bullets!
          - You drink a {rbw}potion of soda{rbw}!
            Delicious and refreshing!
          - You get distracted checking your
            phone and miss your turn!
          - You flex your muscles!

            The {clr2}Demon Lord{clr2} laughs at you!
            {shk}7 pride damage!!{shk}
        }
    }
    {shuffle
      - The {clr2}Demon Lord{clr2} shoots lightning
        at you!  {shk}That hurts!{shk}
      - The {clr2}Demon Lord{clr2} summons a
        meteor from beyond the stars!
        But you're both underground, so
        it doesn't hit you!
      - The {clr2}Demon Lord{clr2} sneezes!
        {shk}Gross!{shk}
      - The {clr2}Demon Lord{clr2} blasts you with
        icicle spears!
        They instantly evaporate in the
        scorching heat!
      - The {clr2}Demon Lord{clr2} summons Exodia!

        You respond with Super Exodia!
        Nice move!
      - The {clr2}Demon Lord{clr2} snarls!
        Roll an intimidation check!
        ...
        Natural 20!  Sweet!
    }
    {
      - {item "sword"} > 0 ?
        You remember you have a sword!
        You swing at the {clr2}Demon Lord{clr2}!
        {shuffle
          - You totally miss!
            Wow, you suck at this!
          - You score a critical hit!
            {shk}712 damage!!{shk}
            Damn!  How much health does she
            have, anyway?!
          - Direct hit to the appendix!
            {shk}0 damage!!{shk}
          - The {clr2}Demon Lord{clr2} has +69 resist to
            sword!  Nice!  Wait, no, bad!
          - The {clr2}Demon Lord{clr2} catches your
            sword in its sheath!
            It's just like Kill Bill and
            very badass!  Wow!
        }
      - else ?
        {shuffle
          - You scratch at the {clr2}Demon Lord{clr2}'s
            eyes!
            But you just cut your nails
            yesterday!  {shk}0 damage!!{shk}
          - You punch the {clr2}Demon Lord{clr2}!
            {shk}84 damage!!{shk}
            Wow, surprisingly effective!

          - You pick your nose!
            No effect!
          - You throw a rock at the {clr2}Demon
            Lord{clr2}!  {shk}Boring!{shk}
        }
    }
    {shuffle
      - The {clr2}Demon Lord{clr2} bathes in the
        light of the netherworld!
        You're not sure what that does,
        but it's probably bad!
      - The {clr2}Demon Lord{clr2} summons your own
        shadow to fight you!
        It's a close battle, but you
        manage to defeat it!
      - The {clr2}Demon Lord{clr2} fans herself!
        It's really hot down here!
      - The {clr2}Demon Lord{clr2} zaps you with a
        {rbw}wand of death{rbw}!
        Phew!  It was out of charges!

    }
    {
      - {item "helm"} > 0 ?
        You put on your helmet and use
        your {rbw}magnet brain powers{rbw}!
        {shuffle
          - But there's no metal down here!
            Good going!
          - You launch the helm at the
            {clr2}Demon Lord{clr2}!  {shk}2 damage!!{shk}
          - The {clr2}Demon Lord{clr2} launches into a
            spiel about scientists and lies!
          - The magnetic waves erase your
            phone!  {shk}Auggh!!{shk}
        }
      - else ?
        {shuffle
          - You headbutt the {clr2}Demon Lord{clr2}!
            Both of you are a bit sore now!
          - You rush the {clr2}Demon Lord{clr2}, but
            trip on a rock!  Oops!
          - You find the {clr2}Demon Lord{clr2}'s old
            DeviantArt account and pass it
            around on Twitter!
            {shk}17 pride damage!!{shk}
          - You spit at the {clr2}Demon Lord{clr2}!
            Uh, nasty!  Maybe don't!
        }
    }
    {shuffle
      - The {clr2}Demon Lord{clr2} writes a callout
        post about you!
        Now everyone thinks you eat
        belly button lint!  {shk}Augh!!{shk}
      - The {clr2}Demon Lord{clr2} summons a horde
        of evil bats!
        But they're evil fruit bats!
        No effect!
      - The {clr2}Demon Lord{clr2} fidgets with her
        cape a bit!  It really chafes!
      - The {clr2}Demon Lord{clr2} opens a portal
        to Hell!
        It's not much different from
        the cave you're in already!
    }
    {shuffle
      - You and the {clr2}Demon Lord{clr2} are
        exhausted from fighting!
        You agree to a truce, but palm
        the {rbw}amulet{rbw}!  {shk}Sneaky!{shk}
      - This is so long that the
        {clr2}Demon Lord{clr2} dies of old age!
        Good enough!  You take the
        {rbw}amulet{rbw}!
      - You attack the {clr2}Demon Lord{clr2} with
        everything you have!
        Bam!  Zap!  Kablooie!  Piff!
        Kerpow!  Boom!  Shakalaka!
        You are victorious!  You claim
        the {rbw}amulet{rbw}!
      - You enter {rbw}Wizard mode{rbw}!
        You delete the {clr2}Demon Lord{clr2}!
        I guess that works!
        The {rbw}amulet{rbw} is yours!
    }
}
"""

DLG SPR_4
"""
{sequence
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
  - It's just a sign.  It says the
    same thing every time.
  - Adventurers beware!
    Ahead lies the Demon Lord!
    Those who seek the {rbw}amulet of
    power{rbw} do so at their own peril!
}
"""

DLG ITM_7
"""
You found a {rbw}magic wand{rbw}!
{shuffle
  - It sparkles in the dim light!
  - You wonder what it does!
  - You try to cast Lumos!  {wvy}Nerd!{wvy}
  - Unless it's an unmagic wand!
  - This will make great cosplay!
  - It whispers dark secrets!
}
"""

END 0
A trap activates beneath you!  A large boxing glove on a spring punches you clear out of the dungeon!  Guess you'll have to start over...

END 1
Looks like the dungeon generation made a dead end.  Dang!

END 2
WOW!  You hit the jackpot!  As a millennial, you've never seen so much money in your whole life.  You gather it up and hurry home to become a sad billionaire who trolls Twitter all day.

END 3
You place the demon lord's amulet on the altar and sacrifice it to your god, who rewards you with a lifetime subscription to Teen Vogue!  Nice job!

END 4
You place the demon lord's amulet on the altar and sacrifice it to your god, who rewards you with only needing to go to the dentist once a year!  Awesome!

END 5
You take a dip in the lava.  It's quite refreshing!  You swim away from this nonsense and explore the lava oceans.

VAR wisdom
0


