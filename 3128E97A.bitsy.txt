Answer Your Bloomin' Phone                                         After having never answered your phone, you decide one day to take the plunge and listen to all those voicemails...

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
45,80,130
101,126,201
255,10,2

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,h,h,h,h,h,h,h,h,h,h,h,h,d,0
0,f,b,j,n,n,p,b,b,b,j,n,n,p,g,0
0,f,b,k,14,15,o,b,b,b,k,0,0,o,g,0
0,f,b,k,b,b,o,b,b,b,k,0,0,o,g,0
0,f,b,k,b,b,o,b,b,b,l,m,m,q,g,0
0,f,b,k,b,b,o,b,b,b,b,b,b,b,g,0
0,f,b,k,b,b,o,b,b,b,b,b,b,b,g,0
0,f,b,k,b,b,o,b,b,r,b,s,b,t,g,0
0,f,b,k,b,b,o,b,b,b,b,b,b,b,g,0
0,f,b,k,b,b,o,b,b,w,b,x,b,y,g,0
0,f,b,k,b,b,o,b,b,b,b,b,b,b,g,0
0,f,b,k,13,16,o,b,b,z,b,10,b,12,g,0
0,f,b,l,m,m,q,b,b,b,b,b,b,b,g,0
0,c,i,i,i,i,i,i,i,i,i,i,i,i,e,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Room 1
PAL 0

TIL 10
10000001
00111100
01100010
01001010
01010010
01000110
00111100
10000001

TIL 12
10000001
00111100
01000110
01000110
01110110
01110110
00111100
10000001

TIL 13
11111111
11111111
11111111
11111111
11111111
11111111
01111111
00111111

TIL 14
00111111
01111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL 15
11111100
11111110
11111111
11111111
11111111
11111111
11111111
11111111

TIL 16
11111111
11111111
11111111
11111111
11111111
11111111
11111110
11111100

TIL a
00111011
01111110
11111111
11111111
01111111
10111111
01111111
11111111

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL c
01111111
10111111
01111111
11111111
11111111
11111111
01111011
00110101

TIL d
10101100
11010110
11111111
11111111
11111111
11111101
11111110
11111111

TIL e
11111110
11111111
11111111
11111110
11111101
11111111
11011110
10101100

TIL f
01111111
10111111
01111111
11111111
10111111
01111111
10111111
11111111

TIL g
11111110
11111101
11111110
11111101
11111111
11111100
11111111
11111110

TIL h
01101011
11010101
11111111
11111111
11111111
11111111
11111111
11111111

TIL i
11111111
11111111
11111111
11111111
11111111
11111111
11101101
01010010

TIL j
11111111
11110000
11100000
11000111
10001100
10011000
10010000
10010000

TIL k
10010000
10010000
10010000
10010000
10010000
10010000
10010000
10010000

TIL l
10010000
10010000
10011000
10001100
11000111
11100000
11110000
11111111

TIL m
00000000
00000000
00000000
00000000
11111111
00000000
00000000
11111111

TIL n
11111111
00000000
00000000
11111111
00000000
00000000
00000000
00000000

TIL o
00001001
00001001
00001001
00001001
00001001
00001001
00001001
00001001

TIL p
11111111
00001111
00000111
11100011
00110001
00011001
00001001
00001001

TIL q
00001001
00001001
00011001
00110001
11100011
00000111
00001111
11111111

TIL r
10000001
00111100
01100110
01110110
01110110
01100010
00111100
10000001

TIL s
10000001
00111100
01100110
01010110
01101110
01100010
00111100
10000001

TIL t
10000001
00111100
01100110
01010110
01100110
01010110
00111100
10000001

TIL w
10000001
00111100
01011110
01010110
01000010
01110110
00111100
10000001

TIL x
10000001
00111100
01000110
01011110
01100110
01000110
00111100
10000001

TIL y
10000001
00111100
01100110
01011110
01000110
01000110
00111100
10000001

TIL z
10000001
00111100
01000010
01111010
01110110
01101110
00111100
10000001

SPR A
00100000
00100000
00100000
10101010
01111111
00111111
00111111
00011110
POS 0 11,15

SPR a
11111111
11111111
11111111
11111111
11111111
11111111
01111111
00111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_0
POS 0 11,4

SPR b
00111111
01111111
11111111
11111111
11111111
11111111
11111111
11111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_1
POS 0 11,3

SPR c
11111100
11111110
11111111
11111111
11111111
11111111
11111111
11111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_2
POS 0 12,3

SPR d
11111111
11111111
11111111
11111111
11111111
11111111
11111110
11111100
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_3
POS 0 12,4

SPR e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 0 0,0

ITM 0
11111100
11111110
11011111
11011111
11001111
11101111
11111111
11111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME blinker
DLG ITM_0

ITM 1
00111111
01111111
11110111
11000111
11101111
11111111
11111111
11111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

ITM 2
11111111
11110111
11110111
11110111
11111111
11111111
01111111
00111111
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

ITM 4
11111111
11111111
11111111
11100111
11101111
11001111
11111110
11111100
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

DLG ITM_0
"""
{shuffle
  - Hey it's
  - Hi there, it's
  - Yo! It's
  - Hello friend, it's
  - To whomever it may concern, it's
  - OMG! It's
  - Belated birthday greetings! It's
  - Wasssssssuuuup! It's
  - Look who's calling you! It's ya boi
  - Salutations! It's 
  - Sup! It's
  - Hey sweety, it's 
  - Yo gorgeous, it's 
  - Hey beautiful, it's 
  - Ooo arr! It's
  - Avast! It be 
  - {clr3}{shk}*heavy breathing*{shk}{clr3} Hey, it's
 - Hail! It's
} {shuffle
  - Dave from 
  - Vlad from 
  - Stacy from 
  - Carole from 
  - Terrance from
  - Laurence from
  - Holly from
  - Jezza from 
  - Mandy from 
  - Elijah from
  - Grace from 
  - Pauline from 
  - Hercules from 
  - Martha from 
  - Mona from 
  - Marcy from
  - Data from
  - Clara from
  - Adam from
  - Deborah from 
  - Jason from
  - Luke from
  - Ezekiel from
  - Nelson from 
  - Del Boy from
  - Jake from 
  - The Rock from
  - Ash from 
  - Moana from
  - Dwayne Dibley from 
  - Agent Smith from
  - The One from
  - the spider from
  - the Pirate King
 - some random chef TV personality from
 - Arthur, King of the Brits, from 
 - King Harold from
 - some Welsh bloke from
 - Sir Edmund Hillary from
} {shuffle
  - the gardening shop.
  - the police academy.
  - the White House.
  - time and space.
  - the garage.
  - the guild. 
  - you-know-where.
  - mercenary club.
  - the danceathon.
  - the gun range.
  - gun huggers club.
  - muggers R' us. 
  - the zombie defence league. 
  - the sandwich disco. 
  - the giant hill behind your house. 
  - the spider web under your stairs. 
  - the badass pirate ship in the bay. 
  - the floating castle in the sky.
  - the car show.
  - the plane ride over.
  - the fine-dining cable TV show: {clr3}{wvy}"That Chef's Literally on Fire!"{wvy}{clr3}
 - the year 1066.
 - Camelot.
 - Wales. 
 - up another bloody mountain.
} {shuffle
  - I'm calling about 
  - I just had a question about
  - I needed to know about
  - Y'all heard about
 - Have you been informed about
 - Are you aware about 
 - I just needed to tell you about
 - I had a thought about
 - Perhaps we could get lunch and talk about 
 - I be sending ye a wee message in this portable bottle doohicky about 
 - I say old sport, I'm here to talk about 
 - There's troubling rumours about
 - Have you seen that show about 
 - I need to discuss with you about 
 - There's this strange thing about
 - I have so many questions about 
 - Have you heard the one about
 - Are you ever scared about
 - Would you possibly care about
 - I'm worried about 
 - Don't tell anyone about 
 - Have I ever told you about
 - I'd like to mansplain about
 - I'm fed up about
 - I'm really bothered about
 - I hate everything about 
 - I love everything about 
 - I can't stand knowing about 
 - 
} {shuffle
  - the... incident...
  - that job
  - your favourite type of fruit
  - the memes
} {shuffle
  - !
  - .
  - ?
  - ?!
}
"""

DLG SPR_0
"""
{shuffle
  - Hey it's
  - Hi there, it's
  - Yo! It's
  - Hello friend, it's
  - OMG! It's
  - Wasssssssuuuup! It's
  - Look who's calling you! It's ya boi
  - Salutations! It's
  - Sup! It's
  - Hey sweety, it's
  - Yo gorgeous, it's
  - Hey beautiful, it's
  - Ooo arr! It's
  - Avast! It be
  - {clr3}{shk}*heavy breathing*{shk}{clr3} Hey, it's
 - Hail! It's
 - Happy birthday! It's
} {shuffle
  - Dave from
  - Vlad from
  - Stacy from
  - Carole from
  - Terrance from
  - Laurence from
  - Holly from
  - Jezza from
  - Mandy from
  - Elijah from
  - Grace from
  - Pauline from
  - Hercules from
  - Martha from
  - Mona from
  - Marcy from
  - Data from
  - Clara from
  - Adam from
  - Deborah from
  - Jason from
  - Luke from
  - Ezekiel from
  - Nelson from
  - Del Boy from
  - Jake from
  - The Rock from
  - Ash from
  - Moana from
  - Dwayne Dibley from
  - Agent Smith from
  - The One from
  - the spider from
  - the Pirate King from
 - some random chef TV personality from
 - Arthur, King of the Brits, from
 - King Harold from
 - some Welsh bloke from
 - Sir Edmund Hillary from
 - Gandalf from
 - the hairy mole from
} {shuffle
  - the gardening shop.
  - the police academy.
  - the White House.
  - time and space.
  - the garage.
  - the guild.
  - you-know-where.
  - mercenary club.
  - the danceathon.
  - the gun range.
  - the Gun Huggers club.
  - Muggers R' Us.
  - the zombie defence league.
  - the sandwich disco.
  - the giant hill behind your house.
  - the spider web under your stairs.
  - the badass pirate ship in the bay.
  - the floating castle in the sky.
  - the car show.
  - the plane ride over.
  - the fine-dining cable TV show: {clr3}{wvy}"That Chef's Literally on Fire!"{wvy}{clr3}
 - the year 1066.
 - Camelot.
 - Wales.
 - up another bloody mountain.
 - the Shire.
 - your backside.
 - the moon.
 - the elevator earlier.
} {shuffle
  - I'm calling about
  - I just had a question about
  - I needed to know about
  - Y'all heard about
 - Have you been informed about
 - Are you aware about
 - I just needed to tell you about
 - I had a thought about
 - Perhaps we could get lunch and talk about
 - I be sending ye a wee message in this portable bottle doohicky about
 - I say old sport, I'm here to talk about
 - There's troubling rumours about
 - Have you seen that show about
 - I need to discuss with you about
 - There's this strange thing about
 - I have so many questions about
 - Have you heard the one about
 - Are you ever scared about
 - Would you possibly care about
 - I'm worried about
 - Don't tell anyone about
 - Have I ever told you about
 - I'd like to mansplain about
 - I'm fed up hearing about
 - I'm really bothered about
 - I hate everything about
 - I love everything about
 - I can't stand knowing about
} {shuffle
  - the incident...
  - that job
  - your favourite type of fruit
  - the memes
 - that one really annoying advert
 - the time you fought Godzilla
 - the time you drank my juice
 - that boxing championship you won
 - that huge ass car outside my house
 - where you find such quality squid?
 - your favourite games
 - your most hated voice
 - the huge wodge of cash in my pocket
 - the camping trip we're planning
 - the drive to Antarctica
 - about having a sleep over
 - spooning
 - cuddling
 - puppies
 - kittens
 - stupidley cute spiders
 - sharks
 - the fine lass I left behind with me peg leg
 - my face
 - how to get down a mountain
 - the time you fought back all those wasps
 - the time I was shot in the eye with an arrow
 - my first true love
 - how you need to take the ring to Mordor
 - how I talk to women
 - how I talk to men
 - how I talk to people
 - those two headed donkeys
 - that time you had the best dream of your life
 - how you complete me
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
} {shuffle
 - Call me
 - Speak soon
 - Can't wait to talk
 - Ciao for now
 - Speak to me
 - Love you
 - Byesies
 - Bye
 - So long sucker
 - Don't be a stranger
 - Answer me
 - Bye for now, BFF
 - It's been too long
 - When did we last speak
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
}
"""

DLG SPR_1
"""
{shuffle
  - Hey it's
  - Hi there, it's
  - Yo! It's
  - Hello friend, it's
  - OMG! It's
  - Wasssssssuuuup! It's
  - Look who's calling you! It's ya boi
  - Salutations! It's
  - Sup! It's
  - Hey sweety, it's
  - Yo gorgeous, it's
  - Hey beautiful, it's
  - Ooo arr! It's
  - Avast! It be
  - {clr3}{shk}*heavy breathing*{shk}{clr3} Hey, it's
 - Hail! It's
 - Happy birthday! It's
} {shuffle
  - Dave from
  - Vlad from
  - Stacy from
  - Carole from
  - Terrance from
  - Laurence from
  - Holly from
  - Jezza from
  - Mandy from
  - Elijah from
  - Grace from
  - Pauline from
  - Hercules from
  - Martha from
  - Mona from
  - Marcy from
  - Data from
  - Clara from
  - Adam from
  - Deborah from
  - Jason from
  - Luke from
  - Ezekiel from
  - Nelson from
  - Del Boy from
  - Jake from
  - The Rock from
  - Ash from
  - Moana from
  - Dwayne Dibley from
  - Agent Smith from
  - The One from
  - the spider from
  - the Pirate King from
 - some random chef TV personality from
 - Arthur, King of the Brits, from
 - King Harold from
 - some Welsh bloke from
 - Sir Edmund Hillary from
 - Gandalf from
 - the hairy mole from
} {shuffle
  - the gardening shop.
  - the police academy.
  - the White House.
  - time and space.
  - the garage.
  - the guild.
  - you-know-where.
  - mercenary club.
  - the danceathon.
  - the gun range.
  - the Gun Huggers club.
  - Muggers R' Us.
  - the zombie defence league.
  - the sandwich disco.
  - the giant hill behind your house.
  - the spider web under your stairs.
  - the badass pirate ship in the bay.
  - the floating castle in the sky.
  - the car show.
  - the plane ride over.
  - the fine-dining cable TV show: {clr3}{wvy}"That Chef's Literally on Fire!"{wvy}{clr3}
 - the year 1066.
 - Camelot.
 - Wales.
 - up another bloody mountain.
 - the Shire.
 - your backside.
 - the moon.
 - the elevator earlier.
} {shuffle
  - I'm calling about
  - I just had a question about
  - I needed to know about
  - Y'all heard about
 - Have you been informed about
 - Are you aware about
 - I just needed to tell you about
 - I had a thought about
 - Perhaps we could get lunch and talk about
 - I be sending ye a wee message in this portable bottle doohicky about
 - I say old sport, I'm here to talk about
 - There's troubling rumours about
 - Have you seen that show about
 - I need to discuss with you about
 - There's this strange thing about
 - I have so many questions about
 - Have you heard the one about
 - Are you ever scared about
 - Would you possibly care about
 - I'm worried about
 - Don't tell anyone about
 - Have I ever told you about
 - I'd like to mansplain about
 - I'm fed up hearing about
 - I'm really bothered about
 - I hate everything about
 - I love everything about
 - I can't stand knowing about
} {shuffle
  - the incident...
  - that job
  - your favourite type of fruit
  - the memes
 - that one really annoying advert
 - the time you fought Godzilla
 - the time you drank my juice
 - that boxing championship you won
 - that huge ass car outside my house
 - where you find such quality squid?
 - your favourite games
 - your most hated voice
 - the huge wodge of cash in my pocket
 - the camping trip we're planning
 - the drive to Antarctica
 - about having a sleep over
 - spooning
 - cuddling
 - puppies
 - kittens
 - stupidley cute spiders
 - sharks
 - the fine lass I left behind with me peg leg
 - my face
 - how to get down a mountain
 - the time you fought back all those wasps
 - the time I was shot in the eye with an arrow
 - my first true love
 - how you need to take the ring to Mordor
 - how I talk to women
 - how I talk to men
 - how I talk to people
 - those two headed donkeys
 - that time you had the best dream of your life
 - how you complete me
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
} {shuffle
 - Call me
 - Speak soon
 - Can't wait to talk
 - Ciao for now
 - Speak to me
 - Love you
 - Byesies
 - Bye
 - So long sucker
 - Don't be a stranger
 - Answer me
 - Bye for now, BFF
 - It's been too long
 - When did we last speak
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
}
"""

DLG SPR_2
"""
{shuffle
  - Hey it's
  - Hi there, it's
  - Yo! It's
  - Hello friend, it's
  - OMG! It's
  - Wasssssssuuuup! It's
  - Look who's calling you! It's ya boi
  - Salutations! It's
  - Sup! It's
  - Hey sweety, it's
  - Yo gorgeous, it's
  - Hey beautiful, it's
  - Ooo arr! It's
  - Avast! It be
  - {clr3}{shk}*heavy breathing*{shk}{clr3} Hey, it's
 - Hail! It's
 - Happy birthday! It's
} {shuffle
  - Dave from
  - Vlad from
  - Stacy from
  - Carole from
  - Terrance from
  - Laurence from
  - Holly from
  - Jezza from
  - Mandy from
  - Elijah from
  - Grace from
  - Pauline from
  - Hercules from
  - Martha from
  - Mona from
  - Marcy from
  - Data from
  - Clara from
  - Adam from
  - Deborah from
  - Jason from
  - Luke from
  - Ezekiel from
  - Nelson from
  - Del Boy from
  - Jake from
  - The Rock from
  - Ash from
  - Moana from
  - Dwayne Dibley from
  - Agent Smith from
  - The One from
  - the spider from
  - the Pirate King from
 - some random chef TV personality from
 - Arthur, King of the Brits, from
 - King Harold from
 - some Welsh bloke from
 - Sir Edmund Hillary from
 - Gandalf from
 - the hairy mole from
} {shuffle
  - the gardening shop.
  - the police academy.
  - the White House.
  - time and space.
  - the garage.
  - the guild.
  - you-know-where.
  - mercenary club.
  - the danceathon.
  - the gun range.
  - the Gun Huggers club.
  - Muggers R' Us.
  - the zombie defence league.
  - the sandwich disco.
  - the giant hill behind your house.
  - the spider web under your stairs.
  - the badass pirate ship in the bay.
  - the floating castle in the sky.
  - the car show.
  - the plane ride over.
  - the fine-dining cable TV show: {clr3}{wvy}"That Chef's Literally on Fire!"{wvy}{clr3}
 - the year 1066.
 - Camelot.
 - Wales.
 - up another bloody mountain.
 - the Shire.
 - your backside.
 - the moon.
 - the elevator earlier.
} {shuffle
  - I'm calling about
  - I just had a question about
  - I needed to know about
  - Y'all heard about
 - Have you been informed about
 - Are you aware about
 - I just needed to tell you about
 - I had a thought about
 - Perhaps we could get lunch and talk about
 - I be sending ye a wee message in this portable bottle doohicky about
 - I say old sport, I'm here to talk about
 - There's troubling rumours about
 - Have you seen that show about
 - I need to discuss with you about
 - There's this strange thing about
 - I have so many questions about
 - Have you heard the one about
 - Are you ever scared about
 - Would you possibly care about
 - I'm worried about
 - Don't tell anyone about
 - Have I ever told you about
 - I'd like to mansplain about
 - I'm fed up hearing about
 - I'm really bothered about
 - I hate everything about
 - I love everything about
 - I can't stand knowing about
} {shuffle
  - the incident...
  - that job
  - your favourite type of fruit
  - the memes
 - that one really annoying advert
 - the time you fought Godzilla
 - the time you drank my juice
 - that boxing championship you won
 - that huge ass car outside my house
 - where you find such quality squid?
 - your favourite games
 - your most hated voice
 - the huge wodge of cash in my pocket
 - the camping trip we're planning
 - the drive to Antarctica
 - about having a sleep over
 - spooning
 - cuddling
 - puppies
 - kittens
 - stupidley cute spiders
 - sharks
 - the fine lass I left behind with me peg leg
 - my face
 - how to get down a mountain
 - the time you fought back all those wasps
 - the time I was shot in the eye with an arrow
 - my first true love
 - how you need to take the ring to Mordor
 - how I talk to women
 - how I talk to men
 - how I talk to people
 - those two headed donkeys
 - that time you had the best dream of your life
 - how you complete me
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
} {shuffle
 - Call me
 - Speak soon
 - Can't wait to talk
 - Ciao for now
 - Speak to me
 - Love you
 - Byesies
 - Bye
 - So long sucker
 - Don't be a stranger
 - Answer me
 - Bye for now, BFF
 - It's been too long
 - When did we last speak
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
}
"""

DLG SPR_3
"""
{shuffle
  - Hey it's
  - Hi there, it's
  - Yo! It's
  - Hello friend, it's
  - OMG! It's
  - Wasssssssuuuup! It's
  - Look who's calling you! It's ya boi
  - Salutations! It's
  - Sup! It's
  - Hey sweety, it's
  - Yo gorgeous, it's
  - Hey beautiful, it's
  - Ooo arr! It's
  - Avast! It be
  - {clr3}{shk}*heavy breathing*{shk}{clr3} Hey, it's
 - Hail! It's
 - Happy birthday! It's
} {shuffle
  - Dave from
  - Vlad from
  - Stacy from
  - Carole from
  - Terrance from
  - Laurence from
  - Holly from
  - Jezza from
  - Mandy from
  - Elijah from
  - Grace from
  - Pauline from
  - Hercules from
  - Martha from
  - Mona from
  - Marcy from
  - Data from
  - Clara from
  - Adam from
  - Deborah from
  - Jason from
  - Luke from
  - Ezekiel from
  - Nelson from
  - Del Boy from
  - Jake from
  - The Rock from
  - Ash from
  - Moana from
  - Dwayne Dibley from
  - Agent Smith from
  - The One from
  - the spider from
  - the Pirate King from
 - some random chef TV personality from
 - Arthur, King of the Brits, from
 - King Harold from
 - some Welsh bloke from
 - Sir Edmund Hillary from
 - Gandalf from
 - the hairy mole from
 - that f***ing guy from
} {shuffle
  - the gardening shop.
  - the police academy.
  - the White House.
  - time and space.
  - the garage.
  - the guild.
  - you-know-where.
  - mercenary club.
  - the danceathon.
  - the gun range.
  - the Gun Huggers club.
  - Muggers R' Us.
  - the zombie defence league.
  - the sandwich disco.
  - the giant hill behind your house.
  - the spider web under your stairs.
  - the badass pirate ship in the bay.
  - the floating castle in the sky.
  - the car show.
  - the plane ride over.
  - the fine-dining cable TV show: {clr3}{wvy}"That Chef's Literally on Fire!"{wvy}{clr3}
 - the year 1066.
 - Camelot.
 - Wales.
 - up another bloody mountain.
 - the Shire.
 - your backside.
 - the moon.
 - the elevator earlier.
} {shuffle
  - I'm calling about
  - I just had a question about
  - I needed to know about
  - Y'all heard about
 - Have you been informed about
 - Are you aware about
 - I just needed to tell you about
 - I had a thought about
 - Perhaps we could get lunch and talk about
 - I be sending ye a wee message in this portable bottle doohicky about
 - I say old sport, I'm here to talk about
 - There's troubling rumours about
 - Have you seen that show about
 - I need to discuss with you about
 - There's this strange thing about
 - I have so many questions about
 - Have you heard the one about
 - Are you ever scared about
 - Would you possibly care about
 - I'm worried about
 - Don't tell anyone about
 - Have I ever told you about
 - I'd like to mansplain about
 - I'm fed up hearing about
 - I'm really bothered about
 - I hate everything about
 - I love everything about
 - I can't stand knowing about
} {shuffle
  - the incident...
  - that job
  - your favourite type of fruit
  - the memes
 - that one really annoying advert
 - the time you fought Godzilla
 - the time you drank my juice
 - that boxing championship you won
 - that huge ass car outside my house
 - where you find such quality squid?
 - your favourite games
 - your most hated voice
 - the huge wodge of cash in my pocket
 - the camping trip we're planning
 - the drive to Antarctica
 - about having a sleep over
 - spooning
 - cuddling
 - puppies
 - kittens
 - stupidley cute spiders
 - sharks
 - the fine lass I left behind with me peg leg
 - my face
 - how to get down a mountain
 - the time you fought back all those wasps
 - the time I was shot in the eye with an arrow
 - my first true love
 - how you need to take the ring to Mordor
 - how I talk to women
 - how I talk to men
 - how I talk to people
 - those two headed donkeys
 - that time you had the best dream of your life
 - how you complete me
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
} {shuffle
 - Call me
 - Speak soon
 - Can't wait to talk
 - Ciao for now
 - Speak to me
 - Love you
 - Byesies
 - Bye
 - So long sucker
 - Don't be a stranger
 - Answer me
 - Bye for now, BFF
 - It's been too long
 - When did we last speak
}{shuffle
  -!
  -.
  -?
  -?!
 -...
 -!!!
 -???
}
"""

VAR a
42

