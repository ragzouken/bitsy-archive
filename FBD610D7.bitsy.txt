
Acid Makes Me Trip Spherical 3D-Shapes!

# BITSY VERSION 4.6

! ROOM_FORMAT 1

PAL 0
NAME FOREST
3,212,7
0,0,0
0,0,0

PAL 1
NAME WATER
3,171,255
240,250,249
231,252,252

PAL 2
NAME RED_AF
255,10,2
239,248,240
255,255,255

PAL 3
255,255,255
255,255,255
0,0,0

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,d,c,c,c,c,c,c,c,c,f,0,0,a
a,0,d,g,0,0,0,0,0,0,0,0,e,f,0,a
a,0,b,0,0,0,0,0,0,0,0,0,0,b,0,a
a,0,b,0,0,0,0,0,0,0,0,0,0,b,0,a
a,0,b,0,0,0,0,0,d,0,f,0,0,b,0,a
a,0,e,c,c,c,c,c,g,0,e,c,c,g,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,d,0,f,0,0,0,0,0,0,a
a,0,0,d,c,c,g,0,b,0,0,0,0,0,0,a
a,0,0,b,0,0,0,0,b,0,0,0,0,0,0,a
a,0,0,b,0,0,0,0,b,0,0,d,0,f,0,a
a,0,0,e,c,c,c,c,g,0,0,b,0,b,0,a
a,0,0,0,0,0,0,0,0,0,0,b,0,b,0,a
a,a,a,a,a,a,a,a,a,a,a,a,0,a,a,a
NAME JUNGLE
ITM 0 9,6
EXT 12,15 1 12,15
PAL 0

ROOM 1
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,c,c,c,c,c,c,0,b,b,0,c,c,c,c,a
a,0,0,0,0,c,c,0,b,b,0,c,c,0,0,a
a,0,a,0,0,c,c,0,b,b,0,c,c,0,0,a
a,0,0,0,0,c,c,0,b,b,0,c,c,0,0,a
a,0,0,0,0,0,c,0,0,0,0,c,0,0,0,a
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,d,c,c,f,0,0,0,0,0,0,0,0,0,a
a,0,b,0,0,0,0,0,0,0,c,b,0,b,c,a
a,0,b,0,0,e,c,f,0,0,c,b,0,b,c,a
a,0,b,0,0,0,0,b,0,0,c,b,0,b,c,a
a,0,b,0,0,0,0,b,0,0,c,b,0,b,c,a
a,0,e,c,c,c,c,g,0,0,c,b,0,b,c,a
a,a,a,a,a,a,a,a,a,a,a,a,0,a,a,a
NAME WATER LEVEL
ITM 1 12,10
ITM 2 4,10
ITM 3 7,2
ITM 4 10,2
EXT 7,1 0 8,2
EXT 10,1 2 11,14
PAL 1

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,b,b,b,0,0,0,0,0,0,0,b,0,0,0,a
a,0,0,0,0,0,b,b,b,b,b,b,0,0,0,a
a,0,0,0,0,0,b,0,0,0,0,0,0,0,0,a
a,0,b,b,b,b,b,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,b,b,b,a
a,0,0,0,0,0,0,0,0,0,0,0,b,0,0,a
a,0,0,0,0,d,d,d,d,0,0,0,b,0,0,a
a,0,0,d,d,d,0,0,d,0,0,0,0,0,0,a
a,0,0,d,0,0,0,0,d,0,0,0,0,0,0,a
a,0,0,d,0,d,d,d,d,0,0,0,0,0,0,a
a,0,0,d,0,d,0,d,d,0,b,0,b,0,0,a
a,0,0,0,0,d,0,d,d,0,b,0,b,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Red_Rum
PAL 2

TIL a
11100111
10011001
10011001
01100110
01100110
10011001
10011001
11100111
WAL true

TIL b
00011000
00011000
00011000
00011000
00011000
00011000
00011000
00011000
WAL true

TIL c
00000000
00000000
00000000
11111111
11111111
00000000
00000000
00000000
WAL true

TIL d
00000000
00000000
00000000
00011111
00011111
00011000
00011000
00011000
WAL true

TIL e
00011000
00011000
00011000
00011111
00011111
00000000
00000000
00000000
WAL true

TIL f
00000000
00000000
00000000
11111000
11111000
00011000
00011000
00011000
WAL true

TIL g
00011000
00011000
00011000
11111000
11111000
00000000
00000000
00000000
WAL true

SPR A
00000000
00011000
00011000
11111111
10011001
10111101
00100100
01100110
>
00000000
00011000
00011000
11111111
10011001
10111101
00100100
01100110
POS 0 8,2

SPR b
00000000
00000000
00000000
11111111
11111111
10100101
10100101
10000001
NAME Table
DLG SPR_1
POS 0 4,2

SPR c
00000000
01100110
01011010
01100110
01011010
01100110
01011010
00111100
NAME Tin of Beans
DLG SPR_2
POS 0 11,3

SPR d
00000000
00011100
00100100
00110011
01011001
10011101
00100110
01001000
>
00000000
00011100
00100100
00110011
11011001
00011101
01100110
00011000
NAME Jellyfish
DLG SPR_0
POS 0 5,11

SPR e
00000000
00000000
00010100
10010110
01111111
10010110
00010100
00000000
>
00000000
00000000
00010100
10001010
01111111
10001010
00010100
00000000
NAME Fish
DLG SPR_3
POS 0 5,4

SPR f
00011001
11001010
00111110
01000010
01000010
00111110
11001010
00011001
>
01001101
11001010
00111110
01000010
01000010
00111110
11001010
01001101
NAME Spider
DLG SPR_4
POS 1 6,13

SPR g
00000000
10000001
11000011
11100111
11111111
11100111
11000011
10000001
NAME Weights
DLG SPR_5
POS 1 2,3

SPR h
00000000
00011100
00100100
00110011
01011001
10011101
00100110
01001000
>
00000000
00011100
00100100
00110011
11011001
00011101
01100110
00011000
NAME Jellyfish2
DLG SPR_6
POS 1 7,7

ITM 0
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Doki Doki
DLG ITM_0

ITM 1
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME OKI DOKI
DLG ITM_1

ITM 2
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME DWS_1
DLG ITM_2

ITM 3
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Bad_Decisions
DLG ITM_3

ITM 4
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME DWS_2
DLG ITM_4

DLG ITM_0
{rbw}{shk}jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!jUsT dIe!{shk}{rbw}

DLG SPR_1
{wvy}Tables are robust, sturdy and reliable, just like my father was... {wvy}

DLG SPR_0
"""
{wvy}Bloop!Bloo! Being a squid out of water is pretty hard, but then again, being alone in a place you don't know is always hard right?
{wvy}
{rbw}{shk}j̷̧͙̘̳̎̾̑̃ͅU̶̹͂̎̈́s̶͇̪̬̺̩͂̈́̀T̶̫̤͍̯̤͂͗̀̏͜͠ ̸̫͓͕̰͛̾͜ͅM̴͉̱͙̃̀̐̒͘͜o̷̥͋̉̒̍̀N̵̡͓̭̗̄̅ì̶̢̝̆̃Ķ̸͈͔͓̦̙̓̉͛̓͌a̷̗̠̞̘͍̱̋̓͂̔!̷̬̤̯̈́̓{shk}{rbw}
"""

DLG SPR_2
{wvy} My teacher once asked her students to use the word "beans" in a sentence. "My father grows beans," said one girl. "My mother cooks beans," said a boy.{wvy} My sister said; "We are all {shk}human beans{shk}.

DLG SPR_3
"""
{wvy}I remember when I had fish, {rbw}{shk} my Dad doesn't because he sat on it{shk}.{rbw}
Poor guy couldn't even say goodbye{wvy}
"""

DLG SPR_4
"""
{shk}Spiders, why did it have to be spiders!{shk}
{rbw}{shk}S̵̺̞̞̈̄͂̈́p̶͖͈͚̐͠i̶̡̫̺̩̋̌͋ď̷̗̦̰ë̴̡̟̥́ͅr̴̨̟̗̽s̷̢̻̲͋͗̓̿ͅ,̶͍̉̄̾̓ ̴̘̗́͂ẅ̸͔͈̔̒̾ẖ̴́̀̽̅ý̷͚ ̴̦͍̽͐͊͝d̷̬͙͇̒̈́̅i̷͙͔̼̳̋̌͐d̵̬̬̊ ̴̩̰̚i̸̛̱̣͆͘t̸̰͋̔́ ̷̡̖̲͂h̷̝̹̩͉͌̀̑̃a̶͉̭̦̺͑v̸͇̤̫͊͆̏e̵̦̖͠ ̶̫̣̯͔͌̾̉̌ẗ̷̺ö̶̡͎̺̲̇͛͊ ̴̪͓̳̏͛b̴̡̼̪͙͝e̴̢̗̣͒ ̷̡̣͈͌̃́̕s̷̻̺͉̀̈͂p̵̦̋i̶̡͆͋̅d̸͙̠̓͆̚ę̷̕͜r̵͔̊̾̈́̀s̸͚̦̲͆̊̿̕!̷̳̻̎͘{shk}{rbw}


"""

DLG SPR_5
"""
{wvy}Gotta Get Beach-body Ready!{wvy}
{rbw}{shk}REEEEEEE!{shk}{rbw}
"""

DLG SPR_6
{rbw}{shk}Whagwarn my G!{shk}{rbw} {wvy}We meet again, I'm J to the -ellyfish, in the flesh...kinda.{wvy}

DLG ITM_1
{rbw}{shk}j̷̧͙̘̳̎̾̑̃ͅU̶̹͂̎̈́s̶͇̪̬̺̩͂̈́̀T̶̫̤͍̯̤͂͗̀̏͜͠ ̸̫͓͕̰͛̾͜ͅM̴͉̱͙̃̀̐̒͘͜o̷̥͋̉̒̍̀N̵̡͓̭̗̄̅ì̶̢̝̆̃Ķ̸͈͔͓̦̙̓̉͛̓͌a̷̗̠̞̘͍̱̋̓͂̔!̷̬̤̯̈́̓j̷̧͙̘̳̎̾̑̃ͅU̶̹͂̎̈́s̶͇̪̬̺̩͂̈́̀T̶̫̤͍̯̤͂͗̀̏͜͠ ̸̫͓͕̰͛̾͜ͅM̴͉̱͙̃̀̐̒͘͜o̷̥͋̉̒̍̀N̵̡͓̭̗̄̅ì̶̢̝̆̃Ķ̸͈͔͓̦̙̓̉͛̓͌a̷̗̠̞̘͍̱̋̓͂̔!̷̬̤̯̈́̓j̷̧͙̘̳̎̾̑̃ͅU̶̹͂̎̈́s̶͇̪̬̺̩͂̈́̀T̶̫̤͍̯̤͂͗̀̏͜͠ ̸̫͓͕̰͛̾͜ͅM̴͉̱͙̃̀̐̒͘͜o̷̥͋̉̒̍̀N̵̡͓̭̗̄̅ì̶̢̝̆̃Ķ̸͈͔͓̦̙̓̉͛̓͌a̷̗̠̞̘͍̱̋̓͂̔!̷̬̤̯̈́̓{shk}{rbw}

DLG ITM_2
{rbw}{shk} DEATH WON'T SAVE YOU 1; It's OK, you had no choice, he was dead anyway, don't think about it to much!{shk}{rbw}

DLG ITM_3
"""
{rbw}{wvy}Are you sure you want to keep going this way?{wvy}{rbw}

"""

DLG ITM_4
{rbw}{shk}DEATH WON'T SAVE YOU 1: He told you not to open the door why didn't you listen!{shk}{rbw}

VAR a
42


