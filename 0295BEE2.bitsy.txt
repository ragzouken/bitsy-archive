
What a fine Sunday morning! Just stretching a little before getting out of bed - what's that!? Oh no! Today is my friend's birthday and I completely forgot! All the shops are closed today, so maybe I can find something around the house? ...

# BITSY VERSION 4.2

! ROOM_FORMAT 1

PAL 0
NAME House
192,192,192
0,128,128
64,0,0

PAL 1
NAME Garden
100,255,100
0,128,0
64,0,0

PAL 2
NAME Shack
209,169,46
128,64,0
64,0,0

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,c,k,r,0,0,0,o,k,i,f,a
a,a,a,a,a,p,l,s,0,0,0,m,11,h,g,a
a,a,a,a,a,0,0,0,0,0,0,m,11,11,n,a
a,a,a,a,a,o,k,k,k,r,0,p,l,l,s,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,p,l,l,l,s,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,0,a,a,a
NAME MyRoom
WAL a,c,a,t,10,12,13,d
EXT 12,15 2 11,0
PAL 0

ROOM 1
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,0,0,0,0,0,0,e,e,0,0,e,e,0,0,e
e,0,0,0,e,e,e,0,0,0,e,e,e,0,0,e
e,0,e,e,e,0,0,0,0,e,e,e,0,e,0,e
e,0,e,e,e,0,0,0,0,e,e,e,e,e,0,e
e,0,0,0,e,0,0,0,0,0,e,e,e,0,0,e
e,0,0,0,0,0,0,0,0,e,0,0,0,0,0,0
e,0,0,e,e,0,0,0,0,0,0,0,0,0,0,e
e,0,e,e,e,e,e,0,0,0,0,0,0,0,0,e
e,e,e,e,e,0,0,0,0,0,0,0,e,e,e,e
e,e,e,e,e,e,0,0,0,0,e,e,e,e,e,e
e,0,0,e,e,e,e,0,0,e,e,0,0,e,e,e
e,0,0,e,e,e,0,0,0,e,0,0,0,0,e,e
e,0,0,0,e,e,0,0,e,e,0,0,0,0,0,e
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
NAME Garden
WAL f,g,h,e
EXT 1,12 5 7,15
EXT 15,6 7 0,6
PAL 1

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,0,a,a,a,a,a,a,a,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,a,0,0,0,0,0,a,a,a
a,d,0,0,0,0,0,a,0,o,k,k,k,k,r,a
a,d,0,d,d,0,z,a,0,m,11,11,11,11,n,a
a,d,0,d,d,0,10,a,0,m,11,t,11,v,f,a
a,d,0,0,0,0,13,a,0,m,11,10,10,y,z,a
a,d,0,0,0,0,13,a,0,m,v,w,w,x,z,a
a,d,10,10,10,10,10,a,0,p,u,j,j,j,g,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME LivingRoom
WAL a,t,10,12,13,d
EXT 11,0 0 12,15
EXT 3,5 3 7,15
EXT 15,7 4 0,7
EXT 0,6 1 15,6
PAL 0

ROOM 3
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,10,10,0,a,0,11,v,f,a,a,a
a,a,a,a,a,b,11,0,a,0,11,11,g,a,a,a
a,a,a,a,a,12,0,0,a,0,0,11,13,a,a,a
a,a,a,a,a,10,10,0,0,0,0,11,13,a,a,a
a,a,a,a,a,0,0,0,0,10,10,0,0,a,a,a
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
NAME Bathroom
WAL a,t,10,12,13
EXT 7,15 2 3,5
PAL 0

ROOM 4
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,d,d,d,d,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,a,a,a,a,a,a,a
a,o,k,r,0,0,t,0,d,a,a,a,a,a,a,a
a,m,11,n,0,0,i,f,d,a,a,a,a,a,a,a
a,c,l,s,0,0,h,g,d,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Sister's Room
WAL c,d,t,a
EXT 0,7 2 15,7
PAL 0

ROOM 5
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,0,0,0,a,0,0,0,0,a,a,a
a,a,a,r,0,0,0,0,a,0,0,0,0,a,a,a
a,a,a,n,0,0,0,0,a,10,0,0,17,a,a,a
a,a,a,n,0,10,10,0,0,0,0,0,0,a,a,a
a,a,a,n,0,10,10,0,0,0,0,0,17,a,a,a
a,a,a,n,0,0,0,0,0,0,0,0,0,a,a,a
a,a,a,14,k,k,k,0,k,k,k,k,r,a,a,a
NAME Shack
WAL a,17,10
EXT 7,15 1 1,12
EXT 3,15 6 5,14
PAL 2

ROOM 6
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,0,0,10,0,a,a,a,a,a,a
a,a,a,a,a,0,0,0,10,0,a,a,a,a,a,a
a,a,a,a,a,0,10,0,0,0,a,a,a,a,a,a
a,a,a,a,a,14,10,10,0,10,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME shack 2nd floor
WAL a,10
ITM 0 9,11
EXT 5,14 5 3,15
PAL 2

ROOM 7
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,0,a,a,a,a,a,a,a,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,a,0,0,0,0,0,a,a,a
a,d,0,0,0,0,0,a,0,o,k,k,k,k,r,a
a,d,0,d,d,0,z,a,0,m,11,11,11,11,n,a
a,d,0,d,d,0,10,a,0,m,11,t,11,v,f,a
a,d,0,0,0,0,13,a,0,m,11,10,10,y,z,a
a,d,0,0,0,0,13,a,0,m,v,w,w,x,z,a
a,d,10,10,10,10,10,a,0,p,u,j,j,j,g,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME LivingRoom2
WAL a,t,10,12,13,d
EXT 0,6 1 15,6
EXT 11,0 8 12,15
EXT 3,5 a 7,15
EXT 15,7 b 0,7
PAL 0

ROOM 8
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,c,k,r,0,0,0,o,k,i,f,a
a,a,a,a,a,p,l,s,0,0,0,m,11,h,g,a
a,a,a,a,a,0,0,0,0,0,0,m,11,11,n,a
a,a,a,a,a,o,k,k,k,r,0,p,l,l,s,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,m,11,11,11,n,0,0,0,0,0,a
a,a,a,a,a,p,l,l,l,s,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,0,a,a,a
NAME MyRoom2
WAL a,c,a,t,10,12,13,d
ITM 2 14,8
EXT 12,15 9 11,0
PAL 0

ROOM 9
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,0,a,a,a,a,a,a,a,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,a,0,0,0,0,0,a,a,a
a,d,0,0,0,0,0,a,0,o,k,k,k,k,r,a
a,d,0,d,d,0,z,a,0,m,11,11,11,11,n,a
a,d,0,d,d,0,10,a,0,m,11,t,11,v,f,a
a,d,0,0,0,0,13,a,0,m,11,10,10,y,z,a
a,d,0,0,0,0,13,a,0,m,v,w,w,x,z,a
a,d,10,10,10,10,10,a,0,p,u,j,j,j,g,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME LivingRoom3
WAL a,t,10,12,13,d
END undefined 11,0
PAL 0

ROOM a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,10,10,0,a,0,11,v,f,a,a,a
a,a,a,a,a,b,11,0,a,0,11,11,g,a,a,a
a,a,a,a,a,12,0,0,a,0,0,11,13,a,a,a
a,a,a,a,a,10,10,0,0,0,0,11,13,a,a,a
a,a,a,a,a,0,0,0,0,10,10,0,0,a,a,a
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
NAME Bathroom2
WAL a,t,10,12,13
EXT 7,15 7 3,5
PAL 0

ROOM b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,d,d,d,d,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,a,a,a,a,a,a,a
a,o,k,r,0,0,t,0,d,a,a,a,a,a,a,a
a,m,11,n,0,0,i,f,d,a,a,a,a,a,a,a
a,c,l,s,0,0,h,g,d,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME SistersRoom2
WAL c,d,t,a
EXT 0,7 7 15,7
PAL 0

TIL 10
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000

TIL 11
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL 12
00000000
00000000
01100000
10010000
00010000
10010000
01100000
00000000
>
00000000
00000000
01100000
10010000
01010000
10010000
01100000
00000000

TIL 13
00000000
00000110
00001001
00010000
00010010
00010000
00001001
00000110

TIL 14
11000000
01000000
01110000
00010000
00011100
00000100
00000111
00000001

TIL 17
11111111
00100010
00111111
00100010
11111111
00100010
01111111
01100010

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL b
00000000
11000000
11000000
11000000
11111100
01111100
01111000
00110000

TIL c
00110000
00111000
00111000
00110011
11111111
00100100
00100100
00100100

TIL d
11111111
10000001
10000001
10000001
10000001
10000001
10000001
11111111

TIL e
00011000
00111100
01111110
01111110
01111110
00111100
00011000
00011000

TIL f
00000000
11111110
00000010
00000010
00000010
00000010
00000010
00000010

TIL g
00000010
00000010
00000010
00000010
00000010
00000010
11111110
00000000

TIL h
10010000
11110000
11110000
11110000
11110000
10010000
11111111
00000000

TIL i
00000000
11111111
10010000
10010000
10010000
10010000
10010000
10010000

TIL j
00000000
00000000
00000000
00000000
00000000
00000000
11111111
00000000

TIL k
00000000
11111111
10101010
01010101
10101010
01010101
10101010
01010101

TIL l
10101010
01010101
10101010
01010101
10101010
01010101
11111111
00000000

TIL m
01101010
01010101
01101010
01010101
01101010
01010101
01101010
01010101

TIL n
10101010
01010110
10101010
01010110
10101010
01010110
10101010
01010110

TIL o
00000000
01111111
01101010
01010101
01101010
01010101
01101010
01010101

TIL p
01101010
01010101
01101010
01010101
01101010
01010101
01111111
00000000

TIL q
01101010
11010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL r
00000000
11111110
10101010
01010110
10101010
01010110
10101010
01010110

TIL s
10101010
01010110
10101010
01010110
10101010
01010110
11111110
00000000

TIL t
00000010
00000100
00001000
01111110
01000010
01000010
01111110
01111110
>
00010010
00000101
00001000
01111110
01000010
01000010
01000010
01111110

TIL u
01000000
01000000
01000000
01000000
01000000
01000000
01111111
00000000

TIL v
00000000
01111111
01000000
01000000
01000000
01000000
01000000
01000000

TIL w
00000000
11111111
00000000
00000000
00000000
00000000
00000000
00000000

TIL x
01000000
11000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL y
01000000
01000000
01000000
01000000
01000000
01000000
01000000
01000000

TIL z
00000010
00000010
00000010
00000010
00000010
00000010
00000010
00000010

SPR 10
00111100
00111100
00111100
00011000
11111111
10011001
10111001
00101000
DLG SPR_o
POS 9 6,9

SPR 11
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_p

SPR 12
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_q
POS 9 15,7

SPR 13
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_r
POS 9 3,5

SPR 14
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_s
POS 9 0,6

SPR 15
00111100
00111100
00111100
11011000
00111000
01011000
10111000
00101000
>
00111100
00111100
10111100
01011000
00111000
01011000
10111000
00101000
DLG SPR_t
POS 7 2,9

SPR 16
00000000
00011100
00011010
00001000
01111000
00001000
00111000
00100000
DLG SPR_u
POS 7 13,12

SPR 17
00000000
00110000
00110000
11110000
00010000
01110000
01000000
11000000
DLG SPR_v
POS b 7,11

SPR 18
00111000
01000100
00100010
01110111
10011101
01010101
00110111
00000000
DLG SPR_w
POS b 8,7

SPR 19
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_x
POS 7 15,4

SPR A
00000000
00001100
01001100
00111000
00011110
00011000
00011000
00010100
>
00000000
00110000
00110000
00011000
00111100
01011010
00011000
00010100
POS 0 13,8

SPR a
00111100
00111100
00111100
00011000
11111111
10011001
10111001
00101000
DLG SPR_0
POS 2 11,13

SPR b
00000000
00011100
00011010
00001000
01111000
00001000
00111000
00100000
DLG SPR_1
POS 2 13,12

SPR c
00011000
00111100
01111110
11111111
01000010
01011010
01011010
01111110
POS 1 1,11

SPR d
00000000
00000000
00000000
00000000
01010100
00111000
00010000
00101000
DLG SPR_2
POS 0 6,13

SPR e
00000000
00000000
00000000
00111100
00011000
00100100
00100100
00011000
DLG SPR_3
POS 2 8,14

SPR f
00000000
00000000
00000000
00000000
00000000
00011000
01111110
00011000
DLG SPR_4
POS 0 5,10

SPR g
00000000
00000000
00010000
00010000
00111000
00010000
00010000
00101000
DLG SPR_5
POS 0 9,14

SPR h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_6
POS 2 15,4

SPR i
00000000
00110000
00110000
11110000
00010000
01110000
01000000
11000000
DLG SPR_7
POS 4 2,12

SPR j
00111000
01000100
00100010
01110111
10011101
01010101
00110111
00000000
DLG SPR_8
POS 4 8,7

SPR k
00000000
00000000
00000000
00111000
00111000
00111000
00000000
00000000
DLG SPR_9
POS 1 13,2

SPR l
00000000
00001000
00001000
00001000
00001000
00001000
00111110
00101010
DLG SPR_a
POS 5 12,9

SPR m
00000000
00001000
00001000
00001000
00001000
00011100
00011100
00011100
DLG SPR_b
POS 5 11,9

SPR n
00000000
00000000
00000000
11111001
11111110
01110000
00100000
01110000
DLG SPR_c
POS 5 10,9

SPR o
11111111
10101011
10101001
10001001
11101011
10110001
11011011
11111111
DLG SPR_e
POS 5 7,9

SPR p
11111111
11110101
10010101
10111111
11100101
10100111
10111101
11111111
DLG SPR_d
POS 5 5,9

SPR q
00000000
00000000
01000000
01000000
01000000
01110100
01010010
01001001
DLG SPR_f
POS 5 3,9

SPR r
11111111
00100010
11111010
00100010
11111111
00100010
11110010
00111111
DLG SPR_g
POS 5 12,12

SPR s
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
DLG SPR_h
POS 6 6,12

SPR t
00000000
00000000
00000000
11111000
10101000
11111100
01010100
11111100
DLG SPR_i
POS 0 5,8

SPR u
00000000
00000000
00000000
11111000
10101000
11111100
01010100
11111100
DLG SPR_j
POS 8 5,8

SPR v
00000000
00000000
00000000
00000000
01010100
00111000
00010000
00101000
DLG SPR_m
POS 8 6,13

SPR w
00000000
00000000
00010000
00010000
00111000
00010000
00010000
00101000
DLG SPR_k
POS 8 8,12

SPR y
00000000
00000000
00000000
00000000
00000000
00011000
01111110
00011000
DLG SPR_l
POS 8 5,10

SPR z
00000000
00011100
00011010
00001000
01111000
00001000
00111000
00100000
DLG SPR_n
POS 9 13,11

ITM 0
00000000
00000000
01000100
00101000
01101110
00000000
01101110
01101110
NAME package
DLG ITM_0

ITM 1
00000000
00000000
00001000
00001000
00011100
00101010
00001000
00010100
NAME figurine

ITM 2
00000000
01000000
10100000
01000100
00101110
11111111
01111110
00111100
DLG ITM_1

DLG SPR_0
My child! You need a present for your friend? I guess you should look for something in your room, you got a lot of junk in there...

DLG ITM_0
You found a perfect package for a present. What a coincidence! There is nothing inside, however...

DLG SPR_1
My child, don't give your friend just anything, spending time with you is the best present!

DLG SPR_2
It's a figurine which I bought on my travels to Japan. I never really use it or play with it, but I don't want to give it away. And I'm not sure if my friend would even like it.

DLG SPR_3
It's a vase my father bought for my mother once. Sometimes there are even flowers in it. But it's not mine to give away.

DLG SPR_4
It's a watch, which I bought some time ago, but the battery is dead and I never got around to change it. Anyway, I can't give something used to my friend.

DLG SPR_5
A figurine. I bought it abroad and it reminds me of good times. When I look at it... Sometimes... I don't want to give it away. 

DLG SPR_6
My parents room... I can't take anything from there.

DLG SPR_7
Sorry, can't help you. Why don't you just make something yourself? DIY is quite "in", I've heard...

DLG SPR_8
Hmmmm. My sister's books. A book might be a good idea, but they are all quite obviously used...

DLG SPR_9
It's my volleyball. But it's too dirty to give to my friend. 

DLG SPR_a
I don't think my friend would like a rusty rake for their birthday...

DLG SPR_b
Really? A shovel? I don't think these old tools are fit to be a present.

DLG SPR_c
These big tools are not a good idea...

DLG SPR_d
Broken plates, which might someday be used for something else...

DLG SPR_e
Ropes, wires, threads, useful for gardening...

DLG SPR_f
A broken chair and its leg. 

DLG SPR_g
It's a shelf. There are tools for gardening in it.

DLG SPR_h
Cardboard boxes all around...

DLG SPR_i
My video games... My friend has already got most of those.

DLG SPR_j
My video games. My friend has already got most of those. 

DLG SPR_k
This figurine reminds me of good times. IF I look at it... Sometimes... I don't want to give it away. 

DLG SPR_l
I can't give this used watch to my friend.

DLG SPR_m
Still don't want to give this figurine to my friend...

DLG ITM_1
It's a basket full of yummie things, good wine and flowers. There's a note saying: "I bought this for a colleague whose birthday is on Tuesday. You can have it for your friend and I will just buy another one tomorrow. Kisses, mum." Phew, lucky me! I should go thank my mother!

DLG SPR_n
You are very welcome! After all, I can't let you give just some bric-a-brac to your friend! Now go to your room, call your friend and relax a little bit. You seem so stressed!

DLG SPR_o
I didn't really do anything, you should thank your mother!

DLG SPR_p
I should go to my room and call my friend.

DLG SPR_q
I should go to my room and call my friend.

DLG SPR_r
I should go to my room and call my friend.

DLG SPR_s
I should go to my room and call my friend.

DLG SPR_t
You should look for something in your room, you got a lot of junk in there...

DLG SPR_u
Maybe you should have a look in your room?

DLG SPR_v
Just make something yourself!

DLG SPR_w
My sister's books. A book might be a good idea, but they are all quite obviously used...

DLG SPR_x
Nothing interesting in my parents' room...

END 0
You wake up in the morning, look on you calendar and panic. You forgot that it's your friend's birthday! And it's Sunday - all the shops are closed, but you need a present. Maybe you can find something in your home?

END undefined
Ahhhh I got something to give to my friend! I am a bit sad that I couldn't think of anything myself, but I was really stressed lately. Plus, I get to keep all my stuff that I don't really need. That's a relief!         The End.       Made by lajiao with Bitsy. 

VAR a
42


