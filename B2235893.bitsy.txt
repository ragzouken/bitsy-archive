scription

# BITSY VERSION 3.5

! ROOM_FORMAT 1

PAL 0
0,0,0
255,255,255
19,255,243

PAL 1
0,0,0
255,255,255
255,11,237

PAL 2
0,0,0
255,255,255
255,255,0

PAL 3
0,0,0
0,255,255
255,0,255

PAL 4
0,0,0
0,255,255
255,255,0

PAL 5
0,0,0
255,0,255
255,255,255

PAL 6
0,0,0
255,255,0
255,0,255

PAL 7
0,0,0
255,0,255
0,255,255

PAL 8
0,0,0
0,255,255
255,255,255

PAL 9
0,0,0
181,240,140
251,207,159

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,d,c,i,a,a,a,a,a,a
a,a,a,a,a,a,a,e,a,b,a,a,a,a,a,a
a,a,a,a,a,a,a,f,h,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM 1
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,d,c,i,a,a,a,a,a,a
a,a,a,a,a,a,a,e,a,b,a,a,a,a,a,a
a,a,a,a,a,a,a,e,a,0,a,a,a,a,a,a
a,a,a,a,a,a,a,f,h,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,7 3 9,8
EXT 10,8 5 10,8
EXT 9,9 4 9,9
EXT 8,8 5 10,8
PAL 0

ROOM 3
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,c,c,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,7 g 9,8
EXT 10,8 7 10,8
EXT 8,8 7 10,8
EXT 9,9 6 9,9
PAL 0

ROOM 4
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,8 6 9,9
EXT 10,9 9 10,9
EXT 9,10 d 9,10
EXT 8,9 9 10,9
PAL 0

ROOM 5
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,7 7 10,8
EXT 11,8 8 11,8
EXT 10,9 9 10,9
EXT 9,8 8 11,8
PAL 0

ROOM 6
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,c,c,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,9 a 10,9
EXT 8,9 a 10,9
EXT 9,10 w 9,10
EXT 9,8 14 9,9
PAL 2

ROOM 7
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,e,c,c,b,b,a,a,a,a,a
a,a,a,a,a,e,e,a,a,g,0,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,9 a 10,9
EXT 9,8 12 11,8
EXT 11,8 12 11,8
EXT 10,7 k 10,8
PAL 1

ROOM 8
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,e,a,a,a,a,b,b,a,a,a,a
a,a,a,a,f,h,g,g,g,g,g,g,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,8 b 12,8
EXT 12,8 b 12,8
EXT 11,7 12 11,8
EXT 11,9 13 11,9
PAL 2

ROOM 9
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,8 a 10,9
EXT 10,10 j 10,10
EXT 9,9 13 11,9
EXT 11,9 13 11,9
PAL 2

ROOM 10
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM 11
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM 12
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,b,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,f,h,g,g,g,g,g,g,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM 13
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,b,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,g,a,a,a,a
a,a,a,a,f,h,g,0,0,0,0,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM 14
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM 15
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 1

ROOM 16
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,b,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,0,a,a,a,a
a,a,a,a,f,h,0,g,g,g,0,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 1

ROOM 17
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,b,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,g,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,g,a,a,a,a
a,a,a,a,f,h,0,g,g,g,g,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 1

ROOM 18
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,0,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,c,c,c,c,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,8 s 10,9
EXT 9,9 c 11,9
EXT 11,9 c 11,9
EXT 10,10 15 10,10
PAL 1

ROOM b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,d,c,c,c,c,c,c,c,c,i,a,a,a
a,a,a,e,a,a,a,a,a,a,a,a,b,a,a,a
a,a,a,f,h,g,g,g,g,g,g,g,g,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 12,7 v 12,8
EXT 11,8 y 13,8
EXT 13,8 y 13,8
EXT 12,9 u 12,9
PAL 2

ROOM c
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,i,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,b,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,g,a,a,a,a
a,a,a,a,e,a,a,a,a,a,a,g,a,a,a,a
a,a,a,a,f,h,g,g,g,g,g,g,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 1

ROOM d
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,9 w 9,10
EXT 9,11 e 9,11
EXT 8,10 j 10,10
EXT 10,10 j 10,10
PAL 0

ROOM e
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,10 x 9,11
EXT 10,11 h 10,11
EXT 9,12 f 9,12
EXT 8,11 h 10,11
PAL 0

ROOM f
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,12 i 10,12
EXT 8,12 i 10,12
PAL 0

ROOM g
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,7 l 9,8
EXT 9,9 14 9,9
EXT 8,8 k 10,8
EXT 10,8 k 10,8
PAL 0

ROOM h
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,12 i 10,12
PAL 0

ROOM i
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM j
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,9 15 10,10
EXT 10,11 h 10,11
EXT 11,10 17 11,10
EXT 9,10 17 11,10
PAL 0

ROOM k
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,f,h,g,g,g,g,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 10,9 s 10,9
EXT 11,8 16 11,8
EXT 9,8 16 11,8
EXT 10,7 18 10,8
PAL 0

ROOM l
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,7 m 9,8
EXT 9,9 o 9,9
EXT 8,8 18 10,8
EXT 10,8 18 10,8
PAL 0

ROOM m
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,0,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,7 n 9,8
EXT 9,9 q 9,9
PAL 0

ROOM n
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM o
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 9,10 p 9,10
EXT 9,8 q 9,9
PAL 0

ROOM p
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM q
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,0,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM r
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM s
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,d,c,c,c,c,i,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,b,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,g,a,a,a,a,a
a,a,a,a,a,e,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,f,h,g,0,g,0,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM t
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,d,c,c,c,c,c,c,c,i,a,a,a
a,a,a,a,e,a,a,a,a,a,a,a,b,a,a,a
a,a,a,a,e,a,a,a,a,a,a,a,0,a,a,a
a,a,a,a,e,a,a,a,a,a,a,a,0,a,a,a
a,a,a,a,f,h,g,g,g,g,g,g,0,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 1

ROOM u
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,d,c,c,c,c,c,c,c,c,i,a,a,a
a,a,a,e,a,a,a,a,a,a,a,a,b,a,a,a
a,a,a,e,a,a,a,a,a,a,a,a,0,a,a,a
a,a,a,f,h,g,g,g,g,g,g,g,g,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM v
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,d,c,c,c,c,c,c,c,c,i,a,a,a
a,a,a,e,a,a,a,a,a,a,a,a,b,a,a,a
a,a,a,e,a,a,a,a,a,a,a,a,0,a,a,a
a,a,a,f,h,g,g,g,g,g,g,g,g,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM w
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM x
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

ROOM y
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,d,c,c,c,c,c,c,c,c,c,c,i,a,a
a,a,e,a,a,a,a,a,a,a,a,a,a,b,a,a
a,a,f,h,g,g,g,g,0,g,g,g,g,g,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 2

ROOM z
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,d,c,c,i,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,b,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,0,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,e,a,a,g,a,a,a,a,a,a
a,a,a,a,a,a,f,h,g,g,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
PAL 0

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL b
00111111
00011111
00001111
00000111
00000011
00000001
00000000
00000000

TIL c
11111111
11111111
11111111
11111111
11111111
11111111
11111111
00000000

TIL d
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111110

TIL e
11111110
11111110
11111110
11111110
11111110
11111110
11111110
11111110

TIL f
11111110
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL g
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL h
00000000
00000000
10000000
11000000
11100000
11110000
11111000
11111100

TIL i
11111111
11111111
11111111
11111111
11111111
11111111
11111111
01111111

SPR 10
10101110
10001111
11111111
11010111
11111111
11001001
11001001
11111111
POS 7 6,7

SPR 11
00110011
10111011
10101110
10111111
10101000
10001111
11111000
11111111
POS 7 7,7

SPR 12
10011111
00001111
00000000
11111010
00001010
11111011
00001111
11111111
POS 7 8,7

SPR 13
01011000
11111111
10001011
11001110
11101111
10111111
10101111
10101011
POS 7 9,7

SPR 14
11101111
01110110
10111100
11011000
11111111
01111110
10111100
11011000
POS 8 5,7

SPR 15
00101011
00001111
00101011
00001111
11111000
00011111
01010100
00011111
POS 8 6,7

SPR 16
10000011
10000011
11111111
11011101
10010100
11111101
01011111
11111111
POS 8 7,7

SPR 17
11111111
00000111
11010111
01010111
01011100
00001101
11011000
11111111
POS 8 8,7

SPR 18
10000111
11111111
11000001
01111100
00000111
11110100
00110101
11110000
POS 8 9,7

SPR 19
01010101
11010101
11110111
00111111
11110001
00010101
11010101
00010101
POS 8 10,7

SPR 20
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 21
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 22
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 23
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR A
11111111
11011011
11111111
10111101
00111100
11000011
11111111
11111111
POS 2 9,8

SPR a
11101111
00000011
10000011
00000011
00000011
10000111
00011111
11111111

SPR b
11111111
11111111
11011011
11111111
11000011
11100111
11111111
11111111
POS 0 8,7

SPR c
11110101
00000001
01010101
01010101
00000101
11010111
00010001
11111111
POS 2 8,7

SPR d
10101011
00000000
11010101
11010101
10000000
11011101
00000101
11111111
POS 2 7,7

SPR e
10100010
10111010
10100010
10111111
10101010
10101111
10001010
11111111
POS 3 7,7

SPR f
00000101
11111111
00100000
11111111
10101010
11111010
10101010
11111111
POS 3 8,7

SPR g
11111111
10001011
10101000
10101110
00101000
10101011
00101000
10101111
POS 3 7,6

SPR h
11010101
11010111
11000101
11111111
10101101
11111111
00000101
11110111
POS 3 8,6

SPR i
10111111
00000000
11011101
01010111
01010101
01110101
11111101
00000111
POS 4 7,7

SPR j
00111101
00000001
11110101
01110111
11010101
01110101
01010001
01011111
POS 4 8,7

SPR k
01110111
01010101
11011101
01110111
01010101
11011101
01110111
11111111
POS 4 7,8

SPR l
00010001
11110001
01000001
01011111
11000000
01010111
01010100
11010111
POS 4 8,8

SPR m
10101011
10000011
11111111
11010101
10000100
11011101
10010111
11111111
POS 5 7,7

SPR n
11110111
10000011
11010111
01010111
00000100
11010111
11111100
11111111
POS 5 8,7

SPR o
10101010
10001000
10101111
10001111
11111000
00011101
01011000
00011111
POS 5 6,7

SPR p
10001111
11111111
11000101
11111100
01000111
11110100
01110101
11111100
POS 5 9,7

SPR q
11111111
10001011
10101001
11111101
10101001
10101011
10101000
10101111
POS 6 7,6

SPR r
00010101
01110101
00000101
11111111
10001101
11111101
00011101
11011111
POS 6 8,6

SPR s
10101111
10101111
10100011
00111011
01110010
01011111
01010101
00011111
POS 6 7,7

SPR t
00011111
01111111
00001000
11111111
10100100
11100100
11100100
11111111
POS 6 8,7

SPR u
11111111
00000100
11110111
10000110
10111111
10001011
11111011
00000011
POS 6 7,8

SPR v
10010100
11111100
11001100
01111111
11100100
00111111
11110010
10011111
POS 6 8,8

SPR w
11111111
10011111
10010001
11110101
11110001
11110101
11111111
10001111
POS 7 6,6

SPR x
11111111
10001011
10101001
10001111
10101110
11111011
11111001
11111111
POS 7 7,6

SPR y
11000111
11010101
11000111
11111101
11010111
11111101
00111111
11111101
POS 7 8,6

SPR z
11111111
11010001
10010101
00010101
11110111
00011111
01011000
00011010
POS 7 9,6

SPR 1a
10101101
10001100
10101111
11111111
11111110
00100111
01110110
00100111
POS 9 6,7

SPR 1b
11101011
00000011
10101111
10111101
10000100
10111111
10010100
11111111
POS 9 7,7

SPR 1c
11111111
01110111
00100100
01110101
11111100
10000111
11010111
10000110
POS 9 6,8

SPR 1d
11111111
10011111
00000000
11111101
00000100
11010111
10000100
11111111
POS 9 8,7

SPR 1e
01010001
11111111
10001101
11001101
01000111
11110110
00110110
11111110
POS 9 9,7

SPR 1f
11110111
11111110
00101111
01111111
00110000
11111010
11110000
00011111
POS 9 7,8

SPR 1g
01100001
11100011
11100001
10111111
11111111
11000010
11100011
11000011
POS 9 8,8

SPR 1h
11111000
00001111
10101100
00001111
11111110
11000011
11100011
11000011
POS 9 9,8

SPR 1i
00111111
00100000
11110101
11100000
11111011
10101011
11111111
11010111
POS a 6,6

SPR 1j
10001111
00100011
10001111
11011111
11011111
11111111
11111001
11111111
POS a 7,6

SPR 1k
11101111
11000101
00010001
11000101
11101111
11101101
10111101
11111111
POS a 8,6

SPR 1l
11010001
10010101
00010111
00010111
00010111
01011111
00011000
11111000
POS a 9,6

SPR 1m
11000110
11010111
11000111
11010111
11111111
01111111
01100010
01100010
POS a 6,7

SPR 1n
00101011
10100011
10101111
10101110
00101011
10101000
00001111
00111001
POS a 7,7

SPR 1o
11111111
11000111
00000001
00000000
11111010
00001111
11011111
00001111
POS a 8,7

SPR 1p
01011000
11111111
10001011
11001010
11101010
11111110
10111111
10101111
POS a 9,7

SPR 1q
11100010
11111101
01110100
11111101
11010100
10000111
11010110
10000110
POS a 6,8

SPR 1r
00110011
01111110
00100100
01111111
00110000
11111010
10110000
00011010
POS a 7,8

SPR 1s
11100001
01100011
11100001
11111111
11000001
10000001
11001001
10000001
POS a 8,8

SPR 1t
10111100
00001111
10101110
00001111
11000001
10000001
11001001
10000001
POS a 9,8

SPR 1u
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 1v
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 1w
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 1x
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 1y
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

SPR 1z
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

DLG a
I'm a cat

