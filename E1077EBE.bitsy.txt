
Super Disco Breakin'

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
190,47,204
255,204,86
255,245,255

PAL 1
255,219,76
194,41,255
0,0,0

PAL 2
NAME toilet
184,204,224
78,98,115
71,71,103

PAL 3
0,0,0
254,255,255
204,200,169

ROOM 0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,a,e,a,f,f,f,f,f,f
f,f,f,f,f,f,f,a,e,a,f,f,f,f,f,f
f,f,f,a,a,a,a,a,e,a,a,a,a,f,f,f
f,f,f,a,0,b,0,b,0,b,0,b,a,f,f,f
f,f,f,a,b,0,b,0,b,0,b,0,a,f,f,f
f,f,f,a,0,b,0,b,0,b,0,b,a,f,f,f
f,f,f,a,b,0,b,0,b,0,b,0,a,a,a,f
f,f,f,a,0,b,0,b,0,b,0,b,0,0,0,f
f,f,f,a,b,0,b,0,b,0,b,0,a,a,a,f
f,f,f,a,0,b,0,b,0,b,0,b,a,f,f,f
f,f,f,a,b,0,b,0,b,0,b,0,a,f,f,f
f,f,f,a,a,a,a,a,0,a,a,a,a,f,f,f
f,f,f,f,f,f,f,a,0,a,f,f,f,f,f,f
f,f,f,f,f,f,f,a,0,a,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME dancefloor0
EXT 4,6 1 4,6
EXT 5,5 1 5,5
EXT 6,4 1 6,4
EXT 4,8 1 4,8
EXT 5,7 1 5,7
EXT 6,6 1 6,6
EXT 7,5 1 7,5
EXT 8,4 1 8,4
EXT 4,10 1 4,10
EXT 5,9 1 5,9
EXT 7,7 1 7,7
EXT 8,6 1 8,6
EXT 9,5 1 9,5
EXT 10,4 1 10,4
EXT 5,11 1 5,11
EXT 6,10 1 6,10
EXT 7,9 1 7,9
EXT 8,8 1 8,8
EXT 9,7 1 9,7
EXT 10,6 1 10,6
EXT 7,11 1 7,11
EXT 8,10 1 8,10
EXT 9,9 1 9,9
EXT 10,8 1 10,8
EXT 11,7 1 11,7
EXT 11,5 1 11,5
EXT 11,9 1 11,9
EXT 10,10 1 10,10
EXT 9,11 1 9,11
EXT 11,11 1 11,11
EXT 13,8 3 4,8
EXT 8,13 4 6,10
PAL 0

ROOM 1
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,a,e,a,f,f,f,f,f,f
f,f,f,f,f,f,f,a,e,a,f,f,f,f,f,f
f,f,f,a,a,a,a,a,e,a,a,a,a,f,f,f
f,f,f,a,b,0,b,0,b,0,b,0,a,f,f,f
f,f,f,a,0,b,0,b,0,b,0,b,a,f,f,f
f,f,f,a,b,0,b,0,b,0,b,0,a,f,f,f
f,f,f,a,0,b,0,b,0,b,0,b,a,a,a,f
f,f,f,a,b,0,b,0,b,0,b,0,0,0,0,f
f,f,f,a,0,b,0,b,0,b,0,b,a,a,a,f
f,f,f,a,b,0,b,0,b,0,b,0,a,f,f,f
f,f,f,a,0,b,0,b,0,b,0,0,a,f,f,f
f,f,f,a,a,a,a,a,0,a,a,a,a,f,f,f
f,f,f,f,f,f,f,a,0,a,f,f,f,f,f,f
f,f,f,f,f,f,f,a,0,a,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME dancefloor1
EXT 4,5 0 4,5
EXT 5,4 0 5,4
EXT 4,7 0 4,7
EXT 5,6 0 5,6
EXT 6,5 0 6,5
EXT 7,4 0 7,4
EXT 9,4 0 9,4
EXT 8,5 0 8,5
EXT 7,6 0 7,6
EXT 6,7 0 6,7
EXT 5,8 0 5,8
EXT 4,9 0 4,9
EXT 4,11 0 4,11
EXT 5,10 0 5,10
EXT 6,11 0 6,11
EXT 6,9 0 6,9
EXT 7,8 0 7,8
EXT 7,10 0 7,10
EXT 8,11 0 8,11
EXT 8,9 0 8,9
EXT 8,7 0 8,7
EXT 9,6 0 9,6
EXT 9,8 0 9,8
EXT 9,10 0 9,10
EXT 10,11 0 10,11
EXT 10,9 0 10,9
EXT 10,7 0 10,7
EXT 10,5 0 10,5
EXT 11,4 0 11,4
EXT 11,6 0 11,6
EXT 11,8 0 11,8
EXT 11,10 0 11,10
EXT 8,2 2 7,10
EXT 8,13 4 6,9
PAL 1

ROOM 2
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,a,a,a,a,a,a,a,f,f,f,f
f,f,f,f,f,a,c,d,d,f,0,a,f,f,f,f
f,f,f,f,f,a,c,d,d,d,0,a,f,f,f,f
f,f,f,f,f,a,c,d,d,d,0,a,f,f,f,f
f,f,f,f,f,a,c,d,d,d,0,a,f,f,f,f
f,f,f,f,f,a,0,d,d,d,0,a,f,f,f,f
f,f,f,f,f,a,a,e,d,a,a,a,f,f,f,f
f,f,f,f,f,f,a,e,a,a,f,f,f,f,f,f
f,f,f,f,f,f,a,e,a,f,f,f,f,f,f,f
f,f,f,f,f,f,a,e,a,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME toilet
EXT 7,12 1 8,3
PAL 2

ROOM 3
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,a,a,a,a,a,a,a,f,f,f,f,f
f,f,f,a,a,0,0,0,0,0,a,f,f,f,f,f
f,f,e,e,e,e,e,e,0,0,a,f,f,f,f,f
f,f,f,a,a,0,0,0,0,0,a,f,f,f,f,f
f,f,f,f,a,a,a,a,a,a,a,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME djbooth
EXT 3,8 0 12,8
PAL 0

ROOM 4
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,g,f,f,f,f,f,f,f
f,f,f,f,f,f,f,a,g,a,f,f,f,f,f,f
f,f,f,f,f,f,0,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,0,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,0,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,0,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,0,0,0,a,f,f,f,f,f,f
f,f,f,f,f,f,a,a,a,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME stairs
EXT 8,7 5 8,7
EXT 6,9 0 8,12
PAL 1

ROOM 5
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,a,a,a,f,f,f,f,f,f,f
f,f,f,f,f,a,0,0,0,a,f,f,f,f,f,f
f,f,f,f,f,a,0,a,0,a,f,f,f,f,f,f
f,f,f,f,f,a,g,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,g,a,g,a,f,f,f,f,f,f
f,f,f,f,f,a,g,a,g,a,f,f,f,f,f,f
f,f,f,f,f,f,g,a,g,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
EXT 6,8 7 4,8
EXT 8,8 4 8,8
PAL 1

ROOM 6
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,0
j,j,j,i,j,j,j,j,j,j,j,j,i,j,j,0
j,j,j,j,j,i,j,j,j,j,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,i,j,j,j,j,j,j
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
PAL 2

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,k
0,0,k,0,0,0,0,k,0,0,0,k,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,s,0,0
0,0,0,k,0,0,0,0,0,0,0,0,p,m,r,0
0,0,0,0,0,0,0,0,0,k,0,0,0,q,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,n,0,n,0,n,0
o,o,o,0,0,o,o,0,o,o,o,o,o,o,o,o
o,o,0,0,0,0,0,o,o,o,o,o,o,o,o,0
0,o,o,o,o,o,0,o,0,o,0,o,0,o,0,0
0,0,l,0,l,0,l,0,l,0,l,0,l,0,l,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
END 0 6,10
PAL 3

TIL a
11111111
10000001
10001001
10111001
10011101
10010001
10000001
11111111
WAL true

TIL b
10000000
01100110
00010010
00010000
00001000
01001000
01100110
00000001

TIL c
11000000
01000000
01000000
01000000
01110000
00010000
11110000
10000000

TIL d
00001000
01000000
10000000
00000000
00000000
00000001
00000010
00010000

TIL e
00010001
01000100
00100010
10001000
00010001
01000100
00100010
10001000

TIL f
11111111
11111111
11111111
11111111
11101111
11111111
11111111
11111111

TIL g
11111111
00000000
11111111
00000000
11111111
00000000
11111111
00000000

TIL h
11111111
11111111
11111111
11111111
10000001
11111111
11111111
11111111

TIL i
00001110
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL j
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL k
00000000
00010000
00010000
01111100
00010000
00010000
00000000
00000000

TIL l
00000000
00000000
00000000
11111111
00000000
00000000
00000000
00000000

TIL m
01111110
11111111
11111111
11111111
11111111
11111111
11111111
01111110

TIL n
00011111
00010011
00010000
00010000
00010000
00010000
00010000
00010000

TIL o
00000010
00000000
00000000
01000000
00000000
00000000
00000000
00000000
WAL true

TIL p
00000000
00000001
00000001
00000011
00000011
00000001
00000001
00000000

TIL q
01111110
00011000
00000000
00000000
00000000
00000000
00000000
00000000

TIL r
00000000
10000000
10000000
11000000
11000000
10000000
10000000
00000000

TIL s
00000000
00000000
00000000
00000000
00000000
00000000
00011000
01111110

SPR 10
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
POS 0 10,4

SPR 11
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
POS 0 6,11

SPR 12
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
POS 0 10,11

SPR 13
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_i
POS 3 5,9

SPR 14
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_j
POS 3 7,9

SPR 15
01001001
01000001
01001101
01001111
01000101
01000001
01010001
01000001
>
01010001
01000001
01001101
01001111
01000101
01000001
01001001
01000001
DLG SPR_k
POS 3 8,8

SPR 16
00100110
00100010
00100010
00100010
00110010
00111110
01000010
01000010
POS 3 8,7

SPR 17
01111110
00100010
00100010
00100010
00100010
00100010
00100010
00100010
POS 3 8,9

SPR 18
11000000
11100000
01100000
11100000
01100000
01100000
11100000
11100000
>
01100000
01100000
00110000
11110000
00110000
00110000
11110000
11110000
POS 3 9,8

SPR 19
00000000
00000000
00000000
00000000
00000000
00000000
00000000
01000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
10000000
POS 3 9,7

SPR A
00111000
01111100
01101000
01111100
01111000
00011000
00011000
00011000
POS 0 4,4

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
DLG SPR_0
POS 1 8,12

SPR b
00000000
00010000
00111000
00111000
00111000
00111000
00010000
00010000
DLG SPR_2
POS 0 6,8

SPR c
00000010
00010100
00011000
00111000
01011000
00011000
00100100
00100100
POS 1 6,8

SPR d
00000000
01111111
01111111
01111111
01111111
01111111
01111111
01111111

SPR e
01111111
01111111
01111111
01111111
01111111
01111111
00100010
00100010

SPR f
01111111
01000001
01000001
01000011
01000011
01001111
01111111
01000111
DLG SPR_8
POS 2 10,7

SPR g
01000111
01111111
01000001
01000001
01000001
01000001
01000001
01111111
POS 2 10,8

SPR h
01111111
01011111
01010111
01011111
01001111
01011111
01111111
01111111
>
01111111
01011111
01011111
01011111
01011111
01001111
01111111
01111111
DLG SPR_6
POS 2 10,6

SPR i
01111111
01000001
01000001
01000011
01000011
01001111
01111111
01000111
DLG SPR_7
POS 2 10,5

SPR j
11111111
01000001
01000001
01000011
01000011
01001111
11111111
01000011
POS 2 10,4

SPR k
00000000
00000001
00000010
00000010
00000010
00000010
00000010
00000011
DLG SPR_3
POS 2 9,4

SPR l
00000000
11000000
10000000
11111000
00001000
11111000
11100000
10000000
>
00000000
11000000
11000000
11111000
00001000
11111000
11100000
10000000
DLG SPR_4
POS 2 6,7

SPR m
00000000
11000000
10000000
11111000
00001000
11111000
11100000
10000000
DLG SPR_5
POS 2 6,8

SPR o
00001000
00111110
00111110
01011010
01011010
01011010
00101000
00101000
DLG SPR_a
POS 0 9,7

SPR p
00010000
00111000
01011100
01011010
00111001
00011000
00010100
00010100
DLG SPR_9
POS 1 9,7

SPR q
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_g
POS 3 7,7

SPR r
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_h
POS 3 5,7

SPR t
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_b
POS 1 10,4

SPR u
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_c
POS 1 6,11

SPR v
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_d
POS 1 6,4

SPR w
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
POS 1 10,11

SPR x
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110

SPR y
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_e

SPR z
01111110
01000010
01000010
01011010
01100110
01100110
01011010
01111110
>
01111110
01000010
10011001
10100101
10100101
10100101
10011001
01111110
DLG SPR_f
POS 0 6,4

SPR 1a
00001000
00001000
00001000
00001000
00001000
00001000
00001000
00001000

SPR 1b
11111111
11111111
11111111
11111111
11100111
11100111
11100111
11100111

SPR 1c
11111111
10000001
10000001
10000001
10000001
10000001
10000001
10000001
DLG SPR_m
POS 7 4,7

SPR 1d
00000000
00111100
01011010
01111110
01011010
00111100
00000000
00000000
POS 7 5,6

SPR 1e
00000000
01111110
01011010
01111110
01011010
01111110
00000000
00000000
POS 7 2,6

SPR 1f
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 7 2,5

SPR 1g
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 7 3,5

SPR 1h
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR 1i
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 7 5,5

SPR 1j
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 7 4,5

SPR 1k
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
POS 7 1,6

SPR 1l
01111111
10000000
10000000
10000000
10000000
10000000
10000000
10000000
POS 7 1,5

SPR 1m
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
POS 7 1,7

SPR 1n
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
POS 7 7,7

SPR 1o
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
POS 7 7,6

SPR 1p
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 7 6,5

SPR 1q
11111000
00000110
00000001
00000001
00000001
00000001
00000001
00000001
POS 7 7,5

SPR 1r
00111100
00111100
01111000
00111100
00111100
00011000
00011000
00000000
DLG SPR_l
POS 7 3,8

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG SPR_0
"""
I'm a cat{
  - {item "tea"} == 1 ?

  - else ?

}
"""

DLG ITM_0
You found a nice warm cup of tea

DLG SPR_1
Hello I'm looking for my friend, the rabbitHave you seen her?

DLG SPR_2
"""
{sequence
  - Hello I'm looking for my friend, the rabbit
    Have you seen her?
    
    {wvy}OOHHHH - the bass keeps vibing and I KEEP JIVING{wvy}
    {wvy}I must've missed your
    floppy friend{wvy}
  - {wvy}HMMM YEAHHH{wvy}
}
"""

DLG SPR_3
It doesn't look pleasing in there...

DLG SPR_4
"""
{cycle
  - You wash your hands

  - Your hair looks nice
}
"""

DLG SPR_5
"""
{cycle
  - You wash your hands
  - Your hair looks great
}
"""

DLG SPR_6
"""
{cycle
  - OCCUPIED
  - OI
    IM IN HERE
}
"""

DLG SPR_7
"""
{sequence
  - It's empty
}
"""

DLG SPR_8
It's empty

DLG SPR_9
"""
{sequence
  - 
  - 
}
"""

DLG SPR_a
"""
{sequence
  - I'm looking Barbra, my bunny
    buddy?
    Have you seen a fluffy dancer?
    
    {wvy}WHATTT?{wvy}
    
    {wvy}YEAH I LIKE SKA MUSIC TOO{wvy}
  - {wvy}Skippidee pop, whop, floo drop{wvy}
}
"""

DLG SPR_b
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_c
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_d
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_e
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_f
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_g
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_h
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_i
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_j
"""
{shuffle
  - The rhythm is grooving
  - The beats are bumping
  - You can feel the bass
  - This song is pretty catchy
  - You kinda like this one
  - This song reminds you of Barbra..
  - You haven't heard this since uni
  - The music is pretty loud
  - You like this singer
  - The music is lively
  - You feel like dancing
  - WUBB, WUBB, SWUBB
  - BOOM, KSSS, CHAAA, TSSSS
  - UNNCEE, TSSS, KAAA
  - BOOOM, BOOM, WUBB
}
"""

DLG SPR_k
"""
{sequence
  - Hello Mr. DJ, sorry to errr- bother you but
    It's just that my friend is missing, a sweet rabbit
    named Barbra
    
    Sorry love, no recommendations
    I planned this setlist two months ago.

  - You'll love this next one
}
"""

DLG SPR_l
"""
{sequence
  - Barbra! I found you
    
    I though't you'd left without me
    
    {clr3}No way, it's too loud in there
    
    I just stepped outside for some fresh air{clr3}
    
    {clr3}Let's go home{clr3}
  - {clr3}Let's go home{clr3}
}
"""

DLG SPR_m
I'd rather not go back in there

END 0
And you go home, The End!

END undefined


END undefinee


END undefinef


END undefineg


END undefineh


END undefinei


END undefinej


END undefinek


END undefinel


VAR a
42


