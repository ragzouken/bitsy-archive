Bunker

# BITSY VERSION 3.2

! ROOM_FORMAT 1

PAL 0
67,58,50
0,0,0
176,167,147

PAL 1
90,78,48
50,41,24
170,192,148

PAL 2
139,107,65
74,61,36
156,202,125

PAL 3
91,187,244
142,104,62
100,179,47

ROOM 0
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,c,f,c,c,c,c,c,b,b,b,b
b,b,b,b,b,c,f,c,c,c,c,c,b,b,b,b
b,b,b,b,b,c,f,c,c,c,c,c,b,b,b,b
b,b,b,b,b,0,f,0,0,0,0,0,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
WAL c,b
EXT 6,0 1 6,15
PAL 0

ROOM 1
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
WAL c,b
EXT 6,15 0 6,0
EXT 6,0 2 6,15
PAL 0

ROOM 2
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
WAL c,b
EXT 6,15 1 6,0
EXT 6,0 3 6,15
PAL 1

ROOM 3
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
WAL c,b
EXT 6,15 2 6,0
EXT 6,0 4 6,15
PAL 2

ROOM 4
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,0,0,0,c,c,c,c,c,c,c,c,c,c,c,c
c,0,0,0,c,c,c,c,c,c,c,c,c,c,c,c
c,0,0,0,c,c,c,c,c,c,c,c,c,c,c,c
c,c,h,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,h,c,c,c,c,c,c,c,c,c,c,c,c,0
e,e,g,e,e,e,e,e,e,e,i,e,e,e,e,e
b,b,b,b,b,b,f,b,b,b,b,b,b,b,b,b
WAL b,c
EXT 6,15 3 6,0
EXT 0,14 5 15,14
PAL 3

ROOM 5
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,h,c,c,c,c,c
c,c,c,c,c,c,l,c,c,c,h,c,c,c,c,c
c,c,c,c,c,c,m,k,k,k,j,c,c,c,c,c
c,c,c,c,c,c,l,c,c,c,h,c,c,c,c,c
c,c,c,c,c,c,l,c,c,c,h,c,c,c,c,c
e,0,0,e,e,e,i,e,e,e,g,e,e,e,e,e
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
WAL b,c
EXT 15,14 4 0,14
END 0 0,14
PAL 3

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL f
00111100
00100100
00111100
00100100
00111100
00100100
00111100
00100100

TIL g
00111100
00111100
00111100
00111100
01111110
01111110
01111110
11111111

TIL h
00111100
00111100
00111100
00111100
00111100
00111100
00111100
00111100

TIL i
00011000
00011000
00011000
00011000
00011000
00011000
00111100
01111110

TIL j
00111100
00111100
11111100
11111100
11111100
11111100
00111100
00111100

TIL k
00000000
00000000
11111111
11111111
11111111
11111111
00000000
00000000

TIL l
00011000
00011000
00011000
00011000
00011000
00011000
00011000
00011000

TIL m
00011000
00011000
00011111
00011111
00011111
00011111
00011000
00011000

SPR A
00011000
00011000
00111100
01011010
01011010
00111100
00100100
00100100
POS 0 9,6

SPR a
00000000
00000000
00000010
00000010
00111110
01001010
01010010
01111110
>
00000000
00000000
00000010
00000010
00111110
01010010
01001010
01111110
POS 0 11,6

SPR b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 5 10,7

SPR c
11111111
11111111
11111111
11111111
11111111
11111111
11111111
00111100
POS 5 10,8

SPR d
11111111
01111111
01111111
00111111
00011111
00000111
00000001
00000000
POS 5 9,8

SPR e
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 5 9,7

SPR f
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 5 11,7

SPR g
00111100
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 5 10,6

SPR h
00000000
00000001
00000111
00011111
00111111
01111111
01111111
11111111
POS 5 9,6

SPR i
00000000
10000000
11100000
11111000
11111100
11111110
11111110
11111111
POS 5 11,6

SPR j
11111111
11111110
11111110
11111100
11111000
11100000
10000000
00000000
POS 5 11,8

SPR k
00111100
01111110
11111111
11111111
11111111
11111111
01111110
00111100
POS 4 10,13

SPR l
00011000
00011000
01111110
01111110
10111101
10111101
00100100
00100100
POS 4 13,14

SPR m
00000000
00000000
00000010
00000010
00111110
01001010
01010010
01111110
>
00000000
00000000
00000010
00000010
00111110
01010010
01001010
01111110
POS 5 8,14

SPR n
00111100
01111110
11111111
11111111
11111111
11111111
01111110
00111100
POS 5 6,9

SPR o
11111111
11111110
11111110
11111100
11111000
11100000
10000000
00000000
POS 4 3,11

SPR p
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 4 2,10

SPR q
00000000
10000000
11100000
11111000
11111100
11111110
11111110
11111111
POS 4 3,9

SPR r
00000000
00000001
00000111
00011111
00111111
01111111
01111111
11111111
POS 4 1,9

SPR s
00111100
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 4 2,9

SPR t
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 4 3,10

SPR u
11111111
01111111
01111111
00111111
00011111
00000111
00000001
00000000
POS 4 1,11

SPR v
11111111
11111111
11111111
11111111
11111111
11111111
11111111
00111100
POS 4 2,11

SPR w
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
POS 4 1,10

DLG a
... no signal for 2 years. Is it safe?

DLG c
It's been so long since I climbed a tree.

DLG k
Am I dreaming?

DLG l
"Dude, how long have you been down there? You look like shit."

DLG m
"Sport sports sports! Someone scored the most goals ever! There's an underdog! Root for the home team!" 

DLG v
I can't believe it. Trees! The world is alive!

DLG n


END 0
In retrospect, you should have known you wouldn't get signal 50ft underground. Guess you wasted two years of your life.

END undefined
The End.

