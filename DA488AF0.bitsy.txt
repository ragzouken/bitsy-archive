
QuelqueChose Jam le jeu !

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
171,41,163
255,230,42
167,255,43

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
b,b,b,a,0,0,0,0,0,0,0,0,0,0,0,0
c,c,c,b,0,0,0,0,0,0,0,0,0,0,0,0
c,c,c,d,0,0,0,0,0,0,0,0,0,0,0,0
NAME entrance
EXT 3,15 2 3,15
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,a,a,a,a,0,0,0,0,0
b,b,b,a,0,0,0,a,b,b,a,0,0,0,0,0
c,c,c,b,b,b,b,b,c,0,b,0,0,0,0,0
c,c,e,c,c,c,c,c,c,c,c,0,0,0,0,0
NAME beginning
ITM 0 9,14
EXT 10,15 3 10,15
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,a,a,a,a,a,a,a,c,c,a
0,0,0,0,0,0,b,b,b,b,b,b,a,c,c,a
a,a,a,a,0,0,0,a,a,a,a,0,a,c,c,a
b,b,b,a,0,0,0,a,b,b,a,0,a,c,c,a
c,c,c,b,b,b,b,b,c,c,b,b,b,c,c,a
c,c,c,c,c,c,c,c,c,e,c,c,c,c,b,b
NAME planning
EXT 13,10 4 13,10
EXT 14,10 4 14,10
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,a,a,a,0,0,0,0,0,0,0,0,0
0,0,0,a,a,b,a,a,a,a,a,a,a,a,a,a
0,0,0,b,b,0,b,b,b,b,b,b,b,b,b,a
0,0,0,0,c,c,c,c,c,c,c,c,c,c,c,a
0,0,0,0,a,a,a,a,a,a,a,a,a,c,c,a
0,0,0,0,b,b,b,b,b,b,b,b,a,e,e,a
a,a,a,a,0,0,0,a,a,a,a,0,a,c,c,a
b,b,b,a,0,0,0,a,b,b,a,0,a,c,c,a
c,c,c,b,b,b,b,b,c,c,b,b,b,c,c,a
c,c,c,c,c,c,c,c,c,c,c,c,c,c,b,b
NAME beginning2
EXT 3,9 5 3,9
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,c,a,0,0,0,0,0,0,0,0,0,0,0,0
a,a,c,a,0,0,0,0,0,0,0,0,0,0,0,0
a,c,c,a,a,a,a,a,a,0,0,0,0,0,0,0
b,a,c,a,a,a,a,a,b,b,b,b,b,b,b,b
0,a,c,a,a,b,a,a,a,a,a,a,a,a,a,a
0,a,c,b,b,c,b,b,b,b,b,b,b,b,b,a
0,a,c,c,e,c,c,c,c,c,c,c,c,c,c,a
0,a,a,a,a,a,a,a,a,a,a,a,a,c,c,a
0,b,b,b,b,b,b,b,b,b,b,b,a,c,c,a
a,a,a,a,0,0,0,a,a,a,a,0,a,c,c,a
b,b,b,a,0,0,0,a,b,b,a,0,a,c,c,a
c,c,c,b,b,b,b,b,c,c,b,b,b,c,c,a
c,c,c,c,c,c,c,c,c,c,c,c,c,c,b,b
NAME prepare
EXT 2,3 6 2,3
PAL 0

ROOM 6
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,a,b,b,b,b,b,b,b,b,b,b,b,b,b,a
0,a,c,c,c,c,c,c,c,c,c,c,c,c,c,a
0,a,c,a,a,a,a,a,a,c,c,c,c,c,c,a
a,a,e,a,a,a,a,a,a,c,c,c,c,c,c,a
a,c,c,a,a,a,a,a,a,c,c,c,c,c,c,d
b,a,c,a,a,a,a,a,b,b,b,b,b,b,b,b
0,a,c,a,a,b,a,a,a,a,a,a,a,a,a,a
0,a,c,b,b,c,b,b,b,b,b,b,b,b,b,a
0,a,c,c,c,c,c,c,c,c,c,c,c,c,c,a
0,a,a,a,a,a,a,a,a,a,a,a,a,c,c,a
0,b,b,b,b,b,b,b,b,b,b,b,a,c,c,a
a,a,a,a,0,0,0,a,a,a,a,0,a,c,c,a
b,b,b,a,0,0,0,a,b,b,a,0,a,c,c,a
c,c,c,b,b,b,b,b,c,c,b,b,b,c,c,a
c,c,c,c,c,c,c,c,c,c,c,c,c,c,b,b
NAME Jamming
END undefined 15,5
PAL 0

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL b
11111111
11111111
11111111
11111111
00000000
00000000
11111111
00000000
WAL true

TIL c
00000000
00000000
00000000
00000000
00000000
00000010
00000110
00000000

TIL d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL e
00000000
01111110
01000010
01010010
01001010
01000010
01111110
00000000
WAL true

SPR A
00011000
00011000
00010000
00111000
01010100
00010000
00101000
00101100
>
00000000
00011000
00011000
00010000
00111000
01010100
00101000
00101100
POS 0 0,14

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
>
00000000
00000000
00000000
01010001
01110010
01111100
00111100
00100100
DLG SPR_0
POS 0 2,14

SPR b
00000000
11111111
10000001
10101101
10000001
11111111
00011000
00111100
DLG SPR_1
POS 2 9,14

SPR c
00000000
11111111
10000001
10101101
10000001
11111111
00011000
00111100
DLG SPR_2
POS 3 14,13

SPR d
00000000
11111111
10100001
10100101
10000101
11111111
00011000
00111100
DLG SPR_3
POS 4 5,8

SPR e
00000000
11111111
11000001
10100111
10000101
11111111
00011000
00111100
DLG SPR_4
POS 5 1,5

SPR f
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_5
POS 6 14,2

SPR g
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_6
POS 6 14,4

SPR h
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_7
POS 6 12,2

SPR i
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_8
POS 6 12,4

SPR j
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_9
POS 6 10,2

SPR k
00000000
00011111
00010001
11110001
10111111
10100100
11101110
00000000
DLG SPR_a
POS 6 10,4

ITM 0
00000000
11111111
10000001
10110101
10000001
11111111
00011000
00111100
NAME tea
DLG ITM_0

DLG SPR_0
Lol tu vas etre en retard pour la jam !

DLG ITM_0
"""
DiscordLCV : 
Jok : lol on pourrait faire une jam.
Damastes : ouiiiii
Baptiste : lol ui
Weis : on peut faire payer l'entree !
Yuriky : et sinon est-ce que vous connaissez Okami ?
"""

DLG SPR_1
"""
DiscordLCV : 
Jok : lol on pourrait faire une jam.
Damastes : ouiiiii
Baptiste : lol ui
Weis : on peut faire payer l'entree !
Yuriky : et sinon est-ce que vous connaissez Okami ?
"""

DLG SPR_2
"""
DiscordLCV :
X : maaaiiis moi je ne sais pas quoi faire !!!
LCV : ce n'est pas grave !
Minrav : oui ! il faut trouver quelque chose pour tout le monde !
Esteban : il faudrait trouver un nom pour la jam et une date !
"""

DLG SPR_3
"""
DiscordLCV :
Esteban : Alors si vous voulez, on peut faire ca la semaine prochaine ?
A : pas la
B : pas la
C : pas la
D : pas la
E : pas la
F : pas la
Jok : alors pendant les vacances de noel ?
A : pas la
B : pas la
C : pas la
D : la
E : pas la
F : pas la
Esteban & Jok : OK, on fait ca pendant noel alors !
"""

DLG SPR_4
"""
DiscordLCV :
Esteban : bon on a toujours pas le nom de la jam...
Jok : bah on n'a qu'a dire que c'est la QuelqueChose Jam !?!
Esteban : ...


...



...


Esteban : OK ! 
"""

DLG SPR_5
"""
DiscordLCV :
Yuriky : finalement, je suis en train de faire un FMV "escape room de ma chambre" !
"""

DLG SPR_6
"""
DiscordLCV :
Tifor : j'organise une exposition photo !!! 
"""

DLG SPR_7
"""
DiscordLCV :
Damastes : c'est vrai que Fugl a des problemes aujourd'hui...
"""

DLG SPR_8
"""
DiscordLCV :
Weis : Bordel, j'arrive pas a looter les objets que je veux pendant mon event !!
(J'ai une idee, je vais faire de la sous-traitance pour le projet Blow Up)
"""

DLG SPR_9
"""
DiscordLCV :
L'homme de Sel : damned, j'attendais Godot pour faire mon jeu, mais en fait, il est nul.
"""

DLG SPR_a
"""
DiscordLCV :
Esteban : c'est terrible javou.
"""

END 0


END undefined
Wow ! c'est commme ca que la "QuelqueChose Jam" est nee !

VAR a
42


