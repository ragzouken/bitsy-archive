Write your game's title here

# BITSY VERSION 3.5

! ROOM_FORMAT 1

PAL 0
20,0,252
250,180,63
221,160,34

PAL 1
255,255,255
255,255,255
255,255,255

ROOM 0
0,0,0,0,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,a,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,0,0,0,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,0,0,a,a,a,a,a,a,a,a
a,a,a,a,a,a,0,0,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,0,0,0,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,0,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,0,0,a,a,a,a,a
a,a,a,a,a,a,a,a,a,0,0,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,0,0,0,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,a,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
WAL a
EXT 9,9 1 8,9
EXT 0,0 0 0,0
EXT 15,14 1 8,9
PAL 0

ROOM 1
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,a,0,0,0,0,a,a,a,a,a,a
a,a,0,0,0,a,0,0,0,0,0,0,a,a,a,a
a,a,0,0,a,a,0,a,a,a,0,0,a,a,a,a
a,a,0,0,a,a,0,a,0,a,a,0,0,a,a,a
a,a,0,0,0,a,0,0,0,0,a,0,0,a,a,a
a,a,0,0,0,a,a,0,0,0,a,0,0,a,a,a
a,a,a,0,0,0,a,a,a,a,a,0,0,a,a,a
a,a,a,a,0,0,0,0,a,a,0,0,0,a,a,a
a,a,a,a,a,a,0,0,0,0,0,0,a,a,a,a
a,a,a,a,a,a,a,a,0,0,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
WAL a
EXT 0,0 2 0,0
PAL 0

ROOM 2
0,0,0,0,a,a,0,0,0,0,0,0,0,0,0,0
b,b,0,0,b,b,0,0,0,0,0,0,0,0,0,0
b,b,0,0,b,b,0,0,0,0,0,0,0,0,0,0
b,b,0,0,b,b,0,0,0,0,0,0,0,0,0,0
a,a,0,0,a,b,0,0,0,0,0,0,0,0,0,0
a,a,0,0,a,a,a,a,a,a,a,a,a,0,0,0
a,a,0,0,0,0,0,0,0,0,0,a,a,0,0,0
a,a,0,a,a,b,b,b,0,0,b,b,b,b,b,b
a,a,0,a,a,a,a,0,0,a,b,b,a,a,a,a
a,a,0,b,a,a,0,0,0,0,0,0,0,0,0,0
b,b,0,b,b,b,b,b,b,b,b,0,0,b,b,a
b,b,0,a,b,b,b,b,b,b,b,0,0,a,a,a
b,b,0,0,b,b,b,b,b,0,0,0,0,a,a,a
b,b,0,0,0,0,0,0,0,0,b,b,0,0,0,0
b,b,a,a,a,b,b,b,b,a,b,b,b,b,b,0
b,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 15,14 3 15,11
EXT 2,13 2 10,9
EXT 0,0 0 0,0
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,a,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,0,a,a,a,a,a,a,a,a,a,a,a,a,a
0,a,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,a,a,a,a,a,a,a,a,a,a,a,a,a,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
WAL a
EXT 0,0 4 15,11
PAL 0

ROOM 4
0,0,0,0,0,0,a,a,0,0,0,0,0,0,0,0
0,0,a,a,0,0,0,0,a,a,0,0,0,0,0,0
a,0,0,0,a,a,0,0,0,0,a,a,0,0,0,0
a,a,a,0,0,0,a,a,0,0,0,0,a,a,0,0
a,a,a,a,a,0,0,0,a,a,a,0,0,0,a,a
a,a,a,a,a,a,a,0,0,0,a,a,a,0,0,0
a,a,a,a,a,a,a,a,a,0,0,0,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 0,11 4 15,9
EXT 15,3 5 3,7
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,a,a,a,a,a,a,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,a,0,a,0
0,a,0,a,a,a,a,a,a,a,a,0,a,0,a,0
0,a,0,0,0,0,0,0,0,0,a,0,a,0,a,0
0,a,0,0,0,a,a,a,a,0,a,0,a,0,a,0
0,a,0,0,0,a,0,0,a,0,a,0,a,0,a,0
0,a,0,a,0,a,0,0,0,0,a,0,a,0,a,0
0,a,0,a,0,a,a,a,a,a,a,0,a,0,a,0
0,a,0,a,0,0,0,0,0,0,0,0,a,0,a,0
0,a,0,a,a,a,a,a,a,a,a,a,a,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
EXT 3,8 5 0,2
EXT 7,8 6 7,7
PAL 0

ROOM 6
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,0,0,0,0,0,a,0,0,a,a
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,a,0,0,0,0,a,a,a,a,a
a,a,a,a,a,0,a,0,0,a,0,a,a,a,a,a
a,a,a,a,a,0,a,0,0,a,0,a,a,a,a,0
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,0,a,a,a,a,a
a,a,a,a,a,0,0,0,0,0,0,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
EXT 7,12 6 12,2
EXT 13,2 7 0,0
PAL 0

ROOM 7
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,a,0,0,a,a,a,a,a,0,0,a,a,a
a,a,0,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,0,0,0,a,a,a,a,a,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,0,0,a,a,a,a,a,a,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
WAL a
EXT 7,3 7 11,1
EXT 12,1 7 13,3
EXT 14,3 7 0,7
EXT 5,8 7 5,8
EXT 2,7 7 5,8
PAL 0

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL b
11111111
11111111
10000000
10011111
10011000
10011011
10011010
10000010

SPR A
00111100
00100100
00111100
00111100
01011010
10011001
00100100
01000010
>
00111100
10100101
01111110
00111100
00011000
00011000
00100100
00100100
POS 0 1,0

SPR a
00000000
00011000
00111100
01100010
01101110
01100010
00111100
00011000
POS 7 7,8

SPR b
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 0 9,9

SPR c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR d
00000000
00000000
10100000
11100000
11100000
01111100
01111010
01001000
>
00000000
00000000
10100000
11101000
11100100
01111100
01111000
01001000
POS 2 12,11

DLG a
I'm kat the ultimate cat

DLG b
i am kat the ultimate cat

DLG d
i am super cat the super cat

