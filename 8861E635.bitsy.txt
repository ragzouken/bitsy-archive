
Fleabag Feline

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
23,23,190
255,154,40
0,0,0

PAL 1
14,87,22
57,208,227
43,50,171

PAL 2
108,145,238
224,255,160
173,40,49

ROOM 0
0,0,0,0,0,0,0,e,e,e,e,e,e,e,e,e
e,e,e,e,e,0,0,e,e,0,0,b,0,0,e,0
e,h,h,0,e,e,e,e,e,0,0,c,c,a,0,0
0,h,h,0,0,0,0,0,0,0,0,c,c,a,b,e
g,a,a,0,0,0,0,0,0,0,0,c,a,a,b,e
e,a,a,0,0,a,c,c,c,b,a,a,b,e,e,e
e,a,a,0,a,c,c,c,c,c,b,a,b,e,e,e
e,a,a,0,c,c,b,b,b,b,a,b,b,b,b,e
e,a,a,c,c,c,a,b,b,a,b,b,a,b,b,b
e,0,0,0,a,b,b,a,a,b,a,a,0,0,0,e
e,0,0,0,a,b,a,a,b,b,a,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,a,0,e,e,e
e,e,e,0,a,b,0,e,e,0,b,a,0,e,e,e
e,e,e,0,0,b,0,e,e,0,0,a,0,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,0
NAME cat1
ITM 0 2,4
ITM 1 14,8
ITM 4 6,10
ITM 4 7,10
ITM 4 8,10
ITM 4 9,10
ITM 5 1,3
ITM 3 5,8
EXT 15,8 1 0,4
PAL 0

ROOM 1
0,0,0,0,0,0,0,e,e,e,e,e,e,e,e,e
e,e,e,e,e,0,0,e,e,0,0,b,0,0,e,e
e,0,0,0,e,e,e,e,e,0,0,c,c,a,0,e
e,0,0,0,0,0,0,0,0,0,0,c,c,f,b,e
a,a,a,0,0,0,0,0,0,0,0,c,a,a,b,e
e,a,c,0,0,a,c,c,c,b,a,a,b,e,e,e
e,a,c,0,a,c,c,c,c,c,b,a,b,e,e,e
e,a,c,0,c,c,b,b,b,b,a,b,b,b,b,e
e,a,c,c,c,c,a,b,b,a,b,b,a,b,b,b
e,0,0,0,a,b,b,a,a,b,a,a,0,0,0,e
e,0,0,0,a,b,a,a,b,b,a,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,a,0,e,e,e
e,e,e,0,a,b,0,e,e,0,b,a,0,e,e,e
e,e,e,0,0,b,0,e,e,0,0,a,0,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
NAME cat2
ITM 3 7,9
ITM 3 5,6
ITM 5 12,3
ITM 2 10,5
ITM 1 15,8
EXT 15,8 2 0,4
PAL 1

ROOM 2
0,0,0,0,0,0,0,e,e,e,e,e,e,e,e,e
e,e,e,e,e,0,0,e,e,0,0,b,0,0,e,e
e,0,0,0,e,e,e,e,e,0,0,c,c,a,0,e
e,0,0,0,0,0,0,0,0,0,0,c,c,f,b,e
a,a,a,0,0,0,0,0,0,0,0,c,a,a,b,e
e,a,c,0,0,a,c,c,c,b,a,a,b,e,e,e
e,a,c,0,a,c,c,c,c,c,b,a,b,e,e,e
e,a,c,0,c,c,b,b,b,b,a,b,b,b,b,e
e,a,c,c,c,c,a,b,b,a,b,b,a,b,b,b
e,0,0,0,a,b,b,a,a,b,a,a,0,0,0,e
e,0,0,0,a,b,a,a,b,b,a,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,b,0,0,0,e
e,e,e,0,a,b,0,0,0,0,b,a,0,e,e,e
e,e,e,0,a,b,0,e,e,0,b,a,0,e,e,e
e,e,e,0,0,b,0,e,e,0,0,a,0,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
NAME cat3
ITM 3 13,11
ITM 3 7,5
ITM 3 3,9
ITM 3 11,1
ITM 3 7,7
ITM 3 11,5
ITM 3 12,5
ITM 3 11,12
ITM 3 10,9
ITM 2 12,3
ITM 1 11,7
ITM 0 5,9
ITM 0 14,3
ITM 5 14,8
ITM 5 5,7
ITM 5 4,4
ITM 3 6,11
EXT 0,4 1 15,8
EXT 15,8 0 0,4
PAL 2

TIL a
00000100
00000100
00001001
00010010
00100100
01001001
10010010
00100100

TIL b
11000100
11010011
00101010
01011100
10101010
00000101
01001011
00010010

TIL c
10001110
10010000
00110001
01100110
10001000
00110001
01000010
10000100

TIL d
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL f
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000

TIL g
00000100
00000100
00001001
00010010
00100100
01001001
10010010
00100100
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
00000100
00000100
00001001
00010010
00100100
01001001
10010010
00100100

SPR A
00011000
00110000
00011110
11111010
00001110
00011100
00010110
00010000
POS 0 10,3

SPR a
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000
NAME cat 1
DLG SPR_0
POS 0 13,3

SPR b
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000
DLG SPR_1
POS 0 13,3

SPR c
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000
NAME cat 3
DLG SPR_2
POS 2 13,3

SPR d
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000
NAME cat 2
DLG SPR_3
POS 1 13,3

ITM 0
00000000
00000000
00000110
00001100
00010100
00111000
00100000
00000000
NAME flea1
DLG ITM_0

ITM 1
00000000
00000000
00000110
00001100
00011100
00111000
00100000
00000000
NAME flea2
DLG ITM_1

ITM 2
00000000
00001100
00011100
00111000
00110000
00110000
00100000
00000000
NAME flea3
DLG ITM_2

ITM 3
00000000
00000010
00000110
00001100
00011100
00011000
00100000
00000000
NAME flea4

ITM 4
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME scratch
DLG ITM_3

ITM 5
00000000
00000000
00000010
00001100
00011100
00111000
00110000
00000000
NAME flea 5
DLG ITM_4

DLG SPR_0
"""
{
  - {item "flea 5"} < 5 ?
    We're such itchy cats! Can you help me and my friends? Come back when you're finished and there may be treat for you...
  - {item "flea 5"} == 5 ?
    Phew! Thank you so much, you're puRrRrfect! Here's your treat, a big.....{rbw}{shk} lick on the face {rbw}{shk}
  - {item "scratch"} == 4 ?
    Ooooo thank you for a lovely belly rub!
}
"""

DLG SPR_1
"""
{
  - {item "flea1"} == 20 ?
    Thank you so much, I feel {wvy}ppuuuuurrrfect!{wvy}
  - {item "flea1"} < 20 ?
    Ow Ow Ow I'm covered in fleas!
}
"""

DLG ITM_0
We're naughty fleas!

DLG SPR_2
This is driving me craazZzYy!!

DLG SPR_3
sooo itchy!

DLG ITM_1
You won't catch my mates!

DLG ITM_2
You can't catch us all!!!!

DLG ITM_3
"""
**scritchy scratch***{
  - {item "0"} == 1 ?

  - else ?

}
"""

DLG ITM_4
**eek!**

VAR a
42


