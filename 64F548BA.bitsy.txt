
Kissy Baby

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME 1
204,76,76
250,245,244
0,0,0

PAL 1
NAME 2
255,255,255
0,0,0
255,87,116

ROOM 0
c,c,c,c,c,c,a,a,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,a,0,b,b,b,b,b,0,a,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,a,0,b,b,b,b,b,0,a,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,a,0,b,b,b,b,b,0,a,c,c,c,c
c,c,c,c,a,a,a,b,a,a,a,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
NAME kiss chamber
ITM 0 7,1
EXT 7,15 2 7,1
PAL 0

ROOM 1
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,a,b,b,b,a,c,c,c,c,c,c
c,c,c,c,a,b,b,b,b,b,a,c,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,c,a,a,b,b,b,a,a,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
NAME hi
EXT 7,0 0 7,14
PAL 1

ROOM 2
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,a,b,b,b,a,c,c,c,c,c,c
c,c,c,c,a,b,b,b,b,b,a,c,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,a,b,b,b,b,b,b,b,a,c,c,c,c
c,c,c,c,a,a,b,b,b,a,a,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
c,c,c,c,c,c,a,b,a,c,c,c,c,c,c,c
NAME bye
EXT 7,0 0 7,14
END undefined 7,15
PAL 1

TIL a
11111111
10000111
10000001
11000001
01000011
11100101
10011001
11111111
WAL true

TIL b
01010101
01010101
01010101
01010101
01010101
01010101
01010101
01010101
>
10101010
10101010
10101010
10101010
10101010
10101010
10101010
10101010
WAL false

TIL c
11111111
00000000
11111111
00000000
11111111
00000000
11111111
00000000
>
00000000
11111111
00000000
11111111
00000000
11111111
00000000
11111111

SPR A
00000000
00000000
00111100
01111110
11000011
01111110
00111100
00000000
>
00000000
00111100
01111110
11000011
01111110
00111100
00000000
00000000
POS 1 7,15

SPR a
01111110
10100101
01000010
00100100
00011000
00011000
00111100
01111110
>
00000000
01111110
10100101
01000010
00100100
00011000
00111100
01111110
DLG SPR_0
POS 0 4,3

SPR b
00000000
00111100
01011010
00100100
00111100
01011010
00011000
00011000
>
00000000
00000000
00111100
01011010
00100100
00111100
01011010
00011000
DLG SPR_1
POS 0 10,13

SPR c
01100000
11100000
01111000
01111111
00101110
00010110
01000110
01011010
>
01110010
00111111
01010011
10000000
01100010
01000000
10011100
00100110
DLG SPR_2
POS 0 4,13

SPR d
00000000
00111100
01000010
01111110
01010100
00101010
01111110
00000000
>
00000000
00111100
01000010
01111110
00101010
01010100
01111110
00000000
DLG SPR_3
POS 0 4,8

SPR e
01111110
11000011
10100101
11000011
10011001
10100101
11011011
01111110
>
01111110
11011011
10100101
10100101
10000001
10111101
11100111
01111110
DLG SPR_4
POS 0 10,8

SPR f
10111101
01010110
00111100
00011000
00011000
00100100
01000010
10000001
>
10111101
01101010
00111100
00011000
00011000
00100100
01000010
10000001
DLG SPR_5
POS 0 10,3

SPR g
01111110
10100101
01000010
00100100
00011000
00011000
00111100
01111110
>
00000000
01111110
10100101
01000010
00100100
00011000
00111100
01111110
DLG SPR_6
POS 2 7,12

SPR h
01111110
10100101
01000010
00100100
00011000
00011000
00111100
01111110
>
00000000
01111110
10100101
01000010
00100100
00011000
00111100
01111110
DLG SPR_7
POS 1 7,12

ITM 0
01111110
01010110
01101010
01010110
01101010
01010110
01111110
00011000
NAME kissy note
DLG ITM_0

ITM 1
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

DLG ITM_0
{rbw}show this to the one you'll kiss{rbw}

DLG SPR_0
"""
{
  - {item "0"} == 1 ?
    {rbw}you kiss the cutest friend smooch smooch smooch{rbw}
  - else ?
    kiss me cause I'm CUTE!
}
"""

DLG SPR_1
"""
{
  - {item "0"} == 1 ?
    {rbw}you kiss the friend that needed it most{rbw}
  - else ?
    I could really use a kiss!
}
"""

DLG SPR_2
"""
{
  - {item "0"} == 1 ?
    {rbw}you kiss everything and everyone you've ever known all that will be all that ever was and nothing will ever be the same again{rbw}
  - else ?
    kiss the void
}
"""

DLG SPR_3
"""
{
  - {item "0"} == 1 ?
    {rbw}the burger's kiss is savoury, salty, everything you needed.{rbw}
  - else ?
    kiss a burger if you want a burger
}
"""

DLG SPR_4
"""
{
  - {item "0"} == 1 ?
    {rbw}you can taste the cosmos{rbw}
  - else ?
    i'm the king of all kisses
}
"""

DLG SPR_5
"""
{
  - {item "0"} == 1 ?
    {rbw}passion fuelled by torment{rbw}
  - else ?
    free me from my shackles via lip to lip extraction
}
"""

DLG SPR_6
"""
{
  - {item "0"} == 1 ?
    Thank you for kissing with us today.
    You're our number one kisstomer.
    
    Don't forget to kiss and tell your friends.
  - else ?
    u didn't even kiss anyone 
}
"""

DLG SPR_7
"""
Welcome to the kissy chamber.

Grab the kissy pass at the end of the hall for an all you can kiss buffet.
"""

END 0


END undefined
I'm not interested in being a 'lover.' I'm interested in only being love. - Ram Dass * * * * * a game by mr. boyfriend

VAR a
42


