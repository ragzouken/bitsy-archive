


# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
138,95,203
243,167,255
255,239,46

PAL 1
255,255,255
255,255,255
255,255,255

PAL 2
255,255,255
255,255,255
255,255,255

PAL 3
255,255,255
255,255,255
255,255,255

PAL 4
255,255,255
255,255,255
255,255,255

ROOM 0
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Pop
ITM 0 8,6
EXT 8,0 1 14,13
END undefined 0,0
PAL 0

ROOM 1
a,0,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,a,a,0,0,0,0,0,0,0,0,0,0,0,a
a,0,a,0,0,a,a,a,a,a,a,a,0,a,0,a
a,0,a,0,a,a,0,0,0,0,a,a,0,a,0,a
a,0,a,0,a,a,0,0,a,0,a,0,0,a,0,a
a,0,a,0,0,a,0,a,a,0,a,0,a,a,a,a
a,0,a,a,0,a,0,a,0,0,0,0,a,a,0,a
a,0,a,0,0,a,a,a,0,a,a,0,0,a,0,a
a,0,a,0,a,a,0,a,a,a,0,a,0,0,0,a
a,0,a,0,a,a,0,a,a,0,0,a,a,a,0,a
a,0,a,0,0,a,0,0,0,0,0,a,a,a,0,a
a,0,a,a,0,a,a,a,a,a,0,0,0,0,0,a
a,0,0,0,0,0,a,0,0,a,0,a,a,a,a,a
a,0,0,0,0,a,a,a,0,a,0,0,0,0,0,a
a,a,a,a,0,0,0,0,0,0,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Rock
ITM 0 1,1
EXT 1,0 2 1,1
PAL 0

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,a,0,0,a,0,a,0,a,0,a,a,a,0,a
a,0,a,a,0,a,0,0,0,0,0,a,a,a,0,a
a,0,a,a,0,0,0,0,a,0,0,0,0,0,0,a
a,0,0,0,0,0,a,a,a,0,a,a,a,a,a,a
a,0,a,a,a,0,a,0,0,0,a,0,0,0,0,a
a,0,0,a,0,0,a,a,a,0,a,0,a,a,0,a
a,0,a,a,0,0,0,0,a,0,a,0,a,a,0,a
a,0,a,a,a,a,a,0,a,0,a,0,a,a,0,a
a,0,0,0,0,0,a,0,a,0,a,0,a,a,0,a
a,0,a,a,0,0,a,0,a,0,a,0,a,a,0,a
a,0,0,a,0,a,a,0,a,0,a,0,a,a,0,a
a,0,0,a,0,a,0,0,a,0,0,0,0,a,0,a
a,0,a,a,0,a,a,a,a,0,a,a,a,a,0,a
a,0,0,a,0,a,0,0,0,0,0,a,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Hip
ITM 0 14,14
EXT 15,14 3 0,13
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,a,0,a,a,a,0,a,0,a,0,a,a,0
0,0,a,0,0,a,0,a,0,a,0,a,0,a,0,0
0,0,a,0,0,a,0,a,0,a,0,a,0,a,a,0
0,0,0,a,0,a,a,a,0,a,0,a,0,a,0,0
0,0,a,a,0,a,0,a,0,0,a,0,0,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,a,0,a,0,a,0,a,a,0,a,0,a,a,a
a,a,a,0,a,0,a,0,a,0,0,a,0,a,0,0
a,0,a,0,a,0,a,0,0,a,0,a,0,a,0,0
a,0,a,0,a,a,a,0,a,a,0,a,0,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Caca
ITM 0 14,13
EXT 15,13 4 8,15
PAL 0

ROOM 4
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,a,0,0,0,0,0,0,0,0,0,a,0,0,a
a,0,a,0,a,a,a,a,a,a,0,a,a,a,0,a
a,0,0,0,a,0,0,0,0,a,0,a,0,0,0,a
a,a,0,a,0,0,0,a,0,a,0,0,0,a,0,a
a,0,0,a,0,0,a,0,0,0,a,0,0,a,0,a
a,0,a,a,0,a,a,0,0,0,a,a,0,a,0,a
a,0,0,a,0,0,a,0,0,0,a,0,0,a,0,a
a,a,0,a,a,0,a,a,a,a,a,a,a,a,0,a
a,0,0,a,0,0,0,a,0,0,a,0,0,a,0,a
a,0,0,0,a,0,0,0,a,0,0,0,a,0,0,a
a,0,0,a,0,a,a,0,a,0,a,a,0,a,0,a
a,a,0,a,0,0,0,0,a,0,0,a,0,a,0,a
a,0,0,a,0,a,a,a,a,0,a,a,0,a,0,a
a,0,0,0,0,0,a,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
NAME Dificil
ITM 0 8,6
EXT 8,7 5 8,15
PAL 0

ROOM 5
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
NAME Final
END undefined 8,4
PAL 0

TIL a
00000000
00111100
01111110
11011011
01011010
01100110
01111110
01100110
>
00111100
01011010
11011011
01100110
01111110
01111110
01100110
00000000
NAME tree

SPR A
00000100
00001000
00111100
01111110
01011010
11011011
01100110
00111100
>
00000000
00000100
00001000
00111100
01111110
01111110
11011011
01100110
POS 0 8,10

SPR a
00000000
00000000
00000100
00001000
01111110
11111111
01011010
01100110
>
00000000
00000100
00001000
01111110
01011010
01011010
11100111
01111110
NAME Mini
DLG SPR_0
POS 5 8,5

SPR b
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

ITM 0
00000000
00000100
01111110
01000010
01000010
01111110
01011010
01010110
NAME tea
DLG ITM_0

DLG SPR_0
"""
{wvy}{rbw}Dance with me!
Iâ€™m a bitsy girl in a bitsy world!
Life in bits IS FANTASBIT!{rbw}{wvy}
"""

DLG SPR_1
"""
Dance with me!{sequence
  - 
  - 
}
"""

DLG SPR_2
"""
Dance with me!{rbw}{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_4
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_5
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_6
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_7
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_8
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_9
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_a
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_b
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_c
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_d
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_e
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_f
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_g
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_h
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_i
"""
{rbw}Dance with mea!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_j
"""
{rbw}Dance with me!{rbw}{sequence
  - 
  - 
}
"""

DLG SPR_k
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_l
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_m
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_n
"""
{rbw}Dance with me!{ / rbw}{sequence
  - U
  - 
}
"""

DLG SPR_o
"""
{rbw}Dance with me!{ / rbw}{sequence
  - U
  - 
}
"""

DLG SPR_p
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Up
  - 
}
"""

DLG SPR_q
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Up
  - 
}
"""

DLG SPR_r
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Ups
  - 
}
"""

DLG SPR_s
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Ups
  - 
}
"""

DLG SPR_t
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Up
  - 
}
"""

DLG SPR_u
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Up
  - 
}
"""

DLG SPR_v
"""
{rbw}Dance with me!{ / rbw}{sequence
  - U
  - 
}
"""

DLG SPR_w
"""
{rbw}Dance with me!{ / rbw}{sequence
  - U
  - 
}
"""

DLG SPR_x
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_y
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_z
"""
{rbw}Dance with me!{ / rbw}{sequence
  - O
  - 
}
"""

DLG SPR_10
"""
{rbw}Dance with me!{ / rbw}{sequence
  - O
  - 
}
"""

DLG SPR_11
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oo
  - 
}
"""

DLG SPR_12
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oo
  - 
}
"""

DLG SPR_13
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oop
  - 
}
"""

DLG SPR_14
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oop
  - 
}
"""

DLG SPR_15
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops
  - 
}
"""

DLG SPR_16
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops
  - 
}
"""

DLG SPR_17
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops!
  - 
}
"""

DLG SPR_18
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops!
  - 
}
"""

DLG SPR_19
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! 
  - 
}
"""

DLG SPR_1a
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! 
  - 
}
"""

DLG SPR_1b
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I
  - 
}
"""

DLG SPR_1c
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I
  - 
}
"""

DLG SPR_1d
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I 
  - 
}
"""

DLG SPR_1e
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I 
  - 
}
"""

DLG SPR_1f
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I d
  - 
}
"""

DLG SPR_1g
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I d
  - 
}
"""

DLG SPR_1h
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I di
  - 
}
"""

DLG SPR_1i
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I di
  - 
}
"""

DLG SPR_1j
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did
  - 
}
"""

DLG SPR_1k
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did
  - 
}
"""

DLG SPR_1l
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did 
  - 
}
"""

DLG SPR_1m
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did 
  - 
}
"""

DLG SPR_1n
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did i
  - 
}
"""

DLG SPR_1o
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did i
  - 
}
"""

DLG SPR_1p
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it
  - 
}
"""

DLG SPR_1q
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it
  - 
}
"""

DLG SPR_1r
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it 
  - 
}
"""

DLG SPR_1s
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it 
  - 
}
"""

DLG SPR_1t
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it a
  - 
}
"""

DLG SPR_1u
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it a
  - 
}
"""

DLG SPR_1v
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it ag
  - 
}
"""

DLG SPR_1w
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it ag
  - 
}
"""

DLG SPR_1x
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it aga
  - 
}
"""

DLG SPR_1y
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it aga
  - 
}
"""

DLG SPR_1z
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it agai
  - 
}
"""

DLG SPR_20
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it agai
  - 
}
"""

DLG SPR_21
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_22
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_23
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again!
  - 
}
"""

DLG SPR_24
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again!
  - 
}
"""

DLG SPR_25
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_26
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_27
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again!
  - 
}
"""

DLG SPR_28
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again!
  - 
}
"""

DLG SPR_29
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_2a
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it again
  - 
}
"""

DLG SPR_2b
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it agai
  - 
}
"""

DLG SPR_2c
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it agai
  - 
}
"""

DLG SPR_2d
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it aga
  - 
}
"""

DLG SPR_2e
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it aga
  - 
}
"""

DLG SPR_2f
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it ag
  - 
}
"""

DLG SPR_2g
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it ag
  - 
}
"""

DLG SPR_2h
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it a
  - 
}
"""

DLG SPR_2i
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it a
  - 
}
"""

DLG SPR_2j
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it 
  - 
}
"""

DLG SPR_2k
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it 
  - 
}
"""

DLG SPR_2l
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it
  - 
}
"""

DLG SPR_2m
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did it
  - 
}
"""

DLG SPR_2n
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did i
  - 
}
"""

DLG SPR_2o
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did i
  - 
}
"""

DLG SPR_2p
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did 
  - 
}
"""

DLG SPR_2q
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did 
  - 
}
"""

DLG SPR_2r
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did
  - 
}
"""

DLG SPR_2s
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I did
  - 
}
"""

DLG SPR_2t
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I di
  - 
}
"""

DLG SPR_2u
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I di
  - 
}
"""

DLG SPR_2v
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I d
  - 
}
"""

DLG SPR_2w
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I d
  - 
}
"""

DLG SPR_2x
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I 
  - 
}
"""

DLG SPR_2y
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I 
  - 
}
"""

DLG SPR_2z
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I
  - 
}
"""

DLG SPR_30
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! I
  - 
}
"""

DLG SPR_31
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! 
  - 
}
"""

DLG SPR_32
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops! 
  - 
}
"""

DLG SPR_33
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops!
  - 
}
"""

DLG SPR_34
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops!
  - 
}
"""

DLG SPR_35
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops
  - 
}
"""

DLG SPR_36
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oops
  - 
}
"""

DLG SPR_37
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oop
  - 
}
"""

DLG SPR_38
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oop
  - 
}
"""

DLG SPR_39
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oo
  - 
}
"""

DLG SPR_3a
"""
{rbw}Dance with me!{ / rbw}{sequence
  - Oo
  - 
}
"""

DLG SPR_3b
"""
{rbw}Dance with me!{ / rbw}{sequence
  - O
  - 
}
"""

DLG SPR_3c
"""
{rbw}Dance with me!{ / rbw}{sequence
  - O
  - 
}
"""

DLG SPR_3d
"""
{rbw}Dance with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3e
"""
{rbw}Danc with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3f
"""
{rbw}Dan with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3g
"""
{rbw}Da with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3h
"""
{rbw}D with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3i
"""
{rbw} with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3j
"""
{rbw}S with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3k
"""
{rbw}Si with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3l
"""
{rbw}Sin with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3m
"""
{rbw}Sing with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3n
"""
{rbw}Sing with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3o
"""
{rbw}Sing with me!{ / rbw}{sequence
  - 
  - 
}
"""

DLG SPR_3p
"""
{rbw}Sing with me!{ / rbw}{sequence
  - I
  - 
}
"""

DLG SPR_3q
"""
{rbw}Sing with me!{ / rbw}{sequence
  - I
  - 
}
"""

DLG SPR_3r
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™
  - 
}
"""

DLG SPR_3s
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™
  - 
}
"""

DLG SPR_3t
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m
  - 
}
"""

DLG SPR_3u
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m
  - 
}
"""

DLG SPR_3v
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m 
  - 
}
"""

DLG SPR_3w
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m 
  - 
}
"""

DLG SPR_3x
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a
  - 
}
"""

DLG SPR_3y
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a
  - 
}
"""

DLG SPR_3z
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a 
  - 
}
"""

DLG SPR_40
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a 
  - 
}
"""

DLG SPR_41
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a b
  - 
}
"""

DLG SPR_42
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a b
  - 
}
"""

DLG SPR_43
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a ba
  - 
}
"""

DLG SPR_44
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a ba
  - 
}
"""

DLG SPR_45
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a bar
  - 
}
"""

DLG SPR_46
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a bar
  - 
}
"""

DLG SPR_47
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barb
  - 
}
"""

DLG SPR_48
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barb
  - 
}
"""

DLG SPR_49
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbj
  - 
}
"""

DLG SPR_4a
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbj
  - 
}
"""

DLG SPR_4b
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbje
  - 
}
"""

DLG SPR_4c
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbje
  - 
}
"""

DLG SPR_4d
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbj
  - 
}
"""

DLG SPR_4e
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbj
  - 
}
"""

DLG SPR_4f
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barb
  - 
}
"""

DLG SPR_4g
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barb
  - 
}
"""

DLG SPR_4h
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbi
  - 
}
"""

DLG SPR_4i
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbi
  - 
}
"""

DLG SPR_4j
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie
  - 
}
"""

DLG SPR_4k
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie
  - 
}
"""

DLG SPR_4l
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie 
  - 
}
"""

DLG SPR_4m
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie 
  - 
}
"""

DLG SPR_4n
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie g
  - 
}
"""

DLG SPR_4o
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie g
  - 
}
"""

DLG SPR_4p
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie gu
  - 
}
"""

DLG SPR_4q
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie gu
  - 
}
"""

DLG SPR_4r
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie gur
  - 
}
"""

DLG SPR_4s
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie gur
  - 
}
"""

DLG SPR_4t
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie gurk
  - 
}
"""

DLG SPR_4u
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl 
  - 
}
"""

DLG SPR_4v
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl,
  - 
}
"""

DLG SPR_4w
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl,
  - 
}
"""

DLG SPR_4x
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, 
  - 
}
"""

DLG SPR_4y
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, 
  - 
}
"""

DLG SPR_4z
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, i
  - 
}
"""

DLG SPR_50
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, i
  - 
}
"""

DLG SPR_51
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in
  - 
}
"""

DLG SPR_52
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in
  - 
}
"""

DLG SPR_53
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in 
  - 
}
"""

DLG SPR_54
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in 
  - 
}
"""

DLG SPR_55
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a
  - 
}
"""

DLG SPR_56
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a
  - 
}
"""

DLG SPR_57
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a 
  - 
}
"""

DLG SPR_58
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a 
  - 
}
"""

DLG SPR_59
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a b
  - 
}
"""

DLG SPR_5a
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a b
  - 
}
"""

DLG SPR_5b
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a ba
  - 
}
"""

DLG SPR_5c
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a ba
  - 
}
"""

DLG SPR_5d
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a bar
  - 
}
"""

DLG SPR_5e
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a bar
  - 
}
"""

DLG SPR_5f
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barb
  - 
}
"""

DLG SPR_5g
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barb
  - 
}
"""

DLG SPR_5h
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbi
  - 
}
"""

DLG SPR_5i
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbi
  - 
}
"""

DLG SPR_5j
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie
  - 
}
"""

DLG SPR_5k
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie
  - 
}
"""

DLG SPR_5l
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie 
  - 
}
"""

DLG SPR_5m
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie 
  - 
}
"""

DLG SPR_5n
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie w
  - 
}
"""

DLG SPR_5o
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie w
  - 
}
"""

DLG SPR_5p
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie wo
  - 
}
"""

DLG SPR_5q
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie wo
  - 
}
"""

DLG SPR_5r
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie wor
  - 
}
"""

DLG SPR_5s
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie wor
  - 
}
"""

DLG SPR_5t
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie work
  - 
}
"""

DLG SPR_5u
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie work
  - 
}
"""

DLG SPR_5v
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie workd
  - 
}
"""

DLG SPR_5w
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - 
}
"""

DLG SPR_5x
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - 
}
"""

DLG SPR_5y
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - l
}
"""

DLG SPR_5z
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - l
}
"""

DLG SPR_60
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - li
}
"""

DLG SPR_61
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - li
}
"""

DLG SPR_62
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - lif
}
"""

DLG SPR_63
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - lif
}
"""

DLG SPR_64
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life
}
"""

DLG SPR_65
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life
}
"""

DLG SPR_66
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life 
}
"""

DLG SPR_67
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life 
}
"""

DLG SPR_68
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life i
}
"""

DLG SPR_69
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life i
}
"""

DLG SPR_6a
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is
}
"""

DLG SPR_6b
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is
}
"""

DLG SPR_6c
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is 
}
"""

DLG SPR_6d
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is 
}
"""

DLG SPR_6e
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is f
}
"""

DLG SPR_6f
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is f
}
"""

DLG SPR_6g
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is 
}
"""

DLG SPR_6h
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is 
}
"""

DLG SPR_6i
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is
}
"""

DLG SPR_6j
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life is
}
"""

DLG SPR_6k
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life i
}
"""

DLG SPR_6l
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life i
}
"""

DLG SPR_6m
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in
}
"""

DLG SPR_6n
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in
}
"""

DLG SPR_6o
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in 
}
"""

DLG SPR_6p
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in 
}
"""

DLG SPR_6q
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in o
}
"""

DLG SPR_6r
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in o
}
"""

DLG SPR_6s
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in ol
}
"""

DLG SPR_6t
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in ol
}
"""

DLG SPR_6u
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in ola
}
"""

DLG SPR_6v
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in ola
}
"""

DLG SPR_6w
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olas
}
"""

DLG SPR_6x
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olas
}
"""

DLG SPR_6y
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olast
}
"""

DLG SPR_6z
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olast
}
"""

DLG SPR_70
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olasti
}
"""

DLG SPR_71
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olasti
}
"""

DLG SPR_72
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olastic
}
"""

DLG SPR_73
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in olastic
}
"""

DLG SPR_74
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic 
}
"""

DLG SPR_75
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic 
}
"""

DLG SPR_76
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic i
}
"""

DLG SPR_77
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic i
}
"""

DLG SPR_78
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is
}
"""

DLG SPR_79
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is
}
"""

DLG SPR_7a
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is 
}
"""

DLG SPR_7b
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is 
}
"""

DLG SPR_7c
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is f
}
"""

DLG SPR_7d
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is f
}
"""

DLG SPR_7e
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fa
}
"""

DLG SPR_7f
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fa
}
"""

DLG SPR_7g
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fan
}
"""

DLG SPR_7h
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fan
}
"""

DLG SPR_7i
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fant
}
"""

DLG SPR_7j
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fant
}
"""

DLG SPR_7k
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fanta
}
"""

DLG SPR_7l
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fanta
}
"""

DLG SPR_7m
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantas
}
"""

DLG SPR_7n
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantas
}
"""

DLG SPR_7o
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantast
}
"""

DLG SPR_7p
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantast
}
"""

DLG SPR_7q
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantasti
}
"""

DLG SPR_7r
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantasti
}
"""

DLG SPR_7s
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantastic
}
"""

DLG SPR_7t
"""
{rbw}Sing with me!{ / rbw}{sequence
  - Iâ€™m a barbie girl, in a barbie world 
  - life in plastic is fantastic
}
"""

DLG ITM_0
Follow the lost iPods through the crowd to go to the party.

END 0


END undefined
Thanks for singing with me!

VAR a
42


