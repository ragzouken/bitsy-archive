What was I doing again?

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
2,72,204
249,254,232
239,254,249

PAL 1
255,255,255
4,38,255
4,43,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,b,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room0
EXT 7,14 2 0,0
PAL 0

ROOM 1
0,0,0,0,a,b,a,0,0,0,0,0,0,0,0,0
0,0,0,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,a,a,0,a,a,a,a,a,a,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,a,0,0,0
0,0,a,a,a,0,0,a,a,a,a,a,a,0,0,0
0,0,0,0,a,0,0,a,0,0,0,0,0,0,0,0
0,0,0,0,a,b,a,a,0,0,0,0,0,0,0,0
NAME kitchen
ITM 0 5,2
ITM 6 5,0
EXT 5,15 3 6,6
PAL 0

ROOM 2
0,a,0,0,0,0,0,0,0,0,0,0,a,a,a,0
0,a,0,0,0,0,0,0,a,a,a,a,a,0,a,0
0,a,0,a,a,a,a,a,a,0,0,0,a,0,a,0
0,a,a,a,0,0,0,0,0,0,a,0,0,a,a,a
0,0,a,0,0,0,a,a,a,a,0,a,a,0,a,0
0,0,a,0,0,0,a,0,a,0,0,0,0,0,a,0
0,a,a,0,0,a,a,0,a,0,0,a,a,a,a,a
0,0,0,0,0,a,0,0,a,0,0,a,0,0,0,0
0,a,a,a,0,a,0,0,a,0,a,a,0,a,a,a
0,a,0,a,0,a,a,0,a,a,0,a,0,a,0,0
0,a,0,a,0,a,0,a,a,0,0,0,0,a,0,0
0,0,a,a,0,0,0,0,0,0,a,0,0,a,0,0
a,a,0,a,0,0,a,0,0,0,0,0,a,a,0,0
a,0,0,a,a,a,0,a,a,a,a,a,0,a,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,a,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,0,0
NAME daydream1
EXT 15,7 1 5,1
PAL 1

ROOM 3
0,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
0,b,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,0,b,b,0,0,0,b,b,b,b,b,b,0,0
b,0,0,b,b,b,b,b,0,0,0,0,0,0,b,b
b,0,b,0,b,b,0,0,0,b,b,b,0,b,b,0
b,0,b,0,b,b,0,b,b,0,b,b,0,b,b,0
b,0,b,0,b,b,0,b,b,0,b,0,0,b,b,0
b,0,0,b,b,b,b,b,b,0,b,0,0,b,b,0
b,0,0,0,0,b,b,b,b,0,b,0,0,b,b,0
b,0,b,b,0,0,0,0,0,b,b,0,b,b,b,0
b,0,b,b,b,b,b,b,b,b,b,0,0,b,b,0
b,0,b,b,0,0,0,0,0,0,0,0,0,b,b,0
b,0,0,0,b,b,b,b,0,b,b,0,0,b,0,0
b,0,0,0,0,0,0,0,0,0,0,b,b,0,0,0
b,b,b,0,0,0,0,b,b,b,b,b,0,0,0,b
0,0,0,b,b,b,b,0,0,0,0,0,0,0,b,0
ITM 0 6,5
EXT 0,0 4 0,8
PAL 1

ROOM 4
0,0,0,0,a,a,c,a,a,c,c,a,a,c,a,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ITM 0 1,8
EXT 0,8 5 7,15
PAL 0

ROOM 5
d,d,d,d,d,d,a,a,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,0,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,0,d,a,0,0,a,d,d,0,d,d,d
d,d,d,d,0,d,a,0,0,a,0,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,d,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,0,d,d,d,d,d
d,d,d,d,d,d,a,0,0,a,0,d,0,d,d,0
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
ITM 0 10,13
ITM 0 10,14
ITM 0 12,14
ITM 0 15,14
ITM 0 5,14
ITM 0 4,14
ITM 0 2,14
ITM 0 3,13
ITM 0 5,12
ITM 0 2,11
ITM 0 5,11
ITM 0 4,10
ITM 0 1,10
ITM 0 1,9
ITM 0 4,9
ITM 0 2,8
ITM 0 1,7
ITM 0 2,7
ITM 0 5,6
ITM 0 4,5
ITM 0 5,4
ITM 0 3,4
ITM 0 1,4
ITM 0 2,3
ITM 0 3,2
ITM 0 1,2
ITM 0 4,1
ITM 0 5,1
ITM 0 2,1
ITM 0 11,2
ITM 0 14,5
ITM 0 12,7
ITM 0 12,9
ITM 0 10,10
ITM 0 15,3
ITM 0 14,1
ITM 0 11,4
ITM 0 14,8
ITM 0 0,2
EXT 8,0 6 7,0
PAL 1

ROOM 6
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
ITM 0 7,1
EXT 0,4 7 15,3
EXT 7,15 8 1,0
EXT 15,4 b 0,3
EXT 0,11 c 15,3
EXT 15,11 f 0,13
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,0,a,a,a,a,a,a,a
0,0,0,0,0,0,0,a,0,a,0,0,0,0,0,0
0,0,0,0,0,0,0,a,0,a,0,0,0,0,0,0
0,0,0,0,0,0,0,a,0,a,0,0,0,0,0,0
0,0,0,0,0,0,0,a,0,a,0,0,0,0,0,0
0,0,0,0,0,0,0,a,0,a,0,0,0,0,0,0
0,0,0,0,a,c,a,a,0,a,a,c,a,0,0,0
0,0,0,0,a,a,a,a,a,a,a,a,a,0,0,0
0,0,0,0,c,a,a,a,a,a,a,a,a,0,0,0
0,0,0,0,a,a,a,a,a,a,a,a,c,0,0,0
0,0,0,0,a,a,a,a,a,a,a,a,c,0,0,0
0,0,0,0,a,a,a,a,a,a,a,a,a,0,0,0
NAME 6_UplEFTa
EXT 0,3 8 15,3
PAL 0

ROOM 8
a,0,a,0,0,0,0,0,0,0,0,a,0,a,0,0
a,0,a,0,0,0,0,0,0,0,0,a,0,a,0,0
a,0,a,a,a,a,a,a,a,a,a,a,0,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,a,a,a,0,a,a,a,a,a
0,0,0,a,0,0,0,a,0,a,0,a,0,0,0,0
0,0,0,a,0,0,0,a,0,a,0,a,a,c,a,0
0,0,0,a,0,0,0,a,0,a,a,a,a,a,c,0
0,0,0,a,0,0,0,a,0,c,a,a,a,a,a,0
0,0,0,a,0,0,0,a,0,c,a,a,a,a,a,c
0,0,0,a,0,0,0,a,0,a,a,a,a,a,a,a
0,0,0,a,0,0,0,a,0,a,a,a,a,a,a,a
0,0,0,a,0,0,0,a,0,a,a,a,a,a,a,a
0,0,0,a,0,0,0,a,0,a,a,a,a,a,a,a
0,0,0,a,0,0,0,a,0,c,a,a,a,a,a,a
0,0,0,a,0,0,0,a,0,a,a,a,a,a,a,a
NAME 6_UpLeftB
EXT 5,15 a 15,8
EXT 0,3 b 15,10
EXT 1,0 6 7,15
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,b
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,a
NAME thelot
ITM 2 14,8
EXT 15,4 h 1,0
EXT 3,15 g 7,14
EXT 4,15 g 7,14
EXT 5,15 g 7,14
EXT 0,3 g 7,14
EXT 6,15 g 7,14
EXT 7,15 g 7,14
EXT 8,15 g 7,14
EXT 9,15 g 7,14
EXT 10,15 g 7,14
EXT 11,15 g 7,14
EXT 12,15 g 7,14
EXT 13,15 g 7,14
EXT 14,15 g 7,14
EXT 15,5 h 14,0
EXT 15,3 h 7,0
EXT 15,8 g 7,14
PAL 0

ROOM a
0,a,0,a,0,a,a,0,a,a,0,a,a,0,a,0
a,a,0,a,0,0,0,0,0,0,0,0,0,0,a,a
0,0,0,0,0,a,0,a,a,0,a,0,a,0,0,0
a,a,0,a,0,a,0,a,a,0,a,0,0,0,a,a
a,a,0,a,0,0,0,0,0,0,0,0,a,0,a,a
a,a,0,a,0,a,0,a,a,0,a,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,a,a,a,a
a,0,a,a,0,a,0,a,a,0,a,0,0,0,0,0
a,0,a,a,0,a,0,a,a,0,a,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,a,a,a,a
a,a,0,a,0,a,0,a,a,0,a,0,0,0,0,0
a,a,0,a,0,0,0,0,0,0,0,0,a,0,a,a
a,a,0,0,0,a,0,a,a,0,a,0,0,0,a,a
0,0,a,a,0,a,0,a,a,0,a,a,0,0,0,0
a,0,0,0,0,0,0,a,a,0,a,a,0,a,a,a
a,a,a,a,0,a,0,a,a,0,a,a,0,a,a,a
NAME up_left_dream1
ITM 1 9,9
ITM 1 6,9
ITM 1 4,9
ITM 1 4,6
ITM 1 14,8
ITM 1 2,6
ITM 1 2,2
ITM 1 0,2
EXT 0,2 9 0,3
PAL 1

ROOM b
0,0,0,0,0,0,0,0,0,0,0,a,0,a,0,0
0,0,0,0,0,0,0,0,0,0,0,a,0,a,0,0
a,a,a,a,a,a,a,a,a,a,a,a,0,a,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,a,0,0
a,a,a,a,0,a,a,a,a,a,a,a,0,a,0,0
0,0,0,a,0,a,0,0,0,0,0,a,0,a,0,0
0,0,0,a,0,a,0,0,0,0,0,a,0,a,0,0
0,0,0,a,0,a,0,0,0,0,0,a,0,a,0,0
a,c,a,a,0,a,a,c,a,0,0,a,0,a,0,0
a,a,a,a,a,a,a,a,a,0,0,a,0,a,a,a
a,a,a,a,a,a,a,a,a,0,0,a,0,0,0,0
a,a,a,a,a,a,a,a,a,a,0,a,0,a,a,a
a,a,a,a,a,a,a,a,a,c,0,a,0,a,0,0
a,a,a,a,a,a,a,a,a,a,0,a,0,a,0,0
a,a,a,a,a,a,a,a,a,c,0,a,0,a,0,0
a,a,a,a,a,a,a,a,a,a,0,a,0,a,0,0
NAME 6_up_right
EXT 15,10 8 0,3
EXT 12,15 8 1,0
EXT 12,0 a 15,8
PAL 0

ROOM c
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,a,0,0,0,0,0
a,0,a,a,a,a,a,a,0,0,a,0,a,a,a,a
a,0,a,a,a,a,a,a,a,0,a,0,a,0,0,0
a,0,a,a,0,0,0,0,0,0,a,a,0,0,a,a
a,0,a,a,0,a,a,a,a,a,a,0,a,0,a,0
0,0,a,a,0,a,0,0,0,0,a,0,a,0,a,0
a,a,a,0,0,a,0,a,a,a,0,a,a,0,a,0
0,a,a,a,0,a,a,0,0,0,0,0,0,0,a,0
0,a,0,0,0,a,0,a,0,a,0,a,a,0,a,0
0,a,0,a,a,0,0,a,0,a,a,0,0,a,0,0
0,a,0,a,0,0,0,a,0,a,0,0,a,0,0,0
a,0,0,0,a,a,a,a,0,a,0,0,a,0,a,a
0,a,a,0,0,0,0,0,0,a,0,a,a,0,a,0
0,0,a,a,a,a,a,a,a,a,0,a,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,a,0,a,a,0
NAME 6_down_left
EXT 15,3 6 0,11
EXT 0,6 d 15,7
EXT 0,15 f 15,9
PAL 0

ROOM d
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,b,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,b,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,b,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,b,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,c,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
NAME 6_down_left_b
EXT 15,7 c 0,6
EXT 15,8 e 7,7
PAL 0

ROOM e
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
0,0,0,0,0,0,0,c,0,c,0,0,0,0,0,0
c,c,c,c,c,c,a,a,0,a,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,a,c,c,c,c,c,0
c,c,c,c,c,c,a,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,a,c,c,c,c,c,c
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,0,c,0,0,0,0,0,0,0
NAME down_left_dream1
EXT 8,0 6 7,15
EXT 15,8 6 0,11
EXT 7,15 6 7,0
EXT 0,7 6 15,4
PAL 1

ROOM f
0,0,a,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,0,a,0,a,0,0,0,0,0,0,0,0,0
0,0,a,0,a,0,a,a,a,a,0,a,a,a,a,a
0,0,a,0,a,0,0,0,0,a,0,a,0,0,0,0
0,0,a,0,a,0,0,0,0,a,0,a,0,0,0,0
0,0,a,0,a,a,a,a,a,a,0,a,a,a,a,a
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,0,a,a,a,a,a,a,a,a,a,a,a,a
0,0,a,0,a,0,0,0,0,0,0,0,0,0,0,0
a,a,a,0,a,a,a,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 6_down_right
ITM 2 0,15
EXT 3,0 b 12,15
EXT 15,9 c 0,15
EXT 0,15 g 7,14
PAL 0

ROOM g
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,0,a,0,0,0,0,0,0,0
0,0,0,0,0,0,a,a,a,0,0,0,0,0,0,0
NAME 6_downr_dreaam
ITM 3 7,12
ITM 3 7,11
ITM 3 7,10
ITM 3 7,9
ITM 3 7,8
ITM 3 7,7
ITM 3 7,6
ITM 3 7,5
ITM 3 7,4
ITM 3 7,3
ITM 3 7,2
ITM 4 7,1
ITM 3 7,13
EXT 7,0 6 7,15
PAL 1

ROOM h
a,0,a,a,a,a,a,0,0,a,a,a,a,a,0,a
a,0,a,0,0,0,a,0,0,a,0,0,0,a,0,a
a,0,a,0,a,0,a,0,0,a,0,a,0,a,0,a
a,0,a,0,0,0,a,0,0,a,0,0,0,a,0,a
a,0,a,a,a,a,a,0,0,a,a,a,a,a,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,a,a,a,a,0,0,0,0,0,a
a,0,0,0,0,a,0,0,0,0,a,0,0,0,0,a
a,0,0,0,0,a,0,0,0,0,a,0,0,0,0,a
a,0,0,0,0,a,0,0,0,a,a,0,0,0,0,a
a,0,0,0,0,0,a,0,0,a,0,0,0,0,0,a
a,0,0,0,0,0,0,0,a,0,0,0,0,0,0,a
a,0,0,0,0,0,a,0,a,0,0,0,0,0,0,a
a,0,0,0,0,0,a,0,a,0,0,0,0,0,0,a
a,0,0,0,0,0,a,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,a,0,a,0,0,0,0,0,0,a
ITM 5 7,8
ITM 6 7,14
EXT 7,15 i 3,3
PAL 1

ROOM i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,0,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
EXT 7,14 2 0,0
END 0 7,13
PAL 0

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME Wall
WAL true

TIL b
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door
WAL false

TIL c
11111111
10000001
10010001
10100001
10000101
10001001
10000001
11111111
NAME window
WAL true

TIL d
00101100
01000010
01000010
00100110
00001000
00000000
00000000
00001000
>
00101100
10011010
01000100
00001010
00000100
00010000
00000001
00001000

SPR A
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 0 4,4

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME cat
DLG SPR_0
POS 0 8,12

SPR b
11111111
10000000
10000000
10011000
10011000
10000000
10000000
11111111
NAME cupboard1
DLG SPR_2
POS 1 10,3

SPR c
11111111
00000001
00000001
00011001
00011001
00000001
00000001
11111111
NAME cubpoard2
POS 1 11,3

SPR d
10001000
00100000
10010001
01110001
01110010
01111100
00111100
00100100
>
00100000
01010000
01001001
01110001
01110010
01111100
00111100
00100100
NAME dreamcat
POS 2 7,5

SPR e
01010000
00010000
01000001
01110001
01110010
01111100
00111100
00100100
>
00101000
01010000
00010001
01110001
01110010
01111100
00111100
00100100
NAME dreamcat2
POS 2 10,7

SPR f
00101000
01010000
00101001
01110001
01110010
01111100
00111100
00100100
>
01010000
01010000
10100001
01110001
01110010
01111100
00111100
00100100
NAME dreamcat3
POS 2 2,12

SPR g
01010000
00101000
01010001
01110001
01110010
01111100
00111100
00100100
>
01000000
10010000
01000001
01110001
01110010
01111100
00111100
00100100
NAME dreamcat4
POS 2 13,1

SPR h
00000000
00000000
00000000
00000000
00001000
00001111
00001111
00001111
NAME bed1
DLG SPR_4
POS 4 13,4

SPR i
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo1
DLG SPR_1
POS 1 5,0

SPR j
00000000
00000000
00000000
00000000
01111110
01000010
01000010
00111100
NAME foodbowl
DLG SPR_3
POS 1 11,4

SPR k
00000000
00000000
00000000
00000001
00000111
11111111
11111111
11111111
NAME bed2
DLG SPR_5
POS 4 14,4

SPR l
11111111
01000010
01000010
01000010
00000000
00000000
00000000
00000000
NAME table
POS 4 14,7

SPR m
00000000
00000000
00010000
00111100
01001010
01010110
01001010
00111100
NAME clock
DLG SPR_6
POS 4 14,6

SPR n
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo2
DLG SPR_7
POS 7 8,10

SPR o
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo3
DLG SPR_8
POS 8 12,0

SPR p
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo4
DLG SPR_9
POS 8 10,6

SPR q
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo5
DLG SPR_a
POS b 4,8

SPR r
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_mogo6
DLG SPR_b
POS f 10,5

SPR s
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME cat_end
DLG SPR_c
POS i 8,12

SPR t
11111111
11111111
11000011
11000011
11000011
11001011
11000011
11000011
NAME door_nogo7
POS i 7,14

ITM 0
00111100
01000010
01000010
00000110
00001100
00001000
00000000
00001000
NAME Huh?
DLG ITM_0

ITM 1
00111100
01000010
01000010
00000110
00001100
00001000
00000000
00001000
NAME huh_daydream1
DLG ITM_1

ITM 2
00011000
00011000
00011000
00011000
00011000
00000000
00011000
00011000
NAME ohno1
DLG ITM_2

ITM 3
00011000
00011000
00011000
00011000
00011000
00000000
00011000
00011000
NAME ohno2
DLG ITM_3

ITM 4
01011101
00111001
01011010
11011101
10111001
01100110
01011010
10011011
>
10010000
10111011
01001000
00010011
01111000
10101110
01101001
10011110
NAME ohno3
DLG ITM_4

ITM 5
00111100
01000010
01000010
00000110
00001100
00001000
00000000
00001000
NAME huh_final
DLG ITM_5

ITM 6
00111100
01000010
01000010
00000110
00001100
00001000
00000000
00001000
NAME huh_final2
DLG ITM_6

DLG SPR_0
"""
{
  - {item "catfood"} == 1 ?
    {shk}purrrrrrrrrrrrrrrrrrrrrr{shk}
  - {item "catfood"} == 0 ?
    Meow?
    
    I think it's hungry.
    
    The cat food is in the kitchen cupboard, right?
}
"""

DLG ITM_0
"""
{
  - huh == 0 ?
    Wait, why did I come here? 
    {huh = huh + 1}
  - huh == 1 ?
    There was somewhere I needed to go... {huh = huh + 1}
  - huh == 2 ?
    What am I doing here? I don't remember going to my room. 
    {huh = huh + 1}
  - huh == 3 ?
    Crap! I spaced out...where was I supposed to turn?
    {huh = huh + 1}
}
"""

DLG SPR_1
I feel like I'm forgetting something...

DLG SPR_2
"""
{
  - catfood == 0 ?
    Cereal, napkins, chocolates...ah! Catfood. 
{catfood = catfood + 1}
}
"""

DLG SPR_3
"""
{
  - catfood == 1 ?
    There we go. Nice and full...was there something else?
}
"""

DLG SPR_4
I can't sleep now. 

DLG SPR_5
I can'r sleep now.

DLG SPR_6
Oh! I'm late for work! How could I forget my early shift...! {gowork = 1}

DLG SPR_7
Wrong house...is it further down, or did I make a wrong turn?

DLG SPR_8
I don't remember this being on this street...

DLG SPR_9
Oh, this is XXXXX's house. Wasn't it in the other direction?

DLG ITM_1
"""
{
  - {item "huh_daydream1"} == 0 ?
    I wish I could focus
  - {item "huh_daydream1"} == 1 ?
    I'm always daydreaming
  - {item "huh_daydream1"} == 2 ?
    So I forget things a lot
  - {item "huh_daydream1"} == 3 ?
    Is this normal?
  - {item "huh_daydream1"} == 4 ?
    Or is something wrong?
  - {item "huh_daydream1"} == 5 ?
    I've tried keeping notes
  - {item "huh_daydream1"} == 6 ?
    But then I forget to read them, or forget where I put them, or...
  - {item "huh_daydream1"} == 7 ?
    I wish I could focus
}
"""

DLG ITM_2
$#@%! I had two deliveries! I forgot all about the second one!

DLG SPR_a
Here it is. Delivery done. Now back to the store...

DLG SPR_b
I thought it was here...guess not...

DLG ITM_3
It's absolutely infuriating

DLG ITM_4
I hate being "clumsy" and "forgetful"

DLG ITM_5
But at least I'm not alone. Forgetting is part of being human.

DLG ITM_6
So I'll learn to forgive this flaw of mine. 

DLG SPR_c
{shk}purrrrrrrrrrrrrrrrrrrrrr{shk}

END 0
Was there anything else?

END undefined
ff

END undefinee


VAR catfood
0

VAR huh
0

VAR gowork
0

