i wanna be with you

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
236,236,240
132,134,69
151,144,152
37,149,153
251,248,247

PAL 1
NAME sunset
255,141,131
151,144,152
151,144,152
37,149,153
251,188,200
237,162,86

ROOM 0
0,0,0,v,u,0,0,0,0,0,0,0,0,0,0,0
0,0,t,q,q,r,0,0,0,0,0,0,0,0,0,0
0,0,y,x,x,z,0,0,0,0,v,u,0,0,0,0
0,0,0,0,0,0,0,0,0,t,q,q,r,0,0,0
0,0,0,0,0,0,0,0,0,y,x,x,z,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,j,b,c,d,e,j,j,j,j,j,j,j,j,j,j
0,k,f,h,i,g,0,0,0,0,0,0,l,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME home1
EXT 15,8 1 0,8
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,u,v,0
0,0,v,u,0,0,0,0,0,0,0,0,t,q,q,r
0,t,q,q,r,0,0,0,u,v,u,0,y,x,x,z
0,y,x,x,0,0,0,t,q,q,q,w,0,0,0,0
0,0,0,0,0,0,0,y,x,x,x,z,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
0,0,k,0,0,0,0,0,0,0,0,0,k,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME boi1
PAL 0

ROOM 2
0,0,0,v,u,0,0,0,0,0,0,0,0,0,0,0
0,0,t,q,q,r,0,0,0,0,0,0,0,0,0,0
0,0,y,x,x,z,0,0,0,0,v,u,0,0,0,0
0,0,0,0,0,0,0,0,0,t,q,q,r,0,0,0
0,0,0,0,0,0,0,0,0,y,x,x,z,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,j,b,c,d,e,j,j,j,j,j,j,j,j,j,j
0,k,f,h,i,g,0,0,0,0,0,0,l,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME home2
EXT 15,8 3 0,8
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,u,v,0
0,0,v,u,0,0,0,0,0,0,0,0,t,q,q,w
0,t,q,q,r,0,0,0,u,v,u,0,y,x,x,z
0,y,x,x,z,0,0,t,q,q,q,w,0,0,0,0
0,0,0,0,0,0,0,y,x,x,x,z,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
0,0,k,0,0,0,0,0,0,0,0,0,k,0,0,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME boi2
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,u,v,0,0,0,0,0,0,0,0,0,0,0,0
0,t,q,q,w,0,0,0,0,0,0,v,u,0,0,0
0,y,x,x,z,0,0,0,0,0,t,q,q,r,0,0
0,0,0,0,0,0,0,0,0,0,y,x,x,z,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,n,n,n,n,n,n,n,n,0,0,0,0
j,j,j,o,o,o,o,o,o,o,o,o,o,j,j,j
0,l,n,n,n,n,n,n,n,n,n,n,n,n,k,0
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
NAME sunset
PAL 1

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME ground
WAL true

TIL b
00000000
00000000
00000011
00000011
00001111
00001111
00000010
00000010
NAME house
WAL true

TIL c
11111111
11111111
11111111
11111111
11111111
11111111
00000000
01111000
NAME house
WAL true

TIL d
11111111
11111111
11111111
11111111
11111111
11111111
00000000
00011110
NAME house
WAL true

TIL e
00000000
00000000
11000000
11000000
11110000
11110000
01000000
01000000
NAME house
WAL true

TIL f
00000010
00000010
00000010
00000010
00000010
00000010
00000010
00000010
NAME house
WAL true

TIL g
01000000
01000000
01000000
01000000
01000000
01000000
01000000
01000000
NAME house
WAL true

TIL h
01111000
01111000
01111000
00000011
00000011
00000011
00000011
00000011
NAME house
WAL true

TIL i
00011110
00011110
00011110
11000000
11000000
11000000
11000000
11000000
NAME house
WAL true

TIL j
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME sky
WAL true

TIL k
00000000
00011000
00011000
01011000
01111010
00011110
00011000
00011000
NAME cactus
COL 2

TIL l
00000000
00011000
00011010
00011110
01011000
01111000
00011000
00011000
NAME cactus2
COL 2

TIL m
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME ground normal
WAL true

TIL n
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME sunset
COL 5

TIL o
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME sunset
WAL true
COL 5

TIL p
11111111
11111111
11111110
11111100
11111000
01110000
00000000
00000000
>
11111111
11111111
11111111
11111110
11111100
11111000
01110000
00000000
NAME clouds
COL 4

TIL q
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME clouds
COL 4

TIL r
11100000
11110000
11110000
11111000
11111000
11110000
11100000
11000000
>
00000000
11100000
11110000
11110000
11111000
11111000
11110000
11100000
NAME clouds
COL 4

TIL t
00000111
00011111
00011111
00011111
00011111
00011111
00001111
00000011
>
00000000
00000111
00011111
00011111
00011111
00011111
00011111
00001111
NAME clouds
COL 4

TIL u
00000000
00000000
00000000
01111100
01111110
11111111
11111111
11111111
>
00000000
00000000
00000000
00000000
01111100
01111110
11111111
11111111
NAME clouds
COL 4

TIL v
00000000
00000000
00000000
00000000
01111100
11111110
11111111
11111111
>
00000000
00000000
00000000
00000000
00000000
01111100
11111110
11111111
NAME clouds
COL 4

TIL w
00000000
11000000
11100000
11100000
11100000
11100000
11100000
11000000
>
00000000
00000000
11000000
11100000
11100000
11100000
11100000
11100000
NAME clouds
COL 4

TIL x
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME clouds
COL 4

TIL y
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
00000011
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME clouds
COL 4

TIL z
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
11000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME clouds
COL 4

SPR A
00000000
00000000
00011000
00011000
00000000
00011000
00011000
00011000
>
00000000
00000000
00000000
00011000
00011000
00000000
00011000
00011000
POS 0 8,8
COL 3

SPR a
00000000
00000000
00011000
00011000
00000000
00011000
00011000
00011000
>
00000000
00000000
00000000
00011000
00011000
00000000
00011000
00011000
NAME boi1
DLG SPR_0
POS 1 8,8

SPR b
00000000
00000000
00011000
00011000
00000000
00011000
00011000
00011000
>
00000000
00000000
00000000
00011000
00011000
00000000
00011000
00011000
NAME boi2
DLG SPR_1
POS 3 8,8

SPR c
00000000
00000000
00011000
00011000
00000000
00011000
00011000
00011000
>
00000000
00000000
00000000
00011000
00011000
00000000
00011000
00011000
NAME boi3
DLG SPR_2
POS 4 8,8

ITM 0
00000000
00000000
00011000
00100100
00100100
00011000
00001000
00001000
>
00000000
00000000
00000000
00011000
00100100
00100100
00011000
00001000
NAME tea

DLG SPR_0
"""
{sequence
  - {clr3}...{clr3}
  - {clr3}...
    
    what?{clr3}
  - {clr3}...
    
    why do you keep looking at me?{clr3}
  - {clr3}what do you want?{clr3}
  - {clr3}...{clr3}
  - {clr3}do you...
    
    do you want to know a secret?{clr3}
  - {clr3}okay...
    
    here goes...{clr3}
  - {clr3}i come here every day and just watch the clouds go by.
    isn't that silly?{clr3}
  - {clr3}...{clr3}
  - {clr3}what?
    
    you don't think so?{clr3}
  - {clr3}you're the first person to say that.{clr3}
  - {clr3}it's very dull and boring in this desert.{clr3}
  - {clr3}everything is gray for all that i can see.{clr3}
  - {clr3}except you.{clr3}
  - {clr3}i like that.{clr3}
  - {clr3}sometimes, the sound of the wind is so loud it drowns out my thoughts.{clr3}
  - {clr3}it's peaceful.
    
    ...
    
    it's lonely.{clr3}
  - {clr3}it's getting pretty late, you should go.{clr3}
  - {clr3}thanks for talking with me.{clr3}(exit "home2,8,8")
}
"""

DLG SPR_1
"""
{sequence
  - {clr3}you're back again!{clr3}
  - {clr3}i enjoyed our talk yesterday.{clr3}
  - {clr3}i did most of the talking though...
    i think i want to hear more about you!{clr3}
  - {clr3}what do you like to do?
    
    ...
    
    that sounds interesting!{clr3}
  - {clr3}what are you afraid of?
    
    ...
    
    ah! me too!
    {clr3}
  - {clr3}what's your favorite color?
    
    ...
    
    mine? hmm...
    
    i don't know the name, but it looks like the color you are, i guess?{clr3}
  - {clr3}do you want to know another secret?{clr3}
  - {clr3}i like to watch the sunset too.{clr3}
  - {clr3}i go to the same place and wait for it to set.{clr3}
  - {clr3}i like to think that maybe if i wait for the sun every day that the sun might...
    
    the sun might be waiting for me too.{clr3}
  - {shk}{clr3}w-why are you laughing?{clr3}{shk}
  - {clr3}because you do that too? 
    
    No way!
    
    i thought i was the only one!{clr3}
  - {clr3}Do you think maybe...
    
    we could watch the sunset together sometime?{clr3}
  - {clr3}...{clr3}
  - {clr3}you will?
    
    that's great!{clr3}
  - {clr3}i think we should go to my spot now if we wanna see it{clr3}
  - {clr3}let's go!{clr3}(exit "sunset,7,8")
}
"""

DLG SPR_2
"""
{sequence
  - {clr3}we made it in time!{clr3}
  - {clr3}isn't it beautiful?{clr3}
  - {clr3}...{clr3}
  - {clr3}can i tell you another secret?{clr3}
  - {clr3}i...
    
    i used to not have a favorite color because every one i had seen was so boring.{clr3}
  - {clr3}but when i saw you, your color became my favorite because you looked so beautiful, even more than a sunset.{clr3}
  - {clr3}i never knew a color like that existed before i met you.{clr3}
  - {clr3}...{clr3}
  - {clr3}thank you for being here with me.{clr3}
  - {clr3}i used to wonder if this was it...
    if this really was all that life had for me.{clr3}
  - {clr3}but now i know that this is all i need.
    as long as you're here too.{clr3}
  - {clr3}it can get lonely in the desert
    
    but we can be lonely together
    
    and that doesn't sound too bad.{clr3}
  - {clr3}i didn't know what i wanted too do before...
    i was just a dreamer...{clr3}
  - {clr3}i know what i want to do now.
    
    i wanna be with you
    
    ...if you wanna be with me?
    
    ...
    
    you do?{clr3}
  - {clr3}i...
    
    i'd like that.{clr3}

  - {clr3}i think i...
    
    i think...{clr3} (end "{clr1}i love you.{clr1}{br}{br}End")

}
"""

VAR a
42

