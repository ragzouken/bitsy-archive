Erin's Birthday Quest

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
173,149,29
11,90,181
255,242,255

PAL 1
155,196,255
11,90,181
255,242,255

ROOM 0
w,w,w,w,w,w,w,w,w,w,w,w,w,w,w,w
b,r,b,b,b,b,b,b,b,b,b,q,b,b,b,b
b,b,m,n,o,p,b,c,d,b,b,b,b,b,b,b
b,q,b,b,b,b,b,0,0,b,r,b,b,b,q,b
b,b,b,b,b,b,q,0,0,b,b,b,b,b,b,b
i,0,0,10,0,0,0,0,0,13,13,z,0,0,0,i
i,0,y,x,0,0,0,0,0,0,0,0,y,0,0,i
i,0,0,0,0,0,z,0,0,0,y,0,0,0,0,i
i,0,0,0,0,0,0,0,0,0,0,0,0,10,0,i
i,0,z,0,0,0,0,x,0,0,0,0,0,0,0,i
0,0,0,0,0,0,0,0,0,0,0,x,0,0,0,0
i,0,0,0,0,y,x,0,0,0,0,0,0,0,0,i
i,0,0,0,0,0,0,0,z,0,0,0,0,z,0,i
i,0,0,y,0,0,0,0,0,0,0,y,x,0,0,i
i,z,0,0,10,0,0,x,0,0,0,0,0,0,0,i
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
ITM 0 9,9
EXT 0,10 1 15,10
EXT 15,10 2 0,10
EXT 7,3 3 7,15
EXT 8,3 3 8,15
PAL 0

ROOM 1
w,w,w,w,w,w,w,w,w,w,w,w,w,w,w,w
b,b,q,b,b,r,b,b,b,b,b,q,b,b,b,b
b,b,b,b,11,12,b,b,b,b,b,11,12,b,b,b
b,r,b,b,b,b,q,b,b,b,r,b,b,b,b,r
b,b,b,b,b,b,b,b,b,b,b,b,q,b,b,b
i,z,0,0,0,0,0,0,14,0,0,z,0,0,0,i
i,0,0,x,0,0,10,0,0,0,0,0,0,x,0,i
i,0,0,0,0,0,0,0,0,y,x,0,0,0,0,i
i,0,0,0,0,0,0,0,0,0,0,0,0,0,0,i
i,0,z,0,0,0,0,0,0,0,0,0,z,0,0,i
i,0,0,0,0,0,0,0,y,x,0,0,0,0,0,0
i,0,y,0,0,0,z,0,0,0,0,0,0,0,0,i
i,0,0,0,0,0,0,0,0,0,0,0,0,y,0,i
i,0,0,0,y,x,0,0,0,z,0,0,x,0,0,i
i,z,0,0,0,0,0,0,0,0,0,0,0,0,10,i
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
EXT 15,10 0 0,10
PAL 0

ROOM 2
w,w,w,w,w,w,w,w,w,w,w,w,w,w,w,w
b,b,q,b,b,r,b,b,b,b,b,b,b,b,b,b
q,b,11,12,b,b,11,12,b,b,r,b,11,12,b,q
b,b,b,b,b,b,q,b,b,b,b,b,b,b,b,b
b,b,r,b,b,b,b,b,b,b,b,q,b,b,b,r
i,y,x,0,0,0,0,0,0,0,10,0,0,z,0,i
i,0,0,0,0,0,z,0,0,x,0,0,0,0,0,i
i,0,10,0,0,0,0,0,0,0,0,y,x,0,0,i
i,0,0,0,x,0,0,0,0,0,0,0,0,0,0,i
i,0,0,0,0,0,0,0,0,z,0,0,0,0,0,i
0,0,0,0,0,0,0,y,x,0,0,0,0,0,0,i
i,z,0,10,0,0,0,0,0,0,0,10,0,y,0,i
i,0,0,0,0,0,0,0,0,0,x,0,0,0,0,i
i,0,0,y,x,0,z,0,0,0,0,0,0,0,0,i
i,0,0,0,0,0,0,0,0,10,0,0,0,0,z,i
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
EXT 0,10 0 15,10
PAL 0

ROOM 3
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,j,j,j,j,j,j,v,v,v,j,v,j,v,j,b
b,v,v,v,v,v,v,v,v,v,j,v,j,v,j,b
b,j,j,j,j,j,j,v,v,v,j,v,j,v,j,b
b,v,v,v,v,v,v,v,v,v,j,v,j,v,j,b
b,v,t,0,v,v,v,v,v,v,j,v,j,v,j,b
b,b,b,b,b,b,s,v,v,v,v,v,v,v,v,b
b,v,v,v,v,v,v,v,v,v,j,v,j,v,j,b
b,v,v,v,v,v,v,v,v,v,j,v,j,v,j,b
b,v,v,v,v,v,v,v,v,v,j,v,j,v,j,b
b,j,v,j,v,l,k,v,v,v,j,v,j,v,j,b
b,j,v,j,v,v,v,v,v,v,v,v,v,v,v,b
b,j,v,j,v,l,k,v,v,v,j,v,k,v,v,b
b,j,v,v,v,v,v,v,v,v,j,v,v,v,0,b
b,j,j,j,j,v,v,v,v,v,j,v,u,b,b,b
b,b,b,b,b,b,s,v,v,u,b,b,b,b,b,b
EXT 7,15 0 7,3
EXT 8,15 0 8,3
PAL 1

TIL 10
00000000
00000000
00000000
00000000
00110000
01111000
00000000
00000000

TIL 11
00000000
01110111
01110111
00000000
01110111
01110111
01110111
00000000

TIL 12
00000111
01110111
01110111
00000111
01110111
01110111
01110111
00000111

TIL 13
00000000
00100010
01010101
01010101
01010101
01010101
01010101
01010101

TIL 14
00001110
00010010
00010010
10010011
01011000
01000000
01100000
00000000
>
00000000
00001110
00010010
00010010
10010011
01011000
01000000
01100000

TIL a
00000000
01111110
11111111
11111111
01011010
00111100
00011000
00011000
WAL true

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL c
11111111
11111100
11110000
11100000
11000000
11000000
10000000
10000000
WAL true

TIL d
11111111
00111111
00001111
00000111
00000011
00000011
00000001
00000001
WAL true

TIL e
00000000
00001111
00010000
00101111
00101010
00101101
00101110
00010101

TIL f
00000000
11111000
00000100
11101010
11010110
10101010
01010110
10101100

TIL g
00001011
00000101
00000010
00000010
00000010
00000010
00000010
00000010

TIL h
01011000
10110000
10100000
01100000
10100000
01100000
10100000
01100000

TIL i
11111111
00010001
00010001
00010001
11111111
01000100
01000100
01000100
WAL true

TIL j
10000001
11111111
01101010
01101010
11111111
00110101
00110101
11111111
WAL true

TIL k
00000000
00011110
00010010
00011110
00010010
01111110
01001010
01001010

TIL l
00000000
00000000
00111100
11111111
01111110
00011000
00011000
00011000

TIL m
11111111
10111010
10111010
10111010
10111010
10001010
11111111
11111111

TIL n
11111111
01100110
10101010
01100110
10101010
01101010
11111111
11111111

TIL o
11111111
00100110
11101010
01100110
11101010
00101010
11111111
11111111

TIL p
11111111
01101011
10101011
01110111
10110111
10110111
11111111
11111111

TIL q
11111111
11111111
11111111
11111111
11111111
11100001
11111011
11111111
WAL true

TIL r
11111111
11111111
11111111
11011111
11011111
00000011
11111011
11111111
WAL true

TIL s
11111000
11111100
11111110
11111111
11111111
11111111
11111111
11111111
WAL true

TIL t
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00011000
WAL true

TIL u
00011111
00111111
01111111
11111111
11111111
11111111
11111111
11111111

TIL v
00000000
10001000
00000000
00100010
00000000
10001000
00000000
00100010

TIL w
11011101
11011101
11011101
00100010
01110111
01110111
01110111
10001000

TIL x
00000000
00000000
00000000
00000000
00000000
10000000
01010000
01010100
>
00000000
00000000
00000000
00000000
00000000
01000000
01010000
01010100

TIL y
00000000
00000000
00000000
00010000
00001010
01001010
00101010
00101010
>
00000000
00000000
00000000
00001000
00001001
00101010
00101010
00101010

TIL z
00000000
00100000
01110000
00000000
00000000
00000000
00000110
00000000

SPR A
00111100
01101000
01111100
00011000
00111100
00111100
00011000
00011000
>
00111100
01101000
01111100
00011000
00111100
00111100
00010100
00100100
POS 0 4,7

SPR a
00100000
01100100
10110010
11111001
00011101
00111111
00111111
01011111
>
00100000
01100001
10110001
11111001
00011101
00111111
00111111
01011111
DLG SPR_0
POS 1 5,9

SPR b
00111000
01010100
00111000
00010000
00111000
01111100
01111100
01111010
DLG SPR_1
POS 3 3,5

SPR c
01110000
00100000
01110000
00110000
01111111
01110011
00110011
00110000
>
01110000
00100000
01110000
00110111
01111011
01110011
00110000
00110000
DLG SPR_3
POS 2 11,9

SPR d
00001110
00000100
00001110
00010100
00101110
01010110
10000110
00000110
>
00001110
00000100
00001110
00110100
11011110
00000110
00000110
00000110
DLG SPR_5
POS 1 9,6

SPR e
00111100
00100100
00100100
00100110
00011000
01111110
01011010
01011010
DLG SPR_4
POS 3 14,13

SPR f
11111111
10000001
10000001
10000101
10000101
10000001
10000001
11111111
DLG SPR_2
POS 2 8,4

ITM 0
00000000
00000000
01111100
00100100
01100100
00100100
01111100
00000000
NAME tea
DLG ITM_0

DLG SPR_0
"""
{
  - kbeans == 1 ?
    Jess is excited for more treats, but you don't want her to get fat!
  - treats == 1 ?
    {kbeans = 1}Jess can smell a treat on you. She's super pumped about this development.
    You give Jess a treat, and she proudly shows you where she has buried some...kidney beans?
    Why would Jess have kidney beans? Oh well. She appears to be offering them to you, and you don't want to appear rude. You are now carrying kidney beans.
  - else ?
    Jess is digging and wagging. What a good pup!
}
"""

DLG ITM_0
{book = 1}Who just leaves a book sitting out in the open? You pick it up and take it with you.

DLG SPR_1
"""
{
  - key == 1 ?
    "Hope you're able to find whatever that key unlocks!"
  - book == 1 ?
    {key = 1}The librarian takes the book from you.
    "Thanks for returning the book! If you're feeling like doing some more good samaritan work, perhaps you can figure out what this key is for?"
    The librarian hands you a key.
  - else ?
    "Welcome to Erry's library! There's so many books here, and all the managers listen to the concerns of the people working under them!"
}
"""

DLG SPR_2
"""
{
  - treats == 1 ?
    You've already taken a few treats, you're going to leave the rest in there for someone else.
  - key == 1 ?
    {treats = 1}You notice that the key fits the lock on this little safe perfectly!
    You open it, and find...dog treats? I wonder who put them here? You take a few, just in case a treat-worthy pup crosses your path.
  - else ?
    There's a small locked safe here! What could be inside?
}
"""

DLG SPR_3
"""
{
  - cbeans == 1 ?
    You can't get the bean collector's attention. He's too interested in whispering to his newly acquired kidney beans. Good for him.
  - kbeans == 1 ?
    {cbeans = 1}"{wvy}WOAH!{wvy} Are those some muthfuggin {rbw}{wvy}KIDNEY BEANS?{wvy}{rbw}
    I'll trade you those kidney beans for some coffee beans! Sound fair?"
    I guess? You're not too fussed about which sort of beans you have in your pockets. You swap your kidney beans for coffee beans.
  - else ?
    "I'm a bean collector! Do you have any beans to trade?"
    You inform him regretfully that you do not.
}
"""

DLG SPR_4
"""
{
  - coffee == 1 ?
    The wonderful aroma of coffee now fills the library.
  - cbeans == 1 ?
    {coffee = 1}You take advantage of this coffee grinder to make coffee grounds from your beans, and then you put them in the press. After adding hot water and leaving it to brew for a minute, you now have some hot black coffee!
  - else ?
    It's a coffee beans grinder, and a coffee press. If you had some coffee beans, you could make some fresh coffee here!
}
"""

DLG SPR_5
"""
{
  - card == 1 ?
    "Thanks for the coffee honey, and I hope you had fun on your birthday quest! This is the end of the game, so you can quit now and you won't be missing anything.
    However...
    
    If you're the sort of person who wants to find secrets, every character or object in this game has 3 things to say, except one of them, which has 4. Did you find that fourth message?"
  - coffee == 1 ?
    {card = 1}You give Peter the coffee.
    "Aw yiss!"
    He sips the coffee excitedly!
    "Thanks honey! Happy birthday!"
    He hands you a birthday card. It has a picture of an owl on the front, and the owl is saying "Hooty Birthday".
    Inside the card is written:
    "Happy birthday honey! Thanks for getting me the yummy coffees, and for thinking of me when you're near the pepperoni shop! I love you :)"
  - cbeans == 1 ?
    "Cool, did the bean collector give them to you?"
    So we're just acting like being a bean collector is a normal thing? That's fine.
    "They look good, but I can't eat them as they are. Remember when I bought a whole bag of coffee beans because I was going to eat coffee beans raw? I ate like...3. Before we had to get rid of them because we had no grinder."
  - else ?
    It's your boyfriend, practising the EWI.
    "Hey honey! I have a card to give you, but I really need some coffee first. Any chance you could find me some?"
    You remind him that it's {wvy}your{wvy} birthday, and you shouldn't have to go on coffee quests on your birthday.
    "There's no such thing as {wvy}my{wvy} birthday in a relationship. We just get two of {wvy}our{wvy} birthdays."
}
"""

VAR book
0

VAR coffee
0

VAR cbeans
0

VAR kbeans
0

VAR treats
0

VAR key
0

VAR card
0

