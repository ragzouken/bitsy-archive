

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
156,167,166
0,0,0
255,255,255

PAL 1
255,255,255
255,255,255
255,255,255

ROOM 0
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,0,d,d,d,d,d,i,d,d,d,d,0,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,l,0,0,0,0,0,0,0,0,0,0,j,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,b,0,0,0,0,0,0,0,0,0,0,a,n,n
n,n,0,c,c,c,c,c,k,c,c,c,c,0,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME lobby
EXT 1,7 1 14,7
EXT 7,1 2 9,9
EXT 14,7 3 2,11
EXT 2,7 1 14,7
EXT 13,7 3 2,11
EXT 8,3 2 9,9
END 0 8,12
PAL 0

ROOM 1
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
0,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
b,0,0,0,0,0,b,0,0,0,0,0,0,0,0,a
b,0,0,0,0,0,b,0,0,0,0,0,0,0,0,a
b,0,0,0,0,0,b,0,0,0,0,0,0,0,0,a
b,0,0,0,0,0,0,j,d,d,d,i,d,d,d,a
b,0,0,0,0,f,c,g,m,0,0,0,0,0,l,q
b,0,0,0,0,j,d,h,m,0,0,0,0,0,0,a
b,0,0,0,0,j,0,b,c,c,c,k,c,c,c,a
b,0,0,0,0,a,0,l,0,0,0,0,0,0,0,a
b,0,0,0,0,a,0,b,0,0,0,0,0,0,0,a
b,0,0,0,0,a,0,b,0,0,0,0,0,0,0,a
0,c,c,c,c,c,c,c,c,c,c,c,c,c,c,0
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME fam home 1
EXT 14,7 0 2,7
EXT 8,7 5 7,7
EXT 8,8 5 7,8
PAL 0

ROOM 2
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,0,d,d,d,d,d,d,d,d,d,d,n,n,n
n,n,b,0,0,0,0,0,b,0,0,0,l,0,0,0
n,n,b,0,0,0,0,0,b,0,0,0,0,a,0,0
n,n,b,0,0,0,0,0,l,0,0,0,0,a,0,0
n,n,b,c,c,c,f,k,g,0,0,f,k,a,0,0
n,n,b,0,0,0,a,0,b,i,0,a,0,a,0,0
n,n,0,c,c,c,c,c,c,q,c,c,c,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME oxf home
EXT 9,9 0 8,3
END 0 15,13
PAL 0

ROOM 3
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,0,d,d,d,d,d,d,d,d,d,d,d,d,d,d
n,b,0,0,0,0,b,p,p,a,0,0,j,0,l,0
n,b,0,0,0,0,b,p,p,a,0,b,0,0,0,a
n,b,0,0,0,0,b,p,p,a,0,b,0,0,0,a
n,b,0,0,0,0,b,p,p,a,0,b,0,0,0,a
n,b,0,0,0,0,b,o,o,a,0,b,0,0,0,a
n,b,d,d,d,i,h,0,0,a,0,b,c,c,c,c
n,b,0,0,0,0,0,0,0,j,0,b,0,0,0,0
n,q,j,0,0,0,0,0,0,a,0,b,0,0,0,0
n,0,c,c,c,c,c,c,c,c,c,c,c,c,c,c
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME york home
EXT 7,8 6 7,7
EXT 8,8 6 8,7
EXT 2,11 0 13,7
PAL 0

ROOM 5
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
0,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
b,0,0,0,0,0,b,0,0,0,0,0,0,0,0,a
b,0,0,i,d,d,h,d,0,0,0,0,0,0,0,a
b,c,g,0,0,0,0,0,j,0,0,0,0,0,0,a
b,0,l,0,0,0,0,0,a,0,0,0,0,0,0,a
b,0,b,0,0,0,0,m,n,n,n,n,n,k,c,a
b,d,h,0,0,0,0,m,n,n,n,n,n,d,d,a
b,0,b,0,0,0,0,0,0,j,0,0,0,0,0,a
b,0,b,0,0,0,0,0,b,0,0,0,0,0,0,a
b,0,0,k,c,c,f,c,c,0,0,0,0,0,0,a
b,0,0,0,0,0,a,0,0,0,0,0,0,0,0,a
0,c,c,c,c,c,c,c,c,c,c,c,c,c,c,0
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME fam home 2
EXT 7,7 1 8,7
EXT 7,8 1 8,8
PAL 0

ROOM 6
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,0,d,d,d,d,d,d,d,d,d,d,d,d,d,d
n,b,0,0,0,0,l,0,0,0,0,j,0,0,0,0
n,b,0,0,0,0,b,0,0,0,0,a,0,0,0,0
n,b,0,0,0,0,b,0,0,0,0,a,0,0,0,0
n,b,0,0,0,0,b,o,o,a,0,a,0,0,0,0
n,b,0,0,0,0,b,p,p,a,0,a,0,0,0,0
n,b,0,0,0,0,b,p,p,f,k,c,c,c,c,c
n,b,0,0,0,0,b,p,p,a,0,0,0,0,0,0
n,b,0,0,0,0,b,p,p,a,0,0,0,0,0,0
n,0,c,c,c,c,c,c,c,c,c,c,c,c,c,c
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
NAME york home 2
EXT 7,7 3 7,8
EXT 8,7 3 8,8
PAL 0

TIL a
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME left wall
WAL true

TIL b
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME right wall
WAL true

TIL c
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME top wall
WAL true

TIL d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME bottom wall
WAL true

TIL e
10000000
10000000
10000000
10000000
10000000
10000000
10000000
11111111
NAME left b corner
WAL true

TIL f
11111111
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME left t corner
WAL true

TIL g
11111111
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME right t corner
WAL true

TIL h
00000001
00000001
00000001
00000001
00000001
00000001
00000001
11111111
NAME right b corner
WAL true

TIL i
00000011
00000111
00001101
00011001
00110001
01100001
11000001
10000001
NAME b door

TIL j
11000000
11100000
00110000
00011000
00001100
00000110
00000011
11111111
NAME l door

TIL k
10000011
10000011
10000110
10001100
10011000
10110000
11100000
11000000
NAME t door
WAL false

TIL l
11111111
11000000
01100000
00110000
00011000
00001100
00000111
00000011
NAME r door

TIL m
11011010
11011010
11011010
11011010
11011010
11011010
11011010
11011010
NAME stairs

TIL n
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME blank space
WAL true

TIL o
11111111
11111111
00000000
11111111
11111111
00000000
11111111
00000000
NAME stairs-north

TIL p
11111111
11111111
00000000
11111111
11111111
00000000
11111111
00000000
NAME stairs-north-block
WAL true

TIL q
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME blank-block
WAL true

SPR A
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 0 8,10

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME host
DLG SPR_0
POS 0 8,8

SPR b
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam-living
DLG SPR_1
POS 1 13,4

SPR c
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam-dining
DLG SPR_2
POS 1 3,4

SPR d
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam-kitchen
DLG SPR_9
POS 1 2,11

SPR e
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam-utility
DLG SPR_a
POS 1 6,11

SPR f
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam-garage
DLG SPR_b
POS 1 12,11

SPR g
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam2-bed
DLG SPR_7
POS 5 12,11

SPR h
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam2-parent
DLG SPR_6
POS 5 12,4

SPR i
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam2-study
DLG SPR_5
POS 5 1,11

SPR j
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam2-bathroom
DLG SPR_4
POS 5 1,6

SPR k
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME fam2-molly
DLG SPR_3
POS 5 2,3

SPR l
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME oxf-living
DLG SPR_c
POS 2 9,5

SPR m
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME oxf-garden
DLG SPR_d
POS 2 15,7

SPR n
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME oxf-kitchen
DLG SPR_e
POS 2 12,9

SPR o
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME oxf-bedroom
DLG SPR_f
POS 2 4,6

SPR p
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME oxf-bath
DLG SPR_g
POS 2 7,9

SPR q
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-living
DLG SPR_h
POS 3 10,11

SPR r
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-downstairs-bed
DLG SPR_i
POS 3 4,6

SPR s
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-kitchen
DLG SPR_j
POS 3 13,7

SPR t
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-bathroom
DLG SPR_k
POS 3 15,4

SPR u
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-bed
DLG SPR_l
POS 6 4,9

SPR v
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-joe
DLG SPR_m
POS 6 14,6

SPR w
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME york-siobhan
DLG SPR_n
POS 6 13,10

SPR x
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME lobby-parent
DLG SPR_o
POS 0 3,8

SPR y
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME lobby-oxf
DLG SPR_p
POS 0 7,4

SPR z
01111110
01111110
01111110
01111110
00001000
00001000
00001000
00001000
NAME lobby-york
DLG SPR_q
POS 0 12,8

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG SPR_0
"""
Welcome to the Space of Places.

Check out the exhibits through each door around you.
The door at the bottom of the room is the exit.
We hope you enjoy your visit.
"""

DLG ITM_0
You found a nice warm cup of tea

DLG SPR_1
"""
{shuffle
  - TV on, dinners resting in laps, a cat sniffing for scraps.
  - Small children run through in an endless cacophony.
  - Fireplace became a door. Table became rug became table.
}
"""

DLG SPR_2
"""
{shuffle
  - A rattle of dice in the night every so often.
  - A shout, a smash, a fist on an already broken table.
  - Rare dinners together, always the same - fajitas, stew, etc.
}
"""

DLG SPR_3
"""
{shuffle
  - Used to be mine. Then no one's. Then someone brand new.
  - An aquarium of brightly coloured fish swim under another layer.
  - Feel like a stranger here. Nothing familiar to speak of.
}
"""

DLG SPR_5
"""
{shuffle
  - Spinning and spinning until you tell me to stop.
  - A hundred boxes, a hundred eBay listings, a fire in your eyes.
  - Bizarre painting of two people fucking while watching football.
}
"""

DLG SPR_6
"""
{shuffle
  - Sneak in on tip-toes so they'll never hear a thing.
  - Christmas presents in the same place in the cupboard.
  - Tiny TV in the corner. Can't sleep. We watch nonsense.
}
"""

DLG SPR_7
"""
{shuffle
  - This is no longer mine. It hasn't been for years.
  - A lock on a door has a thousand different uses.
  - Skin crawls. Body won't move. You scream in whispers.
}
"""

DLG SPR_8
"""
{shuffle
  - Running too fast. 
  - 
}
"""

DLG SPR_9
"""
{shuffle
  - Running too fast. The plate hits the ground. I cry instinctively.
  - There are few pleasures greater than sitting on the worktops.
  - We never saw the fire. Never saw the microwave again either.
}
"""

DLG SPR_a
"""
{shuffle
  - Food stains all over the walls. They never scrub off.
  - The machines rumble on at all times. Never fully clean.
  - Something scratches against the door. Again. And again.
}
"""

DLG SPR_b
"""
{shuffle
  - Broken glass everywhere, and my car resting in the middle.
  - A den. A gym. A playroom. Never the same for more than a year.
  - New door completes the loop. I will never get used to it.
}
"""

DLG SPR_4
"""
{shuffle
  - I come home to find Korean toothpaste. Cannot compute.
  - Bath handle broken. Water on the floor. You nearly drown.
  - Never gets hot. Then too hot. Then too cold. Rinse. Repeat.
}
"""

DLG SPR_c
"""
{shuffle
  - You shout at me from outside. I call you a cab.
  - Workspace. Living space. Music space. Too much.
  - Soft keyboard tones ring out. I pretend I can't here.
}
"""

DLG SPR_d
"""
{shuffle
  - We leave the chairs outside. We don't know any better.
  - Water trickles down the doors. Upstairs plants look lovely.
  - Smoke a cigar. What a stupid idea.
}
"""

DLG SPR_e
"""
{shuffle
  - Too small for two people to work together in.
  - Warm bread smell fills the whole space. Tastes amazing.
  - The machine jumps, screams,  splutters, dies. New one's OK.
}
"""

DLG SPR_f
"""
{shuffle
  - Dust. Just... dust. Everywhere.
  - Cuddle up so we can both see the screen. Stay here for hours.
  - Can't believe we got the deposit back.
}
"""

DLG SPR_g
"""
{shuffle
  - No ventilation. Tiny. Never gets dry. Damp. Damp. Damp.
  - Shower curtain clings to me in tiny cubicle.
  - The spiders I understand. The millipedes give me pause.
}
"""

DLG SPR_h
"""
{shuffle
  - There's nothing here but drying clothes.
  - Morning meetings to discuss the night before. Not invited.
  - Huddled round the laptop to watch University Challenge.
}
"""

DLG SPR_i
I have nothing to say here. Didn't know the guy.

DLG SPR_j
"""
{shuffle
  - Peppers. Onions. Tomato sauce. Sweetcorn. Pasta. Every night.
  - Hoodie hung over oven to dry on a cold, wet day.
  - Bike stolen from back garden. Not ours. Returned next day.
}
"""

DLG SPR_k
"""
{shuffle
  - Slug lives in bathmat. Don't touch slug. He's OK.
  - Shower with bugs and other creatures. Just ignore them.
  - Too many mornings with my head in the toilet. Good times.
}
"""

DLG SPR_l
"""
{shuffle
  - Dominos. Pizza Hut. Dominos. Never take the boxes out.
  - We play games when you're free. It's not very often.
  - Developed a lot of bad habits. Still haven't broken them.
}
"""

DLG SPR_m
"""
{shuffle
  - Gave you may mattress topper when I left. Hope you liked it.
  - Barely big enough to fit your stuff. You make do.
  - Rarely saw you in here. Busy busy.
}
"""

DLG SPR_n
I only remember you crying softly one night.

DLG SPR_o
Weir, 2002-2011, 2014-2015

DLG SPR_p
Oxford, 2017-2018

DLG SPR_q
York, 2015-2016

END 0
Game by Ric Cowley. Inspired by the work of Lauren Hollowday.

END 1


VAR a
42

