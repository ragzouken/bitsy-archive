Episode 1 - High School

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
NAME Normal
51,30,1
99,65,1
255,255,255

PAL 1
NAME Insane
204,225,254
156,190,254
0,0,0

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,b,b,b,b,d,b,b,b,b,b,b,b,b,b,b
a,g,c,c,c,0,c,g,e,e,e,e,e,e,e,g
a,f,q,r,0,d,0,f,q,r,q,d,q,q,r,f
a,0,r,q,d,0,d,0,d,h,d,0,d,0,d,0
a,d,0,d,0,d,0,d,0,d,0,t,0,d,0,d
a,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME HallwayFarLeft
ITM 0 5,5
EXT 2,9 1 2,9
EXT 1,8 1 1,8
EXT 15,7 2 0,7
EXT 15,8 2 0,8
EXT 15,9 2 0,9
EXT 1,7 1 1,7
EXT 2,8 1 2,8
EXT 3,8 1 3,8
EXT 4,6 1 4,6
EXT 4,7 1 4,7
EXT 14,7 1 14,7
EXT 13,7 1 13,7
EXT 12,7 1 12,7
EXT 11,6 1 11,6
EXT 10,7 1 10,7
EXT 9,7 1 9,7
EXT 8,7 1 8,7
END 0 5,3
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,b,b,b,b,d,b,b,b,b,b,b,b,b,b,b
a,g,c,c,c,0,c,g,e,e,e,e,e,e,e,g
a,f,q,r,0,d,0,f,q,r,q,d,q,q,r,f
a,0,r,q,d,0,d,0,d,h,d,0,d,0,d,0
a,d,0,d,0,d,0,d,0,d,0,t,0,d,0,d
a,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME IHallwayFarLeft
EXT 3,9 0 3,9
EXT 4,8 0 4,8
EXT 5,7 0 5,7
EXT 5,6 0 5,6
EXT 8,8 0 8,8
EXT 7,7 0 7,7
EXT 9,8 0 9,8
EXT 10,8 0 10,8
EXT 11,7 0 11,7
EXT 12,8 0 12,8
EXT 13,8 0 13,8
EXT 14,8 0 14,8
EXT 15,7 0 15,7
END 0 5,3
PAL 1

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,a,a,a,a,a,a,a,a,a,a,d,a,a,a
b,d,b,b,b,b,b,b,b,b,b,b,0,b,b,b
c,0,c,g,e,e,e,e,e,e,g,c,d,c,g,e
0,d,0,f,r,r,q,r,q,d,f,d,0,d,f,d
d,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,0,d,0,d,s,d,0,d,0,d,0,d,h,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME HallwayLeft
EXT 0,7 0 15,7
EXT 0,8 0 15,8
EXT 0,9 0 15,9
EXT 9,7 3 9,7
EXT 15,6 4 0,6
EXT 15,7 4 0,7
EXT 15,8 4 0,8
EXT 15,9 4 0,9
EXT 8,7 3 8,7
EXT 7,7 3 7,7
EXT 6,7 3 6,7
EXT 5,7 3 5,7
EXT 4,7 3 4,7
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,a,a,a,a,a,a,a,a,a,a,0,a,a,a
b,d,b,b,b,b,b,b,b,b,b,b,0,b,b,b
c,0,c,g,e,e,e,e,e,e,g,c,d,c,g,e
0,d,0,f,r,r,q,r,q,d,f,d,0,d,f,d
d,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,0,d,0,d,s,d,0,d,0,d,0,d,h,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME IHallwayLeft
EXT 0,7 0 14,7
EXT 0,8 0 14,8
EXT 0,9 0 14,9
EXT 9,8 2 9,8
EXT 10,7 2 10,7
EXT 8,8 2 8,8
EXT 7,8 2 7,8
EXT 6,8 2 6,8
EXT 5,8 2 5,8
EXT 4,8 2 4,8
EXT 3,7 2 3,7
PAL 1

ROOM 4
0,0,0,0,0,a,0,d,0,d,a,0,0,0,0,0
0,0,0,0,0,a,d,0,d,0,a,0,0,0,0,0
0,0,0,0,0,a,0,d,0,d,a,0,0,0,0,0
a,a,a,a,a,a,d,0,d,0,a,a,a,a,a,a
b,b,b,b,b,b,0,d,0,d,b,b,b,b,b,b
e,e,e,e,g,c,r,r,d,0,c,g,e,e,e,e
0,d,q,d,f,r,0,r,0,d,0,f,r,t,0,d
d,0,d,0,d,r,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,s,d,0,d,0,d,0,d,0,d,0,d,0,d,0
a,a,a,a,a,a,q,q,0,d,a,a,a,a,a,a
b,b,b,b,b,a,q,q,d,q,a,b,b,b,b,b
c,c,c,c,c,a,0,t,0,d,a,c,c,c,c,c
0,0,0,0,0,a,a,0,d,a,a,0,0,0,0,0
0,0,0,0,0,b,b,d,0,b,b,0,0,0,0,0
0,0,0,0,0,c,c,0,d,c,c,0,0,0,0,0
NAME HallwayCenter
EXT 0,6 2 15,6
EXT 0,7 2 15,7
EXT 0,8 2 15,8
EXT 0,9 2 15,9
EXT 6,0 6 6,14
EXT 7,0 6 7,14
EXT 8,0 6 8,14
EXT 9,0 6 9,14
EXT 1,6 5 1,6
EXT 2,7 5 2,7
EXT 3,6 5 3,6
EXT 4,7 5 4,7
EXT 5,8 5 5,8
EXT 6,7 5 6,7
EXT 7,7 5 7,7
EXT 6,4 5 6,4
EXT 7,4 5 7,4
EXT 8,5 5 8,5
EXT 8,6 5 8,6
EXT 12,7 5 12,7
EXT 6,9 5 6,9
EXT 7,9 5 7,9
EXT 8,10 5 8,10
EXT 8,11 5 8,11
EXT 9,10 5 9,10
EXT 8,12 5 8,12
EXT 7,12 5 7,12
EXT 6,12 5 6,12
EXT 13,6 5 13,6
EXT 15,6 a 0,6
EXT 15,7 a 0,7
EXT 15,8 a 0,8
EXT 15,9 a 0,9
END 1 7,15
END 1 8,15
PAL 0

ROOM 5
0,0,0,0,0,a,0,d,0,d,a,0,0,0,0,0
0,0,0,0,0,a,d,0,d,0,a,0,0,0,0,0
0,0,0,0,0,a,0,d,0,d,a,0,0,0,0,0
a,a,a,a,a,a,d,0,d,0,a,a,a,a,a,a
b,b,b,b,b,b,0,d,0,d,b,b,b,b,b,b
e,e,e,e,g,c,r,r,d,0,c,g,e,e,e,e
0,d,q,d,f,r,0,r,0,d,0,f,r,t,0,d
d,0,d,0,d,r,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,s,d,0,d,0,d,0,d,0,d,0,d,0,d,0
a,a,a,a,a,a,q,q,0,d,a,a,a,a,a,a
b,b,b,b,b,a,q,q,d,q,a,b,b,b,b,b
c,c,c,c,c,a,0,t,0,d,a,c,c,c,c,c
0,0,0,0,0,a,a,0,d,a,a,0,0,0,0,0
0,0,0,0,0,b,b,d,0,b,b,0,0,0,0,0
0,0,0,0,0,c,c,0,d,c,c,0,0,0,0,0
NAME IHallwayCenter
EXT 1,7 4 1,7
EXT 2,8 4 2,8
EXT 0,6 4 0,6
EXT 3,7 4 3,7
EXT 4,8 4 4,8
EXT 5,9 4 5,9
EXT 6,8 4 6,8
EXT 7,8 4 7,8
EXT 6,3 4 6,3
EXT 7,3 4 7,3
EXT 8,4 4 8,4
EXT 9,5 4 9,5
EXT 9,6 4 9,6
EXT 8,7 4 8,7
EXT 8,9 4 8,9
EXT 9,9 4 9,9
EXT 8,13 4 8,13
EXT 7,13 4 7,13
EXT 11,7 4 11,7
EXT 12,8 4 12,8
EXT 13,7 4 13,7
EXT 14,6 4 14,6
END 1 7,15
END 1 8,15
PAL 1

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,d,a,a,a,a,a,a,a,a,a,a,0,a,a
a,b,0,b,b,b,b,b,b,b,b,b,b,d,b,a
a,c,d,c,g,e,e,e,e,e,e,g,c,s,c,a
a,d,0,d,f,r,q,t,q,d,0,f,0,d,0,a
a,0,d,0,d,0,d,0,d,0,d,0,d,0,d,a
a,d,h,d,0,d,0,d,0,d,0,d,0,d,0,a
a,s,d,0,d,0,d,0,d,0,d,0,d,0,d,a
a,a,a,a,a,a,0,d,0,d,a,a,a,a,a,a
b,b,b,b,b,a,d,0,d,0,a,b,b,b,b,b
c,c,c,c,c,a,0,d,0,d,a,c,c,c,c,c
0,0,0,0,0,a,d,0,d,0,a,0,0,0,0,0
0,0,0,0,0,b,0,d,0,t,b,0,0,0,0,0
0,0,0,0,0,c,d,0,d,0,c,0,0,0,0,0
NAME HallwayTop
ITM 1 14,6
EXT 10,7 7 10,7
EXT 9,6 7 9,6
EXT 6,15 4 6,1
EXT 7,15 4 7,1
EXT 8,15 4 8,1
EXT 9,15 4 9,1
EXT 2,3 9 2,14
EXT 8,7 7 8,7
EXT 7,6 7 7,6
EXT 6,7 7 6,7
EXT 5,7 7 5,7
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,d,a,a,a,a,a,a,a,a,a,a,0,a,a
a,b,0,b,b,b,b,b,b,b,b,b,b,d,b,a
a,c,d,c,g,e,e,e,e,e,e,g,c,s,c,a
a,d,0,d,f,r,q,t,q,d,0,f,0,d,0,a
a,0,d,0,d,0,d,0,d,0,d,0,d,0,d,a
a,d,h,d,0,d,0,d,0,d,0,d,0,d,0,a
a,s,d,0,d,0,d,0,d,0,d,0,d,0,d,a
a,a,a,a,a,a,0,d,0,d,a,a,a,a,a,a
b,b,b,b,b,a,d,0,d,0,a,b,b,b,b,b
c,c,c,c,c,a,0,d,0,d,a,c,c,c,c,c
0,0,0,0,0,a,d,0,d,0,a,0,0,0,0,0
0,0,0,0,0,b,0,d,0,t,b,0,0,0,0,0
0,0,0,0,0,c,d,0,d,0,c,0,0,0,0,0
NAME IHallwayTop
ITM 1 14,6
EXT 9,7 6 9,7
EXT 10,8 6 10,8
EXT 11,7 6 11,7
EXT 7,7 6 7,7
EXT 8,8 6 8,8
EXT 6,8 6 6,8
EXT 5,8 6 5,8
EXT 4,7 6 4,7
PAL 1

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,0,0,0,0,0,0
a,b,b,b,b,n,m,l,b,a,0,0,0,0,0,0
a,c,c,c,c,i,j,k,c,a,0,0,0,0,0,0
a,p,p,p,p,p,p,p,p,a,0,0,0,0,0,0
a,u,p,p,o,o,o,o,o,a,0,0,0,0,0,0
a,p,p,p,p,p,p,p,p,a,0,0,0,0,0,0
a,p,p,p,o,o,o,o,o,a,0,0,0,0,0,0
a,a,p,a,a,a,a,a,a,a,0,0,0,0,0,0
b,b,p,b,b,b,b,b,b,b,0,0,0,0,0,0
c,c,p,c,c,c,c,c,c,c,0,0,0,0,0,0
NAME Classroom
EXT 2,15 6 2,4
PAL 0

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,0,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,d,b,b,b,b,b,b,b,b,b,b,v,w
e,c,c,0,c,g,e,e,e,z,e,e,g,c,x,y
0,f,0,d,0,f,0,d,0,d,0,d,f,t,0,d
d,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,0,d,s,d,0,d,q,q,q,d,0,d,0,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME HallwayRight
EXT 0,6 4 15,6
EXT 0,7 4 15,7
EXT 0,8 4 15,8
EXT 0,9 4 15,9
EXT 6,9 b 6,9
EXT 7,8 b 7,8
EXT 8,7 b 8,7
EXT 9,8 b 9,8
EXT 10,9 b 10,9
EXT 9,5 c 9,7
EXT 15,6 d 0,6
EXT 15,7 d 0,7
EXT 15,8 d 0,8
EXT 15,9 d 0,9
PAL 0

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,0,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,d,b,b,b,b,b,b,b,b,b,b,v,w
e,c,c,0,c,g,e,e,e,z,e,e,g,c,x,y
0,f,0,d,0,f,0,d,0,d,0,d,f,t,0,d
d,0,d,0,d,0,d,0,d,0,d,0,d,0,d,0
0,d,0,d,0,d,0,d,0,d,0,d,0,d,0,d
d,0,d,s,d,0,d,q,q,q,d,0,d,0,d,0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME IHallwayRight
EXT 0,6 4 15,6
EXT 0,7 4 15,7
EXT 0,8 4 15,8
EXT 0,9 4 15,9
EXT 8,6 a 8,6
EXT 7,7 a 7,7
EXT 6,8 a 6,8
EXT 5,9 a 5,9
EXT 9,7 a 9,7
EXT 10,8 a 10,8
EXT 11,9 a 11,9
PAL 1

ROOM c
0,0,0,0,0,0,0,a,a,a,a,a,0,0,0,0
0,0,0,0,0,0,0,a,b,b,b,a,0,0,0,0
0,0,0,0,0,0,0,a,c,c,c,a,0,0,0,0
0,0,0,0,0,0,0,a,d,0,d,a,0,0,0,0
0,0,0,0,0,0,0,a,a,d,a,a,0,0,0,0
0,0,0,0,0,0,0,b,a,0,a,b,0,0,0,0
0,0,0,0,0,0,0,c,b,d,b,c,0,0,0,0
0,0,0,0,0,0,0,0,c,0,c,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME SecretRoom
ITM 2 9,3
EXT 9,7 a 9,5
PAL 0

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,d,a,a,a,d,a,a,a,0,0,0,0,0,0
b,b,h,b,b,b,s,b,b,a,0,0,0,0,0,0
c,c,d,c,g,c,d,c,c,a,0,0,0,0,0,0
0,d,0,d,f,t,0,d,0,a,0,0,0,0,0,0
d,s,d,0,d,0,d,0,d,a,0,0,0,0,0,0
0,d,0,d,0,d,h,d,0,a,0,0,0,0,0,0
d,0,d,s,d,0,d,0,t,a,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,0,0,0,0,0,0
b,b,b,b,b,b,b,b,b,b,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME HallwayFarRight
EXT 0,6 a 15,6
EXT 0,7 a 15,7
EXT 0,8 a 15,8
EXT 0,9 a 15,9
END 2 6,2
PAL 0

TIL a
01010101
10000000
00000001
10000000
00000001
10000000
00000001
10101010
NAME Roof
WAL true

TIL b
11111111
00000000
00000000
00000000
00000000
00000000
00000000
11010011
NAME WallUpper
WAL true

TIL c
11010011
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME WallLower
WAL true

TIL d
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME Floor

TIL e
11111111
10000001
10111101
10000001
10001101
10000001
10000001
11111111
NAME Locker
WAL true

TIL f
10000001
11000011
11111111
11000111
11010111
11011011
01111110
00111100
NAME TrashCanB
WAL true

TIL g
11010011
00000000
00000000
00000000
00111100
01000010
10000001
10000001
NAME TrashCanT
WAL true

TIL h
00000000
00011100
00111110
01111100
01111110
01111110
00110100
00000000
NAME SmallStain

TIL i
11011101
01000000
01011111
01000000
01000000
01111111
00000000
11111111
NAME BlackboardBL
WAL true

TIL j
11101101
00000000
01101111
00000000
00000000
11111111
00000000
11111111
NAME BlackboardB
WAL true

TIL k
10111011
00000010
10000010
00000010
00000010
11111110
00000000
11111111
NAME BlackboardBR
WAL true

TIL l
11111111
00000000
11111110
00000010
00000010
11111010
00000010
00000011
NAME BlackboardTR
WAL true

TIL m
11111111
00000000
11111111
00000000
00000000
00001111
00000000
00000000
NAME BlackboardT
WAL true

TIL n
11111111
00000000
01111111
01000000
01000000
01011110
01000000
11000000
NAME BlackboardTL
WAL true

TIL o
00000000
00111100
01111110
01111110
01000010
01000010
00000000
00000000
NAME Besk
WAL true

TIL p
11110000
11110000
11110000
11110000
00001111
00001111
00001111
00001111
NAME ClassroomFloor

TIL q
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Person(Male)
WAL true

TIL r
00111000
01011000
00000000
00111100
01011010
01011010
00000000
00100100
>
01000000
00111000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Person(Female)
WAL true

TIL s
00000100
00001000
00010000
00001000
00111100
01000010
10000010
00000001
NAME Crack

TIL t
11111011
11110111
11101111
11110111
11000011
10111101
01111101
11111110
NAME TileCrack

TIL u
11110000
01110001
10010110
11101000
00010111
10100011
01001011
10001101
NAME ClassroomTileCrack

TIL v
11111111
00000000
01111111
01000000
01011111
01010000
01010010
01010101
NAME PictureTL
WAL true

TIL w
11111111
00000000
11111110
00000010
11111010
00001010
01101010
00001010
NAME PictureTR
WAL true

TIL x
01010100
01010100
01011000
01011111
01000000
01111111
00000000
11111111
NAME PictureBL
WAL true

TIL y
10001010
01101010
00011010
11111010
00000010
11111110
00000000
11111111
NAME PictureBR
WAL true

TIL z
11111111
10000001
10110101
10000001
10001101
10000001
10000001
11111111
NAME FalseLocker

SPR A
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
POS 0 5,4

SPR a
00111000
01011000
00000000
00111100
01011010
01011010
00000000
00100100
>
01000000
00111000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Girl1
POS 0 1,9

SPR b
00111000
01011000
00000000
00111100
01011010
01011010
00000000
00100100
>
01000000
00111000
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGirl1
DLG SPR_0
POS 1 1,9

SPR c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME LockedIndicator1
DLG SPR_2
POS 2 1,3

SPR d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME LockedIndicator2
DLG SPR_1
POS 2 12,3

SPR e
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Guy1
POS 2 9,6

SPR f
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGuy1
DLG SPR_3
POS 3 9,6

SPR g
00111000
01011000
00000000
00111100
01011010
01011010
00000000
00100100
>
01000000
00111000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Girl2
POS 4 6,6

SPR h
00111000
01011000
00000000
00111100
01011010
01011010
00000000
00100100
>
01000000
00111000
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGirl2
DLG SPR_4
POS 5 6,6

SPR i
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Guy2
POS 4 9,12

SPR j
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGuy2
DLG SPR_5
POS 5 9,12

SPR k
00011100
00011010
00000000
00111100
01011010
01011010
00000000
00100100
>
00000010
00011100
00011000
00000000
00111100
01011010
01011010
00100100
NAME Girl3
POS 6 10,6

SPR l
00011100
00011010
00000000
00111100
01011010
01011010
00000000
00100100
>
00000010
00011100
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGirl3
DLG SPR_6
POS 7 10,6

SPR m
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME LockedIndicator3
DLG SPR_7
POS 6 13,3

SPR n
10111101
10111101
10111101
10100101
00100100
00100100
00100100
00100100
NAME Teacher1Lower
DLG SPR_8
POS 9 8,9

SPR o
00000000
00111000
00111000
00111000
00011000
01111110
10111101
10111101
NAME TeacherUpper
POS 9 8,8

SPR p
11111111
10000001
10111101
10000001
10001101
10000001
10000001
11111111
NAME Locker
DLG SPR_9
POS 0 11,5

SPR q
11111111
10000001
10111101
10000001
10001101
10000001
10000001
11111111
NAME ILocker
DLG SPR_a
POS 1 11,5

SPR r
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME Guy3
POS a 8,8

SPR s
00011000
00011000
00000000
00111100
01011010
01011010
00000000
00100100
>
00000000
00011000
00011000
00000000
00111100
01011010
01011010
00100100
NAME IGuy3
DLG SPR_b
POS b 8,8

SPR t
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Councillor
DLG SPR_c
POS a 3,3

SPR u
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME FemaleToilet
DLG SPR_d
POS d 2,2

ITM 0
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Info
DLG ITM_0

ITM 1
00000000
00000000
00001000
00011100
00111000
00010000
00000000
00000000
NAME JenniferCard
DLG ITM_1

ITM 2
00000000
00000000
00001000
00011100
00111000
00010000
00000000
00000000
NAME Mr Wright Card
DLG ITM_2

DLG ITM_0
Maybe the teachers can help me. But no kids. It's too soon.

DLG SPR_0
Hey there David. Great job in English today. Hope you're ready for math next.

DLG SPR_1
It's locked!

DLG SPR_2
It's locked!

DLG SPR_3
Get out of my face dude!

DLG SPR_4
You know you're never going to get laid with a face like that pal.

DLG SPR_5
Hey man. Hope you did well in your English class today!

DLG SPR_6
Did someone say something?

DLG ITM_1
"""
You found the {rbw}{wvy}Jennifer Card{wvy}{rbw}!

She's the only girl that's ever nice to you.
"""

DLG SPR_7
It's locked!

DLG SPR_8
"""
Oh hello there son.

How you holding up?
"""

DLG ITM_2
"""
You found the {rbw}{wvy}Mr Wright Card{wvy}{rbw}!

He's one of the best teachers. He always makes sure everything is okay.
"""

DLG SPR_9
My locker. Someone's given me another note. This one says "freak".

DLG SPR_a
My locker. Someone's given me another note. This one says "freak".

DLG SPR_b
Beat it runt, unless you wanna be next.

DLG SPR_c
That's the councilors office. I can't go in there yet.

DLG SPR_d
That's the ladies room!

END 0
You decide to return to the classroom as it is the safest place for you to be right now. (Ending 1/3)

END 1
You decide to leave the building and head home to pretend you don't exist. You're safer there. (Ending 2/3)

END 2
You walk into the bathroom, drop to your knees, and begin to sob. You realize that you can't deal with this anymore. (Ending 3/3)

VAR a
42

