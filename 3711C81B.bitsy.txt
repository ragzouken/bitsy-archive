
Another Round of Pong

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
0,0,0
255,255,255
255,255,255

ROOM 0
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME middle
EXT 7,9 1 7,9
EXT 6,9 1 6,9
EXT 5,9 1 5,9
EXT 4,9 1 4,9
EXT 3,9 1 3,9
EXT 2,9 1 2,9
EXT 1,9 1 1,9
EXT 8,6 1 8,6
EXT 9,6 1 9,6
EXT 10,6 1 10,6
EXT 11,6 1 11,6
EXT 12,6 1 12,6
EXT 13,6 1 13,6
EXT 14,6 1 14,6
EXT 7,6 6 7,6
EXT 6,6 6 6,6
EXT 5,6 6 5,6
EXT 4,6 6 4,6
EXT 3,6 6 3,6
EXT 2,6 6 2,6
EXT 1,6 6 1,6
EXT 8,9 6 8,9
EXT 9,9 6 9,9
EXT 10,9 6 10,9
EXT 11,9 6 11,9
EXT 12,9 6 12,9
EXT 13,9 6 13,9
EXT 14,9 6 14,9
END undefined 0,13
END undefined 0,12
END undefined 0,11
END undefined 0,10
END undefined 0,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END 0 15,2
END 0 15,3
END 0 15,4
END 0 15,5
END 0 15,10
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 12,0
END undefinee 11,0
END undefinee 13,0
END undefinee 14,0
PAL 0

ROOM 1
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME left1
EXT 1,8 0 1,8
EXT 2,8 0 2,8
EXT 3,8 0 3,8
EXT 4,8 0 4,8
EXT 5,8 0 5,8
EXT 6,8 0 6,8
EXT 7,8 0 7,8
EXT 8,9 6 8,9
EXT 1,10 3 1,10
EXT 2,10 3 2,10
EXT 3,10 3 3,10
EXT 4,10 3 4,10
EXT 5,10 3 5,10
EXT 6,10 3 6,10
EXT 7,10 3 7,10
EXT 7,6 0 7,6
EXT 8,5 3 8,5
EXT 9,5 3 9,5
EXT 10,5 3 10,5
EXT 11,5 3 11,5
EXT 12,5 3 12,5
EXT 13,5 3 13,5
EXT 14,5 3 14,5
EXT 8,7 0 8,8
EXT 9,7 0 9,8
EXT 10,7 0 10,8
EXT 11,7 0 11,8
EXT 12,7 0 12,8
EXT 13,7 0 13,8
EXT 14,7 0 14,8
END undefined 0,11
END undefined 0,12
END undefined 0,13
END undefined 0,6
END undefined 0,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END 0 15,2
END 0 15,3
END 0 15,4
END 0 15,9
END 0 15,10
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 0,0
END undefinee 1,0
END undefinee 3,0
END undefinee 2,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

ROOM 3
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME left2
EXT 1,9 1 1,9
EXT 2,9 1 2,9
EXT 3,9 1 3,9
EXT 4,9 1 4,9
EXT 5,9 1 5,9
EXT 6,9 1 6,9
EXT 7,9 1 7,9
EXT 8,10 6 8,10
EXT 1,11 4 1,11
EXT 2,11 4 2,11
EXT 3,11 4 3,11
EXT 4,11 4 4,11
EXT 5,11 4 5,11
EXT 6,11 4 6,11
EXT 7,11 4 7,11
EXT 7,5 7 7,5
EXT 8,4 4 8,4
EXT 9,4 4 9,4
EXT 10,4 4 10,4
EXT 11,4 4 11,4
EXT 12,4 4 12,4
EXT 13,4 4 13,4
EXT 14,4 4 14,4
EXT 14,6 1 14,6
EXT 13,6 1 13,6
EXT 12,6 1 12,6
EXT 11,6 1 11,6
EXT 10,6 1 10,6
EXT 9,6 1 9,6
EXT 8,6 1 8,6
END undefined 0,13
END undefined 0,12
END undefined 0,7
END undefined 0,6
END undefined 0,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END 0 15,2
END 0 15,3
END 0 15,8
END 0 15,9
END 0 15,10
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 12,15
END undefinee 11,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

ROOM 4
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME left3
EXT 1,10 3 1,10
EXT 2,10 3 2,10
EXT 3,10 3 3,10
EXT 4,10 3 4,10
EXT 5,10 3 5,10
EXT 6,10 3 6,10
EXT 7,10 3 7,10
EXT 8,11 8 8,11
EXT 1,12 5 1,12
EXT 2,12 5 2,12
EXT 3,12 5 3,12
EXT 4,12 5 4,12
EXT 5,12 5 5,12
EXT 6,12 5 6,12
EXT 7,12 5 7,12
EXT 7,4 8 7,4
EXT 8,3 5 8,3
EXT 9,3 5 9,3
EXT 10,3 5 10,3
EXT 11,3 5 11,3
EXT 12,3 5 12,3
EXT 13,3 5 13,3
EXT 14,3 5 14,3
EXT 8,5 3 8,5
EXT 9,5 3 9,5
EXT 10,5 3 10,5
EXT 11,5 3 11,5
EXT 12,5 3 12,5
EXT 13,5 3 13,5
EXT 14,5 3 14,5
END undefined 0,13
END undefined 0,8
END undefined 0,7
END undefined 0,6
END undefined 0,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END 0 15,2
END 0 15,7
END 0 15,8
END 0 15,9
END 0 15,10
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 13,0
END undefinee 12,0
END undefinee 14,0
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

ROOM 5
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME left4
EXT 1,11 4 1,11
EXT 2,11 4 2,11
EXT 3,11 4 3,11
EXT 4,11 4 4,11
EXT 5,11 4 5,11
EXT 6,11 4 6,11
EXT 7,11 4 7,11
EXT 8,12 9 8,12
EXT 8,13 9 8,13
EXT 14,4 4 14,4
EXT 13,4 4 13,4
EXT 12,4 4 12,4
EXT 11,4 4 11,4
EXT 10,4 4 10,4
EXT 9,4 4 9,4
EXT 8,4 4 8,4
EXT 7,3 9 7,3
EXT 7,2 9 7,2
END undefined 0,9
END undefined 0,8
END undefined 0,7
END undefined 0,6
END undefined 0,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END 0 15,6
END 0 15,7
END 0 15,8
END 0 15,9
END 0 15,10
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

ROOM 6
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME right1
EXT 14,8 0 14,7
EXT 13,8 0 13,7
EXT 12,8 0 12,7
EXT 11,8 0 11,7
EXT 10,8 0 10,7
EXT 9,8 0 9,7
EXT 8,8 0 8,7
EXT 7,9 1 7,9
EXT 14,10 7 14,10
EXT 13,10 7 13,10
EXT 12,10 7 12,10
EXT 11,10 7 11,10
EXT 10,10 7 10,10
EXT 9,10 7 9,10
EXT 8,10 7 8,10
EXT 1,7 0 1,7
EXT 2,7 0 2,7
EXT 3,7 0 3,7
EXT 4,7 0 4,7
EXT 5,7 0 5,7
EXT 6,7 0 6,7
EXT 7,7 0 7,7
EXT 8,6 1 8,6
EXT 1,5 7 1,5
EXT 2,5 7 2,5
EXT 3,5 7 3,5
EXT 4,5 7 4,5
EXT 5,5 7 5,5
EXT 6,5 7 6,5
EXT 7,5 7 7,5
END undefined 0,4
END undefined 0,3
END undefined 0,2
END undefined 0,9
END undefined 0,10
END undefined 0,11
END undefined 0,12
END undefined 0,13
END 0 15,2
END 0 15,3
END 0 15,4
END 0 15,5
END 0 15,6
END 0 15,11
END 0 15,12
END 0 15,13
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 9,0
END undefinee 8,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

ROOM 7
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME right2
EXT 14,9 6 14,9
EXT 13,9 6 13,9
EXT 12,9 6 12,9
EXT 11,9 6 11,9
EXT 10,9 6 10,9
EXT 9,9 6 9,9
EXT 8,9 6 8,8
EXT 7,10 3 7,10
EXT 8,11 8 8,11
EXT 9,11 8 9,11
EXT 10,11 8 10,11
EXT 11,11 8 11,11
EXT 12,11 8 12,11
EXT 13,11 8 13,11
EXT 14,11 8 14,11
EXT 1,4 8 1,4
EXT 2,4 8 2,4
EXT 3,4 8 3,4
EXT 4,4 8 4,4
EXT 5,4 8 5,4
EXT 6,4 8 6,4
EXT 8,5 4 8,4
EXT 1,6 6 1,6
EXT 2,6 6 2,6
EXT 3,6 6 3,6
EXT 4,6 6 4,6
EXT 5,6 6 5,6
EXT 6,6 6 6,6
EXT 7,6 6 7,6
EXT 7,4 8 7,4
END undefined 0,2
END undefined 0,3
END undefined 0,8
END undefined 0,9
END undefined 0,10
END undefined 0,11
END undefined 0,12
END undefined 0,13
END 0 15,2
END 0 15,3
END 0 15,4
END 0 15,5
END 0 15,6
END 0 15,7
END 0 15,12
END 0 15,13
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
PAL 0

ROOM 8
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME right3
EXT 1,3 9 1,3
EXT 2,3 9 2,3
EXT 3,3 9 3,3
EXT 4,3 9 4,3
EXT 5,3 9 5,3
EXT 6,3 9 6,3
EXT 7,3 9 7,3
EXT 8,4 4 8,4
EXT 1,5 7 1,5
EXT 2,5 7 2,5
EXT 3,5 7 3,5
EXT 4,5 7 4,5
EXT 5,5 7 5,5
EXT 6,5 7 6,5
EXT 7,5 7 7,5
EXT 14,12 9 14,12
EXT 13,12 9 13,12
EXT 12,12 9 12,12
EXT 11,12 9 11,12
EXT 10,12 9 10,12
EXT 9,12 9 9,12
EXT 8,12 9 8,12
EXT 7,11 4 7,11
EXT 8,10 7 8,10
EXT 9,10 7 9,10
EXT 10,10 7 10,10
EXT 11,10 7 11,10
EXT 12,10 7 12,10
EXT 13,10 7 13,10
EXT 14,10 7 14,10
END undefined 0,2
END undefined 0,7
END undefined 0,8
END undefined 0,9
END undefined 0,10
END undefined 0,11
END undefined 0,12
END undefined 0,13
END 0 15,13
END 0 15,8
END 0 15,6
END 0 15,7
END 0 15,4
END 0 15,5
END 0 15,2
END 0 15,3
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 15,0
END undefinee 14,0
PAL 0

ROOM 9
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
h,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,0
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
0,0,0,0,0,0,0,a,b,0,0,0,0,0,0,h
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME right4
EXT 1,4 8 1,4
EXT 2,4 8 2,4
EXT 3,4 8 3,4
EXT 4,4 8 4,4
EXT 5,4 8 5,4
EXT 6,4 8 6,4
EXT 7,4 8 7,4
EXT 8,3 5 8,3
EXT 8,2 5 8,2
EXT 7,13 5 7,13
EXT 7,12 5 7,12
EXT 8,11 8 8,11
EXT 9,11 8 9,11
EXT 10,11 8 10,11
EXT 11,11 8 11,11
EXT 12,11 8 12,11
EXT 13,11 8 13,11
EXT 14,11 8 14,11
END undefined 0,6
END undefined 0,7
END undefined 0,8
END undefined 0,9
END undefined 0,10
END undefined 0,11
END undefined 0,12
END undefined 0,13
END 0 15,9
END 0 15,8
END 0 15,7
END 0 15,6
END 0 15,5
END 0 15,4
END 0 15,3
END 0 15,2
END undefinee 0,0
END undefinee 1,0
END undefinee 2,0
END undefinee 3,0
END undefinee 4,0
END undefinee 5,0
END undefinee 6,0
END undefinee 7,0
END undefinee 8,0
END undefinee 9,0
END undefinee 10,0
END undefinee 11,0
END undefinee 12,0
END undefinee 13,0
END undefinee 14,0
END undefinee 15,0
END undefinee 0,15
END undefinee 1,15
END undefinee 2,15
END undefinee 3,15
END undefinee 4,15
END undefinee 5,15
END undefinee 6,15
END undefinee 7,15
END undefinee 8,15
END undefinee 9,15
END undefinee 10,15
END undefinee 11,15
END undefinee 12,15
END undefinee 13,15
END undefinee 14,15
END undefinee 15,15
PAL 0

TIL a
00000000
00000000
00000001
00000001
00000000
00000000
00000001
00000001

TIL b
00000000
00000000
10000000
10000000
00000000
00000000
10000000
10000000

TIL c
00000000
00000000
00000000
11111111
11111111
00000000
00000000
00000000
WAL true

TIL d
00000000
00000000
00000000
11111111
11111111
00000001
00000001
00000001

TIL e
00000000
00000000
00000000
11111111
11111111
10000000
10000000
10000000

TIL f
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000

TIL g
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001

TIL h
00111100
00111100
00111100
00111100
00111100
00111100
00111100
00111100
WAL true

TIL i
00000000
00000000
00000000
11110111
10111101
00000000
00000000
00000000
>
00000000
00000000
00000000
11011110
11110111
00000000
00000000
00000000

SPR A
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000
POS 0 7,7

SPR a
00111100
00111100
00111100
00111100
00111100
00111100
00111100
00111100
DLG SPR_0

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG SPR_0
I'm a cat

DLG ITM_0
You found a nice warm cup of tea

END 0
Player 1 scores a point!

END undefined
Player 2 scores a point!

END undefinee
**ERROR** CYCLE IS BROKEN, PONG BALL FREED **ERROR**

VAR a
42


