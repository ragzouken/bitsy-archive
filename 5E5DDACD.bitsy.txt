space wolf tango

# BITSY VERSION 4.0

! ROOM_FORMAT 1

PAL 0
NAME black & white
0,0,0
192,192,192
255,255,255

PAL 1
0,70,35
128,255,0
255,255,0

PAL 2
64,0,128
159,64,255
202,0,202

PAL 3
119,38,0
252,150,82
183,183,0

PAL 4
0,0,64
255,0,128
255,255,0

PAL 5
0,0,64
0,157,39
173,173,173

PAL 6
0,36,72
128,128,255
255,128,255

PAL 7
21,43,0
12,104,154
206,0,0

PAL 8
36,15,33
193,219,51
85,85,242

PAL 9
0,0,64
255,0,255
255,128,0

PAL a
69,10,43
47,157,28
182,168,1

ROOM 0
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
g,g,0,0,0,h,0,0,0,0,i,0,0,0,g,g
g,0,0,0,0,0,0,0,0,0,0,0,0,0,0,g
g,0,0,0,0,0,0,0,0,0,0,0,0,0,0,g
g,0,h,0,0,0,0,0,k,0,0,0,0,0,0,g
g,0,0,0,0,0,0,0,0,0,0,0,j,0,0,g
g,0,i,0,0,i,0,0,0,0,0,0,0,0,0,0
g,0,0,0,0,0,0,0,0,0,0,0,0,0,0,g
g,0,0,0,0,0,0,0,l,0,0,0,0,k,0,g
g,g,0,j,0,0,0,0,0,0,0,0,0,0,g,g
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
WAL e,g,f,d,t,s,1i,1b,1c
EXT 15,8 1 0,8
PAL 0

ROOM 1
e,e,e,e,e,f,f,f,f,g,g,g,g,g,g,g
e,e,e,e,f,f,g,g,g,0,0,0,0,0,0,g
e,e,e,f,f,g,0,0,0,0,0,0,0,0,0,g
e,e,e,f,f,g,0,0,0,0,j,0,0,0,g,g
e,e,f,f,g,0,0,0,0,0,0,0,0,g,f,f
e,f,f,g,0,0,0,0,0,0,0,0,0,g,f,e
e,f,f,g,0,0,h,0,0,0,0,0,0,g,f,f
g,g,g,0,0,0,0,0,0,0,0,0,0,0,g,g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,g,g,g,0,l,0,0,0,0,0,k,0,0,g,g
f,f,f,f,g,0,0,0,0,0,0,0,0,g,g,f
e,e,e,f,g,0,0,0,0,0,0,0,g,g,f,f
e,e,e,e,f,g,0,0,0,0,0,g,g,f,f,e
e,e,e,e,f,g,g,0,0,0,0,g,f,f,e,e
e,e,e,e,e,f,g,0,0,0,g,f,f,e,e,e
e,e,e,e,e,e,f,g,g,g,g,f,f,e,e,e
WAL e,f,g,d,t,s,1i,1b,1c
EXT 15,8 2 0,13
EXT 0,8 0 15,8
PAL 0

ROOM 2
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,e,e,e,e,e,e,e,e,e,e,e,e,e
e,e,e,f,f,f,f,f,f,f,f,f,f,f,f,f
e,f,f,f,g,g,g,g,g,g,g,g,g,g,g,g
e,f,g,g,g,0,0,0,0,0,0,0,0,0,0,g
e,f,g,0,0,0,0,0,0,0,0,l,0,0,0,g
e,f,g,0,0,0,0,i,0,0,0,0,0,0,0,g
e,f,g,0,h,0,0,0,q,q,q,q,q,q,q,q
e,f,g,0,0,0,0,p,n,n,n,n,n,n,n,d
f,f,g,0,0,0,0,p,n,n,n,n,n,n,n,n
f,g,0,0,0,0,0,p,n,n,n,n,n,n,n,d
g,0,0,0,k,0,0,0,o,o,o,o,o,o,o,o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
WAL e,f,g,d,t,s,1i,1b,1c
EXT 15,10 3 0,2
EXT 0,13 1 15,8
PAL 0

ROOM 3
0,0,0,0,0,0,l,0,0,0,0,0,0,g,g,g
d,d,d,d,d,d,d,d,d,d,d,d,0,0,g,g
n,n,n,n,n,n,n,n,n,t,1i,d,0,0,0,g
d,d,d,d,o,o,u,n,n,0,n,d,l,0,0,0
0,0,0,d,0,0,p,n,n,n,n,d,0,0,0,0
0,0,h,d,0,0,p,n,n,n,n,d,0,0,k,0
g,0,0,d,0,0,p,n,n,n,n,d,0,0,0,0
g,0,0,d,0,0,p,n,n,n,n,d,0,0,0,0
g,0,0,d,0,0,p,n,n,n,n,d,0,0,m,0
g,0,0,d,0,0,p,n,n,n,n,d,0,0,0,0
g,k,0,d,0,0,p,n,n,n,s,d,0,0,0,0
g,0,0,d,0,0,p,n,n,n,n,d,d,d,d,d
g,0,0,d,0,0,p,n,n,n,n,n,n,n,n,n
g,0,0,d,r,d,d,d,d,d,d,d,d,d,d,d
g,0,0,d,r,d,0,0,0,0,0,0,0,0,0,0
g,0,0,d,r,d,0,0,0,0,0,0,0,0,0,0
WAL d,t,s,1i,1b,1c
EXT 4,15 4 4,0
EXT 15,12 5 0,4
EXT 0,2 2 15,10
PAL 1

ROOM 4
g,g,0,d,r,d,0,0,0,0,0,k,0,0,g,g
g,0,0,d,r,d,0,0,0,k,0,0,0,0,0,g
0,0,0,d,r,d,0,0,0,0,0,0,0,j,0,0
0,0,0,d,r,d,d,d,d,d,d,d,d,d,d,0
0,0,0,d,r,0,0,0,0,0,0,0,0,0,d,0
0,0,0,d,r,0,0,0,16,0,0,0,0,0,d,0
0,h,0,d,r,0,0,q,15,q,q,q,q,0,d,0
0,0,0,d,r,0,0,v,o,o,o,o,u,0,d,0
0,0,0,d,r,0,0,w,0,0,0,0,p,0,d,0
l,0,0,d,r,0,0,w,0,0,0,t,p,1i,d,g
0,0,0,d,d,d,d,d,d,d,d,d,d,d,d,g
0,0,0,0,0,0,0,0,0,0,0,0,0,g,g,g
0,i,0,0,l,0,0,0,0,0,0,0,0,g,f,f
g,0,0,0,0,0,0,h,0,0,0,0,0,g,f,e
f,g,0,0,0,k,0,0,0,0,0,0,g,g,f,e
e,f,g,0,0,0,0,0,0,0,g,g,g,f,f,e
WAL d,t,s,1i,1b,1c
EXT 4,0 3 4,15
PAL 2

ROOM 5
g,g,0,0,0,0,0,0,0,l,0,0,0,0,g,g
g,0,0,0,d,d,c,b,c,d,c,b,c,d,0,g
0,0,0,d,d,n,n,n,n,n,n,n,n,d,0,0
d,d,d,d,n,n,n,n,n,n,n,n,n,d,0,0
n,n,n,n,n,n,n,10,12,12,11,n,n,d,l,0
d,d,d,n,n,n,n,w,h,0,p,n,n,d,0,0
0,0,d,n,n,n,n,w,0,k,p,n,n,d,0,0
0,0,d,n,n,n,n,x,y,y,z,n,n,d,0,0
0,0,d,n,n,n,n,n,n,n,n,n,n,d,d,d
j,0,d,n,n,n,n,n,n,n,n,n,n,n,n,n
0,0,d,n,n,n,n,n,n,n,n,n,n,d,d,d
0,0,d,d,d,d,d,d,d,d,d,d,n,d,0,0
0,0,0,0,0,0,0,0,0,0,0,d,n,d,i,0
g,0,0,0,k,0,0,l,0,0,0,d,n,d,0,0
f,g,0,0,0,0,0,0,0,0,0,d,n,d,0,g
e,f,g,0,0,0,0,0,0,g,g,d,n,d,0,g
WAL 12,10,w,p,x,z,y,d,c,b,t,s,1i,1b,1c
EXT 12,15 6 8,0
EXT 0,4 3 15,12
EXT 15,9 7 0,3
PAL 5

ROOM 6
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,0,0,0,0,d,n,d,0,0,0,0,0,0
0,0,0,d,d,d,d,d,n,d,d,d,d,d,0,0
0,0,d,14,n,n,n,n,n,n,n,n,n,n,d,0
0,0,d,13,n,n,n,n,n,n,n,n,n,n,d,0
0,0,d,o,o,o,o,o,o,o,o,o,o,o,d,0
0,0,d,0,0,0,0,0,0,0,0,0,0,0,d,0
0,0,d,0,0,0,0,0,0,0,0,0,0,0,d,0
0,0,0,d,d,d,d,d,d,d,d,d,d,d,0,0
WAL d,t,13,14,s,1i,1b,1c
EXT 8,0 5 12,15
PAL 4

ROOM 7
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
0,0,0,0,h,0,0,0,0,0,0,0,i,0,0,0
d,d,d,d,a,b,d,c,b,c,d,d,d,d,d,d
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,d
d,d,d,n,n,n,n,n,n,n,n,n,n,n,n,n
0,0,d,n,n,n,n,n,n,n,n,n,n,n,n,d
k,0,d,r,d,d,d,r,d,d,d,r,d,r,d,d
0,0,d,r,d,h,d,r,d,j,d,r,d,r,d,i
l,0,d,r,d,0,d,r,d,k,d,r,d,r,d,0
d,d,d,r,d,d,d,r,d,d,d,r,d,r,d,d
d,0,0,0,d,0,0,0,d,16,0,0,d,0,0,d
d,q,q,q,d,q,q,q,d,15,q,q,d,q,q,d
d,v,o,u,d,v,o,u,d,v,o,u,d,v,u,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,0,0,0,0,0,0,0,0,0,0,g,g,g,g,g
WAL d,b,a,c,15,16,t,s,1i,1b,1c
EXT 0,3 5 15,9
EXT 15,4 8 0,2
PAL 3

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,0,h
n,n,n,n,n,t,t,t,t,n,n,n,n,1c,d,0
d,d,n,n,n,n,n,n,n,n,n,n,n,1b,d,0
0,d,n,n,n,n,n,n,d,d,d,d,n,s,d,0
0,d,14,n,n,n,n,n,d,0,0,d,n,s,d,0
0,d,13,n,n,n,n,n,d,0,0,d,n,s,d,0
0,d,n,n,n,n,n,n,d,0,0,d,n,s,d,0
0,d,n,1a,19,17,n,n,d,0,0,d,n,n,d,0
0,d,n,18,18,18,n,n,d,0,0,d,n,n,d,0
0,d,n,n,n,n,n,n,d,d,d,d,n,n,d,0
0,d,n,1a,19,17,n,n,n,n,n,n,n,n,d,0
0,d,n,18,18,18,n,n,n,n,n,n,n,n,d,0
0,0,d,d,d,d,d,d,d,d,d,d,d,n,d,0
0,0,0,0,0,0,0,0,0,0,0,0,d,n,d,0
g,0,0,0,0,0,0,0,i,0,0,0,d,n,d,0
WAL d,17,18,1a,13,14,t,s,1i,1b,1c
EXT 0,2 7 15,4
EXT 13,15 9 6,0
PAL 6

ROOM 9
0,0,0,0,0,d,n,d,0,0,0,0,0,g,f,e
0,0,0,0,0,d,n,d,0,0,0,0,0,0,g,f
0,0,h,0,0,d,n,d,d,d,d,d,0,0,0,g
0,0,0,0,0,d,n,0,0,1e,1h,d,0,0,0,0
0,0,0,0,0,d,n,d,0,0,0,d,0,0,0,0
0,0,0,0,0,d,n,d,0,1d,0,d,0,0,0,0
d,d,d,d,d,d,n,d,o,o,o,d,0,0,0,l
d,0,1e,1g,0,0,n,d,d,d,d,d,0,0,0,0
d,0,0,0,0,d,n,d,0,0,0,0,0,0,0,0
d,0,1d,0,0,d,n,d,0,0,0,0,0,0,0,0
d,o,o,o,o,d,n,d,0,0,0,0,0,0,0,0
d,d,d,d,d,d,n,d,0,0,0,0,0,0,0,0
0,0,0,0,0,d,n,d,0,0,0,0,0,i,0,0
0,0,0,0,0,d,n,d,0,0,0,0,0,0,0,0
0,k,0,0,0,d,n,d,0,0,0,0,0,0,0,0
0,0,0,0,0,d,n,d,0,0,0,0,0,0,0,0
WAL d,t,s,1i,1b,1c
EXT 6,0 8 13,15
EXT 6,15 a 7,0
PAL 7

ROOM a
0,0,0,0,0,0,d,n,d,0,0,0,0,0,0,0
0,0,l,0,0,0,d,n,d,0,0,0,0,0,0,0
0,0,0,0,0,0,d,n,d,0,0,0,j,0,0,0
0,0,0,0,0,0,d,n,d,0,0,0,0,0,0,0
0,0,d,b,d,b,d,n,d,d,b,d,b,d,0,0
0,b,14,n,n,n,n,n,n,n,n,n,n,n,d,0
0,d,13,n,n,n,n,n,n,n,n,n,n,n,b,0
0,b,n,n,n,10,12,12,12,12,11,n,n,1c,d,0
d,d,n,n,n,w,m,0,0,h,p,n,n,1b,b,0
n,n,n,n,n,x,y,y,y,y,z,n,n,n,d,d
d,d,n,n,n,n,n,n,n,n,n,n,n,n,n,n
0,b,n,n,n,n,n,n,n,n,n,n,n,n,d,d
0,0,d,b,d,b,d,b,d,b,d,b,d,b,d,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,i,0,0,0,0,0,0,0,0,0,0,h,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL d,11,z,y,w,10,12,1c,1b,13,14,b,t,s,1i
EXT 7,0 9 6,15
EXT 15,10 b 0,8
EXT 0,9 c 15,11
PAL 8

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,d,d,d,d,0,0,0,0,0
0,0,0,0,0,0,0,d,n,t,t,d,0,0,0,0
0,0,0,0,0,0,0,d,n,n,n,d,0,0,0,0
0,0,0,0,0,0,0,d,16,n,n,n,d,0,0,0
0,0,0,0,0,0,0,d,15,n,n,n,d,0,0,0
0,0,0,0,0,0,0,d,n,n,n,n,1i,d,0,0
d,d,d,d,d,d,d,d,n,n,n,n,n,n,d,0
n,n,n,n,n,n,n,n,n,n,n,n,n,n,d,0
d,d,d,d,d,d,d,d,n,n,n,n,n,n,d,0
0,0,0,0,0,0,0,d,n,n,n,n,n,d,0,0
0,0,0,0,0,0,0,d,n,n,n,n,d,0,0,0
0,0,0,0,0,0,0,d,n,n,n,n,d,0,0,0
0,0,0,0,0,0,0,d,n,n,n,d,0,0,0,0
0,0,0,0,0,0,0,d,n,n,1i,d,0,0,0,0
0,0,0,0,0,0,0,d,d,d,d,0,0,0,0,0
WAL d,1i,t,s,1b,1c
EXT 0,8 a 15,10
PAL 9

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,i,0,0
0,0,h,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,d,b,b,b,d,b,b,d,b,b,b,d,0,0
0,d,t,t,t,n,n,n,n,n,n,n,n,1i,d,0
k,d,n,n,n,n,n,n,n,n,n,n,n,n,d,0
0,o,o,o,u,n,n,n,n,n,n,n,n,n,d,0
1a,19,19,17,p,n,n,n,n,n,n,n,n,s,d,0
18,18,18,18,p,n,n,n,n,n,n,n,n,s,d,0
0,y,y,y,z,n,n,n,n,n,n,n,n,n,d,0
0,d,1c,n,n,n,n,n,n,n,n,n,n,n,d,0
0,d,1b,n,n,n,n,n,n,n,n,n,n,n,d,d
0,d,n,n,n,n,n,n,n,n,n,n,n,n,n,n
0,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,h,g,i,0,0,0,0,0,0,0,0
WAL d,b,s,t,1i,1b,1c
EXT 15,11 a 0,9
PAL a

TIL 10
00000111
00001000
00010000
00100000
01000000
10000000
10000000
10000000
NAME undefined

TIL 11
11100000
00010000
00001000
00000100
00000010
00000001
00000001
00000001
NAME undefined

TIL 12
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME undefined

TIL 13
11000001
11010001
11000001
11000001
11111111
11111111
11111111
11111111
>
11000001
11000001
11000101
11000001
11111111
11111111
11111111
11111111
NAME undefined

TIL 14
00011111
00110001
01100101
11000001
11001001
11000001
11000001
11000001
>
00011111
00110001
01100001
11000001
11000001
11010001
11000101
11000001
NAME undefined

TIL 15
10000001
10000001
10000001
10000001
11000011
01111110
01000010
11111111
NAME undefined

TIL 16
00000000
00111100
01000010
10000001
10000001
10000001
10000001
10000001
NAME undefined

TIL 17
00000000
11111000
00111100
01111010
01111001
11111111
11111111
11111110
NAME undefined

TIL 18
00000000
01111100
00000000
00111000
00000000
00000000
00000000
00000000
>
00000000
00000000
01111100
00000000
00111000
00000000
00000000
00000000
NAME undefined

TIL 19
00000000
00000001
11111111
11011111
11011111
11110000
11111111
11111111
NAME undefined

TIL a
11111111
10000001
10100101
10110101
10101101
10100101
10000001
11111111
NAME undefined

TIL b
11111111
10000001
10011001
10100101
10100101
10011001
10000001
11111111
NAME undefined

TIL c
11111111
10000001
10100001
10100001
10100001
10111101
10000001
11111111
NAME undefined

TIL d
11111111
10000001
10000001
10000001
10000001
10000001
10000001
11111111
NAME undefined

TIL e
10010001
01000000
00101010
10010000
00101010
10000010
00010100
01000010
NAME undefined

TIL f
10010010
00000000
01010100
00100001
00001000
10100000
00000100
00100000
NAME undefined

TIL g
00000000
00010000
00000100
01000000
00000010
00000000
01001000
00000000
NAME undefined

TIL h
00000000
00000010
00000000
00000000
00100000
00000000
00000000
00000000
NAME undefined

TIL i
00000000
00000000
00000000
00010000
00000000
00000000
00000100
00000000
NAME undefined

TIL j
00000000
01000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME undefined

TIL k
00000000
00000000
00000000
00000000
00000000
00001000
00000000
00000000
NAME undefined

TIL l
00000000
00000100
00000000
00000000
00000000
00000000
00000000
00000000
NAME undefined

TIL m
00000000
00000000
00000000
00000000
00000000
00000000
00100000
00000000
NAME undefined

TIL n
00000000
00000000
00100100
00000000
00000000
00100100
00000000
00000000
NAME undefined

TIL o
11111111
01101010
01001000
00001000
00000000
00000000
00000000
00000000
NAME undefined

TIL p
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME undefined

TIL q
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME undefined

TIL r
01111110
00000000
00000000
00000000
01111110
00000000
00000000
00000000
NAME undefined

TIL s
00011111
00111111
01001111
10000111
10001111
11011111
11111111
11111111
NAME undefined

TIL t
01111110
11000011
11000011
11000011
11000011
11111111
11111111
11111111
NAME undefined

TIL u
11111111
01101001
01001001
00001001
00000001
00000001
00000001
00000001
NAME undefined

TIL v
11111111
10001010
10001000
10001000
10000000
10000000
10000000
10000000
NAME undefined

TIL w
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME undefined

TIL x
10000000
11000000
11100000
11110000
01111111
00111111
00011111
00001111
NAME undefined

TIL y
00000000
00000000
00000000
00000000
11111111
11111111
11111111
11111111
NAME undefined

TIL z
00000001
00000011
00000111
00001111
11111110
11111100
11111000
11110000
NAME undefined

TIL 1a
00000000
11111110
10111111
10111111
11111111
11111111
01111111
00111111
NAME undefined

TIL 1b
11110101
10110111
11110000
00010000
01111110
01100110
00100100
00111100
NAME undefined

TIL 1c
00111000
00101000
00111000
00010111
11110101
10111111
11110100
01010111
NAME undefined

TIL 1d
00000000
00000000
00000000
01111110
00100100
00100100
00100100
00100100
NAME undefined

TIL 1e
01111111
10000000
10110111
10100101
10110101
10110111
10000000
01111111
NAME undefined

TIL 1f
11111111
00000000
01001000
01101000
00100010
00001010
00000000
11111111
NAME undefined

TIL 1g
11111110
00000001
01000101
01001101
01010101
01000101
00000001
11111110
NAME undefined

TIL 1h
11111110
00000001
00100101
01011001
01100101
01000101
00000001
11111110
NAME undefined

TIL 1i
00011000
00100100
01111110
01000010
01010010
01010010
01000010
00111100
NAME undefined

SPR 10
00111100
01111110
00100100
01111110
01111110
00100100
00100100
00100100
>
00000000
00111100
01111110
00100100
01111110
01111110
00100100
01100110
DLG SPR_r
POS c 7,5

SPR 11
00111000
00101111
00111101
00000100
00010101
00000100
00001010
00001010
>
00111000
00101111
00111101
00000100
00000100
00010101
00001010
00001010
DLG SPR_u
POS c 8,8

SPR 12
11111100
10110100
11110100
00000100
00111100
00100000
00100000
00111110
>
00000000
11111100
10110100
11110100
00000100
00111100
00100000
00111111
DLG SPR_s
POS c 12,5

SPR 14
00000000
01111000
00101000
00111000
00011110
00111000
00010100
00110110
>
00000000
01111000
00101000
00111010
00011100
01011000
00010101
00110010
DLG SPR_t
POS 2 14,9

SPR A
00000000
01000000
01100000
01111001
11101111
11111111
01111100
00111000
>
00000000
01000000
01100000
01111001
11101111
11111000
01111111
00111000
POS 0 3,4

SPR a
00000000
00010000
00010000
00111000
11111110
00111000
00010000
00010000
>
00000000
00000000
00000000
00010000
00111000
00010000
00000000
00000000
DLG SPR_0
POS 0 9,9

SPR b
00000000
00000100
00011000
00111100
00111100
01011000
10000000
11000000
>
00000000
00000011
00011001
00111110
00111100
00011000
00100000
00000000
DLG SPR_1
POS 1 11,5

SPR c
00000000
00111000
01111100
11000110
11111110
11000110
01111100
00111000
>
00000000
00111000
01111100
11111110
11000110
11111110
01111100
00111000
DLG SPR_2
POS 1 8,10

SPR d
00000000
00000000
00111100
00101100
00111100
00111100
00111100
00000000
>
00000000
00000000
00111100
00110100
00111100
00111100
00111100
00000000
DLG SPR_3
POS 2 5,9

SPR e
00000000
00000000
01111110
01001110
01111110
01111110
01111110
00000000
>
00000000
00000000
01111110
01100110
01111110
01111110
01111110
00000000
DLG SPR_4
POS 3 9,3

SPR f
00000000
01111100
01110100
01111100
00010000
00010000
00111000
00101000
>
00000000
01111100
01101100
01111100
00010000
00010000
00111000
00101000
DLG SPR_5
POS 3 9,10

SPR g
00000000
00111100
01101110
01111110
00111100
00000100
00011100
00100000
>
00000000
00111100
01110110
01111110
00111100
00000100
00011100
00100000
DLG SPR_6
POS 4 11,6

SPR h
00000000
00011100
00010100
00011100
00011100
00001000
00111110
00100010
>
00011100
00010100
00011100
00011100
00001000
00001000
00111110
00100010
DLG SPR_7
POS 4 9,9

SPR i
00000000
00111100
01111110
01010110
01111110
01111110
01111110
01010100
>
00111100
01111110
01010110
01111110
01111110
01111110
00101010
00000000
DLG SPR_8
POS 4 6,5

SPR j
00000000
01111110
01110110
01011010
00010000
00010000
00010000
00111000
>
00000000
01111110
01101110
01011010
00010000
00010000
00010000
00111000
DLG SPR_9
POS 5 6,3

SPR k
00000000
01110010
01111110
01111110
01110010
01111110
01111110
00000000
>
00000000
01110010
01111110
01111110
01111110
01110010
01111110
00000000
DLG SPR_a
POS 5 11,6

SPR l
00000000
00111100
00101100
00111100
00111100
00001100
00111100
00000100
>
00000000
00111100
00101100
00111100
00111100
00111100
00000100
00000100
DLG SPR_b
POS 5 6,9

SPR m
00000000
01111100
01101100
00111100
01111100
00011100
00001100
01111100
>
00000000
01111100
01111100
00101100
01111100
00011100
00001100
01111100
DLG SPR_c
POS 6 5,11

SPR n
01111100
01111100
01011100
01111100
00001100
00011100
00111100
00000000
>
01111100
01111100
01110100
01111100
00001100
00011100
00111100
00000000
DLG SPR_d
POS 6 5,10

SPR o
00000000
01111110
01011110
01111110
00001110
00111100
00111100
00110100
>
00000000
01111110
01111010
01111110
00001110
00111100
00111100
00110100
DLG SPR_e
POS 7 2,11

SPR p
00011100
00010100
01111111
01011101
01001001
01001001
00001000
00001000
>
00000000
00011100
00010100
01111111
01011101
01001001
01001001
00001000
DLG SPR_f
POS 7 5,11

SPR q
00111100
00100100
00111100
00111100
00100100
00100100
00111100
00011000
>
00111100
00100100
00111100
00111100
00111100
00100100
00111100
00011000
DLG SPR_g
POS 7 11,11

SPR r
00000000
00000000
00111000
11111110
01000100
01111100
01010100
01010100
>
00000000
00111000
11111110
01000100
01111100
01010100
01010100
00000000
DLG SPR_h
POS 7 14,10

SPR s
00010000
10111001
01111110
00111000
00101000
00111000
01010100
00010000
>
00010000
10111001
01111110
00111000
00110000
00111000
01010100
00010000
DLG SPR_i
POS 8 6,11

SPR t
00011110
00011100
00011110
00111000
01110000
01100100
00110000
01001000
>
00011110
00011010
00011110
00111000
01110010
01100000
00110000
01001000
DLG SPR_j
POS 8 6,8

SPR u
00111110
00101010
00111110
01111110
11111111
11111111
01111110
01000010
>
00000000
00111110
00101010
01111110
11111111
11111111
01111110
01000010
DLG SPR_k
POS 9 1,9

SPR v
00111000
00101000
00111100
00000100
00001110
00001110
00000100
00001100
>
01110000
01010000
01111100
00000100
00011111
00000100
00000100
00001100
DLG SPR_l
POS 9 10,5

SPR w
00000000
00100000
00111100
00110110
00111110
00111110
01100100
01000110
>
00100000
00111100
00110110
00111110
00111110
00100100
01100100
01000110
DLG SPR_m
POS a 4,8

SPR x
00001000
00011100
00101010
01111111
00010100
00110110
00010100
00110110
>
00010000
00111000
01010100
11111110
00010100
00110110
00010100
00110110
DLG SPR_o
POS a 13,6

SPR y
00000000
00111000
01111100
01000100
01111100
01111100
00111000
00000000
>
00111000
01111100
01000100
01111100
01111100
00111000
00000000
00000000
DLG SPR_p
POS b 12,8

SPR z
00000000
00011100
00010100
00011100
00111101
00010100
00111110
00100010
>
00000000
00011100
00010100
00011100
01011110
00010100
00111110
00100010
DLG SPR_q
POS c 5,6

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
DLG ITM_0

DLG SPR_0
hey you're a cool wolf head in space. pretty rad. 

DLG ITM_0
You got a cup of tea! Nice.

DLG SPR_1
up here you don't have to care about anything and that's good.

DLG SPR_2
it's easier up here, where you're very cool & can fly

DLG SPR_3
TAke a look y'all: every bit of introspection is painful

DLG SPR_4
you create elaborate fantasies to escape to, & fear their reveal.

DLG SPR_5
you are afraid of yourself. or anything else that could pull you apart.

DLG SPR_6
"""
in times of seclusion you seek respite in places that you can control.
"""

DLG SPR_7
when you were young, there wasn't any fear in meeting new people. now you stay in your room for days on end. maybe this is recharging.

DLG SPR_8
hey i'm gerold welcome to my sweet pad!!! we're all friends here haha

DLG SPR_9
this is where we all go to chill out away from things.

DLG SPR_a
sometimes you can look out the window and wish you could just go wherever you want to. luckily you can because you're a flying wolf head, as i understand.

DLG SPR_b
haha! haha! haha!! you can only write about one thing and you do it alllllll the tiiiiiiiiiiiime. it's you.

DLG SPR_c
i love this show. it's got issues but ultimately in times like this i'll take what i can get.

DLG SPR_d
sometimes i wonder if i spend too much time caring for myself and not doing other shit.

DLG SPR_e
i wake up in the morning and i'm sad!!!! 

DLG SPR_f
"""
{cycle
  - i'm well-adjusted and i sleep fine and every day my friends are jealous of me and my accomplishments especially gerald
    
    gerald thinks i am rude
}
"""

DLG SPR_g
"""
it is hard to know who i am some mornings because everything feels so difficult. i don't know how to apologize for this. i simply continue.
"""

DLG SPR_h
"""
these apartments suck but i don't have the money to leave. i've lived here my whole life.
"""

DLG SPR_i
don't touch my car, man. i paid a lot for it.

DLG SPR_j
"""
this is {wvy}CAR ALLEY{wvy}. we love cars here. i love my car because i love to work on it and to show it off!
"""

DLG SPR_k
"""
{cycle
  - hewwo. this is my stowe. store. wegional diawect. you'we wewcome any time. 
    
    one time i was wowking wate at night and i saw a shooting staw. it was the bwightest thing in the univewse at that moment. sometimes i wondew if i will be hewe fowevew.
}
"""

DLG SPR_l
that's my brother's store over there. my store only sells used newspapers.

DLG SPR_m
looking out into space makes me feel very small. i've always wanted to disappear.

DLG null
"""
{sequence
  - i'm the conductor. every day i take a load of tourists in and out.
  - it's not a bad job, really. we aren't allowed to have a union, though.
  - i had a friend who never came back from a run to Turgutz.
  - just a routine thing, right. like. one moment he was out there, next he was gone.
  - i think i was in love with him. i think that now.
  - sorry, i should get back to work.
}
"""

DLG SPR_n
"""
test test.
"""

DLG SPR_o
"""
{sequence
  - my mom's being evicted from her apartment. 
    when i was younger and thought about space it seemed endless and hopeful.
    now i'm here and it's just endless.
    
    sorry. i need a moment.
}
"""

DLG SPR_p
"""
being a robot isn't so bad. you can turn off your emotions whenever you like. 
on purpose, even.
"""

DLG SPR_q
i just got here from Zynab. this is like Zynab but different.

DLG SPR_r
i'm ready to go home. i miss my friends and my nintendo

DLG SPR_s
"""
{sequence
  - i'm hiding over here because i'm not ready to go home yet.
}
"""

DLG SPR_t
welcome to space station centauri-12. come on in. 

DLG SPR_u
"""
i'm the conductor. every day i take a load of tourists in and out.
it's not a bad job, really. we aren't allowed to have a union, though.
i had a friend who never came back from a run to Turgutz.
just a routine thing, right. like. one moment he was out there, next he was gone.
i think i was in love with him. i think that now.
sorry, i should get back to work.

"""

