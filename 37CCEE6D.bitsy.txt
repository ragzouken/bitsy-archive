garden

# BITSY VERSION 3.0

! ROOM_FORMAT 1

PAL 0
88,118,18
213,239,152
255,255,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL a

TIL a
00000000
00000000
01000100
00100010
00100010
10101010
10101010
10101010

SPR A
00000000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 0 5,4

SPR a
00001000
00010100
00101010
00010100
00001000
00001100
00011000
00001000
POS 0 8,12

SPR b
00000000
00000000
00000000
00000000
10010001
01001010
00101010
00000000
POS 0 12,10

SPR c
00000000
00000000
00000000
00000000
00010000
00101000
00010000
00010000
POS 0 8,8

SPR d
00000000
00000000
00000000
00000000
10010001
01001010
00101010
00000000
POS 0 3,11

SPR e
00001000
00010100
00101010
00010100
00001000
00001100
00011000
00001000
POS 0 4,6

SPR f
00000000
00000000
00000000
00000000
00010000
00101000
00010000
00010000
POS 0 11,5

SPR g
00000000
00000000
00000000
00000000
10010001
01001010
00101010
00000000
POS 0 9,4

SPR h
00000000
00000000
00000000
00000000
00010000
00101000
00010000
00010000
POS 0 3,3

DLG a
i'm gay

DLG b
are you enjoying yourself? 

DLG c
hi. do you want to be friends? i like laying in the grass, mostly because i can't move. 

DLG d
i can't stop playing MMOs from my grassy youth. 

DLG e
you don't have any leaves. i could give you one if you like. 

DLG f
i know i'm smol but i'm really passionate about punk music. do you think if i get a leather jacket i'll look more intimidating?

DLG g
would you maybe want to visit my blog on tumblr? my url is pastel-grass-patch. thanks <3

DLG h
oh, hi. it's been a while since i've talked to anyone, sorry. thank you for dropping by.

