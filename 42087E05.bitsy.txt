You are on a little walk with your friend {buckthorn = 0}

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
249,235,196
167,207,93
169,117,85

ROOM 1
a,a,a,a,a,a,a,0,0,a,c,c,a,a,b,a
a,d,b,b,a,a,a,0,0,0,0,0,d,a,a,b
a,a,a,d,a,0,0,0,0,0,0,0,0,a,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,a,a,a
a,0,0,0,0,d,a,a,a,a,0,0,0,a,a,d
0,0,c,c,a,a,b,b,a,a,0,0,0,a,a,0
0,0,c,a,a,b,b,a,a,a,b,a,0,0,0,0
0,0,c,a,d,a,a,a,a,a,b,b,d,0,0,i
0,0,0,a,a,a,a,a,a,d,b,d,a,f,a,a
0,0,0,0,a,a,a,a,a,b,b,d,0,0,f,d
0,0,0,0,0,a,a,a,a,b,a,0,f,f,0,f
0,0,0,0,0,a,a,a,a,a,a,0,0,f,a,a
0,a,0,0,0,0,a,a,a,0,0,0,a,0,a,d
a,a,a,0,0,0,0,0,0,0,0,0,0,c,a,a
a,b,a,a,0,0,0,0,0,0,0,c,c,d,a,b
d,b,b,a,a,a,a,0,0,0,0,c,a,a,b,b
NAME start
EXT 15,7 2 0,7
PAL 0

ROOM 2
a,a,d,b,b,d,b,a,a,0,d,0,c,c,a,a
a,a,c,b,c,a,b,a,a,0,0,a,c,c,a,a
a,a,a,a,a,d,a,0,0,0,a,a,a,a,a,b
a,0,0,a,a,a,a,0,0,0,0,a,a,0,d,0
0,0,a,a,a,0,0,0,0,a,a,a,a,0,0,0
0,0,a,0,0,0,0,0,0,a,a,a,a,i,i,i
0,0,0,0,0,k,0,0,a,a,a,i,i,i,0,0
i,i,i,i,i,i,0,0,0,0,i,i,k,0,0,c
0,0,0,0,0,i,i,i,i,i,i,0,0,0,c,c
j,0,0,0,0,0,j,0,0,0,0,0,0,0,0,0
0,a,c,c,0,0,0,a,a,0,0,0,0,a,a,0
a,d,c,c,a,0,0,a,a,a,0,a,a,b,a,0
a,a,a,a,a,0,0,0,0,a,a,b,b,b,a,a
a,a,a,a,0,0,0,0,a,a,c,c,d,b,a,a
a,b,b,a,a,0,0,a,a,c,c,c,a,a,b,b
b,b,b,0,b,0,0,0,a,a,a,a,a,b,b,b
NAME wild plum
ITM 6 7,14
EXT 15,5 3 0,5
PAL 0

ROOM 3
c,c,a,a,a,0,0,0,a,a,a,a,0,a,a,0
c,c,d,a,0,0,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,0,a,a,a,a,a,a,b,b,a,a
a,a,0,0,0,0,0,a,a,0,a,a,d,b,b,a
0,0,0,0,0,0,0,0,0,0,0,a,a,a,a,a
i,i,i,i,i,i,i,0,0,0,0,0,0,a,a,a
0,0,0,0,0,i,i,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,i,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,i,i,i,i,i,i,i,i,0,0
0,c,c,0,0,0,0,0,0,0,0,0,0,i,0,0
a,a,a,a,a,a,0,0,0,0,k,0,0,i,i,0
a,b,b,a,a,a,a,a,0,j,0,0,0,i,i,i
a,b,d,b,b,a,a,a,0,0,k,0,j,0,0,i
a,c,b,d,b,a,a,a,c,a,j,a,0,k,b,b
a,a,b,c,c,c,a,c,d,a,a,a,a,b,d,a
a,a,a,a,a,c,a,a,a,a,a,a,a,0,a,a
NAME sea buckthorn
ITM 1 5,3
EXT 15,11 4 0,11
PAL 0

ROOM 4
b,b,b,b,d,b,a,a,0,0,0,0,a,a,a,a
b,c,d,b,b,b,a,a,0,0,0,a,a,d,c,a
c,c,b,b,a,a,a,0,0,0,0,a,a,c,c,c
a,c,b,a,a,a,a,0,a,a,0,0,a,a,a,a
a,a,a,a,d,a,0,a,a,a,a,0,0,0,0,0
a,a,a,a,a,0,0,a,a,a,0,0,0,0,0,0
a,a,a,0,0,0,0,0,0,0,0,0,0,0,0,i
0,0,0,0,0,0,0,i,i,i,i,i,i,i,i,i
0,0,0,0,0,0,i,i,i,0,0,0,0,0,0,0
0,0,0,0,0,i,i,0,0,0,0,0,0,0,c,0
0,0,0,0,0,i,0,0,0,0,0,0,0,c,c,0
i,i,i,i,i,i,0,0,0,0,0,0,d,a,a,a
i,0,0,0,0,0,a,a,0,0,0,a,a,a,a,a
0,a,a,0,0,a,a,a,0,0,a,a,a,b,d,a
a,c,c,a,a,a,a,0,0,a,a,a,b,d,b,b
a,a,a,a,a,a,0,0,0,a,a,a,b,b,a,0
NAME fungi
ITM 0 9,4
EXT 15,6 5 0,6
PAL 0

ROOM 5
a,b,b,b,a,c,c,a,c,c,a,c,c,b,b,0
b,b,d,b,c,c,d,a,a,a,a,a,a,b,b,a
a,b,c,c,c,a,c,0,0,0,c,c,c,a,d,a
a,a,a,d,0,0,0,0,0,0,0,0,0,a,a,a
a,0,0,0,0,0,0,0,0,0,a,a,0,0,a,a
0,0,0,0,0,0,0,0,0,a,b,b,a,0,0,a
i,i,i,i,i,i,0,0,0,d,b,c,c,a,0,c
0,0,0,0,0,i,i,0,0,a,b,b,a,0,0,0
0,0,0,0,0,0,i,i,0,0,0,a,a,0,0,0
0,0,0,0,0,0,0,i,i,0,0,0,0,0,0,0
0,0,0,0,0,0,0,i,i,i,i,i,i,i,i,i
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,c,0,0,0,0,0,0,0,0,0,0,a,a,a
b,b,a,d,a,a,0,0,0,0,0,0,a,b,b,c
b,d,b,c,c,a,a,a,a,0,0,0,d,c,b,a
a,b,b,b,c,c,a,a,a,a,0,0,c,a,c,a
NAME fungi2
ITM 7 3,1
ITM 8 5,14
ITM 4 13,3
END undefined 15,10
PAL 0

TIL a
10000000
01001000
00000100
00000000
00000000
01000010
00100001
00000000

TIL b
00000000
00000000
01010000
01110101
00100111
00000010
00000010
00000000
>
00000000
01010000
01110000
00100000
00100101
00000111
00000010
00000000

TIL c
00000000
00000111
00000010
11100000
01000000
01000000
00000000
00000000
>
00000111
00000010
00000010
00000000
11100000
01000000
00000000
00000000

TIL d
00000000
00000000
00000000
00000000
01101100
01111100
00010000
00010000
>
00000000
00000000
00000000
00000000
00000000
01101100
01111100
00010000

TIL f
00000000
00000001
00000101
00100010
01000010
01000001
00100001
00100010
WAL true

TIL g
00000000
00000000
00000000
00000000
00000110
00000110
00000000
00000000

TIL h
00000000
00000110
00000110
00000000
00000000
01100000
01100000
00000000

TIL i
00000000
00000100
01000000
00000000
00001000
00000000
00000000
00000000

TIL j
00000000
00000100
01000010
01000010
00100100
00110100
00001000
00001000

TIL k
00000000
00000010
00000100
01000100
01001000
00101000
00010000
00010000

SPR A
00000000
01100110
01100110
01111110
01111110
01011010
01111110
00100100
>
00000000
00000000
01100110
01100110
01111110
01111110
01011010
01111110
POS 1 8,8

SPR a
00000000
00000000
00000000
01100110
01111110
01111110
01011010
01111110
>
00000000
00000000
01100110
01111110
01111110
01011010
01111110
00100100
NAME friend1
DLG SPR_0
POS 1 6,7

SPR d
00000000
10100001
01000101
01000010
00101010
10100100
01000100
01000010
NAME blackberries
DLG SPR_1
POS 1 13,9

SPR e
00000000
00000000
01111100
01000010
01111100
00010000
00010000
00000000
NAME sign 1
DLG SPR_2
POS 1 15,6

SPR f
00000000
00000000
00000000
01100110
01111110
01111110
01011010
01111110
>
00000000
00000000
01100110
01111110
01111110
01011010
01111110
00100100
NAME friend2
DLG SPR_3
POS 2 9,5

SPR g
00000001
00000101
01000010
01000100
00100100
00011000
00001000
00001000
NAME wild plum
DLG SPR_4
POS 2 11,5

SPR h
00000000
00000000
01111100
01000010
01111100
00010000
00010000
00000000
NAME trail
POS 3 15,10

SPR i
00000000
00000101
01000010
11000010
00100100
00100100
00011000
00010000
NAME sea buckthorn
DLG SPR_6
POS 3 9,6

SPR j
00000000
00000000
00000000
01100110
01111110
01111110
01011010
01111110
>
00000000
00000000
01100110
01111110
01111110
01011010
01111110
00100100
DLG SPR_9
POS 3 8,7

SPR k
00000001
00000101
01000010
01000100
00100100
00011000
00001000
00001000
NAME blackberries2
DLG SPR_5
POS 3 11,11

SPR l
00000000
00000000
00000000
01100110
01111110
01111110
01011010
01111110
>
00000000
00000000
01100110
01111110
01111110
01011010
01111110
00100100
DLG SPR_a
POS 4 4,8

SPR m
00000000
00000000
00000000
00111000
00111000
00010000
00010000
00000000
>
00000000
00000000
00000000
00000000
00111000
00111000
00010000
00000000
NAME amanita
DLG SPR_7
POS 4 7,13

SPR n
00000000
00000000
00000000
01100110
01111110
01111110
01011010
01111110
>
00000000
00000000
01100110
01111110
01111110
01011010
01111110
00100100
DLG SPR_b
POS 5 4,3

SPR o
00000000
00000000
01111100
01000010
01111100
00010000
00010000
00000000
DLG SPR_8
POS 5 15,9

ITM 0
00000000
00000111
00000111
11100010
11100000
01000000
01000000
00000000
>
00000111
00000111
00000010
00000010
11100000
11100000
01000000
00000000
NAME bovist
DLG ITM_3

ITM 1
00000000
00000000
00000010
00100110
00110010
00100011
01100000
00100000
>
00000000
00000010
00000110
00000010
00100011
00110010
00100000
01100000
NAME oregano
DLG ITM_2

ITM 2
00000000
00000000
00000000
00000000
00001000
00011100
00011100
00000000
NAME apple

ITM 3
00000000
00000000
00000000
00001000
00001000
00011100
00011100
00000000
NAME pear

ITM 4
00000000
00000010
00100111
01110111
01110010
00100000
00100000
00000000
>
00000010
00000111
00000111
00100010
01110010
01110000
00100000
00000000
NAME chanterelle
DLG ITM_6

ITM 5
00000000
00000000
01100000
01100000
00000110
00000110
00000000
00000000
NAME walnuts
DLG ITM_0

ITM 6
00000000
00000000
00000000
00001000
00011100
00001000
00000000
00000000
NAME 4leafclover
DLG ITM_1

ITM 7
00000000
00000111
00000111
11100010
11100000
01000000
01000000
00000000
>
00000111
00000111
00000010
00000010
11100000
11100000
01000000
00000000
NAME bovist2
DLG ITM_4

ITM 8
00000000
00000111
00000111
11100010
11100000
01000000
01000000
00000000
>
00000111
00000111
00000010
00000010
11100000
11100000
01000000
00000000
DLG ITM_5

DLG SPR_0
> {clr2}{wvy}Hey!{wvy} I'm glad we found the time to collect some wild plants together. Maybe we find some chanterelles today, it has rained a lot the last days. They are my {wvy}favorite mushroom{wvy} :){clr2}

DLG SPR_1
> Oh there are still some last blackberries left!! (soundeffect "berries1"){rbw}{wvy}Delicious{wvy}.{rbw}

DLG ITM_0
Oh some walnuts!

DLG SPR_2
Old forest

DLG SPR_3
"""
{sequence
  - > {clr2}Look that's a wild plum tree. {wvy}Trust me{wvy} the fruits are very good:D{clr2}
}
"""

DLG SPR_4
"""
{sequence
  - > (soundeffect "berries3"){shk}uuuuu{shk}
  - > {clr2}{wvy}Hehe{wvy} feels funny doesn't it?{clr2}
  - {clr2}Don't worry it only makes your mouth feel dry, otherwise it's a nice fruit :) {shk}Haha.{shk}{clr2}
}
"""

DLG ITM_1
> I found a lucky 4 leaf clover! :)

DLG SPR_5
> {rbw}{wvy}YUM!{wvy}{rbw} (soundeffect "berries2")Some more blackberries.

DLG ITM_2
"""
> I know that one, it's wild oregano! (soundeffect "herb")
> {clr2}Oh, what is it for?{clr2}
> It's a herb that's great for cooking, and I think it goes well with tomatoes. And the bees love it too!
"""

DLG SPR_6
"""
{
  - buckthorn == 1 ?
    > Yes I'm pretty sure :)))
    > {clr2}Oh yess. You know how to collect it?{clr2}
    > Yes kinda, we have some at my parent's place. Here you can use my pocketknife. It's best if you cut off the branches as a whole. If later frozen the berries are waaay easier to remove. (soundeffect "branch")
}
"""

DLG ITM_3
"""
> I found one!(soundeffect "berries1") > {clr2}I'm pretty sure that's an edible bovist! If we find some more we can make a {rbw}{wvy}tasty{wvy}{rbw} stir fry with them.{clr2}
> {wvy}YESS.{wvy} That sounds great :)
"""

DLG SPR_7
"""
{sequence
  - > Here's an amanita :)
  - > {clr2}It's beautiful! Hm, maybe that means there are some edible mushrooms here as well.{clr2}
}
"""

DLG ITM_4
> {clr2}Here is another bovist!(soundeffect "berries2") :) Seems like it might be wet enough for fungi indeed.{clr2}

DLG ITM_5
> I think that's another bovist here! :)(soundeffect "berries3")

DLG ITM_6
"""
> {wvy}A CHANTERELLE!{wvy}
> {clr2}{wvy}wHAAT{wvy} That's {rbw}AMAZING{rbw} :DD And so many in one place. (soundeffect "berries1")You have really good eyes to spot them hidden in the moss like this :){clr2}
"""

DLG SPR_8
Train home

DLG SPR_9
"""
{
  - buckthorn == 0 ?
> {clr2}Is that {wvy}sea buckthorn{wvy}?? I wanted to find this for a loong time.{clr2} {buckthorn = 1}
}
"""

DLG SPR_a
> {clr2}Lots of moss here. Maybe we find some mushrooms. Keep your eyes peeled :){clr2}

DLG SPR_b
> {clr2}We found so many wild fruits today, I'm so happy :){clr2}

END 0


END undefined
Having gathered lots of wild plants you head back home. It's time to make mushroom stir fry, some jam and enjoy the evening :) {wvy}{rbw}Thanks for playing{rbw}{wvy}

VAR a
42