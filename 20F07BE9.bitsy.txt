The Bazaar

# BITSY VERSION 4.2

! ROOM_FORMAT 1

PAL 0
220,72,10
255,168,125
255,255,255

PAL 1
90,152,118
171,209,183
255,255,255

PAL 2
124,58,108
223,181,219
255,255,255

PAL 3
108,49,50
215,188,151
255,255,255

PAL 4
5,80,116
142,162,238
255,255,255

ROOM 0
p,o,o,o,o,o,o,o,o,q,0,p,o,o,o,q
l,a,n,n,n,n,n,n,n,m,0,l,n,n,a,m
l,m,f,0,0,0,0,0,h,t,0,t,f,h,l,m
l,m,0,0,0,0,0,0,0,0,0,0,0,0,l,m
l,m,0,0,i,c,c,c,c,c,c,g,0,0,l,m
l,m,g,0,j,i,d,d,d,d,g,e,0,0,l,m
s,n,v,0,j,e,0,0,0,0,j,e,0,i,l,m
0,0,0,0,j,e,0,0,0,0,j,e,0,w,n,r
p,o,v,0,j,e,0,0,0,0,j,e,0,0,0,0
l,m,f,0,j,e,0,0,0,0,j,e,0,w,o,q
l,m,0,0,j,h,c,c,c,c,f,e,0,h,l,m
l,m,0,0,h,d,d,d,d,d,d,f,0,0,l,m
l,m,0,0,0,0,0,0,0,0,0,0,0,0,l,m
l,m,g,i,u,0,u,g,0,0,0,0,0,i,l,m
l,a,o,o,m,0,l,o,o,o,o,o,o,o,a,m
s,n,n,n,r,0,s,n,n,n,n,n,n,n,n,r
NAME Middle Room
WAL a,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
ITM 5 5,13
EXT 0,7 2 14,7
EXT 15,8 1 1,8
EXT 10,0 4 10,14
EXT 5,15 3 4,1
PAL 0

ROOM 1
p,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q
l,a,n,n,n,n,a,n,n,n,n,n,n,n,a,m
l,m,e,0,0,0,x,e,0,0,0,0,0,h,l,m
l,m,e,0,0,0,x,e,0,0,0,0,0,0,l,m
l,m,e,0,0,0,x,e,0,0,0,0,0,0,l,m
l,m,e,0,w,o,a,o,v,e,0,0,0,0,l,m
l,m,e,0,0,s,n,r,e,0,0,0,0,0,l,m
s,n,v,e,0,0,0,0,0,0,0,0,0,0,l,m
0,0,0,0,0,0,0,0,0,0,0,0,0,0,l,m
p,o,v,e,0,0,0,0,p,o,q,e,0,0,l,m
l,m,e,0,0,0,0,w,n,a,n,v,e,0,l,m
l,m,e,0,0,0,0,0,0,x,e,0,0,0,l,m
l,m,e,0,0,0,0,0,0,x,e,0,0,0,l,m
l,m,e,0,0,0,0,0,0,x,e,0,0,i,l,m
l,a,o,o,o,o,o,o,o,a,o,o,o,o,a,m
s,n,n,n,n,n,n,n,n,n,n,n,n,n,n,r
NAME Right Room
WAL a,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
ITM 2 2,8
EXT 0,8 0 14,8
PAL 1

ROOM 2
p,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q
l,a,n,n,n,n,n,n,n,n,n,n,n,n,a,m
l,m,f,0,0,0,0,0,0,0,0,0,0,j,l,m
l,m,0,0,0,0,0,0,0,0,0,0,0,j,l,m
l,m,0,0,0,0,0,0,j,u,0,0,0,j,l,m
l,m,0,0,j,y,0,0,j,t,0,0,0,j,l,m
l,m,0,j,u,0,0,0,0,j,u,0,j,w,n,r
l,m,0,j,x,0,0,0,0,j,x,0,0,0,0,0
l,m,0,j,x,0,0,0,0,j,x,0,j,w,o,q
l,m,0,j,t,0,0,0,0,j,t,0,0,j,l,m
l,m,0,0,j,u,0,0,j,y,0,0,0,j,l,m
l,m,0,0,j,t,0,0,0,0,0,0,0,j,l,m
l,m,0,0,0,0,0,0,0,0,0,0,0,j,l,m
l,m,g,0,0,0,0,0,0,0,0,0,0,j,l,m
l,a,o,o,o,o,o,o,o,o,o,o,o,o,a,m
s,n,n,n,n,n,n,n,n,n,n,n,n,n,n,r
NAME Left Room
WAL a,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
ITM 3 13,7
EXT 15,7 0 1,7
PAL 2

ROOM 3
p,o,o,q,0,p,o,o,o,o,o,o,o,o,o,q
l,a,n,m,0,l,n,n,n,n,a,n,n,n,a,m
l,m,d,t,0,t,d,d,d,d,x,d,d,d,l,m
l,m,0,d,0,d,0,0,0,0,t,0,0,0,l,m
l,m,0,0,0,0,0,0,0,y,d,y,0,0,l,m
l,a,z,z,v,0,0,0,0,d,0,d,0,0,l,m
l,m,d,d,d,u,0,0,0,0,0,0,0,0,l,m
l,m,0,0,z,t,0,0,0,0,w,z,v,0,l,m
l,m,0,0,y,d,0,0,0,u,d,d,d,0,l,m
l,m,0,0,d,0,0,0,0,x,0,0,0,0,l,m
l,m,0,0,0,0,0,0,0,x,0,0,w,z,a,m
l,m,0,w,v,0,0,0,0,x,0,0,d,d,l,m
l,m,0,d,d,w,q,0,p,r,0,0,0,0,l,m
l,m,g,0,0,d,x,k,x,d,0,0,0,i,l,m
l,a,o,o,o,o,m,0,l,o,o,o,o,o,a,m
s,n,n,n,n,n,r,0,s,n,n,n,n,n,n,r
NAME Entrance
WAL a,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
ITM 0 4,2
ITM 1 7,14
EXT 4,0 0 5,14
END 0 7,15
PAL 3

ROOM 4
p,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q
l,a,n,n,n,n,n,n,n,n,n,n,n,n,a,m
l,m,f,0,0,0,0,0,0,0,0,0,0,h,l,m
l,m,0,0,c,0,0,0,0,c,0,0,0,0,l,m
l,m,0,c,u,0,0,0,0,u,0,0,0,0,l,m
l,m,0,w,m,0,0,0,0,x,0,0,0,0,l,m
l,m,0,0,x,0,0,0,0,t,c,c,c,0,l,m
l,m,0,0,x,c,0,0,0,0,w,z,q,0,l,m
l,m,0,0,s,q,0,0,0,0,0,0,x,0,l,m
l,m,0,0,0,x,0,0,0,0,0,c,x,c,l,m
l,m,0,0,c,x,c,c,0,0,0,w,n,z,a,m
l,m,0,0,w,n,o,v,0,0,0,0,0,0,l,m
l,m,0,0,0,0,x,0,0,c,0,c,0,0,l,m
l,m,c,c,c,c,x,c,c,u,0,u,c,c,l,m
l,a,o,o,o,o,a,o,o,m,0,l,o,o,a,m
s,n,n,n,n,n,n,n,n,r,0,s,n,n,n,r
NAME Top Room
WAL a,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
ITM 4 10,13
EXT 10,15 0 10,1
PAL 4

TIL a
10100101
00100100
11000011
00011000
00011000
11000011
00100100
10100101

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL c
10000001
11000011
11100111
11111111
11111111
11111111
11111111
11111111

TIL d
11111111
11111111
11111111
11111111
11111111
11100111
11000011
10000001

TIL e
11111111
11111110
11111100
11111000
11111000
11111100
11111110
11111111

TIL f
11111111
11111110
11111100
11111000
11110000
11100000
11000000
10000000

TIL g
10000000
11000000
11100000
11110000
11111000
11111100
11111110
11111111

TIL h
11111111
01111111
00111111
00011111
00001111
00000111
00000011
00000001

TIL i
00000001
00000011
00000111
00001111
00011111
00111111
01111111
11111111

TIL j
11111111
01111111
00111111
00011111
00011111
00111111
01111111
11111111

TIL k
00000000
00000000
00010000
00111000
01010100
00010000
00010000
00000000

TIL l
10100101
10100100
11000011
10011000
10011000
11000011
10100100
10100101

TIL m
10100101
00100101
11000011
00011001
00011001
11000011
00100101
10100101

TIL n
10100101
00100100
11000011
00011000
00011000
11000011
00100100
11111111

TIL o
11111111
00100100
11000011
00011000
00011000
11000011
00100100
10100101

TIL p
11111111
10100100
11000011
10011000
10011000
11000011
10100100
10100101

TIL q
11111111
00100101
11000011
00011001
00011001
11000011
00100101
10100101

TIL r
10100101
00100101
11000011
00011001
00011001
11000011
00100101
11111111

TIL s
10100101
10100100
11000011
10011000
10011000
11000011
10100100
11111111

TIL t
10100101
10100101
11000011
10011001
10011001
11000011
10100101
11111111

TIL u
11111111
10100101
11000011
10011001
10011001
11000011
10100101
10100101

TIL v
11111111
00100101
11000011
00011001
00011001
11000011
00100101
11111111

TIL w
11111111
10100100
11000011
10011000
10011000
11000011
10100100
11111111

TIL x
10100101
10100101
11000011
10011001
10011001
11000011
10100101
10100101

TIL y
11111111
10100101
11000011
10011001
10011001
11000011
10100101
11111111

TIL z
11111111
00100100
11000011
00011000
00011000
11000011
00100100
11111111

SPR A
00100000
01011000
00011000
00111100
01111110
10111110
00100100
00000100
>
00100000
01011000
00011000
00111100
01111110
01111101
00100100
00100000
POS 3 7,7

SPR a
00000000
01110000
00111100
00111000
11111110
00111000
00101000
00101000
>
00000000
01110000
00111100
10111010
01111100
00111000
00101000
00101000
DLG SPR_0
POS 0 7,6

SPR b
00111000
00111000
00110000
00111010
01111100
01111100
00101000
00101000
>
00111000
00111000
00110000
00111000
01111110
01111100
00101000
00101000
DLG SPR_7
POS 2 6,7

SPR d
00111100
00011100
00001100
00011100
01111100
00011100
00010100
00010100
>
00111100
00011100
00001100
01011100
00111100
00011100
00010100
00010100
DLG SPR_5
POS 1 12,3

SPR e
00000000
00111100
00011000
00111100
01111110
10111101
00111100
00100100
>
00000000
00111100
00011000
00111100
01111110
01111110
00111100
00100100
DLG SPR_6
POS 4 6,3

SPR f
00000010
00000111
01000010
11100000
01000100
00001110
00000100
00000000
>
00000010
00000101
01000010
10100000
01000100
00001110
00000100
00000000
DLG SPR_3
POS 1 5,4

SPR g
00100000
01010000
00100010
00000101
00000010
00100000
01010000
00100000
>
00100000
01010000
00100010
00000111
00000010
00100000
01110000
00100000
DLG SPR_c

SPR h
00100000
01110000
00100000
00000100
01001010
10100100
01000000
00000000
>
00100000
01010000
00100000
00000100
01001110
11100100
01000000
00000000
DLG SPR_e
POS 0 7,2

SPR i
00000000
00000000
00000000
00000000
00000000
00000100
00001110
00000100

SPR j
00100000
01110000
00100000
00000000
00000100
01001010
11100100
01000000
>
00100000
01110000
00100000
00000000
00000100
01001110
10100100
01000000
DLG SPR_i
POS 1 11,11

SPR k
00001000
00011100
00001000
00000000
00100000
01010000
00100000
00000000
>
00001000
00010100
00001000
00000000
00100000
01110000
00100000
00000000
DLG SPR_2
POS 3 10,13

SPR l
00100000
01110000
00100000
00000000
00000100
00001010
00000100
00000000
>
00100000
01010000
00100000
00000000
00000100
00001110
00000100
00000000
DLG SPR_a
POS 4 13,11

SPR m
00001000
00011100
00001000
01000000
10100000
01000010
00000111
00000010
>
00001000
00011100
00001000
01000000
11100000
01000010
00000101
00000010
DLG SPR_h
POS 4 13,3

SPR n
00000000
00000000
01000000
10100000
01000100
00001110
00000100
00000000
>
00000000
00000000
01000000
11100000
01000100
00001010
00000100
00000000
DLG SPR_j
POS 1 8,13

SPR o
00000010
00000111
01000010
11100000
01000100
00001110
00000100
00000000
>
00000010
00000101
01000010
10100000
01000100
00001110
00000100
00000000
POS 1 5,4

SPR p
00100000
01110000
00100000
00000000
00000100
01001010
11100100
01000000
>
00100000
01110000
00100000
00000000
00000100
01001110
10100100
01000000
DLG SPR_g
POS 4 3,6

SPR q
00100000
01110000
00100000
00000000
00000100
01001010
11100100
01000000
>
00100000
01110000
00100000
00000000
00000100
01001110
10100100
01000000
DLG SPR_1
POS 3 4,7

SPR r
00001000
00011100
00001000
00000000
00100000
01010000
00100000
00000000
>
00001000
00010100
00001000
00000000
00100000
01110000
00100000
00000000
DLG SPR_b
POS 0 13,11

SPR s
00001000
00011100
00001000
00000000
00100000
01010000
00100000
00000000
>
00001000
00010100
00001000
00000000
00100000
01110000
00100000
00000000
DLG SPR_9
POS 2 10,5

SPR t
00100000
01110000
00100000
00000000
00000100
00001010
00000100
00000000
>
00100000
01010000
00100000
00000000
00000100
00001110
00000100
00000000
DLG SPR_f
POS 3 12,4

SPR u
00100000
01110000
00100000
00000000
00000100
00001010
00000100
00000000
>
00100000
01010000
00100000
00000000
00000100
00001110
00000100
00000000
DLG SPR_d
POS 0 13,3

SPR v
00001000
00011100
00001000
01000000
10100000
01000010
00000111
00000010
>
00001000
00011100
00001000
01000000
11100000
01000010
00000101
00000010
DLG SPR_8
POS 2 3,11

SPR w
00001000
00011100
00001000
01000000
10100000
01000010
00000111
00000010
>
00001000
00011100
00001000
01000000
11100000
01000010
00000101
00000010
DLG SPR_4
POS 2 12,13

ITM 0
00000000
00000000
11111111
00000000
00000000
00000000
00000000
00000000
>
00000000
00000000
11100111
00000000
00000000
00000000
00000000
00000000
NAME INTRO1
DLG ITM_0

ITM 1
00000000
00000000
00000000
00000000
00000000
11111111
00000000
00000000
NAME ending
DLG ITM_1

ITM 2
00000100
00000100
00000100
00000100
00000100
00000100
00000100
00000100
>
00000100
00000100
00000100
00000000
00000000
00000100
00000100
00000100
NAME right room
DLG ITM_2

ITM 3
00100000
00100000
00100000
00100000
00100000
00100000
00100000
00100000
>
00100000
00100000
00100000
00000000
00000000
00100000
00100000
00100000
NAME left room
DLG ITM_3

ITM 4
00000000
00000000
11111111
00000000
00000000
00000000
00000000
00000000
>
00000000
00000000
11100111
00000000
00000000
00000000
00000000
00000000
NAME top room
DLG ITM_4

ITM 5
00000000
00000000
11111111
00000000
00000000
00000000
00000000
00000000
>
00000000
00000000
11100111
00000000
00000000
00000000
00000000
00000000
NAME middle room intro
DLG ITM_5

DLG ITM_0
"""
{clr2}You have arrived at the BAZAAR. 
You're here on a mission...to trade items with the merchants, with the ultimate goal of getting a PLANT.
Only then can you go home again.{clr2}
"""

DLG ITM_1
"""
{
  - PLANT == 1 ?
    {clr2}You go home to your cat, satisfied with your nice new PLANT. You think you'll come back next week for some CUSHIONS.{clr2}
  - else ?
    {clr2}You give up early and go home. There's always next week.{clr2}
}
"""

DLG SPR_5
"""
{shuffle
  - Hello, and welcome to my little oasis in this busy world. 
  - Hello, little tadpole. Come, be calm. 
  - Hello and welcome, little tadpole. Relax and be at peace. 
}{
  - TEAPOT == 1 ?
     If you have a TEAPOT, I'll trade you for this gaudy NECKLACE. 
    Oh - you do? Here - take it. It's all yours. 
    Ah, let us take tea together and be calm for a moment. {TEAPOT = 2}{NECKLACE = 1}
  - NECKLACE >= 1 ?
     Thank you for the trade, little one. Will you take some tea with me? Let the aroma surround you. Breathe it in and relax. Let it calm you.
  - else ?
     If you have a TEAPOT, I'll trade you for this gaudy NECKLACE.
    Oh - too bad. Maybe later, then?
}
"""

DLG SPR_6
"""
{shuffle
  - Hey you! C'mere! Let's trade! 
  - Hey! C'mere! Let's trade! 
  - Hey, let's trade! 
}{
  - MIRROR == 1 ?
     Yeah, a trade! Let's see...what I need now is a MIRROR! Got one for me?
    Oh, you do? Let's go! Here, you can have this dusty old TEAPOT. {TEAPOT = 1}
{MIRROR = 2}
  - TEAPOT >= 1 ?
     Oh wait, we already traded, didn't we? See ya next time, then!
  - else ?
     Yeah, a trade! Let's see...what I need now is a MIRROR! Got one for me?
    You don't? Bummer! Come back if you find one, alright?
}
"""

DLG SPR_7
"""
{shuffle
  - Hi... 
  - Hello... 
  - Nice day, isn't it...? 
}{
  - NECKLACE == 1 ?
     Want to trade...? I need a NECKLACE...do you have one?
    Oh...you do? Can I have it?
    Oh...I can? Thank you...Here, you can have this BOWL. I hope that's okay... {NECKLACE = 2}{BOWL = 1}
  - BOWL >= 1 ?
     Thanks for trading with me...
  - else ?
     Want to trade...? I need a NECKLACE...do you have one?
    Oh...that's too bad...
    Sorry for bothering you...
}
"""

DLG SPR_0
"""
{sequence
  - Welcome to the BAZAAR! 
}{
  - BOWL == 1 ?
     Ready to trade your BOWL for my PLANT?
    Great, here you go! See ya next time at the BAZAAR! {PLANT = 1} {BOWL = 2}
  - PLANT >= 1 ?
     Hey! I don't have anything else to trade! Come back next time, okay?!
  - else ?
     What's that? You want to trade for one of my PLANTS? 
    Sure thing...I'll give you a PLANT if you can bring me a nice fancy BOWL. 
    Come back when you've got one!
}
"""

DLG ITM_5
"""
{clr2}You enter the main hall of the BAZAAR. The sights and smells and sounds are almost overwhelming...
You focus on a merchant in the middle of the room, shouting about his PLANTS. Great - that's exactly what you need. Maybe he'll trade with you...?{clr2}
"""

DLG ITM_2
{clr2}Your nose burns with the aroma of coffee beans, chai, and mixed spices. Tucked away behind hessian bags, the merchant waves you over.{clr2}

DLG ITM_3
{clr2}Silk, velvet, cotton. Smooth and rough to the touch. Walls of rugs. Embroidered cushions. You can hear merchants murmuring behind each curtain.{clr2}

DLG ITM_4
"""
{clr2}Guitars being tuned. Pianos -
 violins - trumpets - singers - you can't hear your thoughts. Over the clamor, a merchant calls to you.{clr2}
"""

DLG SPR_1
A young woman is selling her grandmother's collection of souvenir spoons.

DLG SPR_2
Between the cracks in the floor, a spray of bright flowers.

DLG SPR_3
A young couple are selling hot chai at a little stall. You buy a cup.

DLG SPR_4
A stack of massive round floor cushions that have been hand embroidered.

DLG SPR_8
A tapestry hanging on the wall.

DLG SPR_9
An old woman is selling crocheted blankets.

DLG SPR_a
A stack of broken keyboards.

DLG SPR_b
An elderly couple are shouting at each other in a language you don't understand.

DLG SPR_c
A little boy is patting a huge dog. 

DLG SPR_d
The smell of fresh donuts.

DLG SPR_e
A young man is selling tiny, elaborately carved fantasy dioramas.

DLG SPR_f
A stray cat winds its way around your ankles, purring loudly.

DLG SPR_g
A group of young men are singing old songs together, a cappella.

DLG SPR_h
You help an old man with his bags.

DLG SPR_i
When you stop at his stall, an old man presses a small bag of coffee beans into your hands.

DLG SPR_j
Clouds drift over the sun, and you shiver in the sudden cool.

END 0
The END

VAR NECKLACE
0

VAR TEAPOT
0

VAR PLANT
0

VAR BOWL
0

VAR MIRROR
1

