
Elemental Island

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME Air
1,135,23
1,194,33
1,255,43

PAL 1
NAME Fire
107,2,2
196,4,4
255,5,5

PAL 2
NAME Water
1,1,120
1,33,212
3,3,255

PAL 3
255,255,255
171,171,171
0,0,0

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,a,0,a,0,0,0,0,a,0,0,a
a,0,0,0,0,a,0,0,0,0,0,0,a,0,a,a
a,0,0,0,0,a,0,a,0,0,0,0,a,0,0,a
a,a,0,0,a,a,0,a,a,a,0,a,a,0,a,a
a,a,0,0,a,0,0,a,0,0,0,0,a,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,a,0,0,a
a,a,a,a,a,a,a,a,0,a,a,a,a,a,0,a
a,a,0,a,a,0,0,a,0,a,0,0,0,0,0,0
a,a,0,a,a,a,0,0,0,0,0,a,a,a,0,a
a,0,0,0,a,a,a,a,a,a,a,a,0,0,0,a
a,0,0,0,a,0,0,0,0,0,a,0,0,0,0,a
a,0,0,0,a,0,0,0,0,0,a,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,a,0,0,0,0,0,a,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Territhonga1
EXT 2,9 1 1,12
EXT 15,8 2 0,8
PAL 0

ROOM 1
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,a,0,a,0,a,0,a
a,0,a,a,a,a,a,a,a,a,0,a,0,a,0,a
a,0,a,0,0,0,0,0,a,a,0,a,0,a,0,a
a,0,a,0,0,0,0,0,a,0,0,a,0,0,0,a
a,0,a,0,0,0,0,0,a,0,a,a,0,a,0,a
a,0,a,0,0,0,0,0,a,0,0,0,0,a,0,a
a,0,0,0,0,0,0,0,a,a,0,a,a,a,0,a
a,0,a,0,0,0,0,0,0,0,0,0,a,0,0,a
a,0,a,a,a,a,a,a,a,a,a,a,a,0,a,a
a,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
a,a,a,0,a,a,a,0,a,a,0,a,a,a,a,a
a,0,0,0,0,0,a,0,a,0,0,a,0,0,0,a
a,0,a,a,a,0,a,0,a,0,a,a,a,a,0,a
a,0,a,0,0,0,0,0,a,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Territongha2
EXT 1,13 0 2,10
PAL 0

ROOM 2
b,b,0,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,b,0,0,b,0,0,0,b,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,b,0,0,b,0,0,0,b,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,0,b,b,b
b,0,0,b,0,0,b,0,0,0,b,0,0,0,0,b
b,0,0,b,0,0,b,0,0,0,b,0,0,0,0,b
b,b,0,b,0,b,b,b,0,b,b,b,b,0,0,b
0,0,0,0,0,0,0,0,0,0,0,0,b,0,0,b
b,b,0,b,0,b,b,0,0,0,0,0,b,0,0,b
b,0,0,b,0,0,b,0,0,0,0,0,0,0,0,0
b,0,0,b,0,0,b,0,0,0,0,0,b,0,0,b
b,b,b,b,b,b,b,0,b,b,b,b,b,b,b,b
b,0,0,b,0,0,b,0,0,0,0,0,0,0,c,b
b,0,0,0,0,0,0,0,0,b,0,0,b,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME BoilingCity1
EXT 2,0 3 2,15
EXT 15,10 4 0,7
END undefined 14,13
PAL 1

ROOM 3
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,b,0,0,0,0,0,0,0,0,0,b,0,0,b
b,0,b,0,0,0,0,0,0,0,0,0,b,0,0,b
b,0,b,0,0,0,0,0,0,0,0,0,b,0,0,b
b,0,b,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,b,0,0,b
b,0,b,0,0,0,0,0,0,0,0,0,b,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,0,0,b
b,0,0,0,b,0,0,0,0,b,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,b,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,b,0,0,0,0,b,0,0,0,0,0,b
b,b,0,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME BoilingCity2
EXT 2,15 2 2,0
PAL 1

ROOM 4
d,d,d,d,d,d,d,d,0,d,d,d,d,d,d,d
d,d,d,d,d,0,0,0,0,0,0,0,0,d,0,d
d,0,0,d,0,0,0,0,0,0,0,0,d,0,0,d
d,0,0,0,0,0,0,0,0,0,0,0,0,0,0,d
d,0,0,d,0,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,0,0,0,0,0,0,0,0,d,0,0,d
d,d,d,d,0,0,0,0,0,0,0,0,0,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,0,0,0,0,0,0,0,0,0,0,0,d
d,d,d,d,0,0,0,0,0,0,0,0,0,0,d,d
d,0,0,d,0,0,0,0,0,0,0,0,0,d,0,d
d,0,0,0,0,0,0,0,0,0,0,0,d,0,0,d
d,0,0,d,0,0,0,0,0,0,0,0,0,0,0,d
d,d,d,d,0,0,0,0,0,0,0,0,0,0,0,d
d,d,d,d,d,0,0,0,0,0,0,0,d,0,0,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
NAME Seawater1
EXT 0,7 2 15,10
PAL 2

ROOM 5
0,0,0,0,0,0,0,0,0,0,d,0,d,0,0,0
d,d,d,d,d,d,d,d,d,d,d,0,d,d,d,dd,
d,0,0,0,0,0,0,0,0,0,0,0,0,0,0,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,0,d
d,0,0,0,0,0,0,0,0,0,d,0,0,0,0,d
d,d,d,d,d,d,d,0,d,d,d,0,d,d,d,d
d,0,0,0,0,0,d,0,d,0,0,0,0,0,0,dd,
d,0,d,d,d,0,d,0,d,0,d,d,d,d,d,d
d,0,d,d,d,0,d,0,d,0,0,0,0,0,0,d
d,0,d,0,0,0,0,0,0,0,d,0,0,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,0,0,0,d
d,0,0,0,0,0,d,0,0,0,0,d,0,d,d,d
d,d,d,d,0,d,d,d,d,0,d,d,0,0,0,d
d,0,0,0,0,0,0,0,0,0,0,d,d,d,0,d
d,0,d,d,0,d,0,d,0,d,0,d,0,0,0,d
d,0,d,d,d,d,d,d,d,d,d,d,d,d,d,d
NAME SeaWater2
EXT 11,0 6 7,5
PAL 2

ROOM 6
e,0,0,0,e,0,e,e,e,0,e,0,0,e,0,0
e,0,0,0,e,0,0,e,0,0,e,0,e,0,0,0
e,0,e,0,e,0,0,e,0,0,e,0,e,e,0,0
e,e,0,e,e,0,0,e,0,0,e,0,0,e,0,0
e,0,0,0,e,0,0,e,0,0,e,0,e,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,e,e,0,0,e,0,e,e,e,0,0,0,0
0,0,e,0,0,0,e,0,e,e,0,0,e,0,0,0
0,e,0,0,0,0,e,0,e,e,0,0,0,e,0,0
0,e,0,0,e,e,e,0,e,e,0,0,0,e,0,0
0,0,e,0,0,e,e,0,e,e,0,0,e,0,0,0
0,0,0,e,e,0,0,e,0,e,e,e,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME SuperSecretShhhh
PAL 3

TIL a
01100110
11000011
10100101
00011000
00011000
10100101
11000011
01100110
WAL true

TIL b
01101101
00000000
11011011
00000000
01101101
00000000
11011011
00000000
WAL true

TIL c
00011000
00111100
00111100
00111110
00011000
00111100
00111100
00111100
>
00000000
00111100
00111100
00111100
00011000
00111100
00111100
00111100

TIL d
01111001
11111110
11011111
11111110
01111011
01111110
11111110
01001011

TIL e
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000

SPR A
00000000
00011000
00111100
01011010
01100110
01111110
00111100
00100100
POS 0 2,2

SPR a
00000000
00011000
01011010
01000010
01111110
00111100
00011000
00000000
DLG SPR_0
POS 0 6,6

SPR b
00100100
00111100
00100100
00111100
00100100
00111100
00100100
00100100
POS 1 1,14

SPR c
00000000
00011000
01011010
01000010
01111110
10111101
00011000
00011000
DLG SPR_1
POS 1 5,5

SPR d
00100100
00111100
00100100
00111100
00100100
00111100
00100100
00100100

SPR e
00100100
00111100
00100100
00111100
00100100
00111100
00100100
00100100
POS 0 2,8

SPR f
00000000
00011000
01011010
01000010
01111110
00111100
00011000
00000000
DLG SPR_2
POS 2 7,9

SPR g
00000000
00011000
01011010
01000010
01111110
10111101
00011000
00011000
DLG SPR_3
POS 3 7,7

SPR h
00000000
00011000
01011010
01000010
01111110
00111100
00011000
00000000
DLG SPR_4
POS 4 8,7

SPR i
00000000
00000000
00000000
00000000
00011000
00100100
01111110
00111100
DLG SPR_5
POS 5 9,2

ITM 0
00001000
00011000
00011000
00011000
00011000
00111100
00011000
00011000
NAME basicsword
DLG ITM_0

DLG SPR_0
This is {clr1}Territongha Jungle{clr1}! Our village elder is in the treetops, he said something about wanting to see you!

DLG ITM_0
You got the BASIC SWORD! Whoop De Doo.

DLG SPR_1
Ah, the newcomer. So, seeing as you're the player, I've given you a quest. Go find some special gauntlet in the {clr1}Boiling City{clr1} to the {clr1}EAST{clr1}, and then you win or whatever.

DLG SPR_2
Welcome to {clr1}Boiler City!{clr1} Hey, if you were to talk our leader in the {clr1}North District{clr1} into letting us work for less than 50 hours a day, we'll make you a deity! Promise!

DLG SPR_3
(You talk to the mayor of the {clr1}Boiling City{clr1}. As promised the people consider you a deity for lowering their work hours to an hour a day. As food disappears and goods simply stop being manufactured, life is good.

DLG SPR_4
Welcome to the village of {clr1}Seawater!{clr1} Say, all of our water is draining... There's a geyser up {clr1}north{clr1} that can flood the place. Can you go unplug it? Please?

DLG SPR_5
You unplug the geyser! Good job, high five, whoopee.

END 0


END undefined
You find the gauntlet the leader of Territongha wanted. You win, I guess.

VAR a
42


