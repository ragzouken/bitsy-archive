{rbw}{wvy}G  O  O  D  N  I  G  H  T{wvy}{rbw}

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
0,0,0
180,182,194
255,255,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,a,a,a,a,a,a,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,a,a,a,a,a,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME start
EXT 15,13 1 0,1
PAL 0

ROOM 1
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,0,0,0,0,0,0,0,0,b
0,0,0,0,0,0,b,b,b,b,b,b,b,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,0,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,0,b
NAME snek1
EXT 0,1 0 15,13
EXT 14,15 2 1,0
PAL 0

ROOM 2
b,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,c,d,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME tosnekhausto1
ITM 6 4,11
EXT 1,0 1 14,15
EXT 13,4 3 7,13
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,e,e,e,e,e,e,e,e,e,e,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,e,e,e,e,0,e,e,e,e,e,0,0,0
0,0,0,0,0,0,e,e,e,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME inhausto1
EXT 3,12 4 7,7
PAL 0

ROOM 4
h,h,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,0,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
NAME story1
ITM 1 2,1
ITM 1 5,14
ITM 1 12,7
ITM 1 3,11
ITM 1 9,3
ITM 1 14,10
ITM 1 6,10
ITM 1 8,7
EXT 14,0 c 0,7
PAL 0

ROOM 8
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
h,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,0,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,h,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,0,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,h,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,0,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,h,0,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,h,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,0,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,h,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,0,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,h,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,0,h
NAME story2
ITM 3 7,10
ITM 3 8,8
ITM 3 4,3
ITM 3 1,13
ITM 3 11,14
ITM 3 12,4
ITM 3 3,11
ITM 3 9,12
ITM 3 10,6
ITM 3 6,5
ITM 3 5,9
ITM 3 2,1
ITM 3 14,2
EXT 7,7 j 4,13
PAL 0

ROOM 9
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
h,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,0,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,h,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,0,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,h,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,0,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,h,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,0,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,h,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,0,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,h,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,0,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
NAME story3
ITM 4 8,12
ITM 4 12,4
ITM 4 6,10
ITM 4 10,7
ITM 4 11,14
ITM 4 14,3
ITM 4 3,5
ITM 4 4,1
ITM 4 7,5
ITM 4 8,8
ITM 4 1,9
EXT 8,7 k 4,13
PAL 0

ROOM a
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,0,h,0,h,0,h,0,h
h,0,h,0,h,0,h,h,h,h,0,h,0,h,0,h
h,0,h,0,h,0,0,0,0,0,0,h,0,h,0,h
h,0,h,0,h,h,h,h,h,h,h,h,0,h,0,h
h,0,h,0,0,0,0,0,0,0,0,0,0,h,0,h
h,0,h,h,h,h,h,h,h,h,h,h,h,h,0,h
h,0,0,0,0,0,0,0,0,0,0,0,0,0,0,h
h,h,h,h,h,h,h,h,h,h,h,h,h,h,h,h
NAME story4
ITM 5 4,1
ITM 5 14,6
ITM 5 8,14
ITM 5 1,9
ITM 5 9,3
ITM 5 6,7
ITM 5 10,10
ITM 5 6,5
ITM 5 3,11
ITM 5 12,10
EXT 8,8 l 7,1
PAL 0

ROOM c
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
i,i,i,i,i,i,i,0,i,i,i,i,i,i,i,i
i,i,i,i,i,i,0,0,0,i,i,i,i,i,i,i
i,i,i,i,i,0,0,0,0,0,i,i,i,i,i,i
i,i,i,i,0,0,0,0,0,0,0,i,i,i,i,i
i,i,i,0,0,0,0,0,0,0,0,0,i,i,i,i
i,i,0,0,0,0,0,0,0,0,0,0,0,i,i,i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,i,i
i,i,0,0,0,0,0,0,0,0,0,0,0,0,0,0
i,i,i,0,0,0,0,0,0,0,0,0,0,0,i,i
i,i,i,i,0,0,0,0,0,0,0,0,0,i,i,i
i,i,i,i,i,0,0,0,0,0,0,0,i,i,i,i
i,i,i,i,i,i,0,0,0,0,0,i,i,i,i,i
i,i,i,i,i,i,i,0,0,0,i,i,i,i,i,i
i,i,i,i,i,i,i,i,0,i,i,i,i,i,i,i
i,i,i,i,i,i,i,i,i,i,i,i,i,i,i,i
NAME meet
EXT 15,8 g 2,13
PAL 0

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,e,e,e,e,e,e,e,e,e,e,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,e,e,e,e,0,e,e,e,e,e,0,0,0
0,0,0,0,0,0,e,e,e,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME inhausto2
EXT 11,3 8 14,15
PAL 0

ROOM e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,e,e,e,e,e,e,e,e,e,e,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,e,e,e,e,0,e,e,e,e,e,0,0,0
0,0,0,0,0,0,e,e,e,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME inhausto3
EXT 3,3 9 0,14
PAL 0

ROOM f
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,e,e,e,e,e,e,e,e,e,e,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,f,f,f,f,f,f,f,f,f,e,0,0,0
0,0,e,g,f,f,f,f,f,f,f,g,e,0,0,0
0,0,e,e,e,e,e,0,e,e,e,e,e,0,0,0
0,0,0,0,0,0,e,e,e,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME inhausto4
EXT 11,12 a 0,1
PAL 0

ROOM g
b,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,c,d,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME to snekhausto2
ITM 6 5,7
EXT 1,0 1 14,15
EXT 13,4 d 7,13
PAL 0

ROOM h
b,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,c,d,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,0,b,b
NAME to snekhausto3
ITM 6 11,10
EXT 1,0 1 14,15
EXT 13,4 e 7,13
PAL 0

ROOM i
b,0,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,d,d,b
b,0,0,0,0,0,0,0,0,0,0,0,d,c,d,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME to snekhausto4
ITM 6 5,14
EXT 1,0 1 14,15
EXT 13,4 f 7,13
PAL 0

ROOM j
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,0
b,b,b,b,0,0,0,0,0,0,0,0,b,b,b,b
b,b,b,0,0,0,0,0,0,0,0,0,0,b,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,b,0,0,0,0,0,0,0,0,0,0,b,b,b
b,b,b,b,0,0,0,0,0,0,0,0,b,b,b,b
b,b,b,b,0,0,b,0,0,b,0,0,b,b,b,b
b,b,b,b,f,0,b,0,0,b,0,f,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME skull23
EXT 11,13 h 13,15
PAL 0

ROOM k
0,0,0,b,b,b,b,b,b,b,b,b,b,0,0,0
0,0,b,b,0,0,0,0,0,0,0,0,b,b,0,0
0,b,b,0,0,0,0,0,0,0,0,0,0,b,b,0
0,b,0,0,0,0,0,0,0,0,0,0,0,0,b,0
0,b,0,0,0,0,0,0,0,0,0,0,0,0,b,0
0,b,0,0,b,0,b,0,0,b,0,b,0,0,b,0
0,b,0,0,0,b,0,0,0,0,b,0,0,0,b,0
0,b,0,0,b,0,b,0,0,b,0,b,0,0,b,0
0,b,0,0,0,0,0,0,0,0,0,0,0,0,b,0
0,b,0,0,0,0,0,0,0,0,0,0,0,0,b,0
0,b,b,0,0,0,0,0,0,0,0,0,0,b,b,0
0,0,b,b,0,0,0,0,0,0,0,0,b,b,0,0
0,0,0,b,0,0,b,0,0,b,0,0,b,0,0,0
0,0,0,b,f,0,b,0,0,b,0,f,b,0,0,0
0,0,0,b,b,b,b,b,b,b,b,b,b,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME skull34
EXT 11,13 i 1,7
PAL 0

ROOM l
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,0,0,0,f,0,0,0,0,b,b,b,b
b,b,b,0,0,0,0,0,0,0,0,0,0,b,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,b,0,0,0,0,b,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,b,0,0,0,0,0,0,0,0,0,0,b,b,b
b,b,b,b,0,0,0,0,0,0,0,0,b,b,b,b
b,b,b,b,0,0,b,0,0,b,0,0,b,b,b,b
b,b,b,b,0,0,b,0,f,b,0,0,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME skull4fin
EXT 8,13 m 0,5
PAL 0

ROOM m
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
NAME fin1
ITM 7 5,5
ITM 7 13,5
EXT 15,5 n 0,11
PAL 0

ROOM n
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
NAME fin2
ITM 7 3,11
ITM 7 11,11
EXT 15,11 o 7,0
PAL 0

ROOM o
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,0,d,d,d,d,d,d,d,d
NAME fin3
ITM 7 7,2
END 0 7,15
PAL 0

TIL a
00000000
00010000
00010000
00011110
01111000
00001000
00001000
00000000
>
00000000
00000000
00010000
00011100
00111000
00001000
00000000
00000000
WAL true

TIL b
00000000
01000010
00100100
00011000
00011000
00100100
01000010
00000000
>
10000001
01000010
00100100
00011000
00011000
00100100
01000010
10000001
WAL true

TIL c
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL false

TIL d
11111111
10000001
10000001
10000001
10000001
10000001
10000001
11111111
WAL true

TIL e
11111111
11000011
10100101
10000001
10000001
10100101
11000011
11111111
>
11111111
11000011
10100101
10011001
10011001
10100101
11000011
11111111
NAME hauswall
WAL true

TIL f
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME hausfloor

TIL g
01010101
10101010
01010101
10101010
01010101
10101010
01010101
10101010
>
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL h
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL i
01000000
11100000
01000100
00000100
00011111
00000100
00000100
00000000
>
01000000
11110001
01000000
01000100
00001110
00000100
00000000
01000000
WAL true

SPR 10
00001000
00001000
01111111
01010101
01111111
00001000
00001000
00001000
NAME signhel
DLG SPR_e

SPR A
00111100
00111100
00011000
00111100
00111100
00111100
00100100
00110110
>
00111100
00111100
00011000
00111100
00111100
00111100
00100100
00110110
POS 0 5,6

SPR a
00001000
00001000
01111111
01010001
01111111
00001000
00001000
00001000
NAME signpost1
DLG SPR_0
POS 0 7,13

SPR b
00000000
01111111
11111100
11111101
11111100
11111111
11111111
01101100
>
00000000
01111111
11111100
11111100
11111100
11111111
11111111
11111111
NAME snek1pta
DLG SPR_1
POS 1 10,3

SPR c
00000000
11111111
00001111
10001111
00001111
11111111
11111111
11111111
>
00000000
11111111
00001111
11001111
00001111
11111111
11111111
11111111
NAME snek1ptb
DLG SPR_2
POS 1 11,3

SPR d
00000000
11110000
11111100
11111110
11111111
11111111
11111111
11111111
NAME snek1ptc
DLG SPR_3
POS 1 12,3

SPR e
00000000
11111111
01111111
00000000
00000000
00000000
00000000
00000000
>
11111111
11111111
01111111
00000000
00000000
00000000
00000000
00000000
NAME snek1ptd
DLG SPR_4
POS 1 10,4

SPR f
11111111
11111111
11111111
00000011
00000111
00001111
00011111
00111111
NAME snekpt1f
DLG SPR_5
POS 1 11,4

SPR g
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME snek1ptg
DLG SPR_6
POS 1 12,4

SPR h
01111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME snek1pth
DLG SPR_7
POS 1 11,5

SPR i
00000000
00000000
00000001
00000011
00000111
00001111
00011111
00111111
NAME snek1ptgjdjgf
DLG SPR_8
POS 1 10,5

SPR j
11111110
11111100
11111000
11110000
11100000
11000000
10000000
00000000
DLG SPR_9
POS 1 12,5

SPR k
01111111
11111111
11111111
11111111
11111111
11111111
01111111
00111111
DLG SPR_a
POS 1 10,6

SPR l
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
DLG SPR_b
POS 1 11,6

SPR m
11111110
11111111
11111111
11111111
11111111
11111111
11111111
11111110
DLG SPR_c
POS 1 12,6

SPR o
00001000
01111111
01000101
01111111
00001000
00001000
00001000
00001000
NAME signpost2
DLG SPR_d
POS 2 11,4

SPR r
00111100
00101100
00111100
00111100
00010000
00001000
00010000
00001000
>
00111100
00111100
00101100
00111100
00010000
00001000
00010000
00001000
DLG SPR_f
POS c 4,7

SPR s
01111110
10000001
00000000
00000000
00000000
00000000
00000000
11100111
>
01111110
10000001
00000000
00000000
00000000
01100110
01100110
01100110
DLG SPR_g
POS c 10,6

SPR t
00000000
10000001
01000010
00100100
00100000
00100000
00100000
00100000
>
00000000
10000000
01000010
00100100
00101000
00100000
00100000
00100000
DLG SPR_h
POS c 11,6

SPR u
00100111
00100000
00100000
01000000
10000000
00000100
00000010
00000001
>
00101110
00100000
00100000
01000000
10001000
00000100
00000010
00000000
DLG SPR_i
POS c 11,7

SPR v
00000000
00000000
00011000
00000000
10000001
11111111
11111111
11111111
>
00000000
00011000
00011000
00000000
10000001
11111111
11111111
11111111
DLG SPR_j
POS c 10,7

SPR w
01110100
00000100
00000100
00000010
00001001
00010000
00100000
00000000
>
11100100
00000100
00000100
00000010
00000001
00010000
00100000
01000000
DLG SPR_k
POS c 9,7

SPR x
00000000
00000001
01000010
00100100
00010100
00000100
00000100
00000100
>
00000000
10000001
01000010
00100100
00000100
00000100
00000100
00000100
DLG SPR_l
POS c 9,6

SPR y
11111111
00011000
00011000
00100000
00100000
00011100
00000010
00000100
>
11111111
00011000
00011000
00000100
00000100
00111000
01000000
00100000
DLG SPR_m
POS c 10,8

SPR z
00000000
00101010
00101010
00111110
00110110
00111110
00000000
00000000
>
00000000
00000000
00101010
00101010
00111110
00110110
00111110
00000000
DLG SPR_n
POS c 10,5

ITM 1
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME story-r1
DLG ITM_1

ITM 3
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME story-r2
DLG ITM_3

ITM 4
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME story-r1r3
DLG ITM_4

ITM 5
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG ITM_5

ITM 6
00111100
00101100
00101100
00111100
00111100
00111100
00100100
01100110
>
00111100
00110100
00110100
00111100
00111100
00111100
00100100
01100110
DLG ITM_6

ITM 7
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME finroom
DLG ITM_0

DLG SPR_0
{wvy}{shk}Tread Carefully{shk}{wvy}

DLG SPR_1
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_2
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_3
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_4
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_5
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_6
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_7
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_8
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_9
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_a
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_b
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_c
"""
{wvy}Well, hello there!
Are you here for the party?
Come on in!{wvy}
{shk}The more the merrier, right?{shk}
{wvy}Oh, 'scuse me. I get too excited when I meet new {shk}victi-{shk} I mean friends!{wvy}
"""

DLG SPR_d
"""
{rbw}{wvy}PARTY TIME{wvy}{rbw}

{rbw}Come on in!{rbw}
"""

DLG ITM_1
"""
{shuffle
  - {wvy}d0          u          no7           PheEl      {shk}sUfF0c47eD?{shk}{wvy}
  - {wvy}crY AlL u WaN7{wvy}
    
    {shk}n 0        0 N 3        k 4 n       h 3 4 R         U{shk}
  - {wvy}{shk}H0W d0 U le4VE 7h12 n19H7m4RE{shk}{wvy}
  - {shk}1       k4N7       8R347h3{shk}
  - {shk}pL3a53  le4Ve   
    mE     4L0ne{shk}
  - {wvy}WHy W0n7 u l37 m3 l34v3{wvy}
  - {shk}1 jU5t w4nt t0 L34V3{shk}
}
"""

DLG ITM_3
"""
{sequence
  - Once there was a person
  - who wanted nothing more
  - but to be {shk}happy{shk}
  - kept trying
    always {rbw}smiling{rbw}
  - each day just took more
  - more from their heart
  - and less {shk}left{shk} for them
  - until one day
  - {shk}nothing was left for them{shk}
  - {shk}and nothing to give{shk}
  - and then
  - {shk}{wvy}they broke{wvy}{shk}
  - {rbw}sound familiar?{rbw}
}
"""

DLG ITM_4
"""
{sequence
  - why does it hurt to think now
  - i cant remember when i la{shk}st had a thought for myself{shk}
  - {wvy}everyone has the right to my thoughts {shk}except for me{shk}{wvy}
  - everything in this room is {wvy}not mine{wvy}
  - {wvy}someone else thought of this{wvy}
  - {wvy}i just ha{shk}ve to keep it{wvy}{shk}
  - {wvy}like the good person {shk}i am{shk}{wvy}
  - {shk}why do i suffer{shk}
  - think
  - {wvy}think{wvy}
  - {rbw}{wvy}think{wvy}{rbw}
}
"""

DLG ITM_5
"""
{sequence
  - {wvy}why do people take things for granted?{wvy}
  - so so many things
  - they imagine its so {shk}common{shk}
  - but its not that easy to give someone {shk}a part of yourself{shk}
  - they imagine you {wvy}can give everything they need and want{wvy}
  - {wvy}and they think you will {shk}never get tired{wvy}{shk}
  - {shk}but no{shk}
  - {shk}you do{shk}
  - and it {shk}hurts{shk}
  - {rbw}and im so tired{rbw}
}
"""

DLG SPR_f
"""
{sequence
  - {wvy}who are you?
    to come and talk to {rbw}the master?{rbw}{wvy}
  - {wvy}i hope you were enlightened.
    Come back soon!{wvy}
}
"""

DLG ITM_6
"""
{
  - {item "6"} == 1 ?
    H3y, m4yB3 90 t0 TH3 B0tT0M l3Ft h4ND 5ID3 0f tH3 5cR33n Wh3N ur3 1N 73h h0Uz3
  - {item "6"} == 2 ?
    900d, N0w 90 70 73h 70P r19h7 h4nD k0Rn3R
  - {item "6"} == 3 ?
    ALM05T tH3R3 - 90 T0 T3h T0p L3fT
  - {item "6"} == 4 ?
    4lL 7h@ 12 L3F7 12 7h3 8O77Om R19hT NoW
}
"""

DLG SPR_h
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_i
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_j
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_g
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_k
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_l
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_m
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG SPR_n
"""
ah, hello there.

i was expecting you to come earlier, 
but never mind that.

ah, you must be confused.

you must be thinking,
who is this stranger anyway?
to keep it simple,
i am you.
i am what makes you think.
i am what makes you feel.
i know, must be a lot to take in
i do know, because i am you
haha

but to explain everything,
this is your mind at work now
im assuming you went through one of the spirals, right?
those lines you heard walking through?
they were your thoughts of self doubt.
but what about that snake guy in the beginning?
he has a name - rekaso
he is also you, and me.
i am the more sensible side

but he is what releases those thoughts out
he's trying to convince you

he wants you to think his reasoning is better
youre in a dream now
i hope you know that
but the thing about this one
it doesnt exactly end, haha
you need to face all of those thoughts to end this
why?
you kept hiding away
thats what bought me and him as what you see now.
rekaso now has control because he knows 
its time for you to face this.
it was all a set up for you.
you need to face your thoughts
and prove yourself
to yourself.
"""

DLG ITM_0
"""
{sequence
  - {rbw}b r e a t h e{rbw}
  - {wvy}y o u   d i d   i t . . .{wvy}
  - y o u ' v e   f a c e d   y o u r   b i g g e s t   f e a r
  - {wvy}y o u r s e l f   a n d   y o u r   t h o u g h t s{wvy}
  - {rbw}{wvy}n o w   g o   b e   f r e e .{wvy}{rbw}
}
"""

DLG SPR_e
talking to the guy who hangs around in front of the house might be {rbw}useful....{rbw}

END 0
{rbw}{wvy}Y O U ' R E   F R E E{wvy}{rbw}

END undefined


END undefinee


END undefinef


END undefineg


