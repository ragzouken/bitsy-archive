DREAMS OF THE PAST

# BITSY VERSION 5.0

! ROOM_FORMAT 1
! ! NaN

PAL 0
NAME normalworld
94,186,108
216,232,169
28,43,38

PAL 1
NAME ohno!
34,99,28
184,199,114
121,168,167

PAL 2
NAME dream
28,54,96
79,132,173
145,255,211

PAL 3
NAME nightmare
0,0,0
11,35,102
127,235,208

ROOM 0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,c,b,b,b,b,b,b,b,b,b,b,b,b,b
f,f,d,0,0,0,0,0,0,0,0,0,0,0,0,0
f,f,d,0,0,0,0,0,0,0,0,0,0,0,0,0
f,f,d,0,0,0,0,0,0,0,0,0,0,0,0,0
f,f,c,b,b,b,b,b,b,b,b,b,b,b,b,b
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME beginning
EXT 15,6 1 0,6
EXT 15,7 1 0,7
EXT 15,8 1 0,8
EXT 3,6 a 7,12

ROOM 1
f,f,0,0,f,f,c,d,0,d,c,f,f,f,f,f
f,f,f,f,f,f,c,d,0,d,c,f,0,0,0,f
f,f,0,f,f,f,c,d,0,d,c,f,f,f,0,f
f,f,f,f,f,f,c,d,0,d,c,f,f,0,0,f
f,f,f,f,f,f,c,d,0,d,c,f,f,0,f,f
b,b,b,b,b,b,b,c,0,d,c,f,f,f,f,f
0,0,0,0,0,0,0,0,0,d,c,f,f,0,f,f
0,0,0,0,0,0,0,0,0,d,c,f,f,f,f,f
0,0,0,0,0,0,0,0,0,d,c,f,f,f,f,f
b,b,b,b,b,b,b,b,b,c,c,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,0,0,0,f
0,f,f,f,f,f,f,f,f,f,f,f,f,f,0,f
0,f,f,f,f,f,f,f,f,f,f,f,f,0,0,f
0,f,f,f,f,0,0,0,f,f,f,f,f,0,f,f
f,f,f,f,f,f,f,0,f,f,f,f,f,f,f,f
f,f,f,f,f,f,0,0,f,f,f,f,f,0,f,f
NAME path1
EXT 8,0 2 8,15
PAL 0

ROOM 2
f,f,g,g,f,f,f,f,f,f,f,f,g,g,f,f
f,f,g,g,f,f,f,f,f,f,f,f,g,g,f,f
f,f,g,g,g,g,g,g,g,g,g,g,g,g,f,f
f,f,g,g,g,g,g,g,g,g,g,g,g,g,f,0
f,f,g,0,0,0,0,g,g,0,0,0,0,g,f,f
f,f,g,g,g,g,g,g,g,g,g,g,g,g,f,f
f,f,0,g,g,g,0,0,0,0,g,g,g,0,f,f
f,f,g,g,g,0,0,0,0,0,0,g,g,g,f,f
f,f,0,g,g,0,0,0,0,0,0,g,g,0,f,f
f,f,f,g,g,g,c,0,0,c,g,g,g,f,f,f
f,f,f,g,g,g,b,0,0,b,g,g,g,f,f,f
f,f,f,g,g,g,0,0,0,0,g,g,g,f,f,f
f,f,f,g,g,g,0,0,0,0,g,g,g,f,f,f
f,f,f,g,g,g,g,0,0,g,g,g,g,f,f,f
f,f,f,g,g,g,g,0,0,g,g,g,0,f,f,f
f,f,f,g,g,g,g,0,0,g,g,g,g,f,f,f
NAME entrance
EXT 8,8 3 8,15
EXT 7,8 3 7,15
PAL 1

ROOM 3
e,e,e,0,e,e,e,e,e,0,e,e,e,e,0,0
e,e,0,0,0,e,0,e,e,e,e,0,e,e,e,0
e,e,e,0,0,e,e,e,e,e,0,0,e,e,e,e
e,c,0,0,0,c,b,b,b,b,c,0,0,0,c,e
e,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
e,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
0,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
0,d,0,0,0,0,0,0,0,0,0,0,0,0,d,0
e,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
e,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
0,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
e,c,0,c,b,c,0,0,0,0,c,b,c,0,c,e
e,e,e,e,e,d,0,0,0,0,d,e,e,e,0,0
e,0,e,e,0,d,0,0,0,0,d,e,0,e,e,0
e,0,0,e,e,c,0,0,0,0,c,0,0,e,e,e
e,e,e,e,e,d,0,0,0,0,d,e,0,0,e,0
NAME inside
EXT 3,0 4 3,15
PAL 2

ROOM 4
e,c,0,0,0,c,e,e,e,e,c,0,0,0,c,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,d,e,0,0,d,e
e,d,0,0,e,e,e,e,e,e,e,0,0,0,d,e
e,d,0,0,0,e,e,e,e,e,d,0,0,e,e,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,e,e
e,d,0,0,0,c,b,b,b,e,c,0,0,0,d,e
e,d,0,0,0,0,0,0,0,e,0,0,0,0,d,e
e,d,0,0,0,0,0,0,0,0,0,0,0,0,d,e
e,d,0,0,0,0,0,0,0,0,e,0,0,0,d,e
e,e,0,0,0,c,b,b,b,e,e,b,b,b,c,e
e,e,0,0,0,d,e,e,e,e,e,e,e,e,e,e
e,d,0,0,0,d,e,0,e,0,e,e,0,e,0,e
e,d,0,0,0,d,e,0,0,0,e,e,0,0,0,e
e,c,0,0,0,c,e,e,e,e,e,e,e,e,e,e
e,0,0,0,0,0,e,e,e,e,e,e,e,e,e,e
NAME maze1
EXT 12,0 3 13,11
EXT 11,0 3 13,11
EXT 13,0 3 13,11
EXT 4,0 6 4,15
EXT 2,0 6 2,15
EXT 3,0 6 3,15
PAL 2

ROOM 6
e,c,0,0,0,c,e,e,e,e,c,0,0,0,c,e
e,d,e,0,0,d,e,e,e,e,d,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,e,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,e,e
e,d,0,0,0,c,e,e,b,e,c,0,0,0,e,e
e,e,e,0,0,0,e,e,0,0,0,0,0,0,d,e
e,e,0,0,0,0,0,0,0,0,0,0,0,0,d,e
e,d,0,0,0,0,0,0,0,e,e,0,0,0,d,e
e,d,0,0,0,c,b,b,e,b,e,e,b,b,c,e
e,d,0,0,0,d,e,e,e,e,e,e,e,e,e,e
e,d,0,0,0,e,e,0,0,0,e,e,0,0,0,e
e,d,0,0,0,d,e,0,e,0,e,e,0,e,0,e
e,c,0,0,0,c,e,0,0,0,e,e,0,0,0,e
e,0,0,0,0,0,e,e,e,e,e,e,e,e,e,e
NAME maze2
EXT 2,0 3 2,11
EXT 3,0 3 2,11
EXT 4,0 3 2,11
EXT 11,0 7 11,15
EXT 12,0 7 12,15
EXT 13,0 7 13,15
PAL 2

ROOM 7
e,c,0,0,0,c,e,e,e,e,c,0,0,0,c,e
e,d,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,e,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,e,e,0,0,d,e,e,e,e,d,0,e,e,e,e
e,e,e,0,0,d,e,e,e,e,d,0,0,e,e,e
e,e,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,e,0,0,0,c,b,b,e,b,c,0,0,0,d,e
e,d,0,0,0,e,0,0,0,0,0,0,e,0,d,e
e,d,0,0,0,0,e,e,0,0,e,e,0,0,d,e
e,d,e,0,0,0,0,0,0,0,0,0,0,0,d,e
e,c,e,b,b,b,e,e,e,e,e,e,0,0,c,e
e,e,e,e,e,e,e,e,e,e,e,0,0,0,d,e
e,e,0,e,e,e,0,e,e,e,d,0,0,e,e,e
e,0,e,0,e,0,e,0,e,e,e,0,0,0,e,e
e,e,e,e,e,e,e,e,e,e,d,0,0,0,d,e
e,e,e,e,e,e,e,e,e,e,d,0,0,0,d,e
NAME maze3
EXT 2,0 8 2,15
EXT 3,0 8 3,15
EXT 4,0 8 4,15
EXT 11,0 3 13,11
EXT 12,0 3 13,11
EXT 13,0 3 13,11
PAL 2

ROOM 8
e,c,0,0,0,c,e,e,e,e,c,0,0,0,c,e
e,e,0,0,0,d,e,e,e,e,d,0,0,0,d,e
e,d,0,0,0,d,e,e,e,e,e,e,0,0,e,e
e,d,0,e,e,d,e,e,e,e,e,e,0,0,d,e
e,d,0,e,e,e,e,e,e,e,e,e,0,0,d,e
e,e,0,e,0,e,e,e,e,e,e,0,0,0,d,e
e,d,0,0,0,c,e,b,b,b,e,0,0,e,e,e
e,d,0,0,0,0,e,0,0,0,0,0,0,e,e,e
e,e,e,e,0,0,0,0,e,0,0,0,0,0,d,e
e,e,e,0,0,e,0,0,e,e,0,e,0,0,d,e
e,e,e,0,0,e,b,b,e,e,b,e,b,b,c,e
e,e,e,e,0,e,e,e,e,e,e,e,e,e,e,e
e,e,e,0,0,d,e,e,e,e,e,e,e,e,e,e
e,d,0,0,0,e,e,e,e,e,e,e,e,e,e,e
e,c,0,0,0,c,e,e,e,e,e,e,e,e,e,e
e,0,0,0,0,0,e,e,e,e,e,e,e,e,e,e
NAME maze4
EXT 2,0 9 7,15
EXT 3,0 9 7,15
EXT 4,0 9 7,15
EXT 11,0 9 7,15
EXT 12,0 9 7,15
EXT 13,0 9 7,15
PAL 2

ROOM 9
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
g,g,g,g,g,c,b,b,b,b,b,b,b,b,b,b
g,g,g,g,g,d,0,0,0,0,0,0,0,0,0,0
g,g,g,g,g,d,0,0,0,0,0,0,0,0,0,0
g,g,g,g,g,d,0,0,0,0,0,0,0,0,0,0
g,g,g,g,g,d,0,0,0,c,b,b,b,b,b,b
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,d,0,0,0,d,g,g,g,g,g,g
g,g,g,g,g,c,0,0,0,c,g,g,g,g,g,g
NAME corridor
EXT 15,3 a 0,3
EXT 15,4 a 0,4
EXT 15,5 a 0,5
PAL 3

ROOM a
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
c,c,b,b,b,b,b,b,b,b,b,b,b,b,c,g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,d,g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,d,g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,d,g
c,c,b,b,b,b,b,b,b,b,c,0,0,0,d,g
g,d,0,0,0,0,0,0,0,0,0,0,0,0,d,g
g,d,0,0,0,0,0,0,0,0,0,0,0,0,d,g
g,d,0,0,0,0,0,0,0,0,0,0,0,0,d,g
g,d,0,0,0,c,b,b,b,b,b,b,b,b,c,g
g,d,0,0,0,0,0,0,0,0,0,0,0,0,c,b
g,d,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,d,0,0,0,0,0,0,0,0,0,0,0,0,c,b
g,c,b,b,b,b,b,b,b,b,b,b,b,b,c,g
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
NAME the-end
END undefined 15,12
PAL 3

TIL b
00000000
11111111
11111111
11111111
11111111
11111111
11111111
00000000
NAME wall h
WAL true

TIL c
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
NAME wall c
WAL true

TIL d
01111110
01111110
01111110
01111110
01111110
01111110
01111110
01111110
NAME wall v
WAL true

TIL e
11011011
01101101
10110110
11011011
01101101
10110110
11011011
01101101
>
01101111
10110110
11011111
01101101
10110110
11111011
01101101
11110110
NAME ooo
WAL true

TIL f
11000011
11011011
11011011
11000011
00111100
10111101
10111101
00111100
>
11110000
11110110
11110110
11110000
00001111
01101111
01101111
00001111
NAME pretty dark

TIL g
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME filled

SPR A
00100100
11111111
11111111
01011010
01111110
00100100
00100100
01111100
POS 0 4,7

SPR a
01000100
01111100
01111100
00101100
11111100
00111000
00111001
00111110
>
01000100
01111100
01111100
00101100
11111100
00111000
00111000
00111111
NAME drin
DLG SPR_0
POS 0 13,7

SPR b
00000000
00000000
01111110
01111110
01001010
01111110
00011000
00011000
NAME sign
DLG SPR_1
POS 2 6,11

SPR c
00000000
00111100
00111100
01111110
01001010
01011110
01110010
11001100
>
00000000
00111100
00111100
01110010
00011110
00111010
01100010
11001100
NAME golem1A
DLG SPR_2
POS 3 4,8

SPR d
00000000
00100100
01111110
11101011
01010110
11111111
00100100
00111100
>
00000000
00100100
01111110
11010111
01101010
11111111
00100100
00111100
NAME golem2A
DLG SPR_3
POS 3 6,5

SPR e
00000000
01111110
01011010
00111100
00100100
01100110
00100100
00111100
>
00000000
11100111
10100101
01100110
01000010
01011010
11000011
01100110
NAME golem3A
DLG SPR_4
POS 3 8,10

SPR f
00000000
01111110
01011010
00111100
01100110
01011010
00011000
00011000
>
00000000
01111110
00011000
00011000
01100110
01000010
00000000
00011000
NAME golem4A
DLG SPR_5
POS 3 10,6

SPR g
00011000
01100110
10100101
10011001
01100110
00011000
00100100
01100110
>
00111100
00100100
00100100
00100100
00011000
00000000
00011000
00011000
NAME golem5A
DLG SPR_6
POS 3 2,5

SPR h
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option1a
DLG SPR_7
POS 4 3,6

SPR i
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option1b
DLG SPR_8
POS 4 12,6

SPR j
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option2a
DLG SPR_9
POS 6 3,6

SPR k
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option2b
DLG SPR_a
POS 6 12,6

SPR l
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option3a
DLG SPR_b
POS 7 3,6

SPR m
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option3b
DLG SPR_c
POS 7 12,6

SPR n
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option4a
DLG SPR_d
POS 8 3,6

SPR o
00000000
00011000
00111100
01111110
01011010
00011000
00011000
00011000
NAME option4b
DLG SPR_e
POS 8 12,6

SPR p
00000000
01111110
01111010
01010110
01111010
01111110
00011000
00011000
>
00000000
01111110
01110110
01101010
01011110
01111110
00011000
00011000
NAME ending1
DLG SPR_f
POS 9 7,12

SPR q
00000000
01111110
01101110
01100110
01011010
01111110
00011000
00011000
>
00000000
01111110
01010110
01011010
01110110
01111110
00011000
00011000
NAME ending2
DLG SPR_g
POS 9 7,9

SPR r
00000000
01111110
01110110
01011010
01101010
01111110
00011000
00011000
>
00000000
01111110
01011010
01101110
01010110
01111110
00011000
00011000
NAME ending3
DLG SPR_h
POS 9 7,6

SPR s
00000000
00111100
00111100
01111110
01001010
01011110
01110010
11001100
>
00000000
00111100
00111100
01110010
00011110
00111010
01100010
11001100
NAME golem1B
DLG SPR_i
POS a 5,4

SPR t
00000000
00100100
01111110
11101011
01010110
11111111
00100100
00111100
>
00000000
00100100
01111110
11010111
01101010
11111111
00100100
00111100
NAME golem2B
DLG SPR_j
POS a 12,12

SPR u
00000000
01111110
01011010
00111100
00100100
01100110
00100100
00111100
>
00000000
11100111
10100101
01100110
01000010
01011010
11000011
01100110
NAME golem3B
DLG SPR_k
POS a 10,4

SPR v
00000000
01111110
01011010
00111100
01100110
01011010
00011000
00011000
>
00000000
01111110
00011000
00011000
01100110
01000010
00000000
00011000
NAME golem4B
DLG SPR_l
POS a 12,7

SPR w
00011000
01100110
10100101
10011001
01100110
00011000
00100100
01100110
>
00111100
00100100
00100100
00100100
00011000
00000000
00011000
00011000
NAME golem5B
DLG SPR_m
POS a 8,8

ITM 0
00000000
00000000
00100100
00011000
00011000
00100100
00000000
00000000
NAME null
DLG ITM_0

DLG SPR_0
"""
HEY TUG !

I had that crazy dream again last night.
I can't get it to go away. Can you please help me?
Can you go into my head and find the source of the dream?
Thanks pal! I owe you big time.
"""

DLG SPR_1
BEYOND THESE GATES LIES THE THOUGHTS AND DREAMS OF DRIN.

DLG SPR_2
hhhh...

DLG SPR_3
. . .mi amore?

DLG SPR_4
pl lea s e

DLG SPR_5
this cant be normal !

DLG SPR_6
! ! !

DLG SPR_7
the day I met her, my life changed for the worse.

DLG SPR_8
the day I met her, my life changed for the better.

DLG SPR_9
She was an ugly old hag, and yet I fell for her.

DLG SPR_a
She was a stunning girl, and I fell for her.

DLG SPR_b
Why, out of all people, did she choose to hate me?

DLG SPR_c
Why, out of all people, did she choose to love me?

DLG SPR_d
why?

DLG SPR_e
why?

DLG SPR_f
Is it because I drew you in my notebooks ?

DLG SPR_g
Is it because I stared at you every time I saw you?

DLG SPR_h
Is it because I could describe the way you smelled in perfect detail?

DLG SPR_i
hhhh...

DLG SPR_j
"""
.  .  . 

m i  a m o r e ? 
"""

DLG SPR_k
pl lea s e

DLG SPR_l
this cant be normal !

DLG SPR_m
! ! !

DLG ITM_0
null

END 0


END undefined
Let's tell Drin that we found the source of his dream. (FIN)

VAR a
42

