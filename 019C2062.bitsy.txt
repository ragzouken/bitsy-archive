zooberluft

# BITSY VERSION 2.2

! ROOM_FORMAT 1

PAL 0
253,147,176
219,83,97
245,255,174

ROOM 1
b,b,b,b,b,h,b,b,b,b,b,b,b,b,b,b
b,c,b,b,i,h,b,b,b,b,b,b,b,b,b,b
b,b,b,b,h,h,b,b,b,b,b,b,b,i,b,b
b,i,b,b,h,h,b,b,b,b,b,f,b,h,b,b
b,h,b,b,h,h,b,b,f,b,b,e,b,h,i,b
b,h,f,b,h,h,b,b,e,b,b,e,b,h,h,b
b,h,e,b,h,h,b,b,e,b,b,e,b,h,h,b
a,a,d,a,a,a,a,a,d,a,a,d,a,a,a,a
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,q,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,k,0,p,0,0,l,0,0,0,m
n,j,0,0,0,0,0,0,0,0,0,0,0,0,q,m
n,0,0,0,k,0,0,0,k,0,0,0,0,k,0,m
n,p,0,0,0,0,j,0,0,0,0,0,0,0,j,m
n,0,l,0,p,0,0,0,l,0,0,l,0,p,0,m
WAL a,n,m
EXT 15,8 2 1,8

ROOM 2
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,c,b,b,b,b,b,b,b,b,f,b,b,b,b,b
b,f,b,b,b,b,i,b,b,b,e,b,b,b,b,b
b,e,b,b,b,b,h,b,b,b,e,b,b,b,f,b
b,e,i,b,b,b,h,b,b,f,e,b,b,b,e,b
b,e,h,b,f,b,h,i,b,e,e,b,b,b,e,b
b,e,h,b,e,b,h,h,b,e,e,b,b,b,e,b
a,d,a,a,d,a,a,a,a,d,d,a,a,a,d,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,0,0,0,j,0,0,0,0,m
n,0,0,0,q,0,0,0,0,0,0,0,0,0,0,m
n,p,0,0,0,0,0,j,0,0,p,0,0,j,0,m
n,0,0,0,0,q,0,0,0,0,0,0,q,0,0,0
n,0,0,o,0,0,j,0,0,k,0,0,l,0,0,m
n,k,0,0,0,q,0,0,p,0,q,0,0,0,k,m
WAL a,n,m
EXT 15,13 3 0,13
EXT 0,8 1 15,8

ROOM 3
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
i,c,b,b,b,b,b,b,b,b,b,b,b,b,b,b
h,b,b,b,b,b,b,b,b,b,b,v,w,b,b,b
h,b,f,b,b,b,b,b,b,i,b,r,s,b,b,i
h,b,e,i,b,b,b,f,b,h,b,r,s,b,b,h
h,b,e,h,b,b,f,e,b,h,b,r,s,f,b,h
h,b,e,h,b,b,e,e,b,h,b,r,s,e,b,h
a,a,d,a,a,a,d,d,a,a,a,t,u,d,a,a
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,l,0,q,0,0,0,0,0,p,0,j,0,0,m
0,0,0,0,0,0,k,0,0,0,0,0,0,0,l,m
n,0,0,p,0,0,q,0,j,0,0,k,q,0,0,m
n,k,q,0,j,0,0,0,0,0,q,0,0,j,0,m
WAL a,n,m
EXT 15,10 4 0,10
EXT 0,13 2 15,13

ROOM 4
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,c,b,b,b,i,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,h,b,b,b,b,b,b,b,i,b,b
b,i,b,b,b,h,b,b,b,b,b,f,b,h,b,b
b,h,b,b,b,h,b,b,f,b,b,e,b,h,i,b
b,h,f,b,b,h,b,b,e,b,b,e,b,h,h,b
b,h,e,b,b,h,b,b,e,b,b,e,b,h,h,b
a,a,d,a,a,a,a,a,d,a,a,d,a,a,a,a
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
0,0,0,q,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,k,0,p,0,0,l,0,0,0,m
n,j,0,0,0,0,0,0,0,0,0,0,0,0,q,m
n,0,0,0,k,0,0,0,k,0,0,0,0,k,0,m
n,p,0,0,0,0,j,0,0,0,0,0,0,0,j,m
n,0,l,0,p,0,0,0,l,0,0,l,0,p,0,0
WAL a,n,m
EXT 15,15 5 0,15
EXT 0,10 3 15,10

ROOM 5
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,c,b,b,b,b,b,b,b,b,f,b,b,b,b,b
b,f,b,b,b,b,i,b,b,b,e,b,b,b,b,b
b,e,b,b,b,b,h,b,b,b,e,b,b,b,f,b
b,e,i,b,b,b,h,b,b,f,e,b,b,b,e,b
b,e,h,b,f,b,h,i,b,e,e,b,b,b,e,b
b,e,h,b,e,b,h,h,b,e,e,b,b,b,e,b
a,d,a,a,d,a,a,a,a,d,d,a,a,a,d,a
n,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,o,0,0,0,0,0,0,0,0,0,0,0,m
n,0,0,0,0,0,0,x,0,0,j,0,0,0,0,m
n,0,0,0,q,0,0,0,0,0,0,0,0,0,0,m
n,p,0,0,0,0,0,j,0,0,p,0,0,j,0,m
n,0,0,0,0,q,0,0,0,0,0,0,q,0,0,m
n,0,0,0,0,0,j,0,0,k,0,0,l,0,o,m
0,k,0,0,0,q,0,0,p,0,q,0,0,0,k,m
WAL a,n,m
EXT 7,10 6 7,0
EXT 0,15 4 15,15

ROOM 6
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
EXT 7,15 7 7,0
EXT 8,15 7 8,0
EXT 7,0 5 7,10
EXT 8,0 5 7,10

ROOM 7
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,b,y,y,b,b,b,b,b,b,b
b,b,b,b,b,b,y,z,z,y,b,b,b,b,b,b
b,b,b,y,y,y,z,z,z,z,y,y,y,b,b,b
b,b,y,z,z,z,z,10,10,z,z,z,z,y,b,b
b,y,z,10,10,10,10,11,11,10,10,10,10,z,y,b
y,z,10,11,11,11,11,12,12,11,11,11,11,10,z,y
z,10,11,12,12,12,12,12,12,12,12,12,12,11,10,z
10,11,12,12,0,0,0,0,0,0,0,0,12,12,11,10
11,12,12,0,0,0,0,0,0,0,0,0,0,12,12,11
12,12,0,0,0,0,0,0,0,0,0,0,0,0,12,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
EXT 15,15 8 7,0
EXT 0,15 8 7,0

ROOM 8
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,15,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,15,0,0,12
12,0,0,0,0,0,0,15,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,14,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,15,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,15,0,0,0,15,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,13,0,0,0,0,12
12,0,0,15,0,0,0,0,0,0,0,0,0,0,0,12
12,0,0,0,0,0,0,0,0,0,0,0,0,15,0,12
12,0,0,0,0,0,0,15,0,0,0,0,0,0,0,12
12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12

TIL 10
10010101
01010110
10101101
01001010
01010101
10101010
01101101
10010010

TIL 11
00000101
01010000
00100101
00000000
01010100
10000010
01000101
00010010

TIL 12
00000001
00000000
00100000
00000100
00000000
10000000
00001000
00000000

TIL 13
00000000
00000000
00000000
00000000
01100000
11110000
11110000
01111111

TIL 14
00000000
00000000
00000000
00000000
00000110
00001111
00001111
11111110

TIL 15
00000000
00000000
00111010
01000100
01000100
00111010
00000000
00000000

TIL a
01010101
10101010
01010101
10101010
00000000
00000000
00000000
00000000

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL c
11111111
11100011
11001101
11011111
11011111
11001101
11100011
11111111

TIL d
01000001
10000000
01000001
10000000
00000000
00000000
00000000
00000000

TIL e
11000001
11000001
11000001
11000001
11000001
11000001
11000001
11000001

TIL f
11111111
11111111
11111111
11110111
11110111
11100011
11000001
11000001

TIL h
11010101
11101011
11010101
11101011
11010101
11101011
11010101
11101011

TIL i
11111111
11111111
11111111
11110111
11110111
11101011
11010101
11101011

TIL j
00000000
00000000
00001000
00010000
10010000
01000010
01000001
00000001

TIL k
00000100
00001000
00001000
01000000
00100000
00100000
00000000
00000000

TIL l
10000000
01000001
01000010
00000010
00000000
00100000
00010000
00010000

TIL m
00000110
00001100
00000110
00001100
00000110
00001100
00000110
00001100

TIL n
00110000
01100000
00110000
01100000
00110000
01100000
00110000
01100000

TIL o
00000000
00000000
00011100
00111110
00101010
00111110
00010100
00000000

TIL p
00000000
00000000
01000000
00000000
00000000
00000010
00000000
00000000

TIL q
10000000
00000010
00000000
00000000
00000000
00100000
00000000
00000000

TIL r
11000000
11000000
11000000
11000000
11000000
11000000
11000000
11000000

TIL s
00000011
00000011
00000011
00000011
00000011
00000011
00000011
00000011

TIL t
01000000
10000000
01000000
10000000
00000000
00000000
00000000
00000000

TIL u
00000001
00000010
00000001
00000010
00000000
00000000
00000000
00000000

TIL v
11111111
11111110
11111100
11111000
11110000
11110000
11100000
11100000

TIL w
11111111
01111111
00111111
00011111
00001111
00001111
00000111
00000111

TIL x
10000000
00000001
00111110
01111111
01111111
00111110
00000000
01000000

TIL y
11011111
01110111
11111101
11101111
01111111
10111011
11101111
11111010

TIL z
11011101
01110110
10101101
01001011
11110111
10101010
01101111
11010010

SPR A
00011000
00011000
00010000
00111100
01011010
00011000
00100100
00100100
POS 1 1,8

SPR b
00011000
00011000
01001010
00111100
00011000
00011000
00100100
00100100
>
00011000
00011000
00001000
00111100
01011010
00011000
00100100
00100100
POS 1 9,12

SPR c
00011000
00011100
00001000
00011100
00011010
00011000
00100100
00100100
POS 1 12,14

SPR d
00000000
00011000
00111000
00010000
00111110
01011000
00111100
01101100
POS 2 2,14

SPR e
00011000
00111100
00011000
00001000
00111100
01011010
00011000
01100110
POS 2 12,9

SPR f
00011100
00011000
01001010
00111100
00011000
00011000
00100100
00100100
POS 3 13,14

SPR g
00011000
00011000
00010000
00111100
01011010
00011000
00100100
00100010
>
00011000
00011000
00010000
00111100
01011010
00011000
00100100
01000100
POS 3 2,9

SPR h
00011000
00011100
00001000
00111100
01011010
00011000
00100100
00100010
>
00011000
00011100
00001000
00111100
01011010
00011000
00100100
01000100
POS 3 4,9

SPR i
00011000
00011000
00001000
00111100
01011010
00011000
00100100
01000100
>
00011000
00011000
00001000
00111100
01011010
00011000
00100100
00100010
POS 3 5,10

SPR j
00011000
00011000
00001000
00111100
01011010
00011000
00100100
00100100
POS 3 5,8

SPR k
00011000
00011000
00011000
00111100
01011010
00011000
00100100
00100100
POS 4 5,9

SPR l
00011000
00011100
00001000
00111100
01011010
00011000
00100100
00100100
POS 4 11,13

SPR m
00000000
00000000
00011000
00011000
00001000
00111100
00011000
00100100
POS 4 12,14

SPR n
00000000
00011000
00011000
00010000
00111100
01011010
00111000
01101100
POS 5 6,10

SPR o
00001111
00011111
00110011
00110011
00111111
00011111
00001101
00001101
POS 7 7,14

SPR p
11110000
11111000
11001100
11001100
11111100
11111000
10110000
10110000
POS 7 8,14

SPR q
00000000
00000000
00011100
00111000
00111100
01111000
00111000
00101000
POS 7 5,14

SPR r
00000000
00000000
00111000
00011100
00111100
00011110
00011100
00010100
POS 7 10,14

SPR s
00100100
00111100
00010100
00111100
00011000
00111101
00111101
00111111
POS 8 11,11

SPR t
01001000
01111000
01010000
01111000
00110011
01111001
01111001
01111111
>
01001000
01111000
01010000
01111000
00110011
01111010
01111010
01111110
POS 8 3,6

DLG a
I'm a cat

DLG b
I'm outraged!  Totally outraged!  Completely and utterly outraged!

DLG c
I lost my arm in the war, but at least my hair is still nice.

DLG d
Oh, Gerald... you fool...

DLG e
I used to be a ranger... but not anymore.

DLG f
This place... it's way too damn small!!  How am I supposed to maintain my posy collection...

DLG g
Chin up!  Even if we're here, physical fitness is important.

DLG h
If you say so...

DLG i
I feel a bit silly doing this.

DLG j
I'm taking a break.

DLG k
Hoo... sorry, friend.  I do believe the end's up there somewhere.  Have a lemon for your troubles.

DLG l
You know, we can still be happy here.  Never lose hope!

DLG m
My mommy's smarter than you.

DLG n
Yo, check out my hole! ... I think I hear something from the other side!

DLG o
... My child ... you have found me ... This is ... miraculous.  I have been buried deep within the ground for centuries.  How did you ...?  A hole?  ... Extraordinary ... For this, I will grant you one wish. ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... So be it.  They will be free come sunrise.

DLG p
... My child ... you have found me ... This is ... miraculous.  I have been buried deep within the ground for centuries.  How did you ...?  A hole?  ... Extraordinary ... For this, I will grant you one wish. ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... ... So be it.  They will be free come sunrise.

DLG r
Traveled across the lands... one day he stepped on his neighbor's lawn and was banished.

DLG q
Lived for centuries... despite trying our best to serve him, he's still full of dust...

DLG s
What?  You found me?  Shoot, I thought I was hiding really well...  Don't tell anyone, okay?

DLG t
Meow.  That guy on the upper floor stepped on my friend's lawn once... she sure showed him!!

