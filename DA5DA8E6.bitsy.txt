
"Fanciful Time"

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME default
29,30,33
243,37,130
28,215,187

PAL 1
NAME muted
31,51,79
168,146,138
253,251,225

PAL 2
NAME end
247,247,247
18,29,36
29,240,250

PAL 3
NAME M END
29,30,33
28,215,187
255,255,255

ROOM 0
j,j,j,j,j,0,0,0,k,k,k,j,j,j,j,j
j,j,j,j,0,0,0,0,0,k,k,k,k,k,j,j
j,j,j,0,0,0,0,0,0,0,0,k,k,k,k,j
j,j,0,0,0,0,0,g,i,i,h,0,k,k,k,j
j,j,0,0,a,0,0,d,d,d,d,0,0,k,k,j
j,0,0,a,a,a,0,d,d,d,d,0,0,0,k,k
j,0,0,0,a,0,0,d,e,e,d,0,0,0,0,k
j,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,k,0,f,f,e,a,b,b,a,e,f,f,0,0,0
j,k,0,f,f,e,a,b,b,a,e,f,f,0,0,0
j,k,k,0,0,0,0,0,0,0,0,0,0,c,0,0
j,k,k,k,k,0,0,0,0,0,0,0,c,0,c,0
j,j,k,k,k,k,k,0,0,0,0,0,0,c,0,j
j,j,j,j,j,k,k,k,k,0,0,0,0,0,j,j
j,j,j,j,j,j,j,j,j,k,k,0,0,j,j,j
NAME C2
EXT 5,0 1 5,15
EXT 6,0 1 6,15
EXT 8,0 1 8,15
EXT 9,0 1 9,15
EXT 10,0 1 10,15
EXT 7,0 1 7,15
EXT 8,6 2 8,6
EXT 9,6 2 9,6
EXT 15,5 3 0,5
EXT 15,6 3 0,6
EXT 15,7 3 0,7
EXT 15,8 3 0,8
EXT 15,9 3 0,9
EXT 15,10 3 0,10
EXT 15,11 3 0,11
EXT 15,12 3 0,12
EXT 9,15 g 9,0
EXT 10,15 g 10,0
EXT 11,15 g 11,0
EXT 12,15 g 12,0
PAL 0

ROOM 1
d,d,j,j,j,d,j,j,z,z,z,z,j,j,d,d
d,d,d,j,j,d,j,k,k,p,0,p,k,j,d,d
d,d,d,j,d,j,j,k,k,p,0,0,k,0,j,j
d,j,j,j,j,k,k,k,k,k,p,0,p,k,k,j
d,j,d,j,k,k,k,k,k,k,p,0,0,k,k,j
j,d,d,j,k,k,k,k,k,p,0,0,0,0,k,0
d,j,j,j,k,p,p,k,k,p,p,0,0,0,0,0
d,d,j,p,p,p,0,p,p,p,p,p,0,0,0,p
d,j,j,p,p,0,0,p,0,0,0,0,0,p,p,k
d,j,j,p,0,0,0,0,0,p,0,0,0,p,k,k
j,j,j,p,0,0,0,0,0,0,0,0,p,k,k,j
j,j,j,p,0,0,0,0,0,0,p,p,k,k,k,j
j,j,d,0,0,0,0,0,0,0,p,k,k,k,j,j
j,j,j,p,p,0,0,0,0,p,p,k,k,j,d,d
j,d,j,k,p,p,0,0,p,p,k,k,j,j,j,d
j,j,j,j,j,0,0,0,0,0,0,j,j,d,j,j
NAME B2
ITM 0 10,1
EXT 5,15 0 5,0
EXT 6,15 0 6,0
EXT 7,15 0 7,0
EXT 8,15 0 8,0
EXT 9,15 0 9,0
EXT 10,15 0 10,0
EXT 15,5 7 0,5
EXT 15,6 7 0,6
EXT 15,7 7 0,7
EXT 15,8 7 0,8
EXT 15,9 7 0,9
PAL 0

ROOM 2
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,m,m,m,m,m,m,c,c,c,c
c,c,c,c,c,m,m,m,m,m,m,m,m,c,c,c
c,c,c,c,c,m,m,g,i,i,h,m,m,c,c,c
c,c,c,c,c,m,m,e,e,e,e,m,m,c,c,c
c,c,c,c,c,m,m,e,e,e,e,m,m,c,c,c
c,c,c,c,c,m,m,e,f,f,e,m,m,c,c,c
c,c,c,c,c,m,m,m,0,0,m,m,m,c,c,c
c,c,c,c,c,c,m,m,m,m,m,m,c,c,c,c
c,c,c,c,c,c,c,c,m,m,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
0,c,0,c,0,0,c,0,c,0,c,0,0,c,0,0
0,c,0,c,0,0,c,0,c,0,c,0,c,c,0,c
0,0,0,c,0,0,c,0,c,0,c,0,0,c,0,0
0,c,0,c,0,0,c,0,c,0,c,c,0,c,0,c
0,c,0,c,0,0,c,0,0,0,c,0,0,c,0,0
NAME C2a
EXT 8,7 0 8,7
EXT 9,7 0 9,7
PAL 1

ROOM 3
j,j,j,j,j,j,j,j,j,j,r,0,0,0,0,o
j,j,j,j,j,l,l,l,l,l,l,0,0,0,0,o
j,j,k,l,l,l,l,l,l,12,0,0,0,0,r,l
j,k,k,q,q,q,q,l,l,l,l,l,l,l,l,l
j,p,p,p,0,0,0,q,q,l,l,l,l,l,l,l
p,p,0,0,0,0,0,0,0,q,q,q,l,l,l,l
p,0,0,0,0,p,0,0,p,0,0,0,q,q,q,q
0,0,0,0,p,0,0,k,0,p,k,p,p,0,0,0
0,0,0,0,0,p,0,p,k,k,k,k,k,0,p,0
0,0,0,0,0,0,k,p,k,k,t,k,k,p,0,p
0,0,0,0,k,k,0,p,p,k,k,k,k,k,p,o
k,k,k,k,k,0,p,p,p,0,0,k,p,p,0,o
k,k,p,k,0,0,0,0,p,0,0,p,p,p,o,o
j,k,k,k,p,0,0,0,0,0,0,0,p,0,o,o
j,j,j,j,j,j,p,0,0,0,0,0,0,o,o,o
j,j,j,j,j,j,j,j,u,j,j,o,o,o,o,o
NAME C3
ITM 0 13,2
EXT 0,5 0 15,5
EXT 0,6 0 15,6
EXT 0,7 0 15,7
EXT 0,8 0 15,8
EXT 0,9 0 15,9
EXT 0,10 0 15,10
EXT 0,11 0 15,11
EXT 0,12 0 15,12
EXT 8,15 a 8,0
EXT 15,6 4 0,6
EXT 15,7 4 0,7
EXT 15,8 4 0,8
EXT 15,9 4 0,9
EXT 9,2 b 9,2
EXT 10,0 7 10,15
EXT 11,0 7 11,15
EXT 12,0 7 12,15
EXT 13,0 7 13,15
EXT 14,0 7 14,15
PAL 0

ROOM 4
o,o,19,l,l,l,l,l,l,0,0,0,0,0,0,o
o,l,19,l,l,l,l,l,l,0,0,0,0,0,0,o
l,l,19,l,l,l,l,q,q,0,0,0,0,0,0,o
l,l,19,l,q,q,q,0,0,0,0,0,0,0,r,o
l,l,0,q,0,0,0,0,p,0,p,0,0,0,l,o
l,q,0,0,0,p,0,0,0,0,0,0,p,0,l,l
q,0,0,0,p,0,0,p,p,p,0,0,p,0,l,l
0,0,0,0,0,0,p,p,p,0,0,0,0,0,l,l
0,0,0,0,p,0,0,0,0,0,0,p,0,0,l,l
p,p,0,0,0,p,0,p,0,p,0,0,0,0,l,l
o,p,p,0,0,0,0,0,0,0,0,0,r,r,l,l
o,o,p,p,0,0,r,r,r,r,r,r,l,l,l,l
o,o,o,p,r,r,l,l,l,l,l,l,l,l,l,l
o,o,o,o,l,l,l,l,l,l,l,l,l,l,l,o
o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o
o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o
NAME C4
ITM 3 2,4
EXT 11,0 5 11,15
EXT 10,0 5 10,15
EXT 9,0 5 9,15
EXT 0,6 3 15,6
EXT 0,7 3 15,7
EXT 0,8 3 15,8
EXT 0,9 3 15,9
EXT 2,0 b 13,15
PAL 0

ROOM 5
l,l,l,z,z,0,z,z,z,s,z,z,z,z,v,y
l,0,l,l,l,z,z,0,z,0,z,z,w,x,z,z
l,0,q,l,l,l,l,z,z,0,z,p,v,y,p,z
z,0,0,q,l,l,l,z,z,0,z,0,z,z,z,z
w,x,0,0,q,q,l,z,z,t,z,k,z,w,x,p
v,y,k,0,0,0,q,z,z,10,z,z,k,v,y,z
z,k,k,p,0,0,p,z,z,0,w,x,0,p,w,x
p,k,k,k,0,0,p,p,z,0,v,y,0,k,v,y
p,k,0,0,0,0,0,p,z,0,z,0,w,x,z,z
p,0,0,p,p,0,0,0,z,0,z,0,v,y,p,z
p,0,0,0,z,p,0,0,p,0,z,0,p,p,z,z
r,r,0,0,0,z,0,0,0,0,z,0,0,p,z,o
l,l,r,p,0,0,z,z,z,0,p,0,0,p,z,o
o,l,l,z,z,p,0,0,0,0,0,0,0,z,z,o
o,l,l,w,x,z,k,p,0,p,0,0,k,z,k,o
o,l,l,v,y,z,z,z,z,0,0,0,o,o,o,o
NAME B4
ITM 1 1,1
ITM 0 11,8
ITM 2 9,6
EXT 9,15 4 9,0
EXT 10,15 4 10,0
EXT 9,5 6 9,5
EXT 0,11 7 15,11
EXT 0,10 7 15,10
EXT 0,9 7 15,9
EXT 0,8 7 15,8
EXT 0,7 7 15,7
EXT 11,15 4 11,0
PAL 0

ROOM 6
l,l,l,z,z,0,z,z,z,s,z,z,z,z,v,y
l,q,l,l,l,z,z,0,z,0,z,z,w,x,z,z
l,0,q,l,l,l,l,z,z,0,z,p,v,y,p,z
z,1a,0,q,l,l,l,z,z,0,z,p,z,z,z,z
w,x,0,0,q,q,l,z,z,0,z,p,z,w,x,p
v,y,p,0,0,0,q,z,z,f,z,z,1a,v,y,z
z,1a,1a,1a,0,0,0,z,z,0,w,x,0,1a,w,x
p,1a,1a,0,0,0,0,1a,z,0,v,y,0,0,v,y
p,1a,0,0,0,0,0,0,z,0,z,0,w,x,z,z
p,0,0,1a,p,1a,0,0,z,0,z,0,v,y,0,z
p,0,0,0,1a,1a,1a,0,p,0,z,z,0,1a,z,z
r,r,0,0,0,z,1a,0,0,0,z,0,0,0,z,o
l,l,r,0,0,0,z,z,z,0,0,0,0,1a,z,o
o,l,l,z,z,p,0,0,0,0,0,0,0,z,z,o
o,l,l,w,x,z,p,1a,0,0,0,0,1a,z,0,o
o,l,l,v,y,z,z,z,z,f,f,f,o,o,o,o
NAME B4 b
EXT 0,7 h 15,7
EXT 0,8 h 15,8
EXT 0,9 h 15,9
EXT 0,10 h 15,10
EXT 0,11 h 15,11
EXT 9,15 c 9,0
EXT 10,15 c 10,0
EXT 11,15 c 11,0
END 0 9,0
PAL 2

ROOM 7
l,l,l,l,l,l,l,l,l,l,l,l,l,l,l,l
l,l,l,l,l,l,l,l,l,l,l,l,l,l,l,l
l,l,l,l,l,l,l,l,q,q,q,q,q,l,l,l
l,l,l,l,l,l,l,q,0,0,w,x,0,z,z,z
l,l,l,l,l,l,l,0,0,0,v,y,0,0,z,p
q,q,q,l,l,l,l,0,0,0,0,0,0,w,x,z
0,0,0,q,l,l,l,r,0,0,0,0,0,v,y,z
0,0,0,0,q,l,l,l,r,r,0,0,0,0,0,0
0,0,0,0,0,q,q,l,l,l,r,r,0,0,0,0
0,p,0,0,0,0,0,q,q,l,l,l,0,0,0,0
j,j,p,p,0,0,0,0,0,q,q,l,r,r,0,0
j,j,j,j,p,p,0,0,0,0,0,q,l,l,r,r
j,j,j,j,j,j,p,0,0,0,0,0,q,l,l,l
j,j,j,j,j,j,j,p,p,0,0,0,0,q,o,o
d,d,j,j,j,j,j,j,j,p,0,0,0,0,0,o
d,d,d,j,d,j,j,j,j,j,p,0,0,0,0,o
NAME B3
ITM 0 13,4
EXT 0,5 1 15,5
EXT 0,6 1 15,6
EXT 0,7 1 15,7
EXT 0,8 1 15,8
EXT 0,9 1 15,9
EXT 10,15 3 10,0
EXT 11,15 3 11,0
EXT 12,15 3 12,0
EXT 13,15 3 13,0
EXT 14,15 3 14,0
EXT 15,7 5 0,7
EXT 15,8 5 0,8
EXT 15,9 5 0,9
EXT 15,10 5 0,10
EXT 15,11 5 0,11
PAL 0

ROOM a
0,0,0,0,11,11,11,11,0,11,11,11,0,0,0,0
0,0,0,11,11,11,0,0,0,0,11,11,11,0,0,0
0,0,11,11,11,0,0,0,0,0,0,11,11,11,0,0
0,0,11,11,0,0,0,0,0,0,0,0,11,11,0,0
0,11,11,0,0,0,0,0,0,0,0,0,0,11,11,0
0,11,11,0,0,0,0,0,0,0,0,0,0,11,11,0
11,11,0,0,0,0,0,0,0,0,0,0,0,0,11,11
11,11,0,0,0,0,0,0,0,0,0,0,0,0,11,11
11,0,0,0,0,0,0,0,0,0,0,0,0,0,0,11
11,11,0,0,0,0,0,s,s,0,0,0,0,0,11,11
11,11,11,0,0,0,0,0,0,0,0,0,0,11,11,11
0,11,11,11,11,11,11,11,11,11,11,11,11,11,11,0
0,0,11,11,11,11,11,11,11,11,11,11,11,11,0,0
0,0,0,0,0,0,0,11,11,0,0,0,0,0,0,0
0,0,0,0,0,0,0,11,11,0,0,0,0,0,0,0
0,0,0,0,0,0,0,11,11,0,0,0,0,0,0,0
NAME D3
EXT 8,0 3 8,15
END undefined 7,9
END undefined 8,9
PAL 3

ROOM b
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,m,m,m,m,m,m,m,0,13,0,m,m,m,m,n
n,m,m,m,m,m,m,m,14,14,14,m,m,m,m,n
n,m,m,m,m,m,m,m,10,10,10,m,m,m,m,n
n,m,m,m,m,m,m,m,10,10,10,m,m,m,m,n
n,10,10,10,10,10,10,10,10,10,10,m,m,m,m,n
n,10,10,10,10,10,10,10,10,10,10,m,m,m,m,n
n,10,10,10,m,m,m,m,m,m,m,m,m,m,m,n
n,10,10,10,10,10,10,10,10,10,10,10,10,10,10,n
n,10,10,10,10,10,10,10,10,10,10,10,10,10,10,n
n,m,m,m,m,m,m,m,m,m,m,m,10,10,10,n
n,m,m,m,m,m,m,m,m,m,m,m,10,10,10,n
n,m,m,m,m,m,m,m,m,m,m,m,10,10,10,n
n,m,m,m,m,m,m,m,m,m,m,m,10,10,10,n
n,n,n,n,n,n,n,n,n,n,n,n,n,14,n,n
NAME CAVE
ITM 0 1,8
EXT 9,2 3 9,2
EXT 13,15 4 2,0
PAL 1

ROOM c
o,o,19,l,l,l,l,l,l,0,0,0,o,o,o,o
o,l,19,l,l,l,l,l,l,0,0,0,0,o,o,o
l,l,19,l,l,l,l,q,q,0,0,0,0,0,o,o
l,l,19,l,q,q,q,0,0,0,0,0,0,0,r,o
l,l,0,q,0,0,0,0,p,0,p,0,0,0,l,o
l,q,0,0,0,p,0,0,0,0,0,0,p,0,l,l
q,0,0,0,p,0,0,p,p,p,0,0,p,0,l,l
0,0,0,0,0,0,p,p,p,0,0,0,0,0,l,l
0,0,0,0,p,0,0,0,0,0,0,p,0,0,l,l
p,p,0,0,0,p,0,p,0,p,0,0,0,0,l,l
o,p,p,0,0,0,0,0,0,0,0,0,r,r,l,l
o,o,p,p,0,0,r,r,r,r,r,r,l,l,l,l
o,o,o,p,r,r,l,l,l,l,l,l,l,l,l,l
o,o,o,o,l,l,l,l,l,l,l,l,l,l,l,o
o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o
o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o
NAME C4 b
EXT 11,0 6 11,15
EXT 10,0 6 10,15
EXT 9,0 6 9,15
EXT 0,6 f 15,6
EXT 0,7 f 15,7
EXT 0,8 f 15,8
EXT 0,9 f 15,9
EXT 2,0 e 13,15
PAL 2

ROOM d
10,10,14,10,10,f,f,c,c,c,c,c,c,10,10,10
10,10,10,10,f,f,c,c,c,c,c,c,c,c,c,10
10,10,f,f,f,f,c,c,c,c,c,c,c,c,c,10
10,f,f,f,f,f,c,e,c,c,c,c,c,c,c,c
10,f,f,f,f,c,c,c,c,c,c,c,s,c,c,c
f,f,f,f,f,c,c,c,c,c,c,c,c,c,c,c
f,f,f,f,f,c,r,c,c,c,c,c,r,c,c,c
f,f,f,f,f,c,c,r,r,r,r,r,c,c,c,c
f,f,f,f,f,c,c,c,c,c,c,c,c,c,c,c
f,f,f,f,f,f,c,c,c,c,c,c,c,c,c,10
10,f,f,f,f,f,f,c,c,c,c,c,c,c,c,10
10,10,f,f,f,f,f,f,c,c,c,c,c,c,10,10
10,10,10,10,f,f,f,f,f,c,c,c,c,10,10,10
10,10,10,10,10,f,f,f,f,f,f,f,10,10,10,10
10,10,10,10,16,15,17,10,10,10,16,15,17,10,10,10
10,10,10,16,15,15,17,10,10,10,16,15,c,17,10,10
NAME SECRET END
END undefinee 12,4
PAL 2

ROOM e
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
n,s,s,s,s,s,s,s,s,13,s,s,s,s,s,n
n,s,s,s,s,s,s,s,e,e,e,s,s,s,s,n
n,s,s,s,s,s,s,s,f,f,f,s,s,s,s,n
n,s,s,s,s,s,s,s,f,f,f,s,s,s,s,n
n,f,f,f,f,f,f,f,f,f,f,s,s,s,s,n
n,f,f,f,f,f,f,f,f,f,f,s,s,s,s,n
n,f,f,f,s,s,s,s,s,s,s,s,s,s,s,n
n,f,f,f,f,f,f,f,f,f,f,f,f,f,f,n
n,f,f,f,f,f,f,f,f,f,f,f,f,f,f,n
n,s,s,s,s,s,s,s,s,s,s,s,f,f,f,n
n,s,s,s,s,s,s,s,s,s,s,s,f,f,f,n
n,s,s,s,s,s,s,s,s,s,s,s,f,f,f,n
n,s,s,s,s,s,s,s,s,s,s,s,f,f,f,n
n,n,n,n,n,n,n,n,n,n,n,n,n,e,n,n
NAME CAVE b
EXT 9,2 f 9,2
EXT 13,15 c 2,0
PAL 2

ROOM f
j,j,j,j,j,j,j,j,j,j,r,0,0,0,0,o
j,j,j,j,j,l,l,l,l,l,l,0,0,0,0,o
j,j,k,l,l,l,l,l,l,12,0,0,0,0,r,l
j,k,k,q,q,q,q,l,l,l,l,l,l,l,l,l
j,p,p,p,0,0,0,q,q,l,l,l,l,l,l,l
p,p,0,0,0,0,0,0,0,q,q,q,l,l,l,l
p,0,0,0,0,p,0,0,p,0,0,0,q,q,q,q
0,0,0,0,p,0,0,k,0,p,k,p,p,0,0,0
0,0,0,0,0,p,0,p,k,k,k,k,k,0,p,0
0,0,0,0,0,0,k,p,k,k,t,k,k,p,0,p
0,0,0,0,k,k,0,p,p,k,k,k,k,k,p,o
k,k,k,k,k,0,p,p,p,0,0,k,p,p,0,o
k,k,p,k,0,0,0,0,p,0,0,p,p,p,o,o
j,k,k,k,p,0,0,0,0,0,0,0,p,0,o,o
j,j,j,j,j,j,p,0,0,0,0,0,0,o,o,o
j,j,j,j,j,j,j,j,u,j,j,o,o,o,o,o
NAME C3 b
EXT 0,5 k 15,5
EXT 0,6 k 15,6
EXT 0,7 k 15,7
EXT 0,8 k 15,8
EXT 0,9 k 15,9
EXT 0,10 k 15,10
EXT 0,11 k 15,11
EXT 0,12 k 15,12
EXT 8,15 d 2,0
EXT 15,6 c 0,6
EXT 15,7 c 0,7
EXT 15,8 c 0,8
EXT 15,9 c 0,9
EXT 9,2 e 9,2
EXT 10,0 h 10,15
EXT 11,0 h 11,15
EXT 12,0 h 12,15
EXT 13,0 h 13,15
EXT 14,0 h 14,15
PAL 2

ROOM g
j,j,j,j,j,j,j,j,j,0,0,0,0,j,j,d
j,d,j,j,j,j,p,0,0,0,0,0,0,k,j,j
d,d,j,j,p,p,0,0,0,0,0,0,0,k,j,j
j,j,j,k,p,p,0,0,0,0,p,0,0,k,k,j
j,j,j,k,p,0,0,r,r,r,r,0,p,k,k,j
j,j,j,k,p,0,r,l,l,l,l,r,p,k,k,j
d,j,k,k,0,0,l,l,l,l,l,l,0,k,k,j
d,j,k,k,0,0,l,l,l,l,l,l,0,k,k,j
d,j,j,k,p,0,q,l,l,l,l,q,0,0,k,j
j,j,j,k,k,0,0,q,q,q,q,0,0,k,k,j
j,j,j,k,k,0,0,0,0,0,0,0,p,k,j,j
j,j,j,j,k,p,p,0,0,0,0,0,k,k,j,j
j,d,d,j,k,k,k,p,0,0,p,p,k,j,j,j
j,d,j,j,j,j,k,k,k,k,k,j,j,j,j,j
j,d,d,j,j,j,j,j,j,j,j,j,j,d,j,d
j,j,j,j,j,d,d,d,d,d,j,j,j,d,d,d
NAME D2
EXT 12,0 0 12,15
EXT 11,0 0 11,15
EXT 10,0 0 10,15
EXT 9,0 0 9,15
PAL 0

ROOM h
l,l,l,l,l,l,l,l,l,l,l,l,l,l,l,l
l,l,l,l,l,l,l,l,l,l,l,l,l,l,l,l
l,l,l,l,l,l,l,l,q,q,q,q,q,l,l,l
l,l,l,l,l,l,l,q,0,0,w,x,0,z,z,z
l,l,l,l,l,l,l,0,0,0,v,y,a,0,z,p
q,q,q,l,l,l,l,0,0,0,0,0,0,w,x,z
0,0,0,q,l,l,l,r,0,0,0,0,0,v,y,z
0,0,0,0,q,l,l,l,r,r,0,0,0,0,0,0
0,0,0,0,0,q,q,l,l,l,r,r,0,0,0,0
0,p,0,0,0,0,0,q,q,l,l,l,0,0,0,0
j,j,p,p,0,0,0,0,0,q,q,l,r,r,0,0
j,j,j,j,p,p,0,0,0,0,0,q,l,l,r,r
j,j,j,j,j,j,p,0,0,0,0,0,q,l,l,l
j,j,j,j,j,j,j,p,p,0,0,0,0,q,o,o
d,d,j,j,j,j,j,j,j,p,0,0,0,0,0,o
d,d,d,j,d,j,j,j,j,j,p,0,0,0,0,o
NAME B3 b
EXT 0,5 j 15,5
EXT 0,6 j 15,6
EXT 0,7 j 15,7
EXT 0,8 j 15,8
EXT 0,9 j 15,9
EXT 10,15 f 10,0
EXT 11,15 f 11,0
EXT 12,15 f 12,0
EXT 13,15 f 13,0
EXT 14,15 f 14,0
EXT 15,7 6 0,7
EXT 15,8 6 0,8
EXT 15,9 6 0,9
EXT 15,10 6 0,10
EXT 15,11 6 0,11
EXT 12,4 i 10,6
PAL 2

ROOM i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,14,14,14,14,14,14,14,14,14,14,14,14,14,14,0
0,14,14,14,14,14,14,14,14,14,14,14,14,14,14,0
0,14,b,b,b,b,b,b,b,b,b,b,b,b,14,0
0,14,b,a,a,a,a,a,a,a,a,a,a,b,14,0
0,14,b,a,d,d,d,d,d,d,d,d,a,b,14,0
0,14,b,a,d,0,0,0,0,0,f,d,a,b,14,0
0,14,b,a,d,0,0,0,0,0,0,d,a,b,14,0
0,14,b,a,d,13,0,0,0,0,0,d,a,b,14,0
0,14,b,a,d,d,d,d,d,d,d,d,a,b,14,0
0,14,b,a,a,a,a,a,a,a,a,a,a,b,14,0
0,14,b,b,b,b,b,b,b,b,b,b,b,b,14,0
0,14,14,14,14,14,14,14,14,14,14,14,14,14,14,0
0,14,14,14,14,14,14,14,14,14,14,14,14,14,14,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME PEANUT BUTTER
EXT 5,8 h 12,4
PAL 2

ROOM j
d,d,j,j,j,d,j,j,z,18,18,z,j,j,d,d
d,d,d,j,j,d,j,k,k,p,0,p,k,j,d,d
d,d,d,j,d,j,j,k,k,p,0,0,k,0,j,j
d,j,j,j,j,k,k,k,k,k,p,0,p,k,k,j
d,j,d,j,k,k,k,k,k,k,p,0,0,k,k,j
j,d,d,j,k,k,k,k,k,p,0,0,0,0,k,0
d,j,j,j,k,p,p,k,k,p,p,0,0,0,0,0
d,d,j,p,p,p,0,p,p,p,p,p,0,0,0,p
d,j,j,p,p,0,0,p,0,0,0,0,0,p,p,k
d,j,j,p,0,0,0,0,0,p,0,0,0,p,k,k
j,j,j,p,0,0,0,0,0,0,0,0,p,k,k,j
j,j,j,p,0,0,0,0,0,0,p,p,k,k,k,j
j,j,d,0,0,0,0,0,0,0,p,k,k,k,j,j
j,j,j,p,p,0,0,0,0,p,p,k,k,j,d,d
j,d,j,k,p,p,0,0,p,p,k,k,j,j,j,d
j,j,j,j,j,0,0,0,0,0,0,j,j,d,j,j
NAME B2 b
EXT 5,15 k 5,0
EXT 6,15 k 6,0
EXT 7,15 k 7,0
EXT 8,15 k 8,0
EXT 9,15 k 9,0
EXT 10,15 k 10,0
EXT 15,5 h 0,5
EXT 15,6 h 0,6
EXT 15,7 h 0,7
EXT 15,8 h 0,8
EXT 15,9 h 0,9
EXT 9,0 m 9,15
EXT 10,0 m 10,15
PAL 2

ROOM k
j,j,j,j,j,0,0,0,k,k,k,j,j,j,j,j
j,j,j,j,0,0,0,0,0,k,k,k,k,k,j,j
j,j,j,0,0,0,0,0,0,0,0,k,k,k,k,j
j,j,0,0,0,0,0,g,i,i,h,0,k,k,k,j
j,j,0,0,a,0,0,d,d,d,d,0,0,k,k,j
j,0,0,a,19,a,0,d,d,d,d,0,0,0,k,k
j,0,0,0,a,0,0,d,e,e,d,0,0,0,0,k
j,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
j,k,0,f,f,e,a,b,b,a,e,f,f,0,0,0
j,k,0,f,f,e,a,b,b,a,e,f,f,0,0,0
j,k,k,0,0,0,0,0,0,0,0,0,0,c,0,0
j,k,k,k,k,0,0,0,0,0,0,0,c,0,c,0
j,j,k,k,k,k,k,0,0,0,0,0,0,c,0,j
j,j,j,j,j,k,k,k,k,0,0,0,0,0,j,j
j,j,j,j,j,j,j,j,j,k,k,0,0,j,j,j
NAME C2 b
ITM 4 4,6
EXT 5,0 j 5,15
EXT 6,0 j 6,15
EXT 8,0 j 8,15
EXT 9,0 j 9,15
EXT 10,0 j 10,15
EXT 7,0 j 7,15
EXT 15,5 f 0,5
EXT 15,6 f 0,6
EXT 15,7 f 0,7
EXT 15,8 f 0,8
EXT 15,9 f 0,9
EXT 15,10 f 0,10
EXT 15,11 f 0,11
EXT 15,12 f 0,12
EXT 9,15 l 9,0
EXT 10,15 l 10,0
EXT 11,15 l 11,0
EXT 12,15 l 12,0
EXT 4,5 0 3,5
PAL 2

ROOM l
j,j,j,j,j,j,j,j,j,0,0,0,0,j,j,d
j,d,j,j,j,j,p,0,0,0,0,0,0,k,j,j
d,d,j,j,p,p,0,0,0,0,0,0,0,k,j,j
j,j,j,k,p,p,0,0,0,0,p,0,0,k,k,j
j,j,j,k,p,0,0,r,r,r,r,0,p,k,k,j
j,j,j,k,p,0,r,0,0,0,0,r,p,k,k,j
d,j,k,k,0,0,0,0,0,0,0,0,0,k,k,j
d,j,k,k,0,0,0,0,0,0,0,0,0,k,k,j
d,j,j,k,p,0,q,0,0,0,0,q,0,0,k,j
j,j,j,k,k,0,0,q,q,q,q,0,0,k,k,j
j,j,j,k,k,0,0,0,0,0,0,0,p,k,j,j
j,j,j,j,k,p,p,0,0,0,0,0,k,k,j,j
j,d,d,j,k,k,k,p,0,0,p,p,k,j,j,j
j,d,j,j,j,j,k,k,k,k,k,j,j,j,j,j
j,d,d,j,j,j,j,j,j,j,j,j,j,d,j,d
j,j,j,j,j,d,d,d,d,d,j,j,j,d,d,d
NAME D2 b
EXT 12,0 k 12,15
EXT 11,0 k 11,15
EXT 10,0 k 10,15
EXT 9,0 k 9,15
PAL 2

ROOM m
z,z,0,0,z,z,z,z,z,z,z,z,z,z,0,z
z,z,z,z,z,z,z,z,z,z,0,0,z,z,z,z
0,z,z,0,z,z,z,z,z,z,z,z,z,z,z,z
z,z,z,z,z,z,z,z,w,x,z,z,z,z,0,z
z,z,z,z,z,0,0,0,v,y,z,0,z,z,z,z
z,z,z,0,z,w,x,0,0,0,z,0,0,w,x,z
z,z,z,0,z,v,y,0,0,0,z,z,0,v,y,z
0,z,z,z,z,0,0,0,0,0,z,0,z,z,z,0
z,z,0,z,0,0,0,0,0,0,0,z,z,z,z,z
0,z,z,0,z,0,0,0,0,0,0,z,z,z,z,z
z,z,z,0,z,z,0,0,0,0,0,z,0,0,z,0
z,0,0,0,z,0,0,0,0,0,0,0,0,0,0,z
z,w,x,0,z,0,0,0,0,0,0,0,w,x,0,0
z,v,y,z,z,0,0,0,0,0,0,0,v,y,z,0
z,0,z,z,z,w,x,0,0,0,0,0,0,z,w,x
z,z,0,0,0,v,y,z,z,0,0,z,z,z,v,y
NAME A2 b
EXT 9,15 j 9,0
EXT 10,15 j 10,0
PAL 2

TIL 10
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL 11
00111000
01000100
01000100
10000010
10000010
01111100
00010000
00001000
>
00011100
00100010
00100010
01000001
01000001
00111110
00001000
00010000
WAL true

TIL 12
11111111
10000111
10000101
10011101
10010101
11110101
11010101
11111111

TIL 13
01110000
01010000
01011100
01010100
01110111
01011101
01000111
01000001

TIL 14
11111111
10101010
11111111
01010101
11111111
10101010
11111111
01010101

TIL 15
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101
>
00000000
00000000
00100100
00000000
00000000
00100100
00000000
00000000

TIL 16
00000000
00000000
00100100
00000000
00000000
00100100
00000000
00000000
>
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL 17
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
>
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL 18
00011000
00011000
00100100
00100100
01000110
01000110
10000101
11111111

TIL 19
10000000
00001000
00000100
00000000
00000000
10000000
01000010
00000001
>
00000000
10000010
01000000
00000000
00000000
00000000
10000100
00000010
WAL false

TIL a
00100100
11011011
00100100
11011011
00100100
11011011
00100100
11011011

TIL b
01100110
11011011
01100110
10111101
01100110
10111101
01100110
10111101

TIL c
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL d
01001001
01010010
10010100
10100100
00101001
01001010
10010010
10100100
WAL true

TIL e
00000000
01010101
00000000
01010101
00000000
01010101
00000000
01010101

TIL f
00000000
00000000
00100100
00000000
00000000
00100100
00000000
00000000

TIL g
00000000
00000011
00001100
00010000
00100000
00100000
01000000
01000000
WAL true

TIL h
00000000
11000000
00110000
00001000
00000100
00000100
00000010
00000010
WAL true

TIL i
00000000
11111111
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL j
01111110
10000010
10000011
10000101
10011111
11110011
01100111
00111111
WAL true

TIL k
00010000
00010000
00001000
00000010
01000010
01000001
00100000
00000000

TIL l
10000000
00001000
00000100
00000000
00000000
10000000
01000010
00000001
>
00000000
10000010
01000000
00000000
00000000
00000000
10000100
00000010
WAL true

TIL m
11001000
01000000
00000110
00000010
01100000
00100000
00000011
10011001
>
00010000
11000011
01000001
00000000
00110000
00010000
11000110
01000010
WAL true

TIL n
00000000
11011011
11011011
00000000
10110110
10110110
00000000
11011011

TIL o
01111100
10000010
10000010
10000111
01000111
01000111
01101111
00111111
WAL true

TIL p
00000000
00001000
00001001
00010000
00000000
00000000
00000000
00000000

TIL q
01101101
10010000
00000000
00000000
00000000
00000000
00000000
00000000

TIL r
00000000
00000000
00000000
00000000
00000000
00000000
11101110
10111011

TIL s
00000000
00000010
00000000
00000000
01000000
10100000
01000000
00000000
>
00000010
00000111
00000010
00000000
00000000
01000000
00000000
00000000

TIL t
01111110
10000001
10111101
10111101
10011001
10011001
10000001
11000011
WAL true

TIL u
01111110
10000010
10000011
10000101
10011111
11110011
01100111
00111111
WAL false

TIL v
00001000
00001000
00010000
00010000
00100000
00100000
01000000
01111111
WAL true

TIL w
00000000
00000000
00000001
00000001
00000010
00000010
00000100
00000100
WAL true

TIL x
00000100
00000100
10001010
10001010
01010001
01011111
01100000
01100000
WAL true

TIL y
01010000
01010000
01011000
01011000
01010100
01010100
01010110
11111110
WAL true

TIL z
00011000
00011000
00100100
00100100
01000110
01000110
10000101
11111111
WAL true

TIL 1a
00000000
00000000
00000100
00100100
00100000
01000000
00000000
00000000

SPR 10
00000000
11101100
00010010
00000010
00000100
00001000
00001000
00001000
DLG SPR_p
POS f 11,9

SPR 11
00001000
00000111
00001000
01110000
10010000
10000000
10001111
01110000
DLG SPR_o
POS f 10,10

SPR 12
00010000
11101000
00000100
00000110
00001001
00000001
11110001
00001110
DLG SPR_n
POS f 11,10

SPR 13
00011100
00100010
00100010
01000001
01000001
00111110
00001100
00001100
DLG SPR_m
POS g 8,11

SPR 14
01111110
10000001
10000001
10111101
10000001
01111111
00011000
00011000
DLG SPR_r
POS g 5,9

SPR 15
01111100
10101010
10000001
10111001
10100101
10111001
01000010
11111111
>
01111000
10000110
10101001
10000001
10111101
10111001
01000010
11111111
DLG SPR_s
POS i 5,6

SPR 16
00000000
00100010
01010000
00100100
00101010
00100100
10000100
00000100
DLG SPR_t
POS k 13,12

SPR 17
00000000
11111111
10000001
10111101
10000001
10000001
01111110
00011000
DLG SPR_u
POS k 11,6

SPR 18
00000000
00111110
01000001
01010001
01000110
00100100
00111100
01100110
DLG SPR_v
POS l 8,7

SPR 19
01111111
01000001
01011101
01011101
01000001
01111111
00001000
00001000
DLG SPR_w
POS m 8,7

SPR A
00111100
01000010
10100101
10100101
10011001
01000010
00110110
01110111
>
00111100
01000010
10100101
10100101
10011001
01000010
01101100
11101110
POS 0 4,5

SPR a
01111110
10000001
10111001
10000001
10111101
10000001
01111110
00111100
DLG SPR_0
POS 0 13,12

SPR b
01111110
10000001
10111101
10111101
10000001
01111110
00011000
00011000
DLG SPR_1
POS 0 11,6

SPR c
00000000
00111110
01000001
10110001
10000101
11110001
01000010
01111110
>
00111110
01000001
10110001
10000101
11110001
00110010
01000010
01111110
DLG SPR_2
POS 5 9,11

SPR d
00111100
11000010
10100101
10010000
10000111
10000100
10000011
01111111
>
00111100
11000010
10000101
10110000
10000111
10000001
10000001
01111110
DLG SPR_3
POS 2 8,4

SPR e
00001000
00011000
01111110
01000010
01000010
01000010
01100110
00111100
DLG SPR_4
POS 2 10,5

SPR f
01110110
11111111
10101011
11111111
01010111
01111110
00101100
00001000
POS 2 10,4

SPR g
00011100
00100010
00100010
01000001
01000001
00111110
00001100
00001100
DLG SPR_5
POS 1 10,5

SPR h
11111111
10000001
10111101
10000001
10000001
11111111
00011000
00011000
DLG SPR_6
POS 3 8,9

SPR i
00000110
00001001
00001000
00001100
00001000
00010000
00010000
00010000
POS 4 13,0

SPR j
00000000
11101100
00010010
00000010
00000100
00001000
00001000
00001000
POS 4 14,0

SPR k
00001000
00000111
00001000
01110000
10010000
10000000
10001111
01110000
DLG SPR_7
POS 4 13,1

SPR l
00010000
11101000
00000100
00000110
00001001
00000001
11110001
00001110
DLG SPR_8
POS 4 14,1

SPR m
01111110
10000001
10111001
10000001
10111101
10000001
01111110
00111100
DLG SPR_9
POS 4 12,0

SPR n
00000000
00111110
01000001
10110001
10000101
11110001
01000010
01111110
>
00111110
01000001
10110001
10000101
11110001
00110010
01000010
01111110
DLG SPR_a
POS 4 5,12

SPR o
00111100
01000010
10100101
10000001
10011001
10000001
01111110
01100110
>
00111100
01000010
10100101
10000001
10110001
10000001
01111110
11000011
DLG SPR_b
POS 1 8,3

SPR p
00111100
01000010
10100101
10000001
10110001
10000001
01111110
11000011
>
00111100
01000010
10100101
10000001
10011001
10000001
01111110
01100110
DLG SPR_f
POS 1 12,3

SPR q
00111100
01000010
10100101
10000001
10011001
10000001
01111110
01100110
>
00111100
01010010
10000101
10110001
10110001
10000001
01111110
11000011
DLG SPR_g
POS 1 6,5

SPR r
00111100
01000010
10100101
10000001
10011001
10000001
01111110
01100110
>
00111100
01000010
10100101
10000001
10110001
10000001
01111110
11000011
DLG SPR_h
POS 1 14,5

SPR s
00111100
01000010
10100101
10000001
10011001
10000001
01111110
01100110
>
00111100
01000010
10100101
10000001
10110001
10000001
01111110
11000011
DLG SPR_i
POS 1 12,7

SPR t
00111100
01000010
10100101
10011001
10011001
10000001
01111110
00110110
>
00111100
01010010
10000101
10110001
10110001
10000001
01111110
11000011
DLG SPR_j
POS 1 8,7

SPR u
00011100
00100010
00100010
01000001
01000001
00111110
00001100
00001100
DLG SPR_k
POS 3 10,9

SPR v
00011100
00100010
00100010
01000001
01000001
00111110
00001100
00001100
DLG SPR_l
POS 7 9,4

SPR w
01111110
10000001
10111101
10000001
10111101
10000001
01111110
00011000
DLG SPR_c
POS b 1,6

SPR x
11111111
10000001
10111101
10000001
10111101
10000001
11111110
00011000
DLG SPR_d
POS d 7,3

SPR y
00000000
00111110
01000001
10110001
10000101
11110001
01000010
01111110
>
00111110
01000001
10110001
10000101
11110001
00110010
01000010
01111110
DLG SPR_e
POS c 11,11

SPR z
00000110
00001001
00001000
00001100
00001000
00010000
00010000
00010000
DLG SPR_q
POS f 10,9

SPR 1a
00000000
00111110
01000001
10110001
10000101
11110001
01000010
01111110
>
00111110
01000001
10110001
10000101
11110001
00110010
01000010
01111110
DLG SPR_x
POS 6 10,12

ITM 0
00000000
00011000
00100100
01111110
01000010
01000010
00100100
00011000
NAME NUT
DLG ITM_0

ITM 1
00111100
01000010
01111110
01000010
01011010
01010010
01000010
00111100
NAME PB
DLG ITM_2

ITM 2
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME b4end
DLG ITM_3

ITM 3
00010000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG ITM_4

ITM 4
00000000
00000000
00100000
00000100
00100000
00011100
00000000
00000000
NAME itn
DLG ITM_5

DLG SPR_0
IT'S A NOTE. IT SAYS, "PLEASE HAVE FUN!"

DLG SPR_1
"""
"SOMEONE'S HOUSE"
{a = a + 1}
"""

DLG ITM_0
A NUT, OR "SEED."

DLG SPR_2
"""
{
  - p == {item "PB"} ?
    WHAT IS THIS! THE NUTS ARE BROKEN!! COME BACK WITH UNDAMAGED NUTS!
    WELL, ACTUALLY, PEANUTS ARE LEGUMES!
    {p = p - 1}
  - {item "NUT"} == 5 ?
    YOU BROUGHT THE NUTS/SEEDS! I AM VERY "{wvy} PRO SEED, {wvy}" AND NOW YOU MAY "{wvy}PROCEED.{wvy}"
    ...
    ...
    WELL, NOT THAT I COULD'VE STOPPED YOU...
  - {item "NUT"} == 3 ?
    HMM...3 IS PRETTY GOOD...
    BUT NUT GOOD ENOUGH FOR ME TO LET YOU PASS. SORRY...............
  - {item "NUT"} == 2 ?
    HMM... 2 NUTS IS PRETTY GOOD...
    BUT NUT GOOD ENOUGH FOR ME TO LET YOU PASS.
    SORRY..................
  - {item "0"} == 1 ?
    HMM... I SEE YOU BROUGHT ME A NUT.
    PRETTY GOOD.
    BUT NUT GOOD ENOUGH FOR ME... 
    I NEED 4 MORE TO LET YOU PASS!    SORRY.......................
  - {item "NUT"} == 4 ?
    HMM... 4 IS GOOD...
    BUT STILL NUT GOOD ENOUGH FOR ME TO LET YOU PASS... 
    SORRY............

  - {item "NUT"} == 0 ?
    I'M A FAN OF NUTS, or 
    {wvy}SEEDS.{wvy}
    IF YOU BRING ME 5 NUTS, or
    {wvy}SEEDS{wvy} I'LL LET
    YOU PASS.
}
"""

DLG SPR_3
"""
{
  - a > 1 ?
    YES, I AM SOMEONE!
  - a == 1 ?
    WELCOME TO HOUSE!
  - i == a ?
    I COULD BE NO ONE! I COULDN'T BE NO ONE! I'M EVERYONE AND NO ONE!
}
"""

DLG SPR_4
(POTTED PLANT)

DLG ITM_1
A JAR OF PEANUT BUTTER.

DLG ITM_2
IT'S PNUT BUTTER!

DLG SPR_6
"""
{
  - m == 0 ?
    THIS SIGN IS SUBJECT TO CHANGE
    LOVE,
    M
  - m >= 3 ?
    TRY GOING SOUTH...
    LOVE,
    M
  - m == 2 ?
    IT'S POSSIBLE THAT SOMETHING IS CHANGING
    LOVE,
    M
  - m == 1 ?
    THIS SIGN MAY OR MAY NOT CHANGE,
    DEPENDING ON CIRCUMSTANCES.
    LOVE,
    M
}
"""

DLG SPR_9
"NORTH"

DLG SPR_5
"""
{cycle
  - YOU CONTEMPLATE THE M'SHROOM. {m = m + 1}
  - THE M'SHROOM SILENTLY CONTEMPLATES YOU. {m = m - 1}
}
"""

DLG ITM_3
LOOKS LIKE THE END IS BEYOND THIS DOOR.

DLG SPR_c
SOMEONE LEFT A NUT DOWN HERE.

DLG ITM_4
HEH HEH... SECRET ENTRANCE.

DLG SPR_d
"""
"LOOKING FOR A GOOD TIME?
WELL, YOU FOUND IT!"
"""

DLG SPR_f
"""
{
  - {item "itn"} == 0 ?
    IT'S GOOD TO BE. HERE, I MEAN.
  - i == {item "itn"} ?
    I WILL ALWAYS BE HERE FOR THEM.
}
"""

DLG SPR_g
"""
{
  - {item "itn"} == 0 ?
    WE LOVE DOING THIS THING!
  - i == {item "itn"} ?
    I CAN ONLY SPEAK FOR MYSELF, BUT I FREAKIN' LOVE DOING THIS THING!
}
"""

DLG SPR_i
"""
{
  - {item "itn"} == 0 ?
    DOING THIS THING FOREVER!
  - i == {item "itn"} ?
    DOING THIS THING FOREVER?
    FOREVER IS PRETTY LONG!
}
"""

DLG SPR_k
"""
{cycle
  - YOU CONTEMPLATE THE M'SHROOM. {m = m + 1}
  - THE M'SHROOM SILENTLY CONTEMPLATES YOU. {m = m - 1}
}
"""

DLG SPR_l
"""
{cycle
  - YOU CONTEMPLATE THE M'SHROOM. {m = m + 1}
  - THE M'SHROOM SILENTLY CONTEMPLATES YOU. {m = m - 1}
}
"""

DLG SPR_m
"""
{cycle
  - YOU CONTEMPLATE THE M'SHROOM. {m = m + 1}
  - THE M'SHROOM SILENTLY CONTEMPLATES YOU. {m = m - 1}
}
"""

DLG SPR_n
"""
{
  - i = {item "itn"} ?
    SOMETIMES, I THINK PEOPLE TAKE ADVANTAGE OF ME.
  - {item "itn"} == 0 ?
    FACELESS BEAR.
    I THINK I'M PRETTY CUTE, THOUGH. LOOK AT ME! I'D HANG OUT WITH ME.
}
"""

DLG SPR_o
"""
{
  - i = {item "itn"} ?
    SOMETIMES, I THINK PEOPLE TAKE ADVANTAGE OF ME.
  - {item "itn"} == 0 ?
    FACELESS BEAR.
    I THINK I'M PRETTY CUTE, THOUGH. LOOK AT ME! I'D HANG OUT WITH ME.
}
"""

DLG SPR_q
"""
{
  - i = {item "itn"} ?
    SOMETIMES, I THINK PEOPLE TAKE ADVANTAGE OF ME.
  - {item "itn"} == 0 ?
    FACELESS BEAR.
    I THINK I'M PRETTY CUTE, THOUGH. LOOK AT ME! I'D HANG OUT WITH ME.
}
"""

DLG SPR_r
A PEACEFUL POND. PEACE HERE ANYTIME.

DLG SPR_s
"""
{
  - i == {item "itn"} ?
    IT'S HARD TO TALK ABOUT MY INTERESTS WITH PEOPLE!
    YOU'RE A GOOD LISTENER, THOUGH!
    I'M DURBIN!
  - {item "PB"} == 1 ?
    {shk}OH{shk}
    MY {shk}GODD!{shk}
    YOU BROUGHT IT!
    I LOVE PNUT BUTTER {shk}SO MUCH!{shk}
    IF I CAN GET JUST A FRACTION
    OF A TEASPOON
    OF {shk}PNUT BUTTER{shk} OUT OF THE BOTTOM OF THE JAR
    I'M IN
    {rbw}HEAVEN!{rbw}
    *SCREAMS*
  - {item "PB"} < 1 ?
    DID YOU BRING IT?
}
"""

DLG SPR_t
A COUPLE OF FLOWERS.

DLG SPR_u
"NOBODY'S HOUSE"

DLG SPR_v
"""
{
  - {item "itn"} == 0 ?
    WHERE POND GO  
  - i == {item "itn"} ?
    HARD TO GET USED TO
    NO POND
}
"""

DLG SPR_e
"""
{
  - {item "itn"} == 0 ?
    EVERYTHING EXISTS... ALMOST TOO STRONGLY FOR ME.
  - i == {item "itn"} ?
    IT'S JUST... EVERYHING'S HARD, YOU KNOW? SOMETIMES I WONDER WHAT DIFFERENCE IT MAKES THAT I'M HERE.
}
"""

DLG SPR_p
"""
{
  - i = {item "itn"} ?
    SOMETIMES, I THINK PEOPLE TAKE ADVANTAGE OF ME.
  - {item "itn"} == 0 ?
    FACELESS BEAR.
    DON'T JUDGE ME!
}
"""

DLG SPR_w
"""
WHAT ARE YOU LOOKING FOR?
I TRULY BELIEVE THAT IF YOU LOOK HARD ENOUGH, YOU'LL FIND SOMETHING.
I BELIEVE IN YOU!
"""

DLG ITM_5
"""
you're in the world
changing stuff
"""

DLG SPR_a
"""
{
  - {item "itn"} == 0 ?
    I'M NOT THE NUT WANTER, WE JUST LOOK ALIKE.
  - i == {item "itn"} ?
    I WISH PEOPLE DIDN'T MAKE JUDGMENTS ABOUT ME. LIKE, ABOUT WHETHER I LIKE NUTS OR NOT. I CAN'T HELP WHO I LOOK LIKE.
    EVERYONE MAKES JUDGMENTS. THEY CAN'T HELP IT.
}
"""

DLG SPR_7
"""
{
  - i == {item "itn"} ?
    MAYBE WE COULD HANG OUT SOMETIME. GET TO KNOW EACH OTHER BETTER. 
    I KNOW, I DON'T HAVE A FACE. HARD TO RELATE. I BET MOST OF THE PEOPLE WHO COME THROUGH HERE HAVE FACES.
  - {item "itn"} == 0 ?
    THE NUT WANTER IS NORTH OF HERE, I THINK. I DON'T KNOW FOR SURE.
}
"""

DLG SPR_b
"""
{
  - {item "itn"} == 0 ?
    WE JUST LIKE DOIN' THIS THING! IT'S OUR FAVORITE!
  - i == {item "itn"} ?
    CAN I HAVE MORE THAN ONE FAVORITE?
}
"""

DLG SPR_h
"""
{
  - {item "itn"} == 0 ?
    JUST... DANCIN!
  - i == {item "itn"} ?
    HOPE YOU'LL... JOIN US SOMETIME!
}
"""

DLG SPR_j
"""
{
  - {item "itn"} == 0 ?
    ALWAYS DOING THE THING!
  - i == {item "itn"} ?
    {shk}HELL YEAH!{shk}! I AM PUMPED!
}
"""

DLG SPR_8
"""
{
  - {item "itn"} == 0 ?
    I THINK THE NUT WANTER IS NORTH OF HERE.
  - i == {item "itn"} ?
    MAYBE WE COULD HANG OUT SOMETIME. GET TO KNOW EACH OTHER BETTER. 
    I KNOW, I DON'T HAVE A FACE. HARD TO RELATE. I BET MOST OF THE PEOPLE WHO COME THROUGH HERE HAVE FACES.
}
"""

DLG SPR_x
"""
{
  - {item "itn"} == 0 ?
    I KNOW, I'M SELFISH.
  - i == {item "itn"} ?
    I JUST WANT PEOPLE TO GIVE ME WHAT I WANT. I'M HONEST.
}
"""

END 0
YOU FOUND THE END! NORMAL END!

END undefined
MUSH ENDING.
LEARN ABOUT MUSHROOMS AT
YOUR LOCAL LIBARY.

END undefinee
SECRET ENDING!!

END undefinef


VAR a
1

VAR m
0

VAR p
1

VAR i
1


