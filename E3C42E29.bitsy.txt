
Life and Death

# BITSY VERSION 4.6

! ROOM_FORMAT 1

PAL 0
NAME Brown
87,35,25
224,131,33
255,203,66

PAL 1
NAME Inside
0,0,0
163,163,163
255,203,66

PAL 2
NAME Red
82,33,23
255,0,0
255,203,66

ROOM 0
0,a,0,0,g,c,d,c,c,c,d,c,h,14,a,0
r,a,14,0,g,c,c,c,e,c,c,c,h,0,a,r
p,a,0,0,0,f,f,f,13,f,f,f,14,0,a,p
p,a,0,b,b,0,0,14,13,0,0,b,b,0,a,p
p,a,0,i,i,0,0,0,13,0,0,i,i,0,a,p
p,a,0,b,b,0,0,0,13,14,0,b,b,0,a,p
q,a,0,i,i,0,14,0,13,0,0,i,i,0,a,q
0,a,0,0,0,0,0,0,13,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,b,b,0,0,0,0,0,14,b,b,0,a,0
0,a,14,i,i,0,14,0,0,0,0,i,i,0,a,r
0,a,0,b,b,0,0,0,0,0,0,b,0,14,a,p
r,a,0,i,i,0,0,0,0,0,0,i,i,0,a,p
p,a,0,0,0,0,0,0,0,0,14,0,0,0,a,p
p,a,a,a,a,a,0,0,0,a,a,a,a,a,a,p
p,p,p,x,0,a,a,a,a,a,0,y,p,p,p,p
NAME Start
ITM 0 4,6
ITM 0 12,4
ITM 0 3,12
ITM 6 7,14
EXT 2,0 1 2,15
EXT 3,0 1 3,15
EXT 4,0 1 4,15
EXT 12,0 1 12,15
EXT 13,0 1 13,15
PAL 0

ROOM 1
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,14,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,14,0,a
a,0,0,0,0,0,0,0,0,0,0,14,0,0,0,a
a,0,14,0,0,o,o,o,o,o,s,0,0,0,0,a
a,0,0,0,0,u,0,b,b,0,t,0,0,0,0,a
a,0,0,0,0,u,y,r,r,v,t,0,14,0,0,a
a,0,0,0,14,u,0,q,q,z,t,0,0,0,14,a
a,0,0,0,0,11,o,o,o,o,10,0,0,0,0,a
a,0,0,0,0,0,0,0,13,0,0,14,0,0,0,a
a,0,0,14,0,0,0,0,13,0,0,0,0,0,0,a
a,0,0,14,0,0,0,0,13,13,0,0,0,0,14,a
a,14,0,0,0,0,0,0,0,13,14,0,0,0,0,a
a,a,11,12,o,m,n,n,n,15,n,l,o,o,a,a
0,a,14,0,0,j,c,c,c,c,c,k,0,0,a,0
0,a,0,0,0,j,c,c,c,c,c,k,14,0,a,0
NAME Up
ITM 7 7,5
ITM 7 8,5
ITM 6 6,7
ITM 6 3,12
ITM 6 12,14
ITM 9 9,13
EXT 2,15 0 2,0
EXT 3,15 0 3,0
EXT 4,15 0 4,0
EXT 12,15 0 12,0
EXT 13,15 0 13,0
EXT 9,13 2 9,0
PAL 0

ROOM 2
18,18,18,18,18,18,18,a,a,e,a,a,a,a,a,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,0,1h,16,16,16,17,0,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,a,a,a,a,a,e,a,a
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Inside1
ITM 1 8,4
ITM 1 14,4
ITM 6 13,4
EXT 9,0 1 9,12
EXT 13,5 4 13,5
PAL 1

ROOM 3
a,a,a,a,a,a,a,a,a,e,a,a,a,a,a,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,1h,16,16,16,17,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,a,a,a,a,a,e,a,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,0,0,0,a
a,a,a,a,a,e,a,a,a,a,e,a,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,e,a,a,a,a,a,a,a,a,a
NAME FullInside
ITM 2 11,4
EXT 9,0 1 9,12
PAL 1

ROOM 4
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,a,a,a,a,a,a,e,a,a
18,18,18,18,18,18,18,a,1j,1j,1i,1j,1j,0,1j,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,1k,0,0,0,0,0,1k,a
18,18,18,18,18,18,18,a,1k,0,0,0,0,0,1k,a
18,18,18,18,18,18,18,a,1k,0,0,0,1k,1k,1k,a
18,18,18,18,18,18,18,a,a,a,e,a,a,a,a,a
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Inside2
ITM 3 8,7
ITM 6 10,10
ITM 3 13,9
EXT 13,5 2 13,5
EXT 10,11 5 10,11
PAL 1

ROOM 5
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
a,a,a,a,a,e,a,a,a,a,e,a,a,a,a,a
a,1l,1l,1l,1l,1l,1l,1l,1l,1l,0,1l,1l,1l,1l,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,e,a,a,a,a,a,a,a,a,a
NAME Inside3
ITM 4 6,15
ITM 6 10,13
EXT 10,11 4 10,11
EXT 6,15 6 8,1
PAL 1

ROOM 6
0,a,0,0,g,c,d,c,c,c,d,c,h,14,a,0
r,a,14,0,g,c,c,c,e,c,c,c,h,0,a,r
p,a,0,0,0,f,f,f,13,f,f,f,14,0,a,p
p,a,0,b,b,0,0,14,13,0,0,b,b,0,a,p
p,a,0,i,i,0,0,0,13,0,0,i,i,0,a,p
p,a,0,b,b,0,0,0,13,14,0,b,b,0,a,p
q,a,0,i,i,0,14,0,13,0,0,i,i,0,a,q
0,a,0,0,0,0,0,0,13,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,b,b,0,0,0,0,0,14,b,b,0,a,0
0,a,14,i,i,0,14,0,0,0,0,i,i,0,a,r
0,a,0,b,b,0,0,0,0,0,0,b,b,14,a,p
r,a,0,i,i,0,0,0,0,0,0,i,i,0,a,p
p,a,0,0,0,0,0,0,0,0,14,0,0,0,a,p
p,a,a,a,a,a,0,0,0,a,a,a,a,a,a,p
p,p,p,x,0,a,a,a,a,a,0,y,p,p,p,p
NAME StartUnlocked
ITM 6 3,1
EXT 2,0 7 2,15
EXT 3,0 7 3,15
EXT 4,0 7 4,15
EXT 12,0 7 12,15
EXT 13,0 7 13,15
EXT 8,1 10 6,15
PAL 0

ROOM 7
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,14,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,14,0,a
a,0,0,0,0,0,0,0,0,0,0,14,0,0,0,a
a,0,14,0,0,o,o,o,o,o,s,0,0,0,0,a
a,0,0,0,0,u,0,b,b,0,t,0,0,0,0,a
a,0,0,0,0,u,y,r,r,v,t,0,14,0,0,a
a,0,0,0,14,u,0,q,q,z,t,0,0,0,14,a
a,0,0,0,0,11,o,o,12,o,10,0,0,0,0,a
a,0,0,0,0,0,0,0,13,0,0,14,0,0,0,a
a,0,0,14,0,0,0,0,13,0,0,0,0,0,0,a
a,0,0,14,0,0,0,0,13,13,0,0,0,0,14,a
a,14,0,0,0,0,0,0,0,13,14,0,0,0,0,a
a,a,11,12,o,m,n,n,n,15,n,l,o,o,a,a
0,a,14,0,0,j,c,c,c,c,c,k,0,0,a,0
0,a,0,0,0,j,c,c,c,c,c,k,14,0,a,0
NAME UpUnlocked
ITM 5 8,8
ITM 6 6,7
EXT 2,15 6 2,0
EXT 3,15 6 3,0
EXT 4,15 6 4,0
EXT 12,15 6 12,0
EXT 13,15 6 13,0
EXT 9,13 8 9,0
PAL 0

ROOM 8
18,18,18,18,18,18,18,a,a,e,a,a,a,a,a,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,0,1h,16,16,16,17,0,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,a,a,a,a,a,e,a,a
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Inside1Unlocked
ITM 1 8,4
ITM 1 14,4
ITM 6 14,2
EXT 9,0 7 9,12
EXT 13,5 9 13,5
PAL 1

ROOM 9
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,a,a,a,a,a,a,e,a,a
18,18,18,18,18,18,18,a,1j,1j,1i,1j,1j,0,1j,a
18,18,18,18,18,18,18,a,0,0,0,0,0,0,0,a
18,18,18,18,18,18,18,a,1k,0,0,0,0,0,1k,a
18,18,18,18,18,18,18,a,1k,0,0,0,0,0,1k,a
18,18,18,18,18,18,18,a,1k,0,0,0,1k,1k,1k,a
18,18,18,18,18,18,18,a,a,a,e,a,a,a,a,a
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Inside2Unlocked
ITM 3 8,7
ITM 3 13,9
ITM 6 13,7
EXT 13,5 8 13,5
EXT 10,11 10 10,11
PAL 1

ROOM 10
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
a,a,a,a,a,e,a,a,a,a,e,a,a,a,a,a
a,1l,1l,1l,1l,12,1l,1l,1l,1l,0,1l,1l,1l,1l,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,e,a,a,a,a,a,a,a,a,a
NAME Inside3Unlocked
ITM 6 5,13
ITM 1 1,14
ITM 1 14,14
EXT 10,11 9 10,11
EXT 6,15 6 8,1
EXT 5,11 11 5,11
PAL 1

ROOM 11
a,a,1q,a,a,a,a,a,18,18,18,18,18,18,18,18
a,1w,1r,1m,1o,0,0,a,18,18,18,18,18,18,18,18
a,1l,1t,1s,0,0,0,a,18,18,18,18,18,18,18,18
a,0,0,0,0,1h,16,a,18,18,18,18,18,18,18,18
a,0,0,0,0,0,0,a,18,18,18,18,18,18,18,18
a,0,0,0,0,0,0,a,18,18,18,18,18,18,18,18
a,16,17,0,0,0,0,a,18,18,18,18,18,18,18,18
a,0,0,0,0,0,0,a,18,18,18,18,18,18,18,18
a,0,0,0,0,1h,16,a,18,18,18,18,18,18,18,18
a,0,0,0,0,0,0,a,18,18,18,18,18,18,18,18
a,0,0,0,0,0,0,a,18,18,18,18,18,18,18,18
a,a,a,a,a,e,a,a,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Inside4Unlocked
ITM 1 1,10
ITM 1 6,10
ITM 6 4,1
ITM 1 6,1
EXT 2,0 12 2,0
EXT 5,11 10 5,12
PAL 1

ROOM 12
18,a,e,a,a,a,a,a,a,18,18,18,18,18,18,18
18,a,0,0,0,0,0,0,a,18,18,18,18,18,18,18
18,a,0,0,1g,1f,1v,0,a,18,18,18,18,18,18,18
18,a,0,0,1e,0,0,0,a,18,18,18,18,18,18,18
18,a,0,0,0,0,0,0,a,18,18,18,18,18,18,18
18,a,0,0,0,0,0,0,a,18,18,18,18,18,18,18
18,a,a,a,a,a,a,a,a,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18
NAME Upstairs
ITM 8 5,3
ITM 6 2,1
ITM 1 2,5
ITM 1 7,5
EXT 2,0 11 2,0
END undefined 5,3
PAL 2

ROOM 13
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME SpriteStorage
PAL 0

TIL 10
00001010
01010100
10101010
10101010
10101010
10101010
10101010
11111110
NAME FenceHorizontalRightCorner
WAL true

TIL 11
10100000
01010101
10101010
10101010
10101010
10101010
10101010
11111111
NAME FenceHorizontalLeftCorner
WAL true

TIL 12
00000000
00000010
11000100
10000001
11001010
10100000
10001000
11010001
NAME BrokenFence

TIL 13
10001110
10100000
10100111
00101000
00001000
00001010
11100010
00111010
NAME Path

TIL 14
00100000
00000000
00000001
00001000
10000000
00000000
00000000
01000010
NAME OutsideGround

TIL 15
10000000
11111111
00010010
00001001
10000100
01000010
10100001
11111111
NAME RoofTopOpening
WAL false

TIL 16
00000000
00000000
00000000
11111111
11111111
01100110
01100110
01100110
NAME table
WAL true

TIL 17
00000100
00000100
00000100
00000100
00111100
00100100
00100100
00100100
NAME chairRight

TIL 18
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME Question
WAL true

TIL 19
00001111
00010001
00100010
01000010
10000100
11111111
10100100
10010100
NAME CircleTopLeft

TIL a
11111101
00000000
11011111
11110111
00000000
11111111
00000000
10111111
NAME wall1
WAL true

TIL b
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME GraveStone
WAL true

TIL c
00110111
10101000
01100010
10100010
01101101
10000010
01100011
00100010
NAME IvyWall
WAL true

TIL d
11111111
10000001
10000001
10000001
10000101
10000101
10000101
11111111
>
11111111
10000001
10000001
10000101
10001111
10000101
10000101
11111111
NAME CandleWindow

TIL e
11111111
11000011
10000001
10000001
10000101
10000001
10000001
10000001
NAME Door
WAL false

TIL f
10000001
11111111
00000000
00000000
00000000
00000000
00000000
00000000
NAME BorderTop

TIL g
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME BorderLeft

TIL h
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME BorderRight

TIL i
00000000
00100100
01000010
01000010
01000010
01000010
00100100
00000000
NAME Mound

TIL j
01001001
01010001
01100001
01000011
01000101
01001001
01010001
01100001
NAME RoofL
WAL true

TIL k
10100010
10010010
10001010
11000110
10100010
10010010
10001010
10000110
NAME RoofR
WAL true

TIL l
00000000
11111111
01001010
10010010
00100010
10010010
01001010
10100111
NAME RoofRCorner
WAL true

TIL m
00000000
01111111
01010010
01001001
01000100
01001001
01010010
01100101
NAME RoofLCorner
WAL true

TIL n
00000000
11111111
00010010
00001001
10000100
01000010
10100001
11111111
NAME RoofTop
WAL true

TIL o
00000000
01010101
10101010
10101010
10101010
10101010
10101010
11111111
NAME FenceHorizontalLeft
WAL true

TIL p
01001110
00011011
00001110
00000100
11100100
10110001
11100000
01000000
NAME FlowerCenter

TIL q
01001110
00011011
00001110
00000100
00000100
00000000
00000000
00000000
NAME FlowerBottom

TIL r
00000000
00000000
00000000
00000000
11100000
10110001
11100000
01000000
NAME FlowerTop

TIL s
00000000
01010100
10101010
10101010
10101010
10101010
10101010
11111110
NAME FenceHorizontalRight
WAL true

TIL t
00001010
00001010
00001010
00001010
00001010
00000100
00001010
00001010
NAME FenceVertRight
WAL true

TIL u
10100000
10100000
10100000
10100000
10100000
01000000
10100000
10100000
NAME FenceVertLeftFower
WAL true

TIL v
00000000
00000000
00000000
00000000
11100000
10110000
11100000
01000000
NAME FlowerTopLeft

TIL w
00000000
00000000
00000000
00000000
00000000
00000001
00000000
00000000
NAME FlowerTopRight

TIL x
01000000
00000000
00000000
00000000
11100000
10110000
11100000
01000000
NAME FlowerCenterRight

TIL y
00000000
00000000
00000000
00000000
00000000
00000001
00000000
00000000
NAME FlowerCenterRight

TIL z
01000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME FlowerBottomLeft

TIL 1a
11100000
00111000
01001100
01000010
00100001
11111111
00100101
00011001
NAME CircleTopRight

TIL 1b
10001010
10001001
10001000
01001001
01001010
00101100
00011000
00000111
NAME CircleBottomLeft

TIL 1c
00110001
01010001
10010001
01010010
00110010
00010100
00111000
11100000
NAME CircleBottomRight

TIL 1d
00111001
00100101
00100011
11111111
00100010
01000100
01011000
11100000
NAME PentaBottomRight

TIL 1e
10011110
10100100
11000100
11111111
01000010
00100010
00011001
00000111
NAME PentaBottomRight

TIL 1f
11100000
00111000
00010100
00100110
01000101
10000101
10001001
01001001
NAME PentaTopRight

TIL 1g
00001111
00010000
00111000
01010100
10010010
10010001
10001000
10001001
NAME PentaTopLeft

TIL 1h
00100000
00100000
00100000
00100000
00111100
00100100
00100100
00100100
NAME chairLeft

TIL 1i
11111111
10000001
10011001
10000001
01111110
01000010
01001010
11010011
>
11111111
10000001
10011001
10000001
01111110
01000010
01010010
11001011
NAME Oven
WAL true

TIL 1j
11111111
10000001
10000001
11111111
10000001
10100101
10000001
11111111
NAME Counter
WAL true

TIL 1k
00111100
01000010
10000001
11000011
10111101
10000001
01000010
00111100
NAME Barrel
WAL true

TIL 1l
11111111
11010101
11010101
11111111
10101011
10101011
11111111
11111111
NAME Bookshelf
WAL true

TIL 1m
11100000
00011000
00100110
01001001
10010010
01100100
00011001
00000110
NAME Stairs

TIL 1o
00000000
00000000
00000000
10000000
01000000
10100000
01000000
10000000

TIL 1q
11000000
00011110
11110011
11100001
00100001
11100001
00100001
11100001
NAME StairDoor

TIL 1r
00100001
00100000
01000000
10000000
11111111
10000000
10000000
10000000
NAME StairLanding

TIL 1s
00000001
11111111
00000000
00000000
00000000
00000000
00000000
00000000
NAME StairBottomRight
WAL true

TIL 1t
10000000
11111111
00000000
00000000
00000000
00000000
00000000
00000000
NAME StairBottomLeft
WAL true

TIL 1u
00000011
00001100
00101100
00010000
01101000
10100000
00100000
00000000
NAME body

TIL 1v
00000000
00000000
00000100
00001000
01110000
01101000
11111110
11100000
NAME BloodS

TIL 1w
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Blank
WAL true

SPR A
00000000
00111100
01010110
01010110
01111110
00111110
00011100
00001000
>
00000000
00111100
01101010
01101010
01111110
01111100
00111000
00010000
POS 0 7,15

SPR a
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME GSDave
DLG SPR_0
POS 0 3,5

SPR b
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME GSJim
DLG SPR_1
POS 0 12,11

SPR c
00000000
00011000
00111100
01011010
01111110
00111110
00011100
00001000
>
00000000
00011000
00111100
01011010
01111110
01111100
00111000
00010000
NAME AvatarClone
DLG SPR_7
POS 0 7,12

SPR d
00000000
00011000
00111100
01011010
01111110
00111110
00011100
00001000
>
00000000
00011000
00111100
01011010
01111110
01111100
00111000
00010000
NAME IntroGhost
DLG SPR_3
POS 13 4,4

SPR e
01110110
10001001
10000101
10101001
10100101
10001001
10000101
11111111
NAME Gate
DLG SPR_5
POS 1 8,8

SPR f
11111111
10000001
10101001
10010101
10000001
11111111
00011000
00011000
NAME Sign

SPR g
11111111
11000011
10000001
10000001
10000101
10000001
10000001
10000001
NAME DoorLocked
DLG SPR_4
POS 0 8,1

SPR h
00000111
00011100
00110000
01100000
01000000
11000000
10000000
10000000

SPR i
00000000
00011000
00111100
01011010
01111110
00111110
00011100
00001000
>
00000000
00011000
00111100
01011010
01111110
01111100
00111000
00010000
NAME InsideGhost1
DLG SPR_8

SPR j
00000000
00011000
00111100
01011010
01111110
00111110
00011100
00001000
>
00000000
00011000
00111100
01011010
01111110
01111100
00111000
00010000
NAME UnlockedGhost
DLG SPR_9
POS 6 9,3

SPR k
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME GSDave2
DLG SPR_a
POS 6 3,5

SPR l
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME GSJim2
DLG SPR_b
POS 6 12,11

SPR m
01110110
10001001
10000101
10101001
10100101
10001001
10000101
11111111
NAME Gate2
DLG SPR_c

SPR n
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME OriginalPartnerHeadstone
DLG SPR_d
POS 7 8,5

SPR o
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME ParterHeadstone
DLG SPR_e
POS 7 7,5

SPR p
00000000
00011000
00111100
01011010
01111110
00111110
00011100
00001000
>
00000000
00011000
00111100
01011010
01111110
01111100
00111000
00010000
NAME InsideGhost1Unlocked
DLG SPR_f
POS 13 3,4

SPR q
11111111
10010101
10010101
11111111
10100001
10100001
11111111
11111111
NAME BookShelfFragile
DLG SPR_g
POS 5 5,12

SPR r
00011100
00111110
01101101
01111111
01111111
00111110
00011100
00001000
NAME Partner
DLG SPR_h
POS 13 2,4

SPR s
11111111
11010101
11010101
11111111
10101101
10101101
11111111
11111111
NAME BookShelfFull
DLG SPR_i
POS 5 2,12

SPR t
11111111
11010101
11010101
11111111
10101101
10101101
11111111
11111111
NAME BookShelfFull2
DLG SPR_j
POS 10 2,12

ITM 0
00000000
00000000
00000000
00111000
01101100
00111000
00010000
00010000
NAME flower
DLG ITM_0

ITM 1
00010000
00111000
00010000
00111000
00111000
00111000
00111000
01111100
>
00000000
00010000
00010000
00111000
00111000
00111000
00111000
01111100
NAME candle
DLG ITM_1

ITM 2
00000000
00000000
11100000
10100000
10111111
10100101
11100000
00000000
NAME key
DLG ITM_3

ITM 3
00001100
00011000
00010000
01111110
11111111
11111111
01111110
00111100
NAME Apple
DLG ITM_2

ITM 4
11111111
11000011
10000001
10000001
10000101
10000001
10000001
10000001
NAME UnlockedDoor
DLG ITM_4

ITM 5
01110110
10001001
10000101
10101001
10100101
10001001
10000101
11111111
NAME UnlockedGate
DLG ITM_5

ITM 6
00111110
01000001
01001010
01010010
01001010
01010010
10000010
01111100
NAME Scroll
DLG ITM_7

ITM 7
00000000
00111100
01000010
01011010
01000010
01011010
01000010
01000010
NAME UnreachableHeadstone
DLG ITM_6

ITM 8
00000011
00001011
00000100
00011010
00101011
00101001
00000101
00000000
NAME corpse
DLG ITM_8

ITM 9
00000000
11111111
10010101
10001011
11000101
10100011
10010001
11111111
NAME BackDoor
DLG ITM_9

DLG SPR_0
"""
{
  - {item "Scroll"} >= 3 ?
    Dave was your son. He died due to your negligence.
  - else ?
    RIP
    Dave
}
"""

DLG ITM_0
"""
{shuffle
  - You found some flowers, how quaint.
  - You found the flowers, dead people love these.
  - You found some flowers, they are as dead as you are.
}
"""

DLG SPR_1
"""
{
  - {item "Scroll"} >= 3 ?
    Jim was your son. He died due to your negligence.
  - else ?
    RIP
    Jim
}
"""

DLG SPR_3
"""
{
  - {item "flower"} >= 1 ?
    Those flowers smell rotten but a deals a deal.
    Your hint is this: The backdoor frequently fails to seal.
  - else ?
    You seem rather alive for someone who should be dead which was certainly not your intent. Never mind that, show me some flowers and I'll give you a hint.
}
"""

DLG SPR_4
The door is locked.

DLG SPR_5
"""
{shuffle
  - The gate is rusted shut. What a shame.
  - The gate is rusted shut. Those flowers are beautiful though.
  - The gate is rusted shut. You see names on the headstones, but you can't quite make them out.
}
"""

DLG ITM_1
"""
{shuffle
  - A bright candle. It makes you feel more alive.
  - A bright candle. Don't catch fire.
  - A (formerly) bright candle. It goes out as soon as you touch it.
}
"""

DLG SPR_2
"""
{
  - {item "flower"} >= 1 ?
    Those flowers smell rotten but a deals a deal
    Your hint is this: The backdoor frequently fails to seal.
  - else ?
    You seem rather alive for someone who should be dead which was certainly not my intent. Never mind that, show me some flowers and I'll give you a hint.
}
"""

DLG SPR_6
"""
{
  - {item "flower"} >= 1 ?
    Those flowers smell rotten but a deals a deal
    Your hint is this: The backdoor frequently fails to seal.
  - else ?
    You seem rather alive for someone who should be dead which was certainly not my intent. Never mind that, show me some flowers and I'll give you a hint.
}
"""

DLG SPR_7
"""
{
  - {item "flower"} >= 1 ?
    Those flowers smell rotten but a deals a deal.
    Your hint is this: The backdoor frequently fails to seal.
  - else ?
    You seem rather alive for someone who should be dead which was certainly not your intent. Never mind that, show me some flowers and I'll give you a hint.
}
"""

DLG SPR_8
"""
{
  - {item "Apple"} >= 1 ?
    An apple a day doesn't really do much now that I'm dead, thanks though.
    
    Oh, and don't worry about the coffee. We don't actually have any.
  - else ?
    STRANGER DANGER.
    Just kidding.
    Can you grab me some coffee from the kitchen?
}
"""

DLG ITM_2
You found an apple. Yum.

DLG ITM_3
Test

DLG ITM_4
You unlock the door. As you move through the threshold your hear a crash behind you.

DLG SPR_9
"""
{sequence
  - You unlocked the front door, thank you!
    Do you have a minute to talk about our lord and savior, the dark arts..?

  - 
}{
  - {item "Apple"} >= 1 ?
    Thanks for the apple, heres your hint: You are stronger now, you should try that gate around back.
  - else ?
    Just kidding, but if you bring me an apple I may have a hint for you.
}
"""

DLG SPR_a
"""
{
  - {item "Scroll"} >= 3 ?
    Dave was your son. He died due to your negligence.
  - else ?
    RIP
    Dave
}
"""

DLG SPR_b
"""
{
  - {item "Scroll"} >= 3 ?
    Jim was your son. He died due to your negligence.
  - else ?
    RIP
    Jim
}
"""

DLG SPR_c
"""
{shuffle
  - The gate is rusted shut. What a shame.
  - The gate is rusted shut. Those flowers are beautiful though.
  - The gate is rusted shut. You see names on the headstones, but you can't quite make them out.
}
"""

DLG ITM_5
Your returning memories have made you stronger. The gate breaks apart as you attempt to open it.

DLG ITM_6
How did you get here?  Cheater.

DLG SPR_d
"""
{
  - {item "Scroll"} >= 3 ?
    Its your partner's original headstone.
  - else ?
    This headstone has your partner's name on it. They have two headstones side by side?
}
"""

DLG SPR_e
"""
{
  - {item "Scroll"} == 3 ?
    It is a second headstone for your partner something must have happened after you brought them back.
  - {item "Scroll"} == 4 ?
    It is a second headstone for your partner something must have happened after you brought them back.
  - {item "Scroll"} == 5 ?
    It is a second headstone for your partner something must have happened after you brought them back.
  - {item "Scroll"} == 6 ?
    It is a second headstone for your partner something must have happened after you brought them back.
  - {item "Scroll"} >= 7 ?
    Its your partner's second headstone from the second time they died; the second time you murdered them.
  - else ?
    Your partner's name is on this headstone. They have two headstones side by side?
}
"""

DLG SPR_f
"""
{
  - {item "Apple"} >= 1 ?
    An apple a day doesn't really do much now that I'm dead, thanks though.
    
    Oh, and don't worry about the coffee. We don't actually have any.
  - else ?
    STRANGER DANGER.
    Just kidding.
    Can you grab me some coffee from the kitchen?
}
"""

DLG ITM_7
"""
{sequence
  - Scroll 1: Scrolls will help you regain your memories, you should try and collect any you come across.
  - Scroll 2: This is... well... this {wvy}was{wvy} your house, you lived here with your family.
  - Scroll 3: Your family {wvy}died{wvy} here, and it was {wvy}your{wvy} fault.
  - Scroll 4: You were adapt in dark magic though and you had heard of ways to bring people back.
  - Scroll 5: You did your research and you learned a spell to resurrect the dead.
  - Scroll 6:  You cast your new magic and it worked. But the results were grotesque.
Your family screamed in agony as they were {shk}ripped{shk} back into life. 
  - Scroll 7: You put your resurrected family back out of their misery by killing them again. Although they were gone, their cries still tormented you.
  - Scroll 8: You couldn't live with yourself. You saw their tortured faces whenever you closed your eyes.
  - Scroll 9: You couldn't live with yourself. You had to find a way out.
  - Scroll 10: You couldn't live with yourself. The screams were always ringing in your ears.
  - Scroll 11: You couldn't live with yourself. You saw their tortured faces whenever you closed your eyes.
  - Scroll 12: You couldn't live with yourself. You had to find a way out.
  - Scroll 13: You couldn't live with yourself. The cries haunted you always.
  - Scroll 14: You couldn't live with yourself. You saw their tortured faces whenever you closed your eyes.You couldn't live with yourself.
  - Scroll 15: You couldn't live with yourself. You had to find a way out.You couldn't live with yourself.
  - Scroll 16: You couldn't live with yourself. You couldn't live with yourself. You couldn't live with yourself. You couldn't live with yourself.
}
"""

DLG SPR_h
My love! you made it back to me! Can you hear me now, I've missed you so much... Its over now

DLG ITM_8
You recognize this body as your own.

DLG SPR_g
"""
This bookshelf looks rather fragile. It only has a few books on it compared to the rest.

You hear a knock from the front door.
"""

DLG SPR_i
"""
{shuffle
  - This bookshelf is full of books about necromancy. A book labeled "How to Bring Back Those You Love" catches your eye.
  - This bookshelf is full of books about necromancy. A book labeled "Don't Just Miss Them" catches your eye.
  - This bookshelf is full of books about necromancy. A book labeled "Necromancy 101" catches your eye.
}
"""

DLG SPR_j
"""
{shuffle
  - This bookshelf is full of books about necromancy. A book labeled "How to Bring Back Those You Love" catches your eye.
  - This bookshelf is full of books about necromancy. A book labeled "Don't Just Miss Them" catches your eye.
  - This bookshelf is full of books about necromancy. A book labeled "Necromancy 101" catches your eye.
}
"""

DLG ITM_9
The backdoor was not quite sealed shut.

END 0


END undefined
To escape the endless torment you committed suicide - your final act of necromancy.       The sight of your body is too much to handle. Your consciousness is pushed back towards the front of the house. Your recent memories slip away.

END undefinee


VAR a
42


