
Walkin' Golf!

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
8,86,204
128,159,255
255,255,255

PAL 1
27,145,39
105,255,105
255,255,255

PAL 2
89,89,89
171,171,171
255,255,255

PAL 3
189,62,142
255,121,210
255,255,255

PAL 4
191,186,21
233,255,136
255,255,255

PAL 5
90,7,150
160,19,255
255,255,255

ROOM 0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,b,b,f,f,b,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,0,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,f,f,f,f,f,f,f,f
f,f,b,0,0,0,0,b,b,b,b,b,b,b,b,f
f,f,b,0,0,0,0,0,0,0,0,0,0,0,b,f
f,f,b,0,0,0,0,0,0,0,0,0,a,0,b,f
f,f,b,0,0,0,0,0,0,0,0,0,0,0,b,f
f,f,b,b,b,b,b,b,b,b,b,b,b,b,b,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R1
ITM 0 10,14
EXT 12,11 2 2,4
PAL 1

ROOM 1
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,b,b,b,b,b,b,b,b,b,b,b,b,b,b,f
f,f,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,f,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,b,b,b,b,b,b,b,b,b,b,b,0,0,b,f
f,f,f,f,f,f,f,f,f,f,f,b,0,0,b,f
f,f,f,f,f,f,f,f,f,f,f,b,0,0,b,f
f,f,f,f,b,b,b,b,b,b,b,b,0,0,b,f
f,f,f,f,b,0,0,0,0,0,0,0,0,0,b,f
f,f,f,f,b,0,0,0,0,0,0,0,0,0,b,f
f,f,f,f,b,0,0,0,b,b,b,b,b,b,b,f
f,f,f,f,b,0,0,0,b,f,f,f,f,f,f,f
f,f,f,f,b,0,a,0,b,f,f,f,f,f,f,f
f,f,f,f,b,0,0,0,b,f,f,f,f,f,f,f
f,f,f,f,b,b,b,b,b,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R2
ITM 0 10,11
EXT 6,12 2 4,4
PAL 0

ROOM 2
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,c,b,d,b,e,b,b,g,b,h,b,i,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,0,b,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,a,0,0,0,0,0,0,0,0,0,0,0,0,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME OVERWORLD
EXT 2,2 0 4,3
EXT 4,2 1 3,2
EXT 6,2 3 2,2
EXT 9,2 4 13,13
EXT 2,13 5 4,11
EXT 11,2 6 12,13
EXT 13,2 7 2,2
PAL 2

ROOM 3
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,b,f,f,b,b,b,b,b,b,b,b,b,b,b,f
f,b,0,0,b,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,b,0,0,0,0,0,0,0,a,0,b,f
f,b,0,0,b,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,b,0,0,b,b,b,b,b,b,b,b,f
f,b,0,0,b,0,0,b,f,f,f,f,f,f,f,f
f,b,0,0,b,0,0,b,f,f,f,f,f,f,f,f
f,b,0,0,b,0,0,b,b,b,b,b,b,b,b,f
f,b,0,0,b,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,b,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,b,b,b,b,b,b,b,b,0,0,b,f
f,b,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,b,b,b,b,b,b,b,b,b,b,b,b,b,b,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R3
ITM 0 9,7
EXT 12,3 2 6,4
PAL 3

ROOM 4
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,b,b,b,b,b,b,b,f,f,f,f,f,f,f,f
f,b,0,0,0,0,0,b,b,b,b,b,b,b,b,f
f,b,0,a,0,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,0,0,0,b,b,b,b,b,b,0,b,f
f,b,b,b,b,b,b,b,0,0,0,0,0,0,b,f
f,b,0,0,0,0,0,0,0,b,b,b,b,b,b,f
f,b,0,0,b,b,b,b,b,b,f,f,f,f,f,f
f,b,0,0,b,f,f,f,f,f,f,f,f,f,f,f
f,b,0,0,b,f,b,b,b,b,b,b,b,b,b,f
f,b,0,0,b,f,b,0,0,0,0,0,0,0,b,f
f,b,0,0,b,b,b,0,0,0,0,0,0,0,b,f
f,b,0,0,0,0,0,0,0,b,b,b,0,0,f,f
f,b,0,0,0,0,0,0,0,b,f,b,0,0,f,f
f,b,b,b,b,b,b,b,b,b,f,b,b,b,b,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R4
ITM 0 5,9
EXT 3,3 2 9,4
PAL 4

ROOM 5
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,b,b,b,b,b,b,b,b,b,b,f,f,f
f,f,f,b,f,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,0,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,f,0,0,0,0,0,0,0,b,f,f,f
f,f,f,b,b,b,b,b,b,b,b,b,b,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME TEST CHAMBER
EXT 4,4 2 2,13
PAL 2

ROOM 6
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,b,b,b,b,b,b,b,b,b,b,b,b,b,b,f
f,b,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,0,0,0,0,0,0,0,0,0,0,b,f
f,b,0,0,0,b,b,b,b,b,b,b,0,0,b,f
f,b,0,0,0,b,f,f,f,f,f,b,0,0,b,f
f,b,0,0,0,b,b,b,b,b,f,b,0,0,b,f
f,b,0,0,0,b,0,0,0,b,f,b,0,0,b,f
f,b,0,0,0,b,0,a,0,b,f,b,0,0,b,f
f,b,0,0,0,b,0,0,0,b,f,b,0,0,b,f
f,b,0,0,0,b,0,0,0,b,f,b,0,0,b,f
f,b,0,0,0,b,0,0,0,b,f,b,0,0,b,f
f,b,0,0,0,0,0,0,0,b,f,b,0,0,b,f
f,b,0,0,0,0,0,0,0,b,f,b,0,0,b,f
f,b,b,b,b,b,b,b,b,b,f,b,f,f,b,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R5
ITM 0 8,5
EXT 7,8 2 11,4
PAL 4

ROOM 7
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,b,f,b,f,f,f,f,f,f,f,f,f,f,f,f
f,b,0,b,b,b,b,b,b,b,b,f,f,f,f,f
f,b,0,b,0,0,0,0,0,0,b,f,f,f,f,f
f,b,0,b,0,0,0,0,0,0,b,f,f,f,f,f
f,b,0,b,0,0,b,b,0,0,b,b,b,b,b,f
f,b,0,b,0,0,b,0,0,0,0,0,0,0,b,f
f,b,0,b,0,0,b,0,0,0,0,0,0,0,b,f
f,b,0,b,0,0,b,b,b,b,b,0,0,0,b,f
f,b,0,b,0,0,0,0,0,0,b,0,0,0,b,f
f,b,0,b,0,0,0,0,0,0,b,0,a,0,b,f
f,b,0,b,b,b,b,b,0,0,b,0,0,0,b,f
f,b,0,0,0,0,0,0,0,0,b,b,b,b,b,f
f,b,0,0,0,0,0,0,0,0,b,f,f,f,f,f
f,b,b,b,b,b,b,b,b,b,b,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME R6
ITM 0 12,3
EXT 12,10 2 13,4
PAL 5

TIL a
00111100
01000010
10011001
10111101
10111101
10011001
01000010
00111100
>
00111100
01100110
11000011
10011001
10011001
11000011
01100110
00111100
WAL false

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL c
00011000
00101000
00001000
00001000
00001000
00001000
00001000
00111110

TIL d
00011100
00100010
00000010
00000010
00001100
00010000
00100000
00111110

TIL e
00111110
00000100
00001000
00010000
00001100
00000010
00100010
00011100

TIL f
01010101
10101010
01010101
10101010
01010101
10101010
01010101
10101010
WAL false

TIL g
00001100
00010100
00100100
01000100
01000100
01111110
00000100
00000100

TIL h
01111110
01000000
01000000
01111100
00000010
01000010
01000010
00111100

TIL i
00111100
01000000
01000000
01111100
01000010
01000010
01000010
00111100

SPR A
00000000
00111100
01111110
01011010
01111110
01111110
00111100
00100100
>
00000000
00000000
00111100
01111110
01011010
01111110
00111100
00100100
POS 2 7,6

SPR c
00000000
00111100
01111110
01011010
01111110
01100110
01111110
00100100
>
00000000
00111100
01111110
01011010
01111110
01111110
01111110
00100100
DLG SPR_0
POS 0 11,5

SPR d
01111110
10000001
10100101
10000001
10000001
10111101
10000001
01111110
>
01111110
10000001
10100101
10000001
10111101
10111101
10000001
01111110
DLG SPR_1
POS 5 9,6

SPR e
00000000
00111100
01111110
01011010
01111110
01100110
01111110
00100100
>
00000000
00111100
01111110
01011010
01100110
01100110
01111110
00100100
DLG SPR_2
POS 2 13,13

ITM 0
00000000
00111100
01111110
01111110
01111110
01111110
00111100
00000000
NAME ball
DLG ITM_0

DLG ITM_0
You've found a lost BALL!

DLG SPR_0
"""
{sequence
  - {wvy}BE THE BALL{wvy}
  - Do you like golf?
  - You won't find any BIRDIES here.
  - You only BOGEY you'll find around here is wedged in your nose.
  - Just walk into the hole. 
  - It's basically golf, but with legs.
  - I heard something might happen if you find all the lost balls.
  - Talk to the other sorry sods around here for more hints.
  - This is the entire universe. Kinda lame, huh?
  - We only exist here because our DEV couldn't be bothered to learn C++ or download Unity
  - Become the ball. Find your hole. This is the secret of our contentment.
}{
  - {item "ball"} == 6 ?
    You've found all the balls! That means you've finished the only real task assigned to the player in this game. Enjoy the rest of your meaningless life.
  - else ?

}
"""

DLG SPR_1
"""
This is a test chamber. You'll find nothing interesting in here. Go away and do something more useful than bothering me. Can't you see how busy I am?!{
  - {item "0"} == 1 ?

  - else ?

}
"""

DLG SPR_2
"""
{
  - {item "ball"} == 6 ?
    You've found all the balls! That means you've finished the only real task assigned to the player in this game. Enjoy the rest of your meaningless life.
  - else ?
    Come back to me when you've found all the lost balls. Balls are like my thing. I'm pretty into balls.
}
"""

VAR a
42


