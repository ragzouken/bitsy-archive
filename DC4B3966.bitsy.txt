-' * a game by leaf * '-

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
NAME Lobby
132,106,106
198,199,196
162,153,158

PAL 1
NAME Fields
46,31,39
133,77,39
221,114,48

PAL 2
NAME Bedroom
137,2,62
234,99,140
255,217,218

PAL 3
NAME Moon Field
9,9,9
86,60,105
230,84,54

PAL 4
NAME Lulu
117,112,78
202,204,144
244,235,190

ROOM 0
c,a,a,a,a,a,a,a,o,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,0,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,e,a,a,a,a,a,a,d
c,a,m,l,l,l,l,l,0,l,l,l,l,n,a,d
c,a,g,0,0,0,0,0,0,0,0,0,f,k,a,d
c,a,g,e,0,0,0,0,0,0,0,0,0,k,a,d
c,a,g,0,0,0,0,0,f,0,0,0,0,k,a,a
c,a,g,0,q,0,0,0,0,0,0,0,q,0,0,t
c,a,g,0,0,0,0,0,0,e,0,0,0,k,a,a
c,a,g,0,0,0,f,0,0,0,0,0,0,k,a,d
c,a,g,e,0,0,0,0,0,0,0,0,0,k,a,d
c,a,g,0,0,0,0,0,q,0,0,0,e,k,a,d
c,a,h,i,i,0,i,i,i,i,i,i,i,j,a,d
c,a,a,a,a,e,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,0,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,p,a,a,a,a,a,a,a,a,a,d
NAME Lobby
ITM 0 11,5
ITM 1 4,10
ITM e 13,7
EXT 8,0 1 8,15
EXT 5,15 2 4,0
EXT 15,7 4 0,8
PAL 0

ROOM 1
0,f,e,q,e,0,e,e,0,e,e,e,f,e,e,e
f,f,0,0,0,e,0,0,0,0,f,e,f,f,e,0
0,0,0,0,0,0,0,0,e,0,e,e,e,f,e,e
r,0,e,e,e,e,0,0,0,0,0,0,e,e,e,f
0,e,q,e,q,e,e,0,0,0,0,0,e,q,e,0
e,f,f,e,e,0,0,q,0,0,f,0,f,e,0,0
f,e,f,f,e,e,0,0,0,0,0,0,e,f,e,e
f,e,e,e,e,e,0,f,0,0,0,0,0,f,e,e
e,e,e,q,0,0,0,q,0,0,f,q,0,0,e,q
f,e,0,0,0,0,0,0,0,e,0,q,0,f,e,e
f,f,e,0,0,0,0,0,q,f,0,0,0,e,q,e
e,q,e,0,0,0,f,0,0,0,0,0,e,e,f,e
e,e,e,0,f,e,0,0,0,0,q,e,e,f,f,e
q,e,0,0,e,q,0,0,f,0,e,e,q,f,e,f
e,0,0,e,q,e,0,0,0,0,0,f,e,e,q,0
0,e,f,f,e,0,0,0,p,0,0,0,0,f,e,e
NAME Fields
ITM 2 3,1
ITM 3 2,6
ITM 4 14,12
EXT 8,15 0 8,0
EXT 0,3 3 15,8
PAL 1

ROOM 2
c,a,a,a,o,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,0,0,0,a,a,a,a,a,a,a,a,d
c,a,a,0,i,i,0,i,i,i,i,i,0,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,k,0,0,0,0,0,0,0,0,g,a,a,d
c,a,a,0,l,l,l,l,l,l,l,l,0,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
c,a,a,a,a,a,a,a,a,a,a,a,a,a,a,d
NAME Bedroom
ITM 6 7,6
ITM 5 10,3
ITM 7 11,5
EXT 4,0 0 5,15
PAL 2

ROOM 3
0,u,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,u,0,0,u,0,0,0,0,0,u
0,0,r,r,0,0,0,0,0,0,0,0,0,u,0,0
u,0,r,r,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,u,0,0,0,0,u,0,0,0,0,0
0,0,0,0,0,0,0,0,u,0,0,0,0,0,0,u
0,u,0,0,0,0,0,0,0,0,0,0,u,0,0,0
s,s,s,s,s,s,s,s,s,s,s,s,s,s,s,s
0,q,0,0,0,0,0,0,f,0,0,0,0,0,0,t
q,0,0,q,0,0,0,q,q,0,0,0,0,q,0,0
q,0,0,q,q,0,0,0,0,0,0,0,0,0,q,q
0,q,0,0,0,q,e,0,0,e,q,0,e,0,e,q
q,0,0,0,0,0,0,q,f,0,q,0,q,q,q,0
0,0,e,e,0,0,0,0,f,0,0,0,0,0,0,0
0,0,q,f,f,e,0,0,0,e,e,0,0,e,0,q
q,0,0,f,0,q,0,0,q,0,q,e,e,q,q,0
NAME Moon Room
ITM 8 3,1
ITM 8 4,0
ITM 8 10,2
ITM 8 15,2
ITM 8 10,6
ITM 8 0,4
ITM 9 8,0
ITM 9 5,3
ITM 9 6,5
ITM 9 15,0
ITM 9 0,0
ITM 9 12,1
ITM 9 14,6
ITM 9 2,4
ITM 8 6,6
ITM 8 8,3
ITM 9 3,6
ITM 9 12,4
ITM 8 0,1
ITM 8 14,0
ITM 8 11,0
ITM a 14,8
ITM b 11,13
ITM c 5,9
ITM d 3,13
EXT 15,8 1 0,3
END undefined 1,9
PAL 3

ROOM 4
0,0,f,0,0,0,0,0,0,0,0,f,0,0,0,0
e,0,e,e,0,0,e,f,e,0,0,f,f,f,0,e
f,f,0,0,e,e,0,0,0,0,0,0,0,f,0,0
e,0,0,0,0,0,0,0,0,f,f,0,q,e,e,e
0,0,v,w,v,w,0,0,0,0,0,q,q,e,f,0
e,e,0,0,0,f,0,0,0,e,0,q,f,f,e,0
0,e,0,f,x,y,z,0,e,0,0,0,0,f,q,q
0,0,e,e,0,e,0,0,0,0,0,0,0,0,0,0
r,0,0,e,0,10,v,w,12,13,14,f,0,0,0,q
0,0,0,0,0,0,0,0,0,0,0,0,0,0,e,0
0,q,e,0,0,f,0,f,0,10,15,16,0,0,e,q
f,e,e,0,0,f,0,0,0,0,0,0,0,0,e,q
e,e,q,f,f,0,0,0,e,0,0,0,0,0,0,q
0,q,q,q,0,0,q,0,0,q,0,0,0,f,q,e
0,q,q,0,0,0,e,e,0,q,q,q,e,e,f,e
q,0,0,0,q,q,e,e,q,e,e,0,q,q,e,q
NAME Lulu Title
ITM f 1,8
EXT 0,8 0 15,7
PAL 4

TIL 10
00111110
01000000
01000000
01000000
01000000
01000000
01000000
00111110

TIL 11
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL 12
01000001
01100011
01010101
01001001
01000001
01000001
01000001
01000001

TIL 13
00011110
00100000
00100000
00111100
00000010
00000010
00000010
00111100

TIL 14
10000010
10000010
01000100
00111000
00010000
00010000
00010000
00010000

TIL 15
00010000
00101000
01000100
01000100
11111110
10000010
10000010
10000010

TIL 16
01111111
00001000
00001000
00001000
00001000
00001000
00001000
00001001

TIL a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME Wall
WAL true

TIL c
10010001
00001111
01000011
00010001
01000011
00001111
00010111
01000011
>
00000111
01001111
00000001
00100011
00010111
01000001
00010111
10000011
NAME Wall L

TIL d
10001001
11110000
11000010
10001000
11000010
11100000
11010000
10000010
>
11000100
11100010
11001000
10000001
11000010
10000000
11100010
11001000
NAME Wall R

TIL e
00100001
01001000
01000100
00000100
00100001
00010010
10010010
10000100
>
00100001
01001000
01000100
00000100
00100001
00010010
10010010
10000100
NAME Grass

TIL f
10100000
01000001
10100001
01001010
01000100
00001010
10000100
10000100
>
00000000
10100010
01000001
10100000
01001010
00000100
01001010
10000100
NAME Flowers

TIL g
11110101
01110101
11010101
01110101
11010101
01110101
11010101
11110101
NAME Wall Left 2
WAL true

TIL h
11110101
11010100
01110111
11010000
01111111
11101011
01111111
11010101
NAME Wall Corner 2
WAL true

TIL i
11111111
00000000
11111111
00000000
11111111
11010101
11111111
10101011
NAME Wall Bottom 2
WAL true

TIL j
10101111
00101011
11101110
00001011
11111110
11010111
11111110
10101011
NAME Wall Corner 2.1
WAL true

TIL k
10101111
10101011
10101110
10101011
10101110
10101011
10101110
10101111
NAME Wall Right 2
WAL true

TIL l
11010101
11111111
10101011
11111111
00000000
11111111
00000000
11111111
NAME Wall 2 top
WAL true

TIL m
11010111
01111111
11101010
01111111
11010000
01110111
11010100
11110101
NAME Wall Corner 2.2
WAL true

TIL n
11010101
11111111
10101110
11111011
00001110
11101011
00101110
10101111
NAME Wall Corner 2.3
WAL true

TIL o
11111111
11101101
01001110
10010101
00000010
10100100
00000000
01001001
NAME Exits/Entrance

TIL p
01000000
00001010
10000000
01001001
00000010
01010100
10111101
11111111
NAME Exits/Entrance 1

TIL q
00000000
11011010
00100010
00100000
00000000
00011011
01000100
01000100
>
10001000
01010001
00100010
00100000
00010001
00001010
10000100
01000100
NAME Saplings

TIL r
10001010
11000000
11110000
11000010
11001000
11100001
11000000
11100110
NAME Exits/Entrance 2

TIL s
11111111
01000100
11111111
01000100
01000100
11111111
01000100
11111111
NAME Fence
WAL true

TIL t
10010111
00000001
00101111
10000011
00000011
00100001
10000111
00101111
NAME Exits/Entrances 3

TIL u
00000000
00001000
00011100
00111110
00011100
00001000
00000000
00000000
>
00000000
00000000
00001000
00011100
00001000
00000000
00000000
00000000

TIL v
01000000
01000000
01000000
01000000
01000000
01000000
01000000
01111110

TIL w
01000010
01000010
01000010
01000010
01000010
01000010
01000010
00111100

TIL x
11111110
00010000
00010000
00010000
00010000
00010000
00010000
00010000

TIL y
01000010
01000010
01000010
01111110
01000010
01000010
01000010
01000010

TIL z
01111110
01000000
01000000
01000000
01111000
01000000
01000000
01111110

SPR 10
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME bookcase 5
DLG SPR_f
POS 0 6,4

SPR 11
01011111
01001010
01001010
01001010
11111111
10101011
11010101
10101011
NAME Well
DLG SPR_g
POS 3 0,9

SPR 12
00000000
00000000
00000000
00000000
00000000
00000000
01111110
01111110
NAME Well 1
POS 3 0,8

SPR 13
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Well 2
POS 3 0,10

SPR 14
00000000
00111100
00100100
00011000
01111110
01000010
01000010
01000010
NAME Veggies Bowl
DLG SPR_h
POS 2 5,8

SPR 15
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME bookcase 5
DLG SPR_i
POS 0 9,4

SPR 16
00010000
00101000
00010000
00111000
00101000
01111100
01000100
01000100
NAME Flower Vase
DLG SPR_j
POS 0 12,10

SPR 17
00000000
00000000
00000000
00000000
01111100
00111110
01111100
00111110
NAME Bookstack
DLG SPR_k
POS 0 11,10

SPR 18
00000000
00001000
00010100
10111000
01010100
01010110
00111000
00000000
>
00000000
00000000
00001000
00010100
10111000
01010100
01010110
00111000
NAME Bee
DLG SPR_l
POS 1 13,8

SPR 19
00000000
00010000
00101000
00101000
01101100
00010000
00010000
00010000
>
00010000
00101000
00101000
01101100
00010000
00010000
00010000
00010000
NAME Flower
DLG SPR_m
POS 1 14,7

SPR A
00000000
01010000
01110001
01110001
01111110
00111100
00100100
00100100
>
00000000
00000000
01010000
01110001
01110001
01111110
00111100
00100100
POS 4 13,10

SPR a
00000000
01010001
01110010
01110010
01111100
00111100
00100100
00100100
>
00000000
01010100
01110010
01110010
01111100
00111100
00100100
00100100
NAME Cornelius
DLG SPR_0
POS 0 10,9

SPR b
00000000
00000000
00000000
00010000
00010000
00010000
00010000
00011110
NAME Lobby Chair
POS 0 10,5

SPR c
00000000
00000000
00000000
00001000
00001000
00001000
00001000
01111000
NAME Lobby Chair
POS 0 12,5

SPR d
00000000
00100100
00100100
00111100
00111100
00011011
00011111
00111100
>
00000000
00000000
00100100
00100100
00111100
00111100
00011011
00111111
NAME Ella
DLG SPR_1
POS 0 7,4

SPR e
00000000
01010001
01110010
01110010
00111100
01111110
00000000
00000000
>
00000000
01010100
01110010
01110010
00111100
01111110
00000000
00000000
NAME Sitting cat
DLG SPR_2
POS 1 11,3

SPR f
01111110
01000010
01000010
01111110
01111110
01111110
01111110
01111110
NAME Bed
DLG SPR_3
POS 2 4,4

SPR g
01111110
01111110
00000000
00011100
00100010
01100011
00101010
00010100
>
01111110
01111110
00000000
00000000
00011100
00100010
01100011
00111110
NAME Bed extended
DLG SPR_4
POS 2 4,5

SPR h
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME Bookcase
DLG SPR_5
POS 2 11,6

SPR i
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME Bookcase 1
DLG SPR_6
POS 2 10,6

SPR j
00000000
00111100
00100100
00011000
01111110
01000010
01000010
01000010
NAME Fruit bowl
DLG SPR_7
POS 2 4,8

SPR k
00000000
01110000
01011000
01010100
01110100
00101000
00010000
00000000
NAME Paper
DLG SPR_8
POS 2 11,8

SPR l
00000000
01110000
01011000
01010100
01110100
00101000
00010000
00000000
NAME Paper 2
DLG SPR_9
POS 2 8,4

SPR m
00000000
00000000
00000000
00010000
00010000
00010000
00010000
00011110
NAME Bedroom Chair
POS 2 9,3

SPR n
00000000
00000000
00000000
00001000
00001000
00001000
00001000
01111000
NAME Bedroom Chair
POS 2 11,3

SPR o
00000000
11111111
10011001
10110001
10110101
10011001
11111111
00000000
NAME Moon
DLG SPR_a
POS 2 8,7

SPR p
00000000
00000111
00011111
00111111
01111000
11110000
11100000
11000000
>
00000000
00000111
00011111
00111111
01111000
11110000
11100000
11000000
NAME Moon
POS 3 2,1

SPR q
11000000
11000000
11000000
11000000
11000000
11000000
11100000
01110000
>
11000000
11000000
11000000
11000000
11000000
11000000
11100000
01110000
NAME Moon 1
POS 3 2,2

SPR r
01111111
00111111
00001111
00000000
00000000
00000000
00000000
00000000
NAME Moon 2
POS 3 2,3

SPR s
11110000
11100000
11000000
00000000
00000000
00000000
00000000
00000000
NAME Moon 3
POS 3 3,3

SPR t
00000000
11000000
11100000
11110000
00111000
00001100
00000100
00000000
NAME Moon 4
POS 3 3,1

SPR u
00000000
00000000
00000000
00000000
00000000
00000100
00001100
00111000
NAME Moon 5
POS 3 3,2

SPR v
00000001
00000001
00000001
00000001
00000001
00000001
00000000
00000000
NAME Moon 6
POS 3 1,2

SPR w
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000001
NAME Moon 7
POS 3 1,1

SPR x
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME Bookcase 2
DLG SPR_b
POS 0 3,4

SPR y
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME bookcase 3
DLG SPR_c
POS 0 4,4

SPR z
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01111110
NAME bookcase 4
DLG SPR_d
POS 0 5,4

ITM 0
00010000
00001000
00010000
00111100
01100100
00011000
01111110
01000010
>
00001000
00010000
00001000
00111100
01100100
00011000
01111110
01000010
NAME Coffee
DLG ITM_0

ITM 1
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star
DLG ITM_1

ITM 2
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 1
DLG ITM_4

ITM 3
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 2
DLG ITM_3

ITM 4
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 3
DLG ITM_2

ITM 5
00010000
00001000
00010000
00111100
01100100
00011000
01111110
01000010
>
00001000
00010000
00001000
00111100
01100100
00011000
01111110
01000010
NAME Coffee 1
DLG ITM_5

ITM 6
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 4
DLG ITM_6

ITM 7
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 5
DLG ITM_7

ITM 8
00000000
00000000
00000000
00000000
00000000
00000000
01000000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
01000000
00000000
NAME Small

ITM 9
00000000
00000010
00000000
00000000
00000000
00000000
00000000
00000000
>
00000010
00000111
00000010
00000000
00000000
00000000
00000000
00000000
NAME Small 1

ITM a
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 6
DLG ITM_8

ITM b
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 7
DLG ITM_9

ITM c
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 8
DLG ITM_a

ITM d
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 9
DLG ITM_b

ITM e
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 10
DLG ITM_c

ITM f
00001000
00101010
00011100
01110111
00011100
00101010
00001000
00000000
>
01000001
00101010
00011100
00110110
00011100
00101010
01000001
00000000
NAME Star 11
DLG ITM_d

DLG ITM_0
"""
You found a piping hot coffee!
You keep it in your pocket for later. You take the table too 'cause {wvy}{rbw}why not (~:{rbw}{wvy}
"""

DLG ITM_1
It tastes like stardust, but more {clr1}bitter{clr1} than you would've thought.

DLG ITM_2
Your favorite season is arriving soon.

DLG ITM_3
You have to check on the Harvest Moon to see if it's ripe for tomorrow's full moon.

DLG SPR_2
{clr3}Ruby the Sitting Cat{clr3}: The Harvest Moon is pretty {rbw}{wvy}rad{wvy}{rbw}, but something is off about her...

DLG SPR_3
Not a very cozy bed, but it's all you have.

DLG ITM_5
More Coffee, but it's kinda cold! You take it anyway.

DLG ITM_6
You don't like coming in here because it's a {wvy}little{wvy} lonely.

DLG SPR_4
It's your favorite toy! {wvy}{rbw}A little squishy birb.{rbw}{wvy}

DLG SPR_5
"""
{cycle
  - A bunch of books on Autumn, most of them are notes you took last year.
  - There's one called {clr2}"Autumn Equinox"{clr2}.
  - "Occurs during the last week of September! 2015's equinox was on Wednesday, September 23, 2015 at 1:21 A.M. PST"
  - That's all you have, 'cause you got lazy.
}
"""

DLG SPR_6
"""
{cycle
  - There are a bunch of recipe books on this shelf.
  - The oldest looking book is: {clr3}"How to make Spaghetti 101"{clr3}
  - You love Spaghetti, but you suck at making it /^:
  - The next book is {clr3}"How to bake bread"{clr3}.
  - You don't know why you have this book, you don't even have an oven!
}
"""

DLG SPR_7
"""
{sequence
  - A bowl filled with your favorite fruits: strawberry, blueberries, apples, grapes, pineapple...
  - So, pretty much {rbw}all the fruits{rbw}.
}
"""

DLG SPR_8
"""
{sequence
  - Just some papers that you left on the ground.
  - There are random paw prints on it, your toe beans are looking {wvy}healthy{wvy}.
}
"""

DLG SPR_9
"""
{sequence
  - Papers on different assortments of fruit arrangements
  - Your favorite are the {clr1}"Apple Bunnies"{clr1}.
  - They're very cute.
}
"""

DLG SPR_a
"""
{sequence
  - A pretty painting of the moon.
  - She's pretty {clr1}{wvy}rad{wvy}{clr1}!
  - You like sitting down on top of it so you just leave it on the ground.
}
"""

DLG ITM_7
You tried to fill this room with stuff you love but it still feels empty... plus there's no windows so it kinda sucks anyway.

DLG SPR_0
"""
{cycle
  - {clr3}Cornelius the Stray{clr3}: You woke up late again, {shk}and{shk} you left the front door unlocked.
  - You're going to get robbed one day, you know. 
  - Did you know everyone calls you Lulu the Clumsy Cat?
  - Guess it's better than being called 'the Stray' though! kekekek
  - It's okay, it has a nice {wvy}ring{wvy} to it. Make sure you come back early tonight, me and Ella are going to cook {wvy}{rbw}Catnip Soup{rbw}{wvy}!
}
"""

DLG ITM_8
The moon's not very {clr3}{wvy}harvesty...{wvy}{clr3}

DLG ITM_9
I think she's missing some pieces, she won't become a Harvest Moon in time!

DLG ITM_4
She can be seen in the Moon field, to the left.

DLG ITM_a
She looks kind of tired.

DLG ITM_b
Maybe their souls have returned to Earth... 

DLG SPR_b
"""
{cycle
  - {clr1}The most recent recording of the Harvest Moon.{clr1}
  - {clr1}2015-2018:{clr1} Each year, she grows dimmer every day.
  - Replenishing the {clr1}Moon Water{clr1} helps her feel hydrated! But I think we're out this year, a lot of people needed to water their crops and regular water wasn't working...
  - {clr1}2017{clr1}: I'm on my ninth life, but it should be okay! Next year looks promising.
}
"""

DLG SPR_c
"""
{cycle
  - The {clr1}Moon Water{clr1} can be found in a well to the left of the {clr2}Moon Field{clr2}.
  - I like the {clr2}Moon Field{clr2}, it's nice to lay on the grass.
  - Sometimes the {clr3}stardust{clr3} falls, they're kind of bitter. But they taste good with coffee!
}
"""

DLG SPR_d
"""
{cycle
  - This is where I keep my recordings of important people in my life! 
  - {clr3}Ella the Bunny{clr3}: a weirdo. She only likes to come in twice every three months.

  - She always eats the carrots I grab at the farmer's market, but it's okay. I like the company!
  - {clr3}Cornelius the Stray{clr3}: wellknown in the town. He's nice, I let him stay in my house even though all he does is lay around.
  - He reminds me about the Harvest Moon every year, so it's convenient!
  - I forget things easily (lol).
  - {clr3}Ruby the Sitting Kitty{clr3}: always sits. Never. Ever. Stands. I think she drags herself on her legs lolol. 
  - {rbw}THE MOON{rbw}: best bae. {wvy}{clr1}Love{clr1}{wvy} her! She's a darling. I think she's sad though...
}
"""

DLG SPR_1
"""
{cycle
  - {clr3}Ella the Bunny{clr3}: {shk}{rbw}YO!!!!!{rbw}{shk}
  - Stole some carrots again xd don't worry, I got you some plums tho ouob
  - Don't forget to check your bedroom before you go out! It was super messy when I went to get the carrots out of your basket...
}
"""

DLG SPR_e
"""
{cycle
  - Some old photos of mom and dad
  - 
}
"""

DLG SPR_f
"""
{cycle
  - Some old photos of mom and dad.
  - (let's not look at those, tho)
}
"""

DLG SPR_g
"""
{
  - {item "Star 9"} == 1 ?
    It's empty. Maybe e overused the water this year...
}
"""

DLG SPR_h
Some veggies given to me by my neighbors! This is probably where Ella took the carrots out...

DLG SPR_i
"""
{cycle
  - {wvy}{clr1}The Moon Harvester{clr1}{wvy}. This has details on my job as the {clr3}Harvest Moon Keeper{clr3}!
  - I have to make sure the {clr2}Moon Well{clr2} always has {clr2}Moon Water{clr2}, or else she will fade away. My job is to make sure the moon is always there, no matter what! If it's empty, it's good to replenish it before the {clr1}Autumn Equinox{clr1}.
  - If all else fails, the {clr3}Harvest Moon Keeper{clr3} can fuse with the soul of the {clr2}Moon{clr2} to replenish the well. One life is equivalent to one year of replenished {clr2}Moon Water{clr2}. 
  - This usually occurs during a drought, so it doesn't happen too often!
  - Good thing cats have nine lives;;
}
"""

DLG SPR_j
Mom's favorite flower, a {clr2}Lotus{clr2}.

DLG SPR_k
"""
{cycle
  - Dad's favorite books, one of them is {clr1}The Meaning of Flowers{clr1}. He bought it after marrying mom, since she loved flowers.
  - Mom's favorite flower, {clr2}Lotus{clr2}, means rebirth. She liked the idea of being reborn into a bee, after her nine lives were up.
  - That way, she could always be around fresh flowers.
}
"""

DLG SPR_l
"""
{cycle
  - {clr3}Bee{clr3}: {shk}Bzz!{shk}
  - It crawls on my paw for a little, then lands back into the grass filled flowers.
}
"""

DLG SPR_m
A flower about to bloom.

DLG ITM_c
Many of the objects have more than one dialogue.

DLG ITM_d
Make sure to interact with everything before going to the Well!

END 0
this year.

END undefined
The well is empty... It'll be more sad to say goodbye to everyone, I think.

VAR a
42

