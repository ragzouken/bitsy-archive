Fallen Star

# BITSY VERSION 5.1

! ROOM_FORMAT 1

PAL 0
NAME camp
47,92,107
90,171,201
255,255,255

PAL 1
NAME flora
47,92,107
241,255,246
255,255,255

PAL 2
255,255,255
255,255,255
255,255,255

ROOM 0
b,b,b,0,0,0,c,a,e,0,0,f,f,0,e,e
b,b,b,e,e,e,a,a,e,0,f,0,c,0,e,e
0,0,0,e,0,0,a,c,e,0,e,e,c,0,0,e
b,b,0,0,b,0,a,0,e,0,e,b,b,c,c,0
b,b,0,b,b,c,a,a,0,0,e,b,b,b,c,0
b,0,0,b,b,0,c,a,c,0,0,0,0,0,0,0
0,0,d,0,0,0,0,a,a,0,e,e,e,e,0,0
0,0,0,0,e,e,g,g,g,g,0,e,e,e,b,b
0,b,b,0,e,e,g,g,g,g,0,e,e,e,b,b
0,b,b,b,0,0,d,a,a,0,b,b,e,0,b,b
c,0,b,0,0,0,0,a,c,0,b,b,f,f,f,0
c,c,0,e,e,e,0,a,0,e,0,0,f,0,f,d
0,c,0,0,e,e,0,a,c,e,0,0,f,f,f,d
b,b,b,0,0,a,a,a,0,0,e,e,e,e,d,0
b,b,b,d,0,a,a,0,b,b,0,0,b,b,b,b
b,b,d,0,0,c,a,a,b,b,0,0,b,b,b,b
NAME scene1
ITM 0 6,12
EXT 10,15 1 10,0
EXT 11,15 1 11,0
EXT 0,2 2 15,6
PAL 0

ROOM 1
0,f,0,0,0,0,a,a,0,b,0,0,b,0,0,0
f,0,0,f,f,f,a,i,0,b,0,0,b,0,d,0
f,0,e,e,0,0,a,a,0,b,b,b,b,c,e,0
f,0,0,e,0,a,a,a,0,i,i,0,0,0,0,f
0,e,e,e,0,a,c,0,0,0,i,f,0,e,0,0
0,0,0,0,0,a,i,0,f,0,0,0,0,0,f,0
f,c,c,c,b,a,0,0,0,b,0,f,0,f,0,0
0,c,d,c,0,a,a,f,f,0,b,i,i,0,e,b
0,c,c,c,0,a,a,c,f,f,0,0,i,e,b,b
0,0,0,0,0,c,a,a,0,0,0,b,0,e,b,b
0,0,f,f,0,0,a,a,0,0,e,0,0,0,d,0
e,0,f,0,b,c,0,a,e,0,0,f,0,0,0,0
b,b,b,0,0,c,a,a,0,f,f,0,d,0,e,e
b,e,0,0,0,a,a,0,0,0,0,f,f,0,0,0
0,b,0,f,0,a,a,d,e,0,f,i,f,f,f,f
0,0,0,f,f,a,a,0,0,e,i,0,f,f,0,0
NAME scene2
ITM 1 7,7
ITM 0 2,7
EXT 11,15 3 11,0
EXT 9,15 3 9,0
EXT 10,15 3 10,0
EXT 8,15 3 8,0
EXT 12,15 3 12,0
EXT 7,15 3 7,0
EXT 13,15 3 13,0
EXT 14,15 3 14,0
EXT 15,15 3 15,0
EXT 10,0 0 10,15
EXT 11,0 0 11,15
PAL 0

ROOM 2
k,k,k,k,k,k,k,k,k,k,k,k,k,k,k,k
k,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
k,b,d,0,d,0,d,0,d,0,d,j,d,0,d,0
k,b,d,j,0,f,0,0,i,0,0,0,f,f,0,f
k,b,d,i,f,f,0,0,0,0,f,f,0,f,0,f
k,b,d,0,f,0,i,f,f,j,c,c,c,c,c,c
k,b,c,c,c,c,c,c,c,c,h,h,h,h,h,h
k,b,c,h,h,h,h,h,h,h,h,c,c,c,c,c
k,b,c,c,c,c,c,c,c,c,c,0,j,0,0,0
k,b,d,i,j,0,0,0,0,0,0,0,i,f,0,i
k,b,d,f,0,i,f,f,c,a,a,i,0,f,f,0
k,b,d,f,i,f,f,0,a,a,a,a,0,f,i,0
k,b,d,f,0,j,i,0,a,a,0,a,0,j,0,0
k,b,d,0,d,0,d,0,d,0,d,0,d,0,d,0
k,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
k,k,k,k,k,k,k,k,k,k,k,k,k,k,k,k
NAME secret room
ITM 0 3,13
EXT 15,6 0 0,2
PAL 0

ROOM 3
0,0,e,0,0,a,a,0,f,0,b,b,f,0,i,0
f,b,0,b,0,c,a,0,e,e,0,0,b,f,f,0
e,0,b,b,0,a,a,0,i,i,e,0,0,b,b,0
0,d,d,i,a,a,a,i,i,e,0,e,0,i,b,0
i,0,d,0,a,a,0,0,0,e,0,e,e,0,0,0
0,b,0,f,0,a,a,0,b,b,b,i,e,e,0,c
0,e,b,c,f,i,a,0,i,0,0,0,0,0,0,0
e,b,b,0,f,a,a,e,e,c,0,b,0,i,0,b
e,e,0,d,a,a,0,e,0,0,e,e,0,0,b,b
i,i,i,a,a,0,0,e,d,e,0,0,c,0,e,0
i,a,a,a,i,0,e,e,0,b,b,e,e,d,d,0
a,a,a,a,0,0,0,b,b,b,0,0,e,e,d,d
a,a,a,a,c,e,e,e,0,0,d,f,0,0,e,0
a,a,a,a,0,e,d,d,f,f,i,0,b,0,0,0
a,a,a,i,e,e,f,f,0,e,i,i,0,f,e,0
0,e,0,0,e,0,0,e,f,f,f,0,0,f,f,0
NAME scene3
ITM 0 6,4
EXT 0,15 4 15,15
EXT 7,0 1 7,15
EXT 8,0 1 8,15
EXT 9,0 1 9,15
EXT 10,0 1 10,15
EXT 11,0 1 11,15
EXT 12,0 1 12,15
EXT 13,0 1 13,15
EXT 14,0 1 14,15
EXT 15,0 1 15,15
PAL 0

ROOM 4
i,0,0,0,f,f,0,0,0,0,f,0,0,e,e,e
0,b,b,0,0,i,e,e,f,c,0,0,j,0,0,0
h,h,h,d,e,0,0,0,0,e,e,f,f,0,i,0
0,c,h,i,e,e,0,j,0,e,0,0,f,0,0,0
h,h,h,i,0,0,0,0,0,0,0,d,0,b,b,0
0,j,h,0,f,f,c,0,b,b,b,0,b,0,f,0
d,0,h,h,f,0,0,b,0,0,0,0,0,0,f,0
0,0,b,h,0,0,j,0,f,f,0,e,0,0,c,0
0,b,e,h,d,f,0,0,e,e,0,a,a,0,j,0
0,b,0,0,0,0,f,0,a,a,a,a,a,a,0,0
c,b,b,0,0,j,a,a,a,a,a,a,a,a,a,d
0,j,b,b,0,0,a,a,a,a,a,a,a,a,a,a
e,0,0,0,0,a,a,a,a,a,a,a,a,a,a,a
e,0,e,0,0,a,a,a,a,a,a,a,a,a,a,a
0,d,e,e,e,c,a,a,a,a,a,a,a,a,a,a
0,j,0,0,0,0,f,f,f,f,0,0,0,0,0,0
NAME scene4
EXT 0,4 5 15,4
EXT 0,2 5 15,2
EXT 15,15 3 0,15
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,k,k,k,k,k,k,0,0,0,0,0,k,k,k,k
0,k,h,h,h,0,k,k,k,k,k,k,k,h,h,h
0,k,h,k,k,k,k,k,h,h,h,h,h,h,k,k
0,k,h,k,0,0,0,k,h,k,k,k,k,h,h,h
0,k,h,k,k,k,k,k,h,k,0,0,k,k,k,k
0,k,h,h,h,h,h,h,h,k,0,0,k,0,k,0
0,k,k,h,k,k,h,k,k,k,k,k,k,h,k,0
k,k,k,h,k,k,h,h,h,h,h,k,k,h,k,0
k,h,h,h,k,k,k,k,k,k,h,k,k,h,k,0
k,h,k,h,k,k,k,k,k,k,h,h,h,h,k,0
k,h,k,h,k,k,0,h,h,h,h,k,k,k,k,0
k,h,k,h,h,k,k,k,k,k,k,k,k,k,k,0
k,h,k,k,h,h,h,h,h,h,h,h,h,0,k,0
k,0,k,k,k,k,k,k,k,k,k,k,k,k,k,0
k,k,k,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME the scene
ITM 0 5,2
ITM 2 6,11
ITM 3 13,13
EXT 15,2 6 0,2
EXT 15,4 6 0,4
END 0 1,14
PAL 0

ROOM 6
i,0,f,0,f,f,0,e,f,0,f,0,0,e,e,e
0,b,b,0,f,i,e,e,f,c,0,0,j,0,0,0
h,h,h,d,e,0,0,0,0,e,e,f,f,0,i,0
0,c,h,i,e,e,0,j,0,e,0,0,f,0,0,0
h,h,h,i,0,0,0,0,e,0,0,d,0,b,b,0
0,j,h,0,f,f,c,0,e,e,b,0,b,0,f,0
d,0,h,h,f,0,0,b,e,0,0,0,f,0,f,0
0,0,b,h,0,0,j,0,e,f,0,e,f,f,c,0
0,b,e,h,d,f,0,e,e,e,0,a,a,0,j,0
0,b,0,0,0,0,f,e,a,a,a,a,a,a,0,0
c,b,b,0,0,j,a,a,a,a,a,a,a,a,a,d
0,j,b,b,0,0,a,a,a,a,a,a,a,a,a,a
e,0,e,e,0,a,a,a,a,a,a,a,a,a,a,a
e,0,e,0,0,a,a,a,a,a,a,a,a,a,a,a
0,d,e,e,e,e,e,a,a,a,a,a,a,a,a,a
0,j,0,0,0,0,f,f,f,f,0,0,0,0,0,0
NAME the scene 2
ITM 0 14,9
EXT 0,4 5 15,4
EXT 0,2 5 15,2
EXT 15,15 7 0,15
PAL 0

ROOM 7
f,c,0,0,0,a,a,b,0,e,0,b,b,b,d,0
f,f,f,b,0,i,a,0,e,e,0,0,d,b,b,0
e,0,b,b,0,a,a,d,i,i,e,i,0,b,b,0
0,d,d,0,a,a,a,i,i,f,f,f,0,0,b,0
f,0,d,0,a,a,c,0,0,0,i,e,e,0,c,0
0,b,0,e,0,a,a,0,b,b,b,d,e,e,i,0
0,b,b,e,0,i,a,0,e,0,0,e,e,0,e,0
b,b,b,e,0,a,a,e,0,c,0,e,0,e,e,b
0,0,0,c,a,a,0,e,0,0,f,e,f,0,b,b
i,i,i,a,a,0,0,e,f,i,f,0,c,0,0,0
i,a,a,a,i,c,e,e,e,b,b,e,e,d,d,0
a,a,a,a,0,i,0,b,b,b,0,i,e,e,d,d
a,a,a,a,0,0,f,0,0,f,0,0,0,0,e,0
a,a,a,a,c,e,d,d,f,f,i,f,0,0,0,0
a,a,a,i,e,e,f,e,e,0,i,c,0,f,0,0
0,e,0,0,e,0,0,0,f,f,f,0,0,f,f,c
NAME the scene 3
EXT 0,15 4 15,15
EXT 7,0 8 7,15
EXT 8,0 8 8,15
EXT 9,0 8 9,15
EXT 10,0 8 10,15
EXT 11,0 8 11,15
EXT 12,0 8 12,15
EXT 13,0 8 13,15
EXT 14,0 8 14,15
EXT 15,0 8 15,15
PAL 0

ROOM 8
0,f,0,0,0,0,a,a,0,b,0,0,b,e,0,0
f,0,b,f,f,f,a,i,0,b,d,d,b,0,d,e
0,b,e,0,c,f,a,a,e,b,b,b,b,e,e,e
f,b,e,e,0,0,0,a,e,0,0,0,0,c,e,0
d,0,0,e,0,a,a,a,0,i,i,b,e,e,0,0
0,e,e,e,0,a,c,0,0,0,i,0,e,e,e,e
0,c,c,c,0,a,i,0,f,c,0,e,0,b,b,e
0,c,d,c,0,a,a,f,f,0,0,i,i,0,e,b
0,c,c,c,0,a,a,c,f,f,e,0,i,e,b,b
b,b,0,0,0,c,a,a,0,b,0,e,0,e,b,b
0,b,b,f,0,0,a,a,d,0,e,0,e,0,d,0
e,0,f,0,b,c,0,a,e,0,0,f,e,e,0,0
b,b,b,0,0,c,a,a,0,f,f,0,d,0,c,e
b,e,0,e,0,a,a,0,e,0,0,f,f,0,0,0
0,b,0,f,0,a,a,d,e,0,f,i,f,f,f,f
0,d,0,f,f,a,a,0,0,e,i,0,f,f,0,0
NAME the scene 4
EXT 11,15 7 11,0
EXT 9,15 7 9,0
EXT 10,15 7 10,0
EXT 8,15 7 8,0
EXT 12,15 7 12,0
EXT 7,15 7 7,0
EXT 13,15 7 13,0
EXT 14,15 7 14,0
EXT 15,15 7 15,0
EXT 10,0 9 10,15
EXT 11,0 9 11,15
PAL 0

ROOM 9
b,b,b,0,0,0,c,a,e,0,0,f,f,0,e,e
b,b,b,e,e,e,a,a,e,0,f,0,c,0,e,e
0,0,0,e,0,0,a,c,e,0,e,e,c,0,0,e
b,b,0,0,b,0,a,0,e,0,e,b,b,c,c,0
b,b,0,b,b,c,a,a,0,0,e,b,b,b,c,0
b,0,0,b,b,0,c,a,c,0,0,0,0,0,0,0
0,0,d,0,0,0,0,a,a,0,e,e,e,e,0,0
0,0,0,0,e,e,g,g,g,g,0,e,e,e,b,b
0,b,b,0,e,e,g,g,g,g,0,e,e,e,b,b
0,b,b,b,0,0,d,a,a,0,b,b,e,0,b,b
c,0,b,0,0,0,0,a,c,0,b,b,f,f,f,0
c,c,0,e,e,e,0,a,0,e,0,0,f,0,f,d
0,c,0,0,e,e,0,a,c,e,0,0,f,f,f,d
b,b,b,0,0,a,a,a,0,0,e,e,e,e,d,0
b,b,b,d,0,a,a,0,b,b,0,0,b,b,b,b
b,b,d,0,0,c,a,a,b,b,0,0,b,b,b,b
NAME the scene 5
EXT 10,15 8 10,0
EXT 11,15 8 11,0
EXT 3,0 a 3,15
EXT 4,0 a 4,15
EXT 5,0 a 5,15
EXT 6,0 a 6,15
PAL 0

ROOM a
f,j,i,b,0,a,a,a,0,d,b,0,f,f,f,0
d,0,0,b,b,a,a,a,a,a,b,0,f,f,0,0
b,e,0,b,b,c,j,a,a,a,0,b,0,0,j,0
0,e,e,e,0,0,0,a,a,a,0,0,i,e,b,0
0,0,0,e,0,d,g,g,g,g,g,0,0,0,0,0
0,j,e,0,e,b,g,g,g,g,g,0,e,c,j,0
b,0,0,f,0,0,0,a,a,a,0,c,e,0,0,0
0,c,0,0,f,f,0,j,0,a,0,e,e,0,0,i
0,0,b,0,0,c,0,0,d,a,0,j,0,e,f,0
c,0,b,b,e,i,e,0,a,a,0,c,0,e,0,0
0,i,e,b,b,e,e,0,a,i,0,0,0,d,b,0
e,0,b,b,b,e,d,a,a,e,0,b,b,e,b,j
0,e,i,c,0,e,a,a,0,0,0,e,b,b,b,0
0,0,b,0,0,0,a,i,0,j,f,0,e,0,f,0
j,0,c,0,0,0,a,a,0,0,f,c,0,0,b,b
e,e,e,e,0,j,0,a,d,0,f,f,f,f,c,b
NAME END SCENE
EXT 15,5 b 0,5
EXT 15,4 b 0,4
END 0 4,0
END 0 3,0
END 0 8,0
END 0 9,0
PAL 0

ROOM b
i,0,0,b,b,b,0,j,0,0,f,i,0,0,0,d
d,0,e,j,0,d,b,0,0,f,f,0,j,b,b,0
0,e,e,0,c,0,0,i,b,0,0,i,0,0,b,b
k,k,k,k,k,k,k,k,k,k,k,b,b,0,i,b
h,h,h,h,h,h,h,h,h,h,k,b,0,c,0,0
h,h,h,h,h,h,h,h,h,h,k,0,j,0,e,0
k,k,k,k,k,k,k,k,h,h,k,0,0,0,0,e
d,0,0,b,0,0,d,k,h,h,k,b,0,d,i,0
0,j,0,i,0,e,0,k,h,h,k,b,i,0,b,b
b,0,0,0,0,e,0,k,h,h,k,b,c,0,0,0
0,b,f,0,j,0,0,k,0,i,k,0,0,b,0,j
j,0,f,0,b,b,b,k,0,0,k,d,0,b,b,0
0,0,f,0,i,d,0,k,k,k,k,0,b,i,0,b
d,i,0,0,0,b,b,0,0,0,0,0,b,0,c,0
0,0,b,j,d,b,f,f,0,0,j,e,e,e,0,0
e,e,b,b,0,i,0,f,c,b,0,0,0,0,j,0
NAME secret room 2
EXT 0,5 a 15,5
EXT 0,4 a 15,4
PAL 0

TIL a
11111111
11011101
10101010
01110111
11011101
10101010
01110111
11111111
>
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME water1
WAL true

TIL b
00000000
00000101
01010010
00100000
00000000
00001010
10100100
01000000
NAME grass1

TIL c
00000000
00000000
00000000
00001000
00010110
00010110
00010000
00010000
>
00000000
00000000
00000000
00001000
00010110
00010110
00010000
00010000
NAME flower1

TIL d
00000000
00000000
00000000
00111000
00111000
00010000
00010000
00010000
NAME flower2

TIL e
00000000
00000000
00000000
01010000
00100000
00001010
00000100
00000000
NAME grass2
WAL false

TIL f
00000000
00001010
00000100
01010000
00100000
00000000
00000000
00000000
NAME grass3
WAL false

TIL g
00100100
00100100
00100100
00100100
00010010
00010010
00010010
00010010
NAME bridge

TIL h
00000000
11101101
00000000
10110111
00000000
11101101
00000000
10110111
NAME brick
WAL false

TIL i
00000000
00000000
00000000
00000000
00101000
00010000
00010000
00010000
NAME sprout

TIL j
00000000
00100000
01010000
00100000
00001010
00000100
00001010
00000000
>
00000000
01010000
00100000
01010000
00000100
00001010
00000100
00000000
NAME sparkles

TIL k
01001100
10100010
11011011
01001100
10010101
00110010
10100011
01001100
NAME hedge
WAL true

SPR A
00000000
00111100
00100100
00100100
00011000
00011100
00011000
00011000
>
00000000
00111100
00100100
00100100
00011000
00011100
00011000
00011000
POS 0 4,4

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME star
DLG SPR_0
POS 0 13,11

SPR b
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME star2
POS 1 6,11

SPR c
00000000
01111110
01000010
01000010
01111110
00011000
00011000
00011000
NAME sign
DLG SPR_1
POS 0 4,15

SPR d
00000000
00011100
00100010
00100010
00010100
00001110
00000100
00001000
>
00000000
00011100
00100010
00100010
00011010
00000100
00001100
00010000
NAME ghost
DLG SPR_2
POS 2 3,7

SPR e
00000000
00001000
00010100
00011100
00001000
00011100
00011100
00011100
>
00000010
00001000
01010100
00011100
00001001
00011100
00011100
00011100
NAME candles
DLG SPR_3
POS 2 10,12

SPR f
00000000
00000100
00000000
01000000
00001010
00011000
00111100
00000000
>
00000000
00000000
00000000
00000000
00000000
00111100
00011000
00010000
NAME butterfly
DLG SPR_4
POS 2 6,4

SPR h
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME star3
DLG SPR_6
POS 3 13,12

SPR i
01000000
00100000
00110000
01001000
00110000
00111000
00110000
00110000
>
10000000
01100000
00110000
01001000
00110000
00111000
00110000
00110000
NAME witch
DLG SPR_7
POS 4 2,9

SPR j
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME star4
DLG SPR_8
POS 4 10,8

SPR k
00010000
00111000
01111000
00110000
00011000
00011100
00011000
00011000
>
00010000
00111010
01111000
00110000
00011000
01011100
00011001
00011000
NAME dane
DLG SPR_5
POS b 8,11

SPR m
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME star6
DLG SPR_9
POS 5 13,6

SPR n
00000000
00010000
00101000
01000100
00101000
00010000
00000000
00000000
>
00000000
00000000
00101000
00010000
00101000
00000000
00000000
00000000
NAME a star
DLG SPR_a
POS 5 1,14

SPR o
00000000
00000000
10001010
10001110
01001110
00111110
00111100
00100100
>
00000000
00000000
10001010
10001110
01001110
00111110
00111100
00100100
NAME star6
DLG SPR_b
POS 9 1,7

SPR p
01000000
00100000
00110000
01001000
00110000
00111000
00110000
00110000
>
10000000
01100000
00110000
01001000
00110000
00111000
00110000
00110000
NAME witch2
DLG SPR_c
POS a 3,8

SPR q
00000000
01111110
01000010
01000010
01111110
00011000
00011000
00011000
NAME sign2
DLG SPR_d
POS b 9,11

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
>
00010000
00100000
00010000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

ITM 1
00000000
00110000
01001000
01001000
00111000
00000100
00000000
00000000
>
00000000
00110000
01001000
01001000
00111000
00000100
00000000
00000000
NAME collar
DLG ITM_1

ITM 2
00000000
00000000
00010100
00111110
00011100
00001000
00000000
00000000
>
00000010
01000000
00010100
00111110
00011100
00001000
00000010
00000000
NAME hope
DLG ITM_2

ITM 3
00000000
00000000
00000000
11111111
01010010
01000010
01000110
01111110
>
00000000
00000000
00000000
11111111
01000010
01001010
01100010
01111110
NAME fireflies
DLG ITM_3

DLG ITM_0
You found a nice, hot cup of tea! What's it doing here?

DLG SPR_0
Cat: Mrow?

DLG SPR_1
Protected Area: Do Not Enter

DLG ITM_1
It's a collar. It reads, "Star" on the tag.

DLG SPR_2
"""
Spirit: I once chased after the fallen star. I heard it either brought great blessing or great curse.
I was too young. I didn't think I'd end up getting the curse.
I died without purpose. Without purpose, who am I?
"""

DLG SPR_3
It's a lit candle. Creepy.

DLG SPR_4
A butterfly. My sister loves these guys. I never understood why.

DLG SPR_6
"""
Angela: What? Didn't I just see you a minute ago?
Cat: Mrow?
"""

DLG SPR_5
Pain is inevitable, suffering is optional - Some Kid I Go To School With

DLG ITM_2
A heart. You feel yourself flooded with hope.

DLG ITM_3
A jar with fireflies. You're filled with wonder as you pick it up.

DLG SPR_a
"""
A...star? You stare at it in wonder.
Is this it? 
You go to touch it but an invisible force stops your hand from making contact.
"""

DLG SPR_c
"""
Witch: Fallen Star...
Angela: I'm sorry?
Witch: It was a curse that Star had fallen victim to...never to return to her former self.
Angela: Um. Okaay. What about the blessing and the curse?
The witch's face grew dark.
Witch: There is no blessing. Your journey has been for naught.
Angela: Surely that can't be it?
Witch: Everything in this world is a curse. You have to make your own blessings or be a blessing to others.
Angela: ...
Witch: Go now. I'm sick of your face.
Angela: What's across the bridge?
Witch: You're too nosy. Go follow the river. Godspeed.
"""

DLG SPR_d
"""
Thank you for playing Fallen Star! I hope you enjoyed it.
Special thanks to Dane (the kid I go to school with) for helping test the game and making the soundtrack!
"""

DLG SPR_7
"""
Witch: ...
Angela: ...
Witch: ...
Angela: Do you know if the Fallen Star is near here?
Witch: ...
"""

DLG SPR_8
"""
Angela: Do you belong to that witch?
Cat: Mrow!
Angela: Is that a yes?
Cat: *purr*
Angela: Hm.
"""

DLG SPR_9
"""
Cat: Have you realized it yet?
Angela: What?
Cat: Fallen Star.
Angela: I'm sorry?
Cat: Return to start, and you will see.
"""

DLG SPR_b
"""
Cat: Is the blessing really that important to you?
Angela: ...
Cat: Do you have the collar?
Angela: ...well I-
Cat: It's mine. Go see the witch. Follow the stream upwards.
"""

END 0


END undefined


END undefinee


END undefinef


END undefineg


END undefineh


END undefinei


VAR a
42

