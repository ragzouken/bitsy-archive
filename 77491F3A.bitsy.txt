
Bast's Blessing

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
212,171,186
145,29,30
236,253,232

ROOM 0
a,b,o,a,b,c,a,b,c,a,b,c,a,b,0,c
a,0,0,0,0,0,0,0,0,0,0,0,0,b,0,c
a,0,0,0,0,0,a,b,c,a,b,0,0,0,0,0
0,0,0,0,0,0,a,0,0,0,l,0,0,a,b,c
a,b,c,0,0,0,a,0,0,0,l,0,0,0,0,c
a,0,0,0,0,0,a,0,0,0,l,0,0,a,b,0
a,b,c,a,0,0,a,b,c,a,b,0,0,a,b,0
a,0,0,a,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,a,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,a,b,c,a,b,m,c,a,b,c,a,b,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,k,k,k,j,0,0,0,0,d,k,k,k,j,0
0,e,0,0,0,i,0,0,0,0,e,0,0,0,i,0
0,e,0,0,0,i,0,0,0,0,e,0,0,0,i,0
0,f,g,g,g,h,0,0,0,0,f,g,g,g,h,0
0,0,0,0,0,0,0,n,0,0,0,0,0,0,0,0
ITM 3 2,13
ITM 3 3,13
ITM 3 4,13
ITM 4 7,12
ITM 4 7,13
ITM 4 7,14
ITM 4 6,13
ITM 4 6,14
ITM 4 6,15
ITM 4 5,15
ITM 4 3,15
ITM 4 4,15
ITM 4 2,15
ITM 4 1,15
ITM 4 0,15
ITM 4 0,14
ITM 4 0,13
ITM 4 0,12
ITM 4 0,11
ITM 4 6,11
ITM 4 6,12
ITM 4 9,11
ITM 4 9,12
ITM 4 9,14
ITM 4 9,13
ITM 4 9,15
ITM 4 10,15
ITM 4 11,15
ITM 4 12,15
ITM 4 13,15
ITM 4 14,15
ITM 4 15,15
ITM 4 15,14
ITM 4 15,12
ITM 4 15,11
ITM 5 11,13
ITM 5 12,13
ITM 5 13,13
ITM 3 2,12
ITM 3 3,12
ITM 3 4,12
ITM 5 11,12
ITM 5 12,12
ITM 5 13,12
ITM 4 1,10
ITM 4 2,10
ITM 4 3,10
ITM 4 4,10
ITM 4 5,10
ITM 4 6,10
ITM 4 7,10
ITM 4 9,10
ITM 4 10,10
ITM 4 11,10
ITM 4 12,10
ITM 4 13,10
ITM 4 14,10
ITM 4 1,9
ITM 4 1,8
ITM 1 1,7
ITM 4 2,9
ITM 4 2,8
ITM 4 2,7
ITM 2 8,4
ITM 4 15,10
ITM 4 7,11
ITM b 8,15
ITM 6 8,8
ITM 4 8,12
ITM 4 15,13
ITM 4 0,10
ITM 4 8,13
ITM 7 8,10
ITM 4 8,11
ITM 4 8,14
END 0 7,15
PAL 0

TIL a
11100011
00011100
11100011
00011100
11100011
00011100
11100011
00011100
NAME OL tile (1)
WAL true

TIL b
10001110
01110001
10001110
01110001
10001110
01110001
10001110
01110001
NAME I tile (2)
WAL true

TIL c
00111000
11000111
00111000
11000111
00111000
11000111
00111000
11000111
NAME Tile (3)
WAL true

TIL d
00001111
00011000
00110000
01100000
11000000
10000000
10000000
10000000
NAME UR pool
WAL true

TIL e
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME re pool
WAL true

TIL f
10000000
10000000
10000000
11000000
01100000
00110000
00011000
00001111
NAME LR pool
WAL true

TIL g
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME be pool
WAL true

TIL h
00000001
00000001
00000001
00000011
00000110
00001100
00011000
11110000
NAME BL pool
WAL true

TIL i
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME le pool
WAL true

TIL j
11110000
00011000
00001100
00000110
00000011
00000001
00000001
00000001
NAME UL pool
WAL true

TIL k
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME TE pool
WAL true

TIL l
10001110
01110001
10001110
01110001
10001110
01110001
10001110
01110001
NAME Secret Wall
WAL false

TIL m
01111110
11111111
01111110
11111111
01111010
11111111
01111110
11111111
NAME Door

TIL n
00000000
01111110
01000010
01011010
01011010
01000010
01111110
00000000

TIL o
01111110
11111111
01111110
11111111
01111010
11111111
01111110
11111111
NAME Beginning Door
WAL true

SPR A
00000000
00000000
00011000
00011000
00111100
01011010
00011000
00011000
>
00000000
00000000
00011000
01011000
00111100
00011010
00011000
00010100
POS 0 2,1

SPR a
00000000
00000000
01010100
01110010
01110010
01111100
00111100
00100100
>
00000000
00000000
00000000
01010001
01110010
01110010
01111100
00111100
NAME Cat
DLG SPR_0
POS 0 8,15

SPR b
00000000
00111100
00111100
00011000
00111100
01011010
00011000
00011000
>
00000000
00111100
00111100
00011010
00111100
01011000
00011000
00011000
NAME Person 1
DLG SPR_1
POS 0 15,5

SPR c
00000000
00011100
00011000
00011010
00111100
01011000
00011000
00011000
>
00000000
00011100
00011000
00011000
00111100
01011010
00011000
00011000
NAME Person 2
DLG SPR_2
POS 0 0,3

SPR d
00011000
00111100
00011000
01011000
00111100
00011010
00011000
00011000
>
00011000
00111100
00011000
00011000
00111100
01011010
00011000
00011000
NAME Person 3
DLG SPR_3
POS 0 14,0

SPR e
00000000
00000000
00000000
00011000
01011000
00111000
00011000
00111000
>
00000000
00000000
00011000
01011000
00111000
00011000
00011000
00111000
NAME Person 4
DLG SPR_4
POS 0 14,4

SPR f
00000000
00000000
00011000
01011010
00111100
00011000
00011000
00011000
>
00000000
00000000
00000000
00011000
00011000
00111100
00111100
00011000
NAME Person 5
DLG SPR_5
POS 0 1,5

SPR g
00000000
00000010
00011000
00011000
00111100
01011010
00011000
00011000
>
00000000
00000000
00011010
01011000
00111100
00011010
00011000
00011000

ITM 1
00010000
00001000
00010000
00111100
01100100
00100100
00011000
00000000
>
00001000
00010000
00001000
00111100
01100100
00100100
00011000
00000000
NAME Coffee
DLG ITM_0

ITM 2
00000000
00101000
00010100
00111100
01000010
01100110
00111100
00000000
>
00000000
00010100
00101000
00111100
01000010
01100110
00111100
00000000
NAME Milk Offering
DLG ITM_1

ITM 3
00000000
01100000
10010000
00001001
01100110
10010000
00001001
00000110
>
00000110
00001001
10010000
01100110
00001001
10010000
01100000
00000000
NAME Water 1

ITM 4
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Steps

ITM 5
00000110
00001001
10010000
01100110
00001001
10010000
01100000
00000000
>
00000000
01100000
10010000
00001001
01100110
10010000
00001001
00000110
NAME Water 2

ITM 6
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Step 1 voice
DLG ITM_2

ITM 7
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Step 2 voice
DLG ITM_3

ITM b
00000000
00000000
00100100
00011000
00011000
00100100
00000000
00000000
NAME counter

DLG SPR_0
"""
{sequence
  - {
      - {item "Steps"} == 4 ?
        "Hello mortal. I am Bast, the Goddess of protection and cats. 
         I hope you have gathered everything  you need before your journey. 
         But before you go, I want to give you a gift. 
        Here is a compass, to find the the most direct path to your destination." 
{
  - {item "Milk Offering"} == 1 ?
    You return her fine gesture with the milk offering. 
     "Thank you mortal, for the offering. I will protect you through your journey."
}      
     - {item "Steps"} <= 17 ?
        "Hello mortal. I am Bast, the Goddess of protection and cats. 
        I hope you have gathered everything  you need before your journey.
        But before you go, I want to give you a gift. 
        Here is a map, to get you back on your path if you lose your way."
{
  - {item "Milk Offering"} == 1 ?
    You return her fine gesture with the milk offering. 
     "Thank you mortal, for the offering. I will protect you through your journey."
}
      - {item "Steps"} > 17 ?
        "Hello mortal. I am Bast, the Goddess of protection and cats.  
       I hope you have gathered everything  you need before your journey. 
       But before you go, I want to give you a gift. 
        Here is a walking stick, to wander longer before tiring. "
   {
  - {item "Milk Offering"} == 1 ?
     You return her fine gesture with the milk offering. 
     "Thank you mortal, for the offering. I will protect you through your journey."
}
 }
  - "I wish you luck mortal."
}
"""

DLG ITM_0
You found a nice hot cup of coffee to keep you warm and energized for your adventure.

DLG ITM_1
You found a bowl of warm milk.

DLG SPR_1
"""
{sequence
  - The cool thing about traveling alone is that you don't have to fight about directions. 
    You're either lost or you're not!
  - It's okay to get lost, just don't lose yourself in the process.
  - Safe travels my friend.
}
"""

DLG SPR_2
"""
{sequence
  - You are so brave. I know you can do it.
  - I believe in you.
}
"""

DLG ITM_2
You gather yourself to walk outside. Nobody has opened the old door in years.

DLG SPR_3
"""
{sequence
  - I'm so proud of you. This is exactly something your mother would do. But please, be careful.
  - You're definitely her kid.
}
"""

DLG SPR_4
"""
{sequence
  - You're going to have so much fun! I wish I could go with you!
  - I wonder if you'll see a chimera!
}
"""

DLG SPR_5
"""
{sequence
  - Thank you for looking out for us all.
  - I hope you find the answer!
}
"""

DLG ITM_3
As you step outside, you  feel a freedom that humans have been denied for a hundred years. In the distance you see a creature you've never seen before. It's looking directly at you, almost like it's been expecting you.

END 0
The End...?           

END undefined
You have reached the end.

END undefinee


VAR a
42


