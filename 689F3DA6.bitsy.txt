in the wheat fields

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
224,113,14
250,173,70
255,213,127

ROOM 0
b,d,b,d,d,b,d,d,b,d,c,0,c,c,0,0
b,b,b,d,d,c,c,0,0,c,c,c,0,0,c,c
b,d,d,d,c,c,0,c,c,0,d,d,d,d,d,d
d,b,d,d,0,0,0,c,b,d,d,b,d,d,b,d
b,b,d,c,0,0,c,d,d,d,d,b,d,d,d,d
d,d,d,c,0,0,d,d,b,d,d,d,d,d,b,0
d,b,b,0,0,0,c,b,d,b,b,c,b,b,c,c
d,b,c,0,0,c,d,d,d,d,d,d,d,c,c,c
d,c,0,0,c,b,d,d,b,d,b,b,d,d,c,c
d,d,0,0,c,c,d,b,b,d,d,d,d,d,d,d
b,c,c,d,0,b,c,c,c,b,d,b,b,d,b,d
d,d,d,d,0,0,c,c,c,c,b,b,d,b,d,d
d,b,d,d,c,0,0,c,c,c,c,d,d,b,d,d
d,d,b,d,c,0,0,0,c,c,c,b,d,d,d,0
d,b,b,b,b,c,0,0,0,0,0,0,0,0,0,0
b,d,d,b,b,d,d,d,d,d,d,d,d,d,d,d
NAME field start
EXT 15,14 1 0,14
EXT 15,7 1 0,7
PAL 0

ROOM 1
d,b,c,0,b,d,d,b,d,d,b,b,d,b,d,d
b,d,c,0,0,d,d,c,b,c,c,d,d,b,d,b
b,d,c,c,0,0,0,0,0,0,0,c,b,b,d,d
d,b,d,c,0,0,c,c,c,c,c,c,c,c,d,d
d,d,c,0,0,c,b,d,b,b,d,b,c,c,c,0
d,c,0,0,0,0,c,0,c,c,c,c,c,e,c,0
c,c,0,c,d,d,c,c,d,d,b,b,d,0,e,e
c,0,0,0,c,d,d,c,c,d,d,d,d,d,d,c
c,c,c,0,0,c,c,0,0,c,c,d,b,d,b,d
d,c,d,c,c,0,0,0,0,0,0,0,c,d,d,d
d,b,b,d,b,d,0,c,c,c,c,0,0,c,d,b
b,d,b,d,b,b,d,d,c,c,0,0,0,0,c,d
b,d,b,d,d,b,c,c,c,0,0,0,0,c,c,c
d,d,b,b,c,c,0,0,0,c,c,d,c,c,c,c
b,0,0,0,0,0,0,c,c,c,c,c,b,d,b,d
b,d,d,d,d,d,d,d,b,d,b,b,d,d,b,d
NAME field right
EXT 0,7 0 15,7
EXT 3,1 2 0,15
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,d,d
0,0,0,0,d,0,e,0,e,0,0,0,0,d,d,b
0,d,d,d,b,c,c,c,c,c,d,c,d,b,d,c
c,0,d,b,d,c,c,c,c,c,d,0,0,d,d,d
0,0,d,d,c,c,c,c,b,b,0,c,0,0,d,c
0,d,0,d,c,c,0,b,0,0,0,0,0,c,d,d
0,0,0,c,c,0,d,b,d,0,d,0,0,0,b,d
0,0,d,c,0,c,b,b,d,0,0,0,0,c,b,d
c,0,d,d,c,c,c,c,c,d,0,0,d,c,d,d
0,0,b,d,b,d,0,c,c,c,d,d,b,b,d,b
b,c,d,b,b,b,d,c,c,c,d,b,b,d,d,d
d,b,b,c,b,c,c,c,0,b,d,d,d,b,b,d
d,c,c,c,c,c,c,0,c,d,d,b,b,d,d,d
d,c,c,0,c,c,0,0,0,0,d,d,d,d,d,b
0,0,c,0,c,0,0,0,0,c,d,d,b,b,d,d
0,0,0,0,0,0,c,0,0,b,d,b,d,d,d,d
NAME field final
END undefined 8,1
END undefined 6,1
PAL 0

TIL b
00100010
00000000
10001000
01000100
01000100
00100010
00100010
00100010
>
00100010
00000000
01000100
00100010
00100010
00100010
00100010
00100010
NAME wheat 2
WAL true

TIL c
00000000
00100000
00010000
00010000
00001000
00101010
00101010
00101010
>
00000000
00010000
00001000
00001000
00001000
00101010
00101010
00101010
NAME wheat a2
WAL false

TIL d
00000000
00010000
00001000
10001000
10001000
10101010
10101010
10101010
>
00000000
00100000
00010000
00010000
10001000
10101010
10101010
10101010
NAME wheat a3
WAL true

TIL e
00000000
00010000
00001000
00001000
10001000
10001010
10101010
10101010
>
00000000
00100000
00010000
00010000
10001000
10001010
10101010
10101010
NAME ending
WAL false

SPR A
00011100
00000000
01111111
00011100
00101010
00110110
00111110
00111110
POS 0 4,5

SPR a
00011100
00011010
00001110
00000100
00011111
00000100
00000100
00000100
NAME scarecrow
DLG SPR_0
POS 0 7,12

SPR b
00000000
10001000
01000100
01000100
00100010
10101010
10101010
10101010
>
00000000
01000100
00100010
00100010
00100010
10101010
10101010
10101010
NAME wheat
DLG SPR_3
POS 2 3,13

SPR c
00000000
00000000
00111100
01111110
00100100
00111110
00010111
00011111
>
00000000
00000000
00111100
01111110
00100100
00111110
00010111
00011111
NAME lounger
DLG SPR_1
POS 0 13,7

SPR d
00000000
10110000
11001000
10101000
10001100
01110110
01111011
01111111
>
00000000
10110000
11001000
10001000
10001100
01110110
01111011
01111111
NAME sky watcher
DLG SPR_2
POS 0 1,9

SPR e
00111100
11111111
00111100
00111100
01111110
01111110
01111110
01111110
NAME man
DLG SPR_4
POS 2 7,1

SPR f
00000000
00001110
00011111
00010101
00110001
01111110
01111100
01111100
NAME old woman
DLG SPR_5
POS 1 7,7

SPR g
00000000
01001010
00101110
00101110
01011100
01011100
00111100
00111100
>
00000000
00101010
00101110
01001110
10011100
01011100
00111100
00111100
NAME cat
DLG SPR_6
POS 1 3,3

SPR h
00000000
00000000
00000100
00011000
01001100
10001100
10011100
01111100
>
00000000
00000000
00000100
00011000
10001100
10001100
01011100
00111100
NAME rat
DLG SPR_7
POS 1 15,6

SPR i
00000000
00000000
00000100
00011000
01001100
10001100
10011100
01111100
>
00000000
00000000
00000100
00011000
10001100
10001100
01011100
00111100
NAME rat
DLG SPR_8
POS 0 14,1

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

DLG SPR_0
"""
{sequence
  - {shk}BOO!{shk}
  - .........
  - are you scared?
}
"""

DLG ITM_0
You found a nice warm cup of tea

DLG SPR_1
aren't wheat fields nice?

DLG SPR_2
"""
{sequence
  - i love watching the clouds
  - they're all different, but beautiful
  - ..........
  - have you ever noticed they don't look quite real?
}
"""

DLG SPR_3
"""
{shuffle
  - {wvy}i don't like the look of him{wvy}
  - {wvy}what do you think of him?{wvy}
  - {wvy}i don't know what he's doing here{wvy}
  - {wvy}do you know him?{wvy}
  - {wvy}the man in the field...{wvy}
  - {wvy}is he a friend of yours?{wvy}
  - {wvy}he's very quiet...{wvy}
}
"""

DLG SPR_4
"""
{sequence
  - {clr2}oh, hello{clr2}
  - 
  - {clr2}lovely day, isn't it?{clr2}
  - {wvy}{clr2}............{clr2}{wvy}
  - {clr2}i know we've only just met, but may i share my thoughts with you?{clr2}
  - 
  - {clr2}thank you{clr2}
  - {clr2}I always have mixed feelings about the seasons changing{clr2}
  - {clr2}On one hand, autumn is beautiful, and it's good to get a break from the swell of heat summer brings. I always think to myself, 'I like cold weather better', but the cold weather chills my bones, makes it harder to get up in the morning, and autumn brings winter with it.{clr2}
  - {clr2} It's only natural, of course.{clr2}
  - {clr2}Every year I look forward to summer, only to look forward to fall, which leads to dread, and dread, and dread, and dread. And then I look forward to summer, only to look forward to fall, and....{clr2}
  - {shk}{clr2}............{clr2}{shk}
  - {clr2}..................{clr2}
  - {clr2}Winter is just hard, is all.{clr2}
  - 
  - {clr2}the colors autumn holds are beautiful, at least{clr2}
  - {clr2}and the harvest is good{clr2}
  - {clr2}.............{clr2}
  - {clr2}would you like to stand with me?{clr2}
}
"""

DLG SPR_5
i prefer peace and quiet, thank you

DLG SPR_6
{wvy}miaow{wvy}

DLG SPR_7
eek!

DLG SPR_8
squeak!

END 0


END undefined


END undefinee


VAR a
42

