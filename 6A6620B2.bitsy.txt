Learning the code

# BITSY VERSION 3.7

! ROOM_FORMAT 1

PAL 0
163,109,0
0,182,214
255,255,255

PAL 1
164,110,9
220,152,24
255,255,255

PAL 2
255,255,255
255,255,255
255,255,255

PAL 3
152,98,16
56,56,56
255,255,255

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,0,0,0,0,a,a,a,a,a,a
a,a,a,a,a,0,0,0,0,0,0,a,a,a,a,a
a,a,a,a,0,0,0,0,0,0,0,0,a,a,a,a
a,a,a,0,0,0,0,0,0,0,0,0,0,a,a,a
a,a,c,0,0,0,0,0,0,0,0,0,0,0,a,a
a,c,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,n,a,a,a,a,a,a,a,a,a,a,n,a,a
WAL a
EXT 13,15 1 5,2
END undefinel 2,15
PAL 0

ROOM 1
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,0,a,a,0,0,0,0,0,0,a,a,a,a,a
a,0,0,0,0,0,0,0,a,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
WAL a
EXT 1,7 3 13,7
EXT 14,7 4 1,8
PAL 3

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
PAL 0

ROOM 3
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,a,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,a,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
END undefinek 14,8
END undefinek 14,7
PAL 3

ROOM 4
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
a,0,0,0,0,0,0,0,0,0,0,0,0,m,m,m
0,0,0,0,0,0,0,0,0,0,0,0,0,m,m,m
a,0,0,0,0,0,0,0,0,0,0,0,i,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
WAL a,m
EXT 0,8 5 11,7
PAL 3

ROOM 5
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,0,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,n,a,a,a,a,a,a,a,a,a,a
a,a,0,a,a,0,0,0,0,0,0,a,a,a,a,a
a,0,0,0,0,0,0,0,a,0,0,0,0,0,l,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
WAL a
EXT 1,7 6 13,7
END 0 10,8
PAL 3

ROOM 6
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,a,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
m,0,0,0,0,0,0,0,0,0,0,0,0,0,a,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
m,m,m,m,m,m,m,m,m,m,m,m,m,m,m,m
WAL m
END undefinek 14,8
END undefinek 14,7
PAL 3

TIL a
00000000
00111100
01011010
01100110
01100110
01011010
00111100
00000000

TIL b
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL f
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL g
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL i
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000

TIL j
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL k
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL l
11111111
00001000
11111111
00001000
11111111
00001000
11111111
00001000

TIL m
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL n
11111111
10000001
11111111
10000001
11111111
10000001
11111111
10000001

SPR A
00000000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
>
00000000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 0 7,10

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
DLG SPR_0
POS 4 3,9

SPR b
00011000
00111100
00011000
01111110
10110101
00111100
00100100
00100100
>
00011000
00111100
00011000
01111110
10110101
00111100
00100100
00100100
DLG SPR_1
POS 0 9,8

SPR c
00000000
00011100
00100010
01000001
00100010
00010100
01010101
01110111
DLG SPR_2

SPR d
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
>
00011000
00011000
00011000
10111101
01111110
00111100
00100100
00100100
DLG SPR_5
POS 1 8,7

SPR e
00000000
00011000
00100100
01011010
01011010
00100100
00011000
00111100
DLG SPR_6
POS 0 7,8

SPR f
00000000
01111000
01111100
01111000
00010000
00010000
00010000
00010000
DLG SPR_3
POS 0 12,14

SPR g
00000000
00111110
01111110
00111110
00001000
00001000
00001000
00001000
DLG SPR_7
POS 0 3,14

SPR h
00000000
01111000
01111100
01111000
00010000
00010000
00010000
00010000
DLG SPR_4
POS 0 12,14

SPR i
10101010
10101011
11111011
00100011
00100010
00100010
00100010
00100010

SPR j
10101010
10101011
11111011
00100011
00100010
00100010
00100010
00100010

SPR k
00000000
01111000
01111100
01111000
00010000
00010000
00010000
00010000
DLG SPR_8
POS 1 13,6

SPR l
00000000
00111110
01111110
00111110
00001000
00001000
00001000
00001000
DLG SPR_9
POS 1 2,6

SPR m
00000000
00011000
00011000
01111110
00011000
00100100
01000010
00000000
DLG SPR_a
POS 4 12,7

SPR n
00000000
00011000
00011000
11111111
00011000
00011000
00011000
00011000
DLG SPR_b
POS 4 7,7

SPR o
01111110
01111110
01111110
01111110
01111110
00011000
00011000
00011000

SPR p
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
>
00011000
00011000
00011000
10111101
01111110
00111100
00100100
00100100
DLG SPR_c
POS 5 8,7

SPR q
00000000
00011000
00011000
11111111
00011000
00011000
00011000
00011000
DLG SPR_d
POS 3 11,7

SPR r
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
DLG SPR_e
POS 3 4,6

SPR s
00000000
00011000
00011000
01111110
00011000
00100100
01000010
00000000
DLG SPR_f
POS 3 2,8

SPR t
00000000
00011000
00011000
01111110
00011000
00100100
01000010
00000000

SPR u
00000000
00011000
00011000
01111110
00011000
00100100
01000010
00000000
DLG SPR_g
POS 6 3,8

SPR v
00000000
00011000
00011000
11111111
00011000
00011000
00011000
00011000
DLG SPR_h
POS 6 12,7

SPR w
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
DLG SPR_i
POS 6 1,7

SPR x
00000000
00111110
01111110
00111110
00001000
00001000
00001000
00001000
POS 5 2,6

SPR y
00000000
01111000
01111100
01111000
00010000
00010000
00010000
00010000
POS 5 12,6

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
DLG ITM_0

DLG SPR_0
h8y 2v8, 6m 7 c6t. 6nd 6

DLG ITM_0
You got a cup of tea! Nice.

DLG SPR_1
Welcome aboard the Knarr, Engineer Uve.  A ship runs on its stomach so make sure you eat, the Chef can help with that.  Otherwise report to Engineering.

DLG SPR_2
Oohm

DLG SPR_3
To the galley

DLG SPR_4
To the bridge...

DLG SPR_5
Oh! New recruit eh, I recommend you eat in Mess Hall 2.

DLG SPR_6
Navigation Station: Best not touch that.

DLG SPR_7
To Engineering.

DLG SPR_8
Mess Hall 1

DLG SPR_9
Mess Hall 2.

DLG SPR_a
w8lc1m8 7b177rd, 

DLG SPR_b
6m 7 p1l8.

DLG SPR_c
I told you to go the other way.

DLG SPR_d
Engineer Uve, breakfast is served. Feel free to talk to the rest of the crew.

DLG SPR_e
Hey Champ, enjoying your first day? You'll be fine once you pick up the code.

DLG SPR_f
Engineer huh? You'll get to grips with the code no problem.

DLG SPR_g
Engineer huh? I thought you'd be able to follow instructions.

DLG SPR_h
Newbie alert!  You look confused, went to the wrong mess hall I guess.

DLG SPR_i
You won't make that mistake again.

END 0
Ergh, that may have been a mistake, I feel really sick.

END undefined
Ergh, that may have been a mistake, I feel really sick.

END undefinee


END undefinef


END undefineg


END undefineh


END undefinei


END undefinej
Found the cooks select ingrandent.....

END undefinek
Ship corrodor broke.

END undefinel
You don't know enough of the language to go here just yet. Apparently its java

