It's Time to Make a Game...

# BITSY VERSION 4.2

! ROOM_FORMAT 1

PAL 0
0,0,0
191,164,130
255,255,255

PAL 1
0,0,0
133,96,58
224,180,143

PAL 2
0,0,0
70,45,43
197,197,197

PAL 3
0,0,0
58,124,65
137,89,236

PAL 4
159,0,4
0,0,0
119,216,82

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,g,f,f,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,q,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,q,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,0,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,q,f,h,0,0,0,0,0,0
0,0,0,0,0,g,f,i,f,h,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room_1
WAL f,i,j,k
ITM 0 7,12
ITM 2 7,9
ITM 1 7,6
EXT 7,3 4 7,3
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,g,f,f,f,f,f,f,f,f,f,h
0,0,0,0,0,g,f,k,0,0,0,0,0,0,f,h
0,0,0,0,0,g,f,0,l,l,l,l,l,l,f,h
0,0,0,0,0,g,f,0,0,0,0,0,0,0,f,h
0,0,0,0,0,g,f,0,0,0,0,0,0,0,f,h
0,0,0,0,0,g,f,l,0,l,0,l,l,0,f,h
0,0,0,0,0,g,f,0,0,0,0,0,0,0,f,h
0,0,0,0,0,g,f,0,0,l,0,0,0,l,f,h
0,0,0,0,0,g,f,0,0,0,l,0,0,0,f,h
0,0,0,0,0,g,f,0,0,0,0,0,l,l,f,h
0,0,0,0,0,g,f,0,l,0,l,0,0,0,f,h
0,0,0,0,0,g,f,0,0,0,0,0,l,0,f,h
0,0,0,0,0,g,f,0,0,l,0,0,0,0,f,h
0,0,0,0,0,g,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room_2
WAL f,i,j,k
EXT 9,13 5 9,13
PAL 1

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,0,0,0,0,0,k,m,m,m,m,m,m,f,h
g,f,0,0,0,0,l,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,m,0,m,0,m,m,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,m,0,0,0,m,f,h
g,f,0,0,0,0,0,0,0,0,m,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,m,m,f,h
g,f,0,0,0,0,0,0,m,0,m,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,m,0,f,h
g,f,0,0,0,0,0,0,0,m,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME room_3
WAL f,i,j,k
EXT 6,3 6 6,3
PAL 2

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,n,0,0,n,o,k,m,m,m,m,m,m,f,h
g,f,0,n,0,n,0,0,0,0,0,p,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,q,0,0,f,h
g,f,0,n,0,0,0,m,0,m,0,m,m,0,f,h
g,f,n,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,m,0,0,0,m,f,h
g,f,n,n,0,n,0,0,0,0,m,0,0,0,f,h
g,f,0,0,0,n,0,0,0,0,0,0,m,m,f,h
g,f,0,0,0,0,0,0,m,0,m,0,0,0,f,h
g,f,0,n,0,0,0,0,0,0,0,0,m,0,f,h
g,f,0,0,n,0,n,0,0,m,0,0,0,0,f,h
g,f,0,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
EXT 11,4 7 11,4
PAL 3

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,n,0,n,0,0,0,0,0,0,0,f,f,h
g,f,f,0,0,0,0,0,0,0,0,0,0,f,f,h
g,f,f,n,0,0,0,m,0,m,0,m,m,f,f,h
g,f,f,0,0,n,0,0,0,0,0,0,0,f,f,h
g,f,f,0,0,0,0,0,0,m,0,0,0,f,f,h
g,f,f,n,0,n,0,0,0,0,m,0,0,f,f,h
g,f,f,0,0,n,0,0,0,0,0,0,m,f,f,h
g,f,f,0,p,0,0,0,m,0,m,0,0,f,f,h
g,f,f,n,q,0,0,0,0,0,0,0,m,f,f,h
g,f,f,0,n,0,n,0,0,m,0,0,0,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 3 11,3
EXT 4,11 8 4,11
PAL 3

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,0,0,0,0,0,0,0,0,f,f,f,h
g,f,f,f,0,0,0,m,0,m,p,m,f,f,f,h
g,f,f,f,0,n,0,0,0,0,q,0,f,f,f,h
g,f,f,f,0,0,0,0,0,m,0,0,f,f,f,h
g,f,f,f,0,n,0,0,0,0,m,0,f,f,f,h
g,f,f,f,0,n,0,0,0,0,0,0,f,f,f,h
g,f,f,f,0,0,0,0,m,0,m,0,f,f,f,h
g,f,f,f,0,0,0,0,0,0,0,0,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 4 4,10
EXT 10,6 9 10,6
PAL 3

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,0,0,m,0,m,0,f,f,f,f,h
g,f,f,f,f,n,p,0,0,0,0,f,f,f,f,h
g,f,f,f,f,0,q,0,0,m,0,f,f,f,f,h
g,f,f,f,f,n,0,0,0,0,m,f,f,f,f,h
g,f,f,f,f,n,0,0,0,0,0,f,f,f,f,h
g,f,f,f,f,0,0,0,m,0,m,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 5 10,5
EXT 6,7 a 6,7
PAL 3

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,0,0,0,0,f,f,f,f,f,h
g,f,f,f,f,f,0,0,p,m,f,f,f,f,f,h
g,f,f,f,f,f,0,0,q,0,f,f,f,f,f,h
g,f,f,f,f,f,0,0,0,0,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 6 6,6
EXT 8,8 b 8,8
PAL 3

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,0,p,0,0,f,f,f,f,f,h
g,f,f,f,f,f,0,q,0,m,f,f,f,f,f,h
g,f,f,f,f,f,0,0,0,0,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 7 8,7
EXT 7,7 c 7,7
PAL 3

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,0,p,0,f,f,f,f,f,h
g,f,f,f,f,f,f,0,q,m,f,f,f,f,f,h
g,f,f,f,f,f,f,0,0,0,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 8 7,6
EXT 8,7 d 8,7
PAL 3

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,0,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,p,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,q,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 8 8,6
EXT 7,8 e 7,8
PAL 3

ROOM e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,0,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,0,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM 9 7,7
EXT 8,7 f 8,7
PAL 3

ROOM f
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,0,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM a 8,8
EXT 8,9 g 8,9
PAL 4

ROOM g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,n,0,0,n,o,k,m,m,m,m,m,m,f,h
g,f,0,n,0,n,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,n,0,0,0,m,0,m,0,m,m,0,f,h
g,f,n,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,f,0,m,0,0,0,m,f,h
g,f,n,n,0,n,f,f,f,f,m,0,0,0,f,h
g,f,0,f,f,n,f,f,0,f,f,f,m,m,f,h
g,f,0,0,f,0,f,f,m,0,m,f,f,f,f,h
g,f,f,n,0,f,f,f,f,f,0,0,m,f,f,h
g,f,f,f,n,f,n,0,0,m,f,f,0,f,f,h
g,f,f,f,0,n,0,f,f,0,0,0,0,f,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM b 2,2
ITM b 3,3
ITM b 5,3
ITM b 5,2
ITM b 3,5
ITM b 2,6
ITM b 5,6
ITM b 3,8
ITM b 2,8
ITM b 5,8
ITM b 5,9
ITM b 3,11
ITM b 4,12
ITM b 5,13
ITM b 6,12
ITM c 8,2
ITM c 9,2
ITM c 10,2
ITM c 11,2
ITM c 12,2
ITM c 13,2
ITM c 12,5
ITM c 11,5
ITM c 9,5
ITM c 7,5
ITM c 9,7
ITM c 10,8
ITM c 13,7
ITM c 13,9
ITM c 12,9
ITM c 12,11
ITM c 10,10
ITM c 9,12
ITM c 8,10
ITM a 5,10
EXT 5,10 h 5,10
PAL 4

ROOM h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,n,f,f,n,o,k,m,m,m,m,m,m,f,h
g,f,0,n,0,n,0,0,0,f,f,f,f,q,f,h
g,f,f,0,f,f,f,f,f,f,f,f,f,f,f,h
g,f,f,n,f,f,0,m,0,m,0,m,m,0,f,h
g,f,n,0,0,n,0,f,f,f,f,f,f,0,f,h
g,f,f,f,f,f,f,f,0,m,0,f,f,m,f,h
g,f,n,n,f,n,0,0,0,f,m,f,f,0,f,h
g,f,0,0,f,n,f,f,f,f,0,0,m,m,f,h
g,f,0,0,f,0,f,0,m,f,m,f,f,f,f,h
g,f,0,n,f,f,f,0,0,f,f,f,m,0,f,h
g,f,0,0,n,0,n,0,0,m,0,0,0,0,f,h
g,f,0,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM b 5,6
ITM b 2,6
ITM b 3,5
ITM b 3,3
ITM b 2,2
ITM b 5,2
ITM b 5,3
ITM c 8,2
ITM c 9,2
ITM c 10,2
ITM c 11,2
ITM c 12,2
ITM c 13,2
ITM c 12,5
ITM c 11,5
ITM c 9,5
ITM c 7,5
ITM c 9,7
ITM c 10,8
ITM c 12,9
ITM c 13,9
ITM c 13,7
EXT 13,3 i 13,3
PAL 4

ROOM i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,n,0,0,n,o,k,m,m,m,m,m,m,f,h
g,f,0,n,0,n,0,0,0,0,0,0,0,q,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,n,0,0,0,m,0,m,0,m,m,0,f,h
g,f,n,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,n,0,0,m,0,0,0,m,f,h
g,f,n,n,0,n,0,0,0,0,m,0,0,0,f,h
g,f,0,0,0,n,0,0,0,0,0,0,m,m,f,h
g,f,0,0,0,0,0,0,m,0,m,0,0,0,f,h
g,f,0,n,0,0,0,0,0,0,0,0,m,0,f,h
g,f,0,0,n,0,n,0,0,m,0,0,0,0,f,h
g,f,0,0,0,n,0,0,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
ITM a 7,3
EXT 7,3 j 7,3
PAL 4

ROOM j
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
g,f,f,f,f,f,f,f,f,f,f,f,f,f,f,h
g,f,0,0,0,0,0,k,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,0,0,0,0,0,0,0,f,h
g,f,0,0,0,0,0,r,0,0,0,0,0,0,f,h
g,f,f,f,f,f,f,i,f,f,f,f,f,f,f,h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
WAL f,i,j,k,o
END undefined 7,13
PAL 0

TIL f
11001110
00000000
01111011
00000000
11001110
00000000
01111011
00000000

TIL g
00000001
00000000
00000000
00000000
00000001
00000000
00000000
00000000

TIL h
00000000
00000000
10000000
00000000
00000000
00000000
10000000
00000000

TIL i
11000010
00111000
00101011
00111000
10111010
00111000
00111011
00000000

TIL j
11001110
00000000
00111101
00100100
10111100
00000000
01111011
00000000

TIL k
00000000
01111100
01000100
01001100
01111100
00010000
01111100
00111110

TIL l
00000000
00010000
00111000
01010100
00010000
00010000
00010000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL m
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110

TIL n
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000

TIL o
01111000
01000100
01000010
01000010
01000010
01000010
01111110
00000000

TIL p
01111110
10000001
10111101
10000001
10101101
10000001
01011110
00100000

TIL q
00011000
00011000
00010000
00111000
01011100
00011000
00010100
00100100

TIL r
00000000
01010100
00101010
01010100
00101010
01010100
00101010
00000000

SPR 10
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_j
POS 5 5,13

SPR 11
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_k
POS 5 6,12

SPR 12
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_l
POS 5 3,11

SPR 13
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_m
POS 5 5,9

SPR 14
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_n
POS 5 3,8

SPR 15
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_o

SPR 16
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_p
POS 5 5,6

SPR 17
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_q
POS 5 3,5

SPR 18
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_r
POS 5 5,3

SPR 19
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_s
POS 5 3,3

SPR A
00011000
00011000
00010000
00111000
01011100
00011000
00010100
00100100
>
00000000
00011000
00011000
00010000
00111000
01011100
00011100
00100100
POS 2 7,13

SPR b
00000000
01111100
01000100
01001100
01111100
00010000
01111100
00111110
>
00000000
01111100
01000100
01000100
01111100
00010000
01111100
00111110
DLG SPR_1
POS 2 7,2

SPR h
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_0
POS 4 8,2

SPR i
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_1
POS 4 9,2

SPR j
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_2
POS 4 10,2

SPR k
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_3
POS 4 11,2

SPR l
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_4
POS 4 12,2

SPR m
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_5
POS 4 13,2

SPR n
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_6
POS 4 7,5

SPR o
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_7
POS 4 9,7

SPR p
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_8
POS 4 11,5

SPR q
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_9
POS 4 12,9

SPR r
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_a
POS 4 12,5

SPR s
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_b
POS 4 10,8

SPR t
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_c
POS 4 9,5

SPR u
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_d
POS 4 8,10

SPR v
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_e
POS 4 10,10

SPR w
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_f
POS 4 13,9

SPR x
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_g
POS 4 13,7

SPR y
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_h
POS 4 12,11

SPR z
00000000
01110000
10001000
11111110
10000010
10000010
10000010
11111110
DLG SPR_i
POS 4 9,12

SPR 1a
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_t
POS 5 2,2

SPR 1b
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_u
POS 5 2,8

SPR 1c
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_v
POS 5 4,12

SPR 1d
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_w
POS 5 5,8

SPR 1e
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_x
POS 5 2,6

SPR 1f
01111000
01001100
01000010
01000010
01000010
01000010
01111110
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
POS 5 6,2

SPR 1g
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG SPR_y
POS 5 5,2

SPR 1h
00000000
01111100
01000100
01001100
01111100
00010000
01111100
00111110
>
00000000
01111100
01000100
01000100
01111100
00010000
01111100
00111110
DLG SPR_z
POS j 7,2

ITM 0
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
NAME note1
DLG ITM_0

ITM 1
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_1

ITM 2
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_2

ITM 3
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_3

ITM 4
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_4

ITM 5
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_5

ITM 6
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_6

ITM 7
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_7

ITM 8
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_8

ITM 9
01111110
10000001
10101101
10000001
10111101
10000001
01011110
00100000
>
01111110
10000001
10111001
10000001
10101101
10000001
01011110
00100000
DLG ITM_9

ITM a
00011000
00111000
00010000
00000000
00010000
00010000
00010000
00000000
>
00001000
00011100
00011000
00000000
00010000
00010000
00010000
00000000
DLG ITM_a

ITM b
01111000
01000100
01010010
01000010
01011010
01000010
01111110
00000000
DLG ITM_b

ITM c
00000000
01100000
10010000
11111110
10000010
10000010
10000010
11111110
DLG ITM_c

DLG ITM_0
"I'm home at last..." I said.

DLG ITM_1
"It's time to make a game." I said.

DLG ITM_2
"I have so much great ideas." I said.

DLG SPR_0
"AnnaWork" is the folder.

DLG SPR_1
"DankestMemes" is the folder.

DLG SPR_2
"Games" is the folder.

DLG SPR_3
"PicsTemp" is the folder.

DLG SPR_4
"Webm" is the folder.

DLG SPR_5
"BooksGameDesign" is the folder.

DLG SPR_6
"Temp123" is the folder.

DLG SPR_7
"DefinetlyNotAPorn" is the folder.

DLG SPR_8
"FromPinterest" is the folder.

DLG SPR_9
"DoNotDelete" is the folder.

DLG SPR_a
"ReadLater" is the folder.

DLG SPR_b
"MorrowindMods" is the folder.

DLG SPR_c
"cm3d2_mods" is the folder.

DLG SPR_d
"Creepypasta" is the folder.

DLG SPR_e
"Music" is the folder.

DLG SPR_f
"Portfolio" is the folder.

DLG SPR_g
"VN" is the folder.

DLG SPR_h
"OLD_OLD" is the folder.

DLG SPR_i
"MyProjects" is the folder.

DLG SPR_j
"""
"Detective.txt" is the file.
It describes a lovecraftian horror detective quest.
"""

DLG SPR_k
"""
"TheVault.txt" is the file.
It describes a mobile game in which the player should solve puzzles in order to decrypt ancient messages.
"""

DLG SPR_l
"""
"Lab.txt" is the file.
It describes a RPGMaker game in which the player should save the world from an insane labyrinth maker.
"""

DLG SPR_m
"""
"VetisGrounds.txt" is the file.
It describes a text-based game about the girl's struggles in the hell.
"""

DLG SPR_n
"""
"Prophet.txt" is the file.
It describes a mobile game in which the player should solve mysterious procedural generated prophecies.
"""

DLG SPR_o
"""
"TyrannyMod.txt" is the file.
It describes a mod for a game in which the player can befriend with one of the evil characters.
"""

DLG SPR_p
"""
"Mother.txt" is the file.
It describes a mod for a game in which the player should confront his long forgotten past.
"""

DLG SPR_q
"""
"Spaceship.txt" is the file.
It describes a mobile clicker game in which the player constructs a colony ship and fly to stars.
"""

DLG SPR_r
"""
"Mothership.txt" is the file.
It describes a mobile game in which the whole game server is one space station struggling to survive in cold space.
"""

DLG SPR_s
"""
"SayaNoSaya.txt" is the file.
It describes a fan-prequel to the famous horror visual novel.
"""

DLG SPR_t
"""
"Succubus.txt" is the file.
It describes a board card game in which the player should resist the temptation of the succubus.
"""

DLG SPR_u
"""
"PlagueInc.txt" is the file.
It describes a mod for the famous game in which the virus is a succubus.
"""

DLG SPR_v
"""
"Cultists.txt" is the file.
It describes a board card game in which the player should sacrifice cultists in order to summon an evil god.
"""

DLG SPR_w
"""
"Tribes.txt" is the file.
It describes a text-based RPG in which you should make a tribe and evolve.
"""

DLG SPR_x
"""
"SS13_For_Mobile.txt" is the file.
It describes a mobile game in which the player is an AI who can control the space station.
"""

DLG SPR_y
"""
"Liess.txt" is the file.
It describes a visual novel where the happiest ending is to be eaten by the alien monster.
"""

DLG ITM_3
"It's too hard..." I thought.

DLG ITM_4
"May be I should finish my other game?" I thought.

DLG ITM_5
"I have no talant..." I thought.

DLG ITM_6
"It is so confusing..." I thought.

DLG ITM_7
"I can't do anything." I thought.

DLG ITM_8
"I'll never finish anything." I thought.

DLG ITM_9
"Even this." I thought.

DLG ITM_a
"I should burn it all." I thought.

DLG ITM_b
I deleted the file.

DLG ITM_c
I deleted the folder.

DLG SPR_z
"""
It's time to make a game, but... {shuffle
  - I watch Bablylon-5.
  - I play games.
  - I surf YouTube.
  - I save a funny picture.
  - I e-mail to my friend.
}
"""

END 0


END undefined
Sorry for wasting your time.

VAR a
42

