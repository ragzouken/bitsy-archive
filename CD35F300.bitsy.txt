


# BITSY VERSION 4.6

! ROOM_FORMAT 1

PAL 0
NAME cell
84,73,71
115,130,138
159,180,191

PAL 1
NAME 6 night
239,196,187
239,196,187
239,196,187

PAL 2
NAME 5 night
105,18,10
105,18,10
105,18,10

PAL 3
NAME 4 night
169,214,215
169,214,215
169,214,215

PAL 4
NAME 3 night
171,171,227
171,171,227
171,171,227

PAL 5
NAME 2 night 
250,224,146
250,224,146
250,224,146

PAL 6
NAME tv 2
255,255,255
255,255,255
255,255,255

PAL 7
NAME tv 3
232,232,232
232,232,232
232,232,232

PAL 8
NAME tv 4
196,196,196
196,196,196
196,196,196

PAL 9
NAME tv 5
125,125,125
125,125,125
125,125,125

PAL a
NAME tv 6
36,36,36
36,36,36
36,36,36

PAL b
NAME night final
191,214,167
191,214,167
191,214,167

PAL c
NAME escape
84,73,71
84,73,71
84,73,71

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
0,0,0,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,d,0,0,0,0,0,0,m,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 6 left
WAL e,f,b,c,d
EXT 4,7 3 7,3
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
0,0,0,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,d,0,0,0,0,0,0,m,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 5 left
WAL e,f,b,c,d
ITM 0 11,10
ITM 6 5,10
EXT 4,7 4 5,2
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 6 night
ITM 1 7,4
ITM 1 6,3
ITM 1 8,3
ITM 1 7,2
EXT 6,4 2 6,7
EXT 5,3 2 6,7
EXT 6,2 2 6,7
EXT 7,1 2 6,7
EXT 8,2 2 6,7
EXT 9,3 2 6,7
EXT 8,4 2 6,7
EXT 7,3 2 6,7
EXT 7,5 2 6,7
PAL 1

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 5 night
ITM 2 4,2
ITM 2 5,1
ITM 2 6,2
ITM 2 5,3
EXT 3,2 5 6,7
EXT 4,3 5 6,7
EXT 4,1 5 6,7
EXT 5,0 5 6,7
EXT 6,1 5 6,7
EXT 7,2 5 6,7
EXT 5,2 5 6,7
EXT 5,4 5 6,7
EXT 6,3 5 6,7
PAL 2

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
0,0,0,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,0,0,0,0,0,0,0,m,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 4 left
WAL e,f,b,c,d
ITM 4 11,7
EXT 4,7 6 4,2
PAL 0

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 4 night
ITM 5 4,1
ITM 5 5,2
ITM 5 4,3
ITM 5 3,2
EXT 4,0 7 6,7
EXT 3,1 7 6,7
EXT 2,2 7 6,7
EXT 4,4 7 6,7
EXT 4,2 7 6,7
EXT 3,3 7 6,7
EXT 5,3 7 6,7
EXT 6,2 7 6,7
EXT 5,1 7 6,7
PAL 3

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
0,0,0,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,d,0,0,0,0,0,0,m,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 3 left
WAL e,f,b,c,d
ITM 9 5,10
EXT 4,7 8 5,2
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 3 night
ITM 7 4,2
ITM 7 5,1
ITM 7 6,2
ITM 7 5,3
EXT 5,2 9 6,7
EXT 4,1 9 6,7
EXT 4,3 9 6,7
EXT 6,3 9 6,7
EXT 6,1 9 6,7
EXT 5,0 9 6,7
EXT 3,2 9 6,7
EXT 7,2 9 6,7
EXT 5,4 9 6,7
PAL 4

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
0,0,0,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,d,0,0,0,0,0,0,m,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 2 left
WAL e,f,b,c,d
EXT 11,10 a 10,10
EXT 4,7 c 3,2
PAL 0

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,m,j,j,l,0,0,0
0,0,0,d,t,t,t,0,t,m,t,t,e,0,0,0
0,0,r,d,0,0,0,0,0,m,0,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 2 left a
WAL e,f,b,c,d
EXT 9,10 b 9,10
EXT 4,7 c 3,2
PAL 0

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,m,j,j,j,l,0,0,0
0,0,0,d,t,t,t,0,m,t,t,t,e,0,0,0
0,0,r,d,0,0,0,0,m,0,0,0,e,0,0,0
0,0,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 2 left b
WAL e,f,b,c,d
ITM 8 8,9
EXT 4,7 d 2,2
PAL 0

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 2 night a
ITM a 2,2
ITM a 3,1
ITM a 4,2
ITM a 3,3
EXT 2,1 e 6,7
EXT 4,1 e 6,7
EXT 3,0 e 6,7
EXT 3,4 e 6,7
EXT 1,2 e 6,7
EXT 2,3 e 6,7
EXT 4,3 e 6,7
EXT 5,2 e 6,7
EXT 3,2 e 6,7
PAL 5

ROOM d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 2 night b
ITM a 2,1
ITM a 1,2
ITM a 3,2
ITM a 2,3
EXT 1,1 f 6,7
EXT 3,1 f 6,7
EXT 3,3 f 6,7
EXT 2,0 f 6,7
EXT 0,2 f 6,7
EXT 4,2 f 6,7
EXT 1,3 f 6,7
EXT 2,2 f 6,7
EXT 2,4 f 6,7
PAL 5

ROOM e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
t,t,t,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,r,0,0,0,0,0,0,0,m,0,e,0,0,0
t,t,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 1 left a
WAL e,f,b,c,d
ITM c 3,10
ITM d 0,10
EXT 0,10 n 1,1
EXT 4,7 l 13,2
PAL 0

ROOM f
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,m,j,j,j,l,0,0,0
t,t,t,d,t,t,t,0,m,t,t,t,e,0,0,0
0,0,r,0,0,0,0,0,m,0,0,0,e,0,0,0
t,t,s,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 1 left b
WAL e,f,b,c,d
ITM b 7,9
ITM e 0,10
ITM f 3,10
EXT 7,9 g 1,1
EXT 0,10 n 1,1
EXT 4,7 l 13,13
PAL 0

ROOM g
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
v,u,v,u,v,u,v,u,v,u,v,u,v,u,v,u
u,v,u,v,u,v,u,v,u,v,u,v,u,v,u,v
NAME tv 1
EXT 1,0 h 1,1
EXT 2,1 h 1,1
EXT 1,2 h 1,1
EXT 0,1 h 1,1
PAL 0

ROOM h
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME tv 2
EXT 1,0 i 1,1
EXT 0,1 i 1,1
EXT 2,1 i 1,1
EXT 1,2 i 1,1
PAL 6

ROOM i
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME tv 3
EXT 1,0 j 1,1
EXT 0,1 j 1,1
EXT 1,2 j 1,1
EXT 2,1 j 1,1
PAL 7

ROOM j
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME tv 4
EXT 1,0 k 1,1
EXT 0,1 k 1,1
EXT 1,2 k 1,1
EXT 2,1 l 1,1
PAL 8

ROOM k
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME tv 5
EXT 1,0 l 1,1
EXT 0,1 l 1,1
EXT 1,2 l 1,1
EXT 2,1 l 1,1
PAL 9

ROOM l
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME tv 6
ITM h 13,1
ITM h 13,3
ITM h 14,2
ITM h 13,12
ITM h 12,13
ITM h 13,14
ITM h 14,13
ITM h 12,2
EXT 1,0 m 3,2
EXT 0,1 m 3,2
EXT 2,1 m 3,2
EXT 1,2 m 3,2
EXT 13,0 o 6,7
EXT 12,1 o 6,7
EXT 14,1 o 6,7
EXT 11,2 o 6,7
EXT 12,3 o 6,7
EXT 13,2 o 6,7
EXT 15,2 o 6,7
EXT 14,3 o 6,7
EXT 13,4 o 6,7
EXT 13,11 p 6,7
EXT 13,13 p 6,7
EXT 12,12 p 6,7
EXT 11,13 p 6,7
EXT 12,14 p 6,7
EXT 13,15 p 6,7
EXT 14,14 p 6,7
EXT 15,13 p 6,7
EXT 14,12 p 6,7
PAL a

ROOM m
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME night final
ITM g 2,2
ITM g 3,1
ITM g 4,2
ITM g 3,3
END undefined 2,1
END undefined 1,2
END undefined 2,3
END undefined 3,4
END undefined 4,3
END undefined 5,2
END undefined 4,1
END undefined 3,0
PAL b

ROOM n
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME escape
EXT 1,0 m 3,2
EXT 0,1 m 3,2
EXT 2,1 m 3,2
EXT 1,2 m 3,2
PAL c

ROOM o
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,j,j,m,j,l,0,0,0
t,t,t,d,t,t,t,0,t,t,m,t,e,0,0,0
0,0,0,0,0,0,0,0,0,0,m,0,e,0,0,0
t,t,t,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 0 left a
WAL e,f,b,c,d
EXT 4,7 m 3,2
PAL 0

ROOM p
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,f,a,a,a,a,a,a,a,a,h,0,0,0
0,0,0,d,t,t,t,t,t,t,t,t,e,0,0,0
0,0,0,d,t,t,t,t,t,0,t,t,e,0,0,0
0,0,0,d,n,o,0,0,0,0,0,0,e,0,0,0
0,0,0,k,j,j,j,c,m,j,j,j,l,0,0,0
t,t,t,d,t,t,t,0,m,t,t,t,e,0,0,0
0,0,0,d,0,0,0,0,m,0,0,0,e,0,0,0
t,t,0,i,c,c,c,c,c,c,c,c,g,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME 0 left b
WAL e,f,b,c,d
ITM i 7,9
EXT 7,9 g 1,1
EXT 4,7 m 3,2
PAL 0

TIL a
00000000
00000000
00000000
00000000
00000000
00000000
11111111
11111111
NAME ceiling
WAL true

TIL c
11111111
11111111
00000000
00000000
00000000
00000000
00000000
00000000
NAME floor1

TIL d
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME wall

TIL e
01100000
01100000
01100000
01100000
01100000
01100000
01100000
01100000
NAME wall

TIL f
00000000
00000000
00000000
00000000
00000000
00000000
00000111
00000111
NAME wall

TIL g
11100000
11100000
00000000
00000000
00000000
00000000
00000000
00000000
NAME wall
WAL true

TIL h
00000000
00000000
00000000
00000000
00000000
00000000
11100000
11100000
NAME wall
WAL true

TIL i
00000111
00000111
00000000
00000000
00000000
00000000
00000000
00000000
NAME wall
WAL true

TIL j
11111111
11111111
00000000
00000000
00000000
00000000
00000000
00000000
NAME floor2
WAL true

TIL k
00000111
00000111
00000110
00000110
00000110
00000110
00000110
00000110
NAME wall
WAL true

TIL l
11100000
11100000
01100000
01100000
01100000
01100000
01100000
01100000
NAME wall
WAL true

TIL m
01000010
01111110
01000010
01000010
01000010
01111110
01000010
01000010
NAME ladder

TIL n
00000000
01000000
01111000
01000100
01111111
01111111
01000000
01000000
NAME bed

TIL o
00000000
00000000
00000000
00000000
11111100
11111100
00000100
00000100
NAME bed

TIL r
00001100
00011110
00010010
00011110
00111111
00111111
00111111
00011110
NAME guard 1
WAL false

TIL s
00010010
00010010
00000000
00000000
00000000
00000000
00000000
00000000
NAME guard 1
WAL true

TIL t
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME wall
WAL true

TIL u
01001001
01001001
01001001
01001001
01001001
01001001
01001001
01001001
>
00100100
00100100
00100100
00100100
00100100
00100100
00100100
00100100
NAME static 1

TIL v
00100100
00100100
00100100
00100100
00100100
00100100
00100100
00100100
>
01001001
01001001
01001001
01001001
01001001
01001001
01001001
01001001
NAME static 2

SPR 10
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 2 b
DLG SPR_a
POS b 3,10

SPR 11
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 1 b
DLG SPR_e
POS f 9,6

SPR 12
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 1 b
DLG SPR_m
POS e 9,6

SPR 13
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 1 a
DLG SPR_n
POS e 7,9

SPR 14
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 0 a
DLG SPR_o
POS o 3,10

SPR 15
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 0 a
DLG SPR_p
POS o 9,6

SPR 16
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 0 a
DLG SPR_q
POS o 7,9

SPR 18
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 0 b
DLG SPR_s
POS p 9,6

SPR 19
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 0 b
DLG SPR_t
POS p 3,10

SPR A
00000000
00000000
00111100
01111110
01101010
01111110
00111100
00000000
>
00000000
00000000
00111100
01111110
01010110
01111110
00111100
00000000
POS 0 5,10

SPR a
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 6
DLG SPR_0
POS 0 3,10

SPR c
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 6
DLG SPR_2
POS 0 9,6

SPR e
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
>
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
NAME tv 6
DLG SPR_3
POS 0 7,9

SPR f
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 5
DLG SPR_5
POS 2 3,10

SPR g
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 5
DLG SPR_7
POS 2 9,6

SPR i
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
>
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
NAME tv 5
DLG SPR_6
POS 2 7,9

SPR k
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 4
DLG SPR_8
POS 5 9,6

SPR l
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
>
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
NAME tv 4
DLG SPR_9
POS 5 7,9

SPR n
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 4
DLG SPR_b
POS 5 3,10

SPR o
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 3
DLG SPR_c
POS 7 3,10

SPR p
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 3
DLG SPR_d
POS 7 9,6

SPR r
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
>
01111110
01000010
01000010
01000010
01111110
00000000
00000000
00000000
NAME tv 3 
DLG SPR_f
POS 7 7,9

SPR s
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 2
DLG SPR_g
POS 9 9,6

SPR t
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 2
DLG SPR_h
POS 9 7,9

SPR u
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 2 a
DLG SPR_i
POS a 7,9

SPR v
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 2 b
DLG SPR_j
POS b 7,9

SPR w
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 2 a
DLG SPR_k
POS a 9,6

SPR x
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME calendar 1 b
DLG SPR_l
POS b 9,6

SPR y
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 2
DLG SPR_1
POS 9 3,10

SPR z
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 2 a
DLG SPR_4
POS a 3,10

ITM 0
00000000
00000000
00000000
00000000
00000000
00000000
00011100
00111100
>
00000000
00000000
00000000
00000000
00000000
00000000
00001110
00011110
NAME rat
DLG ITM_0

ITM 1
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME 6 night
DLG ITM_1

ITM 2
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME 5 night
DLG ITM_2

ITM 4
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME bug
DLG ITM_3

ITM 5
00000000
00000000
00000000
00000000
00000000
00100000
00000000
00000000
NAME 4 night
DLG ITM_4

ITM 6
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME crumbs
DLG ITM_5

ITM 7
01111110
11111011
11110001
11111011
11101111
11111111
11111111
01111110
NAME 3 night
DLG ITM_6

ITM 8
01000010
01111110
01000010
01000010
01000010
01111110
01000010
01000010
NAME ladder

ITM 9
00000000
00000000
00000000
00000000
00000000
00000000
00011100
00011110
NAME dead rat
DLG ITM_7

ITM a
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME 2 night
DLG ITM_8

ITM b
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 1 b
DLG ITM_9

ITM c
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 1 a
DLG ITM_a

ITM d
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME escape
DLG ITM_b

ITM e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME escape 2
DLG ITM_c

ITM f
00000110
00000110
00000110
00000110
00000110
00000110
00000110
00000110
NAME door 1 b
DLG ITM_d

ITM g
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME night final
DLG ITM_e

ITM h
00000000
00000000
00100000
00000000
00000000
00000000
00000000
00000000
NAME night 1 a
DLG ITM_f

ITM i
01111110
01010110
01101010
01010110
01111110
00000000
00000000
00000000
>
01111110
01101010
01010110
01101010
01111110
00000000
00000000
00000000
NAME tv 0 b
DLG ITM_g

DLG ITM_0
the rat was scared away.

DLG SPR_2
"""
{sequence
  - a digital calendar is embedded into the wall.                  6 DAYS UNTIL TRIAL
  - 6 DAYS UNTIL TRIAL
}
"""

DLG SPR_3
"""
the TV is your only window into the outside world.       
{shuffle
  - "our products are all on sale now!"
  - "record-breaking lows were reported in florence"
  - "expect showers in the late afternoon"
  - "governor proposes transit fare hikes"
  - "top diplomat for azar-5 leaving"
  - "is a high-protein diet for you?"
}
"""

DLG SPR_0
it's locked.

DLG SPR_5
"""
{sequence
  - it's locked.
}
"""

DLG SPR_6
"""
{shuffle
  - "severe drought has swept across finland"
  - "for sale: baby!"
  - "amazon CEOs excommunicate customer for high treason"
  - "pop-up respiration stations are all the rage"
  - "toronto's air quality has dropped significantly"
  - "is a paleo diet for you?"
}
"""

DLG SPR_7
"""
{sequence
  - a digital calendar is embedded into the wall.                  5 DAYS UNTIL TRIAL
  - 5 DAYS UNTIL TRIAL
}
"""

DLG ITM_1
"""
{clr1}she{clr1} waits for you at the top of a hill.                       she motions for you to join her. 
                        
so, together, you watch the dying sun.                     as the light fades, she leans in close.                          "dear, don't you know this isn't real?"
"""

DLG ITM_2
"""
{clr1}wolves{clr1} have caught your scent.                   
the creatures weave through the pines effortlessly,         while you stumble through the forest.                       you realize just how useless your legs are in the snow.      your foot twists, and you hit your head on the fall down.     wolf breath is on your bare neck.                           oddly enough, this is the calmest you've felt in years.   as canine teeth tear into you, you merely sigh.                doesn't hurt, at least.
"""

DLG SPR_8
"""
{sequence
  - a digital calendar is embedded into the wall.                  4 DAYS UNTIL TRIAL
  - 4 DAYS UNTIL TRIAL
}
"""

DLG SPR_9
"""
{shuffle
  - "how this young couple bought their very own planet"
  - "all coastal towns in yamhill have evacuated"
  - "is ginseng tea the cure for YOUR brain?"
  - "android activists have been successfully contained"
  - "eye implants: when to spot a hoax"
  - "are you there? are you there? are you t"                   "are your pets in good hands?"
}
"""

DLG ITM_3
"""
aw cool it's a bug!           
                            it'll keep you from being too lonely.
"""

DLG SPR_b
"""
{
  - {item "bug"} == 1 ?
    the guard sees you messing around with the bug.           it looks ... sorry?            
                               
it's hard to read robotic expressions.
  - else ?
    it's locked.
}
"""

DLG ITM_4
"""
the {clr1}sea{clr1} engulfs you.       
                                the cool caress feels like nothing more than a tickle.     bubbles rise all around you, out your mouth; but you breathe.    violet jellyfish are harmless against your skin.              a glowing squid finds its home in your coral garden.           you've lived for so {clr1}long{clr1}.
"""

DLG SPR_c
"""
{
  - {item "bug"} == 1 ?
    you make a point of showing your bug off to the guard.         
 it hesitates before approaching the door.                       
the bug does a tiny jump. the guard claps politely.
  - else ?
    it's locked.
} 
"""

DLG SPR_d
"""
{sequence
  - a digital calendar is embedded into the wall.                  3 DAYS UNTIL TRIAL
  - 3 DAYS UNTIL TRIAL
}
"""

DLG SPR_f
"""
{sequence
  - "the republic of yr...........y....y"
  - the TV seems broken. you only hear and see static.
  - "..."
  - {shk}"..."{shk}
  - {shk}"ON THE DAWN OF THE FIFTH DAY, FREE US FROM OUR BONDS"{shk}
  - {shk}"YOU WILL BE GREATLY REWARDED"{shk}
  - {shk}"..."{shk}
  - "..."
  - "the republic of yrik has issued a national recall of nutella"
}
"""

DLG ITM_5
some crumbs are on the ground. from the rats, probably.

DLG ITM_6
"""
an alaskan {clr1}salmon{clr1} writhes in your hands.           
it's dying.                     
                                . . .                           
                                
your fangs dispatch it quickly.
"""

DLG SPR_g
"""
{sequence
  - a digital calendar is embedded into the wall.                  2 DAYS UNTIL TRIAL
  - 2 DAYS UNTIL TRIAL
}
"""

DLG SPR_h
"""
{sequence
  - static emanates from the TV. 
  - it's too far up the wall for you to fix it.
}
"""

DLG SPR_i
it's too far up the wall for you to fix it.

DLG SPR_j
"""
{
  - {item "ladder"} == 1 ?
    you knock at the TV. nothing happens.                    well, nothing to lose, right? you start smashing the screen.  . . .                          
    oh shit                  
    {shk}"YOU HAVE FREED US FROM OUR BONDS"{shk}             
    {shk}"LONG HAVE WE WAITED FOR THIS MOMENT"{shk}       
    {shk}"THANK YOU. YOU SHALL BE REWARDED"{shk}        
    {shk}"..."{shk}        
    
    ...
  - else ?
    it's too far up the wall for you to fix it.
}
"""

DLG SPR_k
"""
{sequence
  - a digital calendar is embedded into the wall.                  1 DAY UNTIL TRIAL
  - 1 DAY UNTIL TRIAL
}
"""

DLG SPR_l
"""
{sequence
  - a digital calendar is embedded into the wall.                  1 DAY UNTIL TRIAL
  - 1 DAY UNTIL TRIAL
}
"""

DLG SPR_1
"""
{
  - {item "bug"} == 1 ?
your bug is doing well. the guard appreciates seeing it.    you try striking up a conversation.      unfortunately, it wasn't programmed to converse.         you and the guard sit in mutual silence. the bug crawls on.
  - else ?
    it's locked.
} 
"""

DLG ITM_7
it's a dead rat.                ...                             :(

DLG SPR_4
"""
{
  - {item "bug"} == 1 ?
your bug is doing well. the guard appreciates seeing it.    you try striking up a conversation.      unfortunately, it wasn't programmed to converse.         you and the guard sit in mutual silence. the bug crawls on.
  - else ?
    it's locked.
} 
"""

DLG SPR_a
"""
{
  - {item "bug"} == 1 ?
your bug is doing well. the guard appreciates seeing it.    you try striking up a conversation.      unfortunately, it wasn't programmed to converse.         you and the guard sit in mutual silence. the bug crawls on.
  - else ?
    it's locked.
} 
"""

DLG ITM_8
"""
{clr1}sunlight{clr1} filters through your kitchen window.    
a cookbot is frying something over the stove.                 you were never able to afford something like that.            it beeps excitedly before serving you.                    oatmeal is drizzled with sweetened gooseberry sauce;     plain yogurt with kiwi and guava extract;                        poached bluebird eggs, sprinkled with dark ash.                  it's entirely tasteless. you tip the cookbot nicely.
"""

DLG SPR_e
"""
{sequence
  - a digital calendar is embedded into the wall.                  1 DAY UNTIL TRIAL
  - 1 DAY UNTIL TRIAL
}
"""

DLG ITM_9
"""
oh fuck                         . . .                       
{shk}"AS YOU RELEASED US, WE SHALL RELEASE YOU IN TURN"{shk}
{rbw}{shk}what the fuck{shk}{rbw}                       
{rbw}{shk}`*+;,;+*`*+;,;+*`*+;,;+*`*+;,;+*`*+;,;+*`{shk}{rbw}
"""

DLG SPR_m
"""
{sequence
  - a digital calendar is embedded into the wall.                  1 DAY UNTIL TRIAL
  - 1 DAY UNTIL TRIAL
}
"""

DLG SPR_n
static emanates from the TV.

DLG ITM_a
"""
{
  - {item "bug"} == 1 ?
you stare at the guard.        it does nothing.                not knowing what to say, you give it the bug.               it crawls onto its head.
  - else ?
    it's ... unlocked?
}
"""

DLG ITM_b
"""
{
  - {item "bug"} == 1 ?
you've never run so much in your life. no one seems to stop you. you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going
  - else ?
you are killed immediately after your back is turned.        
looks like the guard was still doing its job.
}
"""

DLG ITM_c
"""
{
  - {item "bug"} == 1 ?
you've never run so much in your life. no one seems to stop you. you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going        
                              you have to keep going
  - else ?
you are killed immediately after your back is turned.        
looks like the guard was still doing its job.
}
"""

DLG ITM_d
"""
{
  - {item "bug"} == 1 ?
you stare at the guard.        it does nothing.                not knowing what to say, you give it the bug.               it crawls onto its head.
  - else ?
    it's ... unlocked?
}
"""

DLG ITM_e
"""
{clr1}she{clr1} waits for you at the top of a hill.                       she motions for you to join her. 
                        
so, together, you watch the dying sun.                     as the light fades, she leans in close.                          and her fangs tear at your flesh.                          you accept this.                
                                blood stains the sky. you're alone.                          you accept this, too.           
                                ...                             
                                haven't you had this dream {clr1}before{clr1}?
"""

DLG SPR_o
"""
{sequence
  - YOU HAVE ONE (1) UNREAD NOTIFICATIONS
  - START MSG:
  -"we regret to inform you that the high court                has pushed your trial date back by a week.                      we apologize for any inconvenience.                  please accept a higher ration for your troubles."
  - END MSG
}
"""

DLG SPR_p
"""
{sequence
  - a digital calendar is embedded into the wall.     
TRIAL DAY
  - TRIAL DAY
  - {wvy}...{wvy}
  - {wvy}PROCESSING. PLEASE STAND BY.{wvy}
  - {wvy}...{wvy}
  - 7 DAYS UNTIL TRIAL
}
"""

DLG SPR_q
static emanates from the TV.

DLG ITM_f
you don't dream tonight.

DLG SPR_s
"""
{sequence
  - a digital calendar is embedded into the wall.     
TRIAL DAY
  - TRIAL DAY
  - {wvy}...{wvy}
  - {wvy}PROCESSING. PLEASE STAND BY.{wvy}
  - {wvy}...{wvy}
  - 7 DAYS UNTIL TRIAL
}
"""

DLG SPR_t
"""
{sequence
  - YOU HAVE ONE (1) UNREAD NOTIFICATIONS
  - START MSG:
  -"we regret to inform you that the high court                has pushed your trial date back by a week.                      we apologize for any inconvenience.                  please accept a higher ration for your troubles."
  - END MSG
}
"""

DLG ITM_g
"""
oh fuck                         . . .                       
{shk}"AS YOU RELEASED US, WE SHALL RELEASE YOU IN TURN"{shk}
{rbw}{shk}what the fuck{shk}{rbw}                       
{rbw}{shk}`*+;,;+*`*+;,;+*`*+;,;+*`*+;,;+*`*+;,;+*`{shk}{rbw}
"""

VAR a
42

VAR 



