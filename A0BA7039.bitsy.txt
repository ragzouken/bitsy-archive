
burgerlands

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
NAME pinkburgers
254,195,251
143,255,253
255,231,157

PAL 1
NAME purpleymuted
237,215,255
231,191,254
238,254,244

PAL 2
NAME yellokitchen
243,255,114
185,217,255
249,209,255

PAL 3
NAME grassy
173,254,244
160,255,145
253,220,254

PAL 4
NAME softpink
255,248,244
255,214,255
242,194,253

ROOM 0
p,p,p,p,p,f,j,g,p,d,d,d,p,d,d,p
p,e,c,e,p,h,k,i,d,d,p,p,d,d,d,d
p,e,p,e,p,p,e,d,d,p,d,d,d,p,d,d
c,c,c,e,c,c,c,c,c,c,c,c,d,d,d,p
p,p,d,e,c,c,e,p,d,d,d,e,p,p,d,d
p,d,p,d,d,p,e,d,d,d,p,e,c,c,c,c
n,n,n,n,n,n,o,n,n,n,n,n,n,n,n,n
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
m,b,l,b,l,b,l,b,m,d,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,d,d,d,a,a,a,a
0,0,0,0,0,0,0,0,0,m,0,d,a,a,a,a
n,n,n,n,n,n,n,n,n,n,0,d,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,d,d,d,d,d
0,0,0,0,0,0,0,0,0,0,0,d,0,0,0,0
NAME burgerparlourL
ITM 3 13,15
ITM 3 14,15
ITM 1 12,15
ITM 3 0,15
ITM 2 1,1
ITM 2 1,2
ITM 2 2,1
ITM 2 11,5
ITM 2 11,4
ITM 2 0,7
ITM 2 14,7
ITM 2 15,7
ITM 2 1,15
ITM 0 2,15
ITM 0 5,15
ITM 2 4,15
ITM 3 3,15
ITM 3 6,15
ITM 3 9,15
ITM 0 8,15
ITM 2 7,15
ITM 0 15,15
ITM 2 10,15
EXT 15,7 1 0,7
EXT 15,8 1 0,8
EXT 15,9 1 0,9
EXT 15,10 1 0,10
EXT 15,11 1 0,11
EXT 15,12 1 0,12
EXT 15,13 1 0,13
EXT 15,5 1 0,5
EXT 6,1 2 7,7
EXT 15,15 1 0,15
EXT 0,3 6 15,3
PAL 0

ROOM 1
p,p,d,d,d,p,d,d,d,p,d,d,d,d,p,d
d,d,d,d,d,d,d,e,d,d,d,p,d,d,e,d
d,d,d,p,e,d,p,e,p,d,d,p,p,d,e,d
p,d,c,c,c,c,c,c,c,e,d,d,d,d,e,p
d,d,e,d,d,d,p,d,d,e,c,c,c,c,e,d
c,c,e,d,p,d,d,d,d,d,d,p,d,d,e,p
n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,1e
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,1e
a,a,a,a,d,a,a,a,a,a,a,a,a,a,a,1e
a,a,a,a,d,a,a,a,a,a,a,a,a,a,a,1e
a,a,a,a,d,a,a,a,a,a,a,a,a,a,a,1e
a,a,a,a,d,b,l,b,l,b,l,b,l,b,m,1c
a,a,a,a,d,0,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,n,n,n,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,m,0,0,0,0,0,0,0,0
NAME burgerparlourR
ITM 3 3,15
ITM 3 1,15
ITM 2 2,3
ITM 2 3,3
ITM 2 4,3
ITM 1 4,2
ITM 1 14,4
ITM 2 9,3
ITM 2 14,5
ITM 2 14,9
ITM 2 14,8
ITM 2 5,11
EXT 0,13 0 15,13
EXT 0,12 0 15,12
EXT 0,11 0 15,11
EXT 0,10 0 15,10
EXT 0,9 0 15,9
EXT 0,8 0 15,8
EXT 0,7 0 15,7
EXT 0,5 0 15,5
EXT 0,15 0 15,15
PAL 0

ROOM 2
v,0,q,0,0,0,0,v,0,0,v,0,q,0,0,q
0,q,0,0,0,v,0,0,0,0,0,0,0,q,0,v
q,0,0,0,0,q,q,q,q,q,0,0,0,0,q,0
v,0,0,q,q,0,0,0,v,0,q,q,0,v,0,q
0,0,q,0,0,0,0,0,0,0,0,0,q,0,0,0
0,q,0,0,v,0,0,0,0,v,0,0,0,q,0,0
q,0,v,0,0,0,f,j,g,0,0,0,v,0,q,0
r,r,r,r,r,r,h,k,i,r,r,r,r,r,r,r
p,p,p,p,p,p,p,e,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,e,p,p,p,f,j,g,p,p
p,p,p,p,p,p,p,e,p,p,p,s,u,t,p,p
p,p,p,p,p,e,c,c,c,c,c,c,e,c,e,p
p,f,j,g,p,e,p,p,p,p,p,p,e,p,e,p
p,s,u,t,p,e,p,p,f,j,g,p,c,c,c,p
p,p,e,p,p,e,p,p,s,u,t,p,e,p,p,p
p,p,e,c,c,c,c,c,c,e,c,c,e,p,p,p
NAME catshrines
EXT 7,7 0 6,1
EXT 2,13 3 6,1
EXT 9,14 4 6,1
EXT 12,10 5 6,1
PAL 1

ROOM 3
a,a,a,a,a,f,j,g,f,j,g,a,a,a,a,a
a,p,p,p,p,x,z,w,x,z,w,p,p,p,p,a
u,p,p,p,c,c,0,c,c,0,c,c,p,p,p,u
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,12,13,p,p,12,13,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,q,q,p,q,q,p,p,p,p,p,p
p,p,p,p,q,0,q,q,q,0,q,p,p,p,p,p
p,p,p,q,q,0,v,0,v,0,q,q,p,p,p,p
p,p,p,q,a,14,0,0,0,14,a,q,p,p,p,p
p,p,p,q,0,0,0,11,0,0,0,q,p,p,p,p
p,q,q,p,q,0,0,0,0,0,q,p,p,q,q,p
q,0,0,q,p,q,q,q,q,q,p,p,q,0,0,q
q,0,0,q,p,p,p,p,p,p,p,p,q,0,0,q
NAME catshrine1
EXT 9,1 2 2,13
PAL 2

ROOM 4
a,a,a,a,a,f,j,g,f,j,g,a,a,a,a,a
a,p,p,p,p,x,z,w,x,z,w,p,p,p,p,a
u,p,p,p,c,c,0,c,c,0,c,c,p,p,p,u
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,12,13,p,p,12,13,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,q,q,p,q,q,p,p,p,p,p,p
p,p,p,p,q,0,q,q,q,0,q,p,p,p,p,p
p,p,p,q,q,0,v,0,v,0,q,q,p,p,p,p
p,p,p,q,a,16,0,0,0,16,a,q,p,p,p,p
p,p,p,q,0,0,0,15,0,0,0,q,p,p,p,p
p,q,q,p,q,0,0,0,0,0,q,p,p,q,q,p
q,0,0,q,p,q,q,q,q,q,p,p,q,0,0,q
q,0,0,q,p,p,p,p,p,p,p,p,q,0,0,q
NAME catshrine2
EXT 9,1 2 9,14
PAL 3

ROOM 5
a,a,a,a,a,f,j,g,f,j,g,a,a,a,a,a
a,p,p,p,p,x,z,w,x,z,w,p,p,p,p,a
u,p,p,p,c,c,0,c,c,0,c,c,p,p,p,u
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,12,13,p,p,12,13,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,p,p,p,p,p,p,p,p,p,p,p
p,p,p,p,p,q,q,p,q,q,p,p,p,p,p,p
p,p,p,p,q,0,q,q,q,0,q,p,p,p,p,p
p,p,p,q,q,0,v,0,v,0,q,q,p,p,p,p
p,p,p,q,a,14,0,0,0,z,a,q,p,p,p,p
p,p,p,q,0,0,0,11,0,0,0,q,p,p,p,p
p,q,q,p,q,0,0,0,0,0,q,p,p,q,q,p
q,0,0,q,p,q,q,q,q,q,p,p,q,0,0,q
q,0,0,q,p,p,p,p,p,p,p,p,q,0,0,q
NAME catshrine3
ITM 1 5,12
ITM 1 9,12
ITM 3 9,9
ITM 3 5,9
ITM 3 1,14
ITM 3 2,14
ITM 3 13,14
ITM 3 14,14
EXT 9,1 2 12,10
EXT 4,12 8 7,5
EXT 10,12 8 8,5
PAL 4

ROOM 6
1l,1l,1l,1l,1l,1l,p,d,d,p,d,d,p,p,p,p
1l,1l,1l,1l,1l,1l,d,d,c,c,c,e,p,d,p,p
1l,1l,1l,1l,p,d,d,p,e,d,d,e,d,d,p,p
1l,1l,1l,1l,d,c,c,c,c,c,c,c,c,c,c,c
p,p,p,p,p,p,d,d,d,d,p,e,p,d,p,p
r,r,r,r,r,r,r,r,r,r,c,c,d,d,p,p
p,1d,1f,1f,1f,1f,1f,1e,p,d,e,r,r,r,r,n
p,1d,1f,1f,1f,1f,1f,1e,d,d,e,d,1i,1j,p,1d
p,1d,1f,1f,1f,1f,1f,1e,d,d,e,d,1g,1h,p,1d
d,1b,n,n,m,n,n,1c,p,d,c,c,c,d,d,1d
p,p,p,18,19,1a,p,d,1i,1k,1j,p,e,d,p,1d
p,d,p,s,o,t,p,p,h,o,i,d,e,p,d,1d
c,c,c,c,c,c,c,c,c,c,d,d,e,c,d,1d
d,1i,1j,p,d,d,p,d,d,e,c,c,c,c,d,1b
a,1g,1h,a,p,p,d,d,p,d,d,d,d,e,d,d
p,e,c,c,c,c,c,c,c,c,c,c,c,e,p,d
NAME town
ITM 2 8,1
ITM 2 8,2
ITM 2 10,1
ITM 2 5,3
ITM 2 13,13
ITM 2 13,14
ITM 2 12,13
ITM 2 10,8
ITM 2 9,1
ITM 2 1,12
ITM 1 2,8
ITM 4 3,6
ITM 4 6,6
ITM 4 6,8
ITM 4 2,7
EXT 15,3 0 0,3
EXT 4,11 6 4,9
EXT 1,14 7 5,12
EXT 4,9 6 4,11
EXT 12,8 8 5,12
PAL 0

ROOM 7
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,1n,1o,1o,1o,1o,1o,1o,1o,1o,1m,q,q,q
q,q,q,1d,19,12,13,19,d,d,d,d,1e,q,q,q
q,q,q,1d,d,d,d,d,d,d,d,d,1e,q,q,q
q,q,q,1d,1r,1s,0,1t,0,0,0,1q,1e,q,q,q
q,q,q,1d,n,n,n,n,n,n,n,n,1e,q,q,q
q,q,q,1d,d,d,d,d,d,d,d,d,1e,q,q,q
q,q,q,1d,d,d,1y,d,d,1y,d,d,1e,q,q,q
q,q,q,1d,d,d,d,d,d,d,d,d,1e,q,q,q
q,q,q,1d,1t,0,1x,1u,1v,1w,1x,1p,1e,q,q,q
q,q,q,1b,n,o,n,n,n,n,n,n,1c,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
NAME home
ITM 5 7,6
ITM 1 6,6
ITM 1 8,6
ITM 1 9,6
ITM 1 10,6
ITM 1 5,11
ITM 1 1,1
ITM 1 14,1
ITM 1 1,14
ITM 1 14,14
ITM 2 1,4
ITM 2 1,5
ITM 2 1,6
ITM 2 6,1
ITM 2 7,1
ITM 2 8,1
ITM 2 14,4
ITM 2 14,5
ITM 2 14,6
ITM 2 8,14
ITM 1 2,1
EXT 5,12 6 1,14
EXT 11,11 7 11,6
EXT 11,6 7 11,11
PAL 0

ROOM 8
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,1n,1o,1o,1o,1o,1o,1o,1o,1o,1m,q,q,q
q,q,q,1d,d,d,d,19,19,d,d,d,1e,q,q,q
q,q,q,1d,1y,d,f,k,k,g,d,1y,1e,q,q,q
q,q,q,1d,1q,1x,0,0,0,0,1x,0,1e,q,q,q
q,q,q,1d,n,n,n,n,n,n,n,n,1e,q,q,q
q,q,q,1d,v,v,v,v,v,v,v,v,1e,q,q,q
q,q,q,1d,d,d,1y,d,d,1y,d,d,1e,q,q,q
q,q,q,1d,1p,d,d,d,d,d,d,d,1e,q,q,q
q,q,q,1d,1x,1x,1x,1x,1x,1x,1x,1x,1e,q,q,q
q,q,q,1b,n,o,n,n,n,n,n,n,1c,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,q
NAME cathouse
ITM 2 10,11
ITM 2 9,11
ITM 2 8,11
ITM 2 7,11
ITM 2 11,11
ITM 2 6,6
ITM 2 7,6
ITM 2 8,6
ITM 2 9,6
ITM 2 11,6
EXT 4,10 8 4,6
EXT 4,6 8 4,10
EXT 5,12 6 12,8
EXT 7,5 5 4,12
EXT 8,5 5 10,12
PAL 4

TIL 10
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME wall_t

TIL 11
00000000
01100110
01111110
00111100
00011000
00011000
10011001
01100110
NAME catmouth

TIL 12
00100000
00100000
01110000
00101100
00110010
01100111
10100110
00000000
NAME ne

TIL 13
00000000
00000000
00111100
00000000
00000000
01000000
00111100
00000000
NAME ko

TIL 14
00000000
00000000
00000000
10000001
11111111
01111110
00000000
10101010
NAME eyeclosed

TIL 15
00000000
01100110
01111110
00111100
00011000
00100100
01000010
00111100
NAME mouthopen

TIL 16
00000000
00111100
01000010
11111111
11110001
11110001
00000000
10101010
>
00000000
00111100
01000010
11111111
10001111
10001111
00000000
10101010
NAME eyelook

TIL 17
00000000
00000000
00000000
10000001
11111111
01111110
00000000
10101010
>
00000000
00111100
01000010
11111111
10111001
10111001
00000000
10101010
NAME eyewink

TIL 18
00000011
00001111
00011111
00111111
01111111
01111111
11111111
11111111
NAME roof_l
WAL true

TIL 19
11111111
11111111
11011011
10100101
10111101
11011011
11100111
11111111
NAME roofheart_m
WAL true

TIL a
00000000
01010000
01110000
00100000
00001010
00001110
00000100
00000000
NAME hearts

TIL b
11111111
11001100
11001100
00110011
00110011
00000000
11111111
11111111
NAME counter
WAL true

TIL c
11111111
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME path_horizontal

TIL d
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME blockwall
WAL true

TIL e
10000001
10000001
10000001
10000001
10000001
10000001
10000001
10000001
NAME path_vertical

TIL f
00110000
01001011
01010100
10000000
10001110
10010101
10001110
10000000
NAME catdoor_tl
WAL true

TIL g
00001100
11010010
00101010
00000001
01110001
10101001
01110001
00000001
NAME catdoor_tr
WAL true

TIL h
10000000
10000111
10001000
10001000
10001000
10001000
10001111
01110000
NAME catdoor_bl
WAL true

TIL i
00000001
11100001
00010001
00010001
00010001
00010001
11110001
00001110
NAME catdoor_br
WAL true

TIL j
01111110
10000001
00000000
00000000
00000000
00101010
00010100
00000000
NAME catdoor_tm
WAL true

TIL k
00000000
11111111
00011000
00011000
00011000
00011000
11111111
00000000
>
00000000
11111111
01100110
00100100
00100100
00100100
00111100
11100111
NAME catdoor_bm

TIL l
11111111
00011000
01111110
10101011
10101011
01010101
00000000
11111111
NAME till
WAL true

TIL m
11100111
10000001
10000001
10100101
10000001
10000001
10000001
11100111
NAME staffdoor

TIL n
00000000
00000000
00000000
11111111
00000000
11111111
00000000
00000000
NAME linewall_bm
WAL true

TIL o
00111100
01000010
10000001
10000001
10000001
10000001
10000001
01111110
NAME door

TIL p
11011111
10101101
11011010
11111101
10111111
01011011
10110101
11111011
NAME flowers
WAL true

TIL q
10000001
11000011
00000000
01110000
11111000
00000000
00000011
10000111
>
11000000
11100001
00000000
00111000
01111100
00000000
10000001
11000011
NAME sky
WAL false

TIL r
00000000
00000000
00000000
01010101
11111111
01010101
01010101
01010101
NAME gatewall
WAL true

TIL s
10000000
10000000
10000000
10000000
10000000
10000000
10000000
01111111
NAME cat_bl
WAL true

TIL t
00000001
00000001
00000001
00000001
00000001
00000001
00000001
11111110
NAME cat_br
WAL true

TIL u
01100110
11011011
10111101
01101110
00111100
00011000
00000000
11111111
>
01100110
11011011
10111101
01110110
00111100
00011000
00000000
11111111
NAME heart_bm1

TIL v
01001001
00010100
00100010
01000001
00100010
00010100
01001001
00000000
>
00000000
00101010
00010100
00100010
00010100
00101010
00000000
00000000
NAME sparkle1

TIL w
00000001
00000001
00000001
00000001
00000001
00000001
00000001
00000001
NAME wall_r

TIL x
10000000
10000000
10000000
10000000
10000000
10000000
10000000
10000000
NAME wall_l

TIL y
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111
NAME wall_b

TIL z
00000000
00111100
01000010
11111111
10111001
10111001
00000000
10101010
NAME eyeopen

TIL 1a
11000000
11110000
11111000
11111100
11111110
11111110
11111111
11111111
NAME roof_r
WAL true

TIL 1b
00101000
00101000
00101000
00101111
00100000
00011111
00000000
00000000
NAME linewall_bl
WAL true

TIL 1c
00010100
00010100
00010100
11110100
00000100
11111000
00000000
00000000
NAME linewall_br
WAL true

TIL 1d
00101000
00101000
00101000
00101000
00101000
00101000
00101000
00101000
NAME linewall_ml
WAL true

TIL 1e
00010100
00010100
00010100
00010100
00010100
00010100
00010100
00010100
NAME linewall_mr
WAL true

TIL 1f
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101
NAME concrete

TIL 1g
10011110
10111111
10111111
10100111
10100111
10111111
10111111
01111111
NAME house_bl
WAL false

TIL 1h
00000001
00111001
01010101
01111101
01010101
00111001
00000001
11111110
NAME house_br
WAL true

TIL 1i
00000011
00001110
00011001
00100110
01111001
01001110
11111111
10000000
NAME house_tl
WAL true

TIL 1j
11000000
01110000
10011000
01100100
10011110
01110010
11111111
00000001
NAME house_tr
WAL true

TIL 1k
11111111
01000010
10011001
00111100
10011001
01000010
11111111
00000000
NAME housecross_tm
WAL true

TIL 1l
10010010
01001001
00100100
01001001
10010010
01001001
00100100
01001001
>
00100101
10010010
01001001
10010010
00100101
10010010
01001001
10010010
NAME seawall
WAL true

TIL 1m
00000000
00000000
11111000
00000100
11110100
00010100
00010100
00010100
NAME linewall_tr
WAL true

TIL 1n
00000000
00000000
00011111
00100000
00101111
00101000
00101000
00101000
NAME linewall_tl
WAL true

TIL 1o
00000000
00000000
11111111
00000000
11111111
00000000
00000000
00000000
NAME linewall_tm

TIL 1p
00000111
00000100
00011100
00010000
01110000
01000000
11000000
00000000
NAME stairs_up

TIL 1q
00000000
00000000
00000000
11111111
01000010
01111110
10000001
11111111
NAME stairs_down

TIL 1r
10000000
10111000
11000101
11111110
11111111
10000000
10000000
01111111
NAME bed_l

TIL 1s
00000000
00000000
11111110
00000001
11111111
00000001
00000001
11111110
NAME bed_r

TIL 1t
00000000
00011000
00100100
01111110
11100111
10011001
10011001
01111110
>
00000000
00011000
00100100
01111110
11100111
10011001
10011001
01111110
NAME stereostill

TIL 1u
01111111
10000100
10000100
10000100
01111111
10000000
10000000
01111111
NAME sofa_l

TIL 1v
11111110
00100001
00100001
00100001
11111110
00000001
00000001
11111110
NAME sofa_r

TIL 1w
00100100
01111110
10000001
10111101
10111101
10000001
01111110
01000010
NAME tv

TIL 1x
00100000
01010100
00001010
00011000
00101100
00001000
00111110
00011100
NAME houseplant

TIL 1y
11111111
11111111
11000111
10101011
10000011
10101011
11000111
11111111
NAME windowwall
WAL true

SPR A
11111111
10000001
01111110
01011010
01111110
01111110
10000001
11111111
POS 0 2,11

SPR a
01111110
10000001
10100101
10000001
10100101
10011001
10000001
01111110
>
01111110
10000001
10100101
10000001
10000001
10111101
10000001
01111110
NAME humanC
DLG SPR_0
POS 0 2,9

SPR b
00000000
01100110
11011011
10111101
11101111
01111110
00011000
00000000
>
00000000
01100110
11011011
10111101
11110111
01111110
00011000
00000000
NAME heartC
DLG SPR_1
POS 0 4,9

SPR c
00101000
01101100
01101100
11111110
11010110
10111010
01111100
00000000
NAME yuC
DLG SPR_2
POS 0 5,6

SPR d
00000000
00000000
11000110
11111110
10111010
11101110
11111110
00000000
NAME cloudC
DLG SPR_3
POS 1 12,11

SPR e
00111000
01000100
10101010
11000101
10000001
10010001
01101110
00000000
NAME ghosto
DLG SPR_5
POS 0 6,9

SPR f
01111100
10000010
10101010
10000010
01111100
00010000
01111100
00010000
>
00111000
01000100
01101100
01000100
00111000
00010000
00111000
00010000
NAME boi
DLG SPR_4
POS 0 4,8

SPR g
11111111
10000001
01111110
01011010
01111110
01111110
10000001
11111111
>
11111111
10000001
00111100
01011010
01111110
00111100
10000001
11111111
NAME server1
DLG SPR_7
POS 0 4,11

SPR h
11111111
10000001
01111110
01011010
01111110
01111110
10000001
11111111
>
11111111
10000001
00111100
01011010
01111110
00111100
10000001
11111111
NAME server2
DLG SPR_8
POS 0 6,11

SPR i
11111111
10000001
01111110
01011010
01111110
01111110
10000001
11111111
>
11111111
10000001
00111100
01011010
01111110
00111100
10000001
11111111
NAME server3
DLG SPR_6
POS 1 10,13

SPR j
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME burgerdude1
DLG SPR_9
POS 1 8,7

SPR k
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME burgerdude2
DLG SPR_a
POS 0 12,7

SPR l
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME shrinedude1
DLG SPR_b
POS 2 6,8

SPR m
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME shrinedude2
DLG SPR_c
POS 2 14,10

SPR n
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME shrinedude3
DLG SPR_d
POS 2 7,14

SPR o
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME shrinedude4
DLG SPR_e
POS 2 1,14

SPR p
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME fielddude1
DLG SPR_f
POS 0 3,4

SPR q
01111000
10100110
01110001
01011111
00111111
00111111
00110011
00010001
>
01111000
10100110
01110001
01011111
00111111
00111111
00100010
00100010
NAME horseBlaze
DLG SPR_g
POS 6 5,7

SPR r
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME fielddude2
DLG SPR_h
POS 6 6,2

SPR s
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME fielddude3
DLG SPR_i
POS 6 9,7

SPR t
01111110
10000001
00111100
01011010
00111100
01110110
10011001
01111110
NAME mum
DLG SPR_j
POS 7 4,10

SPR u
01111110
10000001
01111110
01011010
00111100
01100110
00111100
11111111
NAME dad
DLG SPR_k
POS 7 8,10

SPR v
00000000
00011000
11100111
10000001
10100101
10000001
01111110
00011000
>
00011000
01100110
10000001
10100101
10000001
01111110
00011000
00011000
NAME cathousedude1
DLG SPR_l
POS 8 10,10

ITM 0
01111110
11111111
00000000
01010101
10101010
00000000
11111111
01111110
>
00000000
01111110
11111111
01010101
10101010
11111111
01111110
00000000
NAME burger
DLG ITM_0

ITM 1
00000000
00100100
01111110
01111110
00111100
00011000
00000000
00000000
NAME real love
DLG ITM_2

ITM 2
00000000
00011000
00100100
01011010
01011010
00100100
00011000
00000000
>
00011000
00100100
01011010
10100101
10100101
01011010
00100100
00011000
NAME money

ITM 3
00000000
00100100
01011010
01000010
00100100
00011000
00000000
00000000
NAME fakelove
DLG ITM_1

ITM 4
01111000
10100110
01110001
01011111
00111111
00111111
00110011
00010001
>
01111000
10100110
01110001
01011111
00111111
00111111
00100010
00100010
NAME horseo
DLG ITM_3

ITM 5
00000000
00011000
00100100
01111110
11100111
10011001
10011001
01111110
NAME stereoo
DLG ITM_4

DLG ITM_0
ooooh a burger!

DLG SPR_0
"""
{
  - {item "burger"} == 4 ?
    {rbw}thank you so much!! these burgers are delicious{rbw}
  - else ?
    {sequence
      - hiya i'd like 3 burgers please!
      - uh...can i make that 4 actually?
      - now i think about it, i am pretty hungry...
      - burgers...
    }

}
"""

DLG SPR_1
"""
{
  - {item "real love"} == 3 ?
    {wvy}{clr1}I'm loved up! <3 {clr1}{wvy}
  - else ?
    {sequence
      - can i get some real love please? 
      - enough to heal me temporarily *sigh*
      - that cat in the next room is very nice!
      - love...
    }
}
"""

DLG SPR_2
"""
{sequence
  - Yu: i get so sleepy looking at the flower fields
  - Yu: ..too..sleepy to...move...
  - Yu: zzzz...
}
"""

DLG SPR_3
"""
{cycle
  - Cloud: waah~ have you seen Yu? 
  - Cloud: i hear they have awesome burgers here 
  - Cloud: the floor is so pretty!
  - Cloud: I can sense other cats in the air outside. it feels comforting
  - Cloud: *sniff*
  - Cloud: careful of over-enthusiastic love! ;-;
}
"""

DLG SPR_4
"""
{
  - {item "money"} == 20 ?
    {rbw}I'm rich!! thank you kindly{rbw}
  - else ?
    {sequence
      - dude i'm gonna need all these coins round here
      - I'm not a thief, promise
      - or do i promise?!?
      - haha I was just kidding
      - but I could do with some help from you, friend
      - i seem to be constrained to the spot...
      - coins...
    }

}
"""

DLG SPR_5
"""
{
  - {item "real love"} == 3 ?
    {clr2}hmm I'm not sure if i want anything anymore. I'm just glad you've found real love! Sorry for wasting your time{clr2}
  - else ?
    {sequence
      - booooo! 
      - haha, i haven't decided what i want yet
      - i don't think this server likes me...
      - ah.
    }

}
"""

DLG SPR_6
"""
{sequence
  - sure is quiet round here!
  - {wvy}la la la~~{wvy}
  - I do like to sing
  - I get kinda nervous if there are people listening to me sing though...
  - singing in the shower is the way to go!!!
  - could you please uh... let me count the till money for a bit?  not that i'm going to sing or anything...
}
"""

DLG SPR_7
"""
{cycle
  - pretty busy today!
  - always glad to be here ^-^
  - hope you're doing okay <3
}
"""

DLG SPR_8
"""
{
  - {item "real love"} == 3 ?
    {clr2}thank goodness that ghost has found peace{clr2}
  - else ?
    {sequence
      - heya!
      - ah, i wish i could be outside right now
      - when will this ghost make up their mind...
      - I wonder if they could just pass through the storeroom wall to get what they want...
      - *sigh*
    }
}
"""

DLG ITM_1
it's true love! <3

DLG ITM_2
so this is REAL love...

DLG SPR_9
"""
{
  - {item "real love"} == 3 ?
    {clr1}you have heart eyes!{clr1}
  - else ?
    what are in your eyes, friend?
}
"""

DLG SPR_a
"""
{
  - {item "money"} == 10 ?
    whoa you look expensive!
  - {item "money"} >= 15 ?
    are your pockets weighing you down...
  - {item "money"} < 10 ?
    you work here, right? how do you get money?
  - {item "money"} == 11 ?
    whoa you look expensive!
  - {item "money"} == 12 ?
    whoa you look expensive!
  - {item "money"} == 13 ?
    whoa you look expensive!
  - {item "money"} == 14 ?
    whoa you look expensive!
}
"""

DLG SPR_b
"""
{sequence
  - welcome!
  - you've made it to the er...
  - the cat fields!
  - have a look around!
}
"""

DLG SPR_c
"""
{sequence
  - they say you should use this space to reflect...
  - think about the past, think about the future...
  - put on your favourite song and close your eyes for a bit
  - feel the soft wind rustle the grass and flowers
  - the cat spirits love and care about you
  - are you feeling better now?
  - come back soon!
}
"""

DLG SPR_d
"""
{sequence
  - the cat spirit's heart is like a clock
  - always beating, ticking to it's own rhythm
  - it may seem consistent and it may seem like it knows where it's going...
  - but it's kinda lost
  - lost, like you in the cat shrines!
  - nah I'm kidding, i think you and the cat spirit have ideas on where you're heading
  - please take care on your journey!
}
"""

DLG SPR_e
"""
{
  - {item "fakelove"} == 8 ?
    {clr1}now I can turn it into real real love! thank you so much, i hope that your heart feels more full too{clr1}
  - else ?
    {sequence
      - bopping around outside makes me happy
      - hey if you have some time, could you bring me all the fake love hanging around that lovely burger joint? I'd like to try turn it into something good
      - fake love...
    }
}
"""

DLG SPR_f
"""
{
  - else ?
    {sequence
      - I've heard that the customers are really wanting things today in burgerlands!
      - why don't you go help them?
    }
  - {item "money"} == 20 ?
    {sequence
      - go left for the town!
      - I adore the flowers here
      - more of those in town too!
    }
}
"""

DLG ITM_3
what a beautiful horse! such a lush mane and shiny coat

DLG SPR_g
"""
{
  - else ?
    {sequence
      - what a beautiful horse! such a lush mane and shiny coat... wait, this horse looks familiar!
      - Blaze: hey I'm your horse! no taking me away
      - Blaze: i do love apples
      - Blaze: could i have some apples please? 3 should suffice
      - Blaze: you always used to feed me apples when you were small
      - Blaze: i think your parents miss you. they visit me sometimes and reminisce on the old days. people are always missing things
      - Blaze: how does everything get so lost?
      - Blaze: I miss my friends when they leave here
      - Blaze: I guess I miss you too
      - Blaze: ...
      - Blaze: apples...
    }
}
"""

DLG SPR_h
"""
{
  - else ?
    {sequence
      - looking at water is such a calming passtime
      - watching the sunlight glisten on the lake 
      - if i ignore all the land around me, i feel like i'm moving with the water
      - being pulled out with the ripples and waves
      - it's a nice day for a swim
    }
}
"""

DLG SPR_i
"""
{sequence
  - in this town they keep horses!
  - what a majestic animal
  - sometimes I look at horses and think.. "I wish I had your hair"
}
"""

DLG ITM_4
"""
ah. my old stereo.
I remember this stupid old thing...
I used to put on an album every night before i went to bed.
sometimes the same album...

for months!

I'll always take it with me...in my heart.
"""

DLG SPR_j
"""
{sequence
  - Mum: sweetie! you're home!
  - Mum: please go say hello to the cat! she should be walking around a town somewhere...
  - Mum: I've made your bed so you're welcome to stay for a bit if you like!
  - Mum: try not to make too much noise honey, your dad is sleeping
  - Mum: he may look awake but... that's just taking an undercover snooze...
  - Mum: and why don't you go visit Blaze too, honey?
  - Mum: it's always nice to catch up with things you love
  - Mum: don't forget to come back home every now and then
  - Mum: hmm.. what have I been up to? lots of housework! and meeting old friends over tea. the usual routine, honey
  - Mum: I moved here because I heard this town kept stunningly exquisite horses... I'm glad they still have some around!
}
"""

DLG SPR_k
"""
{sequence
  - Dad: I know your mother thinks I'm asleep, but i'm just catching up with this podcast I've been following...
  - Dad: it's called "how to become wonderful at the internet"
  - Dad: ...
  - Dad: well, it's like an electrically encoded book, so....
  - Dad: that's to do with the internet, right???
  - Dad: zzzz...
  - {clr2}(sounds like fake snoring...){clr2}
}
"""

DLG SPR_l
"""
{sequence
  - there are cats everywhere round here!
  - i wonder why there are so many coins lying on the ground...
  - it's like someone's giving you gifts!
  - i felt a sudden calm when i stepped through the door to this place
  - ...meow?
}
"""

VAR a
42


