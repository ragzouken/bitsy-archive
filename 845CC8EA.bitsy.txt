
Oh no! You got lost in forest!

# BITSY VERSION 4.6

! ROOM_FORMAT 1

PAL 0
NAME forest
235,152,65
58,102,19
55,26,13

PAL 1
NAME hole
28,28,28
0,0,0
84,84,84

PAL 2
NAME home
80,178,255
43,97,133
105,255,255

ROOM 0
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,e,b,b,a,b,b,a,a,a,a,0,b,a
a,a,a,a,a,b,a,b,i,d,0,0,a,a,b,a
a,0,d,0,a,b,a,a,a,a,a,0,a,0,0,a
a,0,a,0,a,0,a,0,e,0,0,d,0,e,0,a
a,d,a,b,a,0,0,0,a,0,a,a,a,a,d,a
a,0,a,b,b,b,a,d,a,b,b,a,0,0,0,a
a,0,a,0,a,b,a,0,a,b,0,a,0,a,a,a
a,0,a,a,a,a,a,e,a,a,a,a,b,a,i,a
a,d,0,a,e,0,0,0,a,0,0,b,b,a,d,a
a,a,a,a,a,a,0,a,a,a,0,a,a,a,0,a
a,i,b,b,e,0,d,a,b,a,b,0,0,a,0,a
a,a,a,a,a,a,a,a,0,b,b,a,d,a,0,a
a,b,b,b,b,d,0,b,0,a,a,a,b,c,e,a
a,0,b,a,a,a,c,a,a,a,0,b,b,a,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME forest maze
ITM 0 14,14
ITM 1 1,14
ITM 2 2,9
EXT 6,14 1 7,7
END 0 8,2
END 0 1,11
END 0 14,8
PAL 0

ROOM 1
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,0,f,f,f,i,f,b,b,d,0,b,e,b,f
f,f,e,f,f,b,b,f,b,f,f,i,g,f,0,f
f,f,0,f,f,b,f,f,e,f,f,f,g,f,0,f
f,0,e,b,b,e,0,d,0,f,f,f,g,f,0,f
f,b,f,f,f,f,f,f,b,f,f,f,0,f,f,f
f,e,f,f,0,b,f,b,e,b,f,f,0,f,f,f
f,b,f,f,f,0,f,f,f,b,f,f,d,f,f,f
f,b,f,f,f,b,d,b,e,0,f,b,b,b,i,f
f,0,e,b,f,f,f,f,f,f,f,b,f,f,f,f
f,f,f,b,f,f,f,f,f,f,f,0,f,f,f,f
f,f,f,b,d,b,0,e,b,b,e,0,d,0,e,f
f,f,g,0,f,f,f,f,0,f,f,f,f,e,f,f
f,f,f,0,f,f,f,f,i,f,f,f,f,0,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME hole
ITM 3 2,2
ITM 3 13,14
ITM 3 12,6
EXT 2,13 2 5,6
END 1 6,2
END 1 8,14
END 1 11,3
END 1 14,9
PAL 1

ROOM 2
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,n,n,n,n,f,n,n,n,n,n,n,n,f,f
f,f,0,0,0,0,f,0,0,0,0,0,0,0,f,f
f,f,0,p,o,0,f,0,l,0,0,0,0,0,f,f
f,f,0,0,0,0,f,0,o,0,0,0,0,0,f,f
f,f,0,0,0,i,f,0,0,0,0,j,k,0,f,f
f,f,0,0,0,0,f,0,m,0,0,0,0,0,f,f
f,f,n,0,0,0,f,n,n,0,0,0,n,n,f,f
f,f,f,f,q,f,f,f,f,f,q,f,f,f,f,f
f,f,n,0,0,0,n,n,n,0,0,0,n,n,f,f
f,f,0,0,0,0,0,0,0,0,0,0,0,0,f,f
f,f,0,m,0,m,0,0,0,m,0,m,0,0,f,f
f,f,n,n,n,n,n,n,n,n,n,n,n,n,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
NAME home
ITM 2 2,7
ITM 2 12,7
ITM 2 7,7
ITM 2 8,12
END 2 13,4
PAL 2

TIL a
00011000
00111100
01111110
00011000
00111100
01111110
11111111
00011000
NAME tree_a
WAL true

TIL b
00000000
00001000
00000000
01000100
00010000
00000010
00100000
00000000
NAME leaves
WAL false

TIL c
00011000
00111100
01111110
00011000
00111100
01111110
11111111
00011000
NAME fake tree

TIL d
00000000
00000000
00000000
00011000
00110100
01011010
00000000
00000000
NAME dirt pile

TIL e
00000000
00000000
00000000
00000000
00011000
01011100
00000000
00000000
NAME rock

TIL f
01001000
11111111
00100100
11111111
10001001
11111111
00100100
11111111
NAME wall
WAL true

TIL g
01001000
11111111
00100100
11111111
10001001
11111111
00100100
11111111
NAME fake wall
WAL false

TIL i
00000000
00111100
01111110
01111110
01111110
01111110
00111100
00000000
NAME hole

TIL j
00000000
01100000
01100000
01111110
01111110
01111110
01000010
00000000
NAME chair
WAL true

TIL k
01111110
01111110
01111110
01111110
01111110
01111110
01111110
01000010
NAME table
WAL true

TIL l
01000010
00100100
11111111
10000001
10111101
10111101
10000001
11111111
NAME tv
WAL true

TIL m
00000000
11000011
11111111
11111111
11111111
11111111
11111111
11111111
NAME sofa
WAL true

TIL n
00000000
11111111
10000001
10111101
10100101
10111101
10000001
11111111
NAME closet
WAL true

TIL o
00000000
00000000
01111110
01000010
01000010
01000010
01111110
00000000
NAME carpet

TIL p
01100000
01100000
01100000
01111110
01111110
00111100
00011000
00111100
NAME toilet
WAL true

TIL q
01111110
10000001
10000001
10110001
10100001
10000001
10000001
11111111
NAME door

SPR A
00111100
00011000
01111110
01111110
01111110
00111100
00100100
00100100
>
00000000
00111100
00011000
01111110
01111110
01111110
00111100
00100100
POS 0 1,1

SPR a
01100110
01111110
01101010
01111110
01111110
01111110
01111110
01000010
NAME bear l
DLG SPR_0
POS 0 3,7

SPR b
01100110
01111110
01010110
01111110
01111110
01111110
01111110
01000010
NAME bear 2
DLG SPR_1
POS 0 13,1

SPR c
01100110
01111110
01101010
01111110
01111110
01111110
01111110
01000010
NAME bear 3
DLG SPR_2
POS 0 10,14

SPR d
01100110
01111110
01101010
01111110
01111110
01111110
01111110
01000010
NAME tunnel bear 1
DLG SPR_3
POS 1 4,7

SPR e
01100110
01111110
01011010
01111110
01111110
01111110
01111110
01000010
NAME tunnel bear 2
DLG SPR_4
POS 1 14,5

SPR f
01100110
01111110
01010110
01111110
01111110
01111110
01111110
01000010
NAME tunnel bear 3
DLG SPR_5
POS 1 3,14

SPR g
01111110
10000001
10000001
10110001
10100001
10000001
10000001
11111111
NAME door
DLG SPR_6
POS 1 11,11

SPR h
01100110
01111110
01101010
01111110
01111110
01111110
01111110
01000010
NAME home bear
DLG SPR_7
POS 2 7,12

SPR i
00000000
10000000
11111110
10000001
10111101
10000001
11111111
01000010
NAME bed
DLG SPR_8
POS 2 12,4

ITM 0
00000000
00000000
00000000
00111100
01010010
01110010
00111100
00000000
NAME anti-bear pill
DLG ITM_0

ITM 1
00000000
00111100
01000010
11111111
10101011
11010101
01111110
00000000
NAME lost food basket
DLG ITM_1

ITM 2
00000000
00000000
01111000
01001110
01111010
01111100
01111000
00000000
NAME mug of beer
DLG ITM_2

ITM 3
00000000
00000000
00001100
00011010
00110010
00101100
00100000
00000000
NAME tunnel berry
DLG ITM_3

DLG ITM_0
You found an anti-bear pill.

DLG SPR_0
"""
{
  - {item "anti-bear pill"} > 0 ?
    Oh no! The anti-bear pill!
  - {item "lost food basket"} > 0 ?
    Oh! Gimme that basket!
    MMMMmmmm! Delicious!
    Hey, did you know there's a secret entrance to the secret meadow! It is near a pile of dirt near the third bear.
  - else ?
    Me is bear. Me like eat you.

}
"""

DLG SPR_1
"""
{
  - {item "anti-bear pill"} > 0 ?
    Oh no! The anti-bear pill!
  - {item "lost food basket"} > 0 ?
    Oh! Gimme that basket!
    MMMMmmmm! Delicious!
    Hey, did you know there's a secret entrance to the secret meadow! It is near a pile of dirt near the third bear.
  - else ?
    Me is another bear. Me like eat you too.
}
"""

DLG SPR_2
"""
{
  - {item "anti-bear pill"} > 0 ?
    Oh no! The anti-bear pill! Take whatever you want!
    Oh, wait...
    There's an {shk}exit{shk} outa here. It's under the {wvy}fourth{wvy} tree behind me!
  - {item "lost food basket"} > 0 ?
    What a strange basket? I don't want it.
  - else ?
    Me is third bear. Me like eat you {wvy}more{wvy} than other bears.
}
"""

DLG ITM_1
You found lost food basket!

DLG ITM_2
"""
Oh look! That's a mug of beer!
You feel much better now.
"""

DLG ITM_3
You found tunnel berry!

DLG SPR_3
Me is tunnel bear. Me not see anything!

DLG SPR_4
Me is tunnel bear. Me wanna say something so: {shk}ADAoaDoaA AIaii ioOAIA IOAOAiia ujsAsS AdadadaAADA Sdasda sdaA SDASDasD{shk}

DLG SPR_5
"""
{
  - {item "tunnel berry"} >= 3 ?
    Oh! You've collected berries for me! 
    {shk}NOM - NOM - NOM -  NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM - NOM!{shk}
    Hey! There's a hole on the left side from you! Jump in!
  - else ?
    Me is tunnel bear. Me wanna eat. Bring me three tunnel berries and I say you where is the exit.
}
"""

DLG SPR_6
"""
{cycle
  - I'm a {shk}bad{shk} door. I {shk}do not{shk} let anyone in{shk}!!!{shk}
  - OK, OK.. Pass the wall right upon that {wvy}annoying{wvy} berry{shk}!!!{shk}
}
"""

DLG SPR_7
"""
Me is home bear. That's weird, but I need to say:
{rbw}THANX FOR PLAYING BRO!!!{rbw}
"""

DLG SPR_8
It's your bed. You need to sleep after that kind of hard day.

END 0
You fell into wrong hole. That's not good.

END 1
You fell into the hole inside the other hole lol.

END 2
Finally you fall into your bed and sleep. 


