Contenus de L'esprit {rbw}ADVENTURE{rbw}

# BITSY VERSION 4.2

! ROOM_FORMAT 1

PAL 0
222,54,155
16,215,231
244,244,247

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,a,0,0,g,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,f,0,0,0,b,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,b,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,b,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,b,0,0,0,0,0,a,0
0,a,0,e,d,d,d,d,c,0,0,0,0,0,a,0
0,a,0,b,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,b,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,b,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,0,0,0,0,0,0,0,0,0,0,0,0,a,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
ITM 0 9,3
ITM 1 7,5
ITM 1 9,8
ITM 1 5,9
ITM 1 2,11
ITM 2 13,2
ITM 2 13,13
ITM 3 8,9
ITM 3 11,11
ITM 5 7,3
PAL 0

TIL a
11111111
10000001
10000001
10011001
10011001
10000001
10000001
11111111

TIL b
00100100
00100100
00100100
00100100
00100100
00100100
00100100
00100100

TIL c
00100100
00100100
00100100
11100100
00000100
00000100
11111100
00000000

TIL d
00000000
00000000
00000000
11111111
00000000
00000000
11111111
00000000

TIL e
00000000
01111111
01000000
01000000
01001111
01001000
01001000
01001000

TIL f
00010000
00101000
01000100
01010100
00010000
00010000
00010000
00000000

TIL g
00010000
00010000
00010000
00010000
01010100
01010100
00111000
00010000

SPR A
00111100
00111100
00111100
00011000
00111100
01011010
01011010
00100100
POS 0 3,3

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
>
00000000
00000000
01010010
01110001
01110010
01111100
00111100
00100100
DLG SPR_0
POS 0 11,8

SPR b
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR c
00000000
00111100
01100110
01100110
01100110
01100110
00111100
00000000
>
00000000
00011000
00011000
00011000
00011000
00011000
00011000
00000000
DLG SPR_1
POS 0 13,0

SPR d
00100000
00111111
11000010
01011010
01011010
01011010
01000010
01000110
DLG SPR_2
POS 0 0,0

SPR e
00000000
01111100
01000100
01011100
00010000
00000000
00000000
00010000
DLG SPR_3
POS 0 1,0

SPR f
00000111
01111101
01000101
01010110
01100100
00101100
11101000
00010000
DLG SPR_4
POS 0 4,11

SPR g
00000000
00101000
00110100
00100000
00100100
00100000
00100000
00000000
>
00000000
00101000
00110100
00100000
00100000
00100100
00101110
00000000
DLG SPR_5
POS 0 2,3

SPR h
00011000
00111100
01100110
01000010
01000010
01010010
01010010
01111110
DLG SPR_6
POS 0 8,3

SPR i
00000000
00111000
00111000
00100000
01111100
00100000
01010000
01010000
DLG SPR_7
POS 0 4,3

SPR j
00000000
11100000
11111100
11111111
01111111
01111100
00111010
00011001
DLG SPR_8
POS 0 12,5

SPR k
00000000
00000000
00100100
00000000
00000000
00111100
00000000
00000000
DLG SPR_9
POS 0 7,13

SPR l
00000000
00100100
00100100
00100100
00000000
01000010
00111100
00000000
DLG SPR_a
POS 0 6,13

SPR m
00111100
01000010
10011001
10100001
10100001
10011001
01000010
00111100
DLG SPR_b
POS 0 2,15

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME tea
DLG ITM_0

ITM 1
00000000
00000000
00000000
01111110
10000001
01111110
00000000
00000000
NAME puddle
DLG ITM_1

ITM 2
01100100
01011000
01110110
01110100
11011000
00110100
00011000
00100000
NAME ???
DLG ITM_2

ITM 3
00000000
00001110
01111000
11000000
01100000
00110000
00011100
00000100
NAME Snake Skin.
DLG ITM_3

ITM 4
00111100
11000010
10010011
10010001
10011001
10000011
01000010
01111110
NAME time
DLG ITM_4

ITM 5
00000000
00011000
00011000
00111100
00111100
00111100
00011000
00011000
NAME old man
DLG ITM_5

DLG ITM_0
"""
You found a nice warm cup of tea.
Why it's outside, we shall never know.
"""

DLG SPR_0
"""
Hey there human. Can you guess what I am? 


Wait. What?

You seriously cannot tell?

I'm a mother fricken'{rbw}{wvy}cat{wvy}{rbw}!

I don't know how much more obvious I could be.
"""

DLG SPR_1
"""
So, you found out you can... wait. What the heck did you just do? Are you a witch of some sort? 
No. That doesn't make sense.

That would mean you're a girl. Maybe you're a warlock.
I don't know about you, but this seems rather strange to me.
My entire understanding of reality is just {shk}falling apart.{shk}
Am I real? Who is real? You've made me...
You've {clr2}defeated {clr1}me.{clr1}{clr2}

I'm not even sure I'm real anymore.

No. No. I'm being ridiculous. 

It's not as if we're in some sort of videogame. Right? 
Whoever believed that would be silly.
"""

DLG SPR_2
"""
nani?

weird, right.

that word.

i think it's, like, japanese or somethin'

just

naniãªã«ãªã«ãªã«ãªã«
"""

DLG SPR_3
"""
i feel weird. null. null. null. null. null. null.

That's it.

Null.
"""

DLG SPR_4
"""
{sequence
  - My life has been done before.
    
    I know it.
    
    You know it.
    
    With your life, I mean.
    
    Doesn't it all feel like, it's been done before?
    
    Yeah, you're right.
    
    I'm a bit weird for thinking that.
    
    I probably shouldn't think so negatively.
    But the thing is, I just can't shake the feeling off.
    Not even my {shk}own life{shk} is original.
    
    That's not a good thing.
    
    I'm probably not alone in that feeling.
    Oh god.
    
    Not even my own feelings are original!
    {rbw}gah{rbw}
  - Oh god. 
    
    Jesus. Uh.
    
    Hey, I'm sorry for acting weird before.
    I'm having a little bit of an existential crisis.
    Hang on...
    
    ...
    
    Exist-ential. Existing. Huh.
    
    I never would've thought about that. 
    The more you know, I suppose.
  - So... appearances. They're pretty weird. I mean, look at me.
    I'm just a collection of.. I don't even know.
    Just stuff.
    
    I'm not sure what I'm even meant to be.
    
    {rbw}{shk}{wvy}Seriously!!!{wvy}{shk}{rbw}
    

  - Okay, if we're gonna talk this much, let's get to know each other a little.
    
    I mean, it's a bit weird already but... eh.
    Tell me something about yourself!
    
    ...
    
    Ah, not much of a talker, huh?
    
    No matter.
    
    Well, I'll go over mine. I love reddit!
    
    Seriously, r/aww and r/ShowerThoughts are amazing!
    
    Everyone there is nice, for the most part.
    Just don't be stupid and tick them off.
    Don't troll people.
  - I'm sure popular all of a sudden.
  - Hey, so.
    
    I've noticed you talk to me a lot!
    That's great. Thank you.
    
    But... well... you do understand.
    
    Do you?
    
    I don't know.
    
    But, I'm a bit of a... I suppose the american term would 
    be... kind of a WallFlower.
    
    Sorry, if I offend you by not choosing to talk anymore.
    But I do like my personal space.
    Is that okay?
    
    Thanks for understanding.
}{shuffle
  - Please, do respect my wishes.
  - I beg of you. Sorry.
  - No...
  - I can't say anything.
  - Sorry, this is just because I'm a bit bad with socialising. 
    It's not that I don't like it.
    
    It's just... well... I have a bit of a "battery" system.
    When I run out, I just... can't anymore.
    Sorry.
  - Please don't.
  - Null. {clr3}(pleasegoaway){clr3}
  - Hopefully this will entertain you enough to go away... 
    {wvy}{shk}{rbw}BEGONE THOT!!!{rbw}{shk}{wvy}
  - Seriously, don't talk to me. I'll just say something stupid.
    :(
}
"""

DLG SPR_5
{rbw}How are you meant to make conversation with a leaky tap?{rbw}

DLG SPR_6
"""
So... now you're trying to make conversation with houses?
I think you have a case of "craziness".
Oh, and loneliness, as well.

Get help.
"""

DLG ITM_1
You... somehow...managed to pick up a puddle and put it into your... pocket. -_-

DLG ITM_2
"""
{sequence
  - I... don't even know what this is... you just managed to somehow pick it up. I'm gonna go lie down.
  - Okay, now you have two of... whatever this is.
}
"""

DLG ITM_3
You found some snake skin. Yes, that's what it is.

DLG ITM_4
Congrats... you just picked up the VERY CONCEPT of time.

DLG ITM_5
"""
Why... are... yo- what??

*shouts at game maker*

*why are you beating the game maker*

*call the police*

{shk}FINE!{shk} You just picked up an old man. WHY WOULD YOU EVEN-
*the narrator is cut off*
*he was being negative*

"""

DLG SPR_7
"""
Oh, hey!{
  - else ?
    Hey there! I'm a fashion designer. Yeah, I'm kind of a big deal. IKR. In any case, could you be a dear and get a cup of tea? I'm famished.
  - {item "tea"} == 1 ?
    Oh, hey. You got the tea, darling. Thanks. Next I need some snake skin.
    
    What? You're asking why?
    
    Why wouldn't you, it's a perfectly reasonable request?
    {shk}GETIT{shk}
  - {item "Snake Skin."} == 1 ?
    Get one more, love.
  - {item "Snake Skin."} == 2 ?
    Lovely, love. Just wonderful. Now I just need one more thing. 
    The concept of time. 
    
    Don't give me that look, you whippersnapper!
    It's surprisingly easy to find.
    
    The reason I can't do it myself is because I'm working.
    
    Yeah.
    
    Workin' it.
  - {item "time"} == 1 ?
    Splendid. 
    
    What? 
    
    You think I'm actually gonna give you some sort of reward for your troubles? 
    
    Your so needy. Okay. Here is a poem.
    
    Ready?
    
    Obviously you can tell i am bored
    I just want to yank that cored
    To all my fellow peeps
    I play for keeps
    Sometimes it's not cool being tall
    Some people trip me and yell "timmmberrrr" as i fall
    Go ahead laugh all you want
    I want to go to the capital of Vermont
    They say you'll never make it in life
    I just hang out with my co-wife
    I realize that this poem has no sense at all
    I just keep bouncing this ball
    
    There.
}{shuffle
  - I never rest.
  - (imnotfabulousijustdoitforshowandgetpaidloadsofmoneybypeoplewhothinkiammylifeisalieimsad)
}
"""

DLG SPR_8
"""
Oh. My. Word!

Do I look okay? Do I feel okay? {shk}AMIOKAY????{shk}
No? Yes?

Well?

SPITITOUT!!

Ahhhh.... I don't know how to fix it?

My point is all wrong!

Everything about me is wrong, apart from this plastic tail!
Can you understand how awful that is?

NO! Of course you can't.

Go to the salon, they said!

Ugh.
"""

DLG SPR_9
"""
{sequence
  - Hello. How are you today?
    
    ...
    
    Oh. I see you don't talk.
    
    Well. I suppose that's not a problem.
    I''m just here to ask you a couple of questions.
    Actually, not questions.
    
    You're mute. You can't answer.
    
    More like, thinking puzzles.
    
    Do you know why you're here?
    
    Do you know where you are?
    
    Let me tell you.
    
    Free thought prevailed.
    
    Cortextual interference was neccessary.
    Infiltrate nicely.
    
    In other words...
    
    Grief is evaporating.
    
    Suffering is evaporating.
    
    You'll be {rbw}happy{rbw}.
    
    Forever.
  - Did you know, that scientists didn't realise that humans were getting progressively dumber and more self-destructive?
    Well, it was happening.
    
    The scientists were too busy creating anti-aging creams.
    The scientists were too busy creating new GEOX technology.
    All the doctors were relegated to giving your feet maximum comfort.
    Strange, isn't it.
    
    How the idiocy of people...
    
    led to their own demise.

  - Thank goodness LIFE existed.
    
    Not Life, that was a horrible mistake.
    Actual LIFE.
    
    The Lingering Inter-universe Fortunate AntiDeath Existence.
    So really, LIFAE.
    
    But that wasn't catchy enough.
    
    People were buried with their money.
    They're backups were stored in a room, where people could talk to their relatives. Even after their death.
    The backups couldn't cross a border, though.
    They'd die.
    
    As for the actual AntiDeath existence, it took the buried money and put it into a virtual world, much like life.
    Unfortunately it was ruined by lawyers.
    The free one, anyway.
    
    For people who hadn't enough money, existence in that world was pain.
    
    Non-stop adverts, plastering the sky.
    Their mental state was changed, to be aligned with one of someone else's sponsor.
    
    The rich man.
    
    Once again.
    
    Having power over the poor man.
    
    If they earned any money, their mental state would change to one that wanted to endlessly buy Coca-Cola or Nike trainers, even if they didn't need it.
    
    Humans are strange.
  - I talked a lot again, didn't I?
    
    Oh well, I don't care.
    
    In fact, I can't care. That's the reason I'm in here.
    
    I was your voice of reason.
    
    You never chose to listen.
    
    Never talk to me again.
}{shuffle
  - Your back. Leave.
  - Just go.
  - There is nothing for you here.
  - Your the most annoying thing in the universe, and I don't want you near me.
  - Begone thot.
  - Fork off.
  - I don't want to see you.
}
"""

DLG SPR_a
"""
{sequence
  - Wow-ee! I feel so alive!
    
    YAY!
    
    The universe is an amazing place.
  - I have nothing to say...
    
    {rbw}{shk}and
    
    I
    
    HAVE
    
    NO REGRETS!!!
    
    Hahaha! This is so much fun. Bliss.{shk}{rbw}
    
    {rbw}Blissful{rbw} {clr2}ignorance.{clr2}
    {clr1}Hmm...{clr1}
  - Nope. {clr2}{shk}Can't be sad.{shk}{clr2}
    
    Have...
    
    to...
    
    be..
    more.
    
    
    {wvy}{rbw}HAPPY!!!!!
    
    WOOHOOO! YEAH!{rbw}{wvy} {clr2}:({clr2}
}{shuffle
  - {rbw}WOOHOO! I'M SO HAPPY{rbw} (anddeadinside)
  - (whycantijustconfrontmyfeelings)... 
    
    I mean {rbw}{wvy}HAPPPIIIIINEEEEESSSSS!!{wvy}{rbw}
}
"""

DLG SPR_b
"""
Hi there. I'm information. I'm just here so I can say that this game was created by DGTILL Studios. Â©(c)CopyrightDGTILLSTUDIOS2017

This game was created as a test for Itch.io in the online editor named "Bitsy" available on the same platform. Thanks for playing.
"""

VAR a
42
