Somewhere in the forest...

# BITSY VERSION 4.1

! ROOM_FORMAT 1

PAL 0
0,0,0
0,255,0
255,255,255

ROOM 0
0,0,0,a,a,a,b,a,a,a,a,a,a,a,a,a
0,0,a,a,a,a,0,a,a,a,a,a,a,a,a,a
0,a,a,a,0,0,0,0,a,a,a,a,a,a,0,0
0,a,a,a,0,0,0,0,a,a,a,a,0,0,0,a
a,a,a,0,0,0,0,0,0,a,a,0,0,a,a,a
a,a,a,0,0,0,0,0,0,0,0,0,a,a,a,a
a,a,a,0,0,0,0,0,0,0,0,a,a,a,a,a
a,a,a,0,0,0,0,0,0,0,0,a,a,a,a,a
a,a,0,0,0,0,0,0,0,0,0,0,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,a,a,a,0
a,0,0,0,0,a,0,0,0,0,0,a,a,a,0,0
a,0,0,0,a,a,a,0,0,0,0,a,a,a,0,0
a,0,0,0,a,a,a,a,0,0,0,a,a,0,0,0
a,a,0,a,a,a,a,a,a,a,a,a,a,0,0,0
a,a,0,a,a,a,a,a,a,a,a,a,0,0,0,0
a,a,0,a,a,a,a,a,a,a,0,0,0,0,0,0
NAME aa
WAL a
EXT 2,15 3 2,0
EXT 15,2 1 0,2
EXT 6,0 9 6,12
PAL 0

ROOM 1
a,a,a,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,0,0,0,0,0,0,0,0,0,0,0
0,0,a,a,a,a,a,0,0,0,0,0,0,0,0,0
a,0,0,0,0,a,a,a,0,0,0,0,0,0,0,0
a,a,0,0,0,0,a,a,a,0,0,0,0,0,0,0
a,a,a,0,0,0,0,a,a,a,a,a,a,a,0,0
0,a,a,a,a,0,0,0,a,a,a,a,a,a,a,a
0,0,a,a,a,a,a,0,0,0,0,0,0,a,a,a
0,0,a,a,a,a,a,a,a,a,a,0,0,0,a,a
0,0,0,a,a,a,a,a,a,a,a,0,0,0,a,a
0,a,a,a,a,a,a,a,a,a,0,0,0,0,a,a
0,a,a,a,a,0,0,0,0,0,0,a,a,0,0,a
a,a,a,0,0,0,0,0,0,0,a,a,a,a,0,0
a,a,a,0,0,0,0,0,0,a,a,a,a,a,a,a
a,a,0,0,a,a,a,a,a,a,a,a,a,a,a,a
a,a,0,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME ab
WAL a
EXT 0,2 0 15,2
EXT 2,15 4 2,0
EXT 15,12 2 0,12
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,a,a,a,a,a,0,0
0,0,0,0,0,0,0,a,a,a,a,a,a,a,a,0
0,0,0,0,0,0,0,a,a,a,0,0,a,a,a,a
0,0,0,0,a,a,a,a,a,0,0,0,0,0,a,a
0,0,a,a,a,a,a,a,0,0,0,0,0,0,a,a
0,0,a,a,a,a,a,0,0,0,0,0,0,0,a,a
0,a,a,a,a,a,0,0,0,0,0,0,0,0,a,a
0,a,a,a,a,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,b,0,0,0,0,0,0,0,0,0,0,a
a,a,a,b,b,0,0,0,0,0,0,0,0,0,0,a
0,0,0,b,b,b,a,a,a,a,0,0,0,a,a,a
a,a,b,b,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,a,a,a,a,a,0,0,0,0,a,a,a,a,a,a
NAME ac
WAL a
EXT 0,12 1 15,12
PAL 0

ROOM 3
a,a,0,a,a,a,a,a,a,a,a,a,a,a,0,0
a,a,0,a,a,a,a,a,a,a,a,a,a,a,a,0
a,a,0,0,a,a,a,a,a,a,a,a,a,a,a,0
a,a,0,0,0,0,0,0,0,0,a,a,a,a,a,0
a,a,a,a,0,0,0,0,0,0,0,a,a,a,a,0
a,a,a,a,a,0,0,0,0,0,0,0,a,a,a,a
0,0,0,a,a,0,0,0,0,0,0,0,a,a,a,a
0,0,a,a,a,a,0,0,0,0,0,0,a,a,a,a
0,0,a,a,a,a,0,0,0,0,0,0,a,a,a,a
0,0,a,a,a,a,0,0,0,0,0,0,0,a,a,a
0,0,a,a,a,a,0,0,0,0,0,0,0,0,a,a
0,0,a,a,a,a,0,0,0,0,0,0,0,0,0,0
0,0,a,a,a,0,0,0,0,0,0,0,0,a,a,a
0,a,a,a,0,0,0,0,0,0,a,a,a,a,a,a
a,a,a,a,a,0,0,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,0,a,a,a,a,a,a,a,a,0
NAME ba
WAL a
EXT 2,0 0 2,15
EXT 6,15 6 6,0
EXT 15,11 4 0,11
PAL 0

ROOM 4
a,a,0,a,a,a,a,a,a,0,0,0,a,a,a,a
a,a,0,0,0,a,a,a,a,a,a,a,a,a,a,a
a,a,a,a,0,0,a,a,a,a,a,a,a,a,a,a
0,a,a,a,a,0,0,0,a,a,a,a,a,a,a,a
0,0,a,a,a,a,0,0,0,0,a,a,a,a,a,a
0,0,a,a,a,a,a,0,0,0,0,a,a,a,0,0
0,0,a,a,a,a,a,0,0,0,0,a,a,a,0,a
0,a,a,a,a,a,0,0,0,0,0,0,0,0,0,a
a,a,a,a,0,0,0,0,0,0,0,0,0,0,a,a
a,a,0,0,0,0,0,0,0,0,0,0,0,0,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,a,a,a
a,a,a,0,0,0,0,0,0,0,0,0,a,a,a,0
a,a,a,a,a,a,0,0,0,0,0,a,a,a,a,0
a,a,a,a,a,a,a,0,0,0,a,a,a,a,a,0
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,0
NAME bb
WAL a
EXT 0,11 3 15,11
EXT 2,0 1 2,15
EXT 15,5 5 0,5
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,a,a,a,0,0,a,a,a
a,a,a,a,a,0,0,a,a,a,a,0,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,a,a
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,a
0,0,0,0,a,a,a,a,a,a,a,0,0,0,0,a
a,a,a,0,0,0,0,0,a,a,0,0,0,0,0,a
a,a,a,a,a,a,a,0,0,0,0,0,0,0,0,a
0,a,a,a,a,a,a,a,a,a,0,0,0,0,0,a
0,0,0,a,a,a,a,a,a,a,a,0,0,0,a,a
0,a,a,a,a,a,a,a,a,a,0,0,0,a,a,a
0,a,a,a,a,0,0,0,0,0,0,0,a,a,a,a
a,a,a,a,0,0,0,0,0,0,a,a,a,a,a,0
a,a,a,0,0,0,a,a,a,a,a,a,a,a,0,0
a,a,0,0,0,a,a,a,a,a,a,a,0,0,0,0
a,a,0,a,a,a,a,a,a,a,0,0,0,0,0,0
NAME bc
WAL a
EXT 0,5 4 15,5
EXT 2,15 8 2,0
PAL 0

ROOM 6
a,a,a,a,a,a,0,a,a,a,a,a,a,a,a,a
a,a,a,a,a,a,0,0,a,a,a,a,a,a,a,a
a,a,a,a,a,a,0,0,a,a,a,a,a,a,a,a
0,a,a,a,a,a,0,0,a,a,a,a,a,a,a,a
0,0,a,a,a,0,0,0,0,a,a,a,a,a,0,0
0,0,a,a,a,0,0,0,0,a,a,a,a,0,0,a
0,0,a,a,a,a,0,0,0,0,0,0,0,0,0,a
0,0,0,a,a,a,0,0,0,0,0,0,0,0,0,a
0,0,0,a,a,0,0,0,0,0,0,0,0,0,0,a
0,0,a,a,a,0,0,0,0,0,0,0,0,a,a,a
0,a,a,a,0,0,0,0,0,0,0,0,a,a,a,a
0,a,a,a,0,0,0,0,0,0,0,0,a,a,a,a
0,a,a,a,a,0,0,0,0,0,0,a,a,a,a,a
0,0,a,a,a,a,0,0,0,0,a,a,a,a,a,a
0,0,a,a,a,a,a,a,a,a,a,a,a,0,a,a
0,0,0,0,0,0,a,a,a,a,a,a,0,0,0,a
NAME ca
WAL a
EXT 6,0 3 6,15
EXT 15,4 7 0,4
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,a,a,a,a,a,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,a,a,0,0,0,a,a,a,0,0,0,0,0
0,0,0,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,0,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,a,a,0,0,0,a,a,a,a,a,0,0,0,0
a,a,a,0,0,0,0,0,a,a,a,a,0,0,0,0
a,a,a,0,0,0,0,0,0,a,a,a,a,a,a,0
a,a,0,0,0,0,0,0,0,0,a,a,a,a,a,a
a,a,0,0,0,0,0,0,0,0,0,a,a,a,a,a
a,a,a,a,0,0,0,0,0,0,0,0,a,a,a,a
0,a,a,a,a,a,a,a,a,0,0,0,0,0,0,0
0,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,a,a,a,a,a,a,a,a,a,a,a,a,0
0,0,0,0,a,a,a,a,0,0,0,0,0,0,0,0
NAME cb
WAL a
ITM 0 3,9
EXT 0,4 6 15,4
EXT 15,12 8 0,12
PAL 0

ROOM 8
a,a,0,a,a,0,0,0,0,0,0,0,0,0,0,0
a,a,0,a,a,a,0,0,0,a,a,a,a,a,a,a
a,a,0,0,a,a,a,a,a,a,a,a,a,a,a,a
a,a,a,0,0,a,a,a,a,a,a,0,0,0,a,a
a,a,a,0,0,0,a,a,a,0,0,0,0,0,a,a
a,a,a,a,a,0,0,0,0,0,0,0,0,0,a,a
0,a,a,a,a,a,a,0,0,0,0,0,0,0,a,a
0,0,a,a,a,a,a,a,0,0,0,0,0,0,a,a
a,a,a,a,a,a,0,0,0,0,0,0,0,a,a,a
a,a,a,a,0,0,0,0,0,0,0,0,a,a,a,0
a,a,0,0,0,0,0,0,0,0,a,a,a,a,0,0
a,0,0,0,0,0,a,a,a,a,a,a,a,0,0,0
0,0,0,a,a,a,a,a,a,a,a,a,0,0,0,0
a,a,a,a,a,a,a,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME cc
WAL a
EXT 0,12 7 15,12
EXT 2,0 5 2,15
PAL 0

ROOM 9
0,0,a,a,a,a,a,a,0,0,0,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,a,a,a,a,a,a,a,a,0,0,0,0,0
a,a,a,a,a,0,0,a,a,a,a,0,0,0,0,0
a,a,a,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,0,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,0,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,0,0,0,0,0,0,a,a,a,0,0,0,0,0
a,a,0,0,0,0,0,0,a,a,a,a,0,0,0,0
a,a,a,0,0,0,0,0,a,a,a,a,0,0,0,0
a,a,a,0,0,0,0,0,a,a,a,a,0,0,0,0
a,a,a,a,a,0,0,a,a,a,a,a,a,0,0,0
0,a,a,a,a,a,0,a,a,a,a,a,a,a,0,0
0,0,a,a,a,a,0,a,a,a,a,a,a,a,a,0
0,0,0,a,a,a,0,a,a,a,a,a,a,a,a,a
0,0,0,a,a,a,0,a,a,a,a,a,a,a,a,a
NAME bonnie
WAL a
END undefined 6,15
PAL 0

TIL a
00000000
01111100
01011110
01111010
01111110
00101110
00111000
00000000
NAME undefined

TIL b
00000000
01111100
01011110
01111010
01111110
00101110
00111000
00000000
NAME undefined

SPR A
00000000
00000000
00001010
00001010
01101110
01111110
00111100
00100100
POS 4 6,10

SPR a
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_0
POS 4 9,11

SPR b
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_2
POS 5 12,3

SPR c
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_4
POS 8 12,4

SPR d
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_3
POS 6 7,11

SPR e
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_5
POS 9 5,5

SPR f
00000000
00000000
01010000
01010000
01110110
01111110
00111100
00100100
DLG SPR_1
POS 2 11,5

ITM 0
00000000
00000000
00000000
00010000
00101000
00010000
00000000
00000000
>
00000000
00000000
00000000
00000000
00010000
00000000
00000000
00000000
NAME strawberry
DLG ITM_0

DLG SPR_0
Hey Clover, have you seen Bonnie today? We talked this morning and she seemed kind of sad.. it's getting dark now and I haven't seen her since. Could you help me look around for her, if you're not too terribly busy? I'm not sure if there's anything we can say to make her feel better.. but maybe her just knowing that we care is worth something. Some of the other bunnies might have seen Bonnie too.. we should ask around and find out!

DLG ITM_0
Hey, a strawberry! It looks so good, I'm tempted to eat it right now.. I'm not in the mood to get my mouth all messy with strawberry juice though, so I'll just save it for later.

DLG SPR_1
"""
{shk}I'm so hungry..{shk} Clover, do you think you could find me a snack? I feel like such a butt for asking but I'm too tired to move.. {shk}ugh...{shk}{
  - {item "0"} == 1 ?
    {shk}So hungry..{shk} Wait, Clover... Is that a strawberry? Did you bring me a {wvy}strawberry{wvy}? Oh, you didn't.. But I can have it? I can really have it? Thank you so much Clover, you're such a good friend! If there's anything I can do for you in return please let me know! Oh, have I seen Bonnie today? I haven't actually, I was playing tag with Rosie in the central clearing this morning, and then I came here to take a nap and I've been here since then. Maybe you could go ask Rosie? She said she was going to go look for dandelions after we finished playing tag, which means she's probably to the east of the where the strawberries grow.
}
"""

DLG SPR_2
Oh, hey Clover, what's up? Oh, you're looking for Bonnie? I saw her earlier today and asked her if she wanted to play in the dirt with me and she said no, and I've just been over here playing in the dirt since then. Do you wanna stay and play in the dirt with me for a little bit? Oh, right, I guess you're already busy looking for Bonnie, huh? I know you two like to keep clean mostly, but maybe when you find her we can all play in the dirt together, and then go play in the stream to clean up!

DLG SPR_3
Oh, hi there Clover! What's up, are you looking for dandelions too? Oh okay, you're looking for Bonnie. I did see her just a little while ago when I started looking for dandelions, that was farther up north. I didn't say hi or anything, she seemed kind of sad so I thought it best to leave her alone for now. I hope she's okay, maybe I should have asked if she wanted to look for dandelions with me.. I'm sure I'll be looking for a while longer, you're both welcome to join me when you find her! If you both feel up to it and all. I can't decide whether I want to wear my dandelion behind my ear or eat it... I hope I find two so I can do both!

DLG SPR_4
"""
{shuffle
  - {shk}snooooooooooork...{shk} mimimimimi...
  - hmmmmbbrhrgh... grmgrgmfmd...
  - It looks like Izzy is fast asleep. Sweet dreams, Izzy~
}
"""

DLG SPR_5
Oh, Clover! Hi.. I'm sorry for hiding from everyone today... Am I okay? Well.. No? I mean, I'm not *not* okay, I'm just... I've just been thinking. Sometimes when I see my reflection in the stream, or when I'm talking with someone and I hear my own voice, I notice how big my whiskers are, and how squeaky my voice is, and how crooked my teeth are and I.. I realize that I don't really like the way that I am. I feel so wrong whenever I become aware of myself like that, it makes me want to cry... Have you felt that way before Clover? Am I wrong to feel like this? I don't know what to do.. I came here today to hide... I'm glad you came to find me. It helps to know that you care, and I know the others care too. How do you ask for help when you don't know how to help yourself, how do you ask for help when you don't know how to accept it? I want to be happy and fulfilled and affirmed in my own body but I don't have any idea how to achieve that. Do you think that's something I can achieve at all, Clover? I... I'm sorry for dumping all of this on you. I feel better just being able to put words to my feelings, even though I don't think the words are all quite right. It's getting dark.. Will you cuddle with me tonight, Clover?

END 0


END undefined
The End

END undefinee
