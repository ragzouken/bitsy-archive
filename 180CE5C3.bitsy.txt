hungry

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
NAME your room
204,197,193
77,1,3
89,21,14

PAL 1
NAME kitchen
250,254,171
112,103,33
191,183,42

PAL 2
NAME living room
255,239,222
255,119,51
255,153,115

PAL 3
NAME outside
160,189,161
15,107,26
35,87,35

PAL 4
NAME sister's room
254,201,244
255,44,227
122,24,102

ROOM 2
a,a,a,a,a,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,a,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,d,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,a,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Your Room
EXT 4,2 3 6,2
PAL 0

ROOM 3
0,0,0,0,0,a,a,a,a,a,a,a,a,0,0,0
0,0,0,0,0,a,0,0,0,0,0,0,a,0,0,0
0,0,0,0,0,d,0,0,0,0,0,0,d,0,0,0
0,0,0,0,0,a,0,0,0,0,0,0,a,0,0,0
0,0,0,0,0,a,a,a,a,a,a,a,a,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Kitchen
ITM 2 11,1
ITM 0 6,1
EXT 5,2 2 3,2
EXT 12,2 4 14,2
PAL 1

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,a,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,d,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,a,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,a,0,a
0,0,0,0,a,a,a,a,a,a,a,a,a,a,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,d,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,a,a,d,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME living room
EXT 13,2 3 11,2
EXT 6,9 5 6,11
EXT 4,7 6 2,7
PAL 2

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,a,a,d,a,a,a,a,a,a,a,a,a
f,0,b,0,0,0,0,c,0,0,0,0,0,0,0,f
f,0,0,0,0,0,0,0,0,0,0,0,0,b,0,f
f,0,0,0,0,c,0,0,0,0,0,c,0,0,0,f
f,0,0,0,0,0,0,0,0,0,0,0,0,0,0,f
f,f,f,f,f,f,f,f,g,f,f,f,f,f,f,f
NAME outside
ITM 1 14,14
ITM 3 2,11
EXT 6,10 4 6,8
PAL 3

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,a,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,d,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,a,0,0,0,0,0,0,0,0,0,0,0,0
a,a,a,a,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME sister's room
EXT 3,7 4 5,7
PAL 4

TIL a
11111111
10000001
10000001
10000001
10000001
10000001
10000001
11111111
NAME wall
WAL true

TIL b
00000000
01010000
00100000
01010000
00000000
00001010
00000100
00001010
NAME grass

TIL c
00000000
01000010
10100101
01000010
00001000
00010100
00001000
00000000
NAME flowers

TIL d
11111111
11000001
10100001
10010001
10001001
10000101
10000011
11111111
NAME door
WAL false

TIL e
00000000
00000000
00000000
11111111
10000001
10000001
10000001
10000001
NAME table
WAL true

TIL f
10000001
01000010
00100100
00011000
00011000
00100100
01000010
10000001
NAME fence
WAL true

TIL g
00000000
00000000
11111111
00000000
11111111
00000000
00000000
00000000
NAME gate
WAL false

SPR A
00011000
00111100
00011000
00111100
01111110
10111101
00100100
00100100
>
00000000
00011000
00111100
00011000
01111110
10111101
00111100
00100100
POS 2 1,1

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
>
00000000
00000000
01010000
01110001
01110001
01111110
00111100
00100100
NAME cat
DLG SPR_0
POS 4 14,8

SPR b
01010000
01010000
11110010
11110001
00111111
00111111
00111111
00101001
>
01010000
01010000
11110001
00110001
11111111
00111111
00111111
00101001
NAME dog
DLG SPR_b
POS 5 2,13

SPR c
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01000010
NAME your shelf
DLG SPR_3
POS 2 3,1

SPR d
00000000
00000000
00000000
00011000
00011000
00111100
00011000
00100100
>
00000000
00000000
00000000
00011000
00011000
00111100
00011000
00100100
NAME kid
DLG SPR_c
POS 6 1,8

SPR e
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01000010
NAME 1 k shelf
DLG SPR_5
POS 3 7,1

SPR f
01111110
01000010
01111110
01000010
01111110
01000010
01111110
01000010
NAME 2 k shelf
DLG SPR_6
POS 3 10,1

SPR g
00000000
00000000
00000000
00000000
00000000
00000000
00100100
00111100
NAME cat food bowl
DLG SPR_2
POS 3 8,1

SPR h
00000000
00000000
00000000
00000000
00000000
00000000
00100100
00111100
NAME dog food bowl
DLG SPR_1
POS 3 9,1

SPR i
00000000
00000000
00000000
00000000
10000000
11111110
11111110
10000010
NAME bed
DLG SPR_4
POS 2 1,3

SPR j
00000000
00000000
00000000
00000000
00000000
01111110
01000010
01000010
NAME table
DLG SPR_7
POS 3 9,3

SPR k
00000000
00000000
00000000
00111100
00111100
11111111
11111111
01111110
NAME L chair 1
DLG SPR_8
POS 4 8,6

SPR l
00000000
00100100
00011000
01111110
01111110
01000010
01111110
00111100
>
00000000
00100100
00011000
01111110
01000010
01111110
01000010
00111100
NAME tele
DLG SPR_a
POS 4 9,8

SPR m
00000000
00000000
00000000
00111100
00111100
11111111
11111111
01111110
NAME L chair 2
DLG SPR_9
POS 4 10,6

SPR n
00000000
00000000
00000000
00000000
00000001
01111111
01111111
01000001
NAME bed 2
DLG SPR_e
POS 6 2,6

SPR o
00000000
00000000
00000000
00000000
01111110
01000010
01000010
01000010
NAME desk
DLG SPR_d
POS 6 1,6

SPR p
00000000
00000000
00000000
11111111
00000000
11111111
00000000
00000000
NAME gstr
DLG SPR_f
POS 5 8,15

ITM 0
00000000
00000000
00000000
00111100
01100100
00100100
00011000
00000000
NAME hot choco
DLG ITM_0

ITM 1
00000000
00011000
00111100
01000010
01000010
01000010
00111100
00000000
NAME pumpkin
DLG ITM_3

ITM 2
00000000
00000000
00100100
01011010
01000010
00111100
00000000
00000000
NAME bean
DLG ITM_1

ITM 3
00000000
00100000
01010000
00100100
00001010
00100100
01010000
00100000
NAME flowers
DLG ITM_2

DLG ITM_0
nice hot chocolate made. you call your sister out to share with you before she goes back to her room.

DLG SPR_0
"""
{shuffle
  - {wvy}Meow.{wvy}
  - {wvy}Miaou.{wvy}
  - {wvy}Miau.{wvy}
  - {wvy}Mjá.{wvy}
}
"""

DLG ITM_1
bean found. beany.

DLG ITM_2
nice smelling flowers found.

DLG ITM_3
good. pumpkin found. perfect for pies.

DLG SPR_1
"""
{sequence
  - it's the dog's food bowl.
  - you can't remember the dog's name.
}
"""

DLG SPR_2
"""
{sequence
  - it's the cat's food bowl.
  - the animals are probably just as hungry as you.
}
"""

DLG SPR_3
"""
{shuffle
  - you grab a random book from your bookshelf. "cooking for idiots"
  - you grab a random book from your bookshelf. "how to be cool"
  - you grab a random book from your bookshelf. "dating 101"
  - you grab a random book from your bookshelf. "generic horror novel"
  - you grab a random book from your bookshelf. "cheesy romance novel"
  - you grab a random book from your bookshelf. "fancy wine guide for people who aren't fancy"
}
"""

DLG SPR_4
it's your bed! comfortable, you guess. 

DLG SPR_5
"""
{cycle
  - the shelves are barren.
  - no food here.
}
"""

DLG SPR_6
"""
{sequence
  - empty. 
  - just like the other one.
  - actually, now that you look in the back there's a can of baked beans.
  - you hate baked beans. 
}
"""

DLG SPR_8
your little sister's chair.

DLG SPR_9
your chair.

DLG SPR_a
"""
{shuffle
  - channel #1 is the local news. it's a farmer and they're talking about how this year the food just won't grow.
  - channel #3 is a kid's channel. something with puppets teaching things how to count. you used to watch this or something.
  - channel #8 is the sports channel. you don't find it very interesting.
  - channel #12 is something about some neat new thingy to make eggs look cool when you cook them.
  - channel #20 is talking about how some types of tigers are endangered.
  - channel #37 is a news channel. they're talking about a nation-wide food crisis. 
  - channel #45 is one of those economic channels. it's talking about how the price of food has skyrocketed. 
  - channel #56 are the cartoons. you'd sit down and watch these, but you feel like you'd forget what you're doing.
  - channel #69 is... {shk}let's change the channel.{shk}
  - channel #99 is one of those channels that try to get people to buy things from their homes.
  - channel #172 is talking about some true crimes and how they happened. you think these are kind of neat, but they're not something you'd get super into.
  - {rbw}channel #420{rbw} is talking about drugs, maaaan. like dude, they are talking about all kinds of those things. brooo.
  - channel {shk}{wvy}#EGG{wvy}{shk} is teaching people how to cook a nice egg.
  - channel #550 is showing a british chef yelling at people and being kind of entertaining.
}
"""

DLG SPR_b
"""
{shuffle
  - {shk}Bark!{shk}
  - {shk}Ouaf!{shk}
  - {shk}Wan wan!{shk}
  - {shk}Bjork!{shk}
}
"""

DLG SPR_c
"""
{
  - {item "flowers"} == 1 ?
    ooo pretty flowers!! they're so nice...
  - else ?
    when's dinner?? i hope it's soon.
  - {item "pumpkin"} == 1 ?
    woah!! we can eat that!!
}
"""

DLG SPR_d
"""
{cycle
  - it's your sister's  desk and it has her diary on it. 
  - "dear daiiry. i really miss mama. and big brohter says that we're preeyt much all out of food. i hope we get something soon."
  - "dear diary. i played with the dog outside in the front yard today and it was very fun! thtere's something maybe growing there but i don't know what."
  - "dear dairryr. cole forgot what the animals were named and i thought that was very silly ! ! the dog's name is barley and the cat's name is annie."
}
"""

DLG SPR_e
your sister's bed. her blanket has a checkered pink pattern on it. 

DLG SPR_7
"""
{
  - {item "pumpkin"} == 1 ?
    with the pumpkin you can make food! finally. maybe you should go out and celebrate this.
  - else ?
    empty table. there's probably not going to be anything on the table for a bit.
}
"""

DLG SPR_f
you know there's not a way to leave the house. if you left, people might steal whatever you and your sister have left. the thought terrifies you.

END 0
funky. you've left. bye !! 

END undefined


END undefinee


VAR a
42

