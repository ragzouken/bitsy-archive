
Sinking Ship

# BITSY VERSION 4.8

! ROOM_FORMAT 1

PAL 0
NAME Normal
0,0,0
128,159,255
255,255,255

PAL 1
NAME Water
1,3,105
128,159,255
255,255,255

PAL 2
NAME Acid
1,69,8
128,159,255
255,255,255

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,f,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,b,b,b,b,a,a,a,b,b,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,b,b,b,a,a,a,b,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,c,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME StartA
EXT 10,4 2 1,8
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,a,h,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,b,b,b,b,a,a,a,b,b,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,a,a,a,a,a,a,b,0,0,0,0,0,0
0,0,b,b,b,b,a,a,a,b,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,f,0,0,0,0,0
0,0,0,0,0,b,a,a,a,a,b,0,0,0,0,0
0,0,0,0,0,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME StartB
EXT 10,12 4 1,10
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,b,b,b,b,b,b,0,0,0,0,0,0,0,0
0,b,a,a,a,a,a,b,b,b,b,b,b,b,b,b
0,b,a,a,a,a,a,b,a,a,a,a,a,a,a,b
h,a,a,a,a,a,a,b,a,a,a,b,a,d,a,b
0,b,a,a,a,a,a,a,a,a,a,b,a,a,a,b
0,b,a,a,a,a,a,b,b,b,b,b,b,b,b,b
0,b,b,b,b,b,b,b,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button1A
EXT 13,8 3 13,8
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,b,b,b,b,b,b,0,0,0,0,0,0,0,0
0,b,a,a,a,a,a,b,b,b,b,b,b,b,b,b
0,b,a,a,a,a,a,b,a,a,a,a,a,a,a,b
0,g,a,a,a,a,a,b,a,a,a,b,a,e,a,b
0,b,a,a,a,a,a,a,a,a,a,b,a,a,a,b
0,b,a,a,a,a,a,b,b,b,b,b,b,b,b,b
0,b,b,b,b,b,b,b,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button1B
EXT 1,8 1 10,4
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,b,b,b,b,b,b,b,b,b,b,b,b,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,b,b,b,b,a,a,a,b,0
h,a,a,a,a,a,a,c,a,a,b,a,d,a,b,0
0,b,a,a,a,a,a,b,b,a,b,i,i,i,b,0
0,b,b,b,b,b,b,b,a,a,b,a,a,a,b,0
0,0,0,0,0,0,0,b,a,b,b,a,a,a,a,h
0,0,0,0,0,0,0,b,a,a,a,a,a,a,b,0
0,0,0,0,0,0,0,b,b,b,b,b,b,b,b,0
NAME Button2A
EXT 12,10 5 12,10
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,b,b,b,b,b,b,b,b,b,b,b,b,b,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,a,a,a,a,a,a,a,b,0
0,b,a,a,a,a,a,b,b,b,b,a,a,a,b,0
h,a,a,a,a,a,a,a,a,a,b,a,e,a,b,0
0,b,a,a,a,a,a,b,b,a,b,i,i,i,b,0
0,b,b,b,b,b,b,b,a,a,b,a,a,a,b,0
0,0,0,0,0,0,0,b,a,b,b,a,a,a,f,0
0,0,0,0,0,0,0,b,a,a,a,a,a,a,b,0
0,0,0,0,0,0,0,b,b,b,b,b,b,b,b,0
NAME Button2B
EXT 14,13 6 2,8
PAL 0

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,h,a,a,a,d,a,a,c,a,a,e,a,a,c,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button3A
EXT 5,8 7 5,8
PAL 0

ROOM 7
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,c,a,a,e,a,a,a,a,a,d,a,a,c,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button3B
EXT 11,8 8 11,8
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,b,b,b,b,b,b,b,b,b,k,b,b,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,c,a,a,e,a,a,a,a,a,e,a,a,c,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button3C
EXT 11,5 b 7,13
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,h,0,0,0,0
0,0,b,b,b,b,b,b,b,b,b,a,b,b,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,c,a,a,e,a,a,c,a,a,e,a,a,f,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button3D
EXT 14,8 d 3,9
PAL 1

ROOM a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,b,b,b,b,b,b,b,b,b,c,b,b,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,c,a,a,e,a,a,c,a,a,e,a,a,a,h
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,a,a,a,a,a,b,a,a,a,a,a,b,0
0,0,b,b,b,b,b,b,b,b,b,l,b,b,b,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button3E
EXT 11,11 g 6,1
PAL 2

ROOM b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,b,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,b,a,a,d,b,a,b,0,0,0,0,0
0,0,0,0,b,a,b,c,b,c,b,0,0,0,0,0
0,0,0,0,b,a,a,a,a,a,b,0,0,0,0,0
0,0,0,0,b,c,b,c,b,a,b,0,0,0,0,0
0,0,0,0,b,a,a,a,a,a,b,0,0,0,0,0
0,0,0,0,b,a,b,c,b,c,b,0,0,0,0,0
0,0,0,0,b,a,a,a,a,a,b,0,0,0,0,0
0,0,0,0,b,c,b,c,b,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,a,a,b,0,0,0,0,0
0,0,0,0,b,b,b,a,b,b,b,0,0,0,0,0
0,0,0,0,0,0,0,h,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button4A
EXT 7,4 c 7,4
PAL 0

ROOM c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,b,b,b,b,b,b,b,0,0,0,0,0
0,0,0,0,b,a,c,d,b,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,b,a,b,0,0,0,0,0
0,0,0,0,b,a,c,a,c,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,b,a,b,0,0,0,0,0
0,0,0,0,b,a,c,a,c,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,b,a,b,0,0,0,0,0
0,0,0,0,b,a,c,a,c,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,b,a,b,0,0,0,0,0
0,0,0,0,b,a,b,a,c,a,b,0,0,0,0,0
0,0,0,0,b,b,b,l,b,b,b,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button4B
EXT 7,13 9 11,5
PAL 1

ROOM d
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,m,m,m,q,m,m,m,q,m,m,m,v
0,0,0,b,a,a,a,p,a,a,a,r,a,a,a,p
0,0,h,a,a,0,a,n,a,0,a,a,a,d,a,p
0,0,0,b,a,a,a,p,a,a,a,s,a,a,a,p
0,0,0,b,m,m,m,t,m,m,m,t,m,m,m,u
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button5A
EXT 13,9 e 13,9
PAL 1

ROOM e
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,m,m,m,q,m,m,m,q,m,m,m,v
0,0,0,b,a,a,a,p,a,a,a,r,a,a,a,p
0,0,0,g,a,0,a,o,a,0,a,a,a,e,a,p
0,0,0,b,a,a,a,p,a,a,a,s,a,a,a,p
0,0,0,b,m,m,m,t,m,m,m,t,m,m,m,u
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button5B
EXT 9,9 f 9,9
EXT 3,9 a 14,8
PAL 2

ROOM f
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,m,m,m,m,m,m,m,v,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,p,0,0,0,0
0,0,0,b,0,w,0,0,0,w,0,p,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,p,0,0,0,0
0,0,0,b,m,m,m,m,m,m,m,u,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,b,0,0,0,0,0,0,0,0,0,0,0,0
NAME Button5C
EXT 5,9 e 5,9
PAL 2

ROOM g
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,b,a,b,b,b,b,b,b,b,b,b
0,0,0,0,p,a,a,a,p,0,0,0,0,0,0,0
0,0,0,0,p,a,a,a,p,0,0,0,0,0,0,0
0,0,0,0,p,a,a,a,p,0,0,0,0,0,0,0
0,0,0,0,x,m,m,m,u,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
NAME End
END 0 6,0
PAL 2

TIL a
11111111
10111101
11000011
11000011
11000011
11000011
10111101
11111111
NAME Floor
WAL false

TIL b
10111101
00100100
11111111
10100101
10100101
11111111
00100100
10111101
NAME Wall
WAL true

TIL c
11111111
10000001
10111101
10100101
10100101
10111101
10000001
11111111
NAME Block
WAL true

TIL d
00000000
01111110
01111110
01111110
01111110
01111110
01111110
00000000
NAME ButtonA

TIL e
00000000
00111100
01011010
01100110
01100110
01011010
00111100
00000000
NAME ButtonB

TIL f
00000000
00010000
00011000
00011100
00011100
00011000
00010000
00000000
NAME Right

TIL g
00000000
00001000
00011000
00111000
00111000
00011000
00001000
00000000
NAME Left

TIL h
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Barrier
WAL true

TIL i
01010101
10101010
01010101
10101010
01010101
10101010
01010101
10101010
NAME Glass
WAL true

TIL k
00000000
00000000
00011000
00111100
01111110
00000000
00000000
00000000
NAME Up

TIL l
00000000
00000000
00000000
01111110
00111100
00011000
00000000
00000000
NAME Down

TIL m
00000000
00000000
00111100
11111111
11111111
00111100
00000000
00000000
NAME FenceA
WAL true

TIL n
00011000
00010000
00000000
00000000
00000000
00000000
00001000
00011000
NAME FenceBA
WAL false

TIL o
00011000
00010000
00000000
00000000
00000000
00000000
00001000
00011000
NAME FenceBB
WAL true

TIL p
00011000
00011000
00111100
00111100
00111100
00111100
00011000
00011000
NAME FenceC
WAL true

TIL q
00000000
00000000
00111100
11111111
11111111
00111100
00011000
00011000
NAME FenceD
WAL true

TIL r
00011000
00011000
00111100
00111100
00111100
00111100
00000000
00000000
NAME FenceE
WAL true

TIL s
00000000
00000000
00111100
00111100
00111100
00111100
00011000
00011000
NAME FenceF
WAL true

TIL t
00011000
00011000
00111100
11111111
11111111
00111100
00000000
00000000
NAME FenceG
WAL true

TIL u
00011000
00011000
00111100
11111100
11111100
00111100
00000000
00000000
NAME FenceH
WAL true

TIL v
00000000
00000000
00111100
11111100
11111100
00111100
00011000
00011000
NAME FenceI
WAL true

TIL w
00000000
00111100
01111110
01111110
01111110
01111110
00111100
00000000
NAME Light

TIL x
00011000
00011000
00111100
00111111
00111111
00111100
00000000
00000000
NAME FenceJ
WAL true

SPR A
01111110
10000001
10010101
10000001
10001001
10010101
10000001
01111110
POS 0 4,8

SPR a
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Lol
DLG SPR_0
POS 0 11,14

ITM 0
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME Lol
DLG ITM_0

DLG ITM_0
How did you find this?

DLG SPR_0
How did you find me?

END 0
Play again?

VAR a
42


