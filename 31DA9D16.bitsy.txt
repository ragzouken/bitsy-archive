The Spirits of Bluebirch Forest by Nic Chin (use the directional keys to move)

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
NAME start
175,189,100
232,110,53
249,255,165

PAL 1
NAME farm_day
196,100,26
255,211,60
198,255,199

PAL 2
NAME farm_evening
153,78,21
255,171,37
198,255,199

PAL 3
NAME farm_end
76,105,47
214,119,37
234,225,98

PAL 4
NAME farm_morning
212,127,63
255,211,60
198,255,199

PAL 5
NAME forest_01
51,79,38
133,87,33
234,225,98

PAL 6
NAME forest_02
38,81,48
82,48,15
156,150,65

PAL 7
NAME forest_end
9,28,13
49,107,93
54,74,37

ROOM 0
v,f,u,v,w,x,u,v,f,d,b,b,b,b,b,b
x,f,w,x,u,v,w,x,0,e,0,f,f,f,0,0
v,u,v,f,w,x,f,0,f,e,f,0,0,f,0,0
x,w,x,u,v,0,0,f,0,e,0,f,0,0,0,0
v,u,v,w,x,f,0,0,0,e,0,0,0,0,0,0
x,w,x,f,0,0,f,0,0,e,0,0,0,0,0,0
f,f,0,0,0,f,0,0,g,e,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
f,0,f,0,0,0,0,f,0,e,0,0,0,0,0,0
f,0,u,v,0,f,0,u,v,e,0,0,0,0,0,0
v,0,w,x,f,0,f,w,x,e,0,f,0,0,0,0
x,f,0,u,v,f,u,v,f,e,0,0,0,f,0,0
v,u,v,w,x,0,w,x,f,e,0,0,f,f,f,0
x,w,x,f,u,v,f,0,f,e,f,f,f,0,0,f
v,f,u,v,w,x,u,v,f,d,b,b,b,b,b,b
NAME farm_start
EXT 15,7 1 0,7
EXT 15,8 1 0,8
PAL 0

ROOM 1
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
e,0,0,f,0,f,0,f,f,0,0,f,0,0,f,e
e,f,f,0,0,f,0,0,0,0,0,0,f,f,f,e
e,0,0,f,0,0,0,k,l,n,s,0,0,f,0,e
e,0,f,0,0,0,i,j,m,m,o,p,q,r,0,e
e,0,0,0,0,0,0,h,h,h,h,h,h,0,0,e
e,0,0,0,0,0,0,h,h,0,h,h,h,0,0,e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
e,a,a,a,a,a,a,a,a,a,a,a,0,0,0,e
e,0,a,a,a,a,a,a,a,a,a,a,a,0,0,e
e,0,0,a,a,a,a,a,a,a,a,a,a,a,0,e
e,0,f,0,a,a,a,a,a,a,a,a,a,a,a,e
e,f,0,0,0,f,f,0,f,0,0,0,f,f,0,e
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME farmhouse
EXT 15,7 2 0,7
EXT 15,8 2 0,8
PAL 4

ROOM 2
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
e,0,0,c,c,c,0,c,c,c,c,c,c,c,c,e
e,c,a,a,a,a,c,c,c,c,a,a,a,a,c,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,c,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME farm_01
ITM 0 4,4
ITM 1 11,3
ITM 2 4,11
ITM 3 11,11
EXT 15,7 3 0,7
EXT 15,8 3 0,8
PAL 1

ROOM 3
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
e,0,0,c,c,c,0,c,c,c,c,c,c,c,c,e
e,c,a,a,a,a,c,c,c,c,a,a,a,a,c,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,c,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,0,0,a,0,0,0,0,a,0,0,a,0,e
e,0,a,a,a,a,0,0,0,0,a,a,a,a,0,e
e,0,0,0,0,0,0,0,0,0,0,0,0,0,0,e
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME farm_02
ITM 4 3,4
ITM 5 11,3
ITM 6 4,11
ITM 7 11,11
EXT 15,7 4 0,7
EXT 15,8 4 0,8
PAL 2

ROOM 4
b,b,b,b,b,t,u,v,0,u,v,w,x,f,0,u
0,0,f,0,0,e,w,x,f,w,x,f,0,u,v,w
0,f,0,f,0,e,f,f,0,0,f,0,0,w,x,f
f,0,0,0,0,e,0,0,u,v,0,f,0,f,f,0
0,0,0,0,0,e,f,0,w,x,f,0,0,f,0,0
0,0,0,0,0,e,0,f,0,0,f,0,f,0,0,0
0,0,0,0,0,e,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,e,0,0,0,0,0,0,0,0,0,f
0,0,0,0,0,e,0,0,0,f,0,0,f,0,0,0
0,0,0,0,0,e,0,f,0,u,v,0,0,f,0,u
0,0,0,0,f,e,0,0,0,w,x,f,0,0,0,w
f,f,0,f,0,e,f,0,f,0,0,u,v,0,f,0
f,f,f,0,0,e,u,v,0,0,f,w,x,f,u,v
b,b,b,b,b,t,w,x,u,v,u,v,u,v,w,x
NAME farm_end
EXT 15,7 5 0,7
EXT 15,8 5 0,8
PAL 3

ROOM 5
v,0,w,x,u,v,w,x,f,0,w,x,f,u,v,w
x,u,v,0,w,x,u,v,0,u,v,u,v,w,x,f
0,w,x,f,f,0,w,x,f,w,x,w,x,f,f,f
f,0,f,0,0,f,f,0,u,v,0,0,f,0,0,f
0,f,u,v,f,0,0,f,w,x,0,f,0,0,0,0
f,0,w,x,0,0,f,0,0,0,f,0,0,0,f,0
0,0,f,0,0,0,0,0,0,0,0,0,f,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
f,0,0,0,0,0,0,0,0,0,0,0,0,0,0,f
0,u,v,0,f,0,0,0,0,0,0,0,f,0,0,0
v,w,x,f,u,v,f,0,0,0,f,0,0,u,v,0
x,0,0,f,w,x,0,u,v,f,0,f,0,w,x,f
f,f,f,0,0,0,f,w,x,0,f,u,v,0,f,f
u,v,f,u,v,f,0,f,0,u,v,w,x,f,0,u
w,x,f,w,x,0,u,v,f,w,x,u,v,0,f,w
NAME path_01
EXT 15,7 6 0,7
EXT 15,8 6 0,8
PAL 5

ROOM 6
x,w,x,w,x,u,v,0,0,0,u,v,w,x,u,v
0,u,v,f,0,w,x,0,0,0,w,x,u,v,w,x
f,w,x,f,f,0,0,0,0,u,v,0,w,x,0,u
u,v,u,v,u,v,0,0,0,w,x,u,v,f,0,w
w,x,w,x,w,x,0,0,0,0,0,w,x,0,f,u
f,f,0,0,f,0,0,0,0,0,f,0,f,f,0,w
0,0,f,0,0,0,0,0,0,0,0,f,0,u,v,u
0,0,0,0,0,0,0,0,0,0,0,0,0,w,x,w
0,0,0,0,0,0,0,0,0,0,0,f,f,0,u,v
0,0,0,0,0,0,0,0,0,0,f,0,0,f,w,x
0,f,0,0,0,0,0,u,v,f,0,u,v,f,0,u
f,0,u,v,f,0,f,w,x,f,0,w,x,u,v,w
0,f,w,x,0,f,0,0,f,0,0,0,0,w,x,0
f,f,0,u,v,f,f,0,0,f,f,u,v,f,u,v
v,f,f,w,x,f,0,f,u,v,f,w,x,f,w,x
x,f,u,v,f,u,v,f,w,x,f,f,u,v,u,v
NAME path_02
EXT 7,0 7 7,15
EXT 8,0 7 8,15
PAL 6

ROOM 7
v,f,u,v,f,w,x,0,w,x,f,f,f,0,u,v
x,f,w,x,f,0,0,f,0,0,f,0,f,0,w,x
v,0,0,f,0,f,y,z,10,11,0,u,v,f,0,0
x,u,v,0,13,12,1g,14,1h,1f,15,16,x,u,v,f
0,w,x,0,17,1h,14,1h,1i,14,14,18,0,w,x,u
f,0,0,0,19,1g,1f,14,1g,1f,14,1a,0,0,0,w
0,f,u,v,1b,1h,1g,1h,14,1g,14,1c,0,0,0,u
u,v,w,x,1d,1e,1f,1g,1h,1i,1j,1k,v,0,f,w
w,x,0,u,v,1l,1m,1n,1o,1p,1q,w,x,f,0,f
f,f,0,w,x,0,1r,1s,1t,1u,0,0,0,u,v,0
u,v,f,0,0,0,0,1v,0,0,0,0,0,w,x,f
w,x,u,v,0,0,0,0,0,0,u,v,0,f,0,0
0,f,w,x,f,f,0,0,0,0,w,x,u,v,0,u
u,v,0,f,u,v,f,0,0,0,f,0,w,x,f,w
w,x,f,0,w,x,0,0,0,f,0,0,u,v,0,0
v,u,v,u,v,f,0,0,0,0,f,0,w,x,f,f
NAME forst_end
END undefined 7,10
PAL 7

TIL 10
00000000
10000100
11001110
11111111
11111111
11111110
01010111
11101111
NAME bigtree_03

TIL 11
00000000
00000000
11000000
11110000
11111100
11011110
01111110
11101111
NAME bigtree_04

TIL 12
00000001
00000111
00011111
00111110
01111111
11011101
11101111
11111111
NAME bigtree_06

TIL 13
00000000
00000000
00000000
00000000
00000000
00000000
00000001
00000111
NAME bigtree_05

TIL 14
11111111
11011111
11101111
11111111
11111111
11111101
11011011
11111111
NAME bigtree_07

TIL 15
11000000
11100000
11111000
11111100
11111100
01110110
11111111
11011111
NAME bigtree_08

TIL 16
01101101
01111011
00111111
00011111
00000111
00000011
10000011
11000111
NAME bigtree_09

TIL 17
00000111
00001111
00001101
00001110
00010111
00011111
00011111
00011011
NAME bigtree_10

TIL 18
11000000
11000000
10110000
11010000
11111000
01111000
10111000
11110000
NAME bigtree_11

TIL 19
00111111
01111101
01111111
11111111
11101101
11110111
01111111
01111111
NAME bigtree_12

TIL a
10111011
01101110
11011001
01101110
10011011
01111110
11010111
01101100
NAME soil
WAL false

TIL b
00011000
00111100
00111100
11111111
11111111
11111111
00111100
00111100
NAME fence_hori
WAL true

TIL c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME ground

TIL d
00011000
00111100
00111100
00111111
00111111
00111111
00111100
00111100
NAME fence_end_TL
WAL true

TIL e
00011000
00111100
00111100
00111100
00111100
00111100
00111100
00011000
NAME fence_vert
WAL true

TIL f
10000100
01001001
00101010
00101100
10111101
11111111
01111110
00111100
NAME grass

TIL g
11111111
11111111
11111111
11111111
00011000
00011000
00011000
00011000
NAME signpost
WAL true

TIL h
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME house_body
WAL true

TIL i
00000000
00000000
00000000
00000000
00000000
00000000
00000001
00000011
NAME roof_01
WAL true

TIL j
00000111
00001111
00011111
00111111
01111111
11111111
11111111
11111111
NAME roof_02
WAL true

TIL k
00000000
00000000
00000000
00000000
00000000
00000000
00000001
00000011
NAME roof_03
WAL true

TIL l
00000000
00000000
00000000
00000000
00011100
01111111
11111111
11111111
NAME roof_04
WAL true

TIL m
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME roof_05
WAL true

TIL n
00000000
00000000
00000000
00000000
00000000
11100000
11111100
11111111
NAME roof_06
WAL true

TIL o
11111000
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME roof_07
WAL true

TIL p
00000000
00000000
11100000
11111110
11111111
11111111
11111111
11111111
NAME roof_08
WAL true

TIL q
00000000
00000000
00000000
00000000
11000000
11111000
11111111
11111111
NAME roof_09
WAL true

TIL r
00000000
00000000
00000000
00000000
00000000
00000000
10000000
11000000
NAME roof_10
WAL true

TIL s
00000000
00000000
00000000
00000000
00000000
00000000
00000000
10000000
NAME roof_11
WAL true

TIL t
00011000
00111100
00111100
11111100
11111100
11111100
00111100
00111100
NAME fence_end_TR

TIL u
00000000
00000011
00001111
00011111
00111111
00110111
00111011
01011111
NAME tree_TL
WAL true

TIL v
00000000
11000000
11110000
11011000
10101100
11111100
11111010
11111110
NAME tree_TR
WAL true

TIL w
01101101
01111011
00111111
00011111
00000111
00000011
00000011
00000111
NAME tree_BL
WAL true

TIL x
11111110
11101110
11111100
10111000
11100000
11000000
11000000
11100000
NAME tree_BR
WAL true

TIL y
00000000
00000000
00000000
00000111
00001111
00111111
01111110
11111011
NAME bigtree_01

TIL z
00000000
00000011
01101111
11111111
11111101
11111111
11101111
01111111
NAME bigtree_02

TIL 1a
11111000
01111000
10111000
11111100
11101110
11011110
11111011
11111111
NAME bigtree_13

TIL 1b
11111111
11111111
11011010
01111101
11111110
11111111
01101111
00111111
NAME bigtree_14

TIL 1c
11111111
11111010
10111111
11011111
01111111
11111111
10101110
11111100
NAME bigtree_15

TIL 1d
00111111
00111101
00110111
00011101
00011111
00010111
00000011
00000000
NAME bigtree_16

TIL 1e
11111111
01111111
10111101
11111110
11110101
11111011
11111111
10101111
NAME bigtree_17

TIL 1f
11111111
11111101
11011111
11101111
11111111
11111011
11111111
11111111
NAME bigtree_18

TIL 1g
11111111
11111110
11111101
10111111
11111111
01111011
11111111
11111111
NAME bigtree_19

TIL 1h
11111111
11110111
11101111
10111101
11111011
11111101
11110111
11111111
NAME bigtree_20

TIL 1i
11111111
11111101
11111111
10101111
11111011
01111101
11111111
11111111
NAME bigtree_21

TIL 1j
11111110
10111111
11011101
10111010
11111111
11101111
10111110
11111100
NAME bigtree_22

TIL 1k
11111000
11110011
11101111
11111111
10111111
00110111
00111011
01011111
NAME bigtree_23

TIL 1l
01111101
00011111
00000011
00000000
00000000
00000000
00000000
00000000
NAME bigtree_24

TIL 1m
11111101
11101111
11111011
00011111
00000011
00000111
00011111
00111111
NAME bigtree_25

TIL 1n
11110111
11101111
11111110
11111111
11111011
01101011
10110111
01101111
NAME bigtree_26

TIL 1o
11101011
11110111
11111111
01111011
01110111
11111011
10111011
11011101
NAME bigtree_27

TIL 1p
11101111
10110111
01111110
10111100
11111000
11111000
10111100
11011110
NAME bigtree_28

TIL 1q
11111000
11000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME bigtree_29

TIL 1r
00001110
00011100
00111110
11111101
00011011
00111111
01111111
01111001
NAME bigtree_30

TIL 1s
11101111
11011110
11101101
11101110
11011101
10111101
11101111
11000111
NAME bigtree_31

TIL 1t
11011111
11101011
11101101
11010111
10110111
11111011
11111111
10001110
NAME bigtree_32

TIL 1u
01110000
01111000
10111100
10110110
11011000
11101100
11111110
11111111
NAME bigtree_33

TIL 1v
00000000
01011010
00100100
01011010
01011010
00100100
01011010
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME end

SPR 10
00011000
00111100
00111100
00111101
10011011
11111111
01111110
00111100
NAME chard_02
POS 3 11,4

SPR 11
00011000
00111100
00111100
00111101
10011011
11111111
01111110
00111100
NAME chard_03
POS 3 12,4

SPR 12
00111100
01111110
11111111
11111111
01111110
00111100
00011000
00011000
NAME broccoli_01
POS 3 3,11

SPR 13
00111100
01111110
11111111
11111111
01111110
00111100
00011000
00011000
NAME broccoli_02
POS 3 3,12

SPR 14
00111100
01111110
11111111
11111111
01111110
00111100
00011000
00011000
NAME broccoli_03
POS 3 4,12

SPR 15
10001100
11011110
11001101
01101010
00111100
01111110
01111110
00111100
NAME kohlrabi_01
POS 3 12,11

SPR 16
10001100
11011110
11001101
01101010
00111100
01111110
01111110
00111100
NAME kohlrabi_02
POS 3 11,12

SPR 17
10001100
11011110
11001101
01101010
00111100
01111110
01111110
00111100
NAME kohlrabi_03
POS 3 12,12

SPR 18
00010000
01100000
11100000
00100000
01111110
00111101
00111100
00100100
>
00000000
00010000
01100000
11100001
00111110
00111100
00111100
00100100
NAME doggo
DLG SPR_8
POS 3 9,7

SPR 19
00011000
00011000
00011000
01111110
01111110
01111110
00111100
00100100
>
00011000
00011000
00011001
01111111
01111111
01111100
00111100
00100100
NAME farm_dad_02
DLG SPR_a
POS 4 2,6

SPR A
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
>
10000001
01000010
00011000
01111110
01011010
01111110
00100100
00000000
POS 0 0,7

SPR a
00011000
00001100
00000110
11111111
11111111
00000110
00001100
00011000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME arrow
DLG SPR_0
POS 0 7,6

SPR b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME door
POS 1 9,6

SPR c
00000000
00011000
00111100
00111100
01011010
00111100
00111100
00011000
>
00000000
00011000
00111100
00111100
01011010
00111110
00111000
00011000
NAME farm_girl_01
DLG SPR_2
POS 1 4,6

SPR d
00000000
00111100
01110110
01011110
01101010
01110110
00111100
00000000
NAME cabbage_01
POS 2 3,3

SPR e
00000000
00111100
01110110
01011110
01101010
01110110
00111100
00000000
NAME cabbage_02
POS 2 4,3

SPR f
00000000
00111100
01110110
01011110
01101010
01110110
00111100
00000000
NAME cabbage_03
POS 2 3,4

SPR g
00000000
00000100
00001000
00111100
00111100
00111100
00011000
00000000
NAME apple_01
POS 2 12,3

SPR h
00000000
00000100
00001000
00111100
00111100
00111100
00011000
00000000
NAME apple_02
POS 2 11,4

SPR i
00000000
00000100
00001000
00111100
00111100
00111100
00011000
00000000
NAME apple_03
POS 2 12,4

SPR j
01000010
00101100
01011010
00111100
00111100
00011000
00011000
00011000
NAME carrot_01
POS 2 3,11

SPR k
01000010
00101100
01011010
00111100
00111100
00011000
00011000
00011000
NAME carrot_2
POS 2 3,12

SPR l
01000010
00101100
01011010
00111100
00111100
00011000
00011000
00011000
NAME carrot_03
POS 2 4,12

SPR m
00001100
00011000
01111110
11010101
11010101
11010101
11010101
01111110
NAME pumpkin_01
POS 2 12,11

SPR n
00001100
00011000
01111110
11010101
11010101
11010101
11010101
01111110
NAME pumpkin_02
POS 2 11,12

SPR o
00001100
00011000
01111110
11010101
11010101
11010101
11010101
01111110
NAME pumpkin_03
POS 2 12,12

SPR p
00000011
00000111
00000111
00001110
00001110
00001111
00001110
00001010
>
00000011
00000111
00000111
00001110
00011110
00001111
00001110
00001010
NAME visitor_01
DLG SPR_6
POS 2 8,11

SPR q
00000000
00000000
00000000
01100000
01100000
11110000
01100000
01100000
>
00000000
00000000
00000000
01100000
01110000
11100000
01100000
01100000
NAME visitor_02
DLG SPR_4
POS 2 9,11

SPR r
00011000
00011000
00011000
01111110
01111110
01111110
00111100
00100100
>
00011000
00011000
00011001
01111111
01111111
01111100
00111100
00100100
NAME farm_dad
DLG SPR_3
POS 2 3,7

SPR s
00011000
00111100
00111100
00011000
00111100
00111100
00111100
00011000
>
00011000
00111100
00111100
00011001
00111110
00111000
00111000
00011000
NAME farm_mum
DLG SPR_5
POS 2 2,7

SPR t
00000000
00011000
00111100
00111100
01011010
00111110
00111000
00011000
>
00000000
00011000
00111100
00111100
01011010
00111100
00111100
00011000
NAME farm_girl_02
DLG SPR_1
POS 2 4,7

SPR w
11001011
00100100
00011000
01111110
01111110
01111110
00111100
00011000
NAME turnip_01
POS 3 3,3

SPR x
11001011
00100100
00011000
01111110
01111110
01111110
00111100
00011000
NAME turnip_02
POS 3 4,3

SPR y
11001011
00100100
00011000
01111110
01111110
01111110
00111100
00011000
NAME turnip_03
POS 3 4,4

SPR z
00011000
00111100
00111100
00111101
10011011
11111111
01111110
00111100
NAME chard_01
POS 3 12,3

SPR 1a
00111000
01111000
00111000
00111100
00111100
00111100
00111000
00101000
>
00000000
00111000
01111000
00111000
00111100
00111100
00111100
00101000
NAME visitor
DLG SPR_7
POS 3 6,11

SPR 1b
00011000
00111100
00111100
00011001
00111110
00111000
00111000
00011000
>
00011000
00111100
00111100
00011000
00111100
00111100
00111100
00011000
NAME farm_mum_02
DLG SPR_9
POS 4 3,6

SPR 1c
00000000
00011000
00111100
00111100
01011010
00111100
00111100
00011000
>
00000000
00011000
00111100
00111100
01011010
00111110
00111000
00011000
NAME farm_girl_03
DLG SPR_b
POS 4 4,6

SPR 1d
10000001
01000010
00011000
01111110
01011010
01111110
00100100
00000000
>
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
NAME spirit_01
DLG SPR_d
POS 7 5,10

SPR 1e
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
>
00000000
01000010
00100100
00011000
01111110
01011010
01111110
00000000
NAME spirit_02
DLG SPR_e
POS 7 10,10

SPR 1f
10000001
01000010
00011000
01111110
01011010
01111110
00100100
00000000
>
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
NAME spirit_03
DLG SPR_f
POS 7 11,9

SPR 1g
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
>
00000000
01000010
00100100
00011000
01111110
01011010
01111110
00000000
NAME spirit_04
DLG SPR_g
POS 7 4,11

SPR 1h
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
>
00000000
01000010
00100100
00011000
01111110
01011010
01111110
00000000
NAME spirit_06
DLG SPR_h
POS 7 2,5

SPR 1i
10000001
01000010
00011000
01111110
01011010
01111110
00100100
00000000
>
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
NAME spirit_05
DLG SPR_i
POS 7 3,4

SPR 1j
10000001
01000010
00011000
01111110
01011010
01111110
00100100
00000000
>
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
NAME spirit_07
DLG SPR_j
POS 7 13,6

SPR 1k
01000010
00100100
00011000
01111110
01011010
01111110
00100100
00000000
>
00000000
01000010
00100100
00011000
01111110
01011010
01111110
00000000
NAME spirit_08
DLG SPR_k
POS 7 12,5

SPR 1m
00000000
01011010
00100100
01011010
01011010
00100100
01011010
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME info
DLG SPR_c
POS 0 2,7

ITM 0
00000000
00111100
01110110
01011110
01101010
01110110
00111100
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME cabbage
DLG ITM_0

ITM 1
00000000
00000100
00001000
00111100
00111100
00111100
00011000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME apple
DLG ITM_b

ITM 2
01000010
00101100
01011010
00111100
00111100
00011000
00011000
00011000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME carrot
DLG ITM_c

ITM 3
00001100
00011000
01111110
11010101
11010101
11010101
11010101
01111110
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME pumpkin
DLG ITM_d

ITM 4
11001011
00100100
00011000
01111110
01111110
01111110
00111100
00011000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME turnip
DLG ITM_1

ITM 5
00011000
00111100
00111100
00111101
10011011
11011111
01111110
00111100
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME chard
DLG ITM_e

ITM 6
00111100
01111110
11111111
11111111
01111110
00111100
00011000
00011000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME broccoli
DLG ITM_2

ITM 7
10001100
11011110
11001101
01101010
00111100
01111110
01111110
00111100
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME kohlrabi
DLG ITM_3

DLG ITM_0
"""
CABBAGE. "Cabbage sweetens up nicely when fried, and is favourite amongst the locals of Bluebirch, even those who hide in the surrounding forest. Sometimes strips of half-eaten cabbage can be seen hanging from high branches, like clothes on a laundry line." (Excerpt taken from 'Bluebirch Farms: A Detailed Introduction' by Steve and Stella McLaughlin)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG SPR_0
The McLaughlin Farm. The autumn harvest is here! Fresh, organic produce home-grown with care. Low prices!

DLG SPR_2
"Hi, I'm Sam, welcome to- aaahh! A forest spirit! Uhm, uhm...the harvest is just over the fence, right this way!"

DLG SPR_4
"But Grandpa, these ones are huuuuuuuge! Can I have one more, pleeeeaaseee??"

DLG SPR_3
"Hello, forest spirit. It's good to see our farm blessed by your kind again."

DLG SPR_5
"""
"Oh, a forest spirit! You gave my daughter a bit of a fright back there. But no worries, it's wonderful to see you. Thank you for visiting our farm, and please, take what you want." 

"""

DLG ITM_1
"""
TURNIP. "One winter spell, I found myself lost in the woods of Bluebirch during a camping holiday. I was starving, and would likely have perished if not for a strange phenomenon - in the distance, I saw three small, unidentifiable creatures floating about a clearing. When I reached the clearing, the creatures immediately vanished! But it was there that I found a forgotten crop of winter turnips. Whatever it was I saw, I am eternally grateful." (Excerpt from 'My Oddball Life" by Jamie Jamison) 
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops! 
"""

DLG ITM_2
"""
BROCCOLI. "My husband spent his childhood in Bluebirch, and claims he saw them a few times hiding amongst their broccoli. broccoli's related to cabbages so that's why they like them, apparently. iirc he said that they're pretty small, have round green bodies with two little legs, and a pair of long leaves (feelers?) on top of their heads. We've visited his family there more than 5 times now, but I've never seen one of these forest spirits before - maybe you can't if you didn't grow up there?" (Excerpt taken from a reply by tonysquared on reddit.com/r/Cryptozoology)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops! 
"""

DLG ITM_3
"""
KOHLRABI. "Ah, kohlrabi. This vegetable is our personal favourite, and we love using the stems for apple-kohlrabi slaw, or roasting the flesh with beef and potatoes. In fact, it was in a patch of kohlrabi that I first saw a forest spirit as a young girl - I almost mistook the spirit for an identical vegetable!" (Excerpt taken from 'Bluebirch Farms: A Detailed Introduction' by Steve and Stella McLaughlin)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_4
Chard.

DLG ITM_5
Pumpkin.

DLG ITM_6
Carrot.

DLG ITM_7
Apple.

DLG SPR_8
"Bork bork!"

DLG ITM_8
"""
APPLE. "It takes about 8 apples to produce a litre of cider. If the air in Bluebirch is giving you a buzz, chances are you've stumbled past an autumnal spirit celebration! Why not join in?" (Excerpt taken from '10 Best Ways to Get Drunk in Bluebirch' by hangrydrunk on fancyfarmfolklore.com)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_9
"""
CARROT. "The first documented carrots were purple or white, until a genetic mutation created the shade of orange seen on most carrots today...there have been four reported sightings so far of cobalt carrots at the farms surrounding Bluebirch Forest. It is unknown if this is the cause of a similar mutation, or the work of other factors." (Excerpt taken from 'A study of unusual developments in British crops' by Dr Tan Wai Feng et al)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_a
"""
PUMPKIN. "The first documented carrots were purple or white, until a genetic mutation created the shade of orange seen on most carrots today...there have been four reported sightings so far of cobalt carrots at the farms surrounding Bluebirch Forest. It is unknown if this is the cause of a similar mutation, or the work of other factors." (Excerpt taken from 'A study of unusual developments in British crops' by Dr Tan Wai Feng et al)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_b
"""
APPLE. "It takes about 8 apples to produce a litre of cider. If you find the air in Bluebirch giving you a buzz, chances are you've stumbled into what the locals call an 'autumnal revelry', said to be the celebrations of forest spirits! Why not join in?" (Excerpt taken from '10 Best Ways to Get Drunk' by hangryanddesperate on fancyfarmfolklore.com)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_c
"""
CARROT. "The first documented carrots were purple or white, until a genetic mutation caused purple carrots to lose their colour, bringing out their orange cores [...] there have so far been four reported sightings of cobalt carrots found at farms surrounding Bluebirch Forest. It is unknown if this is caused by a similar mutation, or the work of other factors." (Excerpt taken from 'A study of recent unusual developments in British crops' by Dr Tan Wai Feng et al)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_d
"""
PUMPKIN. "Halloween is a well-loved holiday in Bluebirch. Most folk celebrate in the usual fashion, carving pumpkin faces and trick-or-treating in costume. However, there are also a few farmers who partake in an almost-forgotten tradition. They take the largest of their pumpkin crop up to the forest, where the vegetables are left as an offering. It is said that if all pumpkins have vanished by the morning, the next year will bring a bountiful harvest." (Excerpt taken from 'Bluebirch Farms: A Detailed Introduction' by Steve and Stella McLaughlin)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops!
"""

DLG ITM_e
"""
CHARD. "Chard is a staple in these parts, and Swiss chard in particular is very popular, known for the rainbow of stems that create a brilliant display on house fronts. Local farmers claim that the 'little spirits of Bluebirch' adore this particular vegetable, and will arrange the stems in circular patterns on the forest floor - that is, if they even exist at all. If I'm honest, I think it's all complete #?**!%@$." (Excerpt taken from 'A Sceptic's Perspective' by ultimateknucklesfan on fancyfarmfolklore.com)
{intHarvest = intHarvest + 1}I now have {say intHarvest} out of 8 crops! 
"""

DLG SPR_1
"I wasn't scared of you, I just wasn't expecting to see you, that's all!"

DLG SPR_6
"Dear, we already have three pumpkins from the previous farms..."

DLG SPR_7
"It's getting late, I should head back to the B&B soon before it gets dark. Sucks though, I wish I could've seen an actual forest spirit."

DLG SPR_9
"Goodbye! It was lovely of you to visit our little farm."

DLG SPR_a
"Goodbye, forest spirit. Hope you and your friends enjoy your harvest."

DLG SPR_b
"I hope I see you again! And next time I'll know you're coming!"

DLG SPR_c
Walk towards moving sprites to interact with them.

DLG SPR_d
"Snacks!"

DLG SPR_e
"Snacks!"

DLG SPR_f
"Snacks!"

DLG SPR_g
"Snacks!"

DLG SPR_h
"Snacks!"

DLG SPR_i
"Snacks!"

DLG SPR_j
"Snacks!"

DLG SPR_k
"Snacks!"

END 0


END undefined
Yay, snacks! Time for sleep now. Goodnight....

VAR intHarvest
0

