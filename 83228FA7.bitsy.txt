
Outside The Room: The Second Idiom Awareness Game

# BITSY VERSION 4.6

! ROOM_FORMAT 1

PAL 0
0,114,178
230,159,0
240,228,66

ROOM 0
a,a,0,a,a,a,a,0,a,0,a,a,0,a,0,a
a,a,0,0,0,0,a,0,0,0,0,0,0,0,0,0
b,c,d,e,0,0,a,a,a,a,0,0,0,a,a,a
f,g,0,0,0,0,i,j,k,l,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,12,13,14,0,0,0,0,0,0,0,0,0,0,0,0
0,0,15,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,a,a,a
0,r,s,t,0,0,0,0,m,n,o,p,0,0,0,0
0,a,a,0,a,a,a,a,a,a,a,a,a,0,0,0
NAME Main Street
ITM 1 12,6
ITM 4 0,11
ITM 4 0,12
ITM 4 0,13
ITM 4 0,14
ITM 4 0,15
ITM 4 15,3
ITM 4 15,4
ITM 4 15,5
ITM 4 15,6
ITM 4 15,7
ITM 4 15,14
ITM 4 15,15
ITM 2 12,7
ITM b 14,3
ITM b 1,10
ITM 8 14,12
ITM c 14,8
ITM e 5,14
ITM e 6,14
ITM e 7,14
ITM e 5,1
ITM e 5,2
ITM e 1,8
ITM e 1,9
ITM a 6,11
ITM a 8,11
ITM d 14,9
EXT 12,14 1 1,1
EXT 2,0 2 7,15
EXT 3,15 3 3,0
EXT 0,7 4 15,7
PAL 0

ROOM 1
0,a,a,0,a,a,0,a,a,0,a,a,0,a,a,0
a,1b,1c,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
0,a,a,0,a,a,0,a,a,0,a,a,0,a,a,0
NAME Kitchen
ITM 3 11,5
ITM 4 0,15
ITM 4 15,15
ITM 4 6,15
ITM 4 9,15
ITM 4 15,12
ITM 4 15,6
ITM 4 15,3
ITM 4 15,0
ITM 4 0,12
ITM 4 0,9
ITM 4 0,6
ITM 4 0,0
ITM 4 3,0
ITM 4 6,0
ITM 4 9,0
ITM 1 3,15
ITM 1 15,9
ITM 1 0,3
ITM 2 12,0
ITM 2 12,15
ITM 7 2,13
ITM 7 5,13
ITM 8 13,7
ITM a 12,2
ITM b 3,13
ITM b 4,13
ITM c 10,13
ITM c 12,13
ITM d 10,11
ITM d 12,11
ITM e 7,1
ITM e 8,1
ITM e 7,2
ITM e 8,2
ITM e 1,10
ITM e 1,11
ITM e 2,10
ITM e 2,11
EXT 1,1 0 12,14
PAL 0

ROOM 2
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,16,17,18,19,1a,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,a,0,a,a,a,a,a,a,a,a
NAME ThunderZone
ITM 2 14,3
ITM 1 1,3
ITM b 10,1
ITM b 3,1
ITM a 10,2
ITM a 14,13
ITM 8 1,14
ITM 7 12,1
ITM 7 13,1
ITM 7 14,1
ITM 7 11,1
EXT 7,15 0 2,0
EXT 5,1 5 7,8
EXT 6,1 5 7,8
EXT 7,1 5 7,8
EXT 8,1 5 7,8
PAL 0

ROOM 3
0,r,s,u,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,16,17,18,19,1a
q,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
q,q,q,0,0,0,0,0,0,0,0,0,0,0,0,0
q,q,q,q,q,0,0,0,0,0,0,0,0,0,0,0
q,q,q,q,q,q,q,0,0,0,0,0,0,0,0,0
q,q,q,q,q,q,q,q,q,q,0,0,0,0,0,0
q,q,q,q,q,q,q,q,q,q,0,0,0,0,0,0
q,q,q,q,q,q,q,q,q,q,q,0,0,0,0,0
q,q,q,q,q,q,q,q,q,q,q,q,0,0,0,0
q,q,q,q,q,q,q,q,q,q,q,q,0,0,0,0
q,q,q,q,q,q,q,q,q,q,q,q,0,0,0,0
q,q,q,q,q,q,q,q,q,q,q,q,q,q,0,0
q,q,q,q,q,q,q,q,q,q,q,q,q,q,0,0
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,0
q,q,q,q,q,q,q,q,q,q,q,q,q,q,q,0
NAME Lake
ITM 1 15,15
ITM 2 11,8
ITM 7 14,3
ITM a 15,7
ITM b 7,5
ITM b 1,2
ITM c 6,0
ITM c 8,0
ITM c 10,0
ITM d 5,0
ITM d 7,0
ITM d 9,0
ITM e 5,1
ITM e 6,1
ITM e 7,1
ITM e 8,1
ITM e 9,1
ITM e 10,1
ITM 7 13,2
ITM 7 15,4
EXT 3,0 0 3,15
EXT 11,1 5 7,8
EXT 12,1 0 0,0
EXT 13,1 0 0,0
EXT 14,1 0 0,0
EXT 15,1 0 0,0
PAL 0

ROOM 4
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,11,0,0,0,0,0,a
a,16,17,18,19,1a,0,10,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,11,0,0,0,0,0,a
a,0,0,0,0,z,0,10,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,y,0,z,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
a,0,0,y,0,x,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,x,0,w,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,v,0,0,0,0,0,a
a,0,0,0,0,0,0,w,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,v,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Forest
ITM 2 1,14
ITM 2 14,1
ITM 1 7,7
ITM 1 9,7
ITM 2 8,7
ITM 7 2,11
ITM 7 4,12
ITM 7 6,14
ITM 7 2,13
ITM 7 4,14
EXT 15,7 0 0,7
EXT 1,2 5 7,8
EXT 2,2 5 7,8
EXT 3,2 5 7,8
EXT 4,2 5 7,8
PAL 0

ROOM 5
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,0,0,0,0,0,0,0,0,0,0,0,0,0,0,a
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
NAME Win Game
ITM 6 7,6
END 0 7,6
PAL 0

TIL 10
00000000
01111100
01000000
01000000
01110000
01000000
01000000
00000000
NAME F
WAL true

TIL 11
00000000
00111100
01000000
01000000
01001100
01000010
00111100
00000000
NAME G
WAL true

TIL 12
01101110
01001010
01001010
01101010
01001010
01001010
01001010
01001110
NAME FOR

TIL 13
11001100
10101001
10101001
10101001
11001100
10101000
10101001
10101100
NAME RES

TIL 14
11001110
00100100
00000100
00000100
11000100
00100100
00100100
11000101
NAME ST.

TIL 15
00000000
00010000
00100000
01111110
00100000
00010000
00000000
00000000
NAME Arrow Left

TIL 16
00000000
11101010
10001011
10001011
11001010
10001010
10001010
10001010
NAME FIN

TIL 17
00000000
01001001
01010101
01010101
11011101
11010101
01010101
01010101

TIL 18
00000000
00011101
00010001
00010001
00011000
00010000
00010001
11011101
NAME LEX

TIL 19
00000000
00101011
00101001
00101001
11001001
11001001
00101001
00101001
NAME XIT

TIL a
11111111
10000001
10000001
10011001
10011001
10000001
10000001
11111111
NAME Wall
WAL true

TIL b
11101001
01001001
01001001
01001111
01001111
01001001
01001001
01001001
NAME TH

TIL c
01001010
01001011
01001010
01001010
01001010
01001010
01001010
00110010
NAME UN

TIL d
00101100
00101010
10101001
10101001
01101001
01101001
00101010
00101100
NAME ND

TIL e
01110110
01000101
01000101
01000101
01100110
01000101
01000101
01110101
NAME ER

TIL f
00000000
11110111
00010101
00100101
01000101
10000101
11110111
00000000
NAME ZO

TIL g
00000000
01001011
01101010
01101010
01011011
01011010
01001011
00000000
NAME NE

TIL h
11111111
10000001
10000001
10000101
10000101
10000001
10000001
11111111
NAME Door
WAL true

TIL i
00000000
10001001
11011010
11011010
10101011
10001010
10001010
10001010
NAME MA

TIL j
00000000
10011001
01010101
01010101
11011001
01010101
01010101
01010101
NAME ARK

TIL k
00000000
00101110
00101000
01001000
10001100
01001000
00101000
00101110
NAME KE

TIL l
00000000
11100000
01000000
01000000
01000000
01000000
01000000
01010000
NAME T.

TIL m
10010101
10010100
10100100
11000100
10100100
10010100
10010100
00000000
NAME KIT

TIL n
11001001
10010101
10010001
10010001
10010001
10010101
10001001
00000000
NAME TCH

TIL o
01011101
01010001
01010001
11011001
01010001
01010001
01011101
00000000
NAME HEN

TIL p
00010000
10010000
10010000
01010000
01010000
01010000
00110100
00000000
NAME N.

TIL q
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME Water Full
WAL true

TIL r
00000000
10000110
10001001
10001001
10001111
10001001
11101001
00000000
NAME LA

TIL s
00000000
01001011
01010010
01100010
01100011
01010010
01001011
00000000
NAME KE

TIL t
00000000
00000100
00000100
00000100
00010101
00001110
00000100
00000000
NAME Arrow Down

TIL u
00000100
00001110
00010101
00000100
00000100
00000100
00000100
00000000
NAME Arrow Up

TIL v
00000000
00111100
01000010
01000010
01111110
01000010
01000010
00000000
NAME A
WAL true

TIL w
00000000
01111100
01000010
01000010
01111100
01000010
01111100
00000000
NAME B
WAL true

TIL x
00000000
00111100
01000010
01000000
01000000
01000010
00111100
00000000
NAME C
WAL true

TIL y
00000000
01110000
01001000
01000100
01000100
01001000
01110000
00000000
NAME D
WAL true

TIL z
00000000
01111100
01000000
01000000
01110000
01000000
01111100
00000000
NAME E
WAL true

TIL 1a
00000000
10000000
00000000
00000000
00000000
00000000
00000000
00100000
NAME T.

TIL 1b
00000000
11000100
10101010
10101010
11001110
10101010
10101010
11001010
NAME BA

TIL 1c
00000000
01101001
10101010
10001010
10001100
10001010
10101010
01101001
NAME CK

SPR 10
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeF
DLG SPR_q
POS 4 7,3

SPR 11
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeG
DLG SPR_p
POS 4 9,2

SPR 12
00010000
00111000
11111110
01111100
00111000
01111100
11101110
00000000
NAME Magic Star Girl
DLG SPR_k
POS 4 11,7

SPR 13
00000000
00001000
01001110
01111100
00011100
00111100
00100110
00000000
NAME Hatty
DLG SPR_o
POS 4 11,4

SPR 14
00000000
00011000
01001110
01111010
00011000
01111000
00001100
00000000
NAME Juniper
DLG SPR_n
POS 4 13,2

SPR 15
00000000
00001100
01001000
01111110
00111100
00100100
00001100
00000000
NAME Izzy
DLG SPR_m
POS 4 11,10

SPR 16
00000000
00011100
01001000
01111110
01011000
00011100
00010100
00000000
NAME Kat
DLG SPR_l
POS 4 13,12

SPR 17
11111111
10000001
10000001
10000101
10000101
10000001
10000001
11111111
NAME DoorA

SPR 19
11111111
10000001
10000001
10000101
10000101
10000001
10000001
11111111
NAME DoorB
DLG SPR_w

SPR A
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
NAME Metaphor Human
POS 0 7,8

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
NAME Cat, Guardian of the Kitchen
DLG SPR_0
POS 0 12,13

SPR b
00000000
00000000
00000000
00000000
00101001
00010010
00011100
00010100
NAME Kitten, Half-Guardian of the Pantry
DLG SPR_1
POS 1 11,7

SPR c
00001000
00011100
00101010
01111111
01110111
00011100
00111110
01100011
NAME Pinecone Face
DLG SPR_2
POS 0 11,2

SPR d
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000
NAME MarketItem1
DLG SPR_3
POS 0 7,0

SPR e
00000000
00000000
00011000
00011000
00011000
00000000
00000000
00000000
NAME MarketItem2
DLG SPR_6
POS 0 9,0

SPR f
00000000
00000000
00010000
00111000
00101000
00000000
00000000
00000000
NAME MarketItem3
DLG SPR_5
POS 0 12,0

SPR g
00000000
00010000
00010000
00010000
00011000
00011000
00000000
00000000
NAME MarketItem4
DLG SPR_4
POS 0 14,0

SPR h
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME Lightning Friend
DLG SPR_7
POS 0 3,1

SPR i
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME Lightning Foe
DLG SPR_8
POS 0 1,1

SPR j
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateAA
DLG SPR_9
POS 2 4,12

SPR l
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateFF
DLG SPR_a
POS 2 11,12

SPR m
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateBB
DLG SPR_b
POS 2 2,9

SPR n
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateCC
DLG SPR_c
POS 2 6,9

SPR o
00000110
00001110
00011010
00110010
11100010
11100010
11100010
00000010
>
00000000
01110000
11011000
00001100
00000110
00000010
00000010
00000010
NAME Fishing Rod
DLG SPR_e
POS 3 10,6

SPR p
00000000
01110001
10001011
11001111
10001011
01110001
00000000
00000000
NAME Fabulous Fish Frieda
DLG SPR_d
POS 3 11,6

SPR q
00000110
00001110
00011010
00110010
11100010
11100010
11100010
00000010
>
00000000
01110000
11011000
00001100
00000110
00000010
00000010
00000010
NAME Fishing Rod 2
DLG SPR_f
POS 3 12,11

SPR r
00000000
01110000
11011000
00001100
00000110
00000010
00000010
00000010
>
00000000
00001110
00011010
00110010
11100010
11100010
11100010
00000010
NAME Fishing Rod 3
DLG SPR_g
POS 3 5,4

SPR s
00000000
01110000
11011000
00001100
00000110
00000010
00000010
00000010
>
00000000
00001110
00011010
00110010
11100010
11100010
11100010
00000010
NAME Fishing Rod 4
DLG SPR_h
POS 3 14,13

SPR t
11111111
10000001
10111101
10011001
10011001
10011001
10000001
11111111
NAME ThunderCounterA
DLG SPR_i

SPR u
11111111
10000001
10111101
10011001
10011001
10011001
10000001
11111111
NAME ThunderCounterB
DLG SPR_j
POS 2 1,4

SPR v
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeA
DLG SPR_u
POS 4 9,12

SPR w
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeB
DLG SPR_t
POS 4 7,11

SPR x
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeC
DLG SPR_s
POS 4 5,9

SPR y
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeD
DLG SPR_v
POS 4 3,7

SPR z
00011000
00111100
01111110
11111111
01111110
00011000
00011000
00111100
NAME TreeE
DLG SPR_r
POS 4 5,5

SPR 1a
11111111
10000001
10000001
10000101
10000101
10000001
10000001
11111111
NAME DoorC

SPR 1b
11111111
10000001
10111101
10011001
10011001
10011001
10000001
11111111
NAME ThunderCounterC
DLG SPR_x
POS 2 14,4

SPR 1c
00000000
00100100
00100100
00100100
10000001
10000001
10000001
11111111
NAME Smiley Man
DLG SPR_y

SPR 1d
00000000
00011000
00111100
01111110
01111110
00111100
00011000
00000000
>
00000010
00000111
00001110
00011100
00111000
01110000
11100000
01000000
NAME The Worm
DLG SPR_z
POS 1 1,8

SPR 1e
00000010
00000000
00000000
01110000
11001000
11001000
11001000
01111000
>
00000000
00000100
00000000
01110000
11001000
11001000
11001000
01111000
NAME Kettle of Fish
DLG SPR_10
POS 1 13,13

SPR 1f
00000000
01000010
11100110
01111111
01111110
11111110
01111110
00111100
>
00000000
00000000
01000010
01100110
00100100
01111110
00111100
00011000
NAME Fire
POS 1 8,13

SPR 1g
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateDD
DLG SPR_11
POS 2 4,6

SPR 1h
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateEE
DLG SPR_12
POS 2 13,8

SPR 1i
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateGG
DLG SPR_13
POS 2 7,4

SPR 1j
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateJJ
DLG SPR_14
POS 2 5,3

SPR 1k
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateKK
DLG SPR_15
POS 2 10,7

SPR 1l
00010000
00110000
01110000
11111100
00111111
00001110
00001100
00001000
NAME ThunderMateOO
DLG SPR_16
POS 2 11,4

ITM 1
00000000
00000000
00010000
00011000
00011100
00100000
01000000
10000000
NAME An arm
DLG ITM_2

ITM 2
00000100
00000100
00000100
00000100
00000100
00000100
00011100
00111100
NAME A leg
DLG ITM_1

ITM 3
11111111
10000001
10110001
10101001
10110001
10101001
10000001
11111111
NAME Recipe
DLG ITM_3

ITM 4
00010000
00010000
00110000
01111100
00111110
00001100
00001000
00001000
NAME Lightning 
DLG ITM_0

ITM 5
00000000
00000000
00111100
00111100
00111100
00111100
00000000
00000000
DLG ITM_4

ITM 6
00000000
00100100
00100100
00100100
10000001
10000001
10000001
11111111
NAME Smiley Man

ITM 7
00011000
00010000
00111100
01111110
01111110
01111110
00111100
00100100
NAME Apple
DLG ITM_5

ITM 8
00111100
01000010
11111111
10000001
10000001
10000001
10000001
11111111
NAME The Bag
DLG ITM_6

ITM a
00100000
01000000
00100000
00001000
00000100
01111100
01111111
01111100
>
00000100
00001000
00000100
01000000
00100000
01111100
01111111
01111100
NAME The Frying Pan
DLG ITM_7

ITM b
00000000
00100000
00010010
00000100
00000000
00111100
11111111
01111110
>
00000000
00000010
00100100
00010000
00000000
00111100
11111111
01111110
NAME Humble Pie
DLG ITM_8

ITM c
00111000
01000100
01000100
10000010
10000010
10000010
10000010
01111100
NAME Good Egg
DLG ITM_9

ITM d
01111100
10000010
10000010
10000010
10000010
01000100
01000100
00111000
NAME Bad Egg
DLG ITM_a

ITM e
01111110
10000001
10000001
01000010
01000010
01000010
01000010
01111110
NAME Sliced Bread
DLG ITM_b

DLG SPR_0
"""
I'm the Cat, the great Guardian of the Kitchen! 
Who dares to enter?
"""

DLG ITM_1
You found a leg!{i = i + 1} {l = l + 1}

DLG ITM_2
You found an arm!{i = i + 1} {a= a + 1}

DLG SPR_1
"""
Hello! Friend!

I'm Kitten, Guardian of the Pantry!
I'm here to help you with your Recipe!
{
  - i >= 5 ?
    You've got enough items to make your food! {f = 1}
  - else ?
    You don't have enough items to make your food!
}{
  - {item "Recipe"} == 1 ?

    You've got the Recipe ready to cook! {r = 1}
  - else ?

    You haven't found the recipe yet! 
}{
  - r == 1 ?
    {
      - f == 1 ?

        You're ready to cook!
        
        Putting everything into a tiny teacup, you brew up a storm.
        It tastes alright...
        
        but you think you've bitten off more than you can chew.
      - else ?

        You're not ready to cook yet!
    }}
"""

DLG ITM_3
You found the Recipe!

DLG SPR_2
"""
Greetings, I am Pinecone Face, renowned Prin of the Land!
I run this entire Marketplace all by myself.
What business do you have here today?

...
{
  - a == 1 ?
    With what you've got, they'll need an ARMy to stop you. {m = m + 1}
  - else ?
}
{
  - l == 1 ?
    I see you've got a LEG up on your competition. {m = m + 1}
  - else ?
}
{
  - m == 2 ?
    The shop is your oyster! Buy whatever you'd like.
  - else ?
    You don't have enough yet. These cost an arm and a leg. {m = 0}
}
"""

DLG SPR_3
"""
A beautiful heart, for those who need some extra love! 
{
  - a >= 1 ?
    {
      - l >= 1 ?
        {b = 1}
    
      - else ?
        You don't have enough. These cost an arm and a leg!
    }
  - else ?
    You don't have enough. These cost an arm and a leg!
}{
  - b == 1 ?
    {
        - hh > 0 ?
        It's all yours! {a = a - 1} {l = l - 1} {b = 0} {hh = hh - 1}

        - else ?
        There's no more hearts left!}
}
"""

DLG SPR_4
"""
A golf club, for fun.

{
  - a >= 1 ?
    {
      - l >= 1 ?
        {b = 1}
    
      - else ?
        YOu don't have enough. These cost an arm and a leg!!
    }
  - else ?
    You don't have enough. These cost an arm and a leg!
}{
  - b == 1 ?
    {
        - golf > 0 ?
        It's all yours! {a = a - 1} {l = l - 1} {b = 0} {golf = golf - 1}

        - else ?
        You've already bought our golf club.}
}
"""

DLG SPR_5
"""
A reliable hand, for those who need some extra help.
{
  - a >= 1 ?
    {
      - l >= 1 ?
        {b = 1}
    
      - else ?
        YOu don't have enough. These cost an arm and a leg!!
    }
  - else ?
    You don't have enough. These cost an arm and a leg!
}{
  - b == 1 ?
    {
        - hand > 0 ?
        It's all yours! {a = a - 1} {l = l - 1} {b = 0} {hand = hand - 1}

        - else ?
        I can't give you a hand today, we're all out!}
}
"""

DLG SPR_6
"""
A powerful body, for those who need some strength.
{
  - a >= 1 ?
    {
      - l >= 1 ?
        {b = 1}
    
      - else ?
        YOu don't have enough. These cost an arm and a leg!!
    }
  - else ?
    You don't have enough. These cost an arm and a leg!
}{
  - b == 1 ?
    {
        - body > 0 ?
        It's all yours! {a = a - 1} {l = l - 1} {b = 0} {body = body - 1}

        - else ?
        I'm sorry, we're all out of body today!}
}
"""

DLG SPR_7
"""
Friend or foe, friend or foe? Never ever shall you know.
Walk between us and one step more,
Enter the room of thunder galore.
{
  - t == 0 ?
    You stole my thunder, friend! {t = t + 4}
  - else ?

}

Your thunder: {say t}
ThunderMate thunder: -4
"""

DLG SPR_8
"""
Foe or friend, foe or friend? Will this cycle ever end?
Walk between us if you so dare,

And find the thunder and all who care.
 {
  - t == 0 ?
    I stole your thunder, foe! {t = t - 4}
  - else ?
    t=t
}

Your thunder: {say t}
ThunderMate thunder: 4
"""

DLG SPR_9
"""
Let's have a thunder showdown! {aa = 1}

{  - t >= aa ?
    {- t == aa ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + aa}}
  - else ?
    I stole your thunder, foe! { t = t - aa}
}

Your thunder: {say t}
ThunderMate thunder: {say aa}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_a
"""
Are you ready to rock? {ff = 10}

{  - t >= ff ?
    {- t == ff ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + ff} {ff = ff - t}}
      - else ?
    I stole your thunder, foe! {t = t - ff} {ff = ff + t}

}

Your thunder: {say t}
ThunderMate thunder: {say ff}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}
}
"""

DLG SPR_b
"""
Well aren't you a bright spark? {bb = 5}

{  - t >= bb ?
    {- t == bb ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + bb} {bb = bb - t}}
      - else ?
    I stole your thunder, foe! { t = t - bb} {bb = bb + t}

}

Your thunder: {say t}
ThunderMate thunder: {say bb}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}
}
"""

DLG ITM_0
"""
{
  - {item "Lightning "} == 1 ?
    When there's lightning, there's thunder! {t = t + 1}
Your thunder: {say t}
  - else ?{t = t + 1}
{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}
}}
"""

DLG SPR_c
"""
Well aren't you a bright spark? {cc = 2}

{  - t >= cc ?
    {
      - t == cc ?
         
        I guess we'll call it even for now...SHOCKING!
      - else ?You stole my thunder, friend! {t = t + cc} {cc = cc - t}
    }  
  - else ?
    I stole your thunder, foe! {t = t - cc} {cc = cc + t}

}

Your thunder: {say t}
ThunderMate thunder: {say cc}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_d
"""
{
  - frieda == 0 ?
    Fancy friending you on this fantastic Friday. I'm Frieda!
    Want to fish? Just grab one of the rods over there.
    {frieda = 1}
  - else ?
    Just grab a rod to fish.
}
"""

DLG SPR_e
"""
{shuffle
  - You are amazing!
  - You're such a champion!
  - You're a pleasure to be around!
  - You're really nice!
  - You're really good at that thing you do!
  - You have a beautiful smile!
  - You are fantastic!
  - You have really good thoughts!
  - You brighten up the room!
  - You are a great friend!
  - You are fabulous!
  - You are really nice!
  - You work really hard at your goals!
  - You are a superstar!
  - You are a really cool person!
  - You have interesting thoughts!
  - You have interesting things to say!
  - You are valuable!
  - You are worthwhile!
}
"""

DLG SPR_f
"""
{shuffle
  - You are amazing!
  - You're such a champion!
  - You're a pleasure to be around!
  - You're really nice!
  - You're really good at that thing you do!
  - You have a beautiful smile!
  - You are fantastic!
  - You have really good thoughts!
  - You brighten up the room!
  - You are a great friend!
  - You are fabulous!
  - You are really nice!
  - You work really hard at your goals!
  - You are a superstar!
  - You are a really cool person!
  - You have interesting thoughts!
  - You have interesting things to say!
  - You are valuable!
  - You are worthwhile!
}
"""

DLG SPR_g
"""
{shuffle
  - You are amazing!
  - You're such a champion!
  - You're a pleasure to be around!
  - You're really nice!
  - You're really good at that thing you do!
  - You have a beautiful smile!
  - You are fantastic!
  - You have really good thoughts!
  - You brighten up the room!
  - You are a great friend!
  - You are fabulous!
  - You are really nice!
  - You work really hard at your goals!
  - You are a superstar!
  - You are a really cool person!
  - You have interesting thoughts!
  - You have interesting things to say!
  - You are valuable!
  - You are worthwhile!
}
"""

DLG SPR_h
"""
{shuffle
  - You are amazing!
  - You're such a champion!
  - You're a pleasure to be around!
  - You're really nice!
  - You're really good at that thing you do!
  - You have a beautiful smile!
  - You are fantastic!
  - You have really good thoughts!
  - You brighten up the room!
  - You are a great friend!
  - You are fabulous!
  - You are really nice!
  - You work really hard at your goals!
  - You are a superstar!
  - You are a really cool person!
  - You have interesting thoughts!
  - You have interesting things to say!
  - You are valuable!
  - You are worthwhile!
}
"""

DLG SPR_i
"""
Your thunder: {say t}

{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you. {t = 57}
}
"""

DLG SPR_j
"""
Your thunder: {say t}

{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you. {t = 57}
}
"""

DLG SPR_k
"""
Greetings, young one. I am the Magic Star Girl of the Forest.
Here are my star children: Hatty, Izzy, Juniper and Kat.
I have wandered through these trees for years and years.
The trees speak to me. They want to find their next protector.
To do that, you need to find the wrong tree.
Can you solve their riddle?
"""

DLG SPR_l
I'm Kat. My clue is: you cannot build me with just straight lines.

DLG SPR_m
I'm Izzy. My clue is: I look different as I grow.

DLG SPR_o
Hi, I'm Hatty. My clue is: I am not a vowel.

DLG SPR_p
You're barking up the wrong tree!

DLG SPR_q
This is not the tree you seek! { t = t -1}

DLG SPR_r
This is not the tree you seek! { i = i -1}

DLG SPR_s
This is not the tree you seek! { i = i -1}

DLG SPR_t
This is not the tree you seek! { t = t -1}

DLG SPR_u
This is not the tree you seek! { i = i -1}

DLG SPR_v
This is not the tree you seek! { t = t -1}

DLG SPR_n
I'm Juniper. My clue: you cannot use me to spell where you rest.

DLG SPR_w
"""
{
  - t >= 10 ?
    (exit "Win Game,8,4")
  - else ?

}
"""

DLG ITM_4
"""
{
  - t >= 12 ?
    (exit "Win Game, 8, 4")
  - else ?

}
"""

DLG SPR_x
"""
Your thunder: {say t}

{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}
}
"""

DLG SPR_y
"""
I hope you enjoyed your time here.
The End.
"""

DLG ITM_5
I'm the apple of your eye. {i = i + 1}

DLG ITM_6
"""
{
  - bag == 0 ?
    Help! Help! My cat is missing. She got out! {i = i + 1}
    You found The Bag. {bag = 1}  
  - else ?
    {i = i + 1}
}
"""

DLG ITM_7
"""
{
  - pan == 0 ?
    Everything jumped out of me!
    
    ...
    
    It's tough being a frying pan. 
    
    You obtained Frying Pan. {i = i + 1} {pan = 1}
  - else ?
    {i = i + 1}
}
"""

DLG SPR_z
"""
Shhhh!
The early bird hasn't caught me yet...
"""

DLG ITM_8
"""
{
  - pie == 0 ?
    I wouldn't say I'm humble. 
    
    I'm just me. A plain old meat pie.
    You obtained Humble Pie. {i = i + 1} {pie = 1}
  - else ?
    {i = i + 1}
}
"""

DLG SPR_10
I'm full of fabulous fish!

DLG ITM_9
You found a Good Egg.

DLG ITM_a
You found a Bad Egg.

DLG ITM_b
"""
{  - bread == 0 ?
    I used to be the greatest thing...
    You found Sliced Bread. {i = i + 1} {bread = 1}
  - else ?{i = i + 1}
}
"""

DLG SPR_11
"""
What a shock finding you here! {dd = 18}

{  - t >= dd ?
    {- t == dd ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + dd} {dd = dd - t}}    
  - else ?
    I stole your thunder, foe! { t = t - dd} {dd = dd + t}

}

Your thunder: {say t}
ThunderMate thunder: {say dd}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_12
"""
What a shock finding you here! {ee = 13}

{  - t >= ee ?
    {- t == ee ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + ee} {ee = ee - t}}
      - else ?
    I stole your thunder, foe! { t = t - ee} {ee = ee + t}

}

Your thunder: {say t}
ThunderMate thunder: {say ee}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_13
"""
What a shock finding you here! {gg = 24}

{  - t >= gg ?
    {- t == gg ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + gg} {gg = gg - t}}
      - else ?
    I stole your thunder, foe! { t = t - gg} {gg = gg + t}

}

Your thunder: {say t}
ThunderMate thunder: {say gg}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_14
"""
What a shock finding you here! {jj = 3}

{  - t >= dd ?
    {- t == dd ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + jj} {jj = jj - t}}
      - else ?
    I stole your thunder, foe! { t = t - jj} {jj = jj + t}

}

Your thunder: {say t}
ThunderMate thunder: {say jj}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_15
"""
What a shock finding you here! {kk = 7}

{  - t >= kk ?
    {- t == kk ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + kk} {kk = kk - t}}
      - else ?
    I stole your thunder, foe! { t = t - kk} {kk = kk + t}

}

Your thunder: {say t}
ThunderMate thunder: {say kk}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

DLG SPR_16
"""
What a shock finding you here! {oo = 18}
{  - t >= oo ?
    {- t == oo ? 
        I guess we'll call it even for now...SHOCKING!
    - else ?You stole my thunder, friend! {t = t + oo} {oo = o - t}}
      - else ?
    I stole your thunder, foe! { t = t - oo} {oo = oo + t}

}

Your thunder: {say t}
ThunderMate thunder: {say oo}{
    - t > 57 ?
    Whoa, you're electrifying! No more thunder for you.  {t = 57}}
"""

END 0
The end. I hope you enjoyed your time.

END undefined
The end.

END undefinee


END undefinef


VAR i
0

VAR r
0

VAR a
0

VAR l
0

VAR m
0

VAR b
0

VAR t
0

VAR frieda
0

VAR hh
4

VAR body
2

VAR hand
5

VAR golf
1

VAR bread
0

VAR pie
0

VAR bag
0

VAR pan
0


