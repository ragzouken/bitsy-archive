A date

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
NAME harvest
204,144,83
125,125,61
194,224,92

PAL 1
NAME outside cinema
204,144,83
255,192,54
255,255,255

ROOM 0
b,b,b,b,b,b,b,b,b,b,b,b,0,0,b,b
b,0,c,c,c,c,c,0,0,b,b,b,0,0,b,b
b,0,a,a,c,c,c,a,a,b,b,c,c,c,b,b
b,0,c,0,0,c,0,a,b,c,c,c,0,b,b,b
b,0,b,0,0,0,a,b,c,0,0,b,b,b,b,b
b,0,c,c,0,0,c,0,c,c,b,b,0,0,0,b
b,0,a,0,0,0,c,c,c,b,b,0,0,0,0,b
b,0,a,a,c,0,a,a,0,b,b,0,c,c,c,b
b,0,a,a,a,0,0,0,0,0,0,0,b,c,c,b
b,0,0,a,a,0,0,0,c,a,a,b,c,c,c,b
b,a,0,a,a,a,a,c,c,c,a,0,c,0,0,b
b,a,a,0,a,a,a,0,0,c,c,0,a,0,a,b
b,a,a,0,0,c,0,0,0,0,c,a,a,a,a,b
b,a,a,0,a,a,c,c,c,c,a,a,0,0,a,b
b,0,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME harvest
ITM 1 14,6
EXT 12,0 1 4,14
EXT 13,0 1 4,14
PAL 0

ROOM 1
b,d,d,d,d,d,d,d,d,d,d,d,b,b,b,b
b,d,d,d,d,d,f,d,d,d,d,d,b,0,0,c
b,b,b,b,b,b,i,b,b,b,b,b,b,0,0,0
0,0,c,0,0,0,0,0,0,0,0,0,c,c,0,0
0,0,0,0,0,c,0,0,0,0,c,c,c,c,0,0
0,0,c,0,0,0,0,0,b,b,g,g,g,g,g,g
g,g,b,b,c,c,0,0,b,g,c,0,0,0,0,0
h,h,g,b,b,c,c,0,g,g,0,0,0,0,0,0
a,h,h,g,g,0,c,c,0,0,0,c,c,c,0,0
h,a,a,h,g,0,0,c,0,g,g,g,g,g,g,g
a,h,h,h,g,0,c,c,0,g,g,h,h,h,h,h
a,a,h,h,g,0,0,c,0,g,h,a,h,h,a,a
a,h,h,h,g,0,0,0,0,g,a,h,h,h,a,a
a,h,a,g,g,0,0,0,g,g,h,h,h,a,a,a
a,h,g,g,c,c,0,g,g,h,h,a,h,h,h,a
a,g,g,0,c,c,g,g,a,h,a,a,a,a,a,a
NAME cinema
ITM 2 13,2
EXT 15,6 2 0,10
EXT 15,7 2 0,11
EXT 15,8 2 0,12
PAL 0

ROOM 2
g,g,g,g,g,g,g,g,g,g,g,g,g,g,g,g
g,j,j,j,j,j,j,k,j,j,j,j,j,j,j,j
g,j,j,j,j,j,j,j,j,j,j,j,j,k,j,j
g,j,k,j,j,j,j,j,j,j,j,j,j,j,j,j
g,j,j,j,j,j,k,j,j,j,j,k,j,j,j,j
g,j,j,j,j,j,j,j,j,j,j,j,j,j,j,j
g,l,l,l,l,l,l,l,l,l,l,l,l,l,l,m
g,0,0,0,0,0,0,0,0,0,0,0,0,0,0,m
g,a,0,c,0,a,a,0,c,0,c,0,0,0,c,m
g,g,0,0,c,c,0,0,0,0,0,0,0,a,a,m
0,g,0,0,a,0,c,c,0,0,0,a,a,c,0,m
0,0,0,0,0,0,0,0,c,0,0,0,0,0,0,m
0,0,c,c,0,0,0,0,0,0,0,0,0,0,0,m
g,0,0,0,0,0,c,0,0,0,c,0,0,0,0,m
g,g,g,g,g,g,g,g,g,0,g,g,g,g,g,g
c,c,a,a,0,a,c,c,g,0,g,a,c,a,a,c
NAME the harvest less
EXT 9,15 3 7,0
PAL 0

ROOM 3
c,0,0,0,a,0,g,0,g,c,0,0,0,0,c,c
c,c,c,a,a,0,g,0,g,0,c,c,c,0,a,0
a,a,0,0,0,g,0,0,g,g,0,a,0,0,a,a
a,c,a,a,g,g,0,0,0,g,g,c,a,c,c,a
a,a,a,g,g,q,q,q,q,q,g,g,c,a,a,a
n,n,n,p,q,q,q,q,q,q,q,p,n,n,n,n
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
NAME bridge
EXT 4,15 4 7,1
EXT 5,15 4 7,1
EXT 6,15 4 7,1
EXT 7,15 4 7,1
EXT 8,15 4 7,1
EXT 9,15 4 7,1
EXT 10,15 4 7,1
PAL 0

ROOM 4
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
o,o,n,p,q,q,q,q,q,q,q,p,n,o,o,o
NAME bridge 2
ITM 3 9,8
END undefined 4,15
END undefined 5,15
END undefined 6,15
END undefined 7,15
END undefined 8,15
END undefined 9,15
END undefined 10,15
PAL 0

TIL a
00000000
00000000
00101000
00101010
01111110
11111111
11111111
00000000
>
00000000
00000000
01010000
00101010
01111110
11111111
11111111
00000000
NAME grass

TIL b
00101100
01111110
11011011
01101110
00111100
00011000
00011000
00011000
>
00110100
01111110
11010111
01111110
00101100
00011000
00011000
00011000
NAME tree
WAL true

TIL c
00000000
00000000
00000000
00000110
00111000
01000000
00000000
00000000
NAME ground

TIL d
11111111
11100111
10000111
11110001
11110001
10001111
11111111
11111111
NAME brick

TIL e
11111111
10000001
10111101
10100101
10100101
10111101
10000001
11111111
NAME tile

TIL f
00000000
01111110
01111110
01111110
01110010
01111110
01111110
01111110
NAME door

TIL g
00011100
01111110
11100111
11011111
10111111
11111111
11111110
01110000
NAME rock
WAL true

TIL h
00010000
00010100
01000100
01000100
00010111
11010110
01001100
00000000
NAME little grass

TIL i
00000000
11111111
10000001
10111101
10000001
11111111
00011000
00011000
NAME sign

TIL j
00000000
00001000
00110000
00000000
00110110
01001000
00000000
00000000
NAME ground empty

TIL k
10000110
01000100
00101100
00011000
11010110
00111000
00010000
00010000
>
00001100
10000100
01101100
00101000
00010100
11011000
00110000
00010000
NAME ground with harvest

TIL l
00000000
10001001
10110001
11000011
11111111
11000011
11111111
11000011
NAME wood
WAL true

TIL m
01000000
01100100
01111000
01011000
01101010
01111001
00011000
00001000
NAME wood side
WAL true

TIL n
00000000
00000000
11011011
11011011
01111110
01111110
01111110
01111110
NAME bridge
WAL true

TIL o
11111111
11110011
11000111
10011111
11110011
10001111
01111111
11111111
>
11111111
11111101
11110001
10001111
11111001
11110111
10011111
00111111
NAME water

TIL p
11111111
10011001
10100101
11000011
11000011
10100101
10011001
11111111
NAME tile

TIL q
00000001
00000000
01000000
00000100
00001000
00100000
00000000
00000000
NAME street

SPR A
00011100
00011100
00011100
00011000
00011000
01111110
00011000
00100100
>
00111000
00111000
00111000
00011000
00011000
01111110
00011000
00100100
POS 0 9,14

SPR a
00111100
00111110
00111110
00011011
00011000
01111110
00011000
00100100
>
00111100
00111110
00111110
00011011
01011000
00111110
00011000
00100100
NAME Valerie
DLG SPR_0
POS 0 4,5

SPR b
00000000
11111111
10000001
10111101
10000001
11111111
00011000
00011000
NAME sign
DLG SPR_1
POS 1 6,2

SPR c
00111100
00111110
00111110
00011011
00011000
01111110
00011000
00100100
NAME Valerie 2
DLG SPR_2
POS 1 6,14

SPR d
00111100
00111110
00111110
00011011
00011000
01111110
00011000
00100100
DLG SPR_3
POS 2 1,12

SPR e
00000000
11111111
10000001
10011101
10100001
10101101
10000001
11111111
NAME newspaper
DLG SPR_5
POS 3 8,3

SPR f
00111100
00111110
00111110
00011011
00011000
01111110
00011000
00100100
NAME Valerie 3
DLG SPR_4
POS 3 6,2

SPR g
00111100
00111100
00011000
01111100
00011010
00100100
01100110
00000000
>
00111100
00111100
10011000
01111100
00011010
00100100
01100110
00000000
NAME citizen
DLG SPR_7
POS 4 5,12

SPR h
00111100
00111100
00011000
01111110
00011000
00100100
01100110
00000000
>
00111100
00111100
10011001
01111110
00011000
00100100
01100110
00000000
NAME citizen 2
DLG SPR_6
POS 4 9,14

SPR i
00111100
00111110
00111110
00011011
00011000
01111110
00011000
00100100
>
00111100
01111100
01111100
11011000
00011000
01111110
00011000
00100100
NAME Valerie 4
DLG SPR_8
POS 4 7,2

ITM 1
00111100
01100110
10000001
01100110
00111100
11011011
01111110
00011000
NAME flower
DLG ITM_0

ITM 2
00000000
00100000
00010000
00111100
01001110
01011110
01111110
00111100
NAME apple
DLG ITM_1

ITM 3
11111111
10011111
11111111
11111111
00011000
00011000
00011000
00011000
NAME protest sign
DLG ITM_2

DLG SPR_0
"""
{
  - a == 1 ?
    Ready to go? {valpoints}
  - a == 0 ?
    Hi! Finally we meet! You are a little late, tho. It's ok. First time traveling to this town is hard. {a = 1}
}{
  - b == 0 ?
    {
      - {item "flower"} > 0 ?
        OMG! Such a beautiful flower! Thank you! So cute! You just won some points, haha.
        Can't wait to go out with you.{b = 1}
{valpoints = valpoints + 1}
    }
}
"""

DLG ITM_0
A beautiful flower

DLG SPR_1
Cinema is closed due to the lack of rights for bringing movies to the country.

DLG SPR_2
"""
{
  - {item "apple"} == 0 ?
    It seems the cinema is closed. Maybe we could go somewhere else. *sigh* I was hoping to eat a popcorn and take away my hunger.
  - apple_del == 1 ?
    It seems the cinema is closed. Maybe we could go somewhere else.
  - {item "apple"} == 1 ?
    Oh thanks! Just what I need. *bite* {apple_del = 1} {valpoints = valpoints + 1}
}
"""

DLG ITM_1
A delicious apple

DLG SPR_3
All the harvest has been expropiated by the government and now the production is very low. The town is falling apart. We, the young people, gotta do something!

DLG SPR_4
What a beautiful bridge, isn't it? There is beauty in this misery afterall.

DLG SPR_5
Newspaper: "The security forces of the government are getting violent. The citizens of the town have taken the streets."

DLG SPR_6
"""
{shuffle
  - Give us our lands back!
  - This is not living, is surviving!
}
"""

DLG ITM_2
A protest sign

DLG SPR_8
"""
{
  - valpoints == 0 ?
    If the cinema had been opened, the date would be more fun. 
  - valpoints == 1 ?
    It was a great date. 
  - valpoints >= 2 ?
    What a perfect day! I like you.
}{sequence
  - I'd like to... LOOK! The people is protesting up ahead the bridge!
  - F***ing government! Sorry if this wasn't what was planned. 
}{
  - sign == 0 ?
    {
      - {item "protest sign"} == 1 ?
        A protest sign? Are you sure? All right! Let's finish this date with a great fight!{valpoints = valpoints + 1} {sign = 1}}}
"""

DLG SPR_7
"""
{shuffle
  - No more kills!
  - We want freedom!
}
"""

END 0


END undefined
Love survives anything...

VAR a
0

VAR b
0

VAR valpoints
0

VAR apple_del
0

VAR sign
0

