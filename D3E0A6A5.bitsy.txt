the gardener's story...

# BITSY VERSION 3.5

! ROOM_FORMAT 1

PAL 0
0,204,35
0,123,14
0,0,0

PAL 1
22,219,0
238,19,19
0,0,0

ROOM 0
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,a,p,p,q,q,p,r,q,r,q,r,b,c,c
c,c,m,l,f,i,0,l,0,f,i,0,f,s,c,c
c,c,m,i,g,v,w,v,w,v,x,h,l,j,q,q
c,c,n,l,t,0,0,0,0,0,0,n,0,l,0,l
c,c,o,0,u,0,12,0,0,0,0,d,w,x,w,x
c,c,m,l,t,0,0,0,0,0,0,0,12,0,0,0
c,c,m,f,s,0,0,12,0,12,0,0,0,0,0,0
c,c,o,i,t,0,0,0,0,0,0,a,p,q,p,q
c,c,n,0,s,0,0,0,0,0,0,m,f,0,i,l
c,c,m,l,j,q,q,r,q,r,q,k,l,g,w,x
c,c,o,f,i,0,l,f,i,0,l,f,0,u,c,c
c,c,d,v,x,v,v,w,v,v,w,w,v,e,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,t,u,v,w,x
EXT 15,7 1 1,7
EXT 15,8 1 1,8
PAL 0

ROOM 1
c,c,c,c,m,l,u,0,0,o,i,u,c,c,c,c
c,c,c,c,m,i,s,0,0,n,l,u,c,c,c,c
c,c,a,p,k,f,u,0,0,o,f,j,r,b,c,c
c,c,n,f,l,i,s,0,0,n,i,0,f,t,c,c
q,q,k,0,g,v,e,0,0,d,v,h,0,s,c,c
f,i,f,i,s,0,0,0,0,0,0,m,i,s,c,c
v,w,v,w,e,0,0,0,0,0,0,n,l,u,c,c
0,0,0,0,0,12,0,0,12,0,0,o,f,s,c,c
0,0,0,0,0,0,0,0,0,0,0,m,f,s,c,c
r,q,r,q,b,0,0,0,0,0,0,n,i,s,c,c
i,0,f,0,s,0,0,0,0,0,0,m,i,t,c,c
w,w,h,l,j,p,b,0,0,a,p,k,i,u,c,c
c,c,o,0,f,l,t,12,0,n,i,l,f,s,c,c
c,c,d,v,h,0,t,0,0,n,l,g,v,e,c,c
c,c,c,c,n,l,t,0,0,n,0,t,c,c,c,c
c,c,c,c,n,i,t,0,0,n,l,t,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 0,7 0 14,7
EXT 0,8 0 14,8
EXT 7,15 2 7,1
EXT 8,15 2 8,1
EXT 7,0 3 7,14
EXT 8,0 3 8,14
PAL 0

ROOM 2
c,c,c,c,m,i,t,0,0,m,f,t,c,c,c,c
c,c,c,c,m,0,s,0,0,n,f,t,c,c,c,c
c,c,a,q,k,f,t,0,12,m,0,f,p,b,c,c
c,c,m,i,f,f,u,0,0,n,i,f,f,s,c,c
c,c,n,0,g,v,e,0,0,d,v,h,f,t,c,c
c,c,m,l,s,0,0,0,0,0,0,o,0,t,c,c
c,c,m,f,t,0,12,0,0,0,0,m,l,u,c,c
c,c,m,0,u,0,0,0,0,0,0,n,f,s,c,c
c,c,m,f,s,0,0,0,0,0,0,m,f,t,c,c
c,c,m,i,t,0,0,0,12,0,0,m,0,t,c,c
c,c,m,0,s,0,0,0,0,0,0,o,l,s,c,c
c,c,m,f,j,p,q,r,q,p,p,k,f,u,c,c
c,c,n,f,f,0,f,0,f,0,f,f,0,s,c,c
c,c,d,v,v,w,v,x,v,v,w,v,x,e,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,u,t,v,w,x
EXT 7,0 1 7,14
EXT 8,0 1 8,14
PAL 0

ROOM 3
c,c,c,c,n,f,u,0,0,o,f,s,c,c,c,c
c,c,c,c,n,0,s,12,0,m,l,u,c,c,c,c
c,c,a,p,k,f,t,0,0,n,0,j,p,b,c,c
c,c,m,0,l,0,s,0,0,m,0,f,i,s,c,c
c,c,o,0,g,v,e,0,0,d,v,h,0,j,q,q
c,c,n,f,s,0,12,0,0,0,0,m,l,0,l,0
c,c,m,0,u,0,0,0,0,0,0,d,w,v,w,v
c,c,m,l,t,0,0,0,0,0,0,0,0,0,0,0
c,c,o,0,s,0,0,0,0,12,0,0,0,12,0,0
c,c,m,f,s,0,0,12,0,0,0,a,q,r,p,q
c,c,m,0,s,0,0,0,0,0,0,m,0,0,l,0
c,c,n,i,j,p,b,0,0,a,p,k,f,g,w,w
c,c,m,f,0,l,s,0,0,o,l,0,i,s,c,c
c,c,d,v,h,0,t,0,0,n,0,g,w,e,c,c
c,c,c,c,o,f,t,0,12,o,f,t,c,c,c,c
c,c,c,c,o,0,t,0,0,o,0,s,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 7,15 1 7,1
EXT 8,15 1 8,1
EXT 7,0 4 7,14
EXT 8,0 4 8,14
EXT 15,7 5 1,7
EXT 15,8 5 1,8
PAL 0

ROOM 4
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,a,p,q,r,p,q,p,p,r,p,p,b,c,c
c,c,o,f,0,l,i,0,f,0,0,i,f,s,c,c
c,c,n,0,g,v,w,v,v,x,v,h,0,t,c,c
c,c,m,i,u,0,0,0,0,0,0,m,0,s,c,c
c,c,m,0,s,0,0,0,0,0,0,n,l,u,c,c
c,c,n,f,s,0,0,0,0,0,12,m,l,s,c,c
c,c,m,0,t,0,0,0,0,0,0,m,0,t,c,c
c,c,m,0,s,0,12,0,0,0,0,o,0,s,c,c
c,c,m,l,s,0,0,0,0,0,0,m,f,s,c,c
c,c,n,0,j,q,b,0,0,a,q,k,0,t,c,c
c,c,m,f,0,l,s,0,0,n,0,i,l,s,c,c
c,c,d,v,h,0,t,0,0,m,i,g,w,e,c,c
c,c,c,c,n,f,s,0,12,n,f,u,c,c,c,c
c,c,c,c,n,0,u,0,0,o,0,s,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 7,15 3 7,1
EXT 8,15 3 8,1
PAL 0

ROOM 5
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,a,p,q,r,p,q,p,r,p,q,p,b,c,c
c,c,m,f,0,l,i,0,0,l,0,i,f,s,c,c
q,p,k,0,g,w,v,x,v,w,v,h,0,j,q,q
f,0,0,f,s,0,0,0,0,0,0,m,f,0,l,l
v,w,v,w,e,0,0,0,0,0,0,d,v,v,v,v
0,0,0,0,0,0,0,0,0,0,12,0,0,0,0,0
0,0,0,12,0,0,0,0,0,0,0,0,0,0,12,0
q,r,q,p,b,0,0,12,0,0,0,a,q,p,q,p
f,0,i,f,s,0,0,0,0,0,0,m,f,0,i,f
v,w,h,0,j,p,b,0,0,a,p,k,0,g,v,v
c,c,m,f,0,f,t,0,0,m,f,0,f,s,c,c
c,c,d,v,h,0,u,0,12,n,0,g,v,e,c,c
c,c,c,c,o,i,t,0,0,m,0,t,c,c,c,c
c,c,c,c,m,f,s,0,0,o,f,t,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 0,7 3 14,7
EXT 0,8 3 14,8
EXT 7,15 6 7,1
EXT 8,15 6 8,1
EXT 15,7 7 1,7
EXT 15,8 7 1,8
PAL 0

ROOM 6
c,c,c,c,m,l,s,0,0,m,l,s,c,c,c,c
c,c,c,c,m,0,u,0,0,o,0,s,c,c,c,c
c,c,a,p,k,f,s,12,0,m,0,j,p,b,c,c
c,c,m,i,0,l,t,0,0,n,l,0,i,s,c,c
c,c,n,f,g,x,e,0,0,d,v,h,f,t,c,c
c,c,m,0,s,0,0,0,0,0,0,m,0,s,c,c
c,c,m,i,t,0,0,0,0,12,0,m,0,s,c,c
c,c,o,f,s,0,0,0,0,0,0,o,f,u,c,c
c,c,m,0,u,0,12,0,0,0,0,m,i,u,c,c
c,c,m,0,s,0,0,0,0,0,0,n,0,s,c,c
c,c,m,0,s,0,0,0,0,0,0,m,0,t,c,c
c,c,n,i,j,p,q,q,r,q,q,k,i,s,c,c
c,c,m,f,0,0,f,i,0,0,0,i,l,s,c,c
c,c,d,v,w,w,x,v,v,v,w,x,v,e,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 7,0 5 7,14
EXT 8,0 5 8,14
PAL 0

ROOM 7
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,a,r,p,p,p,r,p,p,q,p,r,b,c,c
c,c,m,f,i,0,0,f,i,0,0,0,f,s,c,c
p,p,k,0,g,w,x,w,w,x,w,h,i,j,p,r
i,f,0,l,s,0,0,0,0,0,0,m,0,l,i,f
v,w,x,w,e,0,0,0,12,0,0,d,v,w,v,v
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,12,0,0,0,12,0,0,0,0,0
p,q,p,r,b,0,0,0,0,0,0,a,p,q,p,r
f,0,f,l,s,0,0,0,0,0,0,m,f,0,i,f
v,w,h,0,j,p,q,p,p,r,p,k,i,g,v,w
c,c,m,l,i,0,0,i,l,0,i,f,0,s,c,c
c,c,d,v,w,v,v,x,v,v,v,x,v,e,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
WAL a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x
EXT 0,7 5 14,7
EXT 0,8 5 14,8
EXT 15,7 8 1,7
EXT 15,8 8 1,8
PAL 0

ROOM 8
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,a,q,q,q,q,q,q,q,q,q,q,b,c,c
c,c,m,g,v,v,v,v,v,v,v,v,h,s,c,c
c,c,m,s,0,0,0,0,0,0,0,0,m,s,c,c
p,p,k,s,0,12,0,0,0,12,0,0,m,s,c,c
w,w,w,e,0,0,0,0,0,0,0,0,m,s,c,c
0,0,0,0,0,0,0,y,z,0,0,0,m,s,c,c
0,0,0,0,0,0,0,10,11,0,0,0,m,s,c,c
p,p,p,b,0,0,0,0,0,0,0,0,m,s,c,c
w,w,h,s,0,12,0,0,0,0,12,0,m,s,c,c
c,c,m,s,0,0,0,0,0,0,0,0,m,s,c,c
c,c,m,j,q,q,q,q,q,q,q,q,k,s,c,c
c,c,d,v,v,v,v,v,v,v,v,v,v,e,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
c,c,c,c,c,c,c,c,c,c,c,c,c,c,c,c
WAL a,b,d,e,m,p,s,v,q,r,t,u,w,x,n,o
END undefined 7,7
END undefined 8,7
END undefined 7,8
END undefined 8,8
PAL 0

ROOM 9
c,m,f,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,l,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,i,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,l,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,i,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,f,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,i,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,0,t,c,0,0,0,0,0,0,0,0,0,0,0
c,m,l,t,c,0,0,0,0,0,0,0,0,0,0,0
PAL 0

TIL 10
10101010
10101011
10100100
01010011
01001000
00100111
00011000
00000111
>
01010101
01010100
01001011
00100100
00100011
00011000
00000111
00000000

TIL 11
01010101
11010101
00100101
11001010
00010010
11100100
00011000
11100000
>
10101010
00101010
11010010
00100100
11000100
00011000
11100000
00000000

TIL 12
00001000
00000100
00100100
00010110
10010101
01010101
01010101
11111111
>
00000100
00001000
00011000
00011001
01101001
10101010
10101010
11111111

TIL a
00111111
01111111
11110111
11100010
11000000
11100000
11110000
11000000

TIL b
11111100
11111110
01101111
00100111
00000011
00000111
00001111
00000011

TIL c
10101010
01010101
10101010
01010101
10101010
01010101
10101010
01010101

TIL d
11000000
11110000
11100000
11000000
11100010
11110111
01111111
00111111

TIL e
00000011
00000111
00000111
00000011
00100111
01101111
11111110
11111100

TIL f
00000000
01000000
01100000
00000000
00000000
00000100
00001100
00000000

TIL g
00000000
00000000
00000000
00000000
00000000
00000010
00001111
00000111

TIL h
00000000
00000000
00000000
00000000
01100000
01000000
11110000
11100000

TIL i
00000000
01100000
00100000
00000000
00000110
00000010
00000000
00000000

TIL j
00000111
00001111
00000011
00000010
00000000
00000000
00000000
00000000

TIL k
11100000
11110000
11100000
01000000
00000000
00000000
00000000
00000000

TIL l
00000000
00000010
00000110
00000000
00000000
01000000
01100000
00000000

TIL m
11100000
11110000
11100000
11000000
11100000
11110000
11000000
11100000

TIL n
11100000
11000000
11100000
11100000
11000000
11110000
11100000
11000000

TIL o
11100000
11110000
11100000
11000000
11100000
11100000
11000000
11100000

TIL p
11111111
11111111
11101101
01000100
00000000
00000000
00000000
00000000

TIL q
11111111
11111111
01001101
01100100
00000000
00000000
00000000
00000000

TIL r
11111111
11111111
11001001
01100100
00000000
00000000
00000000
00000000

TIL s
00000111
00001111
00000111
00000011
00000111
00001111
00000011
00000111

TIL t
00000111
00000011
00000111
00001111
00000111
00000011
00001111
00000111

TIL u
00000011
00000111
00001111
00000011
00000111
00000011
00001111
00000111

TIL v
00000000
00000000
00000000
00000000
01000100
11101101
11111111
11111111

TIL w
00000000
00000000
00000000
00000000
01010000
11011010
11111111
11111111

TIL x
00000000
00000000
00000000
00000000
00000000
11011010
11111111
11111111

TIL y
00000111
00011000
00100111
01001000
01010011
10100100
10101011
10101010
>
00000000
00000111
00011000
00100011
00100100
01001011
01010100
01010101

TIL z
11100000
00011000
11100100
00010010
11001010
00100101
11010101
01010101
>
00000000
11100000
00011000
11000100
00100100
11010010
00101010
10101010

SPR A
00011000
00011000
01111110
01111110
10111101
00111100
00100100
01100110
POS 0 6,7

SPR a
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 0 9,6

SPR b
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 1 10,7

SPR c
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 2 6,9

SPR d
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 3 6,7

SPR e
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 4 7,6

SPR f
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 5 7,5

SPR g
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 6 7,9

SPR h
11111111
10101011
11010101
10101011
11010101
11111111
00011000
00011000
>
11111111
11010101
10101011
11010101
10101011
11111111
00011000
00011000
POS 8 3,7

DLG a
sign: welcome to the secret maze of the green house, try to see if you can find your way to the end. keep your eyes pealed for hints along the way.

DLG b
Montreal is the capital of Canada. true=down false=up.

DLG c
incorrect... the capital of Canada is Ottawa. +1 point

DLG d
ostriches bury there heads in the sand. true=up false=right.

DLG e
incorrect...it has never been seen done. +1point

DLG f
1/4 of human bones are part of the foot. true= strait false= down

DLG g
incorrect... 1/4 of human bones are in the foot. +1point

DLG h
you have made it to the end.. but before you jump in the hole count your points (points are bad) 0=good 1=alright 2=OK 3=terrible

END 0
To this day Mr. gardener was never heard of again. only one person knows what happened but he has keep it a secret.

END undefined
To this day Mr. gardener was never heard of again. only one person knows what happened but he has keep it a secret.
\