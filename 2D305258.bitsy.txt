RWE und Hambi: eine Liebesgeschichte

# BITSY VERSION 5.3

! ROOM_FORMAT 1

PAL 0
20,133,56
66,36,21
255,210,46

ROOM 0
0,0,0,0,c,0,0,0,c,0,0,a,0,0,0,0
0,0,0,a,0,c,0,a,0,0,0,0,a,0,a,0
0,0,0,0,0,0,0,0,0,c,0,0,0,0,0,a
0,d,0,0,0,0,0,a,0,0,0,a,0,c,0,0
0,0,d,0,0,0,0,0,0,0,0,0,0,0,0,0
0,c,0,d,0,0,c,0,0,a,0,0,0,c,a,0
0,0,0,d,d,d,0,0,0,0,0,a,0,0,0,0
0,0,a,0,a,0,d,d,0,a,0,0,0,0,c,0
0,0,0,0,0,0,c,0,d,d,0,d,d,0,0,0
0,0,0,a,0,0,0,0,a,0,d,0,a,d,d,d
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,0,0,a,0,0,0,0,c,a,0,0,0,0
0,0,0,0,a,0,0,0,0,0,0,0,0,0,0,a
0,0,0,a,0,a,0,0,0,0,a,0,0,0,0,a
a,0,0,0,0,0,0,a,0,0,0,0,a,0,a,0
0,0,0,0,0,0,0,0,a,0,0,0,0,a,0,0
NAME start
EXT 15,9 1 0,8
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,a,0,0,0,0,0,a,0,0,0,c,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,c,0,0,0
0,0,0,0,0,a,0,0,0,a,0,0,c,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,a,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,c,0,0,c,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,d,d,0,0,0
d,d,0,0,0,0,0,0,0,0,d,0,0,d,d,d
0,0,d,0,d,d,d,0,0,d,0,0,0,0,0,0
0,0,0,d,0,0,0,d,d,0,0,0,0,c,0,0
0,c,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,a,0,0,0,0,0,0,a,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,0,0,0,a,0,0
0,0,0,0,0,0,0,a,0,0,0,0,0,0,0,0
NAME path
EXT 15,8 2 0,9
EXT 0,8 0 15,9
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,e,0,0,0
0,0,0,0,0,0,0,a,0,0,c,0,e,0,0,0
0,0,0,c,0,0,0,0,0,0,0,0,e,0,c,0
0,0,0,0,a,c,0,0,0,0,a,0,e,0,0,0
0,c,0,0,0,0,0,0,0,0,0,0,e,0,0,0
0,0,0,0,0,0,0,0,0,a,0,0,e,0,0,0
0,0,0,0,c,0,c,0,0,0,0,0,e,0,a,0
0,0,0,0,0,0,0,0,0,0,0,0,e,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,e,0,0,0
d,0,d,d,d,0,d,0,a,0,0,0,f,0,0,0
0,d,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,a,0,0,0,0,0,0,0,0,0,c,0
0,0,0,0,0,0,c,0,0,0,0,a,0,0,0,0
0,a,0,0,0,0,0,0,0,a,0,0,0,0,a,0
0,0,0,0,0,0,0,0,0,0,0,0,c,0,0,0
0,0,0,0,0,0,0,a,0,0,0,0,0,0,0,0
NAME maschinen
EXT 0,9 1 15,8
PAL 0

TIL a
00000000
00111110
01000001
01100001
01011111
01000101
11010101
11111111
NAME stump1
WAL true

TIL c
00000000
00111100
01000010
01100010
01011110
01000010
01010010
01111110
NAME stump2
WAL true

TIL d
00000000
00001100
01100000
00000110
00000000
00110000
00000011
00000000
NAME path

TIL e
01001010
10101010
11001010
01000011
01010110
01010110
01000010
01000010
NAME trunk
WAL true

TIL f
01000010
01100010
01000010
01000010
11000110
10000101
10000011
01111110
NAME treebase
WAL true

SPR A
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 0 1,1

SPR a
00000000
00000000
01010001
01110001
11110010
00111100
00111100
00100100
NAME fox
DLG SPR_0
POS 0 8,12

SPR b
00000000
01100000
01110000
10111000
00111000
01111000
11101110
00000100
>
00000000
01100010
01101110
10111110
00111100
00111000
00011110
00000100
NAME bird
DLG SPR_2
POS 0 11,5

SPR c
01111100
10000010
10111010
10000010
10111010
10000010
10000010
11111110
DLG SPR_1
POS 1 7,4

SPR d
00011000
00111100
00011001
00111101
00111110
00111100
00100100
00100100
DLG SPR_3
POS 2 11,9

SPR e
00011000
00111100
00011000
00111100
01111110
10111101
00100100
00100100
DLG SPR_4
POS 2 9,7

SPR f
00011000
00111100
00011000
00111100
01111110
01111110
00100100
00100100
DLG SPR_5
POS 2 7,11

ITM 0
00000000
00000000
00000000
00111000
01000110
10000010
10001100
01110000
NAME tea

DLG SPR_0
"""
Wir können keine Kohle essen
Wir können keine Kohle atmen
aber...

#WIRVONHIERSINDDAFüR
(wer?)
"""

DLG SPR_2
"""
Die fällen die Bäume
Die fällen die Menschen
Die fällen das Leben
aber...
#WIRVONHIERSINDDAFüR
(sind wir?)
"""

DLG SPR_3
"""
Kohle!

Kohle und...

VERANTWORTUNG - EIN GROSSES THEMA FüR RWE!
"""

DLG SPR_4
"""
Ein Tod, ja
aber...
RWE IST DAS SICHERHEITSNETZ
DER ENERGIEWENDE!
"""

DLG SPR_5
"""
Nachhaltigkeit...?

RWE STEHT FüR SICHERHEIT IN DER ENERGIEVERSORGUNG!
"""

DLG SPR_1
"""
"Leider sind hier nicht nur Bäume gefallen"
- 19.09.2018

"""

VAR a
42

