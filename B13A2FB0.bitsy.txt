
Bit Fishing.  Try to catch them all.  Good Luck!

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
143,125,99
19,18,54
245,85,57

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 0 8,10
ITM 1 10,8
ITM 1 6,9
EXT 8,10 1 5,5
END 0 6,9
END 0 10,8
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,1c,q,r
0,0,0,0,0,0,0,0,0,0,0,0,0,v,u,s
f,f,f,f,f,f,f,f,f,f,f,f,f,f,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 0 5,12
ITM 1 8,9
ITM 1 9,11
ITM 1 1,14
ITM 1 10,12
EXT 5,12 2 5,5
END 0 8,9
END 0 9,11
END 0 10,12
END 0 1,14
PAL 0

ROOM 2
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,p,1c,q,r
0,0,0,0,0,0,0,0,0,0,0,0,w,v,u,s
f,f,f,f,f,f,f,f,f,f,f,f,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 0 0,14
ITM 1 11,12
ITM 1 9,13
ITM 1 8,9
ITM 1 10,8
ITM 1 15,9
ITM 1 1,9
EXT 0,14 3 5,5
END 0 1,9
END 0 10,8
END 0 11,12
END 0 9,13
END 0 15,9
END 0 8,9
PAL 0

ROOM 3
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,n,o,0,0,0,0
0,0,0,0,0,0,0,0,0,0,b,b,p,1c,q,r
0,0,0,0,0,0,0,0,0,0,1b,1b,w,v,u,s
f,f,f,f,f,f,f,f,f,f,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 1 10,7
ITM 1 11,9
ITM 1 13,12
ITM 1 14,12
ITM 1 5,13
ITM 1 4,12
ITM 1 1,10
ITM 1 1,12
ITM 1 3,7
ITM 0 7,10
EXT 7,10 4 5,5
END 0 3,7
END 0 10,7
END 0 1,10
END 0 11,9
END 0 1,12
END 0 4,12
END 0 5,13
END 0 13,12
END 0 14,12
PAL 0

ROOM 4
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,m,n,o,0,0,0,0
0,0,0,0,0,0,0,0,0,b,b,b,p,1c,q,r
0,0,0,0,0,0,0,0,0,19,1b,1b,w,v,u,s
f,f,f,f,f,f,f,f,f,10,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 0 9,9
ITM 1 3,12
ITM 1 4,13
ITM 1 1,8
ITM 1 2,9
ITM 1 14,9
ITM 1 15,11
ITM 1 9,12
ITM 1 7,10
ITM 1 7,7
EXT 9,9 5 5,5
END 0 7,7
END 0 7,10
END 0 2,9
END 0 1,8
END 0 3,12
END 0 4,13
END 0 9,12
END 0 14,9
END 0 15,11
PAL 0

ROOM 5
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,l,m,n,o,0,0,0,0
0,0,0,0,0,0,0,0,1a,b,b,b,p,1c,q,r
0,0,0,0,0,0,0,0,18,19,1b,1b,w,v,u,s
f,f,f,f,f,f,f,f,17,10,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 1 10,12
ITM 1 7,11
ITM 1 1,9
ITM 0 11,7
EXT 11,7 6 5,5
END 0 1,9
END 0 7,11
END 0 10,12
PAL 0

ROOM 6
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,k,l,m,n,o,0,0,0,0
0,0,0,0,0,0,0,b,1a,b,b,b,p,1c,q,r
0,0,0,0,0,0,0,16,18,19,1b,1b,w,v,u,s
f,f,f,f,f,f,f,11,17,10,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
ITM 0 11,9
ITM 1 3,12
ITM 1 4,13
ITM 1 5,8
ITM 1 8,12
ITM 1 10,13
ITM 1 0,8
ITM 1 15,7
ITM 1 15,12
ITM 1 13,10
EXT 11,9 8 5,5
END 0 5,8
END 0 0,8
END 0 15,7
END 0 3,12
END 0 8,12
END 0 4,13
END 0 10,13
END 0 15,12
END 0 13,10
PAL 0

ROOM 8
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,k,l,m,n,o,0,0,0,0
0,0,0,0,0,0,i,b,1a,b,b,b,p,1c,q,r
0,0,0,0,0,0,13,16,18,19,1b,1b,w,v,u,s
f,f,f,f,f,f,f,11,17,10,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME room 7
ITM 1 0,14
ITM 1 1,13
ITM 1 2,13
ITM 1 3,13
ITM 1 4,14
ITM 1 5,13
ITM 1 6,13
ITM 1 7,13
ITM 1 8,13
ITM 1 9,13
ITM 1 10,12
ITM 1 11,12
ITM 1 12,12
ITM 1 13,12
ITM 1 14,12
ITM 1 15,12
ITM 0 5,10
EXT 5,10 9 5,5
END 0 0,14
END 0 1,13
END 0 2,13
END 0 3,13
END 0 4,14
END 0 5,13
END 0 6,13
END 0 7,13
END 0 8,13
END 0 9,13
END 0 10,12
END 0 11,12
END 0 12,12
END 0 13,12
END 0 14,12
END 0 15,12
PAL 0

ROOM 9
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,k,l,m,n,o,0,0,0,0
0,0,0,0,g,h,i,b,1a,b,b,b,p,1c,q,r
0,0,0,0,15,14,13,16,18,19,1b,1b,w,v,u,s
f,f,f,f,f,f,f,11,17,10,z,y,x,0,t,1d
f,f,f,f,1e,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,e,0,0,0,0,0,0,0,0,0,0
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d
d,d,d,d,d,d,d,d,d,d,a,b,b,b,b,b
d,a,b,c,d,a,b,b,b,b,b,b,b,b,b,b
a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME room 8
ITM 0 6,9
ITM 1 7,13
ITM 1 5,13
ITM 1 3,10
ITM 1 10,10
ITM 1 11,9
ITM 1 12,9
ITM 1 13,10
ITM 1 12,7
ITM 1 3,8
ITM 1 6,7
ITM 1 9,8
END 0 3,8
END 0 3,10
END 0 5,13
END 0 7,13
END 0 6,7
END 0 9,8
END 0 10,10
END 0 11,9
END 0 12,9
END 0 12,7
END 0 13,10
END 1 6,9
PAL 0

TIL 10
11111111
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL 11
10110100
01101010
00110101
00010010
00011001
00001110
00000011
00000000
WAL true

TIL 12
11000011
00111110
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL 13
01010111
00101011
10010111
01001011
01000110
10001100
00011000
11111111
WAL true

TIL 14
11111000
11100100
01111011
10101100
01010100
00101011
00011111
00000111
WAL true

TIL 15
00000010
00000010
00000001
00000001
00000000
00000000
00000000
00000000
WAL true

TIL 16
11111111
01010101
00111000
00001111
00000001
00111100
01000001
01000001
WAL true

TIL 17
10000111
11111000
10000000
10000000
01000000
01000000
11000000
00000000
WAL true

TIL 18
11111111
10101011
01011110
11110000
00000000
00000000
00000000
00000000
WAL true

TIL 19
11100000
11000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL a
11000001
00111110
10000101
01111010
00010101
10101010
01010101
10101010
WAL true

TIL b
01010101
10101010
01010101
10101010
01010101
10101010
01010101
10101010
WAL true

TIL c
11000011
10111100
01000011
10101100
01010000
10101011
01010100
10101010
WAL true

TIL d
11000011
00111100
10000011
01000100
00111000
10000011
01000100
00111000

TIL e
00000000
10000000
01000000
10100000
01010000
10101000
01010100
10101010
WAL true

TIL f
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL g
00000000
00000000
00000000
00000000
00000000
00000001
00000010
00000010
WAL true

TIL h
00000000
00000001
00000110
00111100
01010011
10101111
01010111
10101010
WAL true

TIL i
00001111
11111101
10101010
01010101
10101010
01010101
10101010
01011101
WAL true

TIL j
00000000
00000000
00000000
00000000
00000000
00000000
00000000
11111111
WAL true

TIL k
00000000
00000000
00000000
00000000
00000000
00000000
00011111
11110000
WAL true

TIL l
00000000
00000000
00000000
00000000
00000000
00000001
11111111
11111111
WAL true

TIL m
00000000
00000000
00000000
10000000
10101000
10101010
11111111
11111111
WAL true

TIL n
00000000
00000000
00000001
00000001
00000111
00011111
11111111
11111111
WAL true

TIL o
00110000
11110000
10110000
01010000
10101100
01010100
10101111
11110011
WAL true

TIL p
10000000
01110000
11001111
01111001
00111011
01011111
10101111
01010111
WAL true

TIL q
00000001
00000011
00000110
00000101
00001110
00011001
00010100
11101010
WAL true

TIL r
11100000
00100000
01100000
01000000
01000000
01000000
01000000
01000000
WAL true

TIL s
10000000
10000000
10000000
10000000
10000000
11000000
01000000
01100000
WAL true

TIL t
00000010
00000010
00000001
00000000
00000000
00000000
00000000
00000000
WAL true

TIL u
10110000
01011000
10111000
01110100
11110010
00010000
00001000
00000110
WAL true

TIL v
11010101
10101010
01010101
00101000
00000000
11111111
11100000
10000000
WAL true

TIL w
10101010
01010101
00001000
00000000
00000000
00000000
00000011
11111100
WAL true

TIL x
00000111
00011100
01110000
11000000
00000000
00000000
00000000
00000000
WAL true

TIL y
00111111
11101010
00110100
10000111
01011100
01011000
01100000
00000000
WAL true

TIL z
11111111
00001100
00000011
00000000
00000000
00000000
00000000
00000000
WAL true

TIL 1a
01010101
10101010
01010101
10101010
01010101
10101010
01011001
11111111
WAL true

TIL 1b
01010101
10101010
01000001
00100000
00000000
00000000
00000000
00000000
WAL true

TIL 1c
00000000
00000000
00000000
00000000
10000000
01100000
11011100
11111111
WAL true

TIL 1d
00100000
00010000
11110000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL 1e
00000000
00000000
00000001
00110010
00110100
00011000
00010000
00101000
WAL true

SPR A
00000000
00000000
00000000
00011000
00011000
00000000
00000000
00000000
POS 0 5,5

SPR e
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
DLG SPR_2
POS 0 0,0

ITM 0
00000000
00001000
00110000
01011010
11111100
00110010
00001000
00000000
>
00000000
00000000
00000000
00111100
01011000
11111101
00010010
00000000
NAME Fish
DLG ITM_0

ITM 1
00000000
00001000
00110000
01011010
11111100
00110010
00001000
00000000
>
00000000
00000000
00000000
00111100
01011000
11111101
00010010
00000000
NAME Decoy
DLG ITM_1

DLG ITM_0
"""
{
  - {item "Fish"} == 1 ?
    Nice Catch!  Try to catch another

  - {item "Fish"} == 2 ?
    Wow! This is turning out to be a good spot.
  - {item "Fish"} == 3 ?
    What?  This is almost too easy.
  - {item "Fish"} == 4 ?
    Okay, keep it up.  We are starting to see something.
  - {item "Fish"} == 5 ?
    I hope you are hungry.  This is getting silly.
  - {item "Fish"} == 6 ?
    Um..have you reached your daily limit yet?
  - {item "Fish"} == 7 ?
    Better start cleaning these before the sun goes down.
  - {item "Fish"} == 8 ?
    Okay, that's enough.  Leave some for the next one.
}
"""

DLG ITM_1
Oops!  Lost your bait.

END 0
Lost Your Bait!  Try Again

END 1
Congratulations!  You are a Master Bit Fisher.


