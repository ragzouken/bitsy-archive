
it's not that deep

# BITSY VERSION 4.4

! ROOM_FORMAT 1

PAL 0
NAME depth 0
136,206,249
31,31,222
255,255,255

PAL 1
NAME depth 1
31,31,222
22,22,156
255,255,255

PAL 2
NAME depth 2
22,22,156
14,14,97
255,255,255

PAL 3
NAME depth 3
12,12,78
0,0,0
255,255,255

PAL 4
NAME depth 4
5,5,37
0,0,0
0,0,83

PAL 5
NAME depth 5
5,5,54
0,0,0
11,11,79

PAL 6
NAME death
0,0,0
255,255,255
0,0,0

ROOM 0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,d,d,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,e,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,d,d,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,d,d,d,0,0,0,0,0,0,0
0,0,c,0,0,0,0,0,0,0,0,d,d,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,e,0,0,0,0,0,0,c,0
0,0,0,0,d,d,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,f,0,d,d,0,0
g,f,j,g,k,j,g,j,k,g,g,k,g,j,f,j
h,h,h,h,h,h,h,h,i,h,h,h,h,h,h,h
NAME surface
EXT 0,15 1 0,0
EXT 1,15 1 1,0
EXT 2,15 1 2,0
EXT 3,15 1 3,0
EXT 4,15 1 4,0
EXT 5,15 1 5,0
EXT 6,15 1 6,0
EXT 7,15 1 7,0
EXT 8,15 1 8,0
EXT 9,15 1 9,0
EXT 10,15 1 10,0
EXT 11,15 1 11,0
EXT 12,15 1 12,0
EXT 13,15 1 13,0
EXT 14,15 1 14,0
EXT 15,15 1 15,0
PAL 0

ROOM 1
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,e,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,d,d,d,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,m,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,l,0,0,0,0,0,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,d,0,0,0,0,0,0,0,0,0
0,0,0,0,0,0,0,d,d,0,0,0,0,0,0,0
u,0,0,0,0,0,0,0,0,d,d,0,0,0,0,0
h,t,0,0,0,0,0,0,0,0,0,0,c,0,0,0
h,s,0,0,0,0,0,0,0,0,0,0,0,0,0,n
h,h,v,0,0,0,0,0,0,0,0,0,0,q,o,h
h,h,w,0,0,0,0,0,0,0,0,r,p,h,h,h
NAME depth 1
EXT 3,15 2 3,0
EXT 4,15 2 4,0
EXT 5,15 2 5,0
EXT 6,15 2 6,0
EXT 7,15 2 7,0
EXT 8,15 2 8,0
EXT 9,15 2 9,0
EXT 10,15 2 10,0
PAL 1

ROOM 2
x,x,x,0,0,0,0,0,d,0,0,x,x,x,x,x
x,x,x,11,0,0,0,d,0,0,0,x,x,x,x,x
x,x,x,10,0,0,0,0,0,0,13,x,x,x,x,x
x,x,x,z,0,0,0,d,0,0,15,x,x,x,x,x
x,x,x,y,0,0,0,d,0,0,x,x,x,x,x,x
x,x,x,x,11,0,0,0,0,0,x,x,x,x,x,x
x,x,x,x,y,0,0,0,0,0,x,x,x,x,x,x
x,x,x,x,x,0,0,d,0,p,x,x,x,x,x,x
x,x,x,x,x,0,0,d,0,x,x,x,x,x,x,x
x,x,x,x,x,0,e,d,14,x,x,x,x,x,x,x
x,x,x,x,x,0,0,0,13,x,x,x,x,x,x,x
x,x,x,x,x,0,d,0,12,x,x,x,x,x,x,x
x,x,x,x,x,0,d,0,x,x,x,x,x,x,x,x
x,x,x,x,x,11,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,y,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,d,x,x,x,x,x,x,x,x
NAME depth 2
EXT 6,15 3 6,0
EXT 7,15 3 7,0
PAL 2

ROOM 3
p,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,0,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,v,0,1a,h,h,1b,0,1c,1d,1e
x,x,x,x,x,x,y,0,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,17,x,x,x,x
x,x,z,0,12,x,x,x,17,x,x,z,18,x,x,x
x,x,s,r,x,x,x,x,x,x,x,16,19,x,x,x
x,x,x,x,x,x,x,x,x,x,x,12,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 3
ITM 0 7,7
EXT 15,6 4 0,6
PAL 3

ROOM 4
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,h,h,x,x,x,x,x,x,x,x,x,x,x,x
h,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
h,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
1f,0,0,0,0,0,1g,1h,1i,1k,1k,1j,x,x,x,x
x,x,x,x,11,0,0,0,0,0,0,0,18,x,x,x
x,x,x,x,x,x,x,x,11,0,0,0,1l,x,x,x
x,x,x,x,x,x,x,x,1m,0,0,0,0,0,0,0
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 4
ITM 1 1,6
EXT 15,9 5 0,9
PAL 4

ROOM 5
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-1
ITM 0 14,9
EXT 4,9 6 3,9
PAL 5

ROOM 6
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-2
ITM 0 14,9
EXT 5,9 7 4,9
PAL 5

ROOM 7
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-3
ITM 0 14,9
EXT 6,9 8 5,9
PAL 5

ROOM 8
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-4
ITM 0 14,9
EXT 6,9 9 5,9
PAL 5

ROOM 9
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-5
ITM 0 14,9
EXT 7,9 a 6,9
PAL 5

ROOM a
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-6
ITM 0 14,9
EXT 7,9 b 6,9
PAL 5

ROOM b
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-7
ITM 0 14,9
EXT 8,9 c 7,9
PAL 5

ROOM c
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-8
ITM 0 14,9
EXT 8,9 d 7,9
PAL 5

ROOM d
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-9
ITM 0 14,9
EXT 8,9 e 7,9
PAL 5

ROOM e
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-10
ITM 0 14,9
EXT 9,9 f 8,9
PAL 5

ROOM f
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-11
ITM 0 14,9
EXT 9,9 g 8,9
PAL 5

ROOM g
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-12
ITM 0 14,9
EXT 9,9 h 8,9
PAL 5

ROOM h
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-13
ITM 0 14,9
EXT 10,9 i 9,9
PAL 5

ROOM i
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-14
ITM 0 14,9
EXT 10,9 j 9,9
PAL 5

ROOM j
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-15
ITM 0 14,9
EXT 10,9 k 9,9
PAL 5

ROOM k
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-16
ITM 0 14,9
EXT 11,9 l 10,9
PAL 5

ROOM l
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-17
ITM 0 14,9
EXT 11,9 m 10,9
PAL 5

ROOM m
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-18
ITM 0 14,9
EXT 11,9 n 10,9
PAL 5

ROOM n
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-19
ITM 0 14,9
EXT 11,9 o 10,9
PAL 5

ROOM o
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-20
ITM 0 14,9
EXT 12,9 p 11,9
PAL 5

ROOM p
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-21
ITM 0 14,9
EXT 12,9 q 11,9
PAL 5

ROOM q
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-22
ITM 0 14,9
EXT 12,9 r 11,9
PAL 5

ROOM r
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-23
ITM 0 14,9
EXT 12,9 s 11,9
PAL 5

ROOM s
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-24
ITM 0 14,9
EXT 12,9 t 11,9
PAL 5

ROOM t
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-25
ITM 0 14,9
EXT 13,9 u 12,9
PAL 5

ROOM u
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-26
ITM 0 14,9
EXT 13,9 v 12,9
PAL 5

ROOM v
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-27
ITM 0 14,9
EXT 13,9 w 12,9
PAL 5

ROOM w
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-28
ITM 0 14,9
EXT 13,9 x 12,9
PAL 5

ROOM x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-29
ITM 0 14,9
EXT 13,9 y 12,9
PAL 5

ROOM y
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1q,0,x
x,x,x,x,x,x,x,x,x,x,x,x,1p,0,0,x
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME depth 5-30
ITM 0 14,9
EXT 13,9 z 1,1
PAL 5

ROOM z
0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x
0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x
0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x
0,0,0,0,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1r,x,x
x,x,x,x,x,x,x,x,x,x,x,x,1s,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,1o,1n,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x
NAME death
ITM 2 1,0
ITM 2 2,0
ITM 2 0,1
ITM 2 0,0
ITM 2 2,1
ITM 2 3,0
ITM 2 3,1
ITM 2 3,2
ITM 2 2,2
ITM 2 1,2
ITM 2 0,2
ITM 2 1,3
ITM 2 0,3
ITM 2 2,3
ITM 2 4,3
ITM 2 3,3
ITM 2 4,2
ITM 2 4,0
ITM 2 4,1
ITM 2 5,0
ITM 2 5,1
ITM 2 5,2
ITM 2 5,3
ITM 2 5,4
ITM 2 4,4
ITM 2 2,4
ITM 2 3,4
ITM 2 1,4
ITM 2 0,4
ITM 2 0,5
ITM 2 1,5
ITM 2 2,5
ITM 2 4,5
ITM 2 3,5
ITM 2 5,5
ITM 2 6,5
ITM 2 6,4
ITM 2 6,3
ITM 2 6,2
ITM 2 6,1
ITM 2 6,0
ITM 2 6,8
ITM 2 5,8
ITM 2 4,8
ITM 2 3,8
ITM 2 2,8
ITM 2 1,8
ITM 2 0,8
ITM 2 7,8
ITM 2 8,8
ITM 2 9,8
ITM 2 10,8
ITM 2 11,8
ITM 2 11,7
ITM 2 12,7
ITM 2 12,6
ITM 2 13,6
ITM 2 13,5
ITM 2 13,4
ITM 2 13,3
ITM 2 13,2
ITM 2 13,1
ITM 2 13,0
ITM 2 15,0
ITM 2 15,1
ITM 2 15,2
ITM 2 15,3
ITM 2 15,4
ITM 2 15,5
ITM 2 15,6
ITM 2 15,7
ITM 2 15,8
ITM 2 15,9
ITM 2 15,10
ITM 2 14,10
ITM 2 13,10
ITM 2 11,10
ITM 2 12,10
ITM 2 10,10
ITM 2 9,10
ITM 2 8,10
ITM 2 7,10
ITM 2 6,10
ITM 2 5,10
ITM 2 4,10
ITM 2 2,10
ITM 2 3,10
ITM 2 1,10
ITM 2 0,10
ITM 2 6,6
ITM 2 5,6
ITM 2 4,6
ITM 2 2,6
ITM 2 1,6
ITM 2 0,6
ITM 2 0,7
ITM 2 2,7
ITM 2 1,7
ITM 2 3,6
ITM 2 3,7
ITM 2 4,7
ITM 2 5,7
ITM 2 6,7
ITM 2 7,7
ITM 2 9,7
ITM 2 8,7
ITM 2 10,7
ITM 2 10,6
ITM 2 11,6
ITM 2 12,5
ITM 2 11,5
ITM 2 10,5
ITM 2 12,4
ITM 2 11,4
ITM 2 10,4
ITM 2 10,3
ITM 2 11,3
ITM 2 12,3
ITM 2 12,2
ITM 2 12,1
ITM 2 12,0
ITM 2 11,0
ITM 2 10,0
ITM 2 8,0
ITM 2 9,0
ITM 2 7,0
ITM 2 7,1
ITM 2 9,1
ITM 2 8,1
ITM 2 10,1
ITM 2 11,1
ITM 2 11,2
ITM 2 10,2
ITM 2 9,2
ITM 2 9,3
ITM 2 8,3
ITM 2 8,2
ITM 2 7,2
ITM 2 7,3
ITM 2 7,4
ITM 2 8,4
ITM 2 9,4
ITM 2 9,5
ITM 2 8,5
ITM 2 7,5
ITM 2 7,6
ITM 2 8,6
ITM 2 9,6
ITM 2 15,11
ITM 2 14,11
ITM 2 13,11
ITM 2 12,11
ITM 2 11,11
ITM 2 10,11
ITM 2 9,11
ITM 2 8,11
ITM 2 7,11
ITM 2 6,11
ITM 2 5,11
ITM 2 4,11
ITM 2 3,11
ITM 2 2,11
ITM 2 1,11
ITM 2 0,11
ITM 2 0,12
ITM 2 2,12
ITM 2 1,12
ITM 2 3,12
ITM 2 4,12
ITM 2 5,12
ITM 2 6,12
ITM 2 7,12
ITM 2 8,12
ITM 2 9,12
ITM 2 10,12
ITM 2 11,12
ITM 2 12,12
ITM 2 13,12
ITM 2 14,12
ITM 2 15,12
ITM 2 15,13
ITM 2 15,14
ITM 2 15,15
ITM 2 14,15
ITM 2 13,15
ITM 2 13,14
ITM 2 14,14
ITM 2 14,13
ITM 2 13,13
ITM 2 12,13
ITM 2 12,14
ITM 2 12,15
ITM 2 11,15
ITM 2 11,14
ITM 2 11,13
ITM 2 10,13
ITM 2 10,14
ITM 2 10,15
ITM 2 9,15
ITM 2 9,14
ITM 2 9,13
ITM 2 8,13
ITM 2 8,14
ITM 2 8,15
ITM 2 7,15
ITM 2 7,14
ITM 2 7,13
ITM 2 6,13
ITM 2 6,14
ITM 2 6,15
ITM 2 5,15
ITM 2 5,14
ITM 2 5,13
ITM 2 4,13
ITM 2 4,14
ITM 2 4,15
ITM 2 3,15
ITM 2 3,14
ITM 2 3,13
ITM 2 2,13
ITM 2 2,14
ITM 2 2,15
ITM 2 1,15
ITM 2 1,14
ITM 2 1,13
ITM 2 0,13
ITM 2 0,14
ITM 2 0,15
END 0 0,1
END 0 1,0
END 0 2,1
END 0 1,2
PAL 6

TIL 10
11110000
11110000
11110000
11110000
11110000
11100000
11100000
11000000
WAL true

TIL 11
10000000
11000000
11000000
11100000
11100000
11100000
11100000
11110000
WAL true

TIL 12
00001111
00011111
00011111
00111111
00111111
01111111
01111111
01111111
WAL true

TIL 13
00000011
00000011
00000111
00000111
00000111
00001111
00001111
00001111
WAL true

TIL 14
00000000
00000001
00000001
00000001
00000001
00000011
00000011
00000011
WAL true

TIL 15
00001111
00001111
00111111
01111111
01111111
01111111
01111111
01111111
WAL true

TIL 16
11111000
11111000
11110000
11110000
11100000
11000000
10000000
10000000
WAL true

TIL 17
11111111
11111100
11110000
11110000
11100000
11000000
11000000
11000000
WAL true

TIL 18
11111111
01111111
00111111
00011111
00001111
00001111
00001111
00001111
WAL true

TIL 19
00001111
00001111
00001111
00001111
00011111
00011111
00111111
11111111
WAL true

TIL a
00000000
00011000
00011100
00110010
11100001
00000000
00000000
00000000
WAL false

TIL b
00000000
00011000
00111000
01001100
10000111
00000000
00000000
00000000

TIL c
00000000
00111000
01100100
01000100
01000100
00111000
00000000
00000000
>
00000000
00000000
00010000
00101000
00010000
00000000
00000000
00000000

TIL d
00000000
00000000
00001100
00110011
11000000
00000000
00000000
00000000

TIL e
00000000
00000000
00010000
00101000
00010000
00000000
00000000
00000000
>
00000000
00111000
01100100
01000100
01000100
00111000
00000000
00000000

TIL f
00000000
00000000
01000000
00011011
01100001
11111111
11111111
11111111

TIL g
00000000
01001000
10101010
01010011
11100111
11111111
11111111
11111111

TIL h
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111

TIL i
11111111
11111111
11001110
10100001
10000100
11001111
11111111
11111111

TIL j
00000000
00000011
01001101
10010110
11110010
11001111
11111111
11111111

TIL k
00000000
00000001
01010010
01101010
10111101
11011111
11111111
11111111

TIL l
00000000
00000000
01100000
10010000
10110000
01100000
00000000
00000000
>
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

TIL m
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
>
01111000
11100100
11000100
10000100
10000100
01111000
00000000
00000000

TIL n
00000000
00000000
00000000
00001111
00111111
01111111
11111111
11111111
WAL true

TIL o
00000001
00000011
00000111
00011111
01111111
11111111
11111111
11111111

TIL p
01111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL q
00000000
00000000
00000000
00000000
00000000
00000111
00111111
11111111

TIL r
00000000
00000000
00000001
00000111
00001111
00111111
01111111
11111111
WAL true

TIL s
11110000
11110000
11111000
11111000
11111100
11111100
11111100
11111110
WAL true

TIL t
10000000
10000000
11000000
11000000
11000000
11100000
11100000
11100000
WAL true

TIL u
11000000
11110000
11111000
11111100
11111100
11111110
11111110
11111111
WAL true

TIL v
00000000
10000000
11000000
11100000
11100000
11110000
11110000
11111000
WAL true

TIL w
11111000
11111100
11111100
11111110
11111110
11111111
11111111
11111111
WAL true

TIL x
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL y
11111000
11111000
11111100
11111100
11111100
11111100
11111110
11111110
WAL true

TIL z
11000000
11000000
11000000
11100000
11100000
11110000
11110000
11110000
WAL true

TIL 1a
01111111
00111111
00000111
00000000
00000000
00000000
00111111
01111111

TIL 1b
11111111
11111100
11100000
00000000
00000000
00000000
11111100
11111111

TIL 1c
11111111
01111111
00011100
00000000
00000000
00000000
00000011
00001111

TIL 1d
11110000
10000000
00000000
00000000
00000000
00111100
11111111
11111111

TIL 1e
11111110
00000000
00000000
00000000
00000000
00000000
10000000
11111000

TIL 1f
00111100
01111110
11111111
11111111
11111111
11111111
01111110
00111100
WAL true

TIL 1g
11111111
00111111
00001111
00000001
00000000
00000000
00000000
00000000
WAL true

TIL 1h
11111111
11111111
11111111
11111111
01111111
00111111
00001110
00000000
WAL true

TIL 1i
11111111
11111111
11000000
10000000
00000000
00000000
00000000
00000000
WAL false

TIL 1j
11111111
11111111
00000111
00000011
00000011
00000001
00000000
00000000
WAL false

TIL 1k
11111111
11111111
00000000
00000000
00000000
00000000
00000000
00000000

TIL 1l
00001111
00001111
00001111
00001111
00001111
00000111
00000111
00000011

TIL 1m
11110000
11110000
11110000
11111000
11111000
11111100
11111110
11111111

TIL 1n
11111111
11111111
11111111
11000011
10000001
10011001
10000001
10000001

TIL 1o
11111111
11111111
11111111
11111001
11110010
10100100
00010010
01001000

TIL 1p
11111111
11111111
11111111
11000110
11000000
10000000
00000000
00000000
WAL true

TIL 1q
11111100
11110000
11100000
11100000
11000000
11000000
11000000
10000000
WAL true

TIL 1r
00000011
00001111
00011111
00011111
00111111
00111111
00111111
01111111

TIL 1s
00000000
00000000
00000000
00111001
00111111
01111111
11111111
11111111

SPR A
00000011
00000011
00110000
00110000
11111000
01110000
10100000
10100000
>
00000000
00000000
00110100
00110000
11111000
01110000
10100000
10100000
POS 0 3,3

SPR a
00000000
00000000
01110100
10011000
01100100
00000000
00000000
00000000
DLG SPR_0
POS 0 8,5

SPR b
00000000
00000000
01011100
00110010
01001100
00000000
00000000
00000000
DLG SPR_1
POS 0 3,11

SPR c
00000000
00011000
01110011
11011110
11111110
11111111
00100000
00000000
DLG SPR_2
POS 0 10,13

SPR d
00111100
01111001
11111011
10101111
11111110
10001110
11111011
01110001
DLG SPR_3
POS 1 10,6

SPR e
00000000
00000000
00110111
01111101
11111111
01101100
11000110
00000000
DLG SPR_4
POS 1 3,12

SPR f
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000

SPR g
00000000
00000000
00000000
01000010
11011011
00111100
00111100
01000010
DLG SPR_5
POS 1 12,14

SPR h
00001111
00010101
00001011
00000110
00000001
00011010
00000101
00111010
DLG SPR_6
POS 2 9,6

SPR i
00000000
00000000
00111110
01111111
01111111
00101010
00101010
01001001
>
00111110
01111111
01111111
00101010
00101010
01001001
00000000
00000000
DLG SPR_7
POS 4 10,8

ITM 0
00000000
00000000
00000000
00111100
01111110
01100110
01111110
01111110
NAME chest
DLG ITM_0

ITM 1
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
NAME block
DLG ITM_1

ITM 2
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
NAME fake

DLG ITM_0
"""
you open the chest.......

... it's empty?!
"""

DLG SPR_1
blub blub

DLG SPR_2
holy shit, a human!

DLG SPR_0
glub glub.

DLG SPR_3
woah

DLG SPR_4
i'm a turtle!

DLG SPR_5
i saw something shiny down there !!??

DLG SPR_6
"""
...

.....

..don't cry for me.
"""

DLG ITM_1
a rock fell and blocked the way back!!

DLG SPR_7
"""
i don't know how i got here...

i'd say i was in over my head... if i had one
"""

END 0
ouch

VAR a
42


