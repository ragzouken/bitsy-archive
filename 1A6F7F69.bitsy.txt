
Welcome to the great cave expedition. 

# BITSY VERSION 4.5

! ROOM_FORMAT 1

PAL 0
NAME his face
138,217,205
43,181,99
240,247,253

PAL 1
255,60,29
255,230,169
255,255,255

PAL 2
188,188,134
140,97,76
243,255,243

PAL 3
NAME starting to get deeper
182,214,126
69,77,56
233,254,233

PAL 4
NAME journey's end
0,0,0
61,52,16
255,255,255

ROOM 2
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,c,c,c,c,c,c,c,c,c,c,0,0,0,0
b,0,0,0,0,0,0,0,0,0,0,0,0,b,b,b
b,d,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,d,c,c,c,c,c,c,c,c,c,c,c,c,b,b
b,d,0,0,0,0,0,0,0,0,0,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,d,b,b,b,b,b
b,b,b,b,b,b,b,b,c,c,d,c,c,c,c,b
b,b,b,b,0,c,0,0,0,0,d,0,0,0,0,b
b,b,b,b,0,c,d,b,b,b,b,b,b,b,b,b
c,c,c,c,c,c,d,b,b,b,b,b,b,b,b,b
0,0,0,0,0,0,d,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME entrance
ITM 2 11,4
ITM 1 8,10
ITM 0 4,13
ITM b 12,7
EXT 15,3 3 0,3
EXT 5,10 0 0,0
EXT 14,7 3 0,9
END 0 0,13
PAL 0

ROOM 3
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
0,0,0,0,0,0,0,0,0,0,0,c,c,c,c,b
b,b,b,b,b,b,b,d,b,b,0,0,c,c,c,b
b,b,b,b,b,b,b,d,b,b,b,0,0,b,0,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
0,0,0,0,0,0,b,d,b,b,b,b,b,b,b,b
c,c,c,c,0,0,b,d,b,b,b,b,b,b,b,b
0,0,0,c,c,c,b,d,b,b,b,b,b,b,b,b
b,b,0,0,0,0,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
NAME into the depths
ITM 3 7,9
ITM 0 13,5
EXT 7,15 4 7,0
EXT 0,3 2 14,3
EXT 0,9 2 14,7
PAL 2

ROOM 4
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
NAME the long climb
ITM 4 7,4
ITM 5 7,12
EXT 7,15 5 7,0
EXT 7,0 3 7,15
PAL 2

ROOM 5
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
NAME long climb 2
EXT 7,15 6 7,0
EXT 7,0 4 7,15
PAL 3

ROOM 6
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
NAME long climb final
ITM 6 7,8
EXT 7,0 0 0,0
EXT 7,15 7 7,13
PAL 3

ROOM 7
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,d,b,b,b,b,b,b,b,b
b,b,0,0,0,0,c,d,c,b,b,b,b,b,b,b
b,0,0,0,0,0,c,d,c,0,b,b,b,b,b,b
0,0,0,0,0,0,c,d,c,0,0,b,b,b,b,b
0,0,0,0,0,0,c,d,c,0,0,0,0,0,b,b
0,0,0,0,0,0,c,d,c,0,0,0,0,0,0,b
0,0,0,0,0,0,c,d,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,d,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,d,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,d,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,c,c,0,0,0,0,0,0,0
0,0,0,0,0,0,c,c,c,0,0,0,0,0,0,0
c,c,c,c,c,c,c,0,c,c,c,c,c,c,c,c
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME antechamber
ITM 8 7,14
EXT 7,0 0 0,0
EXT 0,14 8 15,14
EXT 15,14 9 0,14
PAL 4

ROOM 8
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,0,0,0,0,0,0,0,0,0,0,c,c,b
b,b,c,d,b,b,b,b,b,b,b,b,0,0,0,b
b,0,0,d,0,0,0,0,c,c,c,0,b,b,b,b
b,b,b,b,b,b,b,0,0,0,c,0,0,b,b,b
0,c,0,c,0,0,b,b,0,0,c,c,c,b,b,b
c,c,0,c,c,c,c,b,b,0,0,0,0,0,b,b
0,0,0,0,0,0,c,c,b,b,b,b,0,0,0,b
b,b,b,b,b,0,0,c,c,0,0,c,d,b,b,b
b,b,b,b,b,b,0,0,c,c,0,c,d,c,0,0
b,b,b,b,b,b,b,0,0,c,c,c,d,c,c,c
b,b,b,b,b,b,b,b,0,0,0,0,d,0,0,0
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME cave escalation
ITM 9 14,5
EXT 0,10 a 15,10
EXT 1,6 0 0,0
EXT 5,3 0 14,10
EXT 15,14 7 0,14
PAL 4

ROOM 9
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,b,b,0,0,0,0,0,0,0,0,b,b,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,b,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,b,b,b
b,0,0,0,0,0,c,c,c,c,c,c,c,0,b,b
b,c,c,c,c,c,c,0,0,0,0,0,c,c,b,b
b,0,0,0,0,0,0,0,b,b,b,0,0,0,0,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
0,0,b,b,b,b,b,0,0,0,0,0,b,b,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,0,b,b
0,0,0,0,0,0,0,0,0,0,c,c,c,c,b,b
c,c,c,c,c,c,c,c,c,c,c,0,0,0,0,b
0,0,0,0,0,0,0,0,0,0,0,0,b,b,b,b
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME dead end
EXT 0,14 7 15,14
EXT 14,13 9 14,6
EXT 14,6 9 14,13
EXT 13,5 0 0,0
PAL 4

ROOM a
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
b,b,0,0,0,0,0,0,0,0,0,0,0,b,b,b
0,0,0,0,0,0,0,0,0,0,0,0,0,b,b,b
c,c,c,0,0,0,0,0,0,0,0,0,0,b,b,b
0,0,c,c,c,c,c,c,c,c,c,b,b,b,b,0
b,0,0,0,0,0,0,0,0,0,b,b,b,b,0,0
b,b,b,b,b,b,d,b,b,b,b,b,0,0,0,0
b,b,b,b,b,b,d,b,b,b,b,b,0,0,0,0
b,b,b,b,b,b,d,b,b,b,b,0,0,0,0,0
b,b,b,b,b,b,d,b,b,b,b,c,c,c,c,c
b,b,c,c,0,0,d,0,0,0,0,0,0,0,0,0
b,b,0,0,0,b,b,b,b,b,b,b,b,b,b,b
0,0,0,0,b,b,c,c,c,c,c,c,0,0,b,b
b,b,b,0,0,0,0,0,0,0,0,c,c,c,c,b
b,b,b,b,b,b,b,b,b,b,0,0,0,0,0,0
b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b
NAME choice
ITM 7 10,10
EXT 15,10 8 0,10
END 1 0,4
END 2 15,14
END 3 0,12
PAL 4

TIL a
11111111
10000001
10000001
10011001
10011001
10000001
10000001
11111111
WAL true

TIL b
11111111
11111111
11111111
11111111
11111111
11111111
11111111
11111111
WAL true

TIL c
00000000
00000000
00000000
00000000
00000000
00000000
00000000
00000000
WAL true

TIL d
11111111
00000000
00000000
11111111
00000000
00000000
11111111
00000000

SPR A
00011000
00011000
00011000
00111100
01111110
10111101
00100100
00100100
POS 2 2,13

SPR a
00000000
00000000
01010001
01110001
01110010
01111100
00111100
00100100
DLG SPR_0

SPR c
00000000
00110000
00111100
00110110
00100110
00000110
00001010
00001010
DLG SPR_3
POS 3 13,5

SPR d
00011000
00011000
00001000
00011000
00011000
00011000
00101000
00101000
DLG SPR_4
POS 3 4,10

SPR e
00000000
00000000
00011000
00101000
01000000
01010000
01101000
00000000
DLG SPR_2
POS 9 2,6

ITM 0
00000000
01111100
01111110
01000010
01111110
01000010
01111110
00000000
NAME Journal 1
DLG ITM_0

ITM 1
00000000
01111100
01111110
01000010
01111110
01000010
01111110
00000000
NAME Journal 2
DLG ITM_1

ITM 2
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 3
DLG ITM_2

ITM 3
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 4
DLG ITM_3

ITM 4
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 5
DLG ITM_4

ITM 5
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 6
DLG ITM_5

ITM 6
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 7
DLG ITM_6

ITM 7
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 8
DLG ITM_7

ITM 8
00000000
01111100
01111100
01000010
01111110
01000010
01111110
00000000
NAME Journal 9
DLG ITM_8

ITM 9
00000000
00000010
00111000
01000100
01110100
10111000
00010000
00000100
>
00000000
10000000
00111000
01000100
01110101
00111000
00010000
01000000
NAME gem
DLG ITM_a

ITM a
00000000
00000000
00000000
00000000
00000000
00000000
00100000
00000000
NAME fallen event
DLG ITM_9

ITM b
00000000
01111100
01111110
01000010
01111110
01000010
01111110
00000000
NAME journal secret
DLG ITM_b

DLG SPR_0
I'm not a cat

DLG SPR_1
I'm a pile of snow with eyes

DLG ITM_0
It's the beginning of my journey. TBH I'm excited. 

DLG ITM_1
First leg of my journey: my mortal enemy, ladders. 

DLG ITM_2
"""
Uh-oh. Looks like this ledge has no ladder. 
Luckily I know I can jump up or down one square.
"""

DLG ITM_3
I begin my trek down the ladder. This doesn't look so bad? 

DLG ITM_4
Deeper and deeper, rung after rung... 

DLG ITM_5
The top already looks so far away... 

DLG ITM_6
How much further?!

DLG SPR_3
"""
{
  - {item "Journal 4"} == 1 ?
    I see you've come back without going too far. Have you come to your senses? 
  - else ?
    Head my warning: no one who has descended has ever returned. 
}
"""

DLG SPR_4
"""
Congratulations! You found a secret area. 
I'm sure there are many of these deeper in the cave.  
"""

DLG ITM_7
I have a strong feeling that if I take the wrong path here, I could get lost... There must be someone or something down me that can show me the way?

DLG ITM_8
I've fallen. The ladder is so far above me - much too far to reach. Looks like I'm going to have to find another way out. 

DLG SPR_2
"""
{
  - {item "gem"} == 1 ?
    All the way in the left most cavern, you'll find a room where the path splits three ways. Take the bottom left passage to get out. 
  - else ?
    Find me something shiny and I'll tell you how to get out. 
}
"""

DLG ITM_9
I've fallen. The ladder is so far above me - much too far to reach. Looks like I'm going to have to find another way out. 

DLG ITM_a
You've found a beautiful gem stone. 

DLG ITM_b
Oh wow - I didn't see it at first! But just ahead of me is a secret passage. Keep heading to the right. 

END 0
Hecka nope nope nope nope. You're allergic to adventure. You exit the cave continuing on without a care in the world.  

END 1
You head deeper into the cave. Step by step, you don't seem to be making progress. You spend some time here until you find a small family of moles. Over time, you earn their trust and become part of their family. 

END 2
You descend deeper into the cave, hopefully this is the way out? Twist after twist, you continue onwards. Until you accidentally discover the lost city of Atlantis. Here - you live out your life with the Atlantians in peace.  

END 3
You forge ahead. Step after step - more than once you question your way. But in the end, a sliver of light gives way to the outdoors. Fresh air, breathe, you made it.  

VAR a
42


