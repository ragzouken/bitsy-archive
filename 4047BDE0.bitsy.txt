Forbidden Numbers

# BITSY VERSION 3.5

! ROOM_FORMAT 1

PAL 0
0,93,179
128,159,255
245,249,255

PAL 1
179,0,4
255,128,130
245,240,240

PAL 2
178,131,0
255,221,128
245,244,240

PAL 3
0,179,0
128,255,128
240,245,240

ROOM 0
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,e,d,d,c,c,c,c,c,d,d,e,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,d,d,d,d,d,d,d,d,e,f,f
f,f,f,f,e,d,d,d,d,d,d,d,d,e,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,c,c,c,f,f,f,f,f,d,d,d,f,f
f,f,f,c,c,c,c,c,c,c,c,d,d,d,f,f
f,f,f,c,c,c,c,c,c,c,c,d,d,d,f,f
f,f,f,f,c,c,c,c,c,c,c,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
h,g,i,i,i,i,i,i,i,i,i,i,i,i,i,i
WAL f
EXT 2,0 2 1,0
EXT 0,0 7 1,0
PAL 0

ROOM 1
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,f,d,d,d,d,d,d,d,d,d,e,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,c,c,c,f,f
f,f,f,e,e,e,f,f,f,f,f,c,c,c,f,f
f,f,f,e,e,e,f,f,f,f,f,c,c,c,f,f
f,f,f,e,d,d,d,d,d,d,d,d,d,c,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,e,f,f
f,f,f,d,d,d,f,f,f,f,f,e,e,e,f,f
f,f,f,d,d,d,f,f,f,f,f,e,e,e,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,c,d,d,d,d,d,d,d,d,d,c,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,i,i,i,i,h,g,i,i,i,i,i,i
EXT 2,0 3 1,0
EXT 0,0 6 1,0
PAL 0

ROOM 2
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,f,d,d,d,d,d,d,d,d,d,f,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,e,e,e,e,e,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,e,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,f,e,e,e,e,e,e,e,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,h,g,i,i,i,i,i,i,i,i,i,i,i,i
EXT 2,0 4 1,0
EXT 0,0 0 1,0
PAL 1

ROOM 3
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,e,d,d,c,c,c,c,c,d,d,e,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,f,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,i,i,i,i,i,i,h,g,i,i,i,i
EXT 2,0 5 1,0
EXT 0,0 1 1,0
PAL 1

ROOM 4
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,f,e,e,d,d,d,d,e,e,e,e,f,f
f,f,f,e,e,d,d,d,d,d,e,e,e,e,f,f
f,f,f,e,e,d,d,d,d,d,e,e,e,e,f,f
f,f,f,e,e,e,f,c,c,c,f,f,f,f,f,f
f,f,f,e,e,e,f,c,c,c,f,f,f,f,f,f
f,f,f,e,e,e,e,d,d,d,e,e,e,f,f,f
f,f,f,f,e,e,e,d,d,d,e,e,e,e,f,f
f,f,f,f,f,f,f,c,c,c,f,e,e,e,f,f
f,f,f,f,f,f,f,c,c,c,f,e,e,e,f,f
f,f,f,e,e,e,f,c,c,c,f,e,e,e,f,f
f,f,f,e,e,d,d,d,d,d,d,d,e,e,f,f
f,f,f,e,e,d,d,d,d,d,d,d,e,e,f,f
f,f,f,f,e,d,d,d,d,d,d,d,e,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,h,g,i,i,i,i,i,i,i,i,i,i
EXT 2,0 6 1,0
EXT 0,0 2 1,0
PAL 3

ROOM 5
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,e,d,d,c,c,c,c,c,d,d,e,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,e,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,c,d,d,d,d,d,d,d,d,d,e,f,f
f,f,f,c,c,c,f,f,f,f,f,e,e,e,f,f
f,f,f,c,c,c,f,f,f,f,f,e,e,e,f,f
f,f,f,c,c,c,c,c,c,c,c,d,d,d,f,f
f,f,f,c,c,c,c,c,c,c,c,d,d,d,f,f
f,f,f,f,c,c,c,c,c,c,c,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,i,i,i,i,i,i,i,i,h,g,i,i
EXT 2,0 7 1,0
EXT 0,0 3 1,0
PAL 3

ROOM 6
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,f,d,d,d,d,d,d,d,d,d,f,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,c,c,c,f,f,f,f,f,d,d,d,f,f
f,f,f,c,c,c,f,f,f,f,f,d,d,d,f,f
f,f,f,c,c,d,d,d,d,d,d,d,d,c,f,f
f,f,f,f,c,d,d,d,d,d,d,d,d,c,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,f,f,f,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,f,f,f,f,f,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,e,e,e,e,e,e,e,e,d,d,d,f,f
f,f,f,f,e,e,e,e,e,e,e,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,i,i,h,g,i,i,i,i,i,i,i,i
EXT 2,0 1 1,0
EXT 0,0 4 1,0
PAL 2

ROOM 7
b,0,a,f,f,f,f,f,f,f,f,f,f,f,f,f
f,a,f,f,d,d,d,d,d,d,d,d,d,f,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,e,e,e,f,f
f,f,f,d,d,d,f,f,f,f,f,e,e,e,f,f
f,f,f,d,d,d,f,f,f,f,f,e,e,e,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,e,f,f
f,f,f,d,d,d,c,c,c,c,c,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,d,d,d,f,f,f,f,f,d,d,d,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,d,d,d,d,d,d,d,d,d,d,d,f,f
f,f,f,f,d,d,d,d,d,d,d,d,d,f,f,f
f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f
i,i,i,i,i,i,i,i,i,i,i,i,i,i,h,g
EXT 2,0 0 1,0
EXT 0,0 5 1,0
PAL 2

TIL a
00000000
00000000
00001000
00100100
00100100
00001000
00000000
00000000
>
00000000
00000100
00010010
01001001
01001001
00010010
00000100
00000000

TIL b
00000000
00000000
00010000
00100100
00100100
00010000
00000000
00000000
>
00000000
00100000
01001000
10010010
10010010
01001000
00100000
00000000

TIL c
00100001
01000010
10000100
00001001
00010010
00100100
01001000
10010000
>
00000000
00000000
00100000
00000000
00000000
00000000
00000010
00000000

TIL d
00100001
01000010
10000100
00001001
00010010
00100100
01001000
10010000

TIL e
00000000
00000000
00100000
00000000
00000000
00000000
00000010
00000000
>
00100001
01000010
10000100
00001001
00010010
00100100
01001000
10010000

TIL f
00000000
00000000
00100000
00000000
00000000
00000000
00000010
00000000

TIL g
11111111
00000000
11111100
00000010
11111110
11111100
00000000
11111111

TIL h
11111111
00000000
00111111
01000000
01111111
00111111
00000000
11111111

TIL i
11111111
00100010
10001000
00100010
10001000
00100010
10001000
11111111

SPR A
00000000
00000000
01011000
11011010
01011011
00000010
00111000
00010000
>
00000000
00000000
00011010
01011011
11011010
01000000
00011100
00001000
POS 0 1,0

SPR a
00000000
00111000
00011000
00011000
00011000
00011000
00111100
00000000
POS 0 1,1

SPR b
00000000
00111100
00110000
00111100
00001100
00001100
00111100
00000000
POS 1 1,1

SPR c
00000000
00111100
00001100
00001100
00111100
00110000
00111100
00000000
POS 2 1,1

SPR d
00000000
00111100
00110000
00110000
00111100
00110100
00111100
00000000
POS 3 1,1

SPR e
00000000
00111100
00001100
00111100
00001100
00001100
00111100
00000000
POS 4 1,1

SPR f
00000000
00111100
00111100
00001100
00001100
00001100
00001100
00000000
POS 5 1,1

SPR g
00000000
00110100
00110100
00111100
00001100
00001100
00001100
00000000
POS 6 1,1

SPR h
00000000
00111100
00110100
00111100
00110100
00110100
00111100
00000000
POS 7 1,1

DLG a
It's said a secret lies beyond the tide. Count pairs of waves and the water may open wide.

DLG b
A river forks and glistens blue. The secret rests with the lesser of the two.

DLG c
A flicker and a spark. Your clue resides in the difference of each mark.

DLG d
A fiery murmur and blazing flames. Between the two a secret hangs.

DLG e
The plains squeak, the soil sighs. The winds whisper, echo, and multiply.

DLG f
With Mother Earth's two forest clues in tow, you'll find the secret floating high where only the tallest trees grow.

DLG g
The sun does shine with brilliant clarity. Divide its rays to seek prosperity.

DLG h
The final secret is in your sight. To find it, fill nothingness with light.

